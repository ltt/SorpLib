![](LTT_Adsorption_Library/Resources/Images/LTT_logo_RWTH_en.png)

SorpLib - Adsorption Energy Systems Library
=====================================

**SorpLib - Adsorption Energy Systems Library** (short **SorpLib**) is a Modelica model library for simulating adsorption energy systems, such as adsorption chillers, adsorption heat pumps, adsorption thermal storage systems, or desiccant systems. The library is being developed at RWTH Aachen University, Institute of Technical Thermodynamics, Sorption Systems Engineering group in Aachen, Germany.

This repository is used to further develop the library and make it available under the BSD 3-Clause License. 

We would like to build up an active **SorpLib** community. Therefore, we would appreciate a short notice to sorplib@ltt.rwth-aachen.de, when you download and use the **SorpLib**.

### License

The model library **SorpLib** is released by RWTH Aachen University, Institute of Technical Thermodynamics under the [BSD 3-Clause License](https://git.rwth-aachen.de/ltt_public/SorpLib/Sysblob/develop/LICENSE.md).

### Overview

The library provides basic models necessary to model adsorption energy systems. Based on these models, all types of adsorption energy systems can be modeled. In the applications package, a few examples demonstrate possible adsorption-based systems.

### Dependencies

This library was mainly developed using Dymola. Additionally, it was checked that most models also work with OpenModelica.

Regarding non-adsorption specific models, the library depends on the Modelica Standard Library. The comercial libraries [TIL Suite](https://www.tlk-thermo.com/index.php/de/softwareprodukte/til-suite) and [TIL Media Suite](https://www.tlk-thermo.com/index.php/de/softwareprodukte/tilmedia-suite) are not used within this branch:

*TIL Suite is suitable for the stationary and transient simulation of freely configurable thermodynamic systems. Thanks to the substance property library, TILMedia – a component of the TIL Suite – system simulations can be performed extremely quickly and accurately.*

TIL and TILMedia are commercial libraries provided by [TLK-Thermo GmbH](https://www.tlk-thermo.com/index.php/en/). At the moment, it is necessary to have these libraries to use the **SorpLib**. For the future, a light version of TIL and TIL-Media may be freely available.

### Version

The current version 0.2.0 is a pre-release.

### How to cite SorpLib

Please cite **SorpLib** as follows:

- Mirko Engelpracht, Patrik Postweiler, Daniel Rezo, Niklas von der Assen. 
  SorpLib: An open-source Modelica library for dynamic modelling and optimization of adsorption-based process and energy systems. 
  In: RWTH Git respository, Institute of Technical Thermodynamics (LTT), RWTH Aachen University, 2025. 
  [link](https://git.rwth-aachen.de/ltt/SorpLib/-/tree/SorpLib_V3_OpenModelica)
  
- Uwe Bau, Franz Lanzerath, Manuel Gräber, Heike Schreiber, Niklas Thielen, André Bardow.
  Adsorption energy systems library - Modeling adsorption based chillers, heat pumps, thermal storages and desiccant systems. 
  In: H. Tummescheit und K.-E. Årzén, Hg. Proceedings of the 10th International Modelica Conference. Linköping: Modelica Association, 2014, S. 875-883. ISBN 9789175193809.
  [link](http://www.ep.liu.se/ecp/096/091/ecp14096091.pdf)

### How to contribute to the Development of SorpLib

You are invited to activly contribute to the development of **SorpLib**.  
Issues can be reported using this site's [Issues section](https://git.rwth-aachen.de/ltt_public/SorpLib/issues).  
Furthermore, you are welcome to contribute via [Merge Requests](https://git.rwth-aachen.de/ltt_public/SorpLib/merge_requests).  
The workflow for changes is described in our [Wiki](https://git.rwth-aachen.de/ltt_public/SorpLib/wikis/home).  

### Contact

E-Mail: sorplib@ltt.rwth-aachen.de  
Internet: www.ltt.rwth-aachen.de