within SorpLib.Icons;
partial package FittingsPackage "Icon for packages containing fitting models"
  extends Modelica.Icons.Package;

  annotation (Documentation(info="<html>
<p>
This icon indicates a package containing fitting models.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"), Icon(graphics={
      Rectangle(
        extent={{-80,50},{80,-50}},
        lineColor={28,108,200},
        fillColor={28,108,200},
        fillPattern=FillPattern.HorizontalCylinder),
          Line(
            points={{-60,-30},{-60,30},{60,-30},{60,30}},
            thickness=0.5,
        color={255,255,255}),
      Line(
        points={{-60,70},{40,70}},
        color={0,0,0},
        thickness=0.5),
      Polygon(
        points={{60,70},{40,80},{40,60},{60,70}},
        lineColor={0,0,0},
        lineThickness=0.5,
        fillPattern=FillPattern.HorizontalCylinder,
        fillColor={0,0,0})}));
end FittingsPackage;
