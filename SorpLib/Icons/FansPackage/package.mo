within SorpLib.Icons;
partial package FansPackage "Icon for packages containing fan models"
  extends Modelica.Icons.Package;

  annotation (Documentation(info="<html>
<p>
This icon indicates a package containing pump models.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"), Icon(graphics={
          Rectangle(
            extent={{-90,44},{90,-48}},
            fillColor={244,125,35},
            fillPattern=FillPattern.HorizontalCylinder,
        lineColor={0,0,0}),
          Polygon(
            points={{-48,-48},{-72,-88},{72,-88},{48,-48},{-48,-48}},
            lineColor={0,0,0},
            pattern=LinePattern.None,
            fillPattern=FillPattern.VerticalCylinder,
        fillColor={0,0,0}),
          Ellipse(
            extent={{-80,78},{80,-82}},
            fillPattern=FillPattern.Sphere,
            fillColor={244,125,35},
        lineColor={0,0,0}),
          Polygon(
            points={{46,20},{46,-20},{86,0},{46,20}},
            pattern=LinePattern.None,
            fillPattern=FillPattern.HorizontalCylinder,
            fillColor={255,255,255}),
      Polygon(
        points={{-100,100},{-100,100}},
        lineColor={0,0,0},
        pattern=LinePattern.None,
        fillPattern=FillPattern.Sphere,
        fillColor={0,0,0}),
      Polygon(
        points={{54,40},{42,52},{-48,-40},{-38,-50},{54,40}},
        lineColor={0,0,0},
        pattern=LinePattern.None,
        fillPattern=FillPattern.Sphere,
        fillColor={255,255,255},
        smooth=Smooth.Bezier,
        lineThickness=0.5),
      Polygon(
        points={{42,-52},{52,-40},{-38,50},{-48,40},{42,-52}},
        lineColor={0,0,0},
        pattern=LinePattern.None,
        fillPattern=FillPattern.Sphere,
        fillColor={255,255,255},
        smooth=Smooth.Bezier,
        lineThickness=0.5)}));
end FansPackage;
