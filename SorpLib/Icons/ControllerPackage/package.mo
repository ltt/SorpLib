within SorpLib.Icons;
partial package ControllerPackage "Icon for packages containing controller models of sorption modules"
  extends Modelica.Icons.Package;

  annotation (Documentation(info="<html>
<p>
This icon indicates a package containing controller models of sorption modules.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"), Icon(graphics={
        Ellipse(extent={{-80,-80},{80,80}}),
        Line(points={{80,0},{60,0}}),
        Line(points={{69.282,40},{51.962,30}}),
        Line(points={{40,69.282},{30,51.962}}),
        Line(points={{0,80},{0,60}}),
        Line(points={{-40,69.282},{-30,51.962}}),
        Line(points={{-69.282,40},{-51.962,30}}),
        Line(points={{-80,0},{-60,0}}),
        Line(points={{-69.282,-40},{-51.962,-30}}),
        Line(points={{-40,-69.282},{-30,-51.962}}),
        Line(points={{0,-80},{0,-60}}),
        Line(points={{40,-69.282},{30,-51.962}}),
        Line(points={{69.282,-40},{51.962,-30}}),
        Line(points={{80,0},{60,0}}),
        Line(points={{0,0},{-50,50}}),
        Line(points={{0,0},{40,0}})}));
end ControllerPackage;
