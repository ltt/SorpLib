within SorpLib.Icons;
partial package PumpsPackage "Icon for packages containing pump models"
  extends Modelica.Icons.Package;

  annotation (Documentation(info="<html>
<p>
This icon indicates a package containing pump models.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"), Icon(graphics={
          Rectangle(
            extent={{-90,44},{90,-48}},
            fillColor={0,127,255},
            fillPattern=FillPattern.HorizontalCylinder),
          Polygon(
            points={{-48,-48},{-72,-88},{72,-88},{48,-48},{-48,-48}},
            lineColor={0,0,255},
            pattern=LinePattern.None,
            fillPattern=FillPattern.VerticalCylinder),
          Ellipse(
            extent={{-80,78},{80,-82}},
            fillPattern=FillPattern.Sphere,
            fillColor={0,100,199}),
          Polygon(
            points={{-32,24},{-32,-36},{46,-8},{-32,24}},
            pattern=LinePattern.None,
            fillPattern=FillPattern.HorizontalCylinder,
            fillColor={255,255,255})}));
end PumpsPackage;
