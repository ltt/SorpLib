within SorpLib.Icons;
partial package HeatExchangersPackage "Icon for packages containing heat exchanger models"
  extends Modelica.Icons.Package;

  annotation (Documentation(info="<html>
<p>
This icon indicates a package containing heat exchanger models.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"), Icon(graphics={
        Rectangle(
          extent={{-100,-24},{100,-28}},
          fillColor={95,95,95},
          fillPattern=FillPattern.Forward),
        Rectangle(
          extent={{-100,32},{100,28}},
          fillColor={95,95,95},
          fillPattern=FillPattern.Forward),
        Rectangle(
          extent={{-100,62},{100,32}},
          fillPattern=FillPattern.HorizontalCylinder,
          fillColor={0,63,125}),
        Rectangle(
          extent={{-100,-28},{100,-58}},
          fillPattern=FillPattern.HorizontalCylinder,
          fillColor={0,63,125}),
        Rectangle(
          extent={{-100,28},{100,-24}},
          fillPattern=FillPattern.HorizontalCylinder,
          fillColor={0,128,255}),
        Line(
          points={{30,-83},{-60,-83}},
          color={0,128,255}),
        Polygon(
          points={{20,-68},{60,-83},{20,-98},{20,-68}},
          lineColor={0,128,255},
          fillColor={0,128,255},
          fillPattern=FillPattern.Solid),
        Line(
          points={{30,79},{-60,79}},
          color={0,128,255}),
        Polygon(
          points={{-50,94},{-90,79},{-50,64},{-50,94}},
          lineColor={0,128,255},
          fillColor={0,128,255},
          fillPattern=FillPattern.Solid),
      Line(
        points={{-36,40},{-56,32},{-62,18},{-50,10},{-36,0},{-38,-12}},
        color={238,46,47},
        smooth=Smooth.Bezier,
        arrow={Arrow.Filled,Arrow.Filled},
        thickness=1),
      Line(
        points={{-16,40},{-36,32},{-42,18},{-30,10},{-16,0},{-18,-12}},
        color={238,46,47},
        smooth=Smooth.Bezier,
        arrow={Arrow.Filled,Arrow.Filled},
        thickness=1),
      Line(
        points={{-54,40},{-74,32},{-80,18},{-68,10},{-54,0},{-56,-12}},
        color={238,46,47},
        smooth=Smooth.Bezier,
        arrow={Arrow.Filled,Arrow.Filled},
        thickness=1),
      Line(
        points={{13,26},{-7,18},{-13,4},{-1,-4},{13,-14},{11,-26}},
        color={238,46,47},
        smooth=Smooth.Bezier,
        arrow={Arrow.Filled,Arrow.Filled},
        thickness=1,
        origin={67,-16},
        rotation=180),
      Line(
        points={{13,26},{-7,18},{-13,4},{-1,-4},{13,-14},{11,-26}},
        color={238,46,47},
        smooth=Smooth.Bezier,
        arrow={Arrow.Filled,Arrow.Filled},
        thickness=1,
        origin={47,-16},
        rotation=180),
      Line(
        points={{13,26},{-7,18},{-13,4},{-1,-4},{13,-14},{11,-26}},
        color={238,46,47},
        smooth=Smooth.Bezier,
        arrow={Arrow.Filled,Arrow.Filled},
        thickness=1,
        origin={29,-16},
        rotation=180)}));
end HeatExchangersPackage;
