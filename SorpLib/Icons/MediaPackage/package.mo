within SorpLib.Icons;
partial package MediaPackage "Icon for packages containing fluid property models"
  extends Modelica.Icons.Package;

  annotation (Icon(graphics={
        Line(
          points={{-30,30},{-34,98},{-34,98}},
          color={175,175,175}),
        Line(
          points={{-50,-18},{-64,94},{-64,94}},
          color={175,175,175}),
        Line(
          points={{-66,-70},{-84,-6},{-84,-6}},
          color={175,175,175}),
        Line(
          points={{-30,30},{78,30}},
          color={175,175,175}),
        Line(
          points={{-50,-18},{66,-18}},
          color={175,175,175}),
        Line(
          points={{-66,-70},{48,-70}},
          color={175,175,175}),
        Line(
          points={{-66,-70},{-52,-20},{-22,50},{14,76},{58,76},{83,55},{72,2},{58,
              -40},{48,-70}},
          color={64,64,64},
          smooth=Smooth.Bezier),
        Line(
          points={{66,-18},{80,-70}},
          color={175,175,175}),
        Line(
          points={{78,30},{96,-48}},
          color={175,175,175})}), Documentation(info="<html>
<p>
This icon indicates a package containing fluid property models.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end MediaPackage;
