within SorpLib;
package Icons "Library of icons"
  extends Modelica.Icons.IconsPackage;

annotation (Documentation(info="<html>
<p>
This package contains definitions of graphical icons used within the Modelica 
library SorpLib. The icons should be used via inheritance, using the keyword 
\"extends\" in the desired classes.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end Icons;
