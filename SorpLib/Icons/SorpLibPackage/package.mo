within SorpLib.Icons;
partial package SorpLibPackage "Icon for the SorpLib Modelica library"
  extends Modelica.Icons.Package;

annotation (Documentation(info="<html>
<p>
This icon is the base icon for the Modelica library SorpLib.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"), Icon(graphics={
      Rectangle(
        extent={{-92,32},{28,-88}},
        lineColor={0,0,0},
        lineThickness=0.5),
      Rectangle(
        extent={{-32,92},{88,-28}},
        lineColor={0,0,0},
        lineThickness=0.5),
      Line(
        points={{-32,92},{-50,74},{-92,32}},
        color={0,0,0},
        thickness=0.5),
      Line(
        points={{-32,-28},{-50,-46},{-92,-88}},
        color={0,0,0},
        thickness=0.5),
      Line(
        points={{88,-28},{70,-46},{28,-88}},
        color={0,0,0},
        thickness=0.5),
      Line(
        points={{88,92},{70,74},{28,32}},
        color={0,0,0},
        thickness=0.5),
      Ellipse(
        extent={{-94,-90},{-88,-84}},
        lineColor={0,0,0},
        lineThickness=0.5,
        fillColor={0,0,0},
        fillPattern=FillPattern.Solid),
      Ellipse(
        extent={{24,-90},{30,-84}},
        lineColor={0,0,0},
        lineThickness=0.5,
        fillColor={0,0,0},
        fillPattern=FillPattern.Solid),
      Ellipse(
        extent={{84,-30},{90,-24}},
        lineColor={0,0,0},
        lineThickness=0.5,
        fillColor={0,0,0},
        fillPattern=FillPattern.Solid),
      Ellipse(
        extent={{-34,-30},{-28,-24}},
        lineColor={0,0,0},
        lineThickness=0.5,
        fillColor={0,0,0},
        fillPattern=FillPattern.Solid),
      Ellipse(
        extent={{-34,88},{-28,94}},
        lineColor={0,0,0},
        lineThickness=0.5,
        fillColor={0,0,0},
        fillPattern=FillPattern.Solid),
      Ellipse(
        extent={{-94,28},{-88,34}},
        lineColor={0,0,0},
        lineThickness=0.5,
        fillColor={0,0,0},
        fillPattern=FillPattern.Solid),
      Ellipse(
        extent={{24,28},{30,34}},
        lineColor={0,0,0},
        lineThickness=0.5,
        fillColor={0,0,0},
        fillPattern=FillPattern.Solid),
      Ellipse(
        extent={{84,88},{90,94}},
        lineColor={0,0,0},
        lineThickness=0.5,
        fillColor={0,0,0},
        fillPattern=FillPattern.Solid)}));
end SorpLibPackage;
