within SorpLib.Icons;
partial package TubesPackage "Icon for packages containing tube models"
  extends Modelica.Icons.Package;

  annotation (Documentation(info="<html>
<p>
This icon indicates a package containing heat exchanger models.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"), Icon(graphics={
        Rectangle(
          extent={{-100,-24},{100,-28}},
          fillColor={95,95,95},
          fillPattern=FillPattern.Forward),
        Rectangle(
          extent={{-100,32},{100,28}},
          fillColor={95,95,95},
          fillPattern=FillPattern.Forward),
        Rectangle(
          extent={{-100,28},{100,-24}},
          fillPattern=FillPattern.HorizontalCylinder,
          fillColor={0,128,255}),
        Line(
          points={{48,-58},{-52,-58}},
          color={0,0,0}),
        Polygon(
          points={{20,-44},{60,-59},{20,-74},{20,-44}},
          lineColor={0,0,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{-20,15},{20,0},{-20,-15},{-20,15}},
          lineColor={0,0,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid,
        origin={-40,-59},
        rotation=180)}));
end TubesPackage;
