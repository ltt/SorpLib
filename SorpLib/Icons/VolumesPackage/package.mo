within SorpLib.Icons;
partial package VolumesPackage "Icon for packages containing finite volume models"
  extends Modelica.Icons.Package;

  annotation (Documentation(info="<html>
<p>
This icon indicates a package containing volume models.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"), Icon(graphics={
        Rectangle(extent={{-90,30},{30,-90}}, lineColor={0,0,0}),
        Line(points={{-90,30},{-30,90}}, color={0,0,0}),
        Line(
          points={{-30,-30},{-90,-90}},
          color={0,0,0},
          pattern=LinePattern.Dash),
        Line(points={{90,-30},{30,-90}}, color={0,0,0}),
        Line(points={{90,90},{30,30}}, color={0,0,0}),
        Line(points={{-30,90},{90,90},{90,-30}}, color={0,0,0}),
        Line(
          points={{90,-30},{-30,-30}},
          color={0,0,0},
          pattern=LinePattern.Dash),
        Line(
          points={{-30,90},{-30,-30}},
          color={0,0,0},
          pattern=LinePattern.Dash),
        Line(
          points={{-60,-60},{60,-60},{60,60},{-60,60},{-60,-60}},
          color={0,0,0},
          pattern=LinePattern.Dot),
        Line(
          points={{30,-30},{-30,-90},{-30,30},{30,90},{30,30}},
          color={0,0,0},
          pattern=LinePattern.Dot),
        Line(
          points={{-30,30},{-90,-30},{-30,-30}},
          color={0,0,0},
          pattern=LinePattern.Dot),
        Line(
          points={{30,-30},{90,30},{30,30}},
          color={0,0,0},
          pattern=LinePattern.Dot)}));
end VolumesPackage;
