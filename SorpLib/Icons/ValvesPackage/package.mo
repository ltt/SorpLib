within SorpLib.Icons;
partial package ValvesPackage "Icon for packages containing valve models"
  extends Modelica.Icons.Package;

  annotation (Documentation(info="<html>
<p>
This icon indicates a package containing valve models.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"), Icon(graphics={
        Rectangle(
          extent={{-20,60},{20,52}},
          fillPattern=FillPattern.Solid),
        Line(points={{0,52},{0,0}}),
        Polygon(points={{-80,50},{80,-50},{80,50},{0,0},{-80,-50},{-80,50}})}));
end ValvesPackage;
