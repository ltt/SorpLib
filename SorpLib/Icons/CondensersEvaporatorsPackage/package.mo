within SorpLib.Icons;
partial package CondensersEvaporatorsPackage "Icon for packages containing condensers and evaporators"
  extends Modelica.Icons.Package;

  annotation (Documentation(info="<html>
<p>
This icon indicates a package containing recooler models.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"), Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,-40},
            {100,40}}),                                         graphics={
        Text(
          extent={{-60,30},{-40,10}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={255,255,255},
          fillPattern=FillPattern.None,
          fontSize=20,
          textString="1"),
        Text(
          extent={{40,30},{60,10}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={255,255,255},
          fillPattern=FillPattern.None,
          fontSize=20,
          textString="n"),
        Line(
          points={{0,40},{0,22},{-2,14},{2,0},{-2,-14},{2,-24},{0,-40}},
          color={0,0,0},
          smooth=Smooth.Bezier),
        Rectangle(
          extent={{-100,-2},{100,-40}},
          lineColor={28,108,200},
          lineThickness=0.5,
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{-100,-10},{-80,8},{-54,-2},{-36,0},{-6,6},{14,-2},{42,4},{74,
              4},{94,-6},{-100,-10}},
          lineColor={28,108,200},
          lineThickness=0.5,
          smooth=Smooth.Bezier,
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-100,40},{100,-40}},
          lineColor={0,0,0},
          lineThickness=1),
        Polygon(
          points={{-20,4},{-20,4},{-32,18},{-8,18},{-20,4}},
          lineColor={0,0,0},
          lineThickness=0.5),
        Ellipse(
          extent={{-66,38},{-78,26}},
          lineColor={28,108,200},
          lineThickness=0.5,
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-28,38},{-40,26}},
          lineColor={28,108,200},
          lineThickness=0.5,
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-12,38},{-24,26}},
          lineColor={28,108,200},
          lineThickness=0.5,
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{16,22},{4,10}},
          lineColor={28,108,200},
          lineThickness=0.5,
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{28,38},{16,26}},
          lineColor={28,108,200},
          lineThickness=0.5,
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{36,22},{24,10}},
          lineColor={28,108,200},
          lineThickness=0.5,
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-58,24},{-70,12}},
          lineColor={28,108,200},
          lineThickness=0.5,
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{74,38},{62,26}},
          lineColor={28,108,200},
          lineThickness=0.5,
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{48,38},{36,26}},
          lineColor={28,108,200},
          lineThickness=0.5,
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{86,24},{74,12}},
          lineColor={28,108,200},
          lineThickness=0.5,
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-84,34},{-96,22}},
          lineColor={28,108,200},
          lineThickness=0.5,
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid)}));
end CondensersEvaporatorsPackage;
