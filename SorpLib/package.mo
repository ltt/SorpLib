within ;
package SorpLib "SorpLib 2.0 - Adsorption Systems Library"
  extends SorpLib.Icons.SorpLibPackage;

  annotation (uses(Modelica(version="4.0.0")), Documentation(revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end SorpLib;
