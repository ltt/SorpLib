within SorpLib.Choices;
type SorptionEnthalpy = enumeration(
    Constant "Constant sorption enthalpy",
    Formal "Definition according to thermodynamic derivation",
    ClausiusClapeyron "Definition according to Clausius Clapeyron assumptions",
    Dubinin "Definition according to the model of Dubinin")
  "Enumeration defining the calculation approach for the sorption enthalpy";
