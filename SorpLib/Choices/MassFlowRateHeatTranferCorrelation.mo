within SorpLib.Choices;
type MassFlowRateHeatTranferCorrelation = enumeration(
    PortXMinus
      "Mass flow rate at design inlet",
    PortXPlus
      "Mass flow rate at design outlet",
    Average
      "Average absolute mass flow rate")
  "Enumeration defining the mass flow rate used for calculating generic heat transfer 
  correlations";
