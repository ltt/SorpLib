within SorpLib.Choices;
type BalanceEquations = enumeration(
    TransientFreeInitial
      "Transient balance equation - free initial state values",
    TransientFixedInitial
      "Transient balance equation - fixed initial state values",
    TransientSteadyStateInitial
      "Transient balance equation - steady-state initial state values",
    SteadyStateFreeInitial
       "Steady-state balance equation - free initial state values")
  "Enumeration defining balance equations";
