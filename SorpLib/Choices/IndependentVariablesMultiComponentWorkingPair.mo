within SorpLib.Choices;
type IndependentVariablesMultiComponentWorkingPair = enumeration(
    pyT "Pressure, independent mole fractions of gas/vapor phase, and temperature",
    xT "Uptakes and temperature")
  "Enumeration defining the independent variables of a multi-component working pair";
