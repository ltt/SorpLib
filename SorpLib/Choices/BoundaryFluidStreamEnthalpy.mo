within SorpLib.Choices;
type BoundaryFluidStreamEnthalpy = enumeration(
    SpecificEnthalpy "Prescribed specific enthalpy at fluid boundary",
    Temperature "Prescribed temperature at fluid boundary")
  "Enumeration defining prescribed stream variable (specific enthalpy) of a fluid boundary";
