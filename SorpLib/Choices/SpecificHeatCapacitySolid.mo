within SorpLib.Choices;
type SpecificHeatCapacitySolid = enumeration(
    Constant "Constant value",
    GeneralizedFunction "Generalized function often used for fluid property data calculation",
    Interpolation "Table-based interpolation as function of temperature")
  "Enumeration defining the calculation approach for the specific heat capacity of a solid";
