within SorpLib.Choices;
type TemperatureHeatTranferCorrelation = enumeration(
    PortA
      "Average temperature at port a",
    PortB
      "Average temperature at port b",
    Average
      "Average temperature of ports a and b",
    Difference
      "Average temperature difference between ports a and b")
  "Enumeration defining the temperature used for calculating generic heat transfer 
  correlations";
