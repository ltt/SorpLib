within SorpLib.Choices;
type ThermalConductivitySolid = enumeration(
    Constant "Constant value",
    GeneralizedFunction "Generalized function often used for fluid property data calculation",
    Interpolation "Table-based interpolation as function of temperature")
  "Enumeration defining the calculation approach for the thermal conductivity of a solid";
