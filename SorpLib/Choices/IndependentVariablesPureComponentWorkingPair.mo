within SorpLib.Choices;
type IndependentVariablesPureComponentWorkingPair = enumeration(
    pT "Pressure and temperature",
    xT "Uptake and temperature")
  "Enumeration defining the independent variables of a pure component working pair";
