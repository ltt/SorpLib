within SorpLib.Choices;
type InterpolationApproach = enumeration(
    Linear "Linear interpolation without extrapolation",
    CubicSplines "Cubic spline interpolation without extrapolation")
  "Enumeration defining the interpolation approach for calculations";
