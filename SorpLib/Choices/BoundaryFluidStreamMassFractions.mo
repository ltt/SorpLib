within SorpLib.Choices;
type BoundaryFluidStreamMassFractions = enumeration(
    MassFractions "Prescribed mass fractions",
    DryMassFractionsPhi "Prescribed mass fractions of dry air components and relative humidity")
  "Enumeration defining prescribed stream variable (mass fractions) of a fluid boundary";
