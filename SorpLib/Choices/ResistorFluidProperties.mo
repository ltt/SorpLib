within SorpLib.Choices;
type ResistorFluidProperties = enumeration(
    Constant
      "Constant fluid properties",
    PortAInlet
      "Instreaming fluid properties ar port a",
    PortBInlet
      "Instreaming fluid properties ar port b",
    ActualInlet
      "Fluid properties at actual inlet",
    AverageInstreaming
      "Average fluid properties using instreaming fluid properties",
    Detailed
      "Correct fluid properties, also depending on the flow direction")
  "Enumeration defining fluid property calculation for hydraulic resistors";
