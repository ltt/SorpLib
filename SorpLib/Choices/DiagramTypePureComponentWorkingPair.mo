within SorpLib.Choices;
type DiagramTypePureComponentWorkingPair = enumeration(
    Isotherms "Isotherms: T = const, p = variable",
    Isosters "Isosters: x = const, T = variable",
    Isobars "Isobars: p = const, T = variable")
  "Enumeration defining the diagram type (i.e., isotherms, isosters, or isobars) of a pure component working pair";
