within SorpLib.Choices;
type SpecificHeatCapacityAdsorpt = enumeration(
    Constant "Constant value",
    BoilingCurve "Boiling point curve as function of temperature",
    GeneralizedFunction "Generalized function often used for fluid property data calculation",
    Interpolation "Table-based interpolation as function of temperature",
    WaltonLeVan "Derivation according to Walton and LeVan (2005)",
    ChakrabortyElAl "Derivation according to Chakraborty et al. (2009)",
    SchwambergerSchmidt "Derivation according to Schwamberger and Schmidt (2013)")
  "Enumeration defining the calculation approach for the specific heat capacity of the adsorpt phase";
