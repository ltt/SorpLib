within SorpLib;
package Choices "Library containing type definitions representing choices"
extends Modelica.Icons.TypesPackage;

annotation (Documentation(info="<html>
<p>
This package contains definitions of types representing enumeration used within the Modelica library SorpLib.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end Choices;
