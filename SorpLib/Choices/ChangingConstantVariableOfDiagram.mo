within SorpLib.Choices;
type ChangingConstantVariableOfDiagram = enumeration(
    Manual "Manual input",
    Linespace "Linearly distributed values",
    Logspace "Logarithmicaly distributed values")
  "Enumeration defining the changing type (i.e., manuel, linespace, logspace) of the constant variable of a fluid property diagram";
