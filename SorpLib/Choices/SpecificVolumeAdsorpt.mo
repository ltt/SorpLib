within SorpLib.Choices;
type SpecificVolumeAdsorpt = enumeration(
    Constant "Constant value",
    BoilingCurve "Boiling point curve as function of temperature",
    GeneralizedFunction "Generalized function often used for fluid property data calculation",
    Interpolation "Table-based interpolation as function of temperature")
  "Enumeration defining the calculation approach for the specific volume of the adsorpt phase";
