within SorpLib.Choices;
type IASTAlgorithm = enumeration(
    NewtonRaphson "Standard 'Newton-Raphson' algorithm",
    NestedLoop "'Nested loop' algorithm",
    FastIAST "'Fast IAST' algorithm")
  "Enumeration defining the algorithm solving the IAST (i.e., x_i = f(p,y,T))";
