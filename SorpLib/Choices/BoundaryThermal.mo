within SorpLib.Choices;
type BoundaryThermal = enumeration(
    Temperature "Prescribed temperature at thermal boundary",
    HeatFlowRate "Prescribed heat flow rate at thermal boundary")
  "Enumeration defining prescribed variable of a thermal boundary";
