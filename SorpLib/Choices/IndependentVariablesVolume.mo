within SorpLib.Choices;
type IndependentVariablesVolume = enumeration(
    pTX "Pressure, temperature, and independent mass fractions",
    phX "Pressure, specific enthalpy, and independent mass fractions")
  "Enumeration defining the independent variables of a finite volume";
