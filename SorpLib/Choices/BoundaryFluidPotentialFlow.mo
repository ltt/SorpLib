within SorpLib.Choices;
type BoundaryFluidPotentialFlow = enumeration(
    Pressure "Prescribed pressure at fluid boundary",
    MassFlowRate "Prescribed mass flow rate at fluid boundary",
    VolumeFlowRate "Prescribed volume flow rate at fluid boundary")
  "Enumeration defining prescribed potential or flow variable of a fluid boundary";
