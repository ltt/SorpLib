within SorpLib.Choices;
type IsobaricExpansionCoefficientAdsorpt = enumeration(
    Constant "Constant value",
    BoilingCurve "Boiling point curve as function of temperature",
    GeneralizedFunction "Generalized function often used for fluid property data calculation",
    Interpolation "Table-based interpolation as function of temperature")
  "Enumeration defining the calculation approach for the isobaric expansion coefficient of the adsorpt phase";
