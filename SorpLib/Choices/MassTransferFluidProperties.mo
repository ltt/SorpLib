within SorpLib.Choices;
type MassTransferFluidProperties = enumeration(
    PortAInlet
      "Instreaming fluid properties ar port a",
    PortBInlet
      "Instreaming fluid properties ar port b",
    ActualInlet
      "Fluid properties at actual inlet",
    AverageInstreaming
      "Average fluid properties using instreaming fluid properties")
  "Enumeration defining fluid property calculation for mass transfers";
