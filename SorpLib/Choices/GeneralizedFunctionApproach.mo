within SorpLib.Choices;
type GeneralizedFunctionApproach = enumeration(
    PolynomialFunctionTemperature "Polynomial function using temperature",
    PolynomialFunctionReducedTemperature "Polynomial function using reduced temperature",
    ExponentialFunctionTemperature "Exponential function using temperature",
    ExponentialFunctionReducedTemperature "Exponential function using reduced temperature")
  "Enumeration defining the approach used within the generalized function for calculating fluid property data";
