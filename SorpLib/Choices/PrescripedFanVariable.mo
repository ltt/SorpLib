within SorpLib.Choices;
type PrescripedFanVariable = enumeration(
    m_flow
      "Mass flow rate",
    V_flow
      "Volume flow rate")
  "Enumeration defining the variable that is prescribed by a fan";
