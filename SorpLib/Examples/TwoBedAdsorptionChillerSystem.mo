within SorpLib.Examples;
model TwoBedAdsorptionChillerSystem
  "Example of a two-bed adsorption chiller with recooler and recooler controler"
  extends Modelica.Icons.Example;

  //
  // Definition of main components
  //
  Components.HeatExchanger.Adsorbers.SimpleAdsorberDX adsorber1(no_fluidVolumes=
       5, no_sorbentVolumes=1,
    redeclare model WorkingPair =
        SorpLib.Media.WorkingPairs.PureComponents.H2O.Silicagel123_DubininLorentzianCumulative_Schawe2000_VLE
        (
        stateVariables=SorpLib.Choices.IndependentVariablesPureComponentWorkingPair.xT,
        approachSorptionEnthalpy=SorpLib.Choices.SorptionEnthalpy.Dubinin,
        approachSpecificHeatCapacity=SorpLib.Choices.SpecificHeatCapacityAdsorpt.BoilingCurve,
        approachSpecificVolume=SorpLib.Choices.SpecificVolumeAdsorpt.BoilingCurve,
        limitLowerPressureAdsorptive=true)) "First adsorber bed" annotation (Placement(
        transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={30,0})));

  Components.HeatExchanger.Adsorbers.SimpleAdsorberDX adsorber2(
    no_fluidVolumes=5,
    no_sorbentVolumes=1,
    redeclare model WorkingPair =
        SorpLib.Media.WorkingPairs.PureComponents.H2O.Silicagel123_DubininLorentzianCumulative_Schawe2000_VLE
        (
        stateVariables=SorpLib.Choices.IndependentVariablesPureComponentWorkingPair.xT,
        approachSorptionEnthalpy=SorpLib.Choices.SorptionEnthalpy.Dubinin,
        approachSpecificHeatCapacity=SorpLib.Choices.SpecificHeatCapacityAdsorpt.BoilingCurve,
        approachSpecificVolume=SorpLib.Choices.SpecificVolumeAdsorpt.BoilingCurve,
        limitLowerPressureAdsorptive=true)) "Second adsorber bed" annotation (
      Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=90,
        origin={-30,0})));
  Components.HeatExchanger.CondensersEvaporators.SimpleEvaporator evaporator(
    no_fluidVolumes=5,
    nPortsVapor=2,
    nPortsLiquid=1) "Evaporator model"
    annotation (Placement(transformation(extent={{-10,-80},{10,-60}})));

  Components.HeatExchanger.CondensersEvaporators.SimpleCondenser condenser(
    no_fluidVolumes=5,
    nPortsVapor=2,
    nPortsLiquid=1) "Condenser"
    annotation (Placement(transformation(extent={{-10,50},{10,30}})));

  Components.Valves.VLEValves.CondensateRefluxValve condensateRefluxValve
    annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=270,
        origin={-70,0})));

  Components.HeatExchanger.Recoolers.SimpleDryCooler recooler(
    T_air_in=293.15,
    flowDirection=1,
      scaleFactor=10/466) "Dry recooler"
    annotation (Placement(transformation(extent={{10,80},{-10,60}})));

  //
  // Definition of pumps
  //
  Components.Pumps.AffinityPump condenserPump(redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater)
    "Pump of the condenser and recooler"
    annotation (Placement(transformation(extent={{-40,30},{-20,50}})));

  //
  // Definition of controller
  //
  Modelica.Blocks.Continuous.LimPID recoolerControler(
    controllerType=Modelica.Blocks.Types.SimpleController.PI,
    yMax=1,
    yMin=0.1,
    initType=Modelica.Blocks.Types.Init.InitialOutput,
    y_start=0.5,
    homotopyType=Modelica.Blocks.Types.LimiterHomotopy.LowerLimit)
    "Controller of the fan speed"
    annotation (Placement(transformation(extent={{70,80},{50,60}})));

  //
  // Definition of sensors
  //
  Components.Sensors.LiquidSensors.SpecificEnthalpySensor
    sensor_hRecoolerOutlet(redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater)
    "Specific enthalpy at recooler outlet"
    annotation (Placement(transformation(extent={{-50,68},{-30,88}})));

  Components.Sensors.LiquidSensors.TemperatureSensor sensor_TRecoolerOutlet(
      redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater)
    "Outlet temperature of the recooler"
    annotation (Placement(transformation(extent={{-30,68},{-10,88}})));

  //
  // Definition of boundaries
  //
  Components.Pumps.AffinityPump evaporatorPump(redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater)
    "Pump of the evaporator"
    annotation (Placement(transformation(extent={{-40,-80},{-20,-60}})));
  Components.Pumps.AffinityPump adsorperPump1(redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater)
    "Pump of the adsorber 1" annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-30,-30})));
  Components.Pumps.AffinityPump adsorperPump2(redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater)
    "Pump of the adsorber 2" annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={30,-30})));
protected
  Basics.Sources.Fluids.LiquidSource fs_evapA(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    V_flow_fixed=-10/60/1000,
    T_fixed=288.15,
    redeclare package Medium = Modelica.Media.Water.ConstantPropertyLiquidWater)
    "Evaporator inlet"
    annotation (Placement(transformation(extent={{-60,-80},{-40,-60}})));

  Basics.Sources.Fluids.LiquidSource fs_evapB(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    V_flow_fixed=-10/60/1000,
    T_fixed=288.15,
    redeclare package Medium = Modelica.Media.Water.ConstantPropertyLiquidWater)
    "Evaporator outlet"
    annotation (Placement(transformation(extent={{10,-80},{30,-60}})));

  Basics.Sources.Fluids.LiquidSource fs_condA(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.SpecificEnthalpy,
    use_hInput=true,
    redeclare package Medium = Modelica.Media.Water.ConstantPropertyLiquidWater)
    "Condenser inlet"
    annotation (Placement(transformation(extent={{-60,30},{-40,50}})));

  Basics.Sources.Fluids.LiquidSource fs_condB(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    V_flow_fixed=-10/60/1000,
    T_fixed=288.15,
    redeclare package Medium = Modelica.Media.Water.ConstantPropertyLiquidWater)
    "Condenser outlet"
    annotation (Placement(transformation(extent={{-60,60},{-40,80}})));

  Basics.Sources.Fluids.LiquidSource fs_adsA1(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    p_fixed=1000000,
    V_flow_fixed=-10/60/1000,
    use_TInput=true,
    T_fixed=298.15,
    redeclare package Medium = Modelica.Media.Water.ConstantPropertyLiquidWater)
    "Adsorber inlet"
    annotation (Placement(transformation(extent={{-50,-60},{-30,-40}})));

  Basics.Sources.Fluids.LiquidSource fs_adsA2(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    p_fixed=1000000,
    V_flow_fixed=-10/60/1000,
    use_TInput=true,
    T_fixed=298.15,
    redeclare package Medium = Modelica.Media.Water.ConstantPropertyLiquidWater)
    "Adsorber inlet"
    annotation (Placement(transformation(extent={{50,-62},{30,-42}})));

  Basics.Sources.Fluids.LiquidSource fs_adsB1(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    p_fixed=1000000,
    V_flow_fixed=-10/60/1000,
    use_TInput=false,
    T_fixed=298.15,
    redeclare package Medium = Modelica.Media.Water.ConstantPropertyLiquidWater)
    "Adsorber outlet"
    annotation (Placement(transformation(extent={{-50,2},{-30,22}})));

  Basics.Sources.Fluids.LiquidSource fs_adsB2(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    p_fixed=1000000,
    V_flow_fixed=-10/60/1000,
    use_TInput=false,
    T_fixed=298.15,
    redeclare package Medium = Modelica.Media.Water.ConstantPropertyLiquidWater)
    "Adsorber outlet"
    annotation (Placement(transformation(extent={{30,2},{50,22}})));

  //
  // Definitio of input signals
  //
  Modelica.Blocks.Sources.RealExpression fillingLevelInput(y=condenser.phaseSeparatorVolume.l_liq_rel)
    "Relativ filling level of the condenser"
    annotation (Placement(transformation(extent={{-100,-10},{-80,10}})));

  Modelica.Blocks.Sources.RealExpression setPoint_TCondenserInlet(y=-(25 + 273.15))
    "Set point of the condenser inlet temperature"
    annotation (Placement(transformation(extent={{100,60},{80,80}})));

  Modelica.Blocks.Sources.Trapezoid inlet_TAds1(
    amplitude=60,
    rising=25,
    width=490,
    falling=25,
    period=1000,
    offset=273.15 + 25) "Adsorber inlet temperature"
    annotation (Placement(transformation(extent={{-60,-44},{-48,-32}})));

  Modelica.Blocks.Sources.Trapezoid inlet_TAds2(
    amplitude=-60,
    rising=25,
    width=490,
    falling=25,
    period=1000,
    offset=273.15 + 85) "Adsorber inlet temperature"
    annotation (Placement(transformation(extent={{60,-46},{48,-34}})));

  Modelica.Blocks.Math.Gain gainTRecoolerOutlet(k=-1) "For recooler controller"
    annotation (Placement(transformation(extent={{40,82},{50,92}})));

equation
  //
  // Definition of connections
  //
  connect(evaporator.vaporPort[1], adsorber1.evaporatorPort) annotation (Line(
      points={{0,-66.3},{0,-20},{26,-20},{26,-10}},
      color={0,140,72},
      thickness=1));
  connect(adsorber2.evaporatorPort, evaporator.vaporPort[2]) annotation (Line(
      points={{-26,-10},{-26,-20},{0,-20},{0,-65.3}},
      color={0,140,72},
      thickness=1));
  connect(condenser.vaporPort[1], adsorber2.condenserPort) annotation (Line(
      points={{0,36.3},{0,20},{-26,20},{-26,10}},
      color={0,140,72},
      thickness=1));
  connect(condenser.vaporPort[2], adsorber1.condenserPort) annotation (Line(
      points={{0,35.3},{0,20},{26,20},{26,10}},
      color={0,140,72},
      thickness=1));
  connect(condensateRefluxValve.port_b, evaporator.liquidPort[1]) annotation (
      Line(
      points={{-70,-8},{-70,-80},{0,-80},{0,-73.8}},
      color={0,140,72},
      thickness=1));
  connect(condenser.liquidPort[1], condensateRefluxValve.port_a) annotation (
      Line(
      points={{0,43.8},{0,50},{-70,50},{-70,8}},
      color={0,140,72},
      thickness=1));
  connect(fillingLevelInput.y, condensateRefluxValve.processVariable)
    annotation (Line(points={{-79,0},{-78,0},{-78,2.6},{-74.2,2.6}}, color={0,0,
          127}));
  connect(fs_evapB.port, evaporator.port_b) annotation (Line(
      points={{20,-70},{10,-70}},
      color={28,108,200},
      thickness=1));
  connect(fs_adsB1.port, adsorber2.port_b) annotation (Line(
      points={{-40,12},{-30,12},{-30,10}},
      color={28,108,200},
      thickness=1));
  connect(fs_adsB2.port, adsorber1.port_b) annotation (Line(
      points={{40,12},{30,12},{30,10}},
      color={28,108,200},
      thickness=1));
  connect(inlet_TAds1.y, fs_adsA1.T_input)
    annotation (Line(points={{-47.4,-38},{-44,-38},{-44,-52},{-41.2,-52}},
                                                       color={0,0,127}));
  connect(inlet_TAds2.y, fs_adsA2.T_input)
    annotation (Line(points={{47.4,-40},{44,-40},{44,-54},{41.2,-54}},
                                                     color={0,0,127}));
  connect(fs_condA.port, condenserPump.port_a) annotation (Line(
      points={{-50,40},{-38,40}},
      color={28,108,200},
      thickness=1));
  connect(condenserPump.port_b, condenser.port_a) annotation (Line(
      points={{-22,40},{-10,40}},
      color={28,108,200},
      thickness=1));
  connect(condenser.port_b, recooler.port_a) annotation (Line(
      points={{10,40},{20,40},{20,70},{10,70}},
      color={28,108,200},
      thickness=1));
  connect(recooler.port_b, sensor_hRecoolerOutlet.port) annotation (Line(
      points={{-10,70},{-40,70}},
      color={28,108,200},
      thickness=1));
  connect(sensor_hRecoolerOutlet.port, fs_condB.port) annotation (Line(
      points={{-40,70},{-50,70}},
      color={28,108,200},
      thickness=1));
  connect(sensor_hRecoolerOutlet.value, fs_condA.h_input) annotation (Line(
        points={{-40,84.8},{-40,88},{-60,88},{-60,38},{-51.2,38}}, color={0,0,127}));
  connect(sensor_TRecoolerOutlet.port, recooler.port_b) annotation (Line(
      points={{-20,70},{-10,70}},
      color={28,108,200},
      thickness=1));
  connect(setPoint_TCondenserInlet.y, recoolerControler.u_s)
    annotation (Line(points={{79,70},{72,70}}, color={0,0,127}));
  connect(sensor_TRecoolerOutlet.value, gainTRecoolerOutlet.u)
    annotation (Line(points={{-20,84.8},{-20,87},{39,87}}, color={0,0,127}));
  connect(gainTRecoolerOutlet.y, recoolerControler.u_m)
    annotation (Line(points={{50.5,87},{60,87},{60,82}}, color={0,0,127}));
  connect(recoolerControler.y, recooler.relativeFanSpeed) annotation (Line(
        points={{49,70},{40,70},{40,80},{0,80},{0,76}}, color={0,0,127}));

  //
  // Definition of annotations
  //
  connect(fs_evapA.port, evaporatorPump.port_a) annotation (Line(
      points={{-50,-70},{-38,-70}},
      color={28,108,200},
      thickness=1));
  connect(evaporatorPump.port_b, evaporator.port_a) annotation (Line(
      points={{-22,-70},{-10,-70}},
      color={28,108,200},
      thickness=1));
  connect(fs_adsA1.port, adsorperPump1.port_a) annotation (Line(
      points={{-40,-50},{-30,-50},{-30,-38}},
      color={28,108,200},
      thickness=1));
  connect(adsorperPump1.port_b, adsorber2.port_a) annotation (Line(
      points={{-30,-22},{-30,-10}},
      color={28,108,200},
      thickness=1));
  connect(fs_adsA2.port, adsorperPump2.port_a) annotation (Line(
      points={{40,-52},{30,-52},{30,-38}},
      color={28,108,200},
      thickness=1));
  connect(adsorperPump2.port_b, adsorber1.port_a) annotation (Line(
      points={{30,-22},{30,-10}},
      color={28,108,200},
      thickness=1));
  annotation (experiment(
      StopTime=5000));
end TwoBedAdsorptionChillerSystem;
