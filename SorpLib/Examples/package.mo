within SorpLib;
package Examples "Library containing example models"
extends Modelica.Icons.ExamplesPackage;

annotation (Documentation(revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>", info="<html>
<p>
This packge contains example models of sorptions systems.
</p>
</html>"));
end Examples;
