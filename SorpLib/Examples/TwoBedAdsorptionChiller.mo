within SorpLib.Examples;
model TwoBedAdsorptionChiller
  "Example of a two-bed adsorption chiller"
  extends Modelica.Icons.Example;

  //
  // Definition of main components
  //
  Components.HeatExchanger.Adsorbers.SimpleAdsorberDX adsorber1(no_fluidVolumes=
       5, no_sorbentVolumes=1,
    redeclare model WorkingPair =
        SorpLib.Media.WorkingPairs.PureComponents.H2O.Silicagel123_DubininLorentzianCumulative_Schawe2000_VLE
        (
        stateVariables=SorpLib.Choices.IndependentVariablesPureComponentWorkingPair.xT,
        approachSorptionEnthalpy=SorpLib.Choices.SorptionEnthalpy.Dubinin,
        approachSpecificHeatCapacity=SorpLib.Choices.SpecificHeatCapacityAdsorpt.BoilingCurve,
        approachSpecificVolume=SorpLib.Choices.SpecificVolumeAdsorpt.BoilingCurve,
        limitLowerPressureAdsorptive=true)) "First adsorber bed" annotation (Placement(
        transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={30,0})));

  Components.HeatExchanger.Adsorbers.SimpleAdsorberDX adsorber2(
    no_fluidVolumes=5,
    no_sorbentVolumes=1,
    redeclare model WorkingPair =
        SorpLib.Media.WorkingPairs.PureComponents.H2O.Silicagel123_DubininLorentzianCumulative_Schawe2000_VLE
        (
        stateVariables=SorpLib.Choices.IndependentVariablesPureComponentWorkingPair.xT,
        approachSorptionEnthalpy=SorpLib.Choices.SorptionEnthalpy.Dubinin,
        approachSpecificHeatCapacity=SorpLib.Choices.SpecificHeatCapacityAdsorpt.BoilingCurve,
        approachSpecificVolume=SorpLib.Choices.SpecificVolumeAdsorpt.BoilingCurve,
        limitLowerPressureAdsorptive=true)) "Second adsorber bed" annotation (
      Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=90,
        origin={-30,0})));

  Components.HeatExchanger.CondensersEvaporators.SimpleEvaporator evaporator(
    no_fluidVolumes=5,
    nPortsVapor=2,
    nPortsLiquid=1) "Evaporator model"
    annotation (Placement(transformation(extent={{-10,-60},{10,-40}})));

  Components.HeatExchanger.CondensersEvaporators.SimpleCondenser condenser(
    no_fluidVolumes=5,
    nPortsVapor=2,
    nPortsLiquid=1) "Condenser"
    annotation (Placement(transformation(extent={{-10,50},{10,30}})));

  Components.Valves.VLEValves.CondensateRefluxValve condensateRefluxValve
    annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=270,
        origin={-70,0})));

  //
  // Definition of source
  //
protected
  Basics.Sources.Fluids.LiquidSource fs_evapA(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.VolumeFlowRate,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    V_flow_fixed=-10/60/1000,
    T_fixed=288.15,
    redeclare package Medium = Modelica.Media.Water.ConstantPropertyLiquidWater)
    "Evaporator inlet"
    annotation (Placement(transformation(extent={{-30,-60},{-10,-40}})));

  Basics.Sources.Fluids.LiquidSource fs_evapB(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    V_flow_fixed=-10/60/1000,
    T_fixed=288.15,
    redeclare package Medium = Modelica.Media.Water.ConstantPropertyLiquidWater)
    "Evaporator outlet"
    annotation (Placement(transformation(extent={{10,-60},{30,-40}})));

  Basics.Sources.Fluids.LiquidSource fs_condA(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.VolumeFlowRate,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    V_flow_fixed=-10/60/1000,
    T_fixed=298.15,
    redeclare package Medium = Modelica.Media.Water.ConstantPropertyLiquidWater)
    "Condenser inlet"
    annotation (Placement(transformation(extent={{-30,30},{-10,50}})));

  Basics.Sources.Fluids.LiquidSource fs_condB(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    V_flow_fixed=-10/60/1000,
    T_fixed=288.15,
    redeclare package Medium = Modelica.Media.Water.ConstantPropertyLiquidWater)
    "Condenser outlet"
    annotation (Placement(transformation(extent={{10,30},{30,50}})));

  Basics.Sources.Fluids.LiquidSource fs_adsA1(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.VolumeFlowRate,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    V_flow_fixed=-10/60/1000,
    use_TInput=true,
    T_fixed=298.15,
    redeclare package Medium = Modelica.Media.Water.ConstantPropertyLiquidWater)
    "Adsorber inlet"
    annotation (Placement(transformation(extent={{-50,-22},{-30,-2}})));

  Basics.Sources.Fluids.LiquidSource fs_adsA2(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.VolumeFlowRate,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    V_flow_fixed=-10/60/1000,
    use_TInput=true,
    T_fixed=298.15,
    redeclare package Medium = Modelica.Media.Water.ConstantPropertyLiquidWater)
    "Adsorber inlet"
    annotation (Placement(transformation(extent={{50,-22},{30,-2}})));

  Basics.Sources.Fluids.LiquidSource fs_adsB1(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    p_fixed=1000000,
    V_flow_fixed=-10/60/1000,
    use_TInput=false,
    T_fixed=298.15,
    redeclare package Medium = Modelica.Media.Water.ConstantPropertyLiquidWater)
    "Adsorber outlet"
    annotation (Placement(transformation(extent={{-50,2},{-30,22}})));

  Basics.Sources.Fluids.LiquidSource fs_adsB2(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    p_fixed=1000000,
    V_flow_fixed=-10/60/1000,
    use_TInput=false,
    T_fixed=298.15,
    redeclare package Medium = Modelica.Media.Water.ConstantPropertyLiquidWater)
    "Adsorber outlet"
    annotation (Placement(transformation(extent={{30,2},{50,22}})));

  //
  // Definition if input signals
  //
  Modelica.Blocks.Sources.RealExpression fillingLevelInput(y=condenser.phaseSeparatorVolume.l_liq_rel)
    "Relativ filling level of the condenser"
    annotation (Placement(transformation(extent={{-100,-10},{-80,10}})));

  Modelica.Blocks.Sources.Trapezoid inlet_TAds1(
    amplitude=60,
    rising=25,
    width=490,
    falling=25,
    period=1000,
    offset=273.15 + 25) "Adsorber inlet temperature"
    annotation (Placement(transformation(extent={{-60,-20},{-48,-8}})));
  Modelica.Blocks.Sources.Trapezoid inlet_TAds2(
    amplitude=-60,
    rising=25,
    width=490,
    falling=25,
    period=1000,
    offset=273.15 + 85) "Adsorber inlet temperature"
    annotation (Placement(transformation(extent={{60,-20},{48,-8}})));

equation
  //
  // Connections
  //
  connect(evaporator.vaporPort[1], adsorber1.evaporatorPort) annotation (Line(
      points={{0,-46.3},{0,-20},{26,-20},{26,-10}},
      color={0,140,72},
      thickness=1));
  connect(adsorber2.evaporatorPort, evaporator.vaporPort[2]) annotation (Line(
      points={{-26,-10},{-26,-20},{0,-20},{0,-45.3}},
      color={0,140,72},
      thickness=1));
  connect(condenser.vaporPort[1], adsorber2.condenserPort) annotation (Line(
      points={{0,36.3},{0,20},{-26,20},{-26,10}},
      color={0,140,72},
      thickness=1));
  connect(condenser.vaporPort[2], adsorber1.condenserPort) annotation (Line(
      points={{0,35.3},{0,20},{26,20},{26,10}},
      color={0,140,72},
      thickness=1));
  connect(condensateRefluxValve.port_b, evaporator.liquidPort[1]) annotation (
      Line(
      points={{-70,-8},{-70,-60},{0,-60},{0,-53.8}},
      color={0,140,72},
      thickness=1));
  connect(condenser.liquidPort[1], condensateRefluxValve.port_a) annotation (
      Line(
      points={{0,43.8},{0,50},{-70,50},{-70,8}},
      color={0,140,72},
      thickness=1));
  connect(fillingLevelInput.y, condensateRefluxValve.processVariable)
    annotation (Line(points={{-79,0},{-78,0},{-78,2.6},{-74.2,2.6}}, color={0,0,
          127}));
  connect(fs_evapA.port, evaporator.port_a) annotation (Line(
      points={{-20,-50},{-10,-50}},
      color={28,108,200},
      thickness=1));
  connect(fs_evapB.port, evaporator.port_b) annotation (Line(
      points={{20,-50},{10,-50}},
      color={28,108,200},
      thickness=1));
  connect(fs_condA.port, condenser.port_a) annotation (Line(
      points={{-20,40},{-10,40}},
      color={28,108,200},
      thickness=1));
  connect(condenser.port_b, fs_condB.port) annotation (Line(
      points={{10,40},{20,40}},
      color={28,108,200},
      thickness=1));
  connect(fs_adsA1.port, adsorber2.port_a) annotation (Line(
      points={{-40,-12},{-30,-12},{-30,-10}},
      color={28,108,200},
      thickness=1));
  connect(fs_adsB1.port, adsorber2.port_b) annotation (Line(
      points={{-40,12},{-30,12},{-30,10}},
      color={28,108,200},
      thickness=1));
  connect(fs_adsB2.port, adsorber1.port_b) annotation (Line(
      points={{40,12},{30,12},{30,10}},
      color={28,108,200},
      thickness=1));
  connect(fs_adsA2.port, adsorber1.port_a) annotation (Line(
      points={{40,-12},{30,-12},{30,-10}},
      color={28,108,200},
      thickness=1));
  connect(inlet_TAds1.y, fs_adsA1.T_input)
    annotation (Line(points={{-47.4,-14},{-41.2,-14}}, color={0,0,127}));
  connect(inlet_TAds2.y, fs_adsA2.T_input)
    annotation (Line(points={{47.4,-14},{41.2,-14}}, color={0,0,127}));

  //
  // Annotations
  //
  annotation (experiment(
      StopTime=5000));
end TwoBedAdsorptionChiller;
