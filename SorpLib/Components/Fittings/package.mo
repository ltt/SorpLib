within SorpLib.Components;
package Fittings "Adaptors for connections of fluid components and the regulation of fluid flow"
  extends SorpLib.Icons.FittingsPackage;

  annotation (Documentation(info="<html>
<p>
This package includes various models for fittings that can handle (ideal) liquids, 
ideal gases, ideal gas mixtures, ideal gas-vapor mixtures, and real substances (i.e., 
with a two-phase region). The following fittings are implemented:
</p>
<ul>
  <li>
  <a href=\"Modelica://SorpLib.Components.Fittings.InertiaInducers\">InertiaInducers</a>: 
  Inertia inducers may be used to describe the acceleration of a fluid caused by a 
  sudden pressure change, thus also allowing to break algebraic loops.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.Fittings.MultiPorts\">MultiPorts</a>: 
  Multi port models may be used if several connections shall be made to one port. 
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.Fittings.Resistors\">Resistors</a>: 
  Hydraulic resistors can be used to calculate pressure losses as a function of mass 
  flow rate and vice versa.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.Fittings.TJunctions\">TJunctions</a>: 
  T-junction models may be used to connect three fluid flows.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end Fittings;
