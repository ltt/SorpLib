﻿within SorpLib.Components.Fittings.BaseClasses;
partial model PartialPressureLoss
  "Base model for all pressure loss models"

  //
  // Definition of parameters regarding the calculation setup
  //
  parameter SorpLib.Choices.ResistorFluidProperties positionFluidProperties=
    SorpLib.Choices.ResistorFluidProperties.Detailed
    "Position of fluid properties used for calculations"
    annotation (Dialog(tab="General", group="Calculation Setup", enable=false),
                Evaluate=true);
  parameter Boolean requireTransportPropreties = true
    "= true, if transport properties are required"
    annotation (Dialog(tab="General", group="Calculation Setup", enable=false),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);

  parameter Boolean dpFromMFlow = true
    " = true, if static pressure loss is calculated from mass flow rate; otherwise,
    mass flow rate is calculated from static pressure loss"
    annotation (Dialog(tab="General", group="Calculation Setup", enable=false),
                choices(checkBox=true),
                Evaluate=true,
                HideResult = true);

  //
  // Definition of parameters regarding the geometry
  //
  replaceable parameter SorpLib.Components.Fittings.Records.GeometryGenericReistance geometry
    constrainedby SorpLib.Components.Fittings.Records.GeometryGenericReistance
    "Resistor geometry"
    annotation (Dialog(tab="General", group="Geometry", enable=false),
                HideResult=true,
                choicesAllMatching=true);

  parameter Real psi(unit="1") = 1
    "Void fraction of the resistance (i.e., psi = 1 - V_particle / V)"
    annotation (Dialog(tab="General", group="Geometry", enable=false),
                HideResult=true);
  final parameter Modelica.Units.SI.Diameter d_hyd_mean=
    (geometry.d_hyd_a + geometry.d_hyd_b) / 2
    "Hydraulic diameter"
    annotation (Dialog(tab="General", group="Geometry", enable=false),
                HideResult=true);
  final parameter Modelica.Units.SI.Area A_a=
    Modelica.Constants.pi / 4 * geometry.d_hyd_a^2 * psi
    "Cross-sectional area at port a"
    annotation (Dialog(tab="General", group="Geometry", enable=false),
                HideResult=true);
  final parameter Modelica.Units.SI.Area A_b=
    Modelica.Constants.pi / 4 * geometry.d_hyd_b^2 * psi
    "Cross-sectional area at port b"
    annotation (Dialog(tab="General", group="Geometry", enable=false),
                HideResult=true);
  final parameter Modelica.Units.SI.Area A_mean=
    (A_a + A_b) / 2
    "Average cross-sectional area"
    annotation (Dialog(tab="General", group="Geometry", enable=false),
                HideResult=true);

  //
  // Definition of parameters regarding fluid properties
  //
  parameter SorpLib.Components.Fittings.Records.FluidProperties constantFluidProperties
    "Constant fluid properties that might be used for pressure loss calculations"
    annotation (Dialog(tab="General", group="Fluid Properties", enable=false));

  //
  // Definition of parameters regarding fittings
  //
  parameter Boolean flowDirectionDependentZeta = false
    " = true, if loss factors differ with flow direction (e.g., sudden expansion)
    and area and density at port a or b are used"
    annotation (Dialog(tab="General", group="Fittings", enable=false),
                choices(checkBox=true),
                Evaluate=true,
                HideResult = true);
  parameter Real[:] zeta_a(each unit="1") = {0}
    "Loss factors due to fittings at port a"
    annotation (Dialog(tab="General", group="Fittings", enable=false));
  parameter Real[:] zeta_b(each unit="1") = zeta_a
    "Loss factors due to fittings at port b"
    annotation (Dialog(tab="General", group="Fittings", enable=false));

  //
  // Definition of advanced parameters
  //
  parameter Boolean avoid_events = false
    "= true, if events are avoid by using noEvent()-operator"
    annotation (Dialog(tab = "Advanced", group = "Numerics", enable=false),
                choices(checkBox=true),
                Evaluate=true,
                HideResult = true);
  parameter Modelica.Units.SI.MassFlowRate m_flow_small = 1e-4
    "Regularization mass flow rate"
    annotation (Dialog(tab="Advanced", group="Numerics", enable=false));

  parameter Boolean showTotalPressures = false
    "= true, if total pressures are calculated and shown"
    annotation (Dialog(tab = "Advanced", group = "Diagnostics"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult = true);
  parameter Boolean showVelocities = false
    "= true, if fluid velocities at ports are calculated and shown"
    annotation (Dialog(tab = "Advanced", group = "Diagnostics",
                enable=geometry.frictionPressureLoss),
                choices(checkBox=true),
                Evaluate=true,
                HideResult = true);
  parameter Boolean showDarcyFrictionNumber = false
    "= true, if Darcy friction number is calculated and shown"
    annotation (Dialog(tab = "Advanced", group = "Diagnostics",
                enable=geometry.frictionPressureLoss),
                choices(checkBox=true),
                Evaluate=true,
                HideResult = true);
  parameter Boolean showIndividualPressureLosses = false
    "= true, if individual pressure loss contributions are calculated and shown"
    annotation (Dialog(tab = "Advanced", group = "Diagnostics"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult = true);

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.MassFlowRate m_flow
    "Mass flow rate"
    annotation (Dialog(tab="General", group="Inputs", enable=false));

  input SorpLib.Components.Fittings.Records.FluidProperties fluidProperties_a
    "Fluid properties at port a for design flow direction (i.e., a->b)"
    annotation (Dialog(tab="General", group="Inputs", enable=false));
  input SorpLib.Components.Fittings.Records.FluidProperties fluidProperties_b
    "Fluid properties at port b for design flow direction (i.e., a->b)"
    annotation (Dialog(tab="General", group="Inputs", enable=false));
  input SorpLib.Components.Fittings.Records.FluidProperties fluidProperties_a_ad
    "Fluid properties at port a for reverse design flow direction (i.e., a<-b)"
    annotation (Dialog(tab="General", group="Inputs", enable=false));
  input SorpLib.Components.Fittings.Records.FluidProperties fluidProperties_b_ad
    "Fluid properties at port b for reverse design flow direction (i.e., a<-b)"
    annotation (Dialog(tab="General", group="Inputs", enable=false));

  //
  // Definition of outputs
  //
  output Modelica.Units.SI.PressureDifference dp_static
    "Static pressure loss"
    annotation (Dialog(tab="General", group="Outputs", enable=false));

  //
  // Definition of variables
  //
  Modelica.Units.SI.MassFlowRate m_flow_hyd
    "Hydraulic mass flow rate";

  //
  // Definition of protected veriables describing fluid properties
  //
protected
  Modelica.Units.SI.Density rho_mean_dd
    "Average density at port a for design flow direction (i.e., a->b)";
  Modelica.Units.SI.ReynoldsNumber Re_mean_dd
    "Average Reynolds number describing the flow regime for design flow 
    direction (i.e., a->b)";

  Modelica.Units.SI.Density rho_mean_rdd
    "Average density at port a for reverse design flow direction (i.e., b->a)";
  Modelica.Units.SI.ReynoldsNumber Re_mean_rdd
    "Average Reynolds number describing the flow regime for reverse design flow 
    direction (i.e., b->a)";

  Real lambda_mean_dd(unit="1")
    "Average Darcy friction number for design flow direction (a->b)";
  Real lambda_mean_rdd(unit="1")
    "Average Darcy friction number for reverse design flow direction (b->a)";

  Modelica.Units.SI.PressureDifference dp_dyn_dd
    "Dynamic pressure loss for design flow direction (a->b)";
  Modelica.Units.SI.PressureDifference dp_geo_dd
    "Geodetic pressure loss for design flow direction (a->b)";
  Real f_fric_dd(unit="1/(kg.m)")
    "Factor of friction-caused pressure loss for design flow direction (a->b)";
  Real f_fit_dd(unit="1/(kg.m)")
    "Factor of fitting-caused pressure loss for design flow direction (a->b)";

  Modelica.Units.SI.PressureDifference dp_dyn_rdd
    "Dynamic pressure loss for reverse design flow direction (b->a)";
  Modelica.Units.SI.PressureDifference dp_geo_rdd
    "Geodetic pressure loss for reverse design flow direction (b->a)";
  Real f_fric_rdd(unit="1/(kg.m)")
    "Factor of friction-caused pressure loss for reverse design flow direction 
    (b->a)";
  Real f_fit_rdd(unit="1/(kg.m)")
    "Factor of fitting-caused pressure loss for reverse design flow direction 
    (b->a)";

  //
  // Definition of variables required to calculate diagnostic variables
  //
  Modelica.Units.SI.Density rho_a
    "Density at port a";
  Modelica.Units.SI.Density rho_b
    "Density at port b";

  Modelica.Units.SI.DynamicViscosity eta_a
    "Dynamic viscosity at port a";
  Modelica.Units.SI.DynamicViscosity eta_b
    "Dynamic viscosity at port b";

  //
  // Definition (and calculation) of diagnostic variables
  //
public
  Modelica.Units.SI.Velocity w_a
    "Hydraulic fluid velocity at port a";
  Modelica.Units.SI.Velocity w_b
    "Hydraulic fluid velocity at port b";
  Modelica.Units.SI.Velocity w_mean
    "Average hydraulic fluid velocity";

  Modelica.Units.SI.ReynoldsNumber Re_a=
    abs(m_flow_hyd) * geometry.d_hyd_a / (A_a * eta_a) if
    showDarcyFrictionNumber
    "Reynolds number describing the flow regime at port a";
  Modelica.Units.SI.ReynoldsNumber Re_b=
    abs(m_flow_hyd) * geometry.d_hyd_b / (A_b * eta_b) if
    showDarcyFrictionNumber
    "Reynolds number describing the flow regime at port b";
  Modelica.Units.SI.ReynoldsNumber Re_mean=
    abs(m_flow_hyd) * (geometry.d_hyd_a / (A_a * eta_a) +
    geometry.d_hyd_b / (A_b * eta_b)) / 2 if
    showDarcyFrictionNumber
    "Average Reynolds number describing the flow regime";

  Real lambda_mean(unit="1")=
    SorpLib.Numerics.regStep(
      x=m_flow_hyd,
      y1=lambda_mean_dd,
      y2=lambda_mean_rdd,
      x_small=m_flow_small) if
    showDarcyFrictionNumber
    "Average darcy friction number";

  Modelica.Units.SI.Pressure p_total_a=
    fluidProperties_a.p + (w_a^2 / 2 +
    Modelica.Constants.g_n * geometry.z_a) * rho_a if
    showTotalPressures
    "Total pressure at port a";
  Modelica.Units.SI.Pressure p_total_b=
    fluidProperties_b_ad.p + (w_b^2 / 2 +
    Modelica.Constants.g_n * geometry.z_b) * rho_b if
    showTotalPressures
    "Total pressure at port b";
  Modelica.Units.SI.PressureDifference dp_total=
    fluidProperties_a.p + (w_a^2 / 2 +
    Modelica.Constants.g_n * geometry.z_a) * rho_a -
    fluidProperties_b_ad.p - (w_b^2 / 2 +
    Modelica.Constants.g_n * geometry.z_b) * rho_b if
    showTotalPressures
    "Total pressure difference";

  Modelica.Units.SI.PressureDifference dp_dyn=
    1/2 * (rho_a * w_a^2 - rho_b * w_b^2) if
    showIndividualPressureLosses
    "Dynamic pressure difference between port a and b";
  Modelica.Units.SI.PressureDifference dp_geo = Modelica.Constants.g_n *
    (geometry.z_a * rho_a - geometry.z_b * rho_b) if
    showIndividualPressureLosses
    "Geodetic pressure difference between port a and b";
  Modelica.Units.SI.PressureDifference dp_fric=
    SorpLib.Numerics.regSquareWFactors(
      x=m_flow_hyd,
      delta_x=m_flow_small,
      f_positive=1/(f_fric_dd+Modelica.Constants.small),
      f_negative=1/(f_fric_rdd+Modelica.Constants.small)) if
    showIndividualPressureLosses
    "Pressure difference between port a and b due to friction";
  Modelica.Units.SI.PressureDifference dp_fit=
    SorpLib.Numerics.regSquareWFactors(
      x=m_flow_hyd,
      delta_x=m_flow_small,
      f_positive=1/(f_fit_dd+Modelica.Constants.small),
      f_negative=1/(f_fit_rdd+Modelica.Constants.small)) if
    showIndividualPressureLosses
    "Pressure difference between port a and b due to fittings";

equation
  //
  // Assertations
  //
  if not geometry.dynamicPressureLoss then
    assert(Modelica.Math.isEqual(
      s1=geometry.d_hyd_a,
      s2=geometry.d_hyd_b,
      eps=Modelica.Constants.eps),
      "Dynamic pressure loss is not considered: Hydraulic diameters must be " +
      "equal to calculate sound results!",
      level=AssertionLevel.error);
  end if;

  if not geometry.geodeticPressureLoss then
    assert(Modelica.Math.isEqual(
      s1=geometry.z_a,
      s2=geometry.z_b,
      eps=Modelica.Constants.eps),
      "Geodetic pressure loss is not considered: Heights must be equal to " +
      "calculate sound results!",
      level=AssertionLevel.error);
  end if;

  //
  // Calculate variables required to calculate diagnostic variables
  //
  if showVelocities or showTotalPressures or showIndividualPressureLosses then
    if positionFluidProperties==
      SorpLib.Choices.ResistorFluidProperties.Constant then
      rho_a = constantFluidProperties.rho
        "Density at port a";
      rho_b = constantFluidProperties.rho
        "Density at port b";

      eta_a = constantFluidProperties.eta
        "Dynamic viscosity at port a";
      eta_b = constantFluidProperties.eta
        "Dynamic viscosity at port b";

    elseif positionFluidProperties==
      SorpLib.Choices.ResistorFluidProperties.PortAInlet then
      rho_a = fluidProperties_a.rho
        "Density at port a";
      rho_b = fluidProperties_a.rho
        "Density at port b";

      eta_a = fluidProperties_a.eta
        "Dynamic viscosity at port a";
      eta_b = fluidProperties_a.eta
        "Dynamic viscosity at port b";

    elseif positionFluidProperties==
      SorpLib.Choices.ResistorFluidProperties.PortBInlet then
      rho_a = fluidProperties_b_ad.rho
        "Density at port a";
      rho_b = fluidProperties_b_ad.rho
        "Density at port b";

      eta_a = fluidProperties_b_ad.eta
        "Dynamic viscosity at port a";
      eta_b = fluidProperties_b_ad.eta
        "Dynamic viscosity at port b";

    elseif positionFluidProperties==
      SorpLib.Choices.ResistorFluidProperties.ActualInlet then
      if avoid_events then
        rho_a = SorpLib.Numerics.regStep_noEvent(
          x=m_flow_hyd,
          y1=fluidProperties_a.rho,
          y2=fluidProperties_b_ad.rho,
          x_small=m_flow_small)
          "Density at port a";
        rho_b = SorpLib.Numerics.regStep_noEvent(
          x=m_flow_hyd,
          y1=fluidProperties_a.rho,
          y2=fluidProperties_b_ad.rho,
          x_small=m_flow_small)
          "Density at port b";

        eta_a = SorpLib.Numerics.regStep_noEvent(
          x=m_flow_hyd,
          y1=fluidProperties_a.eta,
          y2=fluidProperties_b_ad.eta,
          x_small=m_flow_small)
          "Dynamic viscosity at port a";
        eta_b = SorpLib.Numerics.regStep_noEvent(
          x=m_flow_hyd,
          y1=fluidProperties_a.eta,
          y2=fluidProperties_b_ad.eta,
          x_small=m_flow_small)
          "Dynamic viscosity at port b";

      else
        rho_a = SorpLib.Numerics.regStep(
          x=m_flow_hyd,
          y1=fluidProperties_a.rho,
          y2=fluidProperties_b_ad.rho,
          x_small=m_flow_small)
          "Density at port a";
        rho_b = SorpLib.Numerics.regStep(
          x=m_flow_hyd,
          y1=fluidProperties_a.rho,
          y2=fluidProperties_b_ad.rho,
          x_small=m_flow_small)
          "Density at port b";

        eta_a = SorpLib.Numerics.regStep(
          x=m_flow_hyd,
          y1=fluidProperties_a.eta,
          y2=fluidProperties_b_ad.eta,
          x_small=m_flow_small)
          "Dynamic viscosity at port a";
        eta_b = SorpLib.Numerics.regStep(
          x=m_flow_hyd,
          y1=fluidProperties_a.eta,
          y2=fluidProperties_b_ad.eta,
          x_small=m_flow_small)
          "Dynamic viscosity at port b";

      end if;

    elseif positionFluidProperties==
      SorpLib.Choices.ResistorFluidProperties.AverageInstreaming then
      rho_a = (fluidProperties_a.rho + fluidProperties_b_ad.rho) / 2
        "Density at port a";
      rho_b = (fluidProperties_a.rho + fluidProperties_b_ad.rho) / 2
        "Density at port b";

      eta_a = (fluidProperties_a.eta + fluidProperties_b_ad.eta) / 2
        "Dynamic viscosity at port a";
      eta_b = (fluidProperties_a.eta + fluidProperties_b_ad.eta) / 2
        "Dynamic viscosity at port b";

    else
      if avoid_events then
        rho_a = SorpLib.Numerics.regStep_noEvent(
          x=m_flow_hyd,
          y1=fluidProperties_a.rho,
          y2=fluidProperties_a_ad.rho,
          x_small=m_flow_small)
          "Density at port a";
        rho_b = SorpLib.Numerics.regStep_noEvent(
          x=m_flow_hyd,
          y1=fluidProperties_b.rho,
          y2=fluidProperties_b_ad.rho,
          x_small=m_flow_small)
          "Density at port a";

        eta_a = SorpLib.Numerics.regStep_noEvent(
          x=m_flow_hyd,
          y1=fluidProperties_a.eta,
          y2=fluidProperties_a_ad.eta,
          x_small=m_flow_small)
          "Dynamic viscosity at port a";
        eta_b = SorpLib.Numerics.regStep_noEvent(
          x=m_flow_hyd,
          y1=fluidProperties_b.eta,
          y2=fluidProperties_b_ad.eta,
          x_small=m_flow_small)
          "Dynamic viscosity at port a";

      else
        rho_a = SorpLib.Numerics.regStep(
          x=m_flow_hyd,
          y1=fluidProperties_a.rho,
          y2=fluidProperties_a_ad.rho,
          x_small=m_flow_small)
          "Density at port a";
        rho_b = SorpLib.Numerics.regStep(
          x=m_flow_hyd,
          y1=fluidProperties_b.rho,
          y2=fluidProperties_b_ad.rho,
          x_small=m_flow_small)
          "Density at port a";

        eta_a = SorpLib.Numerics.regStep(
          x=m_flow_hyd,
          y1=fluidProperties_a.eta,
          y2=fluidProperties_a_ad.eta,
          x_small=m_flow_small)
          "Dynamic viscosity at port a";
        eta_b = SorpLib.Numerics.regStep(
          x=m_flow_hyd,
          y1=fluidProperties_b.eta,
          y2=fluidProperties_b_ad.eta,
          x_small=m_flow_small)
          "Dynamic viscosity at port a";

      end if;
    end if;

    w_a = m_flow_hyd / (A_a * rho_a)
      "Hydraulic fluid velocity at port a";
    w_b = m_flow_hyd / (A_b * rho_b)
      "Hydraulic fluid velocity at port b";
    w_mean = (w_a + w_b) / 2
      "Average hydraulic fluid velocity";

  else
    rho_a = 0
      "Density at port a";
    rho_b = 0
      "Density at port b";

    eta_a = 0
      "Dynamic viscosity at port a";
    eta_b = 0
      "Dynamic viscosity at port b";

    w_a = 0
      "Hydraulic fluid velocity at port a";
    w_b = 0
      "Hydraulic fluid velocity at port b";
    w_mean = 0
      "Average hydraulic fluid velocity";

  end if;

  //
  // Calculate average properties for design flow direction (a->b)
  //
  if positionFluidProperties==
    SorpLib.Choices.ResistorFluidProperties.Constant then
    rho_mean_dd = constantFluidProperties.rho
      "Average density for design flow direction (i.e., a->b)";
    Re_mean_dd = abs(m_flow_hyd) / constantFluidProperties.eta *
      (geometry.d_hyd_a / A_a + geometry.d_hyd_b / A_b) / 2
      "Average Reynolds number describing the flow regime for design flow 
      direction (i.e., a->b)";

  elseif positionFluidProperties==
    SorpLib.Choices.ResistorFluidProperties.PortAInlet then
    rho_mean_dd = fluidProperties_a.rho
      "Average density for design flow direction (i.e., a->b)";
    Re_mean_dd = abs(m_flow_hyd) / fluidProperties_a.eta *
      (geometry.d_hyd_a / A_a + geometry.d_hyd_b / A_b) / 2
      "Average Reynolds number describing the flow regime for design flow 
      direction (i.e., a->b)";

  elseif positionFluidProperties==
    SorpLib.Choices.ResistorFluidProperties.PortBInlet then
    rho_mean_dd = fluidProperties_b_ad.rho
      "Average density for design flow direction (i.e., a->b)";
    Re_mean_dd = abs(m_flow_hyd) / fluidProperties_b_ad.eta *
      (geometry.d_hyd_a / A_a + geometry.d_hyd_b / A_b) / 2
      "Average Reynolds number describing the flow regime for design flow 
      direction (i.e., a->b)";

  elseif positionFluidProperties==
    SorpLib.Choices.ResistorFluidProperties.ActualInlet then
    rho_mean_dd = fluidProperties_a.rho
      "Average density for design flow direction (i.e., a->b)";
    Re_mean_dd = abs(m_flow_hyd) / fluidProperties_a.eta *
      (geometry.d_hyd_a / A_a + geometry.d_hyd_b / A_b) / 2
      "Average Reynolds number describing the flow regime for design flow 
      direction (i.e., a->b)";

  elseif positionFluidProperties==
    SorpLib.Choices.ResistorFluidProperties.AverageInstreaming then
    rho_mean_dd = (fluidProperties_a.rho + fluidProperties_b_ad.rho) / 2
      "Average density for design flow direction (i.e., a->b)";
    Re_mean_dd = abs(m_flow_hyd) /
      ((fluidProperties_a.eta + fluidProperties_b_ad.eta) / 2) *
      (geometry.d_hyd_a / A_a + geometry.d_hyd_b / A_b) / 2
      "Average Reynolds number describing the flow regime for design flow 
      direction (i.e., a->b)";

  else
    rho_mean_dd = (fluidProperties_a.rho + fluidProperties_b.rho) / 2
      "Average density for design flow direction (i.e., a->b)";
    Re_mean_dd = abs(m_flow_hyd) *
      (geometry.d_hyd_a / (A_a * fluidProperties_a.eta) +
       geometry.d_hyd_b / (A_b * fluidProperties_b.eta)) / 2
      "Average Reynolds number describing the flow regime for design flow 
      direction (i.e., a->b)";

  end if;

  //
  // Calculate average properties for reverse design flow direction (b->a)
  //
  if positionFluidProperties==
    SorpLib.Choices.ResistorFluidProperties.Constant then
    rho_mean_rdd = rho_mean_dd
      "Average density for reverse design flow direction (i.e., b->a)";
    Re_mean_rdd = Re_mean_dd
      "Average Reynolds number describing the flow regime for reverse flow 
      design direction (i.e., b->a)";

  elseif positionFluidProperties==
    SorpLib.Choices.ResistorFluidProperties.PortAInlet then
    rho_mean_rdd = rho_mean_dd
      "Average density for reverse design flow direction (i.e., b->a)";
    Re_mean_rdd = Re_mean_dd
      "Average Reynolds number describing the flow regime for reverse flow 
      design direction (i.e., b->a)";

  elseif positionFluidProperties==
    SorpLib.Choices.ResistorFluidProperties.PortBInlet then
    rho_mean_rdd = rho_mean_dd
      "Average density for reverse design flow direction (i.e., b->a)";
    Re_mean_rdd = Re_mean_dd
      "Average Reynolds number describing the flow regime for reverse flow 
      design direction (i.e., b->a)";

  elseif positionFluidProperties==
    SorpLib.Choices.ResistorFluidProperties.ActualInlet then
    rho_mean_rdd = fluidProperties_b_ad.rho
      "Average density for reverse design flow direction (i.e., b->a)";
    Re_mean_rdd = abs(m_flow_hyd) / fluidProperties_b_ad.eta *
      (geometry.d_hyd_a / A_a + geometry.d_hyd_b / A_b) / 2
      "Average Reynolds number describing the flow regime for reverse flow 
      design direction (i.e., b->a)";

  elseif positionFluidProperties==
    SorpLib.Choices.ResistorFluidProperties.AverageInstreaming then
    rho_mean_rdd = rho_mean_dd
      "Average density for reverse design flow direction (i.e., b->a)";
    Re_mean_rdd = Re_mean_dd
      "Average Reynolds number describing the flow regime for reverse flow 
      design direction (i.e., b->a)";

  else
    rho_mean_rdd = (fluidProperties_a_ad.rho + fluidProperties_b_ad.rho) / 2
      "Average density for reverse design flow direction (i.e., b->a)";
    Re_mean_rdd = abs(m_flow_hyd) *
      (geometry.d_hyd_a / (A_a * fluidProperties_a_ad.eta) +
       geometry.d_hyd_b / (A_b * fluidProperties_b_ad.eta)) / 2
      "Average Reynolds number describing the flow regime for reverse flow 
      design direction (i.e., b->a)";

  end if;

  //
  // Calculate factors required for dynamic pressure loss
  //
  if geometry.dynamicPressureLoss then
    if positionFluidProperties==
      SorpLib.Choices.ResistorFluidProperties.Constant then
      dp_dyn_dd = 1/2 * m_flow_hyd^2 * 1/constantFluidProperties.rho *
        (1/A_b^2 - 1/A_a^2)
        "Dynamic pressure loss for design flow direction (a->b)";
      dp_dyn_rdd = dp_dyn_dd
        "Dynamic pressure loss for reverse design flow direction (b->a)";

    elseif positionFluidProperties==
      SorpLib.Choices.ResistorFluidProperties.PortAInlet then
      dp_dyn_dd = 1/2 * m_flow_hyd^2 * 1/fluidProperties_a.rho *
        (1/A_b^2 - 1/A_a^2)
        "Dynamic pressure loss for design flow direction (a->b)";
      dp_dyn_rdd = dp_dyn_dd
        "Dynamic pressure loss for reverse design flow direction (b->a)";

    elseif positionFluidProperties==
      SorpLib.Choices.ResistorFluidProperties.PortBInlet then
      dp_dyn_dd = 1/2 * m_flow_hyd^2 * 1/fluidProperties_b_ad.rho *
        (1/A_b^2 - 1/A_a^2)
        "Dynamic pressure loss for design flow direction (a->b)";
      dp_dyn_rdd = dp_dyn_dd
        "Dynamic pressure loss for reverse design flow direction (b->a)";

    elseif positionFluidProperties==
      SorpLib.Choices.ResistorFluidProperties.ActualInlet then
      dp_dyn_dd = 1/2 * m_flow_hyd^2 * 1/fluidProperties_a.rho *
        (1/A_b^2 - 1/A_a^2)
        "Dynamic pressure loss for design flow direction (a->b)";
      dp_dyn_rdd = 1/2 * m_flow_hyd^2 * 1/fluidProperties_b_ad.rho *
        (1/A_b^2 - 1/A_a^2)
        "Dynamic pressure loss for reverse design flow direction (b->a)";

    elseif positionFluidProperties==
      SorpLib.Choices.ResistorFluidProperties.AverageInstreaming then
      dp_dyn_dd =  1 / (fluidProperties_a.rho + fluidProperties_b_ad.rho) *
         m_flow_hyd^2 * (1/A_b^2 - 1/A_a^2)
        "Dynamic pressure loss for design flow direction (a->b)";
      dp_dyn_rdd = dp_dyn_dd
        "Dynamic pressure loss for reverse design flow direction (b->a)";

    else
      dp_dyn_dd = 1/2 * m_flow_hyd^2 * (1 / (A_b^2 * fluidProperties_b.rho) -
        1 / (A_a^2 * fluidProperties_a.rho))
        "Dynamic pressure loss for design flow direction (a->b)";
      dp_dyn_rdd = 1/2 * m_flow_hyd^2 * (1 / (A_b^2 * fluidProperties_b_ad.rho) -
        1 / (A_a^2 * fluidProperties_a_ad.rho))
        "Dynamic pressure loss for reverse design flow direction (b->a)";

    end if;

  else
    dp_dyn_dd = 0
      "Dynamic pressure loss for design flow direction (a->b)";
    dp_dyn_rdd = 0
      "Dynamic pressure loss for reverse design flow direction (b->a)";

  end if;

  //
  // Calculate factors required for geodetic pressure loss
  //
  if geometry.geodeticPressureLoss then
    if positionFluidProperties==
      SorpLib.Choices.ResistorFluidProperties.Constant then
      dp_geo_dd = Modelica.Constants.g_n * constantFluidProperties.rho *
        (geometry.z_b - geometry.z_a)
        "Geodetic pressure loss for design flow direction (a->b)";
      dp_geo_rdd = dp_geo_dd
        "Geodetic pressure loss for reverse design flow direction (b->a)";

    elseif positionFluidProperties==
      SorpLib.Choices.ResistorFluidProperties.PortAInlet then
      dp_geo_dd = Modelica.Constants.g_n * fluidProperties_a.rho *
        (geometry.z_b - geometry.z_a)
        "Geodetic pressure loss for design flow direction (a->b)";
      dp_geo_rdd = dp_geo_dd
        "Geodetic pressure loss for reverse design flow direction (b->a)";

    elseif positionFluidProperties==
      SorpLib.Choices.ResistorFluidProperties.PortBInlet then
      dp_geo_dd = Modelica.Constants.g_n * fluidProperties_b_ad.rho *
        (geometry.z_b - geometry.z_a)
        "Geodetic pressure loss for design flow direction (a->b)";
      dp_geo_rdd = dp_geo_dd
        "Geodetic pressure loss for reverse design flow direction (b->a)";

    elseif positionFluidProperties==
      SorpLib.Choices.ResistorFluidProperties.ActualInlet then
      dp_geo_dd = Modelica.Constants.g_n * fluidProperties_a.rho *
        (geometry.z_b - geometry.z_a)
        "Geodetic pressure loss for design flow direction (a->b)";
      dp_geo_rdd = Modelica.Constants.g_n * fluidProperties_b_ad.rho *
        (geometry.z_b - geometry.z_a)
        "Geodetic pressure loss for reverse design flow direction (b->a)";

    elseif positionFluidProperties==
      SorpLib.Choices.ResistorFluidProperties.AverageInstreaming then
      dp_geo_dd = Modelica.Constants.g_n * (fluidProperties_a.rho +
         fluidProperties_b_ad.rho) / 2 * (geometry.z_b - geometry.z_a)
        "Geodetic pressure loss for design flow direction (a->b)";
      dp_geo_rdd = dp_geo_dd
        "Geodetic pressure loss for reverse design flow direction (b->a)";

    else
      dp_geo_dd = Modelica.Constants.g_n * (fluidProperties_b.rho *
        geometry.z_b - fluidProperties_a.rho * geometry.z_a)
        "Geodetic pressure loss for design flow direction (a->b)";
      dp_geo_rdd = Modelica.Constants.g_n * (fluidProperties_b_ad.rho *
        geometry.z_b - fluidProperties_a_ad.rho * geometry.z_a)
        "Geodetic pressure loss for reverse design flow direction (b->a)";

    end if;

  else
    dp_geo_dd = 0
      "Geodetic pressure loss for design flow direction (a->b)";
    dp_geo_rdd = 0
      "Geodetic pressure loss for reverse design flow direction (b->a)";

  end if;

  //
  // Calculate factors required for friction-caused pressure loss
  //
  if geometry.frictionPressureLoss then
    f_fric_dd = 1/2 * 1 / (rho_mean_dd * A_mean^2) *
      geometry.l / d_hyd_mean * lambda_mean_dd
      "Factor of friction-caused pressure loss for design flow direction (a->b)";
    f_fric_rdd = 1/2 * 1 / (rho_mean_rdd * A_mean^2) *
      geometry.l / d_hyd_mean * lambda_mean_rdd
      "Factor of friction-caused pressure loss for reverse design flow direction 
      (b->a)";

  else
    f_fric_dd = 0
      "Factor of friction-caused pressure loss for design flow direction (a->b)";
    f_fric_rdd = 0
      "Factor of friction-caused pressure loss for reverse design flow direction 
      (b->a)";

  end if;

  //
  // Calculate factors required for fitting-caused pressure loss
  //
  if geometry.fittingPressureLosss then
    if flowDirectionDependentZeta then
      if positionFluidProperties==
        SorpLib.Choices.ResistorFluidProperties.Constant then
        f_fit_dd = 1/2 * sum(zeta_a) / (constantFluidProperties.rho * A_a^2)
          "Factor of fitting-caused pressure loss for design flow direction 
          (a->b)";
        f_fit_rdd = 1/2 * sum(zeta_b) / (constantFluidProperties.rho * A_b^2)
          "Factor of fitting-caused pressure loss for reverse design flow 
          direction (b->a)";

      elseif positionFluidProperties==
        SorpLib.Choices.ResistorFluidProperties.PortAInlet then
        f_fit_dd = 1/2 * sum(zeta_a) / (fluidProperties_a.rho * A_a^2)
          "Factor of fitting-caused pressure loss for design flow direction 
          (a->b)";
        f_fit_rdd = 1/2 * sum(zeta_b) / (fluidProperties_a.rho * A_b^2)
          "Factor of fitting-caused pressure loss for reverse design flow 
          direction (b->a)";

      elseif positionFluidProperties==
        SorpLib.Choices.ResistorFluidProperties.PortBInlet then
        f_fit_dd = 1/2 * sum(zeta_a) / (fluidProperties_b_ad.rho * A_a^2)
          "Factor of fitting-caused pressure loss for design flow direction 
          (a->b)";
        f_fit_rdd = 1/2 * sum(zeta_b) / (fluidProperties_b_ad.rho * A_b^2)
          "Factor of fitting-caused pressure loss for reverse design flow 
          direction (b->a)";

      elseif positionFluidProperties==
        SorpLib.Choices.ResistorFluidProperties.ActualInlet then
        f_fit_dd = 1/2 * sum(zeta_a) / (fluidProperties_a.rho * A_a^2)
          "Factor of fitting-caused pressure loss for design flow direction 
          (a->b)";
        f_fit_rdd = 1/2 * sum(zeta_b) / (fluidProperties_b_ad.rho * A_b^2)
          "Factor of fitting-caused pressure loss for reverse design flow 
          direction (b->a)";

      elseif positionFluidProperties==
        SorpLib.Choices.ResistorFluidProperties.AverageInstreaming then
        f_fit_dd = 1/2 * sum(zeta_a) / ((fluidProperties_a.rho +
          fluidProperties_b_ad.rho) / 2 * A_a^2)
          "Factor of fitting-caused pressure loss for design flow direction 
          (a->b)";
        f_fit_rdd = 1/2 * sum(zeta_b) / ((fluidProperties_a.rho +
          fluidProperties_b_ad.rho) / 2 * A_b^2)
          "Factor of fitting-caused pressure loss for reverse design flow 
          direction (b->a)";

      else
        f_fit_dd = 1/2 * sum(zeta_a) / (fluidProperties_a.rho * A_a^2)
          "Factor of fitting-caused pressure loss for design flow direction 
          (a->b)";
        f_fit_rdd = 1/2 * sum(zeta_b) / (fluidProperties_b_ad.rho * A_b^2)
          "Factor of fitting-caused pressure loss for reverse design flow 
          direction (b->a)";

      end if;

    else
      f_fit_dd = 1/2 * ((sum(zeta_a) + sum(zeta_b)) / 2) /
        (rho_mean_dd * A_mean^2)
        "Factor of fitting-caused pressure loss for design flow direction (a->b)";
      f_fit_rdd = 1/2 * ((sum(zeta_a) + sum(zeta_b)) / 2) /
        (rho_mean_rdd * A_mean^2)
        "Factor of fitting-caused pressure loss for reverse design flow direction 
        (b->a)";

    end if;

  else
    f_fit_dd = 0
      "Factor of fitting-caused pressure loss for design flow direction (a->b)";
    f_fit_rdd = 0
      "Factor of fitting-caused pressure loss for reverse design flow direction 
      (b->a)";

  end if;

  //
  // Calculate pressure difference as function of mass flow rate or vice versa
  //
  if dpFromMFlow then
    if avoid_events then
      if (geometry.dynamicPressureLoss or geometry.geodeticPressureLoss) and
        not (geometry.frictionPressureLoss or geometry.fittingPressureLosss) then
        dp_static =
          SorpLib.Numerics.regStep_noEvent(
            x=m_flow_hyd,
            y1=dp_dyn_dd + dp_geo_dd,
            y2=dp_dyn_rdd + dp_geo_rdd,
            x_small=m_flow_small)
          "Static pressure difference between port a and b";

      elseif not (geometry.dynamicPressureLoss or geometry.geodeticPressureLoss) and
        (geometry.frictionPressureLoss or geometry.fittingPressureLosss) then
        dp_static =
          SorpLib.Numerics.regSquareWFactors_noEvent(
            x=m_flow_hyd,
            delta_x=m_flow_small,
            f_positive=1 / (f_fric_dd + f_fit_dd + Modelica.Constants.small),
            f_negative=1 / (f_fric_rdd + f_fit_rdd + Modelica.Constants.small))
          "Static pressure difference between port a and b";

      else
        dp_static =
          SorpLib.Numerics.regStep_noEvent(
            x=m_flow_hyd,
            y1=dp_dyn_dd + dp_geo_dd,
            y2=dp_dyn_rdd + dp_geo_rdd,
            x_small=m_flow_small) +
          SorpLib.Numerics.regSquareWFactors_noEvent(
            x=m_flow_hyd,
            delta_x=m_flow_small,
            f_positive=1 / (f_fric_dd + f_fit_dd + Modelica.Constants.small),
            f_negative=1 / (f_fric_rdd + f_fit_rdd + Modelica.Constants.small))
          "Static pressure difference between port a and b";

      end if;

    else
      if (geometry.dynamicPressureLoss or geometry.geodeticPressureLoss) and
        not (geometry.frictionPressureLoss or geometry.fittingPressureLosss) then
        dp_static =
          SorpLib.Numerics.regStep(
            x=m_flow_hyd,
            y1=dp_dyn_dd + dp_geo_dd,
            y2=dp_dyn_rdd + dp_geo_rdd,
            x_small=m_flow_small)
          "Static pressure difference between port a and b";

      elseif not (geometry.dynamicPressureLoss or geometry.geodeticPressureLoss) and
        (geometry.frictionPressureLoss or geometry.fittingPressureLosss) then
        dp_static =
          SorpLib.Numerics.regSquareWFactors(
            x=m_flow_hyd,
            delta_x=m_flow_small,
            f_positive=1 / (f_fric_dd + f_fit_dd + Modelica.Constants.small),
            f_negative=1 / (f_fric_rdd + f_fit_rdd + Modelica.Constants.small))
          "Static pressure difference between port a and b";

      else
        dp_static =
          SorpLib.Numerics.regStep(
            x=m_flow_hyd,
            y1=dp_dyn_dd + dp_geo_dd,
            y2=dp_dyn_rdd + dp_geo_rdd,
            x_small=m_flow_small) +
          SorpLib.Numerics.regSquareWFactors(
            x=m_flow_hyd,
            delta_x=m_flow_small,
            f_positive=1 / (f_fric_dd + f_fit_dd + Modelica.Constants.small),
            f_negative=1 / (f_fric_rdd + f_fit_rdd + Modelica.Constants.small))
          "Static pressure difference between port a and b";

      end if;
    end if;

  else
    if avoid_events then
      if (geometry.dynamicPressureLoss or geometry.geodeticPressureLoss) and
        not (geometry.frictionPressureLoss or geometry.fittingPressureLosss) then
        dp_static =
          SorpLib.Numerics.regStep_noEvent(
            x=m_flow_hyd,
            y1=dp_dyn_dd + dp_geo_dd,
            y2=dp_dyn_rdd + dp_geo_rdd,
            x_small=m_flow_small)
          "Static pressure difference between port a and b";

      elseif not (geometry.dynamicPressureLoss or geometry.geodeticPressureLoss) and
        (geometry.frictionPressureLoss or geometry.fittingPressureLosss) then
        m_flow_hyd =
          SorpLib.Numerics.regSquareWFactors_inv_noEvent(
            y=dp_static,
            delta_x=m_flow_small,
            f_positive=1 / (f_fric_dd + f_fit_dd + Modelica.Constants.small),
            f_negative=1 / (f_fric_rdd + f_fit_rdd + Modelica.Constants.small))
          "Hydraulic mass flow rate";

      else
        dp_static =
          SorpLib.Numerics.regStep_noEvent(
            x=m_flow_hyd,
            y1=dp_dyn_dd + dp_geo_dd,
            y2=dp_dyn_rdd + dp_geo_rdd,
            x_small=m_flow_small) +
          SorpLib.Numerics.regSquareWFactors_noEvent(
            x=m_flow_hyd,
            delta_x=m_flow_small,
            f_positive=1 / (f_fric_dd + f_fit_dd + Modelica.Constants.small),
            f_negative=1 / (f_fric_rdd + f_fit_rdd + Modelica.Constants.small))
          "Static pressure difference between port a and b";

      end if;

    else
      if (geometry.dynamicPressureLoss or geometry.geodeticPressureLoss) and
        not (geometry.frictionPressureLoss or geometry.fittingPressureLosss) then
        dp_static =
          SorpLib.Numerics.regStep(
            x=m_flow_hyd,
            y1=dp_dyn_dd + dp_geo_dd,
            y2=dp_dyn_rdd + dp_geo_rdd,
            x_small=m_flow_small)
          "Static pressure difference between port a and b";

      elseif not (geometry.dynamicPressureLoss or geometry.geodeticPressureLoss) and
        (geometry.frictionPressureLoss or geometry.fittingPressureLosss) then
        m_flow_hyd =
          SorpLib.Numerics.regSquareWFactors_inv(
            y=dp_static,
            delta_x=m_flow_small,
            f_positive=1 / (f_fric_dd + f_fit_dd + Modelica.Constants.small),
            f_negative=1 / (f_fric_rdd + f_fit_rdd + Modelica.Constants.small))
          "Hydraulic mass flow rate";

      else
        dp_static =
          SorpLib.Numerics.regStep(
            x=m_flow_hyd,
            y1=dp_dyn_dd + dp_geo_dd,
            y2=dp_dyn_rdd + dp_geo_rdd,
            x_small=m_flow_small) +
          SorpLib.Numerics.regSquareWFactors(
            x=m_flow_hyd,
            delta_x=m_flow_small,
            f_positive=1 / (f_fric_dd + f_fit_dd + Modelica.Constants.small),
            f_negative=1 / (f_fric_rdd + f_fit_rdd + Modelica.Constants.small))
          "Static pressure difference between port a and b";

      end if;
    end if;

  end if;

  //
  // Annotations
  //
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
          Ellipse(
          extent={{-80,80},{80,-80}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={0,127,0},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-60,60},{60,-60}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={135,135,135},
          fillPattern=FillPattern.Solid,
          textString="ṁ 
= 
f(Δp, ...)")}), Diagram(coordinateSystem(preserveAspectRatio=false)),
    Documentation(info="<html>
<p>
This partial model is the base model for all pressure loss models. It defines 
fundamental parameters and variables required by all pressure loss models. Models 
that inherit properties from this partial model have to complete the friction-
based pressure loss correlation. Furthermore, the geometry record may be redeclared
and constrained.
<br/><br/>
The following variables must be specified in the model that inherit properties:
</p>
<ul>
  <li>
  Hydraulic mass flow rate <i>m_flow_hyd</i>.
  </li>
  <li>
  Friction factor <i>lambda_mean_dd</i> for design flow direction (i.e., a->b).
  </li>
  <li>
  Friction factor <i>lambda_mean_rdd</i> for reverse design flow direction (i.e., 
  a->b).
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  December 22, 2023, by Mirko Engelpracht:<br/>
  First implementation
  </li>
</ul>
</html>"));
end PartialPressureLoss;
