﻿within SorpLib.Components.Fittings.BaseClasses;
partial model PartialResistance
  "Base model for all hydraulic resistors"

  //
  // Definition of parameters regarding the medium
  //
  parameter Integer no_components = 1
    "Number of components"
    annotation (Dialog(tab="General", group="Medium"),
                Evaluate=true,
                HideResult=true);

  //
  // Definition of parameters regarding calculation setup
  //
  parameter SorpLib.Choices.ResistorFluidProperties positionFluidProperties=
    SorpLib.Choices.ResistorFluidProperties.ActualInlet
    "Position of fluid properties used for calculations"
    annotation (Dialog(tab="General", group="Calculation Setup"),
                Evaluate=true,
                HideResult = true);
  parameter Boolean instreamingPropertiesByInput = false
    " = true, if instreaming fluid properties are provied via inputs (i.e., 
    fluidProperties_a and fluidProperties_b_ad) if already calculated"
    annotation (Dialog(tab="General", group="Calculation Setup"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult = true);

  parameter Boolean dpFromMFlow = true
    " = true, if static pressure loss is calculated from mass flow rate; otherwise,
    mass flow rate is calculated from static pressure loss"
    annotation (Dialog(tab="General", group="Calculation Setup",
                enable= not dynamicPressureLoss and not geodeticPressureLoss and
                  (frictionPressureLoss or fittingPressureLosss)),
                choices(checkBox=true),
                Evaluate=true,
                HideResult = true);

  parameter Boolean dynamicPressureLoss = false
    " = true, if dynamic pressure loss shall be included (i.e., different
    velocities at port a and b due to change of cross-sectional areas and 
    densities)"
    annotation (Dialog(tab="General", group="Calculation Setup"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult = true);
  parameter Boolean geodeticPressureLoss = false
    " = true, if geodetic pressure loss shall be included (i.e., different
    heights at port a and b)"
    annotation (Dialog(tab="General", group="Calculation Setup"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult = true);
  parameter Boolean frictionPressureLoss = false
    " = true, if pressure loss due to friction shall be included"
    annotation (Dialog(tab="General", group="Calculation Setup"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult = true);
  parameter Boolean fittingPressureLosss = true
    " = true, if pressure losses due to fittings shall be included"
    annotation (Dialog(tab="General", group="Calculation Setup"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult = true);

  //
  // Definition of parameters regarding the resistance characterisation
  //
  replaceable model PressureLossModel =
    SorpLib.Components.Fittings.BaseClasses.PartialPressureLoss
    constrainedby SorpLib.Components.Fittings.BaseClasses.PartialPressureLoss(
      positionFluidProperties=positionFluidProperties,
      dpFromMFlow=dpFromMFlow,
      geometry=geometry,
      constantFluidProperties=
        SorpLib.Components.Fittings.Records.FluidProperties(
          p=p_ref,
          T=T_ref,
          rho=rho_ref,
          eta=eta_ref),
      flowDirectionDependentZeta=flowDirectionDependentZeta,
      zeta_a=zeta_a,
      zeta_b=zeta_b,
      m_flow=m_flow,
      fluidProperties_a=fluidProperties_a,
      fluidProperties_b=fluidProperties_b,
      fluidProperties_a_ad=fluidProperties_a_ad,
      fluidProperties_b_ad=fluidProperties_b_ad,
      avoid_events=avoid_events,
      m_flow_small=m_flow_small)
    "Pressure loss model"
    annotation (Dialog(tab="Resistance Characterisation", group="Models"),
                choicesAllMatching=true);

  replaceable parameter SorpLib.Components.Fittings.Records.GeometryGenericReistance geometry
    constrainedby SorpLib.Components.Fittings.Records.GeometryGenericReistance(
      final dynamicPressureLoss=dynamicPressureLoss,
      final geodeticPressureLoss=geodeticPressureLoss,
      final frictionPressureLoss=frictionPressureLoss,
      final fittingPressureLosss=fittingPressureLosss)
    "Resistor geometry"
    annotation (Dialog(tab="Resistance Characterisation", group="Geometry"),
                choicesAllMatching=true);

  parameter Modelica.Units.SI.Pressure p_ref = 1e5
    "Reference pressure that might be used for pressure loss calculations"
    annotation (Dialog(tab="Resistance Characterisation", group="Fluid Properties",
                enable=positionFluidProperties==
                  SorpLib.Choices.ResistorFluidProperties.Constant));
  parameter Modelica.Units.SI.Temperature T_ref = 298.15
    "Reference temperature that might be used for pressure loss calculations"
    annotation (Dialog(tab="Resistance Characterisation", group="Fluid Properties",
                enable=positionFluidProperties==
                  SorpLib.Choices.ResistorFluidProperties.Constant));
  parameter Modelica.Units.SI.Density rho_ref = 997
    "Reference density that might be used for pressure loss calculations"
    annotation (Dialog(tab="Resistance Characterisation", group="Fluid Properties",
                enable=positionFluidProperties==
                  SorpLib.Choices.ResistorFluidProperties.Constant));
  parameter Modelica.Units.SI.DynamicViscosity eta_ref = 8.9e-4
    "Reference dynamic viscosity that might be used for pressure loss calculations"
    annotation (Dialog(tab="Resistance Characterisation", group="Fluid Properties",
                enable=positionFluidProperties==
                  SorpLib.Choices.ResistorFluidProperties.Constant));

  parameter Boolean flowDirectionDependentZeta = false
    " = true, if loss factors differ with flow direction (e.g., sudden expansion)
    and area and density at port a or b are used"
    annotation (Dialog(tab="Resistance Characterisation", group="Fittings",
                enable=fittingPressureLosss),
                choices(checkBox=true),
                Evaluate=true,
                HideResult = true);
  parameter Real[:] zeta_a(each unit="1") = {0}
    "Loss factors due to fittings at port a"
    annotation (Dialog(tab="Resistance Characterisation", group="Fittings",
                enable=fittingPressureLosss));
  parameter Real[:] zeta_b(each unit="1") = zeta_a
    "Loss factors due to fittings at port b"
    annotation (Dialog(tab="Resistance Characterisation", group="Fittings",
                enable=fittingPressureLosss and flowDirectionDependentZeta));

  //
  // Definition or initialisation parameters
  //
  parameter Modelica.Units.SI.MassFlowRate m_flow_start = 1e-4
    "Start value for mass flow rate"
    annotation (Dialog(tab="Initialisation", group="Start Values"));

  //
  // Definition of advanced parameters
  //
  parameter Boolean avoid_events = false
    "= true, if events are avoid by using noEvent()-operator"
    annotation (Dialog(tab = "Advanced", group = "Numerics"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult = true);
  parameter Modelica.Units.SI.MassFlowRate m_flow_small = 1e-4
    "Regularization mass flow rate"
    annotation (Dialog(tab="Advanced", group="Numerics"));

  //
  // Definition of connectors
  //
  Modelica.Blocks.Interfaces.RealInput p_a(final unit="Pa") = p_ref if
       instreamingPropertiesByInput
    "Pressure at port a for design flow direction (i.e., a->b)"
    annotation (Dialog(tab="Resistance Characterisation", group="Fluid Properties",
                enable=instreamingPropertiesByInput));
  Modelica.Blocks.Interfaces.RealInput T_a(final unit="K") = T_ref if
       instreamingPropertiesByInput
    "Temperature at port a for design flow direction (i.e., a->b)"
    annotation (Dialog(tab="Resistance Characterisation", group="Fluid Properties",
                enable=instreamingPropertiesByInput));
  Modelica.Blocks.Interfaces.RealInput rho_a(final unit="kg/m3") = rho_ref if
     instreamingPropertiesByInput
    "Density at port a for design flow direction (i.e., a->b)"
    annotation (Dialog(tab="Resistance Characterisation", group="Fluid Properties",
                enable=instreamingPropertiesByInput));
  Modelica.Blocks.Interfaces.RealInput eta_a(final unit="Pa.s") = eta_ref if
       instreamingPropertiesByInput
    "Dynamic viscosity at port a for design flow direction (i.e., a->b)"
    annotation (Dialog(tab="Resistance Characterisation", group="Fluid Properties",
                enable=instreamingPropertiesByInput));

  Modelica.Blocks.Interfaces.RealInput p_b_ad(final unit="Pa") = p_ref if
       instreamingPropertiesByInput
    "Pressure at port b for reverse design flow direction (i.e., a<-b)"
    annotation (Dialog(tab="Resistance Characterisation", group="Fluid Properties",
                enable=instreamingPropertiesByInput));
  Modelica.Blocks.Interfaces.RealInput T_b_ad(final unit="K") = T_ref if
       instreamingPropertiesByInput
    "Temperature at port b for reverse design flow direction (i.e., a<-b)"
    annotation (Dialog(tab="Resistance Characterisation", group="Fluid Properties",
                enable=instreamingPropertiesByInput));
  Modelica.Blocks.Interfaces.RealInput rho_b_ad(final unit="kg/m3") = rho_ref if
     instreamingPropertiesByInput
    "Density at port b for reverse design flow direction (i.e., a<-b)"
    annotation (Dialog(tab="Resistance Characterisation", group="Fluid Properties",
                enable=instreamingPropertiesByInput));
  Modelica.Blocks.Interfaces.RealInput eta_b_ad(final unit="Pa.s") = eta_ref if
       instreamingPropertiesByInput
    "Dynamic viscosity at port b for reverse design flow direction (i.e., a<-b)"
    annotation (Dialog(tab="Resistance Characterisation", group="Fluid Properties",
                enable=instreamingPropertiesByInput));

  //
  // Definition of protected connectors
  //
protected
  Modelica.Blocks.Interfaces.RealInput p_a_internal(final unit="Pa")
    "Needed for connecting to conditional connector";
  Modelica.Blocks.Interfaces.RealInput T_a_internal(final unit="K")
    "Needed for connecting to conditional connector";
  Modelica.Blocks.Interfaces.RealInput rho_a_internal(final unit="kg/m3")
    "Needed for connecting to conditional connector";
  Modelica.Blocks.Interfaces.RealInput eta_a_internal(final unit="Pa.s")
    "Needed for connecting to conditional connector";

  Modelica.Blocks.Interfaces.RealInput p_b_ad_internal(final unit="Pa")
    "Needed for connecting to conditional connector";
  Modelica.Blocks.Interfaces.RealInput T_b_ad_internal(final unit="K")
    "Needed for connecting to conditional connector";
  Modelica.Blocks.Interfaces.RealInput rho_b_ad_internal(final unit="kg/m3")
    "Needed for connecting to conditional connector";
  Modelica.Blocks.Interfaces.RealInput eta_b_ad_internal(final unit="Pa.s")
    "Needed for connecting to conditional connector";

  //
  // Definition of ports
  //
public
  replaceable SorpLib.Basics.Interfaces.BaseClasses.PartialFluidPort port_a
    constrainedby Basics.Interfaces.BaseClasses.PartialFluidPort(
      final no_components=no_components,
      m_flow(start=m_flow_start))
    "Fluid port a"
    annotation (Placement(transformation(extent={{-90,-10},{-70,10}}),
                iconTransformation(extent={{-90,-10},{-70,10}})),
                choicesAllMatching=true);

  replaceable SorpLib.Basics.Interfaces.BaseClasses.PartialFluidPort port_b
    constrainedby Basics.Interfaces.BaseClasses.PartialFluidPort(
      final no_components=no_components,
      m_flow(start=-m_flow_start))
    "Fluid port b"
    annotation (Placement(transformation(extent={{70,-10},{90,10}}),
                iconTransformation(extent={{70,-10},{90,10}})),
                choicesAllMatching=true);

  //
  // Definition of models
  //
  PressureLossModel pressureLossModel
    "Model to calculate the pressure loss";

  //
  // Definition of variables
  //
  SorpLib.Components.Fittings.Records.FluidProperties fluidProperties_a
    "Fluid properties at port a for design flow direction (i.e., a->b)";
  SorpLib.Components.Fittings.Records.FluidProperties fluidProperties_b
    "Fluid properties at port b for design flow direction (i.e., a->b)";
  SorpLib.Components.Fittings.Records.FluidProperties fluidProperties_a_ad
    "Fluid properties at port a for reverse design flow direction (i.e., a<-b)";
  SorpLib.Components.Fittings.Records.FluidProperties fluidProperties_b_ad
    "Fluid properties at port b for reverse design flow direction (i.e., a<-b)";

  Modelica.Units.SI.PressureDifference dp
    "Static pressure difference between port a and b";
  Modelica.Units.SI.MassFlowRate m_flow
    "Mass flow rate";

equation
  //
  // Assertions
  //
  assert(dynamicPressureLoss or geodeticPressureLoss or
         frictionPressureLoss or fittingPressureLosss,
         "At least one option (i.e., dynamic pressure loss, geodetic pressure " +
         "loss, friction pressure loss, or fitting pressure loss) must be taken " +
         "into account when calculating the pressure loss!",
         level=AssertionLevel.error);

  if not dpFromMFlow then
    assert(not dynamicPressureLoss and not geodeticPressureLoss and
           (frictionPressureLoss or fittingPressureLosss),
           "Mass flow rate can only be calculated from the static pressure loss " +
           "if just friction- or fitting-caused pressure losses are considered!",
           level=AssertionLevel.error);
  end if;

  //
  // Connectors
  //
  connect(p_a_internal, p_a);
  connect(T_a_internal, T_a);
  connect(rho_a_internal, rho_a);
  connect(eta_a_internal, eta_a);

  connect(p_b_ad_internal, p_b_ad);
  connect(T_b_ad_internal, T_b_ad);
  connect(rho_b_ad_internal, rho_b_ad);
  connect(eta_b_ad_internal, eta_b_ad);

  if not instreamingPropertiesByInput then
    p_a_internal = p_ref
      "Needed for connecting to conditional connector: Set dummy value as not
      needed in this case";
    T_a_internal = T_ref
      "Needed for connecting to conditional connector: Set dummy value as not
      needed in this case";
    rho_a_internal = rho_ref
      "Needed for connecting to conditional connector: Set dummy value as not
      needed in this case";
    eta_a_internal = eta_ref
      "Needed for connecting to conditional connector: Set dummy value as not
      needed in this case";

    p_b_ad_internal = p_ref
      "Needed for connecting to conditional connector: Set dummy value as not
      needed in this case";
    T_b_ad_internal = T_ref
      "Needed for connecting to conditional connector: Set dummy value as not
      needed in this case";
    rho_b_ad_internal = rho_ref
      "Needed for connecting to conditional connector: Set dummy value as not
      needed in this case";
    eta_b_ad_internal = eta_ref
      "Needed for connecting to conditional connector: Set dummy value as not
      needed in this case";
  end if;

  //
  // Momentum balance
  //
  dp = port_a.p - port_b.p
    "Pressure difference between port a and b";
  dp = pressureLossModel.dp_static
    "Pressure loss";

  //
  // Mass balance
  //
  0 = port_a.m_flow + port_b.m_flow
    "Steady-state mass balance";
  port_a.m_flow = m_flow
    "Mass flow rate at port a";

  port_a.Xi_outflow = inStream(port_b.Xi_outflow)
    "Stream variable: Trivial equation since no change of mass fractions";
  port_b.Xi_outflow = inStream(port_a.Xi_outflow)
    "Stream variable: Trivial equation since no change of mass fractions";

  //
  // Energy balance
  //
  port_a.h_outflow = inStream(port_b.h_outflow)
    "Stream variable: Trivial equation since no change of energy";
  port_b.h_outflow = inStream(port_a.h_outflow)
    "Stream variable: Trivial equation since no change of energy";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial model is the base model for all resistors. It defines fundamental 
parameters and variables required by all resistors. Models that inherit properties 
from this partial model have to redeclare the fluid ports. Moreover, fluid properties
at port a and b must be calculated for both the design and reverse the design flow
direction. In this context, appropriate fluid property models are required. Furthermore, 
the geometry record and pressure loss model may be redeclared and constrained.
<br/><br/>
The following variables must be specified in the model that inherit properties:
</p>
<ul>
  <li>
  Pressure <i>fluidProperties_a.p</i> at port a for design flow direction 
  (i.e., a->b).
  </li>
  <li>
  Temperature <i>fluidProperties_a.T</i> at port a for design flow direction 
  (i.e., a->b).
  </li>
  <li>
  Density <i>fluidProperties_a.rho</i> at port a for design flow direction 
  (i.e., a->b).
  </li>
  <li>
  Dynamic viscosity <i>fluidProperties_a.eta</i> at port a for design flow  
  direction (i.e., a->b).
  </li>
  <br/>
  <li>
  Pressure <i>fluidProperties_b.p</i> at port b for design flow direction 
  (i.e., a->b).
  </li>
  <li>
  Temperature <i>fluidProperties_b.T</i> at port b for design flow direction 
  (i.e., a->b).
  </li>
  <li>
  Density <i>fluidProperties_b.rho</i> at port b for design flow direction 
  (i.e., a->b).
  </li>
  <li>
  Dynamic viscosity <i>fluidProperties_b.eta</i> at port b for design flow  
  direction (i.e., a->b).
  </li>
  <br/>  
  <li>
  Pressure <i>fluidProperties_a_ad.p</i> at port a for reverse design flow  
  direction (i.e., b->a).
  </li>
  <li>
  Temperature <i>fluidProperties_a_ad.T</i> at port a for reverse design flow  
  direction (i.e., b->a).
  </li>
  <li>
  Density <i>fluidProperties_a_ad.rho</i> at port a for reverse design flow  
  direction (i.e., b->a).
  </li>
  <li>
  Dynamic viscosity <i>fluidProperties_a_ad.eta</i> at port a for reverse design 
  flow direction (i.e., b->a).
  </li>
  <br/>  
  <li>
  Pressure <i>fluidProperties_b_ad.p</i> at port b for reverse design flow  
  direction (i.e., b->a).
  </li>
  <li>
  Temperature <i>fluidProperties_b_ad.T</i> at port b for reverse design flow  
  direction (i.e., b->a).
  </li>
  <li>
  Density <i>fluidProperties_b_ad.rho</i> at port b for reverse design flow  
  direction (i.e., b->a).
  </li>
  <li>
  Dynamic viscosity <i>fluidProperties_b_ad.eta</i> at port b for reverse design 
  flow direction (i.e., b->a).
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  December 22, 2023, by Mirko Engelpracht:<br/>
  Adaptations due to restructering the library and documentation.
  </li>
  <li>
  January 14, 2021, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"), Icon(graphics={Polygon(
          points={{-80,50},{-80,-50},{-50,-50},{-50,-50},{50,-50},{50,-50},{80,-50},
              {80,50},{-80,50}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Line(
          points={{-60,-20},{-60,-20},{-60,20},{60,-20},{60,20}},
          color={0,0,0},
          thickness=1),
        Text(
          extent={{-60,40},{60,20}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={135,135,135},
          fillPattern=FillPattern.Solid,
          textString="ṁ = f(Δp, ...)"),
        Line(
          points={{-60,-30},{60,-30}},
          color={0,0,0},
          thickness=1,
          arrow={Arrow.Open,Arrow.Open})}));
end PartialResistance;
