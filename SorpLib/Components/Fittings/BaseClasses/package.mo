within SorpLib.Components.Fittings;
package BaseClasses "Base models and functions for all fittings"
  extends Modelica.Icons.BasesPackage;

  annotation (Documentation(info="<html>
<p>
This package contains partial fitting models and functions, containing fundamental 
definitions for fittings. The content of this package is only of interest when 
adding new fittings to the library. 
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end BaseClasses;
