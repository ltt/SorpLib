within SorpLib.Components.Fittings.BaseClasses;
partial model PartialMultiPort
  "Base model for all multi port models (i.e., ideal splitter/junctions)"

  //
  // Definition of parameters regarding the medium
  //
  parameter Integer no_components = 1
    "Number of components"
    annotation (Dialog(tab="General", group="Medium"),
                Evaluate=true,
                HideResult=true);

  //
  // Definition of setup parameters
  //
  parameter Integer no_ports_b = 0
    "Number of ports at position b"
    annotation (Dialog(connectorSizing=true));

  //
  // Definition of ports
  //
  replaceable SorpLib.Basics.Interfaces.BaseClasses.PartialFluidPort port_a
    constrainedby Basics.Interfaces.BaseClasses.PartialFluidPort(
      final no_components=no_components)
    "Fluid port a"
    annotation (Placement(transformation(extent={{-70,-10},{-50,10}}),
                iconTransformation(extent={{-70,-10},{-50,10}})),
                choicesAllMatching=true);

  replaceable SorpLib.Basics.Interfaces.BaseClasses.PartialFluidPort ports_b[no_ports_b]
    constrainedby Basics.Interfaces.BaseClasses.PartialFluidPort(
      each final no_components=no_components)
    "Fluid ports b"
    annotation (Placement(transformation(extent={{50,-10},{70,10}}),
                iconTransformation(extent={{50,-10},{70,10}})),
                choicesAllMatching=true);

  //
  // Definition of variables
  //
protected
  Real denominator = Modelica.Constants.small +
    sum({abs(ports_b[ind_b].m_flow) for ind_b in 1:no_ports_b})
    "Denominator used to calculate mixing stream variables";

equation
  //
  // Momentum balance
  //
  ports_b.p = fill(port_a.p, no_ports_b)
    "No pressure loss";

  //
  // Mass balance
  //
  0 = port_a.m_flow + sum(ports_b.m_flow)
    "Steady-state mass balance";

  for ind_c in 1:no_components-1 loop
    port_a.Xi_outflow[ind_c] =
      (sum({abs(ports_b[ind_b].m_flow) * inStream(ports_b[ind_b].Xi_outflow[ind_c])
        for ind_b in 1:no_ports_b}) + Modelica.Constants.small) / denominator
      "Calculate ideal mixing stream variable for mass flow from port b to port a";
  end for;

  for ind_b in 1:no_ports_b loop
    ports_b[ind_b].Xi_outflow = inStream(port_a.Xi_outflow)
      "Expose stream variable of port due to splitting of mass flow from port a";
  end for;

  //
  // Energy balance
  //
  port_a.h_outflow =
    (sum({abs(ports_b[ind_b].m_flow) * inStream(ports_b[ind_b].h_outflow)
      for ind_b in 1:no_ports_b}) + Modelica.Constants.small) / denominator
    "Calculate ideal mixing stream variable for mass flow from port b to port a";

  for ind_b in 1:no_ports_b loop
    ports_b[ind_b].h_outflow = inStream(port_a.h_outflow)
      "Expose stream variable of port due to splitting of mass flow from port a";
  end for;

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial model is the base model for all multi port models. It defines fundamental
parameters and variables required by all multi port models. Models that inherit 
properties from this partial model have to redeclare the fluid ports and to add a 
medium model.
<br/><br/>
This models is based on
<a href=\"Modelica://Modelica.Fluid.Fittings.MultiPort\">Modelica.Fluid.Fittings.MultiPort</a>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 20, 2023, by Mirko Engelpracht:<br/>
  Minor revisions (documentation).
  </li>
  <li>
  January 19, 2021, by Mirko Engelpracht:<br/>
  First implementation
  </li>
</ul>
</html>"), Icon(graphics={
        Rectangle(
          extent={{-60,100},{60,-100}},
          lineColor={0,0,0},
          lineThickness=0.5,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Line(
          points={{20,80},{40,80}},
          color={0,0,0},
          thickness=0.5),
        Line(
          points={{20,40},{40,40}},
          color={0,0,0},
          thickness=0.5),
        Line(
          points={{-40,0},{-20,0}},
          color={0,0,0},
          thickness=0.5),
        Line(
          points={{20,-80},{40,-80}},
          color={0,0,0},
          thickness=0.5),
        Line(
          points={{20,80},{-20,0},{20,-80}},
          color={0,0,0},
          thickness=0.5),
        Line(
          points={{20,0},{40,0}},
          color={0,0,0},
          thickness=0.5),
        Line(
          points={{20,-40},{40,-40}},
          color={0,0,0},
          thickness=0.5)}));
end PartialMultiPort;
