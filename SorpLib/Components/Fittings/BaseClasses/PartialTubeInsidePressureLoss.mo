within SorpLib.Components.Fittings.BaseClasses;
partial model PartialTubeInsidePressureLoss
  "Base model for all tube-inside pressure loss models"
  extends SorpLib.Components.Fittings.BaseClasses.PartialPressureLoss(
    final psi = 1,
    redeclare replaceable parameter SorpLib.Components.Fittings.Records.GeometryTube geometry
    constrainedby SorpLib.Components.Fittings.Records.GeometryTube);

equation
  //
  // Calculate the hydraulic mass flow rate
  //
  m_flow_hyd = m_flow / geometry.no_hydraulicParallelFlows
    "Hydraulic mass flow rate";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial model is the base model for all pressure loss models applied in tubes.
It defines fundamental parameters and variables required by all pressure loss models. 
Models that inherit properties from this partial model have to complete the friction-
based pressure loss correlation.
<br/><br/>
The following variables must be specified in the model that inherit properties:
</p>
<ul>
  <li>
  Friction factor <i>lambda_mean_dd</i> for design flow direction (i.e., a->b).
  </li>
  <li>
  Friction factor <i>lambda_mean_rdd</i> for reverse design flow direction (i.e., 
  a->b).
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  December 22, 2023, by Mirko Engelpracht:<br/>
  First implementation
  </li>
</ul>
</html>"));
end PartialTubeInsidePressureLoss;
