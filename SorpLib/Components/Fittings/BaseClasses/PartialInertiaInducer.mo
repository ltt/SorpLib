﻿within SorpLib.Components.Fittings.BaseClasses;
partial model PartialInertiaInducer
  "Base model for all inertia inducers"

  //
  // Definition of parameters regarding the medium
  //
  parameter Integer no_components = 1
    "Number of components"
    annotation (Dialog(tab="General", group="Medium"),
                Evaluate=true,
                HideResult=true);

  //
  // Definition of setup parameters
  //
  parameter Modelica.Units.SI.Length f_momentum = 0.1
    "Momentum factor describing acceleration of fluid mass"
    annotation (Dialog(tab="General", group="Momentum Setup"));

  //
  // Definition or initialisation parameters
  //
  parameter Integer initialisationType(min=1, max=3) = 3
    "Initialisation type: Fixed, steady-state, or free"
    annotation (Dialog(tab="Initialisation", group="Type"),
                choices(
                  choice=1 "Fixed",
                  choice=2 "Steady-state",
                  choice=3 "Free"));

  parameter Modelica.Units.SI.MassFlowRate m_flow_initial = 0.01
    "Initial value of mass flow rate"
    annotation (Dialog(tab="Initialisation", group="Initial Values"));

  //
  // Definition of ports
  //
  replaceable SorpLib.Basics.Interfaces.BaseClasses.PartialFluidPort port_a
    constrainedby Basics.Interfaces.BaseClasses.PartialFluidPort(
      final no_components=no_components)
    "Fluid port a"
    annotation (Placement(transformation(extent={{-70,-10},{-50,10}}),
                iconTransformation(extent={{-70,-10},{-50,10}})),
                choicesAllMatching=true);

  replaceable SorpLib.Basics.Interfaces.BaseClasses.PartialFluidPort port_b
    constrainedby Basics.Interfaces.BaseClasses.PartialFluidPort(
      final no_components=no_components)
    "Fluid ports b"
    annotation (Placement(transformation(extent={{50,-10},{70,10}}),
                iconTransformation(extent={{50,-10},{70,10}})),
                choicesAllMatching=true);

  //
  // Definition of variables
  //
  Modelica.Units.SI.PressureDifference dp
    "Pressure difference between port a and b";

  Modelica.Units.SI.MassFlowRate m_flow
    "Mass flow rate";

initial equation
  if initialisationType==1 then
    m_flow = m_flow_initial
      "Fixed intial value";

  elseif initialisationType==2 then
    der(m_flow) = 0
      "Steady-state initialisation";

  end if;

equation
  //
  // Momentum balance
  //
  dp = port_a.p - port_b.p
    "Pressure difference between port a and b";
  der(m_flow) = dp * f_momentum
    "Mass flow rate";

  //
  // Mass balance
  //
  0 = port_a.m_flow + port_b.m_flow
    "Steady-state mass balance";
  port_a.m_flow = m_flow
    "Mass flow rate at port a";

  port_a.Xi_outflow = inStream(port_b.Xi_outflow)
    "Stream variable: Trivial equation since no change of mass fractions";
  port_b.Xi_outflow = inStream(port_a.Xi_outflow)
    "Stream variable: Trivial equation since no change of mass fractions";

  //
  // Energy balance
  //
  port_a.h_outflow = inStream(port_b.h_outflow)
    "Stream variable: Trivial equation since no change of energy";
  port_b.h_outflow = inStream(port_a.h_outflow)
    "Stream variable: Trivial equation since no change of energy";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial model is the base model for all inertia inducers. It defines fundamental
parameters and variables required by all inertia inducers. Models that inherit 
properties from this partial model have to redeclare the fluid ports and to add a 
medium model.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 21, 2023, by Mirko Engelpracht:<br/>
  First implementation
  </li>
</ul>
</html>"), Icon(graphics={
        Rectangle(
          extent={{60,-50},{-60,50}},
          lineColor={0,0,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          lineThickness=1),
        Ellipse(
          extent={{-40,10},{-20,-10}},
          lineColor={135,135,135},
          lineThickness=1,
          fillColor={135,135,135},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-18,2},{-14,-2}},
          lineColor={135,135,135},
          lineThickness=1,
          fillColor={135,135,135},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-12,2},{-8,-2}},
          lineColor={135,135,135},
          lineThickness=1,
          fillColor={135,135,135},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-6,2},{-2,-2}},
          lineColor={135,135,135},
          lineThickness=1,
          fillColor={135,135,135},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{2,2},{6,-2}},
          lineColor={135,135,135},
          lineThickness=1,
          fillColor={135,135,135},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{8,2},{12,-2}},
          lineColor={135,135,135},
          lineThickness=1,
          fillColor={135,135,135},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{14,2},{18,-2}},
          lineColor={135,135,135},
          lineThickness=1,
          fillColor={135,135,135},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{20,10},{40,-10}},
          lineColor={135,135,135},
          lineThickness=1,
          fillColor={135,135,135},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-40,32},{40,10}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={135,135,135},
          fillPattern=FillPattern.Solid,
          textString="dṁ/dtau = ..."),
        Line(
          points={{-40,-16},{40,-16}},
          color={0,0,0},
          thickness=1,
          arrow={Arrow.Open,Arrow.Open})}));
end PartialInertiaInducer;
