within SorpLib.Components.Fittings.BaseClasses;
partial model PartialTJunction
  "Base model for all T-junction elements"

  //
  // Definition of parameters regarding the medium
  //
  parameter Integer no_components = 1
    "Number of components"
    annotation (Dialog(tab="General", group="Medium"),
                Evaluate=true,
                HideResult=true);

  //
  // Definition of parameters regarding geometry
  //
  parameter Modelica.Units.SI.Volume V = 5e-5
    "Fluid volume of the T-junction"
    annotation (Dialog(tab = "General", group = "Geometry"));

  //
  // Definition of parameters regarding calculation setup
  //
  parameter SorpLib.Choices.IndependentVariablesVolume independentStateVariables=
    SorpLib.Choices.IndependentVariablesVolume.phX
    "Independent state variables"
    annotation (Dialog(tab="General", group="Calculation Setup"),
                Evaluate=true,
                HideResult = true);
  parameter Boolean pressureNoStateVariable = true
    " = true, if pressure is not a state variable and is not needed to calculate
    fluid properties like density (e.g., incompressible fluid)"
    annotation (Dialog(tab="General", group="Calculation Setup"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult = true);
  parameter Boolean neglectTermVp = false
    " = true, if term p * v is neglected for the specific internal energy (i.e., 
    u = h)"
    annotation (Dialog(tab="General", group="Calculation Setup"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult = true);

  //
  // Definition or initialisation parameters
  //
  parameter Modelica.Units.SI.Pressure p_initial = 1e5
    "Initial value of pressure"
    annotation (Dialog(tab="Initialisation", group="Initial Values"));
  parameter Modelica.Units.SI.Temperature T_initial = 298.15
    "Initial value of temperature"
    annotation (Dialog(tab="Initialisation", group="Initial Values",
                enable=(independentStateVariables==
                  SorpLib.Choices.IndependentVariablesVolume.pTX)));
  parameter Modelica.Units.SI.SpecificEnthalpy h_initial = 0
    "Initial value of specific enthalpy"
    annotation (Dialog(tab="Initialisation", group="Initial Values",
                enable=(independentStateVariables==
                  SorpLib.Choices.IndependentVariablesVolume.phX)));
  parameter Modelica.Units.SI.MassFraction[no_components] X_i_initial=
    fill(1/no_components, no_components)
    "Initial values of mass fractions"
    annotation (Dialog(tab="Initialisation", group="Initial Values",
                enable=no_components>1));

  parameter Modelica.Units.SI.MassFlowRate m_flow_start = 1e-4
    "Start value for mass flow rate"
    annotation (Dialog(tab="Initialisation", group="Start Values"));

  //
  // Definition of advanced parameters
  //
  parameter SorpLib.Choices.BalanceEquations type_overallMassBalance=
    SorpLib.Choices.BalanceEquations.TransientFixedInitial
    "Handling of the overall mass balance and corresponding initialisation"
    annotation (Dialog(tab = "Advanced", group = "Conservation Equations"),
                Evaluate=true,
                HideResult=true);
  parameter SorpLib.Choices.BalanceEquations type_independentMassBalances=
    type_overallMassBalance
    "Handling of independent mass balances and corresponding initialisations"
    annotation (Dialog(tab = "Advanced", group = "Conservation Equations",
                enable=no_components>1),
                Evaluate=true,
                HideResult=true);
  parameter SorpLib.Choices.BalanceEquations type_energyBalance=
    SorpLib.Choices.BalanceEquations.TransientFixedInitial
    "Handling of the energy balance and corresponding initialisation"
    annotation (Dialog(tab = "Advanced", group = "Conservation Equations"),
                Evaluate=true,
                HideResult = true);

  parameter Boolean avoid_events = false
    "= true, if events are avoid by using noEvent()-operator"
    annotation (Dialog(tab = "Advanced", group = "Numerics"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult = true);

  //
  // Definition of ports
  //
  replaceable SorpLib.Basics.Interfaces.BaseClasses.PartialFluidPort port_a
    constrainedby Basics.Interfaces.BaseClasses.PartialFluidPort(
      final no_components=no_components,
      m_flow(start=m_flow_start))
    "Fluid port a"
    annotation (Placement(transformation(extent={{-10,-88},{10,-68}}),
                iconTransformation(extent={{-10,-88},{10,-68}})),
                choicesAllMatching=true);

  replaceable SorpLib.Basics.Interfaces.BaseClasses.PartialFluidPort port_b
    constrainedby Basics.Interfaces.BaseClasses.PartialFluidPort(
      final no_components=no_components,
      m_flow(start=-0.5*m_flow_start))
    "Fluid port b"
    annotation (Placement(transformation(extent={{-90,-10},{-70,10}}),
                iconTransformation(extent={{-90,-10},{-70,10}})),
                choicesAllMatching=true);

  replaceable SorpLib.Basics.Interfaces.BaseClasses.PartialFluidPort port_c
    constrainedby Basics.Interfaces.BaseClasses.PartialFluidPort(
      final no_components=no_components,
      m_flow(start=-0.5*m_flow_start))
    "Fluid port c"
    annotation (Placement(transformation(extent={{70,-10},{90,10}}),
                iconTransformation(extent={{70,-10},{90,10}})),
                choicesAllMatching=true);

  //
  // Definition of variables
  //
  Modelica.Units.SI.Pressure p(
    start=p_initial,
    stateSelect= if not pressureNoStateVariable then StateSelect.prefer
      else StateSelect.avoid)
    "Pressure";
  Modelica.Units.SI.Temperature T(
    start=T_initial,
    stateSelect= if independentStateVariables==
      SorpLib.Choices.IndependentVariablesVolume.pTX then StateSelect.prefer
      else StateSelect.avoid)
    "Temperature";
  Modelica.Units.SI.SpecificVolume v
    "Specific volume";
  Modelica.Units.SI.Density rho
    "Density";
  Modelica.Units.SI.MassFraction[no_components] X_i(
    start=X_i_initial,
    each stateSelect=if no_components > 1 then StateSelect.prefer
      else StateSelect.avoid)
    "Mass fractions";

  Modelica.Units.SI.SpecificEnthalpy h(
    start=h_initial,
    stateSelect= if independentStateVariables==
      SorpLib.Choices.IndependentVariablesVolume.phX then StateSelect.prefer
      else StateSelect.avoid)
    "Specific enthalpy";
  Modelica.Units.SI.SpecificEnthalpy[no_components] h_i
    "Specific enthalpy of individual components";
  Modelica.Units.SI.SpecificInternalEnergy u
    "Specific internal energy";

  Modelica.Units.SI.Mass m
    "Mass";
  Modelica.Units.SI.Mass[no_components] m_i
    "Mass of individual components";
  Modelica.Units.SI.MassFlowRate dm_dtau
    "Derivative of mass w.r.t. time";
  Modelica.Units.SI.MassFlowRate[no_components] dm_i_dtau
    "Derivative of individual components' masses w.r.t. time";
  Modelica.Units.SI.MassFlowRate mc_flow
    "Sum of all convective mass flow rates across boundaries";
  Modelica.Units.SI.MassFlowRate[no_components] mc_i_flow
    "Sum of all individual components' convective mass flow rates across 
    boundaries";

  Modelica.Units.SI.InternalEnergy U
    "Internal energy";
  Real dU_dtau(final unit="W")
    "Derivative of internal energy w.r.t. time";
  Modelica.Units.SI.EnthalpyFlowRate Hb_flow
    "Sum of all enthalpy flow rates across boundaries";

initial equation
  if not pressureNoStateVariable and type_overallMassBalance ==
    SorpLib.Choices.BalanceEquations.TransientFixedInitial then
    p = p_initial
      "Fixed initial value";

  elseif not pressureNoStateVariable and type_overallMassBalance ==
    SorpLib.Choices.BalanceEquations.TransientSteadyStateInitial then
    der(p) = 0
      "Steady-state initialisation of overall mass balance";

  end if;

  if type_independentMassBalances ==
    SorpLib.Choices.BalanceEquations.TransientFixedInitial then
    X_i = X_i_initial
      "Fixed initial values";

  elseif type_independentMassBalances ==
    SorpLib.Choices.BalanceEquations.TransientSteadyStateInitial then
    der(X_i) = zeros(no_components)
      "Steady-state initialisation of independent mass balances";

  end if;

  if type_energyBalance ==
    SorpLib.Choices.BalanceEquations.TransientFixedInitial then
    if independentStateVariables==
      SorpLib.Choices.IndependentVariablesVolume.pTX then
      T = T_initial
        "Fixed initial value";

    else
      h = h_initial
        "Fixed initial value";

    end if;

  elseif type_energyBalance ==
    SorpLib.Choices.BalanceEquations.TransientSteadyStateInitial then
    if independentStateVariables==
      SorpLib.Choices.IndependentVariablesVolume.pTX then
      der(T) = 0
        "Steady-state initial value";

    else
      der(h) = 0
        "Steady-state initial value";

    end if;
  end if;

equation
  //
  // Property calculation
  //
  v = 1 / rho
    "Specific volume";

  u = if neglectTermVp then h else h - p * v
    "Specific internal energy";

  //
  // Momentum balance
  //
  port_a.p = p
    "Total pressure at the port (i.e., homogenous volume)";
  port_b.p = p
    "Total pressure at the port (i.e., homogenous volume)";
  port_c.p = p
    "Total pressure at the port (i.e., homogenous volume)";

  //
  // Mass balance
  //
  m = V / v
    "Mass";
  m_i = m .* X_i
    "Mass of individual components";

  dm_dtau = mc_flow
    "Overall mass balance";
  dm_i_dtau = mc_i_flow
    "Individual components' mass balances";

  if type_independentMassBalances==
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial then
    dm_i_dtau = zeros(no_components)
      "Steady-state independent mass balances";

  else
    dm_i_dtau = X_i .* dm_dtau .+ m .* der(X_i)
      "Transient independent mass balances";

  end if;

  mc_flow = port_a.m_flow + port_b.m_flow + port_c.m_flow
    "Sum of all convective mass flow rates across boundaries";

  if avoid_events then
    for ind_comp in 1:no_components loop
      if ind_comp <> no_components then
        mc_i_flow[ind_comp] =
          port_a.m_flow * noEvent(actualStream(port_a.Xi_outflow[ind_comp])) +
          port_b.m_flow * noEvent(actualStream(port_b.Xi_outflow[ind_comp])) +
          port_c.m_flow * noEvent(actualStream(port_c.Xi_outflow[ind_comp]))
          "Sum of all convective mass flow rates of component ind_comp across 
          boundaries";

      else
        mc_i_flow[ind_comp] =  mc_flow - sum(mc_i_flow[1:no_components-1])
          "Sum of all convective mass flow rates of last component across 
          boundaries";

      end if;
    end for;

  else
    for ind_comp in 1:no_components loop
      if ind_comp <> no_components then
        mc_i_flow[ind_comp] =
          port_a.m_flow * actualStream(port_a.Xi_outflow[ind_comp]) +
          port_b.m_flow * actualStream(port_b.Xi_outflow[ind_comp]) +
          port_c.m_flow * actualStream(port_c.Xi_outflow[ind_comp])
          "Sum of all convective mass flow rates of component ind_comp across 
          boundaries";

      else
        mc_i_flow[ind_comp] =  mc_flow - sum(mc_i_flow[1:no_components-1])
          "Sum of all convective mass flow rates of last component across 
          boundaries";

      end if;
    end for;
  end if;

  port_a.Xi_outflow = X_i[1:no_components-1]
    "Independent mass fractions leaving the port (i.e., homogenous volume)";
  port_b.Xi_outflow = X_i[1:no_components-1]
    "Independent mass fractions leaving the port (i.e., homogenous volume)";
  port_c.Xi_outflow = X_i[1:no_components-1]
    "Independent mass fractions leaving the port (i.e., homogenous volume)";

  //
  // Energy balance
  //
  U = m * u
    "Internal energy";

  dU_dtau = Hb_flow
    "Energy balane";

  if avoid_events then
    Hb_flow =
      port_a.m_flow * noEvent(actualStream(port_a.h_outflow)) +
      port_b.m_flow * noEvent(actualStream(port_b.h_outflow)) +
      port_c.m_flow * noEvent(actualStream(port_c.h_outflow))
      "Sum of all enthalpy flow rates across boundaries";

  else
    Hb_flow =
      port_a.m_flow * actualStream(port_a.h_outflow) +
      port_b.m_flow * actualStream(port_b.h_outflow) +
      port_c.m_flow * actualStream(port_c.h_outflow)
      "Sum of all enthalpy flow rates across boundaries";

  end if;

  port_a.h_outflow = h
    "Specific enthalpy leaving the port (i.e., homogenous volume)";
  port_b.h_outflow = h
    "Specific enthalpy leaving the port (i.e., homogenous volume)";
  port_c.h_outflow = h
    "Specific enthalpy leaving the port (i.e., homogenous volume)";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial model is the base model for all T-junction elements. It defines 
fundamental parameters and variables required by all T-junction elements. Models 
that inherit properties from this partial model have to redeclare the fluid ports.
Furthermore, conservation equations must be completed, which may require additional
variables. In this context, appropriate fluid property models are required.
<br/><br/>
The following variables must be specified in the model that inherit properties:
</p>
<ul>
  <li>
  Temperature <i>T</i> or specific enthalpy <i>h</i>, depending on the independent
  state variables.
  </li>
  <li>
  Density <i>&rho;</i>.
  </li>
  <li>
  Specific enthalpies of all components <i>h<sub>i</sub></i>.
  </li>
  <br/>
  <li>
  Partial derivative of the overall mass w.r.t. time <i>dm_dtau</i>.
  </li>
  <li>
  Partial derivative of the internal energy w.r.t. time <i>dU_dtau</i>.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  December 21, 2023, by Mirko Engelpracht:<br/>
  First implementation
  </li>
</ul>
</html>"), Icon(graphics={Polygon(
          points={{-80,50},{-80,-50},{-50,-50},{-50,-80},{50,-80},{50,-50},{80,-50},
              {80,50},{-80,50}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid)}));
end PartialTJunction;
