within SorpLib.Components.Fittings.MultiPorts.Tester;
model Test_LiquidMultiPort "Tester for the liquid multi port model"
  extends Modelica.Icons.Example;

  //
  // Definition of boundary models
  //
  SorpLib.Basics.Sources.Fluids.LiquidSource fs_a(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    p_fixed(displayUnit="bar") = 100000,
    T_fixed=303.15)
    "Fluid source a"
    annotation (Placement(transformation(extent={{-70,-10},{-50,10}})));

  SorpLib.Basics.Sources.Fluids.LiquidSource[5] fs_b(
    each boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
    each boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.SpecificEnthalpy,
    each use_mFlowInput=true,
    h_fixed={100e3,200e3,300e3,400e3,500e3})
    "Fluid source b"
    annotation (Placement(transformation(extent={{70,-10},{50,10}})));

  //
  // Definition of multi ports
  //
  SorpLib.Components.Fittings.MultiPorts.LiquidMultiPort multiPort(no_ports_b=5)
    "Multi port model to split connections at port a"
    annotation (Placement(transformation(extent={{-10,-10},{10,10}})));

  //
  // Definition of input signals
  //
protected
  Modelica.Blocks.Sources.Sine input_mFlow(
    amplitude=1,
    f=1/250,
    offset=1)
    "Input signal for mass flow rate"
    annotation (Placement(transformation(extent={{100,-10},{80,10}})));

equation
  //
  // Connections
  //
  connect(fs_a.port, multiPort.port_a) annotation (Line(
      points={{-60,0},{-6,0}},
      color={28,108,200},
      thickness=1));
  connect(fs_b.port, multiPort.ports_b) annotation (Line(
      points={{60,0},{6,0}},
      color={28,108,200},
      thickness=1));

  connect(input_mFlow.y, fs_b[1].m_flow_input)
    annotation (Line(points={{79,0},{70,0},{70,2},{61.2,2}}, color={0,0,127}));
  connect(input_mFlow.y, fs_b[2].m_flow_input)
    annotation (Line(points={{79,0},{70,0},{70,2},{61.2,2}}, color={0,0,127}));
  connect(input_mFlow.y, fs_b[3].m_flow_input)
    annotation (Line(points={{79,0},{70,0},{70,2},{61.2,2}}, color={0,0,127}));
  connect(input_mFlow.y, fs_b[4].m_flow_input)
    annotation (Line(points={{79,0},{70,0},{70,2},{61.2,2}}, color={0,0,127}));
  connect(input_mFlow.y, fs_b[5].m_flow_input)
    annotation (Line(points={{79,0},{70,0},{70,2},{61.2,2}}, color={0,0,127}));

  //
  // Annotations
  //
  annotation (experiment(StopTime=2500), Documentation(info="<html>
<p>
This model checks the liquid multi port model.
<br/><br/>
To see the model behavior, plot the variables of the models over the 
time. The simulation time is correctly preset (Start: 0 s, Stop = 2500 s). 
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 20, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Test_LiquidMultiPort;
