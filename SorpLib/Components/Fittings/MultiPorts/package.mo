within SorpLib.Components.Fittings;
package MultiPorts "Package containing multi port models for efficient connection of multiple ports to one port"
  extends Modelica.Icons.VariantsPackage;

  annotation (Documentation(info="<html>
<p>
This package contains multi port models based on the Modelica Standard library 
(MSL). These models may be used if several connections shall be made to one port. 
If the connections were made directly to the port, the calculation of mixture 
quantities (i.e., stream variables) would be carried out in the port: This may 
lead to large linear systems of equations, which is usually not desired. In 
contrast, no mixture quantities are calculated with this model. Instead, the 
mixture quantities are calculated in a finite volume model or a comparable model 
that must be located before port a. The calculated mixture quantities are 
propagated to the other connections (i.e., port b).
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end MultiPorts;
