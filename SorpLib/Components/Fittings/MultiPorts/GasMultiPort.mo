within SorpLib.Components.Fittings.MultiPorts;
model GasMultiPort
  "Gas multi port model (i.e., ideal splitter/junctions)"
  extends SorpLib.Components.Fittings.BaseClasses.PartialMultiPort(
    redeclare final Basics.Interfaces.FluidPorts.GasPort_out ports_b,
    redeclare final Basics.Interfaces.FluidPorts.GasPort_in port_a,
    final no_components=Medium.nX);

  //
  // Definition of parameters regarding the medium
  //
  replaceable package Medium =
    SorpLib.Media.IdealGasVaporMixtures.MoistAir_N2_O2_CO2_H2O
    constrainedby Modelica.Media.Interfaces.PartialMedium
    "Medium model of the ideal gas, ideal gas mixture, or ideal gas-vapor mixture"
    annotation (Dialog(tab="General", group="Medium"),
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
The gas multi port model s may be used if several connections shall be made to 
one port. If the connections were made directly to the port, the calculation of 
mixture quantities (i.e., stream variables) would be  carried out in the port: 
This may lead to large linear systems of equations, which is usually not desired. 
In contrast, no mixture quantities are calculated with this model. Instead, the 
mixture quantities are calculated in a finite volume model or a comparable 
model that must be located before port a. The calculated mixture quantities are 
propagated to the other connections (i.e., port b).
<br/><br/>
This models is based on
<a href=\"Modelica://Modelica.Fluid.Fittings.MultiPort\">Modelica.Fluid.Fittings.MultiPort</a>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 20, 2023, by Mirko Engelpracht:<br/>
  First implementation
  </li>
</ul>
</html>"));
end GasMultiPort;
