within SorpLib.Components.Fittings.InertiaInducers;
model GasInertiaInducer "Gas inertia inducer"
  extends SorpLib.Components.Fittings.BaseClasses.PartialInertiaInducer(
    redeclare final Basics.Interfaces.FluidPorts.GasPort_out port_b,
    redeclare final Basics.Interfaces.FluidPorts.GasPort_in port_a,
    final no_components=Medium.nX);

  //
  // Definition of parameters regarding the medium
  //
  replaceable package Medium =
    SorpLib.Media.IdealGasVaporMixtures.MoistAir_N2_O2_CO2_H2O
    constrainedby Modelica.Media.Interfaces.PartialMedium
    "Medium model of the ideal gas, ideal gas mixture, or ideal gas-vapor mixture"
    annotation (Dialog(tab="General", group="Medium"),
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
The gas inertia inducer describes the change of the gas velocity due to its 
inertia. Thus, sudden changes in the velocity (i.e., mass flow rate) caused by,
for example, a pressure drop can be puffered, which increases the numerical
stability.
</p>
<pre>
    dm_flow/d&tau; = &Delta;p * &Psi;;
</pre>
<p>
Herein, the derivative of the mass flow rate <i>m_flow</i> w.r.t. time <i>&tau;</i>
is calculated from the pressure difference <i>&Delta;p</i> and momentum factor 
<i>&Psi;</i>, which directly follows from a simplified momentum balance.
</p>

<h4>Assumptions and limitations</h4>
<ul>
  <li>
  Isenthalpic process
  </li>
  <li>
  No storage of mass or energy
  </li>
</ul>

<h4>Typical use</h4>
<p>
The valve is typically used in hydraulic networks to break algebraic loops.
</p>

<h4>Dynamics</h4>
<p>
The model has the mass flow rate <i>m_flow</i> as dynamic state.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 21, 2023, by Mirko Engelpracht:<br/>
  First implementation
  </li>
</ul>
</html>"));
end GasInertiaInducer;
