within SorpLib.Components.Fittings;
package InertiaInducers "Package containing inertia inducers"
  extends Modelica.Icons.VariantsPackage;

  annotation (Documentation(info="<html>
<p>
This package contains inertia inducers based on the Modelica Standard library 
(MSL). Inertia inducers may be used to describe the acceleration of a fluid
caused by a sudden pressure change, thus also allowing to break algebraic loops.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end InertiaInducers;
