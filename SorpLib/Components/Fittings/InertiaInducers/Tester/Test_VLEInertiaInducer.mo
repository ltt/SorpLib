within SorpLib.Components.Fittings.InertiaInducers.Tester;
model Test_VLEInertiaInducer "Tester for the VLE inertia inducer"
  extends Modelica.Icons.Example;

  //
  // Definition of boundary models
  //
  SorpLib.Basics.Sources.Fluids.VLESource fs_a(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    p_fixed(displayUnit="bar"),
    use_mFlowInput=true,
    T_fixed=303.15)
    "Fluid source a"
    annotation (Placement(transformation(extent={{-70,-10},{-50,10}})));

  SorpLib.Basics.Sources.Fluids.VLESource fs_b(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    p_fixed(displayUnit="bar") = 100000,
    T_fixed=303.15)
    "Fluid source b"
    annotation (Placement(transformation(extent={{70,-10},{50,10}})));

  //
  // Definition of multi ports
  //
  SorpLib.Components.Fittings.InertiaInducers.VLEInertiaInducer inertiaInducer(
      f_momentum=1e-5) "Inertia model"
    annotation (Placement(transformation(extent={{-10,-10},{10,10}})));

  //
  // Definition of input signals
  //
protected
  Modelica.Blocks.Sources.Sine input_mFlow(
    amplitude=2,
    f=1/250,
    offset=0)
    "Input signal for mass flow rate"
    annotation (Placement(transformation(extent={{-100,-10},{-80,10}})));

equation
  //
  // Connections
  //
  connect(fs_a.port, inertiaInducer.port_a) annotation (Line(
      points={{-60,0},{-6,0}},
      color={0,140,72},
      thickness=1));
  connect(fs_b.port, inertiaInducer.port_b) annotation (Line(
      points={{60,0},{6,0}},
      color={0,140,72},
      thickness=1));

  connect(input_mFlow.y, fs_a.m_flow_input) annotation (Line(points={{-79,0},{-70,
          0},{-70,2},{-61.2,2}}, color={0,0,127}));

  //
  // Annotations
  //
  annotation (experiment(StopTime=2500), Documentation(info="<html>
<p>
This model checks the inertia model.
<br/><br/>
To see the model behavior, plot the variables of the models over the 
time. The simulation time is correctly preset (Start: 0 s, Stop = 2500 s). 
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 21, 2023, by Mirko Engelpracht:<br/>
  First implementation
  </li>
</ul>
</html>"));
end Test_VLEInertiaInducer;
