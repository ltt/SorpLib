within SorpLib.Components.Fittings.InertiaInducers;
package Tester "Models to test and varify multi port models"
  extends Modelica.Icons.ExamplesPackage;



  annotation (Documentation(info="<html>
<p>
This package contains executable test models for all implemented inertia inducers. 
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Tester;
