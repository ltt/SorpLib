within SorpLib.Components.Fittings.Resistors;
model LiquidGenericResistance "Liquid generic resistance"
  extends SorpLib.Components.Fittings.BaseClasses.PartialResistance(
    redeclare final Basics.Interfaces.FluidPorts.LiquidPort_out port_b,
    redeclare final Basics.Interfaces.FluidPorts.LiquidPort_in port_a,
    final no_components=Medium.nX,
    final frictionPressureLoss=false,
    redeclare final model PressureLossModel =
      SorpLib.Components.Fittings.PressureLossCorrelations.ZeroFriction,
    fittingPressureLosss=true);

  //
  // Definition of parameters regarding the medium
  //
  replaceable package Medium = Modelica.Media.Water.ConstantPropertyLiquidWater
    constrainedby Modelica.Media.Interfaces.PartialMedium
    "Medium model of the (ideal) liquid"
    annotation (Dialog(tab="General", group="Medium"),
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

  //
  // Definition of variables
  //
protected
  Medium.ThermodynamicState state_a
    "Fluid properties at port a for design flow direction (i.e., a->b)";
  Medium.ThermodynamicState state_b
    "Fluid properties at port b for design flow direction (i.e., a->b)";
  Medium.ThermodynamicState state_a_ad
    "Fluid properties at port a for reverse design flow direction (i.e., a<-b)";
  Medium.ThermodynamicState state_b_ad
    "Fluid properties at port b for reverse design flow direction (i.e., a<-b)";

equation
  //
  // Calculate state properties (only if needed)
  //
  if positionFluidProperties==
    SorpLib.Choices.ResistorFluidProperties.Constant then
    state_a = Medium.setState_phX(
      p=Medium.p_default,
      h=Medium.specificEnthalpy_pTX(
        p=Medium.p_default,
        T=Medium.T_default,
        X=Medium.X_default),
      X=Medium.X_default)
      "Fluid properties at port a for design flow direction (i.e., a->b):
      Not needed, so set dummy values";

    state_b = state_a
      "Fluid properties at port b for design flow direction (i.e., a->b):
      Not needed, so set dummy values";
    state_a_ad = state_a
      "Fluid properties at port a for reverse design flow direction (i.e., a<-b): 
      Not needed, so set dummy values";
    state_b_ad = state_a
      "Fluid properties at port b for reverse design flow direction (i.e., a<-b): 
      Not needed, so set dummy values";

  elseif positionFluidProperties==
    SorpLib.Choices.ResistorFluidProperties.PortAInlet then
    if instreamingPropertiesByInput then
      state_a = Medium.setState_phX(
        p=Medium.p_default,
        h=Medium.specificEnthalpy_pTX(
          p=Medium.p_default,
          T=Medium.T_default,
          X=Medium.X_default),
        X=Medium.X_default)
        "Fluid properties at port a for design flow direction (i.e., a->b):
        Not needed, so set dummy values";

    else
      state_a = Medium.setState_phX(
        p=port_a.p,
        h=inStream(port_a.h_outflow),
        X=inStream(port_a.Xi_outflow))
        "Fluid properties at port a for design flow direction (i.e., a->b)";

    end if;

    state_b = Medium.setState_phX(
      p=Medium.p_default,
      h=Medium.specificEnthalpy_pTX(
        p=Medium.p_default,
        T=Medium.T_default,
        X=Medium.X_default),
      X=Medium.X_default)
      "Fluid properties at port b for design flow direction (i.e., a->b):
      Not needed, so set dummy values";
    state_a_ad = state_b
      "Fluid properties at port a for reverse design flow direction (i.e., a<-b): 
      Not needed, so set dummy values";
    state_b_ad = state_b
      "Fluid properties at port b for reverse design flow direction (i.e., a<-b): 
      Not needed, so set dummy values";

  elseif positionFluidProperties==
    SorpLib.Choices.ResistorFluidProperties.PortBInlet then
    if instreamingPropertiesByInput then
      state_b_ad = Medium.setState_phX(
        p=Medium.p_default,
        h=Medium.specificEnthalpy_pTX(
          p=Medium.p_default,
          T=Medium.T_default,
          X=Medium.X_default),
        X=Medium.X_default)
       "Fluid properties at port b for reverse design flow direction (i.e., a<-b): 
        Not needed, so set dummy values";

    else
      state_b_ad = Medium.setState_phX(
        p=port_b.p,
        h=inStream(port_b.h_outflow),
        X=inStream(port_b.Xi_outflow))
       "Fluid properties at port b for reverse design flow direction (i.e., a<-b)";

    end if;

    state_a = Medium.setState_phX(
      p=Medium.p_default,
      h=Medium.specificEnthalpy_pTX(
        p=Medium.p_default,
        T=Medium.T_default,
        X=Medium.X_default),
      X=Medium.X_default)
      "Fluid properties at port a for design flow direction (i.e., a->b):
      Not needed, so set dummy values";
    state_b = state_a
      "Fluid properties at port b for design flow direction (i.e., a->b):
      Not needed, so set dummy values";
    state_a_ad = state_a
      "Fluid properties at port a for reverse design flow direction (i.e., a<-b): 
      Not needed, so set dummy values";

  elseif positionFluidProperties==
    SorpLib.Choices.ResistorFluidProperties.ActualInlet or
    positionFluidProperties==
    SorpLib.Choices.ResistorFluidProperties.AverageInstreaming then
    if instreamingPropertiesByInput then
      state_a = Medium.setState_phX(
        p=Medium.p_default,
        h=Medium.specificEnthalpy_pTX(
          p=Medium.p_default,
          T=Medium.T_default,
          X=Medium.X_default),
        X=Medium.X_default)
        "Fluid properties at port a for design flow direction (i.e., a->b):
        Not needed, so set dummy values";
      state_b_ad = state_a
       "Fluid properties at port b for reverse design flow direction (i.e., a<-b): 
        Not needed, so set dummy values";

    else
      state_a = Medium.setState_phX(
        p=port_a.p,
        h=inStream(port_a.h_outflow),
        X=inStream(port_a.Xi_outflow))
        "Fluid properties at port a for design flow direction (i.e., a->b)";
      state_b_ad = Medium.setState_phX(
        p=port_b.p,
        h=inStream(port_b.h_outflow),
        X=inStream(port_b.Xi_outflow))
       "Fluid properties at port b for reverse design flow direction (i.e., a<-b)";

    end if;

    state_b = Medium.setState_phX(
      p=Medium.p_default,
      h=Medium.specificEnthalpy_pTX(
        p=Medium.p_default,
        T=Medium.T_default,
        X=Medium.X_default),
      X=Medium.X_default)
      "Fluid properties at port b for design flow direction (i.e., a->b):
      Not needed, so set dummy values";
    state_a_ad = state_b
      "Fluid properties at port a for reverse design flow direction (i.e., a<-b): 
      Not needed, so set dummy values";

  else
    if instreamingPropertiesByInput then
      state_a = Medium.setState_phX(
        p=Medium.p_default,
        h=Medium.specificEnthalpy_pTX(
          p=Medium.p_default,
          T=Medium.T_default,
          X=Medium.X_default),
        X=Medium.X_default)
        "Fluid properties at port a for design flow direction (i.e., a->b):
        Not needed, so set dummy values";
      state_b_ad = state_a
       "Fluid properties at port b for reverse design flow direction (i.e., a<-b): 
        Not needed, so set dummy values";

    else
      state_a = Medium.setState_phX(
        p=port_a.p,
        h=inStream(port_a.h_outflow),
        X=inStream(port_a.Xi_outflow))
        "Fluid properties at port a for design flow direction (i.e., a->b)";
      state_b_ad = Medium.setState_phX(
        p=port_b.p,
        h=inStream(port_b.h_outflow),
        X=inStream(port_b.Xi_outflow))
       "Fluid properties at port b for reverse design flow direction (i.e., a<-b)";

    end if;

    state_b = Medium.setState_phX(
      p=port_b.p,
      h=inStream(port_a.h_outflow),
      X=inStream(port_a.Xi_outflow))
      "Fluid properties at port b for design flow direction (i.e., a->b):
      Not needed, so set dummy values";
    state_a_ad = Medium.setState_phX(
      p=port_a.p,
      h=inStream(port_b.h_outflow),
      X=inStream(port_b.Xi_outflow))
      "Fluid properties at port a for reverse design flow direction (i.e., a<-b): 
      Not needed, so set dummy values";

  end if;

  //
  // Pass and calculate further state properties
  //
  if positionFluidProperties==
     SorpLib.Choices.ResistorFluidProperties.Constant then
    fluidProperties_a.p = p_ref
      "Pressure at port a for design flow direction (i.e., a->b)";
    fluidProperties_a.T = T_ref
      "Temperature at port a for design flow direction (i.e., a->b)";
    fluidProperties_a.rho = rho_ref
      "Density at port a for design flow direction (i.e., a->b)";
    fluidProperties_a.eta = eta_ref
      "Dynamic viscosity at port a for design flow direction (i.e., a->b)";

    fluidProperties_b.p = p_ref
      "Pressure at port b for design flow direction (i.e., a->b)";
    fluidProperties_b.T = T_ref
      "Temperature at port b for design flow direction (i.e., a->b)";
    fluidProperties_b.rho = rho_ref
      "Density at port b for design flow direction (i.e., a->b)";
    fluidProperties_b.eta = eta_ref
      "Dynamic viscosity at port b for design flow direction (i.e., a->b)";

    fluidProperties_a_ad.p = p_ref
      "Pressure at port a for reverse design flow direction (i.e., a<-b)";
    fluidProperties_a_ad.T = T_ref
      "Temperature at port a for reverse design flow direction (i.e., a<-b)";
    fluidProperties_a_ad.rho = rho_ref
      "Density at port a for reverse design flow direction (i.e., a<-b)";
    fluidProperties_a_ad.eta = eta_ref
      "Dynamic viscosity at port a for reverse design flow direction (i.e., a<-b)";

    fluidProperties_b_ad.p = p_ref
      "Pressure at port b for reverse design flow direction (i.e., a<-b)";
    fluidProperties_b_ad.T = T_ref
      "Temperature at port b for reverse design flow direction (i.e., a<-b)";
    fluidProperties_b_ad.rho = rho_ref
      "Density at port b for reverse design flow direction (i.e., a<-b)";
    fluidProperties_b_ad.eta = eta_ref
      "Dynamic viscosity at port b for reverse design flow direction (i.e., a<-b)";

  else
    fluidProperties_a.p = if not instreamingPropertiesByInput then
      port_a.p else p_a_internal
      "Pressure at port a for design flow direction (i.e., a->b)";
    fluidProperties_a.T = if not instreamingPropertiesByInput then
      Medium.temperature(state=state_a) else T_a_internal
      "Temperature at port a for design flow direction (i.e., a->b)";
    fluidProperties_a.rho = if not instreamingPropertiesByInput then
      Medium.density(state=state_a) else rho_a_internal
      "Density at port a for design flow direction (i.e., a->b)";
    fluidProperties_a.eta = if not instreamingPropertiesByInput then
      (if pressureLossModel.requireTransportPropreties then
      Medium.dynamicViscosity(state=state_a) else eta_ref) else eta_a_internal
      "Dynamic viscosity at port a for design flow direction (i.e., a->b)";

    fluidProperties_b.p = port_b.p
      "Pressure at port b for design flow direction (i.e., a->b)";
    fluidProperties_b.T = Medium.temperature(state=state_b)
      "Temperature at port b for design flow direction (i.e., a->b)";
    fluidProperties_b.rho = Medium.density(state=state_b)
      "Density at port b for design flow direction (i.e., a->b)";
    fluidProperties_b.eta = if pressureLossModel.requireTransportPropreties then
      Medium.dynamicViscosity(state=state_b) else eta_ref
      "Dynamic viscosity at port b for design flow direction (i.e., a->b)";

    fluidProperties_a_ad.p = port_a.p
      "Pressure at port a for reverse design flow direction (i.e., a<-b)";
    fluidProperties_a_ad.T = Medium.temperature(state=state_a_ad)
      "Temperature at port a for reverse design flow direction (i.e., a<-b)";
    fluidProperties_a_ad.rho = Medium.density(state=state_a_ad)
      "Density at port a for reverse design flow direction (i.e., a<-b)";
    fluidProperties_a_ad.eta = if pressureLossModel.requireTransportPropreties then
      Medium.dynamicViscosity(state=state_a_ad) else eta_ref
      "Dynamic viscosity at port a for reverse design flow direction (i.e., a<-b)";

    fluidProperties_b_ad.p = if not instreamingPropertiesByInput then
      port_b.p else p_b_ad_internal
      "Pressure at port b for reverse design flow direction (i.e., a<-b)";
    fluidProperties_b_ad.T = if not instreamingPropertiesByInput then
      Medium.temperature(state=state_b_ad) else T_b_ad_internal
      "Temperature at port b for reverse design flow direction (i.e., a<-b)";
    fluidProperties_b_ad.rho = if not instreamingPropertiesByInput then
      Medium.density(state=state_b_ad) else rho_b_ad_internal
      "Density at port b for reverse design flow direction (i.e., a<-b)";
    fluidProperties_b_ad.eta = if not instreamingPropertiesByInput then
      (if pressureLossModel.requireTransportPropreties then
      Medium.dynamicViscosity(state=state_b_ad) else eta_ref) else eta_b_ad_internal
      "Dynamic viscosity at port b for reverse design flow direction (i.e., a<-b)";

  end if;

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
The liquid generic resistance can be used to describe dynamic, geodetic, and
fitting-based pressure losses. Friction-based pressure losses are not considered.

<h4>Main equations</h4>
<p>
The static pressure drop <i>&Delta;p<sub>static</sub></i> is calculated as a function 
of the hydraulic mass flow rate <i>m&#x307;<sub>hyd</sub> </i> and follows from the 
extended Bernoulli equation for incompressible fluids:
</p>
<pre>
    &Delta;p<sub>static</sub> = p<sub>a</sub> - p<sub>b</sub> = 1/2 * [&rho;<sub>b</sub> * w<sub>b</sub><sup>2</sup> - &rho;<sub>a</sub> * w<sub>a</sub><sup>2</sup>] + g * [&rho;<sub>b</sub> * z<sub>b</sub> - &rho;<sub>a</sub> * z<sub>a</sub>] + 1/2 * <SPAN STYLE=\"text-decoration:overline\">&rho;</SPAN> * <SPAN STYLE=\"text-decoration:overline\">w</SPAN><sup>2</sup> * [&sum; &zeta;<sub>i</sub> + l/ <SPAN STYLE=\"text-decoration:overline\">d</SPAN> * &lambda;];
    
    &Delta;p<sub>static</sub> = p<sub>a</sub> - p<sub>b</sub> = 1/2 * m&#x307;<sub>hyd</sub><sup>2</sup> * [1 / (&rho;<sub>b</sub> * A<sub>b</sub><sup>2</sup>) - 1 / (&rho;<sub>a</sub> * A<sub>a</sub><sup>2</sup>)] + g * [&rho;<sub>b</sub> * z<sub>b</sub> - &rho;<sub>a</sub> * z<sub>a</sub>] + 1/2 * m&#x307;<sub>hyd</sub><sup>2</sup> * 1/<SPAN STYLE=\"text-decoration:overline\">&rho;</SPAN> * 1/<SPAN STYLE=\"text-decoration:overline\">A</SPAN><sup>2</sup> * [&sum; &zeta;<sub>i</sub> + l/ <SPAN STYLE=\"text-decoration:overline\">d</SPAN> * &lambda;];
</pre>
<p>
Herein, <i>&rho;<sub>a</sub></i>/<i>&rho;<sub>b</sub></i> describes the fluid density 
at port a/b, <i>w<sub>a</sub></i>/<i>w<sub>b</sub></i> is the fluid velocity at port a/b, 
<i>z<sub>a</sub></i>/<i>z<sub>b</sub></i> is geodetic height port a/b, and <i>A<sub>a</sub></i>/<i>A<sub>b</sub></i> 
is the cross-sectional area at port a/b. The variable <i> <SPAN STYLE=\"text-decoration:overline\">&rho;</SPAN></i> 
is the average fluid density, <i> <SPAN STYLE=\"text-decoration:overline\">A</SPAN></i> is 
the average cross-sectional area, <i><SPAN STYLE=\"text-decoration:overline\">d</SPAN></i> 
is the average diameter, <i> <SPAN STYLE=\"text-decoration:overline\">w</SPAN></i> is the
average fluid velocity, <i>l</i> is the length, and <i>&lambda;</i> is the Darcy friction factor.
<br/><br/>
The used fluid properties can be selected via the option <i>positionFluidProperties</i>, 
and it is possible to choose constant fluid properties or fluid properties at the current 
flow inlet. Note that the equation of the static pressure loss is implemented both for 
the design direction (a->b) and reverse the design direction (b->a) to enable flow reversal
 via the numerically robust functions
<a href=\"Modelica://SorpLib.Numerics.regStep\">SorpLib.Numerics.regStep</a> and
<a href=\"Modelica://SorpLib.Numerics.regSquareWFactors\">SorpLib.Numerics.regSquareWFactors</a>.
</p>

<h4>Friction-based pressure loss</h4>
<p>
This model assumes zero friction, so it does not calculate friction-based
pressure losses:
</p>
<pre>
    &lambda; = 0;
</pre>

<h4>Assumptions and limitations</h4>
<ul>
  <li>
  Steady-state flow regime
  </li>
  <li>
  Incompressible fluid (i.e., may not be applicable for gases/gas mixtures)
  </li>
  <li>
  Applies along a streamline and not for an entire flow field
  </li>
  <li>
  Adiabatic process
  </li>
  <li>
  Isenthalpic process
  </li>
  <li>
  No storage of mass or energy
  </li>
</ul>

<h4>Typical use</h4>
<p>
The generic resistance is typically used to described pressure losses caused
by fittings, e.g., sliders, bends, or changes in cross-section.
</p>


<h4>Important parameters and options</h4>
<ul>
  <li>
  <i>positionFluidProperties</i>:
  Defines the position of the fluid property calculation used to calculate
  pressure losses.
  </li>
  <li>
  <i>instreamingPropertiesByInput</i>:
  Defines if the instreaming fluid properties are provided via an input, so
  they do not need to be calculated internally.
  </li>
  <li>
  <i>dpFromMFlow</i>:
  Defines if static pressure loss is calculated from hydraulic mass flow rate
  or vice versa.
  </li>
  <li>
  <i>dynamicPressureLoss</i>:
  Defines if dynamic pressure losses are considered.
  </li>
  <li>
  <i>geodeticPressureLoss</i>:
  Defines if geodetic pressure losses are considered.
  </li>
  <li>
  <i>fittingPressureLosss</i>:
  Defines if fitting-based pressure losses are considered.
  </li>
  <br/>
  <li>
  <i>flowDirectionDependentZeta</i>:
  Defines if the fitting-caused pressure loss is dependent on the flow direction.
  </li>
  <li>
  <i>geometry</i>:
  Defines the resistance geometry required to calculate pressure losses.
  </li>
  <br/>
  <li>
  <i>avoid_events</i>:
  Defines if events shall be avoided via the noEvent()-operator.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  December 22, 2023, by Mirko Engelpracht:<br/>
  First implementation
  </li>
</ul>
</html>"));
end LiquidGenericResistance;
