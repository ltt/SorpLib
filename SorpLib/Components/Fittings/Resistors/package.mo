within SorpLib.Components.Fittings;
package Resistors "Package containing hydraulic resistors"
  extends Modelica.Icons.VariantsPackage;

  annotation (Documentation(info="<html>
<p>
This package contains hydraulic resistors based on the Modelica Standard library 
(MSL). Hydraulic resistors can be used to calculate pressure losses as a function 
of mass flow rate and vice versa. These resistors can be used on their own, to 
represent pressure losses caused by fittings. Alternatively,  these resistors can 
be used in components such as pipes or open adsorbers to calculate pressure losses 
due to friction, geostatic height differences, or cross-section changes (i.e., 
velocity changes).
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructering the library.
  </li>
</ul>
</html>"));
end Resistors;
