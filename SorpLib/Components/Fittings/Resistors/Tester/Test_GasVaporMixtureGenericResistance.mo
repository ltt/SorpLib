within SorpLib.Components.Fittings.Resistors.Tester;
model Test_GasVaporMixtureGenericResistance
  "Tester for the gas generic resistance"
  extends Modelica.Icons.Example;

  //
  // Definition of parameters regarding the medium
  //
  replaceable package Medium =
    SorpLib.Media.IdealGasVaporMixtures.MoistAir_N2_O2_CO2_H2O
    constrainedby Modelica.Media.Interfaces.PartialMedium
    "Medium model of the ideal gas, ideal gas mixture, or ideal gas-vapor mixture"
    annotation (Dialog(tab="General", group="Medium"),
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

  //
  // Definition of boundary models
  //
  SorpLib.Basics.Sources.Fluids.GasVaporMixtureSource fs_a(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    use_mFlowInput=true,
    T_fixed=288.15,
    redeclare final package Medium = Medium)
    "Fluid source a"
    annotation (Placement(transformation(extent={{-70,-10},{-50,10}})));
  SorpLib.Basics.Sources.Fluids.GasVaporMixtureSource fs_b(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    use_pInput=true,
    T_fixed=323.15,
    redeclare final package Medium = Medium)
    "Fluid source b"
    annotation (Placement(transformation(extent={{70,-10},{50,10}})));

  //
  // Definition of models
  //
  SorpLib.Components.Fittings.Resistors.GasGenericResistance resistance(
    positionFluidProperties=SorpLib.Choices.ResistorFluidProperties.ActualInlet,
    dpFromMFlow=true,
    dynamicPressureLoss=false,
    geodeticPressureLoss=false,
    instreamingPropertiesByInput=false,
    flowDirectionDependentZeta=true,
    zeta_b={0.1,0.1,0.1},
    geometry(d_hyd_a=0.15, z_a=9),
    zeta_a={0.1,0.2,0.4},
    m_flow_start=-1,
    redeclare final package Medium = Medium)
    "Resistance model without volumes at inlet and outlet"
    annotation (Placement(transformation(extent={{-10,-10},{10,10}})));

  //
  // Definition of input signals
  //
  Modelica.Blocks.Sources.Sine input_m_flow(
    amplitude=1,
    f=1/500,
    phase=1.5707963267949,
    offset=0)  "Input signal for mass flow rate"
    annotation (Placement(transformation(extent={{-100,-10},{-80,10}})));
  Modelica.Blocks.Sources.Ramp input_p(
    height=5e5,
    duration=2500,
    offset=5e5) "Input signal for pressure"
    annotation (Placement(transformation(extent={{100,-10},{80,10}})));

equation
  //
  // Connections
  //
  connect(fs_a.port, resistance.port_a) annotation (Line(
      points={{-60,0},{-8,0}},
      color={244,125,35},
      thickness=1));
  connect(resistance.port_b, fs_b.port) annotation (Line(
      points={{8,0},{60,0}},
      color={244,125,35},
      thickness=1));

  connect(input_m_flow.y, fs_a.m_flow_input) annotation (Line(points={{-79,0},{
          -70,0},{-70,2},{-61.2,2}},
                                   color={0,0,127}));
  connect(input_p.y, fs_b.p_input) annotation (Line(points={{79,0},{70,0},{70,5},
          {61.2,5}},  color={0,0,127}));

  //
  // Annotations
  //
  annotation (experiment(StopTime=2500), Documentation(info="<html>
<p>
This model checks the gas generic resistance model using an ideal gas-vapor 
mixture.
<br/><br/>
To see the model behavior, plot the variables of the models over the 
time. The simulation time is correctly preset (Start: 0 s, Stop = 2500 s). 
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Test_GasVaporMixtureGenericResistance;
