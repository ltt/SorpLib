within SorpLib.Components.Fittings.Resistors.Tester;
model Test_LiquidGenericResistance
  "Tester for the liquid generic resistance"
  extends Modelica.Icons.Example;

  //
  // Definition of parameters regarding the medium
  //
  replaceable package Medium = Modelica.Media.Water.ConstantPropertyLiquidWater
    constrainedby Modelica.Media.Interfaces.PartialMedium
    "Medium model of the (ideal) liquid"
    annotation (Dialog(tab="General", group="Medium"),
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

  //
  // Definition of boundary models
  //
  SorpLib.Basics.Sources.Fluids.LiquidSource fs_a(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    use_mFlowInput=true,
    T_fixed=288.15,
    redeclare final package Medium = Medium)
    "Fluid source a"
    annotation (Placement(transformation(extent={{-70,20},{-50,40}})));
  SorpLib.Basics.Sources.Fluids.LiquidSource fs_b(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    use_pInput=true,
    T_fixed=323.15,
    redeclare final package Medium = Medium)
    "Fluid source b"
    annotation (Placement(transformation(extent={{70,20},{50,40}})));

  SorpLib.Basics.Sources.Fluids.LiquidSource fs_a_wVolume(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    use_mFlowInput=true,
    T_fixed=288.15,
    redeclare final package Medium = Medium)
    "Fluid source a"
    annotation (Placement(transformation(extent={{-70,-40},{-50,-20}})));
  SorpLib.Basics.Sources.Fluids.LiquidSource fs_b_wVolume(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    use_pInput=true,
    T_fixed=323.15,
    redeclare final package Medium = Medium)
    "Fluid source b"
    annotation (Placement(transformation(extent={{70,-40},{50,-20}})));

  //
  // Definition of models
  //
  SorpLib.Components.Fittings.Resistors.LiquidGenericResistance resistance(
    positionFluidProperties=SorpLib.Choices.ResistorFluidProperties.ActualInlet,
    dynamicPressureLoss=true,
    geodeticPressureLoss=true,
    instreamingPropertiesByInput=false,
    flowDirectionDependentZeta=true,
    zeta_b={0.1,0.1,0.1},
    geometry(
      d_hyd_b=0.008,
      z_a=9,
      z_b=10),
    zeta_a={0.1,0.2,0.4},
    m_flow_start=-1,
    redeclare final package Medium = Medium)
    "Resistance model without volumes at inlet and outlet"
    annotation (Placement(transformation(extent={{-10,20},{10,40}})));

  SorpLib.Components.Fittings.Resistors.LiquidGenericResistance
    resistance_wVolume(
    positionFluidProperties=SorpLib.Choices.ResistorFluidProperties.ActualInlet,
    dynamicPressureLoss=true,
    geodeticPressureLoss=true,
    flowDirectionDependentZeta=true,
    zeta_b={0.1,0.1,0.1},
    geometry(
      d_hyd_b=0.008,
      z_a=9,
      z_b=10),
    zeta_a={0.1,0.2,0.4},
    m_flow_start=-1,
    redeclare final package Medium = Medium)
    "Resistance model without volumes at inlet and outlet"
    annotation (Placement(transformation(extent={{-10,-40},{10,-20}})));

  SorpLib.Basics.Volumes.FluidVolumes.LiquidVolume volume_a(
    p_initial=600000,
    T_initial=288.15,
    mc_flow_initialX=-1,
    useHeatPorts=false,
    useHeatPortsY=false,
    type_overallMassBalance=SorpLib.Choices.BalanceEquations.TransientFreeInitial,
                                                            nPorts_cfp_xMinus=1,
      nPorts_cfp_xPlus=1,
    redeclare final package Medium = Medium) "Volume at port a"
    annotation (Placement(transformation(extent={{-40,-40},{-20,-20}})));
  SorpLib.Basics.Volumes.FluidVolumes.LiquidVolume volume_b(
    p_initial=500000,
    T_initial=288.15,
    mc_flow_initialX=-1,
    useHeatPorts=false,
    useHeatPortsY=false,
    type_overallMassBalance=SorpLib.Choices.BalanceEquations.TransientFreeInitial,
                                                            nPorts_cfp_xMinus=1,
      nPorts_cfp_xPlus=1,
    redeclare final package Medium = Medium) "Volume at port b"
    annotation (Placement(transformation(extent={{20,-40},{40,-20}})));

  //
  // Definition of input signals
  //
  Modelica.Blocks.Sources.Sine input_m_flow(
    amplitude=1,
    f=1/500,
    phase=1.5707963267949,
    offset=0)  "Input signal for mass flow rate"
    annotation (Placement(transformation(extent={{-100,-10},{-80,10}})));
  Modelica.Blocks.Sources.Ramp input_p(
    height=5e5,
    duration=2500,
    offset=5e5) "Input signal for pressure"
    annotation (Placement(transformation(extent={{100,-10},{80,10}})));

equation
  //
  // Connections
  //
  connect(fs_a.port, resistance.port_a) annotation (Line(
      points={{-60,30},{-8,30}},
      color={28,108,200},
      thickness=1));
  connect(resistance.port_b, fs_b.port) annotation (Line(
      points={{8,30},{60,30}},
      color={28,108,200},
      thickness=1));
  connect(fs_a_wVolume.port, volume_a.cfp_xMinus[1]) annotation (Line(
      points={{-60,-30},{-50,-30},{-50,-28.2},{-34.2,-28.2}},
      color={28,108,200},
      thickness=1));
  connect(volume_a.cfp_xPlus[1], resistance_wVolume.port_a) annotation (Line(
      points={{-22.2,-28.2},{-16,-28.2},{-16,-30},{-8,-30}},
      color={28,108,200},
      thickness=1));
  connect(resistance_wVolume.port_b, volume_b.cfp_xMinus[1]) annotation (Line(
      points={{8,-30},{16,-30},{16,-28.2},{25.8,-28.2}},
      color={28,108,200},
      thickness=1));
  connect(volume_b.cfp_xPlus[1], fs_b_wVolume.port) annotation (Line(
      points={{37.8,-28.2},{50,-28.2},{50,-30},{60,-30}},
      color={28,108,200},
      thickness=1));

  connect(input_m_flow.y, fs_a.m_flow_input) annotation (Line(points={{-79,0},{-70,
          0},{-70,32},{-61.2,32}}, color={0,0,127}));
  connect(input_m_flow.y, fs_a_wVolume.m_flow_input) annotation (Line(points={{-79,
          0},{-70,0},{-70,-28},{-61.2,-28}}, color={0,0,127}));
  connect(input_p.y, fs_b.p_input) annotation (Line(points={{79,0},{70,0},{70,35},
          {61.2,35}}, color={0,0,127}));
  connect(input_p.y, fs_b_wVolume.p_input) annotation (Line(points={{79,0},{70,0},
          {70,-25},{61.2,-25}}, color={0,0,127}));

  //
  // Annotations
  //
  annotation (experiment(StopTime=2500), Documentation(info="<html>
<p>
This model checks the liquid generic resistance model.
<br/><br/>
To see the model behavior, plot the variables of the models over the 
time. The simulation time is correctly preset (Start: 0 s, Stop = 2500 s). 
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Test_LiquidGenericResistance;
