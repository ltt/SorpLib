within SorpLib.Components.Fittings.Resistors.Tester;
model Test_VLETubeResistance "Tester for the VLE tube resistance"
  extends Modelica.Icons.Example;

  //
  // Definition of parameters regarding the medium
  //
  replaceable package Medium =
    Modelica.Media.Water.StandardWater
    constrainedby Modelica.Media.Interfaces.PartialTwoPhaseMedium
    "Medium model of the real fluid (i.e., with two-phase regime)"
    annotation (Dialog(tab="General", group="Medium"),
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

  replaceable model PressureLossModel =
    SorpLib.Components.Fittings.PressureLossCorrelations.TubeInside.Colebrook
    constrainedby
    SorpLib.Components.Fittings.BaseClasses.PartialTubeInsidePressureLoss
    "Pressure loss model"
    annotation (Dialog(tab="General", group="Models"),
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

  //
  // Definition of boundary models
  //
  SorpLib.Basics.Sources.Fluids.VLESource fs_a(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    use_mFlowInput=true,
    T_fixed=288.15,
    redeclare final package Medium = Medium)
    "Fluid source a"
    annotation (Placement(transformation(extent={{-70,-10},{-50,10}})));
  SorpLib.Basics.Sources.Fluids.VLESource fs_b(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    use_pInput=true,
    T_fixed=323.15,
    redeclare final package Medium = Medium)
    "Fluid source b"
    annotation (Placement(transformation(extent={{70,-10},{50,10}})));

  //
  // Definition of models
  //
  SorpLib.Components.Fittings.Resistors.VLETubeResistance resistance(
    positionFluidProperties=SorpLib.Choices.ResistorFluidProperties.ActualInlet,
    dynamicPressureLoss=true,
    geodeticPressureLoss=true,
    frictionPressureLoss=true,
    instreamingPropertiesByInput=false,
    redeclare final model PressureLossModel = PressureLossModel,
    flowDirectionDependentZeta=true,
    zeta_b={0.1,0.1,0.1},
    geometry(
      d_hyd_b=0.008,
      z_a=9,
      z_b=10),
    zeta_a={0.1,0.2,0.4},
    m_flow_start=-1,
    redeclare final package Medium = Medium)
    "Resistance model without volumes at inlet and outlet"
    annotation (Placement(transformation(extent={{-10,-10},{10,10}})));

  //
  // Definition of input signals
  //
  Modelica.Blocks.Sources.Sine input_m_flow(
    amplitude=1,
    f=1/500,
    phase=1.5707963267949,
    offset=0)  "Input signal for mass flow rate"
    annotation (Placement(transformation(extent={{-100,-10},{-80,10}})));
  Modelica.Blocks.Sources.Ramp input_p(
    height=5e5,
    duration=2500,
    offset=5e5) "Input signal for pressure"
    annotation (Placement(transformation(extent={{100,-10},{80,10}})));

equation
  //
  // Connections
  //
  connect(fs_a.port, resistance.port_a) annotation (Line(
      points={{-60,0},{-8,0}},
      color={0,140,72},
      thickness=1));
  connect(resistance.port_b, fs_b.port) annotation (Line(
      points={{8,0},{60,0}},
      color={0,140,72},
      thickness=1));

  connect(input_m_flow.y, fs_a.m_flow_input) annotation (Line(points={{-79,0},{
          -70,0},{-70,2},{-61.2,2}},
                                   color={0,0,127}));
  connect(input_p.y, fs_b.p_input) annotation (Line(points={{79,0},{70,0},{70,5},
          {61.2,5}},  color={0,0,127}));

  //
  // Annotations
  //
  annotation (experiment(StopTime=2500), Documentation(info="<html>
<p>
This model checks the VLE tube resistance model.
<br/><br/>
To see the model behavior, plot the variables of the models over the 
time. The simulation time is correctly preset (Start: 0 s, Stop = 2500 s). 
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Test_VLETubeResistance;
