within SorpLib.Components.Fittings.Records;
record GeometryOpenAdsorber
  "This record contains the geometry of resistors for open adsorber"
  extends SorpLib.Components.Fittings.Records.GeometryGenericReistance;

  //
  // Definition of parameters regarding the geometry
  //
  parameter Modelica.Units.SI.Diameter d_particle = 0.7 / 1000
    "Diameter of the adsorbent particle"
    annotation (Dialog(tab="General", group="Geometry"));
  parameter Real psi(unit="1") = 0.32
    "Void fraction of the open adsorber (i.e., 1 for free adsorber)"
    annotation (Dialog(tab="General", group="Geometry"));
  parameter Integer no_hydraulicParallelFlows = 1
    "Number of hydraulically parallel flows (e.g., for heat exchanger tubes)"
    annotation (Dialog(tab="General", group="Geometry",
                enable=dynamicPressureLoss or frictionPressureLoss or
                  fittingPressureLosss));

  //
  // Annotations
  //
    annotation (Documentation(info="<html>
<p>
This record contains geometric parameters required by hydraulic resistors for
open adsorbers.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end GeometryOpenAdsorber;
