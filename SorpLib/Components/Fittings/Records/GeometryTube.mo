within SorpLib.Components.Fittings.Records;
record GeometryTube
  "This record contains the geometry of resistors for tubes"
  extends SorpLib.Components.Fittings.Records.GeometryGenericReistance;

  //
  // Definition of parameters regarding the geometry
  //
  parameter Modelica.Units.SI.Length roughness = 7.5e-7
    "Absolute roughness of tubes"
    annotation (Dialog(tab="General", group="Geometry",
                enable=frictionPressureLoss));
  parameter Integer no_hydraulicParallelFlows = 1
    "Number of hydraulically parallel flows (e.g., for heat exchanger tubes)"
    annotation (Dialog(tab="General", group="Geometry",
                enable=dynamicPressureLoss or frictionPressureLoss or
                  fittingPressureLosss));

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This record contains geometric parameters required by hydraulic resistors for
tubes.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end GeometryTube;
