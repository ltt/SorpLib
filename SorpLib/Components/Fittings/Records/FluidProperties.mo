within SorpLib.Components.Fittings.Records;
record FluidProperties
  "This record contains fluid properties required for resistors"
  extends Modelica.Icons.Record;

  //
  // Definition of variables
  //
  Modelica.Units.SI.Pressure p
    "Pressure";
  Modelica.Units.SI.Temperature T
    "Temperature";
  Modelica.Units.SI.Density rho
    "Density";
  Modelica.Units.SI.DynamicViscosity eta
    "Dynamic viscosity";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This record contains fluid properties required for resistances.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end FluidProperties;
