within SorpLib.Components.Fittings.Records;
record GeometryGenericReistance
  "This record contains the geometry of generic resistors"
  extends Modelica.Icons.Record;

  //
  // Definition of parameters regarding the calculation setup
  //
  parameter Boolean dynamicPressureLoss = false
    " = true, if dynamic pressure loss shall be included (i.e., different
    velocities at port a and b due to change of cross-sectional areas)"
    annotation (Dialog(tab="General", group="Calculation Setup", enable=false),
                choices(checkBox=true),
                Evaluate=true);
  parameter Boolean geodeticPressureLoss = false
    " = true, if geodetic pressure loss shall be included (i.e., different
    heights at port a and b)"
    annotation (Dialog(tab="General", group="Calculation Setup", enable=false),
                choices(checkBox=true),
                Evaluate=true);
  parameter Boolean frictionPressureLoss = true
    " = true, if pressure loss due to friction shall be included"
    annotation (Dialog(tab="General", group="Calculation Setup", enable=false),
                choices(checkBox=true),
                Evaluate=true);
  parameter Boolean fittingPressureLosss = false
    " = true, if pressure losses due to fittings shall be included"
    annotation (Dialog(tab="General", group="Calculation Setup", enable=false),
                choices(checkBox=true),
                Evaluate=true);

  //
  // Definition of parameters regarding the geometry
  //
  parameter Modelica.Units.SI.Length l = 1
    "Length (i.e., distance between port a and b)"
    annotation (Dialog(tab="General", group="Geometry",
                enable=frictionPressureLoss));
  parameter Modelica.Units.SI.Diameter d_hyd_a = 0.01
    "Hydraulic diameter at port a"
    annotation (Dialog(tab="General", group="Geometry",
                enable=dynamicPressureLoss or frictionPressureLoss or
                  fittingPressureLosss));
  parameter Modelica.Units.SI.Diameter d_hyd_b = d_hyd_a
    "Hydraulic diameter at port b"
    annotation (Dialog(tab="General", group="Geometry",
                enable=dynamicPressureLoss or frictionPressureLoss or
                  fittingPressureLosss));
  parameter Modelica.Units.SI.Height z_a = 1
    "Height at port a (only relevant if geodetic pressure loss is included)"
    annotation (Dialog(tab="General", group="Geometry",
                enable=geodeticPressureLoss));
  parameter Modelica.Units.SI.Height z_b = z_a
    "Height at port b (only relevant if geodetic pressure loss is included)"
    annotation (Dialog(tab="General", group="Geometry",
                enable=geodeticPressureLoss));

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This record contains geometric parameters required by all hydraulic resistors.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end GeometryGenericReistance;
