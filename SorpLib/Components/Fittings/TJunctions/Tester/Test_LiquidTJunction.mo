within SorpLib.Components.Fittings.TJunctions.Tester;
model Test_LiquidTJunction "Tester for the liquid T-junction element"
  extends Modelica.Icons.Example;

  //
  // Definition of boundary models
  //
  SorpLib.Basics.Sources.Fluids.LiquidSource fs_a(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    use_mFlowInput=true,
    T_fixed=293.15) "
    Fluid source a"
    annotation (Placement(transformation(extent={{10,-10},{-10,10}},
        rotation=270,
        origin={0,-60})));
  SorpLib.Basics.Sources.Fluids.LiquidSource fs_b(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    p_fixed(displayUnit="bar") = 200000,
    T_fixed=303.15)
    "Fluid source b"
    annotation (Placement(transformation(extent={{-70,-10},{-50,10}})));
  SorpLib.Basics.Sources.Fluids.LiquidSource fs_c(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    use_mFlowInput=true,
    T_fixed=313.15) "Fluid source c"
    annotation (Placement(transformation(extent={{70,-10},{50,10}})));

  //
  // Definition of multi ports
  //
  SorpLib.Components.Fittings.TJunctions.LiquidTJunction TJunction(
      type_overallMassBalance=SorpLib.Choices.BalanceEquations.TransientFreeInitial)
    "T-junction element"
    annotation (Placement(transformation(extent={{-10,-10},{10,10}})));

  //
  // Definition of input signals
  //
protected
  Modelica.Blocks.Sources.Sine input_mFlow_a(
    amplitude=1,
    f=1/250,
    offset=0)
    "Input signal for mass flow rate at port a"
    annotation (Placement(transformation(extent={{10,-10},{-10,10}},
        rotation=270,
        origin={0,-80})));
  Modelica.Blocks.Sources.Sine input_mFlow_c(
    amplitude=-1,
    f=1/250,
    offset=0) "Input signal for mass flow rate at port c"
    annotation (Placement(transformation(extent={{90,-10},{70,10}})));

equation
  //
  // Connections
  //
  connect(fs_a.port, TJunction.port_a) annotation (Line(
      points={{0,-60},{0,-7.8}},
      color={28,108,200},
      thickness=1));
  connect(fs_b.port, TJunction.port_b) annotation (Line(
      points={{-60,0},{-8,0}},
      color={28,108,200},
      thickness=1));
  connect(TJunction.port_c, fs_c.port) annotation (Line(
      points={{8,0},{60,0}},
      color={28,108,200},
      thickness=1));

  connect(input_mFlow_a.y, fs_a.m_flow_input) annotation (Line(points={{0,-69},{
          0,-66},{2,-66},{2,-61.2}}, color={0,0,127}));
  connect(input_mFlow_c.y, fs_c.m_flow_input)
    annotation (Line(points={{69,0},{66,0},{66,2},{61.2,2}}, color={0,0,127}));

  //
  // Annotations
  //
  annotation (experiment(StopTime=2500), Documentation(info="<html>
<p>
This model checks the liquid T-junction element.
<br/><br/>
To see the model behavior, plot the variables of the models over the 
time. The simulation time is correctly preset (Start: 0 s, Stop = 2500 s). 
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 21, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Test_LiquidTJunction;
