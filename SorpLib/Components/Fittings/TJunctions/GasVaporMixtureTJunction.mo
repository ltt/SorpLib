within SorpLib.Components.Fittings.TJunctions;
model GasVaporMixtureTJunction "Gas-vapor mixture T-junction element"
  extends SorpLib.Components.Fittings.BaseClasses.PartialTJunction(
    redeclare final Basics.Interfaces.FluidPorts.GasPort_out port_c,
    redeclare final Basics.Interfaces.FluidPorts.GasPort_out port_b,
    redeclare final Basics.Interfaces.FluidPorts.GasPort_in port_a,
    final no_components=Medium.nX,
    final pressureNoStateVariable = Medium.singleState,
    final neglectTermVp = false,
    independentStateVariables=SorpLib.Choices.IndependentVariablesVolume.phX,
    h_initial=
      Medium.specificEnthalpy_pTX(p=p_initial, T=T_initial, X=Medium.reference_X),
    X_i_initial=Medium.reference_X,
    type_overallMassBalance=
      SorpLib.Choices.BalanceEquations.TransientFixedInitial,
    type_independentMassBalances=
      SorpLib.Choices.BalanceEquations.TransientFixedInitial,
    type_energyBalance=
      SorpLib.Choices.BalanceEquations.TransientFixedInitial);

  //
  // Definition of parameters regarding the medium
  //
  replaceable package Medium =
    SorpLib.Media.IdealGasVaporMixtures.MoistAir_N2_O2_CO2_H2O
    constrainedby
    SorpLib.Media.IdealGasVaporMixtures.Interfaces.PartialIdealGasWaterVaporMixture
    "Medium model of the ideal gas-vapor mixture"
    annotation (Dialog(tab="General", group="Medium"),
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

  //
  // Definition of parameters regarding the calculation setup
  //
  parameter Boolean idealGasVaporMixture = true
    " = true, if medium is an ideal gas-vapoor mixture and govering equations are  
    explicitly written for an ideal gas mixture (i.e., unsaturated air)"
    annotation (Dialog(tab="General", group="Calculation Setup"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult = true);

  //
  // Definition of protected variables
  //
protected
  Medium.ThermodynamicState state
    "Thermodynamic state required to calculate medium properties";

  Modelica.Units.SI.SpecificHeatCapacity cp
    "Specific heat capacity at constant pressure";
  Modelica.Media.Common.IsobaricVolumeExpansionCoefficient beta
    "Isobaric expnasion coefficient";
  Modelica.Media.Common.IsothermalCompressibility kappa
    "Isothermal compressibility";
  Modelica.Media.Common.JouleThomsonCoefficient my
    "Joule-Thomson coefficient";

equation
  //
  // Assertations
  //
  assert(pressureNoStateVariable or not (
    type_energyBalance<>SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial and
    type_overallMassBalance==SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial),
    "Steady-state mass balance combined with transient energy balance is not " +
    "sound if the fluid volume is fixed!",
    level = AssertionLevel.warning);

  if idealGasVaporMixture then
    assert(Medium.relativeHumidity(state=state) < 1.0,
      "Govering equations according to ideal gas mixture can only be applied if " +
      "dry air is not saturatred! Results may not be reasonable!",
      level = AssertionLevel.warning);

  end if;

  //
  // Calculation of properties
  //
  if independentStateVariables ==
    SorpLib.Choices.IndependentVariablesVolume.pTX then
    state = Medium.setState_pTX(p=p, T=T, X=X_i)
      "Thermodynamic state required to calculate medium properties";
    h = Medium.specificEnthalpy(state=state)
      "Specific enthalpy";

  else
    state = Medium.setState_phX(p=p, h=h, X=X_i)
      "Thermodynamic state required to calculate medium properties";
    T = Medium.temperature(state=state)
      "Temperature";

  end if;

  rho = Medium.density(state=state)
    "Density";

  for ind in 1:no_components loop
    h_i[ind] = Medium.specificEnthalpy_i(ind_component=ind, state=state)
      "Specific enthalpy of individual components";
  end for;

  cp = if ((independentStateVariables==
    SorpLib.Choices.IndependentVariablesVolume.phX and
    type_overallMassBalance<>
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial) or
    (independentStateVariables==
    SorpLib.Choices.IndependentVariablesVolume.pTX and
    type_energyBalance<>
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial) or
    (independentStateVariables==
    SorpLib.Choices.IndependentVariablesVolume.phX and
    type_energyBalance<>
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial and
    not neglectTermVp)) then
    Medium.specificHeatCapacityCp(state=state) else 0
    "Specific heat capacity";
  beta = if (type_overallMassBalance<>
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial or
    (independentStateVariables==
    SorpLib.Choices.IndependentVariablesVolume.pTX and
    type_energyBalance<>
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial) or
    (independentStateVariables==
    SorpLib.Choices.IndependentVariablesVolume.phX and
    type_energyBalance<>
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial and
    not neglectTermVp)) then
    Medium.isobaricExpansionCoefficient(state=state) else 0
    "Isobaric expnasion coefficient";
  kappa = if (type_overallMassBalance<>
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial or
    (type_energyBalance<>
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial and
    not neglectTermVp)) then
    Medium.isothermalCompressibility(state=state) else 0
    "Isothermal compressibility";
  my = if (independentStateVariables==
    SorpLib.Choices.IndependentVariablesVolume.phX and
    (type_overallMassBalance<>
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial or
    (type_energyBalance<>
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial and
    not neglectTermVp))) then
    v / cp * (beta * T - 1) else 0
    "Joule-Thomson coefficient";

  //
  // Mass balance
  //
  if type_overallMassBalance==
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial then
    dm_dtau = 0
      "Steady-state overall mass balance";

  else
    if independentStateVariables==
      SorpLib.Choices.IndependentVariablesVolume.pTX then
      dm_dtau = V * (Medium.density_derX(state=state) * der(X_i) +
        rho * (kappa *der(p) - beta * der(T)))
        "Transient overall mass balance";

    else
      if idealGasVaporMixture then
        dm_dtau = V * (rho * (kappa * der(p) - beta / cp * der(h)) +
          (Medium.density_derX(state=state) .+ rho .* beta .*
          Medium.dh_dX_pT(state=state) ./ cp) * der(X_i))
          "Transient overall mass balance";

      else
        dm_dtau = V * (rho * ((kappa - beta * my) * der(p) -
          beta / cp * der(h)) + (Medium.density_derX(state=state) .+ rho .*
          beta .* Medium.dh_dX_pT(state=state) ./ cp) * der(X_i))
          "Transient overall mass balance";

      end if;
    end if;
  end if;

  //
  // Energy balance
  //
  if type_energyBalance==
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial then
    dU_dtau = 0
      "Steady-state energy balance";

  else
    if independentStateVariables==
      SorpLib.Choices.IndependentVariablesVolume.pTX then
      if idealGasVaporMixture then
        dU_dtau = u * dm_dtau + m * ((cp - p * v * beta) * der(T) +
          (Medium.dh_dX_pT(state=state) .+ p .* v.^2 .*
          Medium.density_derX(state=state)) * der(X_i))
          "Transient energy balance";

      else
        dU_dtau = u * dm_dtau + m * (v * (p * kappa - T * beta) * der(p) +
          (cp - p * v * beta) * der(T) + (Medium.dh_dX_pT(state=state) .+ p .*
          v.^2 .* Medium.density_derX(state=state)) * der(X_i))
          "Transient energy balance";

      end if;

    else
      if idealGasVaporMixture then
        dU_dtau = u * dm_dtau + m * ((1 - p * v * beta / cp) * der(h) +
          p .* v.^2 .* (Medium.density_derX(state=state) .+ rho .*
          beta .* Medium.dh_dX_pT(state=state) ./ cp) * der(X_i))
          "Transient energy balance";

      else
        dU_dtau = u * dm_dtau + m * ((1 - p * v * beta / cp) * der(h) +
          v * (p * (kappa - beta * my) - 1) * der(p) + p .* v.^2 .*
          (Medium.density_derX(state=state) .+ rho .* beta .*
          Medium.dh_dX_pT(state=state) ./ cp) * der(X_i))
          "Transient energy balance";

      end if;
    end if;
  end if;

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This model represents a gas-vapor mixture T-junction element, applying a lumped 
modeling approach. This T-junction is used to connect three gas-vapor mixture flows, 
thereby calculating mixing properties via (transient) mass and energy balances.
</p>

<h4>Main equations</h4>
<p>
The main equations are a momentum balance and (transient) mass and energy balances.
Independent states are either the pressure <i>p</i>, temperature <i>T</i>, and mass
fractions </i>X_i</i> or the pressure <i>p</i>, specific enthalpy <i>h</i>, and mass
fractions </i>X_i</i>. The most important equations can be found in the documentation 
of the model
<a href=\"Modelica://SorpLib.Basics.Volumes.FluidVolumes.GasVaporMixtureVolume\">SorpLib.Basics.Volumes.FluidVolumes.GasVaporMixtureVolume</a>.
</p>

<h4>Assumptions and limitations</h4>
<ul>
  <li>
  Constant volume <i>V</i>
  </li>
  <li>
  Homogenoues properties within the volume
  </li>
  <li>
  Ideal gas-vapor mixture
  </li>
</ul>

<h4>Typical use</h4>
<p>
This model is typically used to connect three medium flows. In this model, the mixing
properties are calculated via (dynamic) mass and energy balances. Hence, this model acts
like a PT1-element and can be used to break algebraic loops.
</p>

<h4>Important parameters and options</h4>
<ul>
  <li>
  <i>independentStateVariables</i>:
  Defines independent state variables.
  </li>
  <li>
  <i>idealGasVaporMixture</i>:
  Defines if the medium is an ideal gas-vapor mixture and, thus, the governing  
  equations can be simplified.
  </li>
  <br/>
  <li>
  <i>type_overallMassBalance</i>:
  Defines the type of the overall mass balance.
  </li>
  <li>
  <i>type_independentMassBalances</i>:
  Defines the type of the independent mass balances.
  </li>
  <li>
  <i>type_energyBalance</i>:
  Defines the type of the energy balance.
  </li>
  <br/>
  <li>
  <i>avoid_events</i>:
  Defines if events shall be avoided via the noEvent()-operator.
  </li>
</ul>
<p>
Note that not all combinations of govering equation types are reasonable. Typically,
a transient mass balance is combined with a transient energy balance.
</p>

<h4>Dynamics</h4>
<p>
This model has three dynamic state that can be selected (see options):
</p>
<ul>
  <li>
  Pressure <i>p</i> , temperature <i>T</i>, and mass fractions <i>X_i</i> (recommended), or
  </li>
  <li>
  pressure <i>p</i>, specific enthalpy <i>h</i>, and mass fractions <i>X_i</i>.
  </li>
</ul>
<p>
Note that this model does not have the pressure <i>p</i> as dynamic state if the
fluid property model is a single state model.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 21, 2023, by Mirko Engelpracht:<br/>
  First implementation
  </li>
</ul>
</html>"));
end GasVaporMixtureTJunction;
