within SorpLib.Components.Fittings.TJunctions;
model LiquidTJunction "Liquid T-junction element"
  extends SorpLib.Components.Fittings.BaseClasses.PartialTJunction(
    redeclare final Basics.Interfaces.FluidPorts.LiquidPort_out port_c,
    redeclare final Basics.Interfaces.FluidPorts.LiquidPort_out port_b,
    redeclare final Basics.Interfaces.FluidPorts.LiquidPort_in port_a,
    final no_components=Medium.nX,
    final pressureNoStateVariable = Medium.singleState,
    final X_i_initial=ones(no_components),
    final type_independentMassBalances=
      SorpLib.Choices.BalanceEquations.TransientFixedInitial,
    independentStateVariables=SorpLib.Choices.IndependentVariablesVolume.pTX,
    h_initial=
      Medium.specificEnthalpy_pTX(p=p_initial, T=T_initial, X=Medium.reference_X),
    type_overallMassBalance=
      SorpLib.Choices.BalanceEquations.TransientFixedInitial,
    type_energyBalance=
      SorpLib.Choices.BalanceEquations.TransientFixedInitial);

  //
  // Definition of parameters regarding the medium
  //
  replaceable package Medium = Modelica.Media.Water.WaterIF97_R1pT
    constrainedby Modelica.Media.Interfaces.PartialMedium
    "Medium model of the (ideal) liquid"
    annotation (Dialog(tab="General", group="Medium"),
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

  //
  // Definition of parameters regarding the calculation setup
  //
  parameter Boolean incompressibleLiquid = Medium.singleState
    " = true, if medium is incompressible (i.e., partial derivative of specific
    volume w.r.t. pressure at constant temperature is constant) and govering
    equations are explicitly written for an incompressible fluid"
    annotation (Dialog(tab="General", group="Calculation Setup"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);

  //
  // Definition of protected variables
  //
protected
  Medium.ThermodynamicState state
    "Thermodynamic state required to calculate medium properties";

  Modelica.Units.SI.SpecificHeatCapacity cp
    "Specific heat capacity at constant pressure";
  Modelica.Media.Common.IsobaricVolumeExpansionCoefficient beta
    "Isobaric expnasion coefficient";
  Modelica.Media.Common.IsothermalCompressibility kappa
    "Isothermal compressibility";
  Modelica.Media.Common.JouleThomsonCoefficient my
    "Joule-Thomson coefficient";

equation
  //
  // Assertations
  //
  assert(no_components <= 1,
    "The liquid volume model can only handel pure fluids (i.e., with one component)!",
    level = AssertionLevel.error);

  assert(pressureNoStateVariable or not (
    type_energyBalance<>SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial and
    type_overallMassBalance==SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial),
    "Steady-state mass balance combined with transient energy balance is not " +
    "sound if the fluid volume is fixed!",
    level = AssertionLevel.warning);

  //
  // Calculation of properties
  //
  if independentStateVariables ==
    SorpLib.Choices.IndependentVariablesVolume.pTX then
    state = Medium.setState_pTX(p=p, T=T, X=Medium.reference_X)
      "Thermodynamic state required to calculate medium properties";
    h = Medium.specificEnthalpy(state=state)
      "Specific enthalpy";

  else
    state = Medium.setState_phX(p=p, h=h, X=Medium.reference_X)
      "Thermodynamic state required to calculate medium properties";
    T = Medium.temperature(state=state)
      "Temperature";

  end if;

  rho = Medium.density(state=state)
    "Density";
  h_i = fill(h, no_components)
    "Specific enthalpy of individual components: Liquid T-junction can handle
    only one component";

  cp = if ((independentStateVariables==
    SorpLib.Choices.IndependentVariablesVolume.phX and
    type_overallMassBalance<>
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial) or
    (independentStateVariables==
    SorpLib.Choices.IndependentVariablesVolume.pTX and
    type_energyBalance<>
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial) or
    (independentStateVariables==
    SorpLib.Choices.IndependentVariablesVolume.phX and
    type_energyBalance<>
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial and
    not neglectTermVp)) then
    Medium.specificHeatCapacityCp(state=state) else 0
    "Specific heat capacity";
  beta = if (type_overallMassBalance<>
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial or
    (independentStateVariables==
    SorpLib.Choices.IndependentVariablesVolume.pTX and
    type_energyBalance<>
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial) or
    (independentStateVariables==
    SorpLib.Choices.IndependentVariablesVolume.phX and
    type_energyBalance<>
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial and
    not neglectTermVp)) then
    Medium.isobaricExpansionCoefficient(state=state) else 0
    "Isobaric expnasion coefficient";
  kappa = if (not incompressibleLiquid and
    (type_overallMassBalance<>
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial or
    (type_energyBalance<>
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial and
    not neglectTermVp))) then
    Medium.isothermalCompressibility(state=state) else 0
    "Isothermal compressibility";
  my = if (independentStateVariables==
    SorpLib.Choices.IndependentVariablesVolume.phX and
    (type_overallMassBalance<>
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial or
    (type_energyBalance<>
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial and
    not neglectTermVp))) then
    v / cp * (beta * T - 1) else 0
    "Joule-Thomson coefficient";

  //
  // Mass balance
  //
  if type_overallMassBalance==
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial then
    dm_dtau = 0
      "Steady-state overall mass balance";

  else
    if independentStateVariables==
      SorpLib.Choices.IndependentVariablesVolume.pTX then
      if incompressibleLiquid then
        dm_dtau = -V * rho * beta * der(T)
          "Transient overall mass balance";

      else
        dm_dtau = V * rho * (kappa *der(p) - beta * der(T))
          "Transient overall mass balance";

      end if;

    else
      if incompressibleLiquid then
        dm_dtau = -V * rho * beta * (my * der(p) + 1 / cp * der(h))
          "Transient overall mass balance";

      else
        dm_dtau = V * rho * ((kappa - beta * my) * der(p) - beta / cp * der(h))
          "Transient overall mass balance";

      end if;
    end if;
  end if;

  //
  // Energy balance
  //
  if type_energyBalance==
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial then
    dU_dtau = 0
      "Steady-state energy balance";

  else
    if independentStateVariables==
      SorpLib.Choices.IndependentVariablesVolume.pTX then
      if neglectTermVp then
        dU_dtau = u * dm_dtau +
          m * (v * (1 - T * beta) * der(p) +
          cp * der(T))
          "Transient energy balance";

      else
        if incompressibleLiquid then
          dU_dtau = u * dm_dtau +
            m * (-v * T * beta * der(p) +
            (cp - p * v * beta) * der(T))
            "Transient energy balance";

        else
          dU_dtau = u * dm_dtau +
            m * (v * (p * kappa - T * beta) * der(p) +
            (cp - p * v * beta) * der(T))
            "Transient energy balance";

        end if;
      end if;

    else
      if neglectTermVp then
        dU_dtau = u * dm_dtau + m * der(h)
          "Transient energy balance";

      else
        if incompressibleLiquid then
          dU_dtau = u * dm_dtau +
            m * ((1 - p * v * beta / cp) * der(h) -
            v * (p * beta * my + 1) * der(p))
            "Transient energy balance";

        else
          dU_dtau = u * dm_dtau +
            m * ((1 - p * v * beta / cp) * der(h) +
            v * (p * (kappa - beta * my) - 1) * der(p))
            "Transient energy balance";

        end if;
      end if;
    end if;
  end if;

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This model represents a liquid T-junction element, applying a lumped modeling 
approach. This T-junction is used to connect three liquid flows, thereby calculating
mixing properties via (transient) mass and energy balances.
</p>

<h4>Main equations</h4>
<p>
The main equations are a momentum balance and (transient) mass and energy balances.
Independent states are either the pressure <i>p</i>, temperature <i>T</i>, and mass
fractions </i>X_i</i> or the pressure <i>p</i>, specific enthalpy <i>h</i>, and mass
fractions </i>X_i</i>. The most important equations can be found in the documentation 
of the model
<a href=\"Modelica://SorpLib.Basics.Volumes.FluidVolumes.LiquidVolume\">SorpLib.Basics.Volumes.FluidVolumes.LiquidVolume</a>.
</p>

<h4>Assumptions and limitations</h4>
<ul>
  <li>
  Constant volume <i>V</i>
  </li>
  <li>
  Homogenoues properties within the volume
  </li>
</ul>

<h4>Typical use</h4>
<p>
This model is typically used to connect three medium flows. In this model, the mixing
properties are calculated via (dynamic) mass and energy balances. Hence, this model acts
like a PT1-element and can be used to break algebraic loops.
</p>

<h4>Important parameters and options</h4>
<ul>
  <li>
  <i>independentStateVariables</i>:
  Defines independent state variables.
  </li>
  <li>
  <i>incompressibleLiquid</i>:
  Defines if the medium is incompressible and the partial derivative of specific 
  volume w.r.t. pressure at constant temperature is constant, thus leading to
  simplified governing equations.
  </li>
  <li>
  <i>neglectTermVp</i>:
  Defines if the term 'p*V' is neglected to calculate specific internal energy.
  </li>
  <br/>
  <li>
  <i>type_overallMassBalance</i>:
  Defines the type of the overall mass balance.
  </li>
  <li>
  <i>type_energyBalance</i>:
  Defines the type of the energy balance.
  </li>
  <br/>
  <li>
  <i>avoid_events</i>:
  Defines if events shall be avoided via the noEvent()-operator.
  </li>
</ul>
<p>
Note that not all combinations of govering equation types are reasonable. Typically,
a transient mass balance is combined with a transient energy balance.
</p>

<h4>Dynamics</h4>
<p>
This model has three dynamic state that can be selected (see options):
</p>
<ul>
  <li>
  Pressure <i>p</i> , temperature <i>T</i>, and mass fractions <i>X_i</i> (recommended), or
  </li>
  <li>
  pressure <i>p</i>, specific enthalpy <i>h</i>, and mass fractions <i>X_i</i>.
  </li>
</ul>
<p>
Note that this model does not have the pressure <i>p</i> as dynamic state if the
fluid property model is a single state model.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 21, 2023, by Mirko Engelpracht:<br/>
  First implementation
  </li>
</ul>
</html>"));
end LiquidTJunction;
