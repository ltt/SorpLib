within SorpLib.Components.Fittings;
package TJunctions "Package containing T-junction elements"
  extends Modelica.Icons.VariantsPackage;

  annotation (Documentation(info="<html>
<p>
This package contains T-junction elements based on the Modelica Standard library 
(MSL). These models may be used to connect three fluid flows.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end TJunctions;
