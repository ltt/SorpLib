within SorpLib.Components.Fittings.PressureLossCorrelations.OpenAdsorber;
model QuadraticLambda
  "Pressure loss model with friction contribution according to Darcy friction number that is quadratically dependent on the mass flow rate"
  extends SorpLib.Components.Fittings.BaseClasses.PartialOpenAdsorberPressureLoss(
    final requireTransportPropreties=false);

  //
  // Definition of parameters
  //
  parameter Real lambda_quadratic(final unit="s2/kg2") = 0.025
    "Darcy friction factor at mass flow rate of 1 kg/s"
    annotation (Dialog(tab="General", group="Calculation Setup"));

equation
  lambda_mean_dd = lambda_quadratic * m_flow_hyd^2
    "Average Darcy friction number for design flow direction (a->b)";
  lambda_mean_rdd = lambda_quadratic * m_flow_hyd^2
    "Average Darcy friction number for reverse design flow direction (b->a)";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
The 'QuadraticLambda' pressure loss model calculates the pressure loss of a resistance
for open adsorbers, with the friction-based pressure loss according to a Darcy friction 
number that is quadratically dependent on the mass flow rate. The model also includes 
dynamic, geodetic, and installation-related pressure losses. The pressure loss is 
calculated as a function of the mass flow rate, and there is no inverse function.
</p>

<h4>Main equations</h4>
<p>
The static pressure drop <i>&Delta;p<sub>static</sub></i> is calculated as a function 
of the hydraulic mass flow rate <i>m&#x307;<sub>hyd</sub> </i> and follows from the 
extended Bernoulli equation for incompressible fluids:
</p>
<pre>
    &Delta;p<sub>static</sub> = p<sub>a</sub> - p<sub>b</sub> = 1/2 * [&rho;<sub>b</sub> * w<sub>b</sub><sup>2</sup> - &rho;<sub>a</sub> * w<sub>a</sub><sup>2</sup>] + g * [&rho;<sub>b</sub> * z<sub>b</sub> - &rho;<sub>a</sub> * z<sub>a</sub>] + 1/2 * <SPAN STYLE=\"text-decoration:overline\">&rho;</SPAN> * <SPAN STYLE=\"text-decoration:overline\">w</SPAN><sup>2</sup> * [&sum; &zeta;<sub>i</sub> + l/ <SPAN STYLE=\"text-decoration:overline\">d</SPAN> * &lambda;];
    
    &Delta;p<sub>static</sub> = p<sub>a</sub> - p<sub>b</sub> = 1/2 * m&#x307;<sub>hyd</sub><sup>2</sup> * [1 / (&rho;<sub>b</sub> * A<sub>b</sub><sup>2</sup>) - 1 / (&rho;<sub>a</sub> * A<sub>a</sub><sup>2</sup>)] + g * [&rho;<sub>b</sub> * z<sub>b</sub> - &rho;<sub>a</sub> * z<sub>a</sub>] + 1/2 * m&#x307;<sub>hyd</sub><sup>2</sup> * 1/<SPAN STYLE=\"text-decoration:overline\">&rho;</SPAN> * 1/<SPAN STYLE=\"text-decoration:overline\">A</SPAN><sup>2</sup> * [&sum; &zeta;<sub>i</sub> + l/ <SPAN STYLE=\"text-decoration:overline\">d</SPAN> * &lambda;];
</pre>
<p>
Herein, <i>&rho;<sub>a</sub></i>/<i>&rho;<sub>b</sub></i> describes the fluid density 
at port a/b, <i>w<sub>a</sub></i>/<i>w<sub>b</sub></i> is the fluid velocity at port a/b, 
<i>z<sub>a</sub></i>/<i>z<sub>b</sub></i> is geodetic height port a/b, and <i>A<sub>a</sub></i>/<i>A<sub>b</sub></i> 
is the cross-sectional area at port a/b. The variable <i> <SPAN STYLE=\"text-decoration:overline\">&rho;</SPAN></i> 
is the average fluid density, <i> <SPAN STYLE=\"text-decoration:overline\">A</SPAN></i> is 
the average cross-sectional area, <i><SPAN STYLE=\"text-decoration:overline\">d</SPAN></i> 
is the average diameter, <i> <SPAN STYLE=\"text-decoration:overline\">w</SPAN></i> is the
average fluid velocity, <i>l</i> is the length, and <i>&lambda;</i> is the Darcy friction factor.
<br/><br/>
The used fluid properties can be selected via the option <i>positionFluidProperties</i>, 
and it is possible to choose constant fluid properties or fluid properties at the current 
flow inlet. Note that the equation of the static pressure loss is implemented both for 
the design direction (a->b) and reverse the design direction (b->a) to enable flow reversal
 via the numerically robust functions
<a href=\"Modelica://SorpLib.Numerics.regStep\">SorpLib.Numerics.regStep</a> and
<a href=\"Modelica://SorpLib.Numerics.regSquareWFactors\">SorpLib.Numerics.regSquareWFactors</a>.
</p>

<h4>Friction-based pressure loss</h4>
<p>
The Darcy friction factor <i>&lambda;</i> is defined as
</p>
<pre>
    &lambda; = &lambda;<sub>quadratic</sub> * m&#x307;<sub>hyd</sub><sup>2</sup>;
</pre>
<p>
Herein, <i>&lambda;<sub>quadratic</sub></i> is the Darcy friction number at a mass
flow rate of 1 kg/s.
</p>

<h4>Assumptions and limitations</h4>
<ul>
  <li>
  Steady-state flow regime
  </li>
  <li>
  Incompressible fluid (i.e., may not be applicable for gases/gas mixtures)
  </li>
  <li>
  Applies along a streamline and not for an entire flow field
  </li>
  <li>
  Adiabatic process
  </li>
  <li>
  Isenthalpic process
  </li>
  <li>
  No storage of mass or energy
  </li>
</ul>

<h4>Typical use</h4>
<p>
The pressure loss model is used within tubes if friction shall be included..
</p>

<h4>Important parameters and options</h4>
<ul>
  <li>
  <i>positionFluidProperties</i>:
  Defines the position of the fluid property calculation used to calculate
  pressure losses.
  </li>
  <li>
  <i>dpFromMFlow</i>:
  Defines if static pressure loss is calculated from hydraulic mass flow rate
  or vice versa.
  </li>
  <br/>
  <li>
  <i>geometry</i>:
  Defines the resistance geometry required to calculate pressure losses and
  contains parameters defining which pressure losses shall be included.
  </li>
  <li>
  <i>flowDirectionDependentZeta</i>:
  Defines if the fitting-caused pressure loss is dependent on the flow direction.
  </li>
  <br/>
  <li>
  <i>showTotalPressures</i>:
  Defines if the total pressure shall be calculated (i.e., diagnostics).
  </li>
  <li>
  <i>showVelocities</i>:
  Defines if velocities shall be calculated (i.e., diagnostics).
  </li>
  <li>
  <i>showDarcyFrictionNumber</i>:
  Defines if the Darcy friction number shall be calculated (i.e., diagnostics).
  </li>
  <li>
  <i>showIndividualPressureLosses</i>:
  Defines if individual pressure losses shall be calculated (i.e., diagnostics).
  </li>
  <br/>
  <li>
  <i>Re_transition</i>:
  Defines the transition length to change between laminar and turbulent flow
  regime.
  </li>
  <li>
  <i>noDiffTransition</i>:
  Defines how often the transition function can be differentiated.
  </li>
  <li>
  <i>avoid_events</i>:
  Defines if events shall be avoided via the noEvent()-operator.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  December 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end QuadraticLambda;
