within SorpLib.Components.Fittings.PressureLossCorrelations.OpenAdsorber;
model Kast
  "Pressure loss model with friction contribution according to Kast"
  extends SorpLib.Components.Fittings.BaseClasses.PartialOpenAdsorberPressureLoss(
     final requireTransportPropreties=true);

  //
  // Definition of parameters
  //
  parameter Real f_correction = 1
    "Correction factor to adjust pressure drop correlation"
    annotation (Dialog(tab="General", group="Calculation Setup"));
  parameter Real f_particle = 1.8
    "Particle-specific factor (i.e., 1.8 for spherical particles or 2.6 for sharp-
    edged particles)"
    annotation (Dialog(tab="General", group="Calculation Setup"));

  //
  // Definition of variables
  //
protected
  Modelica.Units.SI.Diameter d_hyd_mean_ = d_hyd_mean /
    (2/3 * geometry.psi / (1 - geometry.psi) * geometry.d_particle)
    "Corrected hydraulic average diameter";

equation
  lambda_mean_dd = f_correction * d_hyd_mean_ * (
    64 / max(Re_mean_dd, 1e-12) + f_particle / max(Re_mean_dd, 1e-12) ^ (0.1))
    "Average Darcy friction number for design flow direction (a->b)";
  lambda_mean_rdd = f_correction * d_hyd_mean_ * (
    64 / max(Re_mean_rdd, 1e-12) + f_particle / max(Re_mean_rdd, 1e-12) ^ (0.1))
    "Average Darcy friction number for reverse design flow direction (b->a)";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
The 'Kast' pressure loss model calculates the pressure loss of a resistance for open
adsorbers, with the friction-based pressure loss according to the Blasius correlation. 
The model also includes dynamic, geodetic, and installation-related pressure losses. 
The pressure loss is calculated as a function of the mass flow rate, and there is 
no inverse function.
</p>

<h4>Main equations</h4>
<p>
The static pressure drop <i>&Delta;p<sub>static</sub></i> is calculated as a function 
of the hydraulic mass flow rate <i>m&#x307;<sub>hyd</sub> </i> and follows from the 
extended Bernoulli equation for incompressible fluids:
</p>
<pre>
    &Delta;p<sub>static</sub> = p<sub>a</sub> - p<sub>b</sub> = 1/2 * [&rho;<sub>b</sub> * w<sub>b</sub><sup>2</sup> - &rho;<sub>a</sub> * w<sub>a</sub><sup>2</sup>] + g * [&rho;<sub>b</sub> * z<sub>b</sub> - &rho;<sub>a</sub> * z<sub>a</sub>] + 1/2 * <SPAN STYLE=\"text-decoration:overline\">&rho;</SPAN> * <SPAN STYLE=\"text-decoration:overline\">w</SPAN><sup>2</sup> * [&sum; &zeta;<sub>i</sub> + l/ <SPAN STYLE=\"text-decoration:overline\">d</SPAN> * &lambda;];
    
    &Delta;p<sub>static</sub> = p<sub>a</sub> - p<sub>b</sub> = 1/2 * m&#x307;<sub>hyd</sub><sup>2</sup> * [1 / (&rho;<sub>b</sub> * A<sub>b</sub><sup>2</sup>) - 1 / (&rho;<sub>a</sub> * A<sub>a</sub><sup>2</sup>)] + g * [&rho;<sub>b</sub> * z<sub>b</sub> - &rho;<sub>a</sub> * z<sub>a</sub>] + 1/2 * m&#x307;<sub>hyd</sub><sup>2</sup> * 1/<SPAN STYLE=\"text-decoration:overline\">&rho;</SPAN> * 1/<SPAN STYLE=\"text-decoration:overline\">A</SPAN><sup>2</sup> * [&sum; &zeta;<sub>i</sub> + l/ <SPAN STYLE=\"text-decoration:overline\">d</SPAN> * &lambda;];
</pre>
<p>
Herein, <i>&rho;<sub>a</sub></i>/<i>&rho;<sub>b</sub></i> describes the fluid density 
at port a/b, <i>w<sub>a</sub></i>/<i>w<sub>b</sub></i> is the fluid velocity at port a/b, 
<i>z<sub>a</sub></i>/<i>z<sub>b</sub></i> is geodetic height port a/b, and <i>A<sub>a</sub></i>/<i>A<sub>b</sub></i> 
is the cross-sectional area at port a/b. The variable <i> <SPAN STYLE=\"text-decoration:overline\">&rho;</SPAN></i> 
is the average fluid density, <i> <SPAN STYLE=\"text-decoration:overline\">A</SPAN></i> is 
the average cross-sectional area, <i><SPAN STYLE=\"text-decoration:overline\">d</SPAN></i> 
is the average diameter, <i> <SPAN STYLE=\"text-decoration:overline\">w</SPAN></i> is the
average fluid velocity, <i>l</i> is the length, and <i>&lambda;</i> is the Darcy friction factor.
<br/><br/>
The used fluid properties can be selected via the option <i>positionFluidProperties</i>, 
and it is possible to choose constant fluid properties or fluid properties at the current 
flow inlet. Note that the equation of the static pressure loss is implemented both for 
the design direction (a->b) and reverse the design direction (b->a) to enable flow reversal
 via the numerically robust functions
<a href=\"Modelica://SorpLib.Numerics.regStep\">SorpLib.Numerics.regStep</a> and
<a href=\"Modelica://SorpLib.Numerics.regSquareWFactors\">SorpLib.Numerics.regSquareWFactors</a>.
</p>

<h4>Friction-based pressure loss</h4>
<p>
The Darcy friction factor <i>&lambda;</i> is defined as
</p>
<pre>
    &lambda; = 64 / Re + f<sub>particle</sub> / Re<sup>0.1</sup>;
</pre>
<p>
Herein, <i>Re</i> is the Reynold number and <i>f<sub>particle</sub></i> is a particle-
specifc facotr (i.e., 1.8 for spherical particles or 2.6 for sharp-edged particles). 
Note that the average diameter <i><SPAN STYLE=\"text-decoration:overline\">d</SPAN></i> 
must be corrected to
</p>
<pre>
    <SPAN STYLE=\"text-decoration:overline\">d</SPAN> = 2/3 * &Psi; / (1 - &Psi;) * d<sub>particle</sub>;
</pre>
<p>
with the void fraction <i>&Psi;</i> and particle diameter <i>d<sub>particle</sub></i>.
</p>

<h4>Assumptions and limitations</h4>
<ul>
  <li>
  Steady-state flow regime
  </li>
  <li>
  Incompressible fluid (i.e., may not be applicable for gases/gas mixtures)
  </li>
  <li>
  Applies along a streamline and not for an entire flow field
  </li>
  <li>
  Adiabatic process
  </li>
  <li>
  Isenthalpic process
  </li>
  <li>
  No storage of mass or energy
  </li>
</ul>

<h4>Typical use</h4>
<p>
The pressure loss model is used within tubes if friction shall be included..
</p>

<h4>Important parameters and options</h4>
<ul>
  <li>
  <i>positionFluidProperties</i>:
  Defines the position of the fluid property calculation used to calculate
  pressure losses.
  </li>
  <li>
  <i>dpFromMFlow</i>:
  Defines if static pressure loss is calculated from hydraulic mass flow rate
  or vice versa.
  </li>
  <br/>
  <li>
  <i>geometry</i>:
  Defines the resistance geometry required to calculate pressure losses and
  contains parameters defining which pressure losses shall be included.
  </li>
  <li>
  <i>flowDirectionDependentZeta</i>:
  Defines if the fitting-caused pressure loss is dependent on the flow direction.
  </li>
  <br/>
  <li>
  <i>showTotalPressures</i>:
  Defines if the total pressure shall be calculated (i.e., diagnostics).
  </li>
  <li>
  <i>showVelocities</i>:
  Defines if velocities shall be calculated (i.e., diagnostics).
  </li>
  <li>
  <i>showDarcyFrictionNumber</i>:
  Defines if the Darcy friction number shall be calculated (i.e., diagnostics).
  </li>
  <li>
  <i>showIndividualPressureLosses</i>:
  Defines if individual pressure losses shall be calculated (i.e., diagnostics).
  </li>
  <br/>
  <li>
  <i>Re_transition</i>:
  Defines the transition length to change between laminar and turbulent flow
  regime.
  </li>
  <li>
  <i>noDiffTransition</i>:
  Defines how often the transition function can be differentiated.
  </li>
  <li>
  <i>avoid_events</i>:
  Defines if events shall be avoided via the noEvent()-operator.
  </li>
</ul>

<h4>References</h4>
<ul>
  <li>
  Kast, W. (1998). Adsorption aus der Gasphase: Ingenieurwissenschaftliche Grundlagen und technische Verfahren (in German). VCH Verlagsgesellschaft, Weinheim, Basel, Cambridge, New York. DOI:  https://doi.org/10.1002/bbpc.19900940122.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  December 22, 2023, by Mirko Engelpracht:<br/>
  Modifications due to restructering the library and documentation.
  </li>
  <li>
  January 14, 2021, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Kast;
