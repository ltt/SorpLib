within SorpLib.Components.Fittings.PressureLossCorrelations;
package OpenAdsorber "Pressure loss correlations for fluids flowing through open adsorbers"
extends Modelica.Icons.FunctionsPackage;

annotation (Documentation(info="<html>
<p>
This package contains pressure loss correlations that can be used within tubes
to calculate friction-based pressure losses:
</p>
<ul>
  <li>
  <a href=\"Modelica://SorpLib.Components.Fittings.PressureLossCorrelations.TubeInside.ZeroFriction\">ZeroFriction</a>: 
  No friction is considered.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.Fittings.PressureLossCorrelations.TubeInside.ConstantLambda\">ConstantLambda</a>: 
  Constant Darcy friction number.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.Fittings.PressureLossCorrelations.TubeInside.LinearLambda\">LinearLambda</a>: 
  Darcy friction number that is linearly dependent on the mass flow rate.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.Fittings.PressureLossCorrelations.TubeInside.QuadraticLambda\">QuadraticLambda</a>: 
  Darcy friction number that is quadratically dependent on the mass flow rate.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.Fittings.PressureLossCorrelations.TubeInside.Kast\">Kast</a>: 
  Darcy friction number according to Kast for packed beds.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end OpenAdsorber;
