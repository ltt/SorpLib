within SorpLib.Components.Fittings;
package PressureLossCorrelations "Package containing pressure loss correlations for hydraulic resistors"
extends Modelica.Icons.FunctionsPackage;

annotation (Documentation(info="<html>
<p>
This package contains pressure loss correlations that can be used within resistance
models. The pressure loss correlations calculate the pressure drop as a function
of mass flow rate. Pressure loss correlations are implemented for the following
resistance types:
</p>
<ul>
  <li>
  <a href=\"Modelica://SorpLib.Components.Fittings.PressureLossCorrelations.TubeInside\">TubeInside</a>: 
  Pressure loss correlations for fluids flowing through a tube.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.Fittings.PressureLossCorrelations.OpenAdsorber\">OpenAdsorber</a>: 
  Pressure loss correlations for fluids flowing through an open adsorber containing, 
  e.g., a packed bed.
  </li>
</ul>
<p>
Furthermore, a 'ZeroFriction' pressure loss correlation exists for generic resistance
models.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructering the library.
  </li>
</ul>
</html>"));
end PressureLossCorrelations;
