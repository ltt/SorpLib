within SorpLib.Components.Fittings.PressureLossCorrelations;
package TubeInside "Pressure loss correlations for fluids flowing through pipes"
extends Modelica.Icons.FunctionsPackage;

  annotation (Documentation(info="<html>
<p>
This package contains pressure loss correlations that can be used within tubes
to calculate friction-based pressure losses:
</p>
<ul>
  <li>
  <a href=\"Modelica://SorpLib.Components.Fittings.PressureLossCorrelations.TubeInside.ZeroFriction\">ZeroFriction</a>: 
  No friction is considered.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.Fittings.PressureLossCorrelations.TubeInside.ConstantLambda\">ConstantLambda</a>: 
  Constant Darcy friction number.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.Fittings.PressureLossCorrelations.TubeInside.LinearLambda\">LinearLambda</a>: 
  Darcy friction number that is linearly dependent on the mass flow rate.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.Fittings.PressureLossCorrelations.TubeInside.QuadraticLambda\">QuadraticLambda</a>: 
  Darcy friction number that is quadratically dependent on the mass flow rate.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.Fittings.PressureLossCorrelations.TubeInside.Blasius\">Blasius</a>: 
  Darcy friction number according to Blasius for smooth, straight tubes.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.Fittings.PressureLossCorrelations.TubeInside.Konakov\">Konakov</a>: 
  Darcy friction number according to Konakov for smooth, straight tubes.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.Fittings.PressureLossCorrelations.TubeInside.PrandtlKarman\">PrandtlKarman</a>: 
  Darcy friction number according to Prandtl and Karman for smooth, straight tubes.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.Fittings.PressureLossCorrelations.TubeInside.Colebrook\">Colebrook</a>: 
  Darcy friction number according to Colebrook for rough, straight tubes.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.Fittings.PressureLossCorrelations.TubeInside.MishraGupaSchmidt\">MishraGupaSchmidt</a>: 
  Darcy friction number according to Mishra, Gupa, and Schmidt for smooth, helix tubes.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end TubeInside;
