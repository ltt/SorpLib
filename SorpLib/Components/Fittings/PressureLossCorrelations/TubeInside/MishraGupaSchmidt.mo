within SorpLib.Components.Fittings.PressureLossCorrelations.TubeInside;
model MishraGupaSchmidt
  "Pressure loss model with friction contribution according to Mishra, Gupa, and Schmidt"
  extends SorpLib.Components.Fittings.BaseClasses.PartialTubeInsidePressureLoss(
    final requireTransportPropreties=true);

  //
  // Definition of parameters
  //
  parameter Real f_correction = 1
    "Correction factor to adjust pressure drop correlation"
    annotation (Dialog(tab="General", group="Calculation Setup"));

  parameter Modelica.Units.SI.Diameter d_avg_helix = 0.1
    "Average diameter of the helix tube (i.e., top-view projection)"
    annotation (Dialog(tab="General", group="Geometry"));
  parameter Modelica.Units.SI.Height h_helix = 0.025
    "Inclinde of the helix tube"
    annotation (Dialog(tab="General", group="Geometry"));
  final parameter Modelica.Units.SI.Diameter d_bend_avg_helix = d_avg_helix *
    (1 + (h_helix / Modelica.Constants.pi / d_avg_helix) ^ 2)
    "Average bending diameter of the helix tube"
    annotation (Dialog(tab="General", group="Geometry", enable=false));

  final parameter Modelica.Units.SI.ReynoldsNumber Re_crit = 2300 *
    (1 + 8.6 * (d_hyd_mean / d_bend_avg_helix) ^ (0.45))
    "Critical Reynolds number"
    annotation (Dialog(tab="Advanced", group="Numerics", enable=false));
  parameter Modelica.Units.SI.ReynoldsNumber Re_transition = 100
    "Transition length to change between laminar and turbulent flow regime"
    annotation (Dialog(tab="Advanced", group="Numerics"),
                Evaluate=true,
                HideResult=true);
  parameter Integer noDiffTransition = 3
    "Specification how often transition function can be differentiated"
    annotation (Dialog(tab="Advanced", group="Numerics"),
                Evaluate=true,
                HideResult=true);

  //
  // Definition of variables
  //
protected
  Real wf_dd = if avoid_events then
    SorpLib.Numerics.smoothTransition_noEvent(Re_mean_dd, 2300, Re_transition,
    noDiffTransition) else SorpLib.Numerics.smoothTransition(Re_mean_dd, 2300,
    Re_transition, noDiffTransition)
    "Weighting factor for the design direction";
  Real wf_rdd = if avoid_events then
    SorpLib.Numerics.smoothTransition_noEvent(Re_mean_rdd, 2300, Re_transition,
    noDiffTransition) else SorpLib.Numerics.smoothTransition(Re_mean_rdd, 2300,
    Re_transition, noDiffTransition)
    "Weighting factor for the reverse design direction";

equation
  lambda_mean_dd = f_correction* (wf_dd * 64 / max(Re_mean_dd, 1e-12) * (1 +
    0.033 * Modelica.Math.log10(max(Re_mean_dd * sqrt(d_hyd_mean /
    d_bend_avg_helix), 1e-12)) ^ 4) + (1-wf_dd) * 0.3164 *
    max(Re_mean_dd, 1e-12) ^ (-0.25) * (1 + 0.095 * sqrt(d_hyd_mean /
    d_bend_avg_helix) * max(Re_mean_dd, 1e-12) ^ (0.25)))
    "Average Darcy friction number for design flow direction (a->b)";
  lambda_mean_rdd = f_correction* (wf_rdd * 64 / max(Re_mean_rdd, 1e-12) * (1 +
    0.033 * Modelica.Math.log10(max(Re_mean_rdd * sqrt(d_hyd_mean /
    d_bend_avg_helix), 1e-12)) ^ 4) + (1-wf_rdd) * 0.3164 *
    max(Re_mean_rdd, 1e-12) ^ (-0.25) * (1 + 0.095 * sqrt(d_hyd_mean /
    d_bend_avg_helix) * max(Re_mean_rdd, 1e-12) ^ (0.25)))
    "Average Darcy friction number for reverse design flow direction (b->a)";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
The 'MishraGupaSchmidt' pressure loss model calculates the pressure loss of a tube 
resistance, with the friction-based pressure loss according to the Mishra, Gupa, and
Schmidt correlation. The model also includes dynamic, geodetic, and installation-related 
pressure losses. The pressure loss is calculated as a function of the mass flow rate, 
and there is no inverse function.
</p>

<h4>Main equations</h4>
<p>
The static pressure drop <i>&Delta;p<sub>static</sub></i> is calculated as a function 
of the hydraulic mass flow rate <i>m&#x307;<sub>hyd</sub> </i> and follows from the 
extended Bernoulli equation for incompressible fluids:
</p>
<pre>
    &Delta;p<sub>static</sub> = p<sub>a</sub> - p<sub>b</sub> = 1/2 * [&rho;<sub>b</sub> * w<sub>b</sub><sup>2</sup> - &rho;<sub>a</sub> * w<sub>a</sub><sup>2</sup>] + g * [&rho;<sub>b</sub> * z<sub>b</sub> - &rho;<sub>a</sub> * z<sub>a</sub>] + 1/2 * <SPAN STYLE=\"text-decoration:overline\">&rho;</SPAN> * <SPAN STYLE=\"text-decoration:overline\">w</SPAN><sup>2</sup> * [&sum; &zeta;<sub>i</sub> + l/ <SPAN STYLE=\"text-decoration:overline\">d</SPAN> * &lambda;];
    
    &Delta;p<sub>static</sub> = p<sub>a</sub> - p<sub>b</sub> = 1/2 * m&#x307;<sub>hyd</sub><sup>2</sup> * [1 / (&rho;<sub>b</sub> * A<sub>b</sub><sup>2</sup>) - 1 / (&rho;<sub>a</sub> * A<sub>a</sub><sup>2</sup>)] + g * [&rho;<sub>b</sub> * z<sub>b</sub> - &rho;<sub>a</sub> * z<sub>a</sub>] + 1/2 * m&#x307;<sub>hyd</sub><sup>2</sup> * 1/<SPAN STYLE=\"text-decoration:overline\">&rho;</SPAN> * 1/<SPAN STYLE=\"text-decoration:overline\">A</SPAN><sup>2</sup> * [&sum; &zeta;<sub>i</sub> + l/ <SPAN STYLE=\"text-decoration:overline\">d</SPAN> * &lambda;];
</pre>
<p>
Herein, <i>&rho;<sub>a</sub></i>/<i>&rho;<sub>b</sub></i> describes the fluid density 
at port a/b, <i>w<sub>a</sub></i>/<i>w<sub>b</sub></i> is the fluid velocity at port a/b, 
<i>z<sub>a</sub></i>/<i>z<sub>b</sub></i> is geodetic height port a/b, and <i>A<sub>a</sub></i>/<i>A<sub>b</sub></i> 
is the cross-sectional area at port a/b. The variable <i> <SPAN STYLE=\"text-decoration:overline\">&rho;</SPAN></i> 
is the average fluid density, <i> <SPAN STYLE=\"text-decoration:overline\">A</SPAN></i> is 
the average cross-sectional area, <i><SPAN STYLE=\"text-decoration:overline\">d</SPAN></i> 
is the average diameter, <i> <SPAN STYLE=\"text-decoration:overline\">w</SPAN></i> is the
average fluid velocity, <i>l</i> is the length, and <i>&lambda;</i> is the Darcy friction factor.
<br/><br/>
The used fluid properties can be selected via the option <i>positionFluidProperties</i>, 
and it is possible to choose constant fluid properties or fluid properties at the current 
flow inlet. Note that the equation of the static pressure loss is implemented both for 
the design direction (a->b) and reverse the design direction (b->a) to enable flow reversal
 via the numerically robust functions
<a href=\"Modelica://SorpLib.Numerics.regStep\">SorpLib.Numerics.regStep</a> and
<a href=\"Modelica://SorpLib.Numerics.regSquareWFactors\">SorpLib.Numerics.regSquareWFactors</a>.
</p>

<h4>Friction-based pressure loss</h4>
<p>
The laminar Darcy friction factor <i>&lambda;</i> is defined as
</p>
<pre>
    &lambda; = 64 / Re * [1 + 0.033 * (<strong>log</strong>(Re * <strong>sqrt</strong>(d/D)))<sup>4</sup>] <strong>for</strong> Re &le; Re<sub>crit</sub>;
</pre>
<p>
and the turbulent Darcy friction factor <i>&lambda;</i> is defined as
</p>
<pre>
    &lambda; =0.3164 / Re<sup>0.25</sup> * [1 + 0.095 * <strong>sqrt</strong>(d/D) * Re<sup>0.25</sup>] <strong>for</strong> Re<sub>crit</sub> &le; Re &le; 10<sup>5</sup>;
</pre>
<p>
Herein, <i>Re</i> is the Reynold number, <i>d</i> is the hydraulic diameter, and <i>D</i> is 
the average diameter of the helix tube:
</p>
<pre>
    D = D* * [1 + (h / &pi; / D*)<sup>2</sup>];
</pre>
<p>
Herein, <i>D*</i> is the average diameter assuming a horizontal approximation, and <i>h</i>
is the inclinde of the helix tube. Smooth transition is applied using the function
<a href=\"Modelica://SorpLib.Numerics.smoothTransition\">SorpLib.Numerics.smoothTransition</a>
to change between laminar and turbulent flow regime, with the critical Renolds number
<i>Re<sub>crit</sub></i> calculated as:
</p>
<pre>
    Re<sub>crit</sub> = 2300 * [1 + 8.6 (d / D*)<sup>0.45</sup>];
</pre>

<h4>Assumptions and limitations</h4>
<ul>
  <li>
  Steady-state flow regime
  </li>
  <li>
  Incompressible fluid (i.e., may not be applicable for gases/gas mixtures)
  </li>
  <li>
  Applies along a streamline and not for an entire flow field
  </li>
  <li>
  Adiabatic process
  </li>
  <li>
  Isenthalpic process
  </li>
  <li>
  No storage of mass or energy
  </li>
  <li>
  Smooth helix tube
  </li>
</ul>

<h4>Typical use</h4>
<p>
The pressure loss model is used within tubes if friction shall be included..
</p>

<h4>Important parameters and options</h4>
<ul>
  <li>
  <i>positionFluidProperties</i>:
  Defines the position of the fluid property calculation used to calculate
  pressure losses.
  </li>
  <li>
  <i>dpFromMFlow</i>:
  Defines if static pressure loss is calculated from hydraulic mass flow rate
  or vice versa.
  </li>
  <br/>
  <li>
  <i>geometry</i>:
  Defines the resistance geometry required to calculate pressure losses and
  contains parameters defining which pressure losses shall be included.
  </li>
  <li>
  <i>flowDirectionDependentZeta</i>:
  Defines if the fitting-caused pressure loss is dependent on the flow direction.
  </li>
  <br/>
  <li>
  <i>showTotalPressures</i>:
  Defines if the total pressure shall be calculated (i.e., diagnostics).
  </li>
  <li>
  <i>showVelocities</i>:
  Defines if velocities shall be calculated (i.e., diagnostics).
  </li>
  <li>
  <i>showDarcyFrictionNumber</i>:
  Defines if the Darcy friction number shall be calculated (i.e., diagnostics).
  </li>
  <li>
  <i>showIndividualPressureLosses</i>:
  Defines if individual pressure losses shall be calculated (i.e., diagnostics).
  </li>
  <br/>
  <li>
  <i>Re_transition</i>:
  Defines the transition length to change between laminar and turbulent flow
  regime.
  </li>
  <li>
  <i>noDiffTransition</i>:
  Defines how often the transition function can be differentiated.
  </li>
  <li>
  <i>avoid_events</i>:
  Defines if events shall be avoided via the noEvent()-operator.
  </li>
</ul>

<h4>References</h4>
<ul>
  <li>
  Kast, W., (Revised by Hermann Nirschl), and Gaddis, E.S., and Wirth, KE., and Stichlmair, J. (2010). L1 Pressure Drop in Single Phase Flow. In: VDI Heat Atlas. VDI-Buch. Springer, Berlin, Heidelberg. DOI: https://doi.org/10.1007/978-3-540-77877-6_70.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  December 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end MishraGupaSchmidt;
