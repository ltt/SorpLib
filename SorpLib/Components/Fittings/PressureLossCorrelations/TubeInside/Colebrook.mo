within SorpLib.Components.Fittings.PressureLossCorrelations.TubeInside;
model Colebrook
  "Pressure loss model with friction contribution according to Colebrook"
  extends SorpLib.Components.Fittings.BaseClasses.PartialTubeInsidePressureLoss(
    final requireTransportPropreties=true);

  //
  // Definition of parameters
  //
  parameter Real f_correction = 1
    "Correction factor to adjust pressure drop correlation"
    annotation (Dialog(tab="General", group="Calculation Setup"));

  parameter Boolean useExplicitFormulation = true
    " = true, if explicit formulation usign the Lambert W function shall be
    used; otherwise, use implicit formulation"
    annotation (Dialog(tab="Advanced", group="Numerics"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);
  parameter Boolean useExactLambertWFunction = false
    " = true, if exact iterative solution of the Lambert W function shall be
    used; otherwise, use closed form approximation"
    annotation (Dialog(tab="Advanced", group="Numerics",
                       enable=useExplicitFormulation),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);

  parameter Modelica.Units.SI.ReynoldsNumber Re_transition = 2000
    "Transition length to change between laminar and turbulent flow regime"
    annotation (Dialog(tab="Advanced", group="Numerics"),
                Evaluate=true,
                HideResult=true);
  parameter Integer noDiffTransition = 3
    "Specification how often transition function can be differentiated"
    annotation (Dialog(tab="Advanced", group="Numerics"),
                Evaluate=true,
                HideResult=true);

  //
  // Definition of variables
  //
protected
  Real lambda_mean_turb_dd
    "Average Darcy friction factor for the design direction";
  Real lambda_mean_turb_rdd
    "Average Darcy friction factor for the reverse design direction";

  Real wf_dd = if avoid_events then
    SorpLib.Numerics.smoothTransition_noEvent(Re_mean_dd, 2300, Re_transition,
    noDiffTransition) else SorpLib.Numerics.smoothTransition(Re_mean_dd, 2300,
    Re_transition, noDiffTransition)
    "Weighting factor for the design direction";
  Real wf_rdd = if avoid_events then
    SorpLib.Numerics.smoothTransition_noEvent(Re_mean_rdd, 2300, Re_transition,
    noDiffTransition) else SorpLib.Numerics.smoothTransition(Re_mean_rdd, 2300,
    Re_transition, noDiffTransition)
    "Weighting factor for the reverse design direction";

equation
  //
  // Calculate turbulent Darcy friction factor
  //
  if useExplicitFormulation then
    if useExactLambertWFunction then
      lambda_mean_turb_dd = 1 / (2 *
        Modelica.Fluid.Dissipation.Utilities.Functions.General.LambertWIter(
        log(10) * Re_mean_dd / (2 * 2.51) * 10 ^ ((geometry.roughness * Re_mean_dd) /
        (2 * 2.51 * 14.8 * d_hyd_mean))) / log(10) - (geometry.roughness * Re_mean_dd) /
        (2.51 * 14.8 * d_hyd_mean)) ^ 2
        "Average Darcy friction factor for the design direction";
      lambda_mean_turb_rdd =  1 / (2 *
        Modelica.Fluid.Dissipation.Utilities.Functions.General.LambertWIter(
        log(10) * Re_mean_rdd / (2 * 2.51) * 10 ^ ((geometry.roughness * Re_mean_rdd) /
        (2 * 2.51 * 14.8 * d_hyd_mean))) / log(10) - (geometry.roughness * Re_mean_rdd) /
        (2.51 * 14.8 * d_hyd_mean)) ^ 2
        "Average Darcy friction factor for the reverse design direction";

    else
      lambda_mean_turb_dd = 1 / (2 *
        Modelica.Fluid.Dissipation.Utilities.Functions.General.LambertW(
        log(10) * Re_mean_dd / (2 * 2.51) * 10 ^ ((geometry.roughness * Re_mean_dd) /
        (2 * 2.51 * 14.8 * d_hyd_mean))) / log(10) - (geometry.roughness * Re_mean_dd) /
        (2.51 * 14.8 * d_hyd_mean)) ^ 2
        "Average Darcy friction factor for the design direction";
      lambda_mean_turb_rdd = 1 / (2 *
        Modelica.Fluid.Dissipation.Utilities.Functions.General.LambertW(
        log(10) * Re_mean_rdd / (2 * 2.51) * 10 ^ ((geometry.roughness * Re_mean_rdd) /
        (2 * 2.51 * 14.8 * d_hyd_mean))) / log(10) - (geometry.roughness * Re_mean_rdd) /
        (2.51 * 14.8 * d_hyd_mean)) ^ 2
        "Average Darcy friction factor for the reverse design direction";

    end if;

  else
    1 / sqrt(max(lambda_mean_turb_dd, 1e-12)) = -2 * Modelica.Math.log10(
      2.51 / (max(Re_mean_dd, 1e-12) * sqrt(max(lambda_mean_turb_dd, 1e-12))) +
      geometry.roughness / d_hyd_mean / 3.71)
      "Average Darcy friction factor for the design direction";
    1 / sqrt(max(lambda_mean_turb_rdd, 1e-12)) = -2 * Modelica.Math.log10(
      2.51 / (max(Re_mean_rdd, 1e-12) * sqrt(max(lambda_mean_turb_rdd, 1e-12))) +
      geometry.roughness / d_hyd_mean / 3.71)
      "Average Darcy friction factor for the reverse design direction";

  end if;

  //
  // Calculate overall Darcy friction factor
  //
  lambda_mean_dd = f_correction* (
    wf_dd * 64 / max(Re_mean_dd, 1e-12) + (1-wf_dd) * lambda_mean_turb_dd)
    "Average Darcy friction number for design flow direction (a->b)";
  lambda_mean_rdd = f_correction* (
    wf_rdd * 64 / max(Re_mean_rdd, 1e-12) + (1-wf_rdd) * lambda_mean_turb_rdd)
    "Average Darcy friction number for reverse design flow direction (b->a)";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
The 'Colebrook' pressure loss model calculates the pressure loss of a tube resistance, 
with the friction-based pressure loss according to the Colebrook correlation. The 
model also includes dynamic, geodetic, and installation-related pressure losses. The 
pressure loss is calculated as a function of the mass flow rate, and there is no 
inverse function.
</p>

<h4>Main equations</h4>
<p>
The static pressure drop <i>&Delta;p<sub>static</sub></i> is calculated as a function 
of the hydraulic mass flow rate <i>m&#x307;<sub>hyd</sub> </i> and follows from the 
extended Bernoulli equation for incompressible fluids:
</p>
<pre>
    &Delta;p<sub>static</sub> = p<sub>a</sub> - p<sub>b</sub> = 1/2 * [&rho;<sub>b</sub> * w<sub>b</sub><sup>2</sup> - &rho;<sub>a</sub> * w<sub>a</sub><sup>2</sup>] + g * [&rho;<sub>b</sub> * z<sub>b</sub> - &rho;<sub>a</sub> * z<sub>a</sub>] + 1/2 * <SPAN STYLE=\"text-decoration:overline\">&rho;</SPAN> * <SPAN STYLE=\"text-decoration:overline\">w</SPAN><sup>2</sup> * [&sum; &zeta;<sub>i</sub> + l/ <SPAN STYLE=\"text-decoration:overline\">d</SPAN> * &lambda;];
    
    &Delta;p<sub>static</sub> = p<sub>a</sub> - p<sub>b</sub> = 1/2 * m&#x307;<sub>hyd</sub><sup>2</sup> * [1 / (&rho;<sub>b</sub> * A<sub>b</sub><sup>2</sup>) - 1 / (&rho;<sub>a</sub> * A<sub>a</sub><sup>2</sup>)] + g * [&rho;<sub>b</sub> * z<sub>b</sub> - &rho;<sub>a</sub> * z<sub>a</sub>] + 1/2 * m&#x307;<sub>hyd</sub><sup>2</sup> * 1/<SPAN STYLE=\"text-decoration:overline\">&rho;</SPAN> * 1/<SPAN STYLE=\"text-decoration:overline\">A</SPAN><sup>2</sup> * [&sum; &zeta;<sub>i</sub> + l/ <SPAN STYLE=\"text-decoration:overline\">d</SPAN> * &lambda;];
</pre>
<p>
Herein, <i>&rho;<sub>a</sub></i>/<i>&rho;<sub>b</sub></i> describes the fluid density 
at port a/b, <i>w<sub>a</sub></i>/<i>w<sub>b</sub></i> is the fluid velocity at port a/b, 
<i>z<sub>a</sub></i>/<i>z<sub>b</sub></i> is geodetic height port a/b, and <i>A<sub>a</sub></i>/<i>A<sub>b</sub></i> 
is the cross-sectional area at port a/b. The variable <i> <SPAN STYLE=\"text-decoration:overline\">&rho;</SPAN></i> 
is the average fluid density, <i> <SPAN STYLE=\"text-decoration:overline\">A</SPAN></i> is 
the average cross-sectional area, <i><SPAN STYLE=\"text-decoration:overline\">d</SPAN></i> 
is the average diameter, <i> <SPAN STYLE=\"text-decoration:overline\">w</SPAN></i> is the
average fluid velocity, <i>l</i> is the length, and <i>&lambda;</i> is the Darcy friction factor.
<br/><br/>
The used fluid properties can be selected via the option <i>positionFluidProperties</i>, 
and it is possible to choose constant fluid properties or fluid properties at the current 
flow inlet. Note that the equation of the static pressure loss is implemented both for 
the design direction (a->b) and reverse the design direction (b->a) to enable flow reversal
 via the numerically robust functions
<a href=\"Modelica://SorpLib.Numerics.regStep\">SorpLib.Numerics.regStep</a> and
<a href=\"Modelica://SorpLib.Numerics.regSquareWFactors\">SorpLib.Numerics.regSquareWFactors</a>.
</p>

<h4>Friction-based pressure loss</h4>
<p>
The laminar Darcy friction factor <i>&lambda;</i> is defined as
</p>
<pre>
    &lambda; = 64 / Re <strong>for</strong> Re &le; 2300;
</pre>
<p>
and the turbulent Darcy friction factor <i>&lambda;</i> is defined as
</p>
<pre>
    1 / <strong>sqrt</strong>(&lambda;) = -2 <strong>log</strong>(2.51 / (Re * <strong>sqrt</strong>(&lambda;)) + &epsilon; / 3.71) <strong>for</strong> 4000 &le; Re;
</pre>
<p>
Herein, <i>Re</i> is the Reynold number and <i>&epsilon;</i> is the relative
roughness. Smooth transition is applied using the function
<a href=\"Modelica://SorpLib.Numerics.smoothTransition\">SorpLib.Numerics.smoothTransition</a>
to change between laminar and turbulent flow regime.
</p>

<h4>Assumptions and limitations</h4>
<ul>
  <li>
  Steady-state flow regime
  </li>
  <li>
  Incompressible fluid (i.e., may not be applicable for gases/gas mixtures)
  </li>
  <li>
  Applies along a streamline and not for an entire flow field
  </li>
  <li>
  Adiabatic process
  </li>
  <li>
  Isenthalpic process
  </li>
  <li>
  No storage of mass or energy
  </li>
  <li>
  Rough tube
  </li>
</ul>

<h4>Typical use</h4>
<p>
The pressure loss model is used within tubes if friction shall be included..
</p>

<h4>Important parameters and options</h4>
<ul>
  <li>
  <i>positionFluidProperties</i>:
  Defines the position of the fluid property calculation used to calculate
  pressure losses.
  </li>
  <li>
  <i>dpFromMFlow</i>:
  Defines if static pressure loss is calculated from hydraulic mass flow rate
  or vice versa.
  </li>
  <br/>
  <li>
  <i>geometry</i>:
  Defines the resistance geometry required to calculate pressure losses and
  contains parameters defining which pressure losses shall be included.
  </li>
  <li>
  <i>flowDirectionDependentZeta</i>:
  Defines if the fitting-caused pressure loss is dependent on the flow direction.
  </li>
  <br/>
  <li>
  <i>showTotalPressures</i>:
  Defines if the total pressure shall be calculated (i.e., diagnostics).
  </li>
  <li>
  <i>showVelocities</i>:
  Defines if velocities shall be calculated (i.e., diagnostics).
  </li>
  <li>
  <i>showDarcyFrictionNumber</i>:
  Defines if the Darcy friction number shall be calculated (i.e., diagnostics).
  </li>
  <li>
  <i>showIndividualPressureLosses</i>:
  Defines if individual pressure losses shall be calculated (i.e., diagnostics).
  </li>
  <br/>
  <li>
  <i>useExplicitFormulation</i>:
  Defines if explicit formulation of the turbulent Darcy friction factor shall be
  used by applying the Lambert W function.
  </li>
  <li>
  <i>useExactLambertWFunction</i>:
  Defines if approximation or exact solution (i.e., iteration) of the Lambert W
  function shall be used.
  </li>
  <li>
  <i>Re_transition</i>:
  Defines the transition length to change between laminar and turbulent flow
  regime.
  </li>
  <li>
  <i>noDiffTransition</i>:
  Defines how often the transition function can be differentiated.
  </li>
  <li>
  <i>avoid_events</i>:
  Defines if events shall be avoided via the noEvent()-operator.
  </li>
</ul>

<h4>References</h4>
<ul>
  <li>
  Kast, W., (Revised by Hermann Nirschl), and Gaddis, E.S., and Wirth, KE., and Stichlmair, J. (2010). L1 Pressure Drop in Single Phase Flow. In: VDI Heat Atlas. VDI-Buch. Springer, Berlin, Heidelberg. DOI: https://doi.org/10.1007/978-3-540-77877-6_70.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  December 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Colebrook;
