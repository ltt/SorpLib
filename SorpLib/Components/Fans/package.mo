within SorpLib.Components;
package Fans "Fans to move fluids"
  extends SorpLib.Icons.FansPackage;

  annotation (Documentation(info="<html>
<p>
This package contains fans. Ready-to-use models are based on the Modelica 
Standard library (MSL). Fans are used to prescripe a mass or volume flow rate
to a hydraulic fluid.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Fans;
