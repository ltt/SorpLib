within SorpLib.Components.Fans.BaseClasses;
partial model PartialAffinityFan
  "Base model for all fans using affinity laws"
  extends SorpLib.Components.Fans.BaseClasses.PartialFan;

  //
  // Definition of paramteres describring the fan's characteristics
  //
  parameter Modelica.Units.SI.Frequency n_ref = 50
    "Nominal rotational speed"
    annotation (Dialog(tab="General", group="Fan Characteristics - Nominal point"));
  parameter Modelica.Units.SI.VolumeFlowRate V_flow_ref = 10/1000/60
    "Nominal volume flow rate"
    annotation (Dialog(tab="General", group="Fan Characteristics - Nominal point"));
  parameter Modelica.Units.SI.Efficiency eta_fan_ref = 0.4
    "Nominal fan efficiency"
    annotation (Dialog(tab="General", group="Fan Characteristics - Nominal point"));
  parameter Real f_loss = 0.3
    "Loss factor describing the fan efficiency"
    annotation (Dialog(tab="General", group="Fan Characteristics - Nominal point"));

  //
  // Definition of parameters describing the inputs
  //
  parameter Boolean use_nInput = false
    "=true, if n is defined by input; otherwise, fixed value is used"
    annotation (Dialog(tab="General",group="Inputs"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);
  parameter Modelica.Units.SI.Frequency n_fixed = 50
    "Fixed rotational speed"
    annotation (Dialog(tab="General",group="Inputs",
                enable=not use_nInput));

  //
  // Definition of connectors
  //
  Modelica.Blocks.Interfaces.RealInput n_input(final unit="1/s") if
    use_nInput
    "Input for rotational speed"
    annotation (Placement(transformation(extent={{-20,-20},{20,20}},
        rotation=90,
        origin={0,-90}),
      iconTransformation(extent={{-10,-10},{10,10}},
        rotation=90,
        origin={0,-80})));

  //
  // Definition of protected connectors
  //
protected
  Modelica.Blocks.Interfaces.RealInput n_internal(final unit="1/s")
    "Needed for connecting to conditional connector";

  //
  // Definition of variables
  //
public
  Modelica.Units.SI.Efficiency eta_fan
    "Fan efficiency";

equation
  //
  // Assertions
  //
  assert(n_internal >= 0,
         "Rotational speed cannot be negative!",
         level=AssertionLevel.error);

  //
  // Connectors
  //
  connect(n_internal, n_input);

  if not use_nInput then
    n_internal = n_fixed
      "Needed for connecting to conditional connector";
  end if;

  //
  // Mass balance
  //
  m_flow = V_flow * rho
    "Mass flow rate at port a";
  V_flow = V_flow_ref * (n_internal / n_ref)
    "Volume flow rate at port a (affinity law)";

  //
  // Energy balance
  //
  if assumeIsenthalpicFan then
    port_a.h_outflow = inStream(port_b.h_outflow)
      "Stream variable: Trivial equation since no change of energy due to
      forbidden flow revesal";
    port_b.h_outflow = inStream(port_a.h_outflow) + (1/eta_fan - 1) * dp / rho
      "Increase of specific enthalpy due to internal losses of the fan";

  else
    port_a.h_outflow = inStream(port_b.h_outflow)
      "Stream variable: Trivial equation since no change of energy";
    port_b.h_outflow = inStream(port_a.h_outflow)
      "Stream variable: Trivial equation since no change of energy";

  end if;

  //
  // Power calculations
  //
  eta_fan = eta_fan_ref * (1 - f_loss * (V_flow / V_flow_ref - 1) ^ 2)
    "Efficiency ot the fan dependent on the volume flow rate";

  P_shaft = P_hydraulic / eta_fan
    "Shaft power consumption of fan";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial model is the base model for all fans using affinity laws and volume-
flow-dependent efficiencies. It defines fundamental parameters and variables required 
by all affinity fans. Models that inherit properties from this partial model have 
to redeclare the fluid ports. Moreover, the instreaming density must be calculated. 
In this context, appropriate fluid property models are required.
<br/><br/>
The following variables must be specified in the model that inherit properties:
</p>
<ul>
  <li>
  Instreaming density <i>rho</i>.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  January 11, 2024, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end PartialAffinityFan;
