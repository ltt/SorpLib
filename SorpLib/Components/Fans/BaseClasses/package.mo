within SorpLib.Components.Fans;
package BaseClasses "Base models and functions for all pumps"
  extends Modelica.Icons.BasesPackage;

  annotation (Documentation(info="<html>
<p>
This package contains partial fan models, containing fundamental definitions for 
pumps. The content of this package is only of interest when adding new fans to 
the library. 
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end BaseClasses;
