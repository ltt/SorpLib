within SorpLib.Components.Fans.BaseClasses;
partial model PartialSimpleFan
  "Base model for all simple fans with constant efficiencies"
  extends SorpLib.Components.Fans.BaseClasses.PartialFan(final
      calculateDrivePower=true);

  //
  // Definition of paramteres describring the fan's characteristics
  //
  parameter Modelica.Units.SI.Efficiency eta_fan = 0.4
    "Efficiency of fan that is used to calculate shaft power"
    annotation (Dialog(tab="General", group="Fan Characteristics"));

  //
  // Definition of parameters describing the inputs
  //
  parameter SorpLib.Choices.PrescripedFanVariable prescribedInput=
    SorpLib.Choices.PrescripedFanVariable.V_flow
    "Prescribed input variable"
    annotation (Dialog(tab="General", group="Inputs"));

  parameter Boolean use_mFlowInput = false
    "=true, if m_flow is defined by input; otherwise, fixed value is used"
    annotation (Dialog(tab="General",group="Inputs",
                enable=(prescribedInput ==
                SorpLib.Choices.PrescripedFanVariable.m_flow)),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);
  parameter Modelica.Units.SI.MassFlowRate m_flow_fixed = 0.1
    "Fixed mass flow rate"
    annotation (Dialog(tab="General",group="Inputs",
                enable=(prescribedInput ==
                SorpLib.Choices.PrescripedFanVariable.m_flow)
                and not use_mFlowInput));

  parameter Boolean use_VFlowInput = false
    "=true, if V_flow is defined by input; otherwise, fixed value is used"
    annotation (Dialog(tab="General",group="Inputs",
                enable=(prescribedInput ==
                SorpLib.Choices.PrescripedFanVariable.V_flow)),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);
  parameter Modelica.Units.SI.VolumeFlowRate V_flow_fixed = 10/1000/60
    "Fixed volume flow rate"
    annotation (Dialog(tab="General",group="Inputs",
                enable=(prescribedInput ==
                SorpLib.Choices.PrescripedFanVariable.V_flow)
                and not use_VFlowInput));

  //
  // Definition of connectors
  //
  Modelica.Blocks.Interfaces.RealInput m_flow_input(final unit="kg/s") if
    (prescribedInput == SorpLib.Choices.PrescripedFanVariable.m_flow and
    use_mFlowInput)
    "Input for mass flow rate"
    annotation (Placement(transformation(extent={{-20,-20},{20,20}},
        rotation=90,
        origin={0,-90}),
      iconTransformation(extent={{-10,-10},{10,10}},
        rotation=90,
        origin={0,-80})));

  Modelica.Blocks.Interfaces.RealInput V_flow_input(final unit="m3/s") if
    (prescribedInput == SorpLib.Choices.PrescripedFanVariable.V_flow and
    use_VFlowInput)
    "Input for volume flow rate"
    annotation (Placement(transformation(extent={{-20,-20},{20,20}},
        rotation=90,
        origin={30,-90}),
      iconTransformation(extent={{-10,-10},{10,10}},
        rotation=90,
        origin={30,-80})));

  //
  // Definition of protected connectors
  //
protected
  Modelica.Blocks.Interfaces.RealInput m_flow_internal(final unit="kg/s")
    "Needed for connecting to conditional connector";
  Modelica.Blocks.Interfaces.RealInput V_flow_internal(final unit="m3/s")
    "Needed for connecting to conditional connector";

equation
  //
  // Connectors
  //
  connect(m_flow_internal, m_flow_input);
  connect(V_flow_internal, V_flow_input);

  if not use_mFlowInput then
    m_flow_internal = m_flow_fixed
      "Needed for connecting to conditional connector";
  end if;
  if not use_VFlowInput then
    V_flow_internal = V_flow_fixed
      "Needed for connecting to conditional connector";
  end if;

  //
  // Mass balance
  //
  if prescribedInput == SorpLib.Choices.PrescripedFanVariable.m_flow then
    m_flow = m_flow_internal
      "Mass flow rate at port a";
    V_flow = m_flow_internal / rho
      "Volume flow rate at port a";

  elseif prescribedInput == SorpLib.Choices.PrescripedFanVariable.V_flow then
    m_flow = V_flow_internal * rho
      "Mass flow rate at port a";
    V_flow = V_flow_internal
      "Volume flow rate at port a";

  end if;

  //
  // Energy balance
  //
  if assumeIsenthalpicFan then
    port_a.h_outflow = inStream(port_b.h_outflow)
      "Stream variable: Trivial equation since no change of energy due to
      forbidden flow revesal";
    port_b.h_outflow = inStream(port_a.h_outflow) + (1/eta_fan - 1) * dp / rho
      "Increase of specific enthalpy due to internal losses of the fan";

  else
    port_a.h_outflow = inStream(port_b.h_outflow)
      "Stream variable: Trivial equation since no change of energy";
    port_b.h_outflow = inStream(port_a.h_outflow)
      "Stream variable: Trivial equation since no change of energy";

  end if;

  //
  // Power calculations
  //
  P_shaft = P_hydraulic / eta_fan
    "Shaft power consumption of fan";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial model is the base model for all simple fans with constant efficiencies.
It defines fundamental parameters and variables required by all simple fans. Models 
that inherit properties from this partial model have to redeclare the fluid ports. 
Moreover, the instreaming density must be calculated. In this context, appropriate 
fluid property models are required.
<br/><br/>
The following variables must be specified in the model that inherit properties:
</p>
<ul>
  <li>
  Instreaming density <i>rho</i>.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  January 10, 2024, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end PartialSimpleFan;
