within SorpLib.Components.Fans;
model AffinityFan "Model of a fan based on affinity laws"
  extends BaseClasses.PartialAffinityFan(
    redeclare final Basics.Interfaces.FluidPorts.GasPort_out port_b,
    redeclare final Basics.Interfaces.FluidPorts.GasPort_in port_a,
    final no_components=Medium.nX);

  //
  // Definition of parameters regarding the medium
  //
  replaceable package Medium =
    SorpLib.Media.IdealGasVaporMixtures.MoistAir_N2_O2_CO2_H2O
    constrainedby Modelica.Media.Interfaces.PartialMedium
    "Medium model of the ideal gas, ideal gas mixture, or ideal gas-vapor mixture"
    annotation (Dialog(tab="General", group="Medium"),
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

equation
  //
  // Calculation of fluid properties
  //
  rho = Medium.density_phX(
    p=port_a.p,
    h=inStream(port_a.h_outflow),
    X=cat(1, inStream(port_a.Xi_outflow), {1-sum(inStream(port_a.Xi_outflow))}))
    "Instreaming density";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
The affinity fan can be used to prescribe the mass or volume flow rate in hydraulic
components, such as tubes or heat exchangers. The model assumes a non-constant fan
efficiency that depends on the volume flow rate. Changes compared to a nominal operating
point are considered by affinity laws.

<h4>Main equations</h4>
<p>
The model calculates the hydraulic, shaft, and drive power transmitted to the fluid
or required to drive the fan. The hydraulic power <i>P<sub>hydraulic</sub></i> 
transmitted to the fluid is defined as:
</p>
<pre>
    P<sub>hydraulic</sub> = m&#x307; / &rho;<sub>in</sub> * &Delta;p;
</pre>
<p>
Herein, <i>m&#x307;</i> is the mass flow rate, <i>&rho;<sub>in</sub></i> describes the 
fluid density at the inlet, and <i>&Delta;p = p<sub>b</sub> - p<sub>a</sub></i> is the 
pressure difference between port b and a. The mass flow rate <i>m&#x307;</i> follows
from the volume flow rate <i>V<sub>flow</sub></i> that depends on the rotational speed
<i>n</i>, the nominal volume flow rate <i>V<sub>flow,ref</sub></i>, and the nominal
rotational speed <i>n<sub>ref</sub></i>:
</p>
<pre>
    V<sub>flow</sub> = m&#x307; / &rho;<sub>in</sub> = V<sub>flow,ref</sub> * n / n<sub>ref</sub>;
</pre>
<p>
The shaft power <i>P<sub>shaft</sub></i> depends on the fan efficiency 
<i>&eta;<sub>fan</sub></i> and is defined as:
</p>
<pre>
    P<sub>shaft</sub> = P<sub>hydraulic</sub> / &eta;<sub>fan</sub>;
</pre>
<p>
with
</p>
<pre>
    &eta;<sub>fan</sub> = &eta;<sub>fan,ref</sub> * (1 - f<sub>loss</sub> * ((m&#x307; / &rho;<sub>in</sub>) / V<sub>flow,ref</sub> - 1) ^ 2);
</pre>
<p>
Herein, <i>f<sub>loss</sub></i> is a loss factor describing the dependency on the volume 
flow rate, <i>&eta;<sub>fan,ref</sub></i> is the efficiency at the nominal operating point, 
and <i>V<sub>flow,ref</sub></i> is the nominal volume flow rate. The shaft power is completly 
transmitted to the fluid, thus increasing the outflowing specific enthalpy of the fluid.
<br/><br/>
The drive power <i>P<sub>drive</sub></i> is required to drive the fan, depends on the
drive efficienciy <i>&eta;<sub>drive</sub></i>, and is defined as:
</p>
<pre>
    P<sub>drive</sub> = P<sub>shaft</sub> / &eta;<sub>drive</sub>;
</pre>

<h4>Assumptions and limitations</h4>
<ul>
  <li>
  Steady-state process
  </li>
  <li>
  No flow reversal
  </li>
  <li>
  Affinity laws apply to describe changes in operating conditions
  </li>
  <li>
  No storage of mass or energy
  </li>
</ul>

<h4>Typical use</h4>
<p>
The affinity fan is typically used to prescripe the mass or volume flow rate
in hydraulic components, such as tubes or heat exchangers, if process controlelrs
are investigated and the flow rate changes.
</p>

<h4>Important parameters and options</h4>
<ul>
  <li>
  <i>assumeIsenthalpicFan</i>:
  Defines if the hydraulic losses are considered in the energy balance.
  </li>
  <li>
  <i>calculateDrivePower</i>:
  Defines if the driving power is calculates within the model.
  </li>
  <br/>
  <li>
  <i>avoid_events</i>:
  Defines if events shall be avoided via the noEvent()-operator.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  January 11, 2024, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end AffinityFan;
