within SorpLib.Components.Fans.Tester;
model Test_SimpleFan "Tester for the simple fan"
  extends Modelica.Icons.Example;

  //
  // Definition of parameters regarding the medium
  //
  replaceable package Medium =
    SorpLib.Media.IdealGasVaporMixtures.MoistAir_N2_O2_CO2_H2O
    constrainedby Modelica.Media.Interfaces.PartialMedium
    "Medium model of the ideal gas, ideal gas mixture, or ideal gas-vapor mixture"
    annotation (Dialog(tab="General", group="Medium"),
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

  //
  // Definition of boundary models
  //
  SorpLib.Basics.Sources.Fluids.GasVaporMixtureSource fs_a(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    redeclare final package Medium = Medium)
    "Fluid source a"
    annotation (Placement(transformation(extent={{-70,-10},{-50,10}})));

  SorpLib.Basics.Sources.Fluids.GasVaporMixtureSource fs_b(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    redeclare final package Medium = Medium)
    "Fluid source b"
    annotation (Placement(transformation(extent={{70,-10},{50,10}})));

  //
  // Definition of multi ports
  //
  SorpLib.Components.Fans.SimpleFan fan(use_VFlowInput=true, redeclare final
      package Medium = Medium) "Simple fan"
    annotation (Placement(transformation(extent={{-40,-10},{-20,10}})));

  SorpLib.Components.Fittings.Resistors.GasTubeResistance                       pressureLoss(
    positionFluidProperties=SorpLib.Choices.ResistorFluidProperties.PortAInlet,
    frictionPressureLoss=true,
    fittingPressureLosss=false,
    redeclare model PressureLossModel =
        Fittings.PressureLossCorrelations.TubeInside.Blasius,
    redeclare replaceable parameter Fittings.Records.GeometryTube geometry(l=5,
        d_hyd_a=0.25) constrainedby
      SorpLib.Components.Fittings.Records.GeometryTube,
    redeclare final package Medium = Medium) "Pressure loss model"
    annotation (Placement(transformation(extent={{20,-10},{40,10}})));

  //
  // Definition of input signals
  //
protected
  Modelica.Blocks.Sources.Sine input_VFlow(
    amplitude=100/1.2/60/60,
    f=1/250,
    offset=100/1.2/60/60)
    "Input signal for volume flow rate"
    annotation (Placement(transformation(extent={{-80,-40},{-60,-20}})));

equation
  //
  // Connections
  //
  connect(fs_a.port, fan.port_a) annotation (Line(
      points={{-60,0},{-38,0}},
      color={244,125,35},
      thickness=1));
  connect(pressureLoss.port_a, fan.port_b) annotation (Line(
      points={{22,0},{-22,0}},
      color={244,125,35},
      thickness=1));
  connect(fs_b.port, pressureLoss.port_b) annotation (Line(
      points={{60,0},{38,0}},
      color={244,125,35},
      thickness=1));

  connect(input_VFlow.y, fan.V_flow_input)
    annotation (Line(points={{-59,-30},{-27,-30},{-27,-8}}, color={0,0,127}));

  //
  // Annotations
  //
  annotation (experiment(StopTime=2500), Documentation(info="<html>
<p>
This model checks the simple fan.
<br/><br/>
To see the model behavior, plot the variables of the models over the 
time. The simulation time is correctly preset (Start: 0 s, Stop = 2500 s). 
</p>
</html>", revisions="<html>
<ul>
  <li>
  January 10, 2024, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Test_SimpleFan;
