within SorpLib.Components.Valves.BaseClasses;
partial model PartialValve
  "Base model for all valves"

  //
  // Definition of parameters regarding the medium
  //
  parameter Integer no_components = 1
    "Number of components"
    annotation (Dialog(tab="General", group="Medium"),
                Evaluate=true,
                HideResult=true);

  //
  // Definition of setup parameters
  //
  parameter Boolean controllableOpening = false
    " = true, if valve opening (i.e., position) is contrallable via an input 
    signal"
    annotation (Dialog(tab="General", group="Valve Setup"),
                choices(checkBox=true),
                HideResult=true,
                Evaluate=true);
  parameter Real fixedOpening(min=0, max=1) = 0
    "Fixed valve opening (i.e., position) if the valve is not controllable"
    annotation (Dialog(tab="General", group="Valve Setup",
                enable=not controllableOpening));

  parameter Boolean useTimeConstant = false
    " = true, if time constant is used to delay valve opening (i.e., position)"
    annotation (Dialog(tab="General", group="Valve Setup",
                enable=controllableOpening),
                choices(checkBox=true),
                HideResult=true,
                Evaluate=true);
  parameter Modelica.Units.SI.Time tau = 1
    "Time constant for valve opening (i.e., position)"
    annotation (Dialog(tab="General", group="Valve Setup",
                enable=controllableOpening and useTimeConstant));

  parameter Boolean leackage = false
    " = true, if valve has a leackage mass flow rate (i.e., minimal opening) to 
    avoid singularities"
    annotation (Dialog(tab="General", group="Valve Setup"),
                choices(checkBox=true),
                HideResult=true,
                Evaluate=true);
  parameter Real leackageOpening(min=0, max=1) = 1e-3
    "Leackage valve opening (i.e., minimal valve position) to avoid singularities"
    annotation (Dialog(tab="General", group="Valve Setup",
                enable=leackage));

  parameter Boolean checkValve = false
    " = true, if valve is a check valve (i.e., mass flow direction from port a 
    to port b)"
    annotation (Dialog(tab="General", group="Valve Setup"),
                choices(checkBox=true),
                HideResult=true,
                Evaluate=true);
  parameter Boolean strictCheckValve = false
    " = true, if check valve is strict: Mass can only flow from port a to b; 
    otherwise, a smoothTransition-function is used that allows minimal mass flow 
    from port b around the transition point of the check valve (e.g., pressure 
    difference) but increases numerical stability"
    annotation (Dialog(tab="General", group="Valve Setup",
                enable=checkValve),
                choices(checkBox=true),
                HideResult=true,
                Evaluate=true);

  //
  // Definition of parameters regarding the valve characteristic
  //
  parameter Boolean accountHydraulicHead = false
    " = true, if additional hydraulic head shall be considered to calculate the
    driving potential (i.e., pressure difference) for the mass flow rate"
    annotation (Dialog(tab="Valve Characteristic", group="Driving Potential"),
                choices(checkBox=true),
                HideResult=true,
                Evaluate=true);

  parameter Modelica.Units.SI.Pressure p_hydraulicHead = 1000 * 9.81 * 0.25
    "Additional hydraulic head (i.e., rho * g * h) used to increase the pressure 
    at port a (i.e., positive if port a is located higher than port b)"
    annotation (Dialog(tab="Valve Characteristic", group="Driving Potential",
                enable=accountHydraulicHead));

  //
  // Definition or initialisation parameters
  //
  parameter Real opening_initial = 1
    "Initial value of valve opening (i.e., position) if valve opening is delayed"
    annotation (Dialog(tab="Initialisation", group="Initial Values",
                enable=controllableOpening and useTimeConstant));

  parameter Modelica.Units.SI.MassFlowRate m_flow_start = 1e-4
    "Start value for mass flow rate"
    annotation (Dialog(tab="Initialisation", group="Start Values"));

  //
  // Definition of advanced parameters
  //
  parameter Boolean avoid_events = false
    "= true, if events are avoid by using noEvent()-operator"
    annotation (Dialog(tab = "Advanced", group = "Numerics"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult = true);

  //
  // Definition of inputs
  //
  Modelica.Blocks.Interfaces.RealInput opening(min=0, max=1) if
    controllableOpening
    "Valve opening (i.e., position) input signal"
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={0,70}), iconTransformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={0,70})));

  //
  // Definition of ports
  //
  replaceable SorpLib.Basics.Interfaces.BaseClasses.PartialFluidPort port_a
    constrainedby Basics.Interfaces.BaseClasses.PartialFluidPort(
      final no_components=no_components,
      m_flow(start=m_flow_start))
    "Fluid port a"
    annotation (Placement(transformation(extent={{-90,-10},{-70,10}}),
                iconTransformation(extent={{-90,-10},{-70,10}})),
                choicesAllMatching=true);

  replaceable SorpLib.Basics.Interfaces.BaseClasses.PartialFluidPort port_b
    constrainedby Basics.Interfaces.BaseClasses.PartialFluidPort(
      final no_components=no_components,
      m_flow(start=-m_flow_start))
    "Fluid port b"
    annotation (Placement(transformation(extent={{70,-10},{90,10}}),
                iconTransformation(extent={{70,-10},{90,10}})),
                choicesAllMatching=true);

  //
  // Definition of variables
  //
  Modelica.Units.SI.PressureDifference dp
    "Pressure difference between ports";
  Modelica.Units.SI.PressureDifference dp_wHydraulicHead
    "Pressure difference between ports when accounting the hydraulic";

  //
  // Definition of blocks
  //
  Modelica.Blocks.Continuous.CriticalDamping filter(
    n=2,
    f=5 / (2 * Modelica.Constants.pi * tau),
    normalized=true,
    initType=Modelica.Blocks.Types.Init.InitialOutput,
    y_start=opening_initial) if controllableOpening and useTimeConstant
    "Critical damping filter to filter opening signal"
    annotation (Placement(transformation(
                extent={{-10,-10},{10,10}},
                rotation=270,
                origin={0,40})),
                HideResult=true);

  SorpLib.Components.Valves.Utilities.Limiter limiter(
    lowerLimit=if leackage then leackageOpening else 0,
    upperLimit=1) if controllableOpening
    "Limits the input signal"
    annotation (Placement(transformation(
                extent={{-10,-10},{10,10}},
                rotation=270,
                origin={0,10})),
                HideResult=true);

  //
  // Definition of protected connectors
  //
protected
  Modelica.Blocks.Interfaces.RealInput opening_internal(min=0, max=1)
    "Needed for connecting to conditional components";

  //
  // Definition of variables
  //
  Modelica.Units.SI.PressureDifference dp_
    "Pressure difference used as driving potential to calculate mass flow rate";

equation
  //
  // Assertations
  //
  if not controllableOpening then
    assert(fixedOpening >= 0 and fixedOpening <= 1,
          "Fixed opening (" + String(fixedOpening) + ") must between 0 and 1!",
          level = AssertionLevel.error);
  end if;

  if leackage then
    assert(leackageOpening >= 0 and leackageOpening <= 1,
          "Leackage opening (" + String(leackageOpening) + ") must between 0 " +
          "and 1!",
          level = AssertionLevel.error);
  end if;

  //
  // Momentum balance
  //
  dp = port_a.p - port_b.p
    "Pressure difference between ports";
  dp_wHydraulicHead = (port_a.p + p_hydraulicHead) - port_b.p
    "Pressure difference between ports when accounting the hydraulic";

  dp_ = if accountHydraulicHead then dp_wHydraulicHead else dp
    "Pressure difference used as driving potential to calculate mass flow rate";

  //
  // Mass balance
  //
  0 = port_a.m_flow + port_b.m_flow
    "Steady-state mass balance";

  port_a.Xi_outflow = inStream(port_b.Xi_outflow)
    "Stream variable: Trivial equation since no change of mass fractions";
  port_b.Xi_outflow = inStream(port_a.Xi_outflow)
    "Stream variable: Trivial equation since no change of mass fractions";

  //
  // Energy balance
  //
  port_a.h_outflow = inStream(port_b.h_outflow)
    "Stream variable: Trivial equation since no change of energy";
  port_b.h_outflow = inStream(port_a.h_outflow)
    "Stream variable: Trivial equation since no change of energy";

  //
  // Calculate actual opening
  //
  if not controllableOpening then
    opening_internal = if leackage then max(fixedOpening, leackageOpening) else
      fixedOpening
      "Needed for connecting to conditional components";
  else
    if useTimeConstant then
      connect(opening, filter.u)
        annotation (Line(points={{0,70},{0,52}}, color={0,0,127}));
      connect(filter.y, limiter.u)
        annotation (Line(points={{0,29},{0,22}}, color={0,0,127}));

    else
      connect(opening, limiter.u)
        annotation (Line(points={{0,70},{0,22}}, color={0,0,127}));

    end if;

    connect(opening_internal, limiter.y)
      "Needed for connecting to conditional components";

  end if;

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial model is the base model of all valves. It defines fundamental parameters
and variables required by all valves. 
<br/><br/>
Models that inherit properties from this partial model have to redeclare the fluid ports.
Furthermore, inherting models may have to add further inputs and a medium model. The following 
variables must be specified in the model that inherit properties:
</p>
<ul>
  <li>
  The flow variable <i>port_a.m_flow</i> that defines the mass flow rate at port a. The
  mass flow rate often depends on the pressure difference <i>dp</i> between the two ports
  but alternative calculation approaches can also be implemented.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  December 19, 2023, by Mirko Engelpracht:<br/>
  Major revisions (new functionalities, documentation).
  </li>
  <li>
  January 20, 2021, by Mirko Engelpracht:<br/>
  Smaller revisions after restructuring of the library.
  </li>
  <li>
  December 11, 2017, by Uwe Bau:<br/>
  Tidy up implementation and enhance documentation for publication of library.
  </li>
</ul>
</html>"), Icon(coordinateSystem(extent={{-100,-100},{100,100}}),
                graphics={    Line(
          points={{0,40},{0,0}},
          color={0,0,0},
          thickness=1),
        Polygon(
          points={{-80,62},{-80,62}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{-80,50},{-80,-50},{0,0},{-80,50}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{-40,50},{-40,-50},{40,0},{-40,50}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          origin={40,0},
          rotation=180),
        Ellipse(
          extent={{-16,60},{16,28}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-10,54},{10,34}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          textString="M",
          textStyle={TextStyle.Bold}),
        Line(
          points={{-40,-40},{40,-40}},
          color={0,0,0},
          arrow={Arrow.None,Arrow.Filled}),
        Line(
          points={{8,-20},{-12,-20}},
          color={238,46,47},
          thickness=1,
          arrow={Arrow.None,Arrow.Filled},
          visible=checkValve),
        Text(
          extent={{-10,-6},{10,-30}},
          lineColor={238,46,47},
          lineThickness=1,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          textStyle={TextStyle.Bold},
          textString="x",
          visible=checkValve)}),
    Diagram(coordinateSystem(extent={{-100,-100},{100,100}})));
end PartialValve;
