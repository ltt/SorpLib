within SorpLib.Components.Valves.BaseClasses;
partial model PartialThreeWayValve
  "Base model for all three-way valves"

  //
  // Definition of parameters regarding the medium
  //
  parameter Integer no_components = 1
    "Number of components"
    annotation (Dialog(tab="General", group="Medium"),
                Evaluate=true,
                HideResult=true);

  //
  // Definition of setup parameters
  //
  parameter Boolean controllablePosition = false
    " = true, if valve position is contrallable via an input signal"
    annotation (Dialog(tab="General", group="Valve Setup"),
                choices(checkBox=true),
                HideResult=true,
                Evaluate=true);
  parameter Real fixedPosition(min=0, max=1) = 0
    "Fixed valve position if the valve is not controllable: If 0, mass flows from
    port a to b; if 1, mass flows from port a to c"
    annotation (Dialog(tab="General", group="Valve Setup",
                enable=not controllablePosition));

  parameter Boolean useTimeConstant = false
    " = true, if time constant is used to delay valve position"
    annotation (Dialog(tab="General", group="Valve Setup",
                enable=controllablePosition),
                choices(checkBox=true),
                HideResult=true,
                Evaluate=true);
  parameter Modelica.Units.SI.Time tau = 1
    "Time constant for valve position"
    annotation (Dialog(tab="General", group="Valve Setup",
                enable=controllablePosition and useTimeConstant));

  parameter Boolean leackage = false
    " = true, if valve has a leackage mass flow rate (i.e., cannot be fully 
    switched to position 0 or 1) to avoid singularities"
    annotation (Dialog(tab="General", group="Valve Setup"),
                choices(checkBox=true),
                HideResult=true,
                Evaluate=true);
  parameter Real leackagePosition(min=0, max=1) = 1e-3
    "Leackage valve position to avoid singularities"
    annotation (Dialog(tab="General", group="Valve Setup",
                enable=leackage));

  //
  // Definition of parameters regarding the valve characteristic
  //
  parameter Boolean useKvsValue = true
    " = true, if Kvs-value is used to determine mass flow rate; otherwise, A_eff
    is used to determine mass flow rate"
    annotation (Dialog(tab="Valve Characteristic", group="Type"),
                Evaluate=true);

  parameter Modelica.Units.SI.Area A_eff_max_b = 0.3e-6
    "Maximal effective flow area at port b if valve is completly open"
    annotation (Dialog(tab="Valve Characteristic", group="Effective Flow Area",
                enable=not useKvsValue));
  parameter Modelica.Units.SI.Area A_eff_max_c = A_eff_max_b
    "Maximal effective flow area at port c if valve is completly open"
    annotation (Dialog(tab="Valve Characteristic", group="Effective Flow Area",
                enable=not useKvsValue));

  parameter Modelica.Units.SI.PressureDifference dp_ref_b = 1e5
    "Reference pressure difference that corresponds to the Kvs-value at port b"
    annotation (Dialog(tab="Valve Characteristic", group="Flow coefficient",
                enable=useKvsValue));
  parameter Modelica.Units.SI.PressureDifference dp_ref_c = dp_ref_b
    "Reference pressure difference that corresponds to the Kvs-value at port c"
    annotation (Dialog(tab="Valve Characteristic", group="Flow coefficient",
                enable=useKvsValue));
  parameter Modelica.Units.SI.Density rho_ref_b = 1e3
    "Reference density that corresponds to the Kvs-value at port b"
    annotation (Dialog(tab="Valve Characteristic", group="Flow coefficient",
                enable=useKvsValue));
  parameter Modelica.Units.SI.Density rho_ref_c = rho_ref_b
    "Reference density that corresponds to the Kvs-value at port c"
    annotation (Dialog(tab="Valve Characteristic", group="Flow coefficient",
                enable=useKvsValue));
  parameter Real Kvs_b(unit="m3/h") = 1.5
    "Maximal flow coefficient at port b if valve is completly open"
    annotation (Dialog(tab="Valve Characteristic", group="Flow coefficient",
                enable=useKvsValue));
  parameter Real Kvs_c(unit="m3/h") = Kvs_b
    "Maximal flow coefficient at port c if valve is completly open"
    annotation (Dialog(tab="Valve Characteristic", group="Flow coefficient",
                enable=useKvsValue));

  //
  // Definition or initialisation parameters
  //
  parameter Real position_initial = 1
    "Initial value of valve position if valve position is delayed"
    annotation (Dialog(tab="Initialisation", group="Initial Values",
                enable=controllablePosition and useTimeConstant));

  parameter Modelica.Units.SI.MassFlowRate m_flow_start = 1e-4
    "Start value for mass flow rate"
    annotation (Dialog(tab="Initialisation", group="Start Values"));

  //
  // Definition of advanced parameters
  //
  parameter Boolean avoid_events = false
    "= true, if events are avoid by using noEvent()-operator"
    annotation (Dialog(tab = "Advanced", group = "Numerics"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult = true);
  parameter Modelica.Units.SI.PressureDifference dpRegularization = 10
    "Regularization around zero pressure drop for check valve implementation"
    annotation (Dialog(tab="Advanced", group="Numerics"),
                Evaluate=true,
                HideResult=true);

  //
  // Definition of inputs
  //
  Modelica.Blocks.Interfaces.RealInput position(min=0, max=1) if
    controllablePosition
    "Valve position input signal"
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={0,70}), iconTransformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={0,70})));

  //
  // Definition of ports
  //
  replaceable SorpLib.Basics.Interfaces.BaseClasses.PartialFluidPort port_a
    constrainedby Basics.Interfaces.BaseClasses.PartialFluidPort(
      final no_components=no_components,
      m_flow(start=m_flow_start))
    "Fluid port a"
    annotation (Placement(transformation(extent={{-10,-88},{10,-68}}),
                iconTransformation(extent={{-10,-88},{10,-68}})),
                choicesAllMatching=true);

  replaceable SorpLib.Basics.Interfaces.BaseClasses.PartialFluidPort port_b
    constrainedby Basics.Interfaces.BaseClasses.PartialFluidPort(
      final no_components=no_components,
      m_flow(start=-m_flow_start))
    "Fluid port b"
    annotation (Placement(transformation(extent={{-90,-10},{-70,10}}),
                iconTransformation(extent={{-90,-10},{-70,10}})),
                choicesAllMatching=true);

  replaceable SorpLib.Basics.Interfaces.BaseClasses.PartialFluidPort port_c
    constrainedby Basics.Interfaces.BaseClasses.PartialFluidPort(
      final no_components=no_components,
      m_flow(start=-m_flow_start))
    "Fluid port c"
    annotation (Placement(transformation(extent={{70,-10},{90,10}}),
                iconTransformation(extent={{70,-10},{90,10}})),
                choicesAllMatching=true);

  //
  // Definition of variables
  //
  Modelica.Units.SI.PressureDifference dp_ab
    "Pressure difference between port a and b";
  Modelica.Units.SI.PressureDifference dp_ac
    "Pressure difference between port a and c";

  Modelica.Units.SI.Area A_eff_b
    "Effective flow area used to calculate the mass flow rate at port b: This area 
    considers the actual position and the calculation approach (i.e., Kvs-value 
    or effective flow area";
  Modelica.Units.SI.Area A_eff_c
    "Effective flow area used to calculate the mass flow rate at port c: This area 
    considers the actual position and the calculation approach (i.e., Kvs-value 
    or effective flow area";

  //
  // Definition of blocks
  //
  Modelica.Blocks.Continuous.CriticalDamping filter(
    n=2,
    f=5 / (2 * Modelica.Constants.pi * tau),
    normalized=true,
    initType=Modelica.Blocks.Types.Init.InitialOutput,
    y_start=position_initial) if controllablePosition and useTimeConstant
    "Critical damping filter to filter position signal"
    annotation (Placement(transformation(
                extent={{-10,-10},{10,10}},
                rotation=270,
                origin={0,40})),
                HideResult=true);

  SorpLib.Components.Valves.Utilities.Limiter limiter(
    lowerLimit=if leackage then leackagePosition else 0,
    upperLimit=if leackage then 1-leackagePosition else 1) if
    controllablePosition
    "Limits the input signal"
    annotation (Placement(transformation(
                extent={{-10,-10},{10,10}},
                rotation=270,
                origin={0,10})),
                HideResult=true);

  //
  // Definition of protected connectors
  //
protected
  Modelica.Blocks.Interfaces.RealInput position_internal(min=0, max=1)
    "Needed for connecting to conditional components";

equation
  //
  // Assertations
  //
  if not controllablePosition then
    assert(fixedPosition >= 0 and fixedPosition <= 1,
          "Fixed position (" + String(fixedPosition) + ") must between 0 and 1!",
          level = AssertionLevel.error);
  end if;

  //
  // Momentum balance
  //
  dp_ab = port_a.p - port_b.p
    "Pressure difference between port a and b";
  dp_ac = port_a.p - port_c.p
    "Pressure difference between port a and c";

  //
  // Mass balance
  //
  0 = port_a.m_flow + port_b.m_flow + port_c.m_flow
    "Steady-state mass balance";

  port_a.Xi_outflow = SorpLib.Components.Valves.Utilities.mixStreamVariables(
    m_flow_1=port_b.m_flow,
    m_flow_2=port_c.m_flow,
    streamVariable_1=inStream(port_b.Xi_outflow),
    streamVariable_2=inStream(port_c.Xi_outflow))
    "Mixed stream variables accounting for flow directions of the three-way
    valve";
  port_b.Xi_outflow = SorpLib.Components.Valves.Utilities.mixStreamVariables(
    m_flow_1=port_a.m_flow,
    m_flow_2=port_c.m_flow,
    streamVariable_1=inStream(port_a.Xi_outflow),
    streamVariable_2=inStream(port_c.Xi_outflow))
    "Mixed stream variables accounting for flow directions of the three-way
    valve";
  port_c.Xi_outflow = SorpLib.Components.Valves.Utilities.mixStreamVariables(
    m_flow_1=port_a.m_flow,
    m_flow_2=port_b.m_flow,
    streamVariable_1=inStream(port_a.Xi_outflow),
    streamVariable_2=inStream(port_b.Xi_outflow))
    "Mixed stream variables accounting for flow directions of the three-way
    valve";

  //
  // Energy balance
  //
  port_a.h_outflow = SorpLib.Components.Valves.Utilities.mixStreamVariables(
    m_flow_1=port_b.m_flow,
    m_flow_2=port_c.m_flow,
    streamVariable_1=inStream(port_b.h_outflow),
    streamVariable_2=inStream(port_c.h_outflow))
    "Mixed stream variables accounting for flow directions of the three-way
    valve";
  port_b.h_outflow = SorpLib.Components.Valves.Utilities.mixStreamVariables(
    m_flow_1=port_a.m_flow,
    m_flow_2=port_c.m_flow,
    streamVariable_1=inStream(port_a.h_outflow),
    streamVariable_2=inStream(port_c.h_outflow))
    "Mixed stream variables accounting for flow directions of the three-way
    valve";
  port_c.h_outflow = SorpLib.Components.Valves.Utilities.mixStreamVariables(
    m_flow_1=port_a.m_flow,
    m_flow_2=port_b.m_flow,
    streamVariable_1=inStream(port_a.h_outflow),
    streamVariable_2=inStream(port_b.h_outflow))
    "Mixed stream variables accounting for flow directions of the three-way
    valve";

  //
  // Calculate actual opening
  //
  if not controllablePosition then
    position_internal = if leackage then
      min(max(fixedPosition,leackagePosition),1-leackagePosition) else
      fixedPosition
      "Needed for connecting to conditional components";
  else
    if useTimeConstant then
      connect(position, filter.u)
        annotation (Line(points={{0,70},{0,52}}, color={0,0,127}));
      connect(filter.y, limiter.u)
        annotation (Line(points={{0,29},{0,22}}, color={0,0,127}));

    else
      connect(position, limiter.u)
        annotation (Line(points={{0,70},{0,22}}, color={0,0,127}));

    end if;

    connect(position_internal, limiter.y)
      "Needed for connecting to conditional components";

  end if;

  //
  // Calculate the effective flow area
  //
  if useKvsValue then
    A_eff_b = Kvs_b * sqrt(rho_ref_b / (2 * dp_ref_b)) / 3600 *
      (1 - position_internal)
      "Effective flow area used to calculate the mass flow rate at port b";
    A_eff_c = Kvs_c * sqrt(rho_ref_c / (2 * dp_ref_c)) / 3600 *
      position_internal
      "Effective flow area used to calculate the mass flow rate at port c";

  else
    A_eff_b = A_eff_max_b * (1 - position_internal)
      "Effective flow area used to calculate the mass flow rate at port b";
    A_eff_c = A_eff_max_c * position_internal
      "Effective flow area used to calculate the mass flow rate at port c";

  end if;

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial model is the base model of all three-way valves. It defines fundamental 
parameters and variables required by all valves. 
<br/><br/>
Models that inherit properties from this partial model have to redeclare the fluid 
ports. Furthermore, inherting models may have to add a medium model. The following 
variables must be specified in the model that inherit properties:
</p>
<ul>
  <li>
  The flow variable <i>port_b.m_flow</i> that defines the mass flow rate at port b
  in dependance of the pressure drop between port a and b <i>dp_ab</i>.
  </li>
  <li>
  The flow variable <i>port_c.m_flow</i> that defines the mass flow rate at port c
  in dependance of the pressure drop between port a and c <i>dp_ac</i>.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  December 19, 2023, by Mirko Engelpracht:<br/>
  First implementation
  </li>
</ul>
</html>"), Icon(coordinateSystem(extent={{-100,-100},{100,100}}),
                graphics={    Line(
          points={{0,40},{0,0}},
          color={0,0,0},
          thickness=1),
        Polygon(
          points={{-80,62},{-80,62}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{-80,50},{-80,-50},{0,0},{-80,50}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{-40,50},{-40,-50},{40,0},{-40,50}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          origin={40,0},
          rotation=180),
        Ellipse(
          extent={{-16,60},{16,28}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-10,54},{10,34}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          textString="M",
          textStyle={TextStyle.Bold}),
        Polygon(
          points={{-40,50},{-40,-50},{40,0},{-40,50}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          origin={0,-40},
          rotation=90),
        Line(
          points={{-40,0},{-40,-40}},
          color={0,0,0},
          arrow={Arrow.None,Arrow.Filled},
          origin={20,40},
          rotation=90),
        Line(
          points={{10,0},{-10,0}},
          color={238,46,47},
          thickness=1,
          arrow={Arrow.None,Arrow.Filled},
          origin={0,-50},
          rotation=90),
        Text(
          extent={{-10,12},{10,-12}},
          lineColor={238,46,47},
          lineThickness=1,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          textStyle={TextStyle.Bold},
          textString="x",
          origin={0,-46},
          rotation=180),
        Line(
          points={{-40,-80},{-40,-40}},
          color={0,0,0},
          arrow={Arrow.None,Arrow.Filled},
          origin={-100,40},
          rotation=90)}),
    Diagram(coordinateSystem(extent={{-100,-100},{100,100}})));
end PartialThreeWayValve;
