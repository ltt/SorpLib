within SorpLib.Components.Valves.BaseClasses;
partial model PartialIncompressibleValve
  "Base model for all valves for incompressible fluids"
  extends SorpLib.Components.Valves.BaseClasses.PartialOrificeValve(
    final strictCheckValve=true);

  //
  // Definition of variables
  //
protected
  Modelica.Units.SI.Density rho_a
    "Instreaming density at port a";
  Modelica.Units.SI.Density rho_b
    "Instreaming density at port b";

equation
  //
  // Calculate the mass flow rate
  //
  if checkValve and strictCheckValve then
    port_a.m_flow = A_eff * (if avoid_events then
      SorpLib.Numerics.regSquareWFactors_inv_noEvent(
      y=dp_,
      delta_x=sqrt(dpRegularization),
      f_positive=2*rho_a,
      f_negative=0) else
      SorpLib.Numerics.regSquareWFactors_inv(
      y=dp_,
      delta_x=sqrt(dpRegularization),
      f_positive=2*rho_a,
      f_negative=0))
      "Mass flow rate at port a: Design flow direction from port a to b and no
      other flow direction is allowed";

  elseif checkValve and not strictCheckValve then
    port_a.m_flow = A_eff * (if avoid_events then
      SorpLib.Numerics.regSquareWFactors_inv_noEvent(
      y=dp_,
      delta_x=sqrt(dpRegularization),
      f_positive=2*rho_a,
      f_negative=0) else
      SorpLib.Numerics.regSquareWFactors_inv(
      y=dp_,
      delta_x=sqrt(dpRegularization),
      f_positive=2*rho_a,
      f_negative=0))
      "Mass flow rate at port a: Design flow direction from port a to b and a
      minimal flow from port b to a is allowed";

  else
    port_a.m_flow = A_eff * (if avoid_events then
      SorpLib.Numerics.regSquareWFactors_inv_noEvent(
      y=dp_,
      delta_x=sqrt(dpRegularization),
      f_positive=2*rho_a,
      f_negative=2*rho_b) else
      SorpLib.Numerics.regSquareWFactors_inv(
      y=dp_,
      delta_x=sqrt(dpRegularization),
      f_positive=2*rho_a,
      f_negative=2*rho_b))
      "Mass flow rate at port a: Design flow direction from port a to b but
      both flow directions are allowed";

  end if;

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial model is the base model of all valves for incompresible fluids. It 
defines fundamental parameters and variables required by all valves for incompresible
fluids.
<br/><br/>
Models that inherit properties from this partial model have to redeclare the fluid 
ports and to add a medium model. The following variables must be specified in the 
model that inherit properties:
</p>
<ul>
  <li>
  <i>rho_a</i>: Instreaming density at port a.
  </li>
  <li>
  <i>rho_b</i>: Instreaming density at port b.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  December 19, 2023, by Mirko Engelpracht:<br/>
  First implementation
  </li>
</ul>
</html>"));
end PartialIncompressibleValve;
