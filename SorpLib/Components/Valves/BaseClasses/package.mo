within SorpLib.Components.Valves;
package BaseClasses "Base models and functions for all valves"
  extends Modelica.Icons.BasesPackage;

  annotation (Documentation(info="<html>
<p>
This package contains partial valve models and valve characteristic functions,
containing fundamental definitions for valves and their charecteristics. The 
content of this package is only of interest when adding new valves to the library. 
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end BaseClasses;
