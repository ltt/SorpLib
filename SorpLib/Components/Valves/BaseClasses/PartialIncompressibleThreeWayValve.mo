within SorpLib.Components.Valves.BaseClasses;
model PartialIncompressibleThreeWayValve
  "Base model for all three-way valves for incompressible fluids"
  extends SorpLib.Components.Valves.BaseClasses.PartialThreeWayValve;

  //
  // Definition of variables
  //
protected
  Modelica.Units.SI.Density rho_a
    "Instreaming density at port a";

equation
  //
  // Calculate the mass flow rates
  //
  port_b.m_flow = -A_eff_b * (if avoid_events then
    SorpLib.Numerics.regSquareWFactors_inv_noEvent(
    y=dp_ab,
    delta_x=sqrt(dpRegularization),
    f_positive=2*rho_a,
    f_negative=0) else
    SorpLib.Numerics.regSquareWFactors_inv(
    y=dp_ab,
    delta_x=sqrt(dpRegularization),
    f_positive=2*rho_a,
    f_negative=0))
    "Mass flow rate at port b: Design flow direction from port a to b and no
    other flow direction is allowed";
  port_c.m_flow = -A_eff_c * (if avoid_events then
    SorpLib.Numerics.regSquareWFactors_inv_noEvent(
    y=dp_ac,
    delta_x=sqrt(dpRegularization),
    f_positive=2*rho_a,
    f_negative=0) else
    SorpLib.Numerics.regSquareWFactors_inv(
    y=dp_ac,
    delta_x=sqrt(dpRegularization),
    f_positive=2*rho_a,
    f_negative=0))
    "Mass flow rate at port c: Design flow direction from port a to c and no
    other flow direction is allowed";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial model is the base model of all three-way valves for incompresible 
fluids. It defines fundamental parameters and variables required by all three-way
valves for incompresible fluids.
<br/><br/>
Models that inherit properties from this partial model have to redeclare the fluid 
ports and to add a medium model. The following variables must be specified in the 
model that inherit properties:
</p>
<ul>
  <li>
  <i>rho_a</i>: Instreaming density at port a.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  December 19, 2023, by Mirko Engelpracht:<br/>
  First implementation
  </li>
</ul>
</html>"));
end PartialIncompressibleThreeWayValve;
