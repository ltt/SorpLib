within SorpLib.Components.Valves.BaseClasses;
model PartialCompressibleThreeWayValve
  "Base model for all three-way valves for compressible fluids"
  extends SorpLib.Components.Valves.BaseClasses.PartialThreeWayValve;

  //
  // Definition of variables
  //
protected
  Modelica.Units.SI.Density rho_a
    "Instreaming density at port a";
  Modelica.Media.Common.IsentropicExponent gamma_a
    "Instreaming isentropic exponent at port a";

  Real radicand_ab
    "Radicand if flowing from port a to b";
  Real radicand_ac
    "Radicand if flowing from port a to c";

  Real weightingFactor_ab
    "Weighting factor used to distinguish between flow regimes if flowing from
    port a to b";
  Real weightingFactor_ac
    "Weighting factor used to distinguish between flow regimes if flowing from
    port b to c";

equation
  //
  // Calculate radicands
  //
  if avoid_events then
    weightingFactor_ab =
      SorpLib.Numerics.smoothTransition_noEvent(
        x=(port_b.p / port_a.p),
        transitionPoint=(2 / (gamma_a + 1)) ^ (gamma_a / (gamma_a - 1)),
        transitionLength=1e-4,
        noDiff=2)
      "Weighting factor used to distinguish between flow regimes if flowing from
      port a to b";
    weightingFactor_ac =
      SorpLib.Numerics.smoothTransition_noEvent(
        x=(port_c.p / port_a.p),
        transitionPoint=(2 / (gamma_a + 1)) ^ (gamma_a / (gamma_a - 1)),
        transitionLength=1e-4,
        noDiff=2)
      "Weighting factor used to distinguish between flow regimes if flowing from
      port a to c";

  else
    weightingFactor_ab =
      SorpLib.Numerics.smoothTransition(
        x=(port_b.p / port_a.p),
        transitionPoint=(2 / (gamma_a + 1)) ^ (gamma_a / (gamma_a - 1)),
        transitionLength=1e-4,
        noDiff=2)
      "Weighting factor used to distinguish between flow regimes if flowing from
      port a to b";
    weightingFactor_ac =
      SorpLib.Numerics.smoothTransition(
        x=(port_c.p / port_a.p),
        transitionPoint=(2 / (gamma_a + 1)) ^ (gamma_a / (gamma_a - 1)),
        transitionLength=1e-4,
        noDiff=2)
      "Weighting factor used to distinguish between flow regimes if flowing from
      port a to c";

  end if;

  radicand_ab = (2 * rho_a * port_a.p) *
    (weightingFactor_ab * (2 ^ (2 / (gamma_a - 1)) * gamma_a /
    (gamma_a + 1) ^ ((gamma_a + 1) / (gamma_a - 1))) +
    (1-weightingFactor_ab) * (gamma_a / (gamma_a - 1) *
    ((port_b.p / port_a.p) ^ (2 / gamma_a) -
    (port_b.p / port_a.p) ^ ((gamma_a + 1) / gamma_a))))
    "Radicand if flowing from port a to b";
  radicand_ac = (2 * rho_a * port_a.p) *
    (weightingFactor_ac * (2 ^ (2 / (gamma_a - 1)) * gamma_a /
    (gamma_a + 1) ^ ((gamma_a + 1) / (gamma_a - 1))) +
    (1-weightingFactor_ac) * (gamma_a / (gamma_a - 1) *
    ((port_c.p / port_a.p) ^ (2 / gamma_a) -
    (port_c.p / port_a.p) ^ ((gamma_a + 1) / gamma_a))))
    "Radicand if flowing from port a to c";

  //
  // Calculate the mass flow rates
  //
  port_b.m_flow = -A_eff_b * (if avoid_events then
    SorpLib.Numerics.regSquareWFactors_inv_noEvent(
    y=dp_ab,
    delta_x=sqrt(dpRegularization),
    f_positive=radicand_ab,
    f_negative=0) else
    SorpLib.Numerics.regSquareWFactors_inv(
    y=dp_ab,
    delta_x=sqrt(dpRegularization),
    f_positive=radicand_ab,
    f_negative=0))
    "Mass flow rate at port b: Design flow direction from port a to b and no
    other flow direction is allowed";
  port_c.m_flow = -A_eff_c * (if avoid_events then
    SorpLib.Numerics.regSquareWFactors_inv_noEvent(
    y=dp_ac,
    delta_x=sqrt(dpRegularization),
    f_positive=radicand_ac,
    f_negative=0) else
    SorpLib.Numerics.regSquareWFactors_inv(
    y=dp_ac,
    delta_x=sqrt(dpRegularization),
    f_positive=radicand_ac,
    f_negative=0))
    "Mass flow rate at port c: Design flow direction from port a to c and no
    other flow direction is allowed";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial model is the base model of all three-way valves for compressible 
fluids. It defines fundamental parameters and variables required by all three-way
valves for compressible fluids.
<br/><br/>
Models that inherit properties from this partial model have to redeclare the fluid 
ports and to add a medium model. The following variables must be specified in the 
model that inherit properties:
</p>
<ul>
  <li>
  <i>rho_a</i>: Instreaming density at port a.
  </li>
  <li>
  <i>gamma_a</i>: Instreaming isentropic exponent at port a.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  December 19, 2023, by Mirko Engelpracht:<br/>
  First implementation
  </li>
</ul>
</html>"));
end PartialCompressibleThreeWayValve;
