within SorpLib.Components.Valves.BaseClasses;
partial model PartialCondensateRefluxValve
  "Base model for all condensate reflux valves"

  //
  // Definition setup parameters
  //
  parameter Real setPoint = 0.25
    "Set point of process variable that is controlled (e.g., relative filling
    level of a phase separator volume) at port a (i.e., mass flows from port a to
    b)"
    annotation (Dialog(tab="General", group="Valve Setup"));

  //
  // Definition of parameters regarding the valve characteristic
  //
  parameter Integer controlType(min=1, max=3) = 2
    "Control type: Constant, linear, or quadratic"
    annotation (Dialog(tab="Valve Characteristic", group="Type"),
                choices(
                  choice=1 "Constant",
                  choice=2 "Linear",
                  choice=3 "Quadratic"));

  parameter Modelica.Units.SI.MassFlowRate m_flow_constant = 10/60
    "Constant mass flow rate used if controlType = 'Constant'"
    annotation (Dialog(tab="Valve Characteristic", group="Constant Controler",
                enable=controlType==1));
  parameter Real kp_linear(final unit="kg/s") = 0.15
    "Linear valve coefficient if controlType = 'Linear'"
    annotation (Dialog(tab="Valve Characteristic", group="Linear Controler",
                enable=controlType==2));
  parameter Real kp_quadratic(final unit="kg/s") = 0.15
    "Quadratic valve coefficient if controlType = 'Quadratic'"
    annotation (Dialog(tab="Valve Characteristic", group="Quadratic Controler",
                enable=controlType==3));

  extends SorpLib.Components.Valves.BaseClasses.PartialValve(
    final controllableOpening=false,
    final fixedOpening=1,
    final useTimeConstant=false,
    final tau=1,
    final leackage=false,
    final leackageOpening=0,
    final accountHydraulicHead=false,
    final p_hydraulicHead = 1000 * 9.81 * 0.25,
    final opening_initial=0);

  //
  // Definition of advanced parameters
  //
  parameter Real setPointRegularization = 1e-2
    "Regularization around set point required for smooth transition to zero mass 
    flow rate if valve is a check valve or controlType = 'Constant'"
    annotation (Dialog(tab="Advanced", group="Numerics",
                enable=checkValve or controlType==1),
                Evaluate=true,
                HideResult=true);
  parameter Integer noDiff(min=1,max=3) = 3
    "Specification how often smoothTransition-function can be differentiated 
    (i.e., 1, 2 or 3) if valve is a check valve"
    annotation (Dialog(tab="Advanced", group="Numerics",
                enable=checkValve),
                Evaluate=true,
                HideResult=true);

  //
  // Definition of inputs
  //
  Modelica.Blocks.Interfaces.RealInput processVariable
    "Process variable that is controlled to reach a set point"
    annotation (Placement(transformation(extent={{-36,32},{-16,52}}),
                iconTransformation(extent={{-36,32},{-16,52}})));

  //
  // Definition of variables
  //
protected
  Real weightingFactor = if checkValve then (if avoid_events then
    SorpLib.Numerics.smoothTransition_noEvent(
      x=processVariable,
      transitionPoint=setPoint,
      transitionLength=setPointRegularization,
      noDiff=noDiff) else
    SorpLib.Numerics.smoothTransition(
      x=processVariable,
      transitionPoint=setPoint,
      transitionLength=setPointRegularization,
      noDiff=noDiff)) else 0
    "Weighting factor used for smooth transition to zero mass flow rate if valve
    is a check valve: 1 at 'setPoint-setPointRegularization/2' and 0 at 
    'setPoint+setPointRegularization/2";

equation
  //
  // Calculate flow characteristic depending on the control type
  //
  if controlType == 1 then
    if checkValve and strictCheckValve then
      port_a.m_flow = (1 - (if avoid_events then
        SorpLib.Numerics.smoothTransition_noEvent(
          x=processVariable,
          transitionPoint=setPoint+setPointRegularization/2,
          transitionLength=setPointRegularization,
          noDiff=noDiff) else
        SorpLib.Numerics.smoothTransition(
          x=processVariable,
          transitionPoint=setPoint+setPointRegularization/2,
          transitionLength=setPointRegularization,
          noDiff=noDiff))) * m_flow_constant
        "Mass flow rate at port a: Design flow direction from port a to b and no
        other flow direction is allowed";

    elseif checkValve and not strictCheckValve then
      port_a.m_flow = (1-weightingFactor) * m_flow_constant
        "Mass flow rate at port a: Design flow direction from port a to b and a
        minimal flow from port b to a is allowed";

    else
      port_a.m_flow = if avoid_events then
        SorpLib.Numerics.regStep_noEvent(
          x=processVariable-setPoint,
          y1=m_flow_constant,
          y2=-m_flow_constant,
          x_small=setPointRegularization) else
        SorpLib.Numerics.regStep(
          x=processVariable-setPoint,
          y1=m_flow_constant,
          y2=-m_flow_constant,
          x_small=setPointRegularization)
        "Mass flow rate at port a: Design flow direction from port a to b but
        both flow directions are allowed";

    end if;

  elseif controlType == 2 then
    if checkValve and strictCheckValve then
      port_a.m_flow = smooth(0, max(processVariable - setPoint, 0)) * kp_linear
        "Mass flow rate at port a: Design flow direction from port a to b and no
        other flow direction is allowed";

    elseif checkValve and not strictCheckValve then
      port_a.m_flow = (1-weightingFactor) * (processVariable - setPoint) * kp_linear
        "Mass flow rate at port a: Design flow direction from port a to b and a
        minimal flow from port b to a is allowed";

    else
      port_a.m_flow = (processVariable - setPoint) * kp_linear
        "Mass flow rate at port a: Design flow direction from port a to b but
        both flow directions are allowed";

    end if;

  else
    if checkValve and strictCheckValve then
      port_a.m_flow = smooth(0, max(sign(processVariable - setPoint) *
        (processVariable - setPoint)^2, 0)) * kp_quadratic
        "Mass flow rate at port a: Design flow direction from port a to b and no
        other flow direction is allowed";

    elseif checkValve and not strictCheckValve then
      port_a.m_flow = (1-weightingFactor) * sign(processVariable - setPoint) *
        (processVariable - setPoint)^2 * kp_quadratic
        "Mass flow rate at port a: Design flow direction from port a to b and a
        minimal flow from port b to a is allowed";

    else
      port_a.m_flow = sign(processVariable - setPoint) *
        (processVariable - setPoint)^2 * kp_quadratic
        "Mass flow rate at port a: Design flow direction from port a to b but
        both flow directions are allowed";

    end if;
  end if;

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial model is the base model of all condensate reflux valves. It defines fundamental 
parameters and variables required by all condensate reflux valves. These valves act as control 
valves: I.e., a process variable (e.g., the relative filling level in a phase separator volume)
is regulated to its setpoint via the mass flow through the valve. Three characteristics are 
available for the control behavior: A constant mass flow, a mass flow that is linear to the 
deviation from the setpoint, and a mass flow that is quadratic to the deviation from the setpoint.
<br/><br/>
Models that inherit properties from this partial model have to redeclare the fluid ports and to
add a medium model.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 19, 2023, by Mirko Engelpracht:<br/>
  Major revisions (new functionalities, documentation).
  </li>
  <li>
  January 20, 2021, by Mirko Engelpracht:<br/>
  Smaller revisions after restructuring of the library.
  </li>
  <li>
  December 11, 2017, by Uwe Bau:<br/>
  Tidy up implementation and enhance documentation for publication of library.
  </li>
</ul>
</html>"));
end PartialCondensateRefluxValve;
