within SorpLib.Components.Valves.BaseClasses;
partial model PartialCompressibleValve
  "Base model for all valves for compressible fluids"
  extends SorpLib.Components.Valves.BaseClasses.PartialOrificeValve(
    final strictCheckValve=true,
    final accountHydraulicHead=false,
    final p_hydraulicHead=1000*9.81*0.25);

  //
  // Definition of variables
  //
protected
  Modelica.Units.SI.Density rho_a
    "Instreaming density at port a";
  Modelica.Units.SI.Density rho_b
    "Instreaming density at port b";

  Modelica.Media.Common.IsentropicExponent gamma_a
    "Instreaming isentropic exponent at port a";
  Modelica.Media.Common.IsentropicExponent gamma_b
    "Instreaming isentropic exponent at port b";

  Real radicand_a
    "Radicand if flowing from port a to b";
  Real radicand_b
    "Radicand if flowing from port b to a";

  Real weightingFactor_a
    "Weighting factor used to distinguish between flow regimes if flowing from
    port a to b";
  Real weightingFactor_b
    "Weighting factor used to distinguish between flow regimes if flowing from
    port b to a";

equation
  //
  // Calculate radicands
  //
  if avoid_events then
    weightingFactor_a =
      SorpLib.Numerics.smoothTransition_noEvent(
        x=(port_b.p / port_a.p),
        transitionPoint=(2 / (gamma_a + 1)) ^ (gamma_a / (gamma_a - 1)),
        transitionLength=1e-4,
        noDiff=2)
      "Weighting factor used to distinguish between flow regimes if flowing from
      port a to b";
    weightingFactor_b =
      SorpLib.Numerics.smoothTransition_noEvent(
        x=(port_a.p / port_b.p),
        transitionPoint=(2 / (gamma_b + 1)) ^ (gamma_b / (gamma_b - 1)),
        transitionLength=1e-4,
        noDiff=2)
      "Weighting factor used to distinguish between flow regimes if flowing from
      port a to b";

  else
    weightingFactor_a =
      SorpLib.Numerics.smoothTransition(
        x=(port_b.p / port_a.p),
        transitionPoint=(2 / (gamma_a + 1)) ^ (gamma_a / (gamma_a - 1)),
        transitionLength=1e-4,
        noDiff=2)
      "Weighting factor used to distinguish between flow regimes if flowing from
      port a to b";
    weightingFactor_b =
      SorpLib.Numerics.smoothTransition(
        x=(port_a.p / port_b.p),
        transitionPoint=(2 / (gamma_b + 1)) ^ (gamma_b / (gamma_b - 1)),
        transitionLength=1e-4,
        noDiff=2)
      "Weighting factor used to distinguish between flow regimes if flowing from
      port a to b";

  end if;

  radicand_a = (2 * rho_a * port_a.p) *
    (weightingFactor_a * (2 ^ (2 / (gamma_a - 1)) * gamma_a /
    (gamma_a + 1) ^ ((gamma_a + 1) / (gamma_a - 1))) +
    (1-weightingFactor_a) * (gamma_a / (gamma_a - 1) *
    ((port_b.p / port_a.p) ^ (2 / gamma_a) -
    (port_b.p / port_a.p) ^ ((gamma_a + 1) / gamma_a))))
    "Radicand if flowing from port a to b";
  radicand_b = (2 * rho_b * port_b.p) *
    (weightingFactor_b * (2 ^ (2 / (gamma_b - 1)) * gamma_b /
    (gamma_b + 1) ^ ((gamma_b + 1) / (gamma_b - 1))) +
    (1-weightingFactor_b) * (gamma_b / (gamma_b - 1) *
    ((port_a.p / port_b.p) ^ (2 / gamma_b) -
    (port_a.p / port_b.p) ^ ((gamma_b + 1) / gamma_b))))
    "Radicand if flowing from port b to a";

  //
  // Calculate the mass flow rate
  //
  if checkValve and strictCheckValve then
    port_a.m_flow = A_eff * (if avoid_events then
      SorpLib.Numerics.regSquareWFactors_inv_noEvent(
      y=dp_,
      delta_x=sqrt(dpRegularization),
      f_positive=radicand_a,
      f_negative=0) else
      SorpLib.Numerics.regSquareWFactors_inv(
      y=dp_,
      delta_x=sqrt(dpRegularization),
      f_positive=radicand_a,
      f_negative=0))
      "Mass flow rate at port a: Design flow direction from port a to b and no
      other flow direction is allowed";

  elseif checkValve and not strictCheckValve then
    port_a.m_flow = A_eff * (if avoid_events then
      SorpLib.Numerics.regSquareWFactors_inv_noEvent(
      y=dp_,
      delta_x=sqrt(dpRegularization),
      f_positive=radicand_a,
      f_negative=0) else
      SorpLib.Numerics.regSquareWFactors_inv(
      y=dp_,
      delta_x=sqrt(dpRegularization),
      f_positive=radicand_a,
      f_negative=0))
      "Mass flow rate at port a: Design flow direction from port a to b and a
      minimal flow from port b to a is allowed";

  else
    port_a.m_flow = A_eff * (if avoid_events then
      SorpLib.Numerics.regSquareWFactors_inv_noEvent(
      y=dp_,
      delta_x=sqrt(dpRegularization),
      f_positive=radicand_a,
      f_negative=radicand_b) else
      SorpLib.Numerics.regSquareWFactors_inv(
      y=dp_,
      delta_x=sqrt(dpRegularization),
      f_positive=radicand_a,
      f_negative=radicand_b))
      "Mass flow rate at port a: Design flow direction from port a to b but
      both flow directions are allowed";

  end if;

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial model is the base model of all valves for compressible fluids. It 
defines fundamental parameters and variables required by all valves for compressible
fluids.
<br/><br/>
Models that inherit properties from this partial model have to redeclare the fluid 
ports and to add a medium model. The following variables must be specified in the 
model that inherit properties:
</p>
<ul>
  <li>
  <i>rho_a</i>: Instreaming density at port a.
  </li>
  <li>
  <i>rho_b</i>: Instreaming density at port b.
  </li>
  <li>
  <i>gamma_a</i>: Instreaming isentropic exponent at port a.
  </li>
  <li>
  <i>gamma_b</i>: Instreaming isentropic exponent at port b.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  December 19, 2023, by Mirko Engelpracht:<br/>
  First implementation
  </li>
</ul>
</html>"));
end PartialCompressibleValve;
