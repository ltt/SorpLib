within SorpLib.Components.Valves.BaseClasses;
partial function PartialValveCharacteristic
  "Base function for all valve characteristics"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Real opening(min=0, max=1)
    "Valve opening (i.e., position)"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real fc_max = 1.6
    "Maximal flow coefficient if valve is completly open"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of outputs
  //
  output Real fc
    "Flow coefficient"
    annotation (Dialog(tab="General", group="Outputs", enable=false));

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial function is the base function for all valve characteristics. It
definies common inputs and outputs. This partial functions is based on the function
<a href=\"Modelica://Modelica.Fluid.Valves.BaseClasses.ValveCharacteristics.baseFun\">Modelica.Fluid.Valves.BaseClasses.ValveCharacteristics.baseFun</a>.
<br/><br/>
Functions that inherit properties from this partial function may have to add 
further inputs and the calculation approach for the valve characteristic 
(i.e., <i>fc(opening)</i>).
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 19, 2023, by Mirko Engelpracht:<br/>
  First implementation
  </li>
</ul>
</html>"));
end PartialValveCharacteristic;
