within SorpLib.Components.Valves.BaseClasses;
partial model PartialOrificeValve
  "Base model for all orifice valves"

  //
  // Definition of parameters regarding the valve characteristic
  //
  parameter Boolean useKvsValue = true
    " = true, if Kvs-value is used to determine mass flow rate; otherwise, A_eff
    is used to determine mass flow rate"
    annotation (Dialog(tab="Valve Characteristic", group="Type"),
                Evaluate=true);
  parameter Integer valveCharacteristic = 1
    "Valve characteristic used to calculate the flow coefficient or effective area
    depending on the actual opening: Linear, quadratic, or equal percentage"
    annotation (Dialog(tab="Valve Characteristic", group="Type"),
                choices(
                  choice=1 "Linear",
                  choice=2 "Quadratic",
                  choice=3 "Equal percentage"));

  parameter Real opening_A_eff_min = 1e-2
    "Minimal valve opening (i.e., position) that corresponds to minimal effective 
    flow area"
    annotation (Dialog(tab="Valve Characteristic", group="Effective Flow Area",
                enable=not useKvsValue and valveCharacteristic==3));
  parameter Modelica.Units.SI.Area A_eff_min = 0.1e-6
    "Minimal effective flow area if valve is almost closed"
    annotation (Dialog(tab="Valve Characteristic", group="Effective Flow Area",
                enable=not useKvsValue and valveCharacteristic==3));
  parameter Modelica.Units.SI.Area A_eff_max = 0.3e-6
    "Maximal effective flow area if valve is completly open"
    annotation (Dialog(tab="Valve Characteristic", group="Effective Flow Area",
                enable=not useKvsValue));

  parameter Modelica.Units.SI.PressureDifference dp_ref = 1e5
    "Reference pressure difference that corresponds to the Kvs-value"
    annotation (Dialog(tab="Valve Characteristic", group="Flow coefficient",
                enable=useKvsValue));
  parameter Modelica.Units.SI.Density rho_ref = 1e3
    "Reference density that corresponds to the Kvs-value"
    annotation (Dialog(tab="Valve Characteristic", group="Flow coefficient",
                enable=useKvsValue));
  parameter Real opening_Kv_min = 1e-2
    "Minimal valve opening (i.e., position) that corresponds to minimal flow
    coefficient"
    annotation (Dialog(tab="Valve Characteristic", group="Flow coefficient",
                enable=useKvsValue and valveCharacteristic==3));
  parameter Real Kv_min(unit="m3/h") = 0.5
    "Minimal flow coefficient if valve is almost closed"
    annotation (Dialog(tab="Valve Characteristic", group="Flow coefficient",
                enable=useKvsValue and valveCharacteristic==3));
  parameter Real Kvs(unit="m3/h") = 1.5
    "Maximal flow coefficient if valve is completly open"
    annotation (Dialog(tab="Valve Characteristic", group="Flow coefficient",
                enable=useKvsValue));

  extends SorpLib.Components.Valves.BaseClasses.PartialValve(
    controllableOpening=true);

  //
  // Definition of advanced parameters
  //
  parameter Modelica.Units.SI.PressureDifference dpRegularization = 10
    "Regularization around zero pressure drop for continous flow reversal or
    check valve implementation"
    annotation (Dialog(tab="Advanced", group="Numerics"),
                Evaluate=true,
                HideResult=true);

  //
  // Definition of variables
  //
  Modelica.Units.SI.Area A_eff
    "Effective flow area used to calculate the mass flow rate: This area considers 
    the actual opening and the calculation approach (i.e., Kvs-value or effective 
    flow area";

equation
  //
  // Calculate the effective flow area
  //
  if useKvsValue then
    if valveCharacteristic == 1 then
      A_eff =
        SorpLib.Components.Valves.ValveCharacteristics.linearCharacteristic(
        opening=opening_internal,
        fc_max=Kvs) * sqrt(rho_ref / (2 * dp_ref)) / 3600
        "Effective flow area used to calculate the mass flow rate";

    elseif valveCharacteristic == 2 then
      A_eff =
        SorpLib.Components.Valves.ValveCharacteristics.quadraticCharacteristic(
        opening=opening_internal,
        fc_max=Kvs) * sqrt(rho_ref / (2 * dp_ref)) / 3600
        "Effective flow area used to calculate the mass flow rate";

    else
      A_eff =
        SorpLib.Components.Valves.ValveCharacteristics.equalPercentageCharacteristic(
        opening=opening_internal,
        fc_max=Kvs,
        fc_min=Kv_min,
        opening_min=opening_Kv_min) * sqrt(rho_ref / (2 * dp_ref)) / 3600
        "Effective flow area used to calculate the mass flow rate";

    end if;

  else
    if valveCharacteristic == 1 then
      A_eff =
        SorpLib.Components.Valves.ValveCharacteristics.linearCharacteristic(
        opening=opening_internal,
        fc_max=A_eff_max)
        "Effective flow area used to calculate the mass flow rate";

    elseif valveCharacteristic == 2 then
      A_eff =
        SorpLib.Components.Valves.ValveCharacteristics.quadraticCharacteristic(
        opening=opening_internal,
        fc_max=A_eff_max)
        "Effective flow area used to calculate the mass flow rate";

    else
      A_eff =
        SorpLib.Components.Valves.ValveCharacteristics.equalPercentageCharacteristic(
        opening=opening_internal,
        fc_max=A_eff_max,
        fc_min=A_eff_min,
        opening_min=opening_A_eff_min)
        "Effective flow area used to calculate the mass flow rate";

    end if;
  end if;

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial model is the base model of all orifice valves. It defines fundamental parameters 
and variables required by all orifice valves.
<br/><br/>
Models that inherit properties from this partial model have to redeclare the fluid ports and to
add a medium model. The following variables must be specified in the model that inherit 
properties:
</p>
<ul>
  <li>
  The flow variable <i>port_a.m_flow</i> that defines the mass flow rate at port a. The
  mass flow rate depends on the pressure difference <i>dp_</i> between the two ports. Further,
  the mass flow rate depends on the valve type (i.e., (strict) check valve or normal valve).
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  December 19, 2023, by Mirko Engelpracht:<br/>
  First implementation
  </li>
</ul>
</html>"));
end PartialOrificeValve;
