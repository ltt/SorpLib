within SorpLib.Components.Valves.VLEValves.Tester;
model Test_CompressibleThreeWayValve
  "Tester for the three-way valve for compressible fluids"
  extends Modelica.Icons.Example;

  //
  // Definition of boundary models
  //
  SorpLib.Basics.Sources.Fluids.VLESource[3] fs_a(
    each boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
    each boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    each p_fixed(displayUnit="bar") = 50000,
    each T_fixed=373.15)
    "Fluid source a"
    annotation (Placement(transformation(extent={{-70,-10},{-50,10}})));

  SorpLib.Basics.Sources.Fluids.VLESource[3] fs_b(
    each boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
    each boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    each use_pInput=true,
    each use_TInput=true)
    "Fluid source b"
    annotation (Placement(transformation(extent={{70,40},{50,60}})));

  SorpLib.Basics.Sources.Fluids.VLESource[3] fs_c(
    each boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
    each boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    each use_pInput=true,
    each use_TInput=true)
    "Fluid source c"
    annotation (Placement(transformation(extent={{70,-40},{50,-60}})));

  //
  // Definition of valves
  //
  SorpLib.Components.Valves.VLEValves.CompressibleThreeWayValve threeWayValve(
    controllablePosition=true)
    "Three-way valve: Controllable, no time constant, no leackage" annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={0,40})));

  SorpLib.Components.Valves.VLEValves.CompressibleThreeWayValve threeWayValve_delay(
    controllablePosition=true,
    useTimeConstant=true,
    tau=10,
    position_initial=0)
    "Three-way valve: Controllable, time constant, no leackage" annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={0,0})));

  SorpLib.Components.Valves.VLEValves.CompressibleThreeWayValve threeWayValve_delay_leackage(
    controllablePosition=true,
    useTimeConstant=true,
    leackage=true,
    position_initial=0)
    "Three-way valve: Controllable, time constant, leackage" annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={0,-40})));

  //
  // Definition of input signals
  //
protected
  Modelica.Blocks.Sources.Trapezoid input_lRel(
    amplitude=1,
    rising=150,
    width=50,
    falling=0,
    period=250,
    offset=0)
    "Input signal for relative filling level"
    annotation (Placement(transformation(extent={{-10,-10},{10,10}},
        rotation=270,
        origin={10,90})));

  Modelica.Blocks.Sources.Sine input_p_b(
    amplitude=-0.4e5,
    f=1/100,
    offset=0.5e5)
                 "Input signal for pressure at port b"
    annotation (Placement(transformation(extent={{100,40},{80,60}})));
  Modelica.Blocks.Sources.Sine input_T(
    amplitude=25,
    f=1/100,
    offset=273.15 + 150)
    "Input signal for temperature"
    annotation (Placement(transformation(extent={{100,-10},{80,10}})));
  Modelica.Blocks.Sources.Sine input_p_c(
    amplitude=-0.4e5,
    f=1/100,
    offset=0.5e5)
                 "Input signal for pressure at port c"
    annotation (Placement(transformation(extent={{100,-60},{80,-40}})));

equation
  //
  // Connections
  //
  connect(fs_a[1].port, threeWayValve.port_a) annotation (Line(
      points={{-60,0},{-20,0},{-20,40},{-7.8,40}},
      color={0,140,72},
      thickness=1));
  connect(fs_a[2].port, threeWayValve_delay.port_a) annotation (Line(
      points={{-60,0},{-7.8,0}},
      color={0,140,72},
      thickness=1));
  connect(fs_a[3].port, threeWayValve_delay_leackage.port_a) annotation (Line(
      points={{-60,0},{-20,0},{-20,-40},{-7.8,-40}},
      color={0,140,72},
      thickness=1));
  connect(threeWayValve.port_b, fs_b[1].port) annotation (Line(
      points={{0,48},{0,50},{60,50}},
      color={0,140,72},
      thickness=1));
  connect(threeWayValve_delay.port_b, fs_b[2].port) annotation (Line(
      points={{0,8},{0,10},{20,10},{20,50},{60,50}},
      color={0,140,72},
      thickness=1));
  connect(threeWayValve_delay_leackage.port_b, fs_b[3].port) annotation (Line(
      points={{0,-32},{0,-30},{20,-30},{20,50},{60,50}},
      color={0,140,72},
      thickness=1));
  connect(threeWayValve.port_c, fs_c[1].port) annotation (Line(
      points={{0,32},{0,30},{40,30},{40,-50},{60,-50}},
      color={0,140,72},
      thickness=1));
  connect(threeWayValve_delay.port_c, fs_c[2].port) annotation (Line(
      points={{0,-8},{0,-10},{40,-10},{40,-50},{60,-50}},
      color={0,140,72},
      thickness=1));
  connect(threeWayValve_delay_leackage.port_c, fs_c[3].port) annotation (Line(
      points={{0,-48},{0,-50},{60,-50}},
      color={0,140,72},
      thickness=1));

  for ind in 1:3 loop
    connect(input_T.y, fs_b[ind].T_input) annotation (Line(points={{79,0},{70,0},
            {70,48},{61.2,48}},
                          color={0,0,127}));
    connect(input_T.y, fs_c[ind].T_input) annotation (Line(points={{79,0},{70,0},{70,
            -48},{61.2,-48}}, color={0,0,127}));

    connect(input_p_b.y, fs_b[ind].p_input) annotation (Line(points={{79,50},{70,
            50},{70,55},{61.2,55}}, color={0,0,127}));
    connect(input_p_c.y, fs_c[ind].p_input) annotation (Line(points={{79,-50},{70,-50},
            {70,-55},{61.2,-55}}, color={0,0,127}));
  end for;

  connect(input_lRel.y, threeWayValve.position)
    annotation (Line(points={{10,79},{10,40},{7,40}}, color={0,0,127}));
  connect(input_lRel.y, threeWayValve_delay.position)
    annotation (Line(points={{10,79},{10,0},{7,0}}, color={0,0,127}));
  connect(input_lRel.y, threeWayValve_delay_leackage.position)
    annotation (Line(points={{10,79},{10,-40},{7,-40}}, color={0,0,127}));

  //
  // Annotations
  //
  annotation (experiment(StopTime=2500), Documentation(info="<html>
<p>
This model checks the compressible three-way valve.
<br/><br/>
To see the model behavior, plot the variables of the models over the 
time. The simulation time is correctly preset (Start: 0 s, Stop = 2500 s). 
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 19, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Test_CompressibleThreeWayValve;
