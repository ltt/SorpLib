﻿within SorpLib.Components.Valves.VLEValves.Tester;
model Test_CondensateRefluxValve
  "Tester for the condensate reflux valve"
  extends Modelica.Icons.Example;

  //
  // Definition of boundary models
  //
  SorpLib.Basics.Sources.Fluids.VLESource[9] fs_a(
    each boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
    each boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    each p_fixed(displayUnit="kPa") = 1000*(4.246*1.1),
    each T_fixed=303.15)
    "Fluid source a"
    annotation (Placement(transformation(extent={{-70,-10},{-50,10}})));

  SorpLib.Basics.Sources.Fluids.VLESource[9] fs_b(
    each boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
    each boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    each p_fixed(displayUnit="kPa") = 1000*(1.7051*1.1),
    each T_fixed=288.15)
    "Fluid source b"
    annotation (Placement(transformation(extent={{70,-10},{50,10}})));

  //
  // Definition of valves
  //
  SorpLib.Components.Valves.VLEValves.CondensateRefluxValve refluxValve_cType1(
    controlType=1) "Condensate reflux valve: No check valve, control type 1"
    annotation (Placement(transformation(extent={{-10,80},{10,100}})));
  SorpLib.Components.Valves.VLEValves.CondensateRefluxValve refluxValve_cType2(
    controlType=2) "Condensate reflux valve: No check valve, control type 2"
    annotation (Placement(transformation(extent={{-10,60},{10,80}})));
  SorpLib.Components.Valves.VLEValves.CondensateRefluxValve refluxValve_cType3(
    controlType=3) "Condensate reflux valve: No check valve, control type 3"
    annotation (Placement(transformation(extent={{-10,40},{10,60}})));

  SorpLib.Components.Valves.VLEValves.CondensateRefluxValve refluxValve_cType1_checkValve(
      controlType=1,
      checkValve=true) "Condensate reflux valve: Ceck valve, control type 1"
    annotation (Placement(transformation(extent={{-10,10},{10,30}})));
  SorpLib.Components.Valves.VLEValves.CondensateRefluxValve refluxValve_cType2_checkValve(
      controlType=2,
      checkValve=true) "Condensate reflux valve: Check valve, control type 2"
    annotation (Placement(transformation(extent={{-10,-10},{10,10}})));
  SorpLib.Components.Valves.VLEValves.CondensateRefluxValve refluxValve_cType3_checkValve(
      controlType=3,
      checkValve=true) "Condensate reflux valve: Check valve, control type 3"
    annotation (Placement(transformation(extent={{-10,-30},{10,-10}})));

  SorpLib.Components.Valves.VLEValves.CondensateRefluxValve refluxValve_cType1_strictCheckValve(
      controlType=1,
      checkValve=true,
      strictCheckValve=true)
    "Condensate reflux valve: Ceck valve, control type 1"
    annotation (Placement(transformation(extent={{-10,-60},{10,-40}})));
  SorpLib.Components.Valves.VLEValves.CondensateRefluxValve refluxValve_cType2_strictCheckValve(
      controlType=2,
      checkValve=true,
      strictCheckValve=true)
    "Condensate reflux valve: Check valve, control type 2"
    annotation (Placement(transformation(extent={{-10,-80},{10,-60}})));
  SorpLib.Components.Valves.VLEValves.CondensateRefluxValve refluxValve_cType3_strictCheckValve(
      controlType=3,
      checkValve=true,
      strictCheckValve=true)
    "Condensate reflux valve: Check valve, control type 3"
    annotation (Placement(transformation(extent={{-10,-100},{10,-80}})));

  //
  // Definition of input signals
  //
protected
  Modelica.Blocks.Sources.Trapezoid input_lRel(
    amplitude=-0.6,
    rising=200,
    width=50,
    falling=200,
    period=500,
    offset=0.75)
    "Input signal for relative filling leveö"
    annotation (Placement(transformation(extent={{-100,-10},{-80,10}})));

equation
  //
  // Connections
  //
  connect(fs_a[1].port, refluxValve_cType1.port_a) annotation (Line(
      points={{-60,0},{-32,0},{-32,90},{-8,90}},
      color={0,140,72},
      thickness=1));
  connect(refluxValve_cType3.port_a, fs_a[2].port) annotation (Line(
      points={{-8,50},{-32,50},{-32,0},{-60,0}},
      color={0,140,72},
      thickness=1));
  connect(refluxValve_cType2.port_a, fs_a[3].port) annotation (Line(
      points={{-8,70},{-32,70},{-32,0},{-60,0}},
      color={0,140,72},
      thickness=1));
  connect(refluxValve_cType1_checkValve.port_a, fs_a[4].port) annotation (Line(
      points={{-8,20},{-32,20},{-32,0},{-60,0}},
      color={0,140,72},
      thickness=1));
  connect(refluxValve_cType2_checkValve.port_a, fs_a[5].port) annotation (Line(
      points={{-8,0},{-60,0}},
      color={0,140,72},
      thickness=1));
  connect(refluxValve_cType3_checkValve.port_a, fs_a[6].port) annotation (Line(
      points={{-8,-20},{-32,-20},{-32,0},{-60,0}},
      color={0,140,72},
      thickness=1));
  connect(refluxValve_cType1_strictCheckValve.port_a, fs_a[7].port) annotation (
      Line(
      points={{-8,-50},{-32,-50},{-32,0},{-60,0}},
      color={0,140,72},
      thickness=1));
  connect(refluxValve_cType2_strictCheckValve.port_a, fs_a[8].port) annotation (
      Line(
      points={{-8,-70},{-32,-70},{-32,0},{-60,0}},
      color={0,140,72},
      thickness=1));
  connect(refluxValve_cType3_strictCheckValve.port_a, fs_a[9].port) annotation (
      Line(
      points={{-8,-90},{-32,-90},{-32,0},{-60,0}},
      color={0,140,72},
      thickness=1));

  connect(fs_b[1].port, refluxValve_cType1.port_b) annotation (Line(
      points={{60,0},{30,0},{30,90},{8,90}},
      color={0,140,72},
      thickness=1));
  connect(fs_b[2].port, refluxValve_cType2.port_b) annotation (Line(
      points={{60,0},{30,0},{30,70},{8,70}},
      color={0,140,72},
      thickness=1));
  connect(fs_b[3].port, refluxValve_cType3.port_b) annotation (Line(
      points={{60,0},{30,0},{30,50},{8,50}},
      color={0,140,72},
      thickness=1));
  connect(fs_b[4].port, refluxValve_cType1_checkValve.port_b) annotation (Line(
      points={{60,0},{30,0},{30,20},{8,20}},
      color={0,140,72},
      thickness=1));
  connect(fs_b[5].port, refluxValve_cType2_checkValve.port_b) annotation (Line(
      points={{60,0},{8,0}},
      color={0,140,72},
      thickness=1));
  connect(fs_b[6].port, refluxValve_cType3_checkValve.port_b) annotation (Line(
      points={{60,0},{30,0},{30,-20},{8,-20}},
      color={0,140,72},
      thickness=1));
  connect(fs_b[7].port, refluxValve_cType1_strictCheckValve.port_b) annotation (
      Line(
      points={{60,0},{30,0},{30,-50},{8,-50}},
      color={0,140,72},
      thickness=1));
  connect(fs_b[8].port, refluxValve_cType2_strictCheckValve.port_b) annotation (
      Line(
      points={{60,0},{30,0},{30,-70},{8,-70}},
      color={0,140,72},
      thickness=1));
  connect(fs_b[9].port, refluxValve_cType3_strictCheckValve.port_b) annotation (
      Line(
      points={{60,0},{30,0},{30,-90},{8,-90}},
      color={0,140,72},
      thickness=1));

  connect(input_lRel.y, refluxValve_cType1.processVariable) annotation (Line(
        points={{-79,0},{-70,0},{-70,94.2},{-2.6,94.2}}, color={0,0,127}));
  connect(input_lRel.y, refluxValve_cType2.processVariable) annotation (Line(
        points={{-79,0},{-70,0},{-70,74.2},{-2.6,74.2}}, color={0,0,127}));
  connect(input_lRel.y, refluxValve_cType3.processVariable) annotation (Line(
        points={{-79,0},{-70,0},{-70,54.2},{-2.6,54.2}}, color={0,0,127}));
  connect(input_lRel.y, refluxValve_cType1_checkValve.processVariable)
    annotation (Line(points={{-79,0},{-70,0},{-70,24.2},{-2.6,24.2}}, color={0,0,
          127}));
  connect(input_lRel.y, refluxValve_cType2_checkValve.processVariable)
    annotation (Line(points={{-79,0},{-70,0},{-70,16},{-40,16},{-40,4.2},{-2.6,4.2}},
        color={0,0,127}));
  connect(input_lRel.y, refluxValve_cType3_checkValve.processVariable)
    annotation (Line(points={{-79,0},{-70,0},{-70,-15.8},{-2.6,-15.8}}, color={0,
          0,127}));
  connect(input_lRel.y, refluxValve_cType1_strictCheckValve.processVariable)
    annotation (Line(points={{-79,0},{-70,0},{-70,-45.8},{-2.6,-45.8}}, color={0,
          0,127}));
  connect(input_lRel.y, refluxValve_cType2_strictCheckValve.processVariable)
    annotation (Line(points={{-79,0},{-70,0},{-70,-65.8},{-2.6,-65.8}}, color={0,
          0,127}));
  connect(input_lRel.y, refluxValve_cType3_strictCheckValve.processVariable)
    annotation (Line(points={{-79,0},{-70,0},{-70,-85.8},{-2.6,-85.8}}, color={0,
          0,127}));

  //
  // Annotations
  //
  annotation (experiment(StopTime=2500), Documentation(info="<html>
<p>
This model checks the condensate reflux valve.
<br/><br/>
To see the model behavior, plot the variables of the models over the 
time. The simulation time is correctly preset (Start: 0 s, Stop = 2500 s). 
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 19, 2023, by Mirko Engelpracht:<br/>
  Minor revisions (documentation, test new functionalities).
  </li>
  <li>
  January 20, 2021, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Test_CondensateRefluxValve;
