within SorpLib.Components.Valves.VLEValves;
model IncompressibleThreeWayValve
  "Three-way valve for incompressible fluids"
  extends
    SorpLib.Components.Valves.BaseClasses.PartialIncompressibleThreeWayValve(
    redeclare final Basics.Interfaces.FluidPorts.VLEPort_out port_b,
    redeclare final Basics.Interfaces.FluidPorts.VLEPort_out port_c,
    redeclare final Basics.Interfaces.FluidPorts.VLEPort_in port_a,
    final no_components=Medium.nX);

  //
  // Definition of parameters regarding the medium
  //
  replaceable package Medium =
    Modelica.Media.Water.StandardWater
    constrainedby Modelica.Media.Interfaces.PartialTwoPhaseMedium
    "Medium model of the real fluid (i.e., with two-phase regime)"
    annotation (Dialog(tab="General", group="Medium"),
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

equation
  //
  // Calculate fluid properties
  //
  rho_a = Medium.density_phX(
    p=port_a.p,
    h=inStream(port_a.h_outflow),
    X=cat(1,inStream(port_a.Xi_outflow),{1-sum(inStream(port_a.Xi_outflow))}))
    "Instreaming density at port a";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This three-way valve can be applied for incompressible fluids, such as liquid water. 
The valve splits a mass flow entering port a into two mass flows leaving port b 
and c. For thus purpose, the valve position defines the division of the instreaming 
mass flow at port a: I.e., for <i>position = 0</i>, the mass flows to port b; 
for <i>position = 1</i>, the mass flows to port c. The position can be set by an 
input signal.
</p>

<h4>Main equations</h4>
<p>
The mass flow rates <i>port_b.m_flow</i> and <i>port_c.m_flow</i> are calculated 
in dependance of the pressure drops <i>&Delta;p<sub>ab</sub> = port_a.p - port_b.p</i>, 
and <i>&Delta;p<sub>ac</sub> = port_a.p - port_c.p</i>, following the equation of 
Bernoulli for incompressible fluids:
</p>
<pre>
    port_b.m_flow = -A<sub>eff,b</sub> * <strong>sqrt</strong>(2 * &rho; * &Delta;p<sub>ab</sub>) *(1 - position);
</pre>
<pre>
    port_c.m_flow = -A<sub>eff,c</sub> * <strong>sqrt</strong>(2 * &rho; * &Delta;p<sub>ac</sub>) * position;
</pre>
<p>
The effective areas <i>A<sub>eff,i</sub></i> depends on the actual valve position
and are calculated via a linear valve characteristic. For this purpose, the maximal 
effective areas <i>A<sub>eff,max,i</sub></i> are required. The maximal effective 
areas <i>A<sub>eff,max,i</sub></i> are present if the valve is fully opened and 
can directly be specified. Alternatively, they can be calculated using the maximal 
flow coefficients <i>Kvs<sub>i</sub></i>, reference densities <i>&rho;<sub>ref,i</sub></i>, 
and reference pressure drops <i>&Delta;p<sub>ref,i</sub></i>:
</p>
<pre>
    <i>A<sub>eff,max,i</sub></i> = Kvs<sub>i</sub> * <strong>sqrt</strong>(&rho;<sub>ref,i</sub> / (2 * &Delta;p<sub>ref,i</sub>));
</pre>
<p>
The density <i>&rho;</i> corresponds to the inlet density of the actual flow 
direction. Note that the valve is a strict check valve, thus mass can only flow 
from port a to port b and c.
</p>

<h4>Assumptions and limitations</h4>
<ul>
  <li>
  Incompressible fluid
  </li>
  <li>
  Adiabatic process
  </li>
  <li>
  Isenthalpic process
  </li>
  <li>
  No storage of mass or energy
  </li>
</ul>

<h4>Typical use</h4>
<p>
The valve is typically used in hydraulic networks of closed sorption systems to
split a mass flow rate.
</p>

<h4>Important parameters and options</h4>
<ul>
  <li>
  <i>controllablePosition</i>:
  Defines if the valve position can be set via an input signal.
  </li>
  <li>
  <i>useTimeConstant</i>:
  Defines if the valve position input signal is delayed by a time constant.
  </li>
  <li>
  <i>leackage</i>:
  Defines if the valve has a small leackage flow.
  </li>
  <br/>
  <li>
  <i>useKvsValue</i>:
  Defines if the mass flow rates at port b and c are calculated using the 
  Kvs-approach or A_eff-approach.
  </li>
  <br/>
  <li>
  <i>avoid_events</i>:
  Defines if events shall be avoided via the noEvent()-operator.
  </li>
</ul>

<h4>Dynamics</h4>
<p>
The model has two dynamic states if the input signal describing the valve position
is delayed by a time constant. Otherwise, the model has no dynamic states.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 19, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end IncompressibleThreeWayValve;
