within SorpLib.Components.Valves.VLEValves;
model CondensateRefluxValve
  "Condensate reflux valve"
  extends SorpLib.Components.Valves.BaseClasses.PartialCondensateRefluxValve(
    redeclare final Basics.Interfaces.FluidPorts.VLEPort_out port_b,
    redeclare final Basics.Interfaces.FluidPorts.VLEPort_in port_a,
    final no_components=Medium.nX);

  //
  // Definition of parameters regarding the medium
  //
  replaceable package Medium =
    Modelica.Media.Water.StandardWater
    constrainedby Modelica.Media.Interfaces.PartialTwoPhaseMedium
    "Medium model of the real fluid (i.e., with two-phase regime)"
    annotation (Dialog(tab="General", group="Medium"),
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
The control valve acts as a simple P-like controler to regulate a process variable 
(e.g., the relative filling level in a phase separator volume) to its setpoint via 
the mass flow through the valve.
</p>

<h4>Main equations</h4>
<p>
Three characteristics are available for the control behavior: A constant mass flow, 
a mass flow that is linear to the deviation from the setpoint, and a mass flow that 
is quadratic to the deviation from the setpoint:
</p>
<pre>
    'Constant': port_a.m_flow = <strong>if</strong> (processVariable - setPoint) &gt; 0 <strong>then</strong> m_flow_constant <strong>else</strong> -m_flow_constant;
</pre>
<pre>
    'Linear': port_a.m_flow = (processVariable - setPoint) * kp<sub>linear</sub>;
</pre>
<pre>
    'Quadratic': port_a.m_flow = (processVariable - setPoint)<sup>2</sup> * kp<sub>quadratic</sub>;
</pre>

<p>
If the valve is a check valve (see options), mass can only flow from port a to port 
b. To achiev this, the valve can act a 'normal' check valve or a 'strict' check 
valve. A 'normal' check valve regulates the mass flow rate to zero if the process
variables reaches its setpoint via the function
<a href=\"Modelica://SorpLib.Numerics.smoothTransition\">SorpLib.Numerics.smoothTransition</a>.
Thus, minimal mass flows from port b to port a are possible, which can increase 
the numerical stability of the model. These mass flows are not necessarily possible 
in reality, but they are usually so small that the error is negligible. In contract, 
a 'strict' check valve strictly regulates the mass flow rate to zero if the process
variables reaches its setpoint, and now flow reversal is possible.
</p>

<h4>Assumptions and limitations</h4>
<ul>
  <li>
  Incompressible fluid
  </li>
  <li>
  Adiabatic process
  </li>
  <li>
  Isenthalpic process
  </li>
  <li>
  No change in kinetic and potential energy between inlet and outlet
  </li>
  <li>
  No storage of mass or energy
  </li>
</ul>

<h4>Typical use</h4>
<p>
The valve is typically used in closed sorption systems (e.g., adsorption chillers) 
to return the condensate from the condenser into the evaporator.
</p>

<h4>Important parameters and options</h4>
<ul>
  <li>
  <i>checkValve</i>:
  Defines if the valve is a check valve, thus only allowing a mass flow from port
  a to port b except for a minor mass flow rate from port b to port a to increase
  numerical stability.
  </li>
  <li>
  <i>strictCheckValve</i>:
  Defines if the valve is a strict check valve, thus only allowing a mass flow 
  from port a to port b.
  </li>
  <br/>
  <li>
  <i>controlType</i>:
  Defines the control type for determining the mass flow rate at port a (see main
  equations).
  </li>
  <br/>
  <li>
  <i>avoid_events</i>:
  Defines if events shall be avoided via the noEvent()-operator.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  December 19, 2023, by Mirko Engelpracht:<br/>
  Major revisions (new functionalities, documentation).
  </li>
  <li>
  January 20, 2021, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end CondensateRefluxValve;
