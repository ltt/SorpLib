within SorpLib.Components.Valves.ValveCharacteristics.Tester;
model Test_AllValveCharacteristics
  "Tester for all valve characteristics"
  extends Modelica.Icons.Example;

  //
  // Definition of paramters
  //
  parameter Real fc_min = 0.5
    "Minimal flow coefficient if valve is almost closed (just for equal percentage
    valve characteristic)"
    annotation (Dialog(tab="General", group="Test Setup"));
  parameter Real fc_max = 1.56
    "Maximal flow coefficient"
    annotation (Dialog(tab="General", group="Test Setup"));
  parameter Real opening_min = 1e-2
    "Minimal valve opening (i.e., position) at which the minimal flow coefficient
    is reached (just for equal percentage valve characteristic)"
    annotation (Dialog(tab="General", group="Test Setup"));

  //
  // Definition of variables
  //
  Real opening(start=0, fixed=true)
    "Actual opening";

  Real fc_linear=
    SorpLib.Components.Valves.ValveCharacteristics.linearCharacteristic(
      opening=opening,
      fc_max=fc_max)
    "Linear valve characteristic";
  Real fc_quadratic=
    SorpLib.Components.Valves.ValveCharacteristics.quadraticCharacteristic(
      opening=opening,
      fc_max=fc_max)
    "Quadratic valve characteristic";
  Real fc_equalPercentage=
    SorpLib.Components.Valves.ValveCharacteristics.equalPercentageCharacteristic(
      opening=opening,
      fc_max=fc_max,
      fc_min=fc_min,
      opening_min=opening_min)
    "Equal percentage valve characteristic";

equation
  //
  // Calculate test setup
  //
  der(opening) = 1/2500
    "Actual opening";

  //
  // Annotations
  //
  annotation (experiment(StopTime=2500), Documentation(info="<html>
<p>
This model checks the valve characteristics.
<br/><br/>
To see the model behavior, plot the variables of the models over the 
time. The simulation time is correctly preset (Start: 0 s, Stop = 2500 s). 
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 19, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Test_AllValveCharacteristics;
