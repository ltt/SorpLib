within SorpLib.Components.Valves.ValveCharacteristics;
package Tester "Models to test and varify valve characteristic functions"
  extends Modelica.Icons.ExamplesPackage;

  annotation (Documentation(info="<html>
<p>
This package contains executable test models for all implemented valve
characteristics. 
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 19 2023, by Mirko Engelpracht:<br/>
  First implementation
  </li>
</ul>
</html>"));
end Tester;
