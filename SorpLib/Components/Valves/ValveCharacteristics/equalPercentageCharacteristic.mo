within SorpLib.Components.Valves.ValveCharacteristics;
function equalPercentageCharacteristic
  "Equal percentage valve characteristic"
  extends BaseClasses.PartialValveCharacteristic;

  //
  // Definition of inputs
  //
  input Real fc_min = 0.5
    "Minimal flow coefficient if valve is almost closed"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real opening_min = 1e-2
    "Minimal opening used for linear regularization"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of variables
  //
protected
  Real weightingFactor=
    SorpLib.Numerics.smoothTransition(
      x=opening,
      transitionPoint=opening_min,
      transitionLength=2*opening_min,
      noDiff=3)
    "Weighting factor used for linear regularization";

algorithm
  fc := weightingFactor * (opening / opening_min) *
    exp(fc_max / fc_min)^(opening_min - 1) * fc_max +
    (1-weightingFactor) * exp(fc_max / fc_min)^(opening - 1) * fc_max
    "Actual flow coefficient quadratically depends on the maximal flow coefficient";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
The equal percentage valve characteristic calculates the flow coefficient such
that the relative change of the flow coefficient is linearly proportional to
the change of the opening:
</p>
<pre>
    fc = <strong>exp</strong>(fc_max / fc_min)<sup>(opening - 1)</sup> * fc_max;
</pre>
<p>
If the opening is below a minimal opening <i>opening_min</i>, the flow coefficient
is calculated linearly proportional to the flow coefficient of the minimal opening:
</p>
<pre>
    fc = (opening / opening_min) * <strong>exp</strong>(fc_max / fc_min)<sup>(opening_min - 1)</sup> * fc_max;
</pre>
<p>
This function is based on the function
<a href=\"Modelica://Modelica.Fluid.Valves.BaseClasses.ValveCharacteristics.equalPercentage\">Modelica.Fluid.Valves.BaseClasses.ValveCharacteristics.equalPercentage</a>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 19, 2023, by Mirko Engelpracht:<br/>
  First implementation
  </li>
</ul>
</html>"));
end equalPercentageCharacteristic;
