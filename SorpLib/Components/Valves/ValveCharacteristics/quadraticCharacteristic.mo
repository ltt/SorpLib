within SorpLib.Components.Valves.ValveCharacteristics;
function quadraticCharacteristic "Quadratic valve characteristic"
  extends BaseClasses.PartialValveCharacteristic;

algorithm
  fc := opening^2 * fc_max
    "Actual flow coefficient quadratically depends on the maximal flow coefficient";
  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
The linear valve characteristic calculates the flow coefficient quadratically 
proportional to the maximum flow coefficient <i>fc_max</i>:
</p>
<pre>
    fc = opening<sup>2</sup> * fc_max;
</pre>
<p>
This function is based on the function
<a href=\"Modelica://Modelica.Fluid.Valves.BaseClasses.ValveCharacteristics.quadratic\">Modelica.Fluid.Valves.BaseClasses.ValveCharacteristics.quadratic</a>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 19, 2023, by Mirko Engelpracht:<br/>
  First implementation
  </li>
</ul>
</html>"));
end quadraticCharacteristic;
