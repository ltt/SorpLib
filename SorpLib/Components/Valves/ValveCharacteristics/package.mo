within SorpLib.Components.Valves;
package ValveCharacteristics "Package containing functions describing valve characteristics"
  extends Modelica.Icons.FunctionsPackage;

  annotation (Documentation(info="<html>
<p>
This package contains different functions describing valve characteristics. 
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 19, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end ValveCharacteristics;
