within SorpLib.Components.Valves.ValveCharacteristics;
function linearCharacteristic "Linear valve characteristic"
  extends BaseClasses.PartialValveCharacteristic;

algorithm
  fc := opening * fc_max
    "Actual flow coefficient linearly depends on the maximal flow coefficient";
  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
The linear valve characteristic calculates the flow coefficient linearly 
proportional to the maximum flow coefficient <i>fc_max</i>:
</p>
<pre>
    fc = opening * fc_max;
</pre>
<p>
This function is based on the function
<a href=\"Modelica://Modelica.Fluid.Valves.BaseClasses.ValveCharacteristics.linear\">Modelica.Fluid.Valves.BaseClasses.ValveCharacteristics.linear</a>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 19, 2023, by Mirko Engelpracht:<br/>
  First implementation
  </li>
</ul>
</html>"));
end linearCharacteristic;
