within SorpLib.Components.Valves.LiquidValves.Tester;
model Test_IncompressibleValve
  "Tester for the valve for incompressible fluids"
  extends Modelica.Icons.Example;

  //
  // Definition of boundary models
  //
  SorpLib.Basics.Sources.Fluids.LiquidSource[9] fs_a(
    each boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
    each boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    each p_fixed(displayUnit="bar") = 1000000,
    each T_fixed=303.15)
    "Fluid source a"
    annotation (Placement(transformation(extent={{-70,-10},{-50,10}})));

  SorpLib.Basics.Sources.Fluids.LiquidSource[9] fs_b(
    each boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
    each boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    each use_pInput=true,
    each use_TInput=true)
    "Fluid source b"
    annotation (Placement(transformation(extent={{70,-10},{50,10}})));

  //
  // Definition of valves
  //
  SorpLib.Components.Valves.LiquidValves.IncompressibleValve valve_controllable_Kvs_cType1(
    controllableOpening=true,
    useTimeConstant=false,
    leackage=false,
    checkValve=false,
    useKvsValue=true,
    valveCharacteristic=1) "Incompressible valve: Controllable, no time constant, no leackage, no check valve, 
    Kvs-approach, and characterisitc 1"
    annotation (Placement(transformation(extent={{-10,80},{10,100}})));
  SorpLib.Components.Valves.LiquidValves.IncompressibleValve valve_controllable_Kvs_cType2(
    controllableOpening=true,
    useTimeConstant=false,
    leackage=false,
    checkValve=false,
    useKvsValue=true,
    valveCharacteristic=2) "Incompressible valve: Controllable, no time constant, no leackage, no check valve, 
    Kvs-approach, and characterisitc 2"
    annotation (Placement(transformation(extent={{-10,60},{10,80}})));
  SorpLib.Components.Valves.LiquidValves.IncompressibleValve valve_controllable_Kvs_cType3(
    controllableOpening=true,
    useTimeConstant=false,
    leackage=false,
    checkValve=false,
    useKvsValue=true,
    valveCharacteristic=3) "Incompressible valve: Controllable, no time constant, no leackage, no check valve, 
    Kvs-approach, and characterisitc 3"
    annotation (Placement(transformation(extent={{-10,40},{10,60}})));

  SorpLib.Components.Valves.LiquidValves.IncompressibleValve valve_controllable_delay_Kvs_cType1(
    controllableOpening=true,
    useTimeConstant=true,
    tau=10,
    leackage=false,
    checkValve=false,
    useKvsValue=true,
    valveCharacteristic=1,
    opening_initial=0) "Incompressible valve: Controllable, time constant, no leackage, no check valve, 
    Kvs-approach, and characterisitc 1"
    annotation (Placement(transformation(extent={{-10,10},{10,30}})));
  SorpLib.Components.Valves.LiquidValves.IncompressibleValve valve_controllable_delay_Kvs_cType2(
    controllableOpening=true,
    useTimeConstant=true,
    tau=10,
    leackage=false,
    checkValve=false,
    useKvsValue=true,
    valveCharacteristic=2,
    opening_initial=0) "Incompressible valve: Controllable, time constant, no leackage, no check valve, 
    Kvs-approach, and characterisitc 2"
    annotation (Placement(transformation(extent={{-10,-10},{10,10}})));
  SorpLib.Components.Valves.LiquidValves.IncompressibleValve valve_controllable_delay_Kvs_cType3(
    controllableOpening=true,
    useTimeConstant=true,
    tau=10,
    leackage=false,
    checkValve=false,
    useKvsValue=true,
    valveCharacteristic=3,
    opening_initial=0) "Incompressible valve: Controllable, time constant, no leackage, no check valve, 
    Kvs-approach, and characterisitc 3"
    annotation (Placement(transformation(extent={{-10,-30},{10,-10}})));

  SorpLib.Components.Valves.LiquidValves.IncompressibleValve valve_controllable_delay_leackage_check_Kvs_cType1(
    controllableOpening=true,
    useTimeConstant=true,
    tau=1,
    leackage=true,
    checkValve=true,
    useKvsValue=true,
    valveCharacteristic=1,
    opening_initial=0) "Incompressible valve: Controllable, time constant, leackage, check valve, 
    Kvs-approach, and characterisitc 1"
    annotation (Placement(transformation(extent={{-10,-60},{10,-40}})));
  SorpLib.Components.Valves.LiquidValves.IncompressibleValve valve_controllable_delay_leackage_check_Kvs_cType2(
    controllableOpening=true,
    useTimeConstant=true,
    tau=1,
    leackage=true,
    checkValve=true,
    useKvsValue=true,
    valveCharacteristic=2,
    opening_initial=0) "Incompressible valve: Controllable, time constant, leackage, check valve, 
    Kvs-approach, and characterisitc 2"
    annotation (Placement(transformation(extent={{-10,-80},{10,-60}})));
  SorpLib.Components.Valves.LiquidValves.IncompressibleValve valve_controllable_delay_leackage_check_Kvs_cType3(
    controllableOpening=true,
    useTimeConstant=true,
    leackage=true,
    checkValve=true,
    useKvsValue=true,
    valveCharacteristic=3,
    opening_initial=0) "Incompressible valve: Controllable, time constant, leackage, check valve, 
    Kvs-approach, and characterisitc 3"
    annotation (Placement(transformation(extent={{-10,-100},{10,-80}})));

  //
  // Definition of input signals
  //
protected
  Modelica.Blocks.Sources.Trapezoid input_lRel(
    amplitude=1,
    rising=150,
    width=50,
    falling=0,
    period=250,
    offset=0)
    "Input signal for relative filling level"
    annotation (Placement(transformation(extent={{-100,-10},{-80,10}})));

  Modelica.Blocks.Sources.Sine input_T(
    amplitude=25,
    f=1/100,
    offset=273.15 + 50)
    "Input signal for temperature"
    annotation (Placement(transformation(extent={{100,-30},{80,-10}})));

equation
  //
  // Connections
  //
  connect(fs_a[1].port, valve_controllable_Kvs_cType1.port_a) annotation (Line(
      points={{-60,0},{-20,0},{-20,90},{-8,90}},
      color={28,108,200},
      thickness=1));
  connect(fs_a[2].port, valve_controllable_Kvs_cType2.port_a) annotation (Line(
      points={{-60,0},{-20,0},{-20,70},{-8,70}},
      color={28,108,200},
      thickness=1));
  connect(fs_a[3].port, valve_controllable_Kvs_cType3.port_a) annotation (Line(
      points={{-60,0},{-20,0},{-20,50},{-8,50}},
      color={28,108,200},
      thickness=1));
  connect(fs_a[4].port, valve_controllable_delay_Kvs_cType1.port_a) annotation (
      Line(
      points={{-60,0},{-20,0},{-20,20},{-8,20}},
      color={28,108,200},
      thickness=1));
  connect(fs_a[5].port, valve_controllable_delay_Kvs_cType2.port_a) annotation (
      Line(
      points={{-60,0},{-8,0}},
      color={28,108,200},
      thickness=1));
  connect(fs_a[6].port, valve_controllable_delay_Kvs_cType3.port_a) annotation (
      Line(
      points={{-60,0},{-20,0},{-20,-20},{-8,-20}},
      color={28,108,200},
      thickness=1));
  connect(fs_a[7].port, valve_controllable_delay_leackage_check_Kvs_cType1.port_a)
    annotation (Line(
      points={{-60,0},{-20,0},{-20,-50},{-8,-50}},
      color={28,108,200},
      thickness=1));
  connect(fs_a[8].port, valve_controllable_delay_leackage_check_Kvs_cType2.port_a)
    annotation (Line(
      points={{-60,0},{-20,0},{-20,-70},{-8,-70}},
      color={28,108,200},
      thickness=1));
  connect(fs_a[9].port, valve_controllable_delay_leackage_check_Kvs_cType3.port_a)
    annotation (Line(
      points={{-60,0},{-20,0},{-20,-90},{-8,-90}},
      color={28,108,200},
      thickness=1));

  connect(fs_b[1].port, valve_controllable_Kvs_cType1.port_b) annotation (Line(
      points={{60,0},{20,0},{20,90},{8,90}},
      color={28,108,200},
      thickness=1));
  connect(fs_b[2].port, valve_controllable_Kvs_cType2.port_b) annotation (Line(
      points={{60,0},{20,0},{20,70},{8,70}},
      color={28,108,200},
      thickness=1));
  connect(fs_b[3].port, valve_controllable_Kvs_cType3.port_b) annotation (Line(
      points={{60,0},{20,0},{20,50},{8,50}},
      color={28,108,200},
      thickness=1));
  connect(fs_b[4].port, valve_controllable_delay_Kvs_cType1.port_b) annotation (
      Line(
      points={{60,0},{20,0},{20,20},{8,20}},
      color={28,108,200},
      thickness=1));
  connect(fs_b[5].port, valve_controllable_delay_Kvs_cType2.port_b) annotation (
      Line(
      points={{60,0},{8,0}},
      color={28,108,200},
      thickness=1));
  connect(fs_b[6].port, valve_controllable_delay_Kvs_cType3.port_b) annotation (
      Line(
      points={{60,0},{20,0},{20,-20},{8,-20}},
      color={28,108,200},
      thickness=1));
  connect(fs_b[7].port, valve_controllable_delay_leackage_check_Kvs_cType1.port_b)
    annotation (Line(
      points={{60,0},{20,0},{20,-50},{8,-50}},
      color={28,108,200},
      thickness=1));
  connect(fs_b[8].port, valve_controllable_delay_leackage_check_Kvs_cType2.port_b)
    annotation (Line(
      points={{60,0},{20,0},{20,-70},{8,-70}},
      color={28,108,200},
      thickness=1));
  connect(fs_b[9].port, valve_controllable_delay_leackage_check_Kvs_cType3.port_b)
    annotation (Line(
      points={{60,0},{20,0},{20,-90},{8,-90}},
      color={28,108,200},
      thickness=1));

  for ind in 1:9 loop
    connect(input_T.y, fs_b[ind].T_input) annotation (Line(points={{79,-20},{70,
            -20},{70,-2},{61.2,-2}},
                          color={0,0,127}));
    connect(input_p.y, fs_b[ind].p_input)
      annotation (Line(points={{79,20},{70,20},{70,5},{61.2,5}},
                                                               color={0,0,127}));
  end for;

  connect(input_lRel.y, valve_controllable_Kvs_cType1.opening) annotation (Line(
        points={{-79,0},{-70,0},{-70,97},{0,97}}, color={0,0,127}));
  connect(input_lRel.y, valve_controllable_Kvs_cType2.opening) annotation (Line(
        points={{-79,0},{-70,0},{-70,77},{0,77}}, color={0,0,127}));
  connect(input_lRel.y, valve_controllable_Kvs_cType3.opening) annotation (Line(
        points={{-79,0},{-70,0},{-70,57},{0,57}}, color={0,0,127}));
  connect(input_lRel.y, valve_controllable_delay_Kvs_cType1.opening)
    annotation (Line(points={{-79,0},{-70,0},{-70,27},{0,27}}, color={0,0,127}));
  connect(input_lRel.y, valve_controllable_delay_Kvs_cType2.opening)
    annotation (Line(points={{-79,0},{-70,0},{-70,7},{0,7}}, color={0,0,127}));
  connect(input_lRel.y, valve_controllable_delay_Kvs_cType3.opening)
    annotation (Line(points={{-79,0},{-70,0},{-70,-13},{0,-13}}, color={0,0,127}));
  connect(input_lRel.y, valve_controllable_delay_leackage_check_Kvs_cType1.opening)
    annotation (Line(points={{-79,0},{-70,0},{-70,-43},{0,-43}}, color={0,0,127}));
  connect(input_lRel.y, valve_controllable_delay_leackage_check_Kvs_cType2.opening)
    annotation (Line(points={{-79,0},{-70,0},{-70,-63},{0,-63}}, color={0,0,127}));
  connect(input_lRel.y, valve_controllable_delay_leackage_check_Kvs_cType3.opening)
    annotation (Line(points={{-79,0},{-70,0},{-70,-83},{0,-83}}, color={0,0,127}));

  //
  // Annotations
  //
  annotation (experiment(StopTime=2500), Documentation(info="<html>
<p>
This model checks the incompressible valve.
<br/><br/>
To see the model behavior, plot the variables of the models over the 
time. The simulation time is correctly preset (Start: 0 s, Stop = 2500 s). 
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 19, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Test_IncompressibleValve;
