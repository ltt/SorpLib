within SorpLib.Components.Valves.LiquidValves;
model IncompressibleValve "Valve for incompressible fluids"
  extends SorpLib.Components.Valves.BaseClasses.PartialIncompressibleValve(
    redeclare final Basics.Interfaces.FluidPorts.LiquidPort_out port_b,
    redeclare final Basics.Interfaces.FluidPorts.LiquidPort_in port_a,
    final no_components=Medium.nX);

  //
  // Definition of parameters regarding the medium
  //
  replaceable package Medium = Modelica.Media.Water.WaterIF97_R1pT
    constrainedby Modelica.Media.Interfaces.PartialMedium
    "Medium model of the (ideal) liquid"
    annotation (Dialog(tab="General", group="Medium"),
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

equation
  //
  // Calculate fluid properties
  //
  rho_a = Medium.density_phX(
    p=port_a.p,
    h=inStream(port_a.h_outflow),
    X=cat(1,inStream(port_a.Xi_outflow),{1-sum(inStream(port_a.Xi_outflow))}))
    "Instreaming density at port a";
  rho_b = Medium.density_phX(
    p=port_b.p,
    h=inStream(port_b.h_outflow),
    X=cat(1,inStream(port_b.Xi_outflow),{1-sum(inStream(port_b.Xi_outflow))}))
    "Instreaming density at port b";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This valve can be applied for incompressible fluids, such as liquid water. The valve
opening (i.e., position) can be set via an input signal. The valve can be used as a
simple on/off valve (i.e., opening = 0/1) or as a control valve by setting the valve
opening betwenn 0 and 1.
</p>

<h4>Main equations</h4>
<p>
The mass flow rate <i>port_a.m_flow</i> is calculated in dependance of the pressure 
drop <i>&Delta;p = (port_a.p + &Delta;p<sub>hydraulic head</sub>) - port_b.p</i>, 
which can be increased by accounting a hydaulic head (see options), following the 
equation of Bernoulli for incompressible fluids:
</p>
<pre>
    port_a.m_flow = A<sub>eff</sub> * <strong>sqrt</strong>(2 * &rho; * &Delta;p);
</pre>
<p>
The effective area <i>A<sub>eff</sub></i> depends on the actual valve opening (i.e., 
position) and is calculated via the selected 
<a href=\"Modelica://SorpLib.Components.Valves.ValveCharacteristics\">valve characteristic</a>).
For this purpose, the maximal effective area <i>A<sub>eff,max</sub></i> is required.
The maximal effective area <i>A<sub>eff,max</sub></i> is present if the valve is fully
opened and can directly be specified. Alternatively, it can be calculated using the maximal 
flow coefficient <i>Kvs</i>, reference density <i>&rho;<sub>ref</sub></i>, and reference 
pressure drop <i>&Delta;p<sub>ref</sub></i>:
</p>
<pre>
    <i>A<sub>eff,max</sub></i> = Kvs * <strong>sqrt</strong>(&rho;<sub>ref</sub> / (2 * &Delta;p<sub>ref</sub>));
</pre>
<p>
The density <i>&rho;</i> corresponds to the inlet density of the actual flow direction.
To account for the flow direction, the root expression is calculated using the anti-symmetric 
root approximation
<a href=\"Modelica://SorpLib.Numerics.regSquareWFactors\">SorpLib.Numerics.regSquareWFactors</a>.
Furthermore, the valve can be a check valve (see options). If the valve is a check valve, mass 
can only flow from port a to port b.
</p>

<h4>Assumptions and limitations</h4>
<ul>
  <li>
  Incompressible fluid
  </li>
  <li>
  Adiabatic process
  </li>
  <li>
  Isenthalpic process
  </li>
  <li>
  No storage of mass or energy
  </li>
</ul>

<h4>Typical use</h4>
<p>
The valve is typically used in hydraulic networks of closed sorption systems.
</p>

<h4>Important parameters and options</h4>
<ul>
  <li>
  <i>controllableOpening</i>:
  Defines if the valve opening can be set via an input signal.
  </li>
  <li>
  <i>useTimeConstant</i>:
  Defines if the valve opening input signal is delayed by a time constant.
  </li>
  <li>
  <i>leackage</i>:
  Defines if the valve has a small leackage flow.
  </li>
  <li>
  <i>checkValve</i>:
  Defines if the valve is a strict check valve, thus only allowing a mass flow from 
  port a to port b.
  </li>
  <br/>
  <li>
  <i>useKvsValue</i>:
  Defines if the mass flow rate is calculated using the Kvs-approach or A_eff-approach.
  </li>
  <li>
  <i>valveCharacteristic</i>:
  Defines the valve characteristic (see 
  <a href=\"Modelica://SorpLib.Components.Valves.ValveCharacteristics\">SorpLib.Components.Valves.ValveCharacteristics</a>).
  </li>
  <li>
  <i>accountHydraulicHead</i>:
  Defines if a hydraulic head is used to increase the pressure at port a.
  </li>
  <br/>
  <li>
  <i>avoid_events</i>:
  Defines if events shall be avoided via the noEvent()-operator.
  </li>
</ul>

<h4>Dynamics</h4>
<p>
The model has two dynamic states if the input signal describing the valve opening is
delayed by a time constant. Otherwise, the model has no dynamic states.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 19, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end IncompressibleValve;
