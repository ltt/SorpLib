within SorpLib.Components.Valves.GasValves;
model CompressibleValve "Valve for compressible fluids"
  extends SorpLib.Components.Valves.BaseClasses.PartialCompressibleValve(
    redeclare final Basics.Interfaces.FluidPorts.GasPort_out port_b,
    redeclare final Basics.Interfaces.FluidPorts.GasPort_out port_a,
    final no_components=Medium.nX);

  //
  // Definition of parameters regarding the medium
  //
  replaceable package Medium =
    SorpLib.Media.IdealGasVaporMixtures.MoistAir_N2_O2_CO2_H2O
    constrainedby Modelica.Media.Interfaces.PartialMedium
    "Medium model of the ideal gas, ideal gas mixture, or ideal gas-vapor mixture"
    annotation (Dialog(tab="General", group="Medium"),
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

  //
  // Definition of variabbles
  //
protected
  Medium.ThermodynamicState state_a_in
    "Thermodynamic state record with instreaming properties at port a";
  Medium.ThermodynamicState state_b_in
    "Thermodynamic state record with instreaming properties at port b";

equation
  //
  // Calculate fluid properties
  //
  state_a_in = Medium.setState_phX(
    p=port_a.p,
    h=inStream(port_a.h_outflow),
    X=cat(1,inStream(port_a.Xi_outflow),{1-sum(inStream(port_a.Xi_outflow))}))
    "Thermodynamic state record with instreaming properties at port a";
  state_b_in = Medium.setState_phX(
    p=port_b.p,
    h=inStream(port_b.h_outflow),
    X=cat(1,inStream(port_b.Xi_outflow),{1-sum(inStream(port_b.Xi_outflow))}))
    "Thermodynamic state record with instreaming properties at port b";

  rho_a = Medium.density(state=state_a_in)
    "Instreaming density at port a";
  rho_b = Medium.density(state=state_b_in)
    "Instreaming density at port b";

  gamma_a = Medium.isentropicExponent( state=state_a_in)
    "Instreaming isentropic exponent at port a";
  gamma_b = Medium.isentropicExponent( state=state_b_in)
    "Instreaming isentropic exponent at port b";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This valve can be applied for compressible fluids, such as air. The valve opening 
(i.e., position) can be set via an input signal. The valve can be used as a simple 
on/off valve (i.e., opening = 0/1) or as a control valve by setting the valve opening 
betwenn 0 and 1.
</p>

<h4>Main equations</h4>
<p>
The mass flow rate <i>port_a.m_flow</i> is calculated in dependance of the instreaming
pressure  <i>p</i>, instreaming density <i>&rho;</i>, and discharge function <i>&Psi;</i>,
following the equation of Bernoulli for compressible fluids (i.e., equation of Saint-Venant 
& Wantzel):
</p>
<pre>
    port_a.m_flow = A<sub>eff</sub> * <strong>sqrt</strong>(2 * &rho; * p) * &Psi;;
</pre>
<p>
The effective area <i>A<sub>eff</sub></i> depends on the actual valve opening (i.e., 
position) and is calculated via the selected 
<a href=\"Modelica://SorpLib.Components.Valves.ValveCharacteristics\">valve characteristic</a>).
For this purpose, the maximal effective area <i>A<sub>eff,max</sub></i> is required.
The maximal effective area <i>A<sub>eff,max</sub></i> is present if the valve is fully
opened and can directly be specified. Alternatively, it can be calculated using the maximal 
flow coefficient <i>Kvs</i>, reference density <i>&rho;<sub>ref</sub></i>, and reference 
pressure drop <i>&Delta;p<sub>ref</sub></i>:
</p>
<pre>
    <i>A<sub>eff,max</sub></i> = Kvs * <strong>sqrt</strong>(&rho;<sub>ref</sub> / (2 * &Delta;p<sub>ref</sub>));
</pre>
<p>
The discharge function <i>&Psi;</i> depends on the pressure ratio between the outstreaming and
instreaming pressure <i>p<sub>out</sub>/p<sub>in</sub></i> as long as the velocity is below the
speed of sound. Once the speed of sound is reached, the discharge function does not change anymore:
</p>
<pre>
    &Psi; = <strong>if</strong> p<sub>out</sub>/p<sub>in</sub> &gt; (2 / (&gamma; + 1)) ^ (&gamma; / (&gamma; - 1)) <strong>then</strong> 
         <strong>sqrt</strong>(&gamma; / (&gamma; - 1) * ((p<sub>out</sub>/p<sub>in</sub>) ^ (2 / &gamma;) - (p<sub>out</sub>/p<sub>in</sub>) ^ ((&gamma; + 1) / &gamma;))) <strong>else</strong>
         <strong>sqrt</strong>(2 ^ (2 / (&gamma; - 1)) * &gamma; / (&gamma; + 1) ^ ((&gamma; + 1) / (&gamma; - 1)));
</pre>
<p>
To account for the flow direction, the root expression is calculated using the anti-symmetric 
root approximation
<a href=\"Modelica://SorpLib.Numerics.regSquareWFactors\">SorpLib.Numerics.regSquareWFactors</a>.
Furthermore, the valve can be a check valve (see options). If the valve is a check valve, mass 
can only flow from port a to port b.
</p>

<h4>Assumptions and limitations</h4>
<ul>
  <li>
  Compressible fluid
  </li>
  <li>
  Adiabatic process
  </li>
  <li>
  Isenthalpic process
  </li>
  <li>
  Isentropic process
  </li>
  <li>
  No storage of mass or energy
  </li>
</ul>

<h4>Typical use</h4>
<p>
The valve is typically used in hydraulic networks of open sorption systems.
</p>

<h4>Important parameters and options</h4>
<ul>
  <li>
  <i>controllableOpening</i>:
  Defines if the valve opening can be set via an input signal.
  </li>
  <li>
  <i>useTimeConstant</i>:
  Defines if the valve opening input signal is delayed by a time constant.
  </li>
  <li>
  <i>leackage</i>:
  Defines if the valve has a small leackage flow.
  </li>
  <li>
  <i>checkValve</i>:
  Defines if the valve is a strict check valve, thus only allowing a mass flow from 
  port a to port b.
  </li>
  <br/>
  <li>
  <i>useKvsValue</i>:
  Defines if the mass flow rate is calculated using the Kvs-approach or A_eff-approach.
  </li>
  <li>
  <i>valveCharacteristic</i>:
  Defines the valve characteristic (see 
  <a href=\"Modelica://SorpLib.Components.Valves.ValveCharacteristics\">SorpLib.Components.Valves.ValveCharacteristics</a>).
  </li>
  <br/>
  <li>
  <i>avoid_events</i>:
  Defines if events shall be avoided via the noEvent()-operator.
  </li>
</ul>

<h4>Dynamics</h4>
<p>
The model has two dynamic states if the input signal describing the valve opening is
delayed by a time constant. Otherwise, the model has no dynamic states.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 19, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end CompressibleValve;
