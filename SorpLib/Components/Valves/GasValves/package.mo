within SorpLib.Components.Valves;
package GasValves "Gas, gas mixture, and gas-vapor mixture valves"
  extends Modelica.Icons.VariantsPackage;

  annotation (Documentation(info="<html>
<p>
This package contains gas, gas mixture, and gas-vapor mixture valves based on 
the open-source Modelica Standard Library (MSL).
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end GasValves;
