within SorpLib.Components.Valves.GasValves;
model CompressibleThreeWayValve
  "Three-way valve for compressible fluids"
  extends
    SorpLib.Components.Valves.BaseClasses.PartialCompressibleThreeWayValve(
    redeclare final Basics.Interfaces.FluidPorts.GasPort_out port_c,
    redeclare final Basics.Interfaces.FluidPorts.GasPort_out port_b,
    redeclare final Basics.Interfaces.FluidPorts.GasPort_in port_a,
    final no_components=Medium.nX);

  //
  // Definition of parameters regarding the medium
  //
  replaceable package Medium =
    SorpLib.Media.IdealGasVaporMixtures.MoistAir_N2_O2_CO2_H2O
    constrainedby Modelica.Media.Interfaces.PartialMedium
    "Medium model of the ideal gas, ideal gas mixture, or ideal gas-vapor mixture"
    annotation (Dialog(tab="General", group="Medium"),
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

  //
  // Definition of variabbles
  //
protected
  Medium.ThermodynamicState state_a_in
    "Thermodynamic state record with instreaming properties at port a";

equation
  //
  // Calculate fluid properties
  //
  state_a_in = Medium.setState_phX(
    p=port_a.p,
    h=inStream(port_a.h_outflow),
    X=cat(1,inStream(port_a.Xi_outflow),{1-sum(inStream(port_a.Xi_outflow))}))
    "Thermodynamic state record with instreaming properties at port a";

  rho_a = Medium.density(state=state_a_in)
    "Instreaming density at port a";
  gamma_a = Medium.isentropicExponent( state=state_a_in)
    "Instreaming isentropic exponent at port a";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This three-way valve can be applied for compressible fluids, such as air. 
The valve splits a mass flow entering port a into two mass flows leaving port b 
and c. For thus purpose, the valve position defines the division of the instreaming 
mass flow at port a: I.e., for <i>position = 0</i>, the mass flows to port b; 
for <i>position = 1</i>, the mass flows to port c. The position can be set by an 
input signal.
</p>

<h4>Main equations</h4>
<p>
The mass flow rates <i>port_b.m_flow</i> and <i>port_c.m_flow</i> are calculated 
in dependance of the instreaming pressure <i>p</i>, instreaming density <i>&rho;</i>, 
and discharge function <i>&Psi;<sub>i</sub></i>, following the equation of Bernoulli 
for compressible fluids (i.e., equation of Saint-Venant & Wantzel):
</p>
<pre>
    port_b.m_flow = -A<sub>eff,b</sub> * <strong>sqrt</strong>(2 * &rho; * p) * &Psi;<sub>a</sub>;
</pre>
<pre>
    port_c.m_flow = -A<sub>eff,c</sub> * <strong>sqrt</strong>(2 * &rho; * p) * &Psi;<sub>c</sub>;
</pre>
<p>
The effective areas <i>A<sub>eff,i</sub></i> depends on the actual valve position
and are calculated via a linear valve characteristic. For this purpose, the maximal 
effective areas <i>A<sub>eff,max,i</sub></i> are required. The maximal effective 
areas <i>A<sub>eff,max,i</sub></i> are present if the valve is fully opened and 
can directly be specified. Alternatively, they can be calculated using the maximal 
flow coefficients <i>Kvs<sub>i</sub></i>, reference densities <i>&rho;<sub>ref,i</sub></i>, 
and reference pressure drops <i>&Delta;p<sub>ref,i</sub></i>:
</p>
<pre>
    <i>A<sub>eff,max,i</sub></i> = Kvs<sub>i</sub> * <strong>sqrt</strong>(&rho;<sub>ref,i</sub> / (2 * &Delta;p<sub>ref,i</sub>));
</pre>
<p>
The discharge functions <i>&Psi;<sub>i</sub></i> depend on the pressure ratios between the 
outstreaming and instreaming pressures <i>p<sub>out,i</sub>/p<sub>in</sub></i> as long as 
the velocity is below the speed of sound. Once the speed of sound is reached, the discharge 
functions do not change anymore:
</p>
<pre>
    &Psi;<sub>i</sub> = <strong>if</strong> p<sub>out,i</sub>/p<sub>in</sub> &gt; (2 / (&gamma; + 1)) ^ (&gamma; / (&gamma; - 1)) <strong>then</strong> 
         <strong>sqrt</strong>(&gamma; / (&gamma; - 1) * ((p<sub>out,i</sub>/p<sub>in</sub>) ^ (2 / &gamma;) - (p<sub>out,i</sub>/p<sub>in</sub>) ^ ((&gamma; + 1) / &gamma;))) <strong>else</strong>
         <strong>sqrt</strong>(2 ^ (2 / (&gamma; - 1)) * &gamma; / (&gamma; + 1) ^ ((&gamma; + 1) / (&gamma; - 1)));
</pre>
<p>
Not that the valve is a check valve, thus mass can only flow from port a to port b and c.
</p>

<h4>Assumptions and limitations</h4>
<ul>
  <li>
  Compressible fluid
  </li>
  <li>
  Adiabatic process
  </li>
  <li>
  Isenthalpic process
  </li>
  <li>
  Isentropic process
  </li>
  <li>
  No storage of mass or energy
  </li>
</ul>

<h4>Typical use</h4>
<p>
The valve is typically used in hydraulic networks of open sorption systems to
split a mass flow rate.
</p>

<h4>Important parameters and options</h4>
<ul>
  <li>
  <i>controllablePosition</i>:
  Defines if the valve position can be set via an input signal.
  </li>
  <li>
  <i>useTimeConstant</i>:
  Defines if the valve position input signal is delayed by a time constant.
  </li>
  <li>
  <i>leackage</i>:
  Defines if the valve has a small leackage flow.
  </li>
  <br/>
  <li>
  <i>useKvsValue</i>:
  Defines if the mass flow rates at port b and c are calculated using the 
  Kvs-approach or A_eff-approach.
  </li>
  <br/>
  <li>
  <i>avoid_events</i>:
  Defines if events shall be avoided via the noEvent()-operator.
  </li>
</ul>

<h4>Dynamics</h4>
<p>
The model has two dynamic states if the input signal describing the valve position
is delayed by a time constant. Otherwise, the model has no dynamic states.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 19, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end CompressibleThreeWayValve;
