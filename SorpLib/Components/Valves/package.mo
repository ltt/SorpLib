within SorpLib.Components;
package Valves "Valves to regulate and control fluid flows"
  extends SorpLib.Icons.ValvesPackage;

  annotation (Documentation(info="<html>
<p>
This package includes various valves for (ideal) liquids, ideal gases, ideal gas 
mixtures, ideal gas-vapor mixtures, and real substances (i.e., with a two-phase 
region). The valve types include simple orfice valves for incompressible or 
compressible fluids, three-way valves for incompressible or compressible fluids, 
and P-like control valves.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end Valves;
