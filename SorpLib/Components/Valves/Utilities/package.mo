within SorpLib.Components.Valves;
package Utilities "Utility models and functions for all valves"
  extends Modelica.Icons.UtilitiesPackage;

  annotation (Documentation(info="<html>
<p>
This package contains utility models and functions used to create valve models. 
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 19, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Utilities;
