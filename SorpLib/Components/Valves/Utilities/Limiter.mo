within SorpLib.Components.Valves.Utilities;
block Limiter
  "Limiter that limits a signal to an upper and a lower bound"
  extends Modelica.Blocks.Interfaces.SISO;

  //
  // Definition of parameters
  //
  parameter Real lowerLimit = 0
    "Lower limit"
    annotation (Dialog(tab="General", group="Limits"));
  parameter Real upperLimit = 1
    "Upper limit"
    annotation (Dialog(tab="General", group="Limits"));

equation
  //
  // Assertations
  //
  assert(upperLimit >= lowerLimit,
        "Limits must be consistent, but upper limit (" + String(upperLimit) +
        ") is greater than lower limit (" + String(upperLimit) + ")!",
        level = AssertionLevel.error);

  //
  // Limit signal
  //
  y = smooth(0, max(min(u, upperLimit), lowerLimit))
    "Limited input signal";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This blocks limit the input signal <i>u</i> to the lower limit <i>lowerLimit</i>
and upper limit <i>upperLimit</i>. This block is a simplified version of
<a href=\"Modelica://Modelica.Blocks.Nonlinear.Limiter\">Modelica.Blocks.Nonlinear.Limiter</a>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 19, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"), Icon(graphics={
    Polygon(
      points={{0,90},{-8,68},{8,68},{0,90}},
      lineColor={192,192,192},
      fillColor={192,192,192},
      fillPattern=FillPattern.Solid),
    Line(points={{0,-90},{0,68}}, color={192,192,192}),
    Line(points={{-90,0},{68,0}}, color={192,192,192}),
    Polygon(
      points={{90,0},{68,-8},{68,8},{90,0}},
      lineColor={192,192,192},
      fillColor={192,192,192},
      fillPattern=FillPattern.Solid),
    Line(points={{-80,-70},{-50,-70},{50,70},{80,70}})}));
end Limiter;
