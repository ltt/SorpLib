within SorpLib.Components.Valves.Utilities;
function mixStreamVariables
  "Function mixing two stream variables of three-way valves"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.MassFlowRate m_flow_1
    "Flow variable of first stream variable"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.MassFlowRate m_flow_2
    "Flow variable of second stream variable"
    annotation (Dialog(tab="General", group="Inputs"));

  input Real streamVariable_1
    "First stream variable"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real streamVariable_2
    "Second stream variable"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of outputs
  //
  output Real mixedStreamVariable
    "Mixed stream variable"
    annotation (Dialog(tab="General", group="Outputs", enable=false));

  //
  // Definition of constants
  //


algorithm
  mixedStreamVariable := (streamVariable_1 * (max(m_flow_1,0) +
    max(-m_flow_2,0) + 0.5*Modelica.Constants.eps) + streamVariable_2 *
    (max(m_flow_2,0) + max(-m_flow_1,0) +0.5*Modelica.Constants.eps)) /
    (abs(m_flow_1) + abs(m_flow_2) + Modelica.Constants.eps)
    "Mixed stream variable";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function mixes two stream variables as required by three-way valves. The
function is based on the Modelica Standard library (MSL)
(<a href=\"Modelica://Modelica.Fluid.Fittings.MultiPort\">Modelica.Fluid.Fittings.MultiPort</a>)
and TIL library
(<a href=\"Modelica://TIL.Internals.joiningTwoStreams\">TIL.Internals.joiningTwoStreams</a>).
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 19, 2023, by Mirko Engelpracht:<br/>
  First implementation
  </li>
</ul>
</html>"));
end mixStreamVariables;
