within SorpLib.Components.Tubes;
model LiquidTube
  "Tube model that is one 1D discretized in the flow direction"
  extends BaseClasses.PartialTube(
    redeclare final Basics.Interfaces.FluidPorts.LiquidPort_in fp_a,
    redeclare final Basics.Interfaces.FluidPorts.LiquidPort_out fp_b,
    final no_components=Medium.nX,
    final type_energyBalance=
    SorpLib.Choices.BalanceEquations.TransientFixedInitial,
    redeclare Basics.Volumes.FluidVolumes.LiquidVolume fluidVolumes(
      redeclare each final package Medium = Medium,
      redeclare each final Basics.Volumes.Records.VolumeGeometry geometry(
        dx=geometry.l/geometry.no_fluidVolumes,
        dy=geometry.d_hydInner,
        dz=geometry.d_hydInner,
        A_xy=-1,
        A_xz=geometry.A_heatTransferInner/geometry.no_fluidVolumes,
        A_yz=geometry.A_hydCrossInner,
        V=geometry.V_inner/geometry.no_fluidVolumes*geometry.no_hydraulicParallelTubes)),
    redeclare Basics.Volumes.SolidVolumes.SolidVolume wallVolumes(
      redeclare each final WallMaterial solidMedium,
      redeclare each final Basics.Volumes.Records.VolumeGeometry geometry(
        dx=geometry.l/geometry.no_wallVolumes,
        dy=geometry.t_wall/2,
        dz=Modelica.Constants.pi*(geometry.d_hydInner + geometry.d_hydOuter)/2,
        A_xy=geometry.t_wall*geometry.l/geometry.no_wallVolumes,
        A_xz=Modelica.Constants.pi*(geometry.d_hydInner + geometry.d_hydOuter)/2
            *geometry.l/geometry.no_wallVolumes,
        A_yz=geometry.A_hydCrossWall/geometry.no_wallVolumes,
        V=geometry.V_wall/geometry.no_wallVolumes*geometry.no_hydraulicParallelTubes),
      final T_initial=T_wallInitial,
      final p = p_wall,
      each independentStateVariables=SorpLib.Choices.IndependentVariablesVolume.pTX),
    redeclare Fittings.Resistors.LiquidTubeResistance pressureDrop(
      redeclare each final model PressureLossModel = PressureDrop,
      redeclare each final package Medium = Medium,
      redeclare each final SorpLib.Components.Fittings.Records.GeometryTube geometry(
        l=geometry.l/(geometry.no_fluidVolumes-1),
        d_hyd_a=geometry.d_hydInner,
        d_hyd_b=geometry.d_hydInner,
        roughness=geometry.roughness,
        no_hydraulicParallelFlows=geometry.no_hydraulicParallelTubes)),
    thermalConduction_wall1(final fluidProperties=
      SorpLib.Components.HeatTransfer.Records.FluidProperties(
        p=wallVolumes[1:no_wallVolumes-1].solidProperties.p,
        T=wallVolumes[1:no_wallVolumes-1].solidProperties.T,
        rho={1/wallVolumes[i].solidProperties.v for i in 1:no_wallVolumes-1},
        cp=wallVolumes[1:no_wallVolumes-1].solidProperties.c,
        eta=0,
        lambda=wallVolumes[1:no_wallVolumes-1].solidProperties.lambda)),
    thermalConduction_wall2(final fluidProperties=
      SorpLib.Components.HeatTransfer.Records.FluidProperties(
        p=wallVolumes[2:no_wallVolumes].solidProperties.p,
        T=wallVolumes[2:no_wallVolumes].solidProperties.T,
        rho={1/wallVolumes[i].solidProperties.v for i in 2:no_wallVolumes},
        cp=wallVolumes[2:no_wallVolumes].solidProperties.c,
        eta=0,
        lambda=wallVolumes[2:no_wallVolumes].solidProperties.lambda)),
    thermalConduction_wallToHeatPort(final fluidProperties=
      SorpLib.Components.HeatTransfer.Records.FluidProperties(
        p=wallVolumes[:].solidProperties.p,
        T=wallVolumes[:].solidProperties.T,
        rho={1/wallVolumes[i].solidProperties.v for i in 1:no_wallVolumes},
        cp=wallVolumes[:].solidProperties.c,
        eta=0,
        lambda=wallVolumes[:].solidProperties.lambda)));

  //
  // Definition of parameters
  //
  replaceable package Medium = Modelica.Media.Water.ConstantPropertyLiquidWater
    constrainedby Modelica.Media.Interfaces.PartialMedium
    "Medium model of the (ideal) liquid"
    annotation (Dialog(tab="General", group="Medium"),
                choicesAllMatching = true,
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

  replaceable model WallMaterial =
      SorpLib.Media.Solids.MetalsAndMetalAlloys.Copper
    constrainedby SorpLib.Media.Solids.BaseClasses.PartialSolid
    "Solid wall medium"
    annotation (Dialog(tab="General", group="Medium"),
                choicesAllMatching = true,
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

  //
  // Definition of state records
  //
  Medium.ThermodynamicState state_a = Medium.setState_phX(
    p = fp_a.p,
    h = if avoid_events then noEvent(actualStream(fp_a.h_outflow)) else
      actualStream(fp_a.h_outflow),
    X = if avoid_events then noEvent(actualStream(fp_a.Xi_outflow)) else
      actualStream(fp_a.Xi_outflow))
    "Record describing current state properties at port a";
  Medium.ThermodynamicState state_b = Medium.setState_phX(
    p = fp_b.p,
    h = if avoid_events then noEvent(actualStream(fp_b.h_outflow)) else
      actualStream(fp_b.h_outflow),
    X = if avoid_events then noEvent(actualStream(fp_b.Xi_outflow)) else
      actualStream(fp_b.Xi_outflow))
    "Record describing current state properties at port b";

equation
  //
  // Calculate difference enthalpy stream
  //
  DH_flow = m_flow_a * Medium.specificEnthalpy(state=state_a) +
    m_flow_a * Medium.specificEnthalpy(state=state_b)
    "Difference enthalpy flow rate between port a and port b";

  //
  // Calculate fluid properties
  //
  if no_fluidVolumes == no_wallVolumes then
    //
    // Identical discretization number
    //
    propertiesWall.p = wallVolumes.solidProperties.p
      "Pressures used to calcualte conductive heat transfer at fluid side";
    propertiesWall.T = wallVolumes.solidProperties.T
      "Temperatures used to calcualte conductive heat transfer at fluid side";
    propertiesWall.rho = 1 ./ wallVolumes.solidProperties.v
      "Densities used to calcualte conductive heat transfer at fluid side";
    propertiesWall.cp = wallVolumes.solidProperties.c
      "Specific heat capacities used to calcualte conductive heat transfer at 
      fluid side";
    propertiesWall.eta = zeros(no_wallVolumes)
      "Dynamic viscosities used to calcualte conductive heat transfer at 
      fluid side";
    propertiesWall.lambda = wallVolumes.solidProperties.lambda
      "Thermal conductivities used to calcualte conductive heat transfer at 
      fluid side";

  elseif no_fluidVolumes > no_wallVolumes then
    //
    // More fluid volumes than wall volumes
    //
    propertiesWall.p = wallVolumes.solidProperties.p
      "Pressures used to calcualte conductive heat transfer at fluid side";
    propertiesWall.T = wallVolumes.solidProperties.T
      "Temperatures used to calcualte conductive heat transfer at fluid side";
    propertiesWall.rho = 1 ./ wallVolumes.solidProperties.v
      "Densities used to calcualte conductive heat transfer at fluid side";
    propertiesWall.cp = wallVolumes.solidProperties.c
      "Specific heat capacities used to calcualte conductive heat transfer at 
      fluid side";
    propertiesWall.eta = zeros(no_wallVolumes)
      "Dynamic viscosities used to calcualte conductive heat transfer at 
      fluid side";
    propertiesWall.lambda = wallVolumes.solidProperties.lambda
      "Thermal conductivities used to calcualte conductive heat transfer at 
      fluid side";

  else
    //
    // More wall volumes than fluid volumes
    //
    if fluidPropertyPosition == 1 then
      for i in 1:no_fluidVolumes loop
        propertiesWall[i].p =
          wallVolumes[1+(i-1)*factorDiscretization].solidProperties.p
          "Pressures used to calcualte conductive heat transfer at fluid side";
        propertiesWall[i].T =
          wallVolumes[1+(i-1)*factorDiscretization].solidProperties.T
          "Temperatures used to calcualte conductive heat transfer at fluid side";
        propertiesWall[i].rho =
          1 ./ wallVolumes[1+(i-1)*factorDiscretization].solidProperties.v
          "Densities used to calcualte conductive heat transfer at fluid side";
        propertiesWall[i].cp =
          wallVolumes[1+(i-1)*factorDiscretization].solidProperties.c
          "Specific heat capacities used to calcualte conductive heat transfer at 
          fluid side";
        propertiesWall[i].eta = 0
          "Dynamic viscosities used to calcualte conductive heat transfer at 
          fluid side";
        propertiesWall[i].lambda =
          wallVolumes[1+(i-1)*factorDiscretization].solidProperties.lambda
          "Thermal conductivities used to calcualte conductive heat transfer at 
          fluid side";
      end for;

    elseif fluidPropertyPosition == 2 then
      for i in 1:no_fluidVolumes loop
        propertiesWall[i].p =
          wallVolumes[i*factorDiscretization].solidProperties.p
          "Pressures used to calcualte conductive heat transfer at fluid side";
        propertiesWall[i].T =
          wallVolumes[i*factorDiscretization].solidProperties.T
          "Temperatures used to calcualte conductive heat transfer at fluid side";
        propertiesWall[i].rho =
          1 ./ wallVolumes[i*factorDiscretization].solidProperties.v
          "Densities used to calcualte conductive heat transfer at fluid side";
        propertiesWall[i].cp =
          wallVolumes[i*factorDiscretization].solidProperties.c
          "Specific heat capacities used to calcualte conductive heat transfer at 
          fluid side";
        propertiesWall[i].eta = 0
          "Dynamic viscosities used to calcualte conductive heat transfer at 
          fluid side";
        propertiesWall[i].lambda =
          wallVolumes[i*factorDiscretization].solidProperties.lambda
          "Thermal conductivities used to calcualte conductive heat transfer at 
          fluid side";
      end for;

    else
      for i in 1:no_fluidVolumes loop
        propertiesWall[i].p = 1/factorDiscretization *
          sum(wallVolumes[1+(i-1)*factorDiscretization:
          i*factorDiscretization].solidProperties.p)
          "Pressures used to calcualte conductive heat transfer at fluid side";
        propertiesWall[i].T = 1/factorDiscretization *
          sum(wallVolumes[1+(i-1)*factorDiscretization:
          i*factorDiscretization].solidProperties.T)
          "Temperatures used to calcualte conductive heat transfer at fluid side";
        propertiesWall[i].rho = 1/factorDiscretization *
          sum(1 ./ wallVolumes[1+(i-1)*factorDiscretization:
          i*factorDiscretization].solidProperties.v)
          "Densities used to calcualte conductive heat transfer at fluid side";
        propertiesWall[i].cp = 1/factorDiscretization *
          sum(wallVolumes[1+(i-1)*factorDiscretization:
          i*factorDiscretization].solidProperties.c)
          "Specific heat capacities used to calcualte conductive heat transfer at 
          fluid side";
        propertiesWall[i].eta = 0
          "Dynamic viscosities used to calcualte conductive heat transfer at 
          fluid side";
        propertiesWall[i].lambda = 1/factorDiscretization *
          sum(wallVolumes[1+(i-1)*factorDiscretization:
          i*factorDiscretization].solidProperties.lambda)
          "Thermal conductivities used to calcualte conductive heat transfer at 
          fluid side";
      end for;
    end if;
  end if;

  //
  // Annotations
  //

  annotation (Documentation(revisions="<html>
<ul>
  <li>
  January 29, 2024, by Mirko Engelpracht:<br/>
  Major adaptations due to restructering of the library and documentation.
  </li>
  <li>
  January 14, 2021, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>", info="<html>
<p>
The tube model is 1D-discretized in axial direction via an upwing discretization
scheme. It models the liquid flow through a pipe or a bunch of parallel pipes,
considering the friction-based pressure drop and heat transfer between the fluid
and wall. By design, the tube uses circular cross-sectional areas, but non-circular
cross-sectional areas can be used via appropriate hydraulic geometry parameter.
</p>

<h4>Main equations</h4>
<p>
The tube model has a steady-state/transient mass balance for the liquid, and
a transient energy balance for the liquid and wall. For a detailed documentation,
see
<a href=\"Modelica://SorpLib.Basics.Volumes.FluidVolumes.LiquidVolume\">SorpLib.Basics.Volumes.FluidVolumes.LiquidVolume</a>
and
<a href=\"Modelica://SorpLib.Basics.Volumes.SolidVolumes.SolidVolume\">SorpLib.Basics.Volumes.SolidVolumes.SolidVolume</a>.
<br/<br/>
The fluid volumes are coupled to each other via a hydraulic resistor, which
allows to consider different pressure drop correlations. For a detailed documentation,
see
<a href=\"Modelica://SorpLib.Components.Fittings.Resistors.LiquidTubeResistance\">SorpLib.Components.Fittings.Resistors.LiquidTubeResistance</a>.
<br/<br/>
The fluid and wall volumes are coupled via a heat transfer model, describing the
convective heat transfer fluid at the tube side. For a detailed documentation, see
<a href=\"Modelica://SorpLib.Components.HeatTransfer.TubeInsideHeatTransfer\">SorpLib.Components.HeatTransfer.TubeInsideHeatTransfer</a>.
<br/<br/>
Moreover,
<a href=\"Modelica://SorpLib.Components.HeatTransfer.ConductionHeatTransfer\">conductive heat transfer</a>
can be considered within the wall.
</p>

<h4>Assumptions and limitations</h4>
<ul>
  <li>  
  Constant volumes within the control volumes
  </li>
  <li>
  Homogenous properties within the control volumes
  </li>
</ul>

<h4>Typical use</h4>
<p>
The tube model is typically used to model tubes within heat exchangers, such as
evaporators, condensers, or adsorbers.
</p>

<h4>Important parameters and options</h4>
<ul>
  <li>
  <i>useConductionPerpendicularFlowDirection</i>:
  Defines if conductive heat transfer perpendicular to the flow direction
  is considered within the wall.
  </li>
  <li>
  <i>useConductionFlowDirection</i>:
  Defines if conductive heat transfer in the flow direction is considered 
  within the wall.
  </li>
  <li>
  <i>calcFluidTransportProperties</i>:
  Defines if fluid and transport properties are required for transport
  phenomena.
  </li>
  <br/>
  <li>
  <i>type_overallMassBalance</i>:
  Defines the type of the overall mass balance.
  </li>
  <li>
  <i>type_independentMassBalances</i>:
  Defines the type of the independent mass balances.
  </li>
  <li>
  <i>type_energyBalance</i>:
  Defines the type of the energy balance.
  </li>
  <br/>
  <li>
  <i>avoid_events</i>:
  Defines if events shall be avoided via the noEvent()-operator.
  </li>
</ul>

<h4>Dynamics</h4>
<p>
This model has three dynamic state when using default options:
</p>
<ul>
  <li>
  Fluid pressure <i>p<sub>liq</sub></i>, fluid temperature <i>T<sub>liq</sub></i>, and wall temperature <i>T<sub>wall</sub></i>
  </li>
</ul>
<p>
Note that this model does not have the pressure <i>p</i> as dynamic state if the
fluid medium is a single state model.
</p>
</html>"));
end LiquidTube;
