within SorpLib.Components;
package Tubes "Tubes containing fluids"
  extends SorpLib.Icons.TubesPackage;

  annotation (Documentation(info="<html>
<p>
This package contains tubes. Ready-to-use models are based on the Modelica 
Standard library (MSL).
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end Tubes;
