within SorpLib.Components.Tubes.Records;
record GeometryTube
  "This record contains the geometry of tubes"
  extends Modelica.Icons.Record;

  //
  // Definition of parameters regarding the disretization
  //
  parameter Integer no_fluidVolumes(min=1) = 1
    "Number of fluid volumes"
    annotation (Dialog(tab="General", group="Discretization", enable=false));
  parameter Integer no_wallVolumes(min=1) = 1
    "Number of wall volumes"
    annotation (Dialog(tab="General", group="Discretization", enable=false));

  //
  // Definition of parameters regarding the general geometry
  //
  parameter Integer no_hydraulicParallelTubes(min=1) = 1
    "Number of hydraulically parallel tubes"
    annotation (Dialog(tab="General", group="Geometry"));

  parameter Modelica.Units.SI.Length l = 1
    "Length of the tube"
    annotation (Dialog(tab="General", group="Geometry"));

  parameter Modelica.Units.SI.Length roughness = 7.5e-7
    "Absolute roughness of the tube"
    annotation (Dialog(tab="General", group="Geometry"));

  //
  // Definition of parameters regarding diameters
  //
  parameter Modelica.Units.SI.Diameter d_inner = 0.01
    "Inner diameter"
    annotation (Dialog(tab="General", group="Geometry - Diameters"));
  parameter Modelica.Units.SI.Diameter d_outer = 0.012
    "Outer diameter"
    annotation (Dialog(tab="General", group="Geometry - Diameters"));

  parameter Modelica.Units.SI.Diameter d_hydInner = d_inner
    "Hydraulic inner diameter"
    annotation (Dialog(tab="General", group="Geometry - Diameters"));
  parameter Modelica.Units.SI.Diameter d_hydOuter = d_outer
    "Hydraulic outer diameter"
    annotation (Dialog(tab="General", group="Geometry - Diameters"));

  parameter Modelica.Units.SI.Thickness t_wall = (d_outer - d_inner) / 2
    "Wall thickness"
    annotation (Dialog(tab="General", group="Geometry - Diameters"));

  //
  // Definition of parameters regarding areas
  //
  parameter Modelica.Units.SI.Area A_crossInner = Modelica.Constants.pi/4 * d_inner^2
    "Inner cross-sectional area"
    annotation (Dialog(tab="General", group="Geometry - Areas"));
  parameter Modelica.Units.SI.Area A_crossOuter = Modelica.Constants.pi/4 * d_outer^2
    "Outer cross-sectional area"
    annotation (Dialog(tab="General", group="Geometry - Areas"));
  parameter Modelica.Units.SI.Area A_crossWall = A_crossOuter - A_crossInner
    "Wall cross-sectional area"
    annotation (Dialog(tab="General", group="Geometry - Areas"));

  parameter Modelica.Units.SI.Area A_hydCrossInner = A_crossInner
    "Hydraulic inner cross-sectional area"
    annotation (Dialog(tab="General", group="Geometry - Areas"));
  parameter Modelica.Units.SI.Area A_hydCrossOuter = A_crossOuter
    "Hydraulic outer cross-sectional area"
    annotation (Dialog(tab="General", group="Geometry - Areas"));
  parameter Modelica.Units.SI.Area A_hydCrossWall = A_hydCrossOuter - A_hydCrossInner
    "Hydraulic wall cross-sectional area"
    annotation (Dialog(tab="General", group="Geometry - Areas"));

  parameter Modelica.Units.SI.Area A_heatTransferInner=
    Modelica.Constants.pi * d_inner * l
    "Total inner heat transfer area"
    annotation (Dialog(tab="General", group="Geometry - Areas"));
  parameter Modelica.Units.SI.Area A_heatTransferOuter=
    Modelica.Constants.pi * d_outer * l
    "Total outer heat transfer area"
    annotation (Dialog(tab="General", group="Geometry - Areas"));

  parameter Real f_finAreaRatioInner(min=0, max=1) = 0
    "Ratio of total inner fin area to total inner heat transfer area"
    annotation (Dialog(tab="General", group="Geometry - Areas"));
  parameter Real f_finAreaRatioOuter(min=0, max=1) = 0
    "Ratio of total outer fin area to total outer heat transfer area"
    annotation (Dialog(tab="General", group="Geometry - Areas"));

  //
  // Definition of parameters regarding volumes
  //
  parameter Modelica.Units.SI.Volume V_inner = A_crossInner * l
    "Total inner volume"
    annotation (Dialog(tab="General", group="Geometry - Volumes"));
  parameter Modelica.Units.SI.Volume V_outer = A_crossOuter * l
    "Total outer volume"
    annotation (Dialog(tab="General", group="Geometry - Volumes"));
  parameter Modelica.Units.SI.Volume V_wall = V_outer - V_inner
    "Total wall volume"
    annotation (Dialog(tab="General", group="Geometry - Volumes"));

  parameter Real f_finVolumeRatioInner(min=0, max=1) = 0
    "Ratio of total inner fin volume to total wall volume"
    annotation (Dialog(tab="General", group="Geometry - Volumes"));
  parameter Real f_finVolumeRatioOuter(min=0, max=1) = 0
    "Ratio of total outer fin volume to total wall volume"
    annotation (Dialog(tab="General", group="Geometry - Volumes"));

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This record contains geometric parameters required by models calculating the heat
transfer coefficients at the inside or outside of tubes.
</p>
</html>", revisions="<html>
<ul>
  <li>
  January 29, 2024, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end GeometryTube;
