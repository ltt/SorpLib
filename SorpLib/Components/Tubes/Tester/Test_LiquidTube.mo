within SorpLib.Components.Tubes.Tester;
model Test_LiquidTube "Tester for the liquid tube"
  extends Modelica.Icons.Example;

  //
  // Definition of parameters regarding the medium
  //
  replaceable package Medium = Modelica.Media.Water.ConstantPropertyLiquidWater
    constrainedby Modelica.Media.Interfaces.PartialMedium
    "Medium model of the (ideal) liquid"
    annotation (Dialog(tab="General", group="Medium"),
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

  //
  // Definition of boundary models
  //
  SorpLib.Basics.Sources.Fluids.LiquidSource fs_a(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.VolumeFlowRate,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    use_mFlowInput=false,
    use_VFlowInput=true,
    V_flow_fixed=-10/60/1000,
    T_fixed=288.15,
    redeclare final package Medium = Medium)
    "Fluid source a"
    annotation (Placement(transformation(extent={{-70,-10},{-50,10}})));
  SorpLib.Basics.Sources.Fluids.LiquidSource fs_b(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    use_pInput=true,
    T_fixed=358.15,
    redeclare final package Medium = Medium)
    "Fluid source b"
    annotation (Placement(transformation(extent={{70,-10},{50,10}})));

  SorpLib.Basics.Sources.Thermal.HeatSource hs(
    boundaryType=SorpLib.Choices.BoundaryThermal.Temperature,
    use_TInput=true)
    "Heat source for walls" annotation (Placement(
        transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={0,40})));

  //
  // Definition of models
  //
  SorpLib.Components.Tubes.LiquidTube tube(
    redeclare Records.GeometryTube geometry(l=10),
    fluidPropertyPosition=3,
    redeclare model FluidThermalConvectionTubeInside =
      HeatTransfer.HeatTransferCoefficientCorrelations.TubeInside.ConstantAlphaA
        (
       constantAlphaA=100),
    redeclare model PressureDrop =
      SorpLib.Components.Fittings.PressureLossCorrelations.TubeInside.Konakov (
      showTotalPressures=true,
      showVelocities=true,
      showDarcyFrictionNumber=true,
      showIndividualPressureLosses=true),
    redeclare final package Medium = Medium,
    redeclare model WallMaterial = Media.Solids.MetalsAndMetalAlloys.Copper (
      calcCaloricProperties=false,
      approach_c=SorpLib.Choices.SpecificHeatCapacitySolid.Constant,
      approach_lambda=SorpLib.Choices.ThermalConductivitySolid.Constant))
    "Tube model"
    annotation (Placement(transformation(extent={{-10,-10},{10,10}})));

  SorpLib.Components.HeatTransfer.GenericHeatTransfer heatTransfer_boundaryToWall(
    n_a=tube.no_wallVolumes,
    n_b=1,
    redeclare model HeatTransferCoefficient =
        HeatTransfer.HeatTransferCoefficientCorrelations.Generic.ConstantAlphaA
        (constantAlphaA=100)) "Heat transfer from heat source to wall"
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={0,20})));

  //
  // Definition of input signals
  //
protected
  Modelica.Blocks.Sources.Sine input_V_flow(
    amplitude=15/1000/60,
    f=1/500,
    offset=-10/1000/60) "Input signal for volume flow rate"
    annotation (Placement(transformation(extent={{-100,-10},{-80,10}})));
  Modelica.Blocks.Sources.Ramp input_p(
    height=1e5,
    duration=2500,
    offset=2e5) "Input signal for pressure"
    annotation (Placement(transformation(extent={{100,-10},{80,10}})));
  Modelica.Blocks.Sources.Sine input_TWall(
    amplitude=25,
    f=1/250,
    offset=273.15 + 50) "Input signal for wall temperature boundary"
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={0,60})));

equation
  //
  // Connections
  //
  connect(fs_a.port, tube.fp_a) annotation (Line(
      points={{-60,0},{-10,0}},
      color={28,108,200},
      thickness=1));
  connect(tube.fp_b, fs_b.port) annotation (Line(
      points={{10,0},{60,0}},
      color={28,108,200},
      thickness=1));
  connect(hs.port, heatTransfer_boundaryToWall.hp_b[1]) annotation (Line(
      points={{0,40},{0,28}},
      color={238,46,47},
      thickness=1));
  connect(heatTransfer_boundaryToWall.hp_a, tube.hp_wall) annotation (Line(
      points={{0,12},{0,4}},
      color={238,46,47},
      thickness=1));

  connect(input_V_flow.y, fs_a.V_flow_input) annotation (Line(points={{-79,0},{-70,
          0},{-70,2},{-61.2,2}}, color={0,0,127}));
  connect(input_p.y, fs_b.p_input)
    annotation (Line(points={{79,0},{70,0},{70,5},{61.2,5}}, color={0,0,127}));
  connect(input_TWall.y, hs.T_input) annotation (Line(points={{-2.22045e-15,49},
          {0,49},{0,44},{-5.2,44},{-5.2,41}}, color={0,0,127}));

  //
  // Annotations
  //
  annotation (experiment(
      StopTime=2500), Documentation(info="<html>
<p>
This model checks the liquid tube.
<br/><br/>
To see the model behavior, plot the variables of the models over the 
time. The simulation time is correctly preset (Start: 0 s, Stop = 2500 s). 
</p>
</html>", revisions="<html>
<ul>
  <li>
  January 29, 2024, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Test_LiquidTube;
