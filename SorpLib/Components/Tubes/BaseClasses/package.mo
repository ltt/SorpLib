within SorpLib.Components.Tubes;
package BaseClasses "Base models and functions for all tubes"
  extends Modelica.Icons.BasesPackage;

  annotation (Documentation(info="<html>
<p>
This package contains partial tubbes, containing fundamental definitions for tubes.
The content of this package is only of interest when adding new tubes to the library. 
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end BaseClasses;
