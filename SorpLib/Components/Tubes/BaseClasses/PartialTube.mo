within SorpLib.Components.Tubes.BaseClasses;
partial model PartialTube "Base model for all tubes"

  //
  // Definition of general parameters
  //
  parameter Integer no_fluidVolumes(min=2)=10
    "Discretization number of fluid volumes"
    annotation (Dialog(tab = "General", group = "Discretization"),
                Evaluate=true,
                HideResult=true);
  parameter Integer no_wallVolumes(min=2)=no_fluidVolumes
    "Discretization number of wall volumes (must be a factor of no_fluidVolumes)"
    annotation (Dialog(tab = "General", group = "Discretization"),
                Evaluate=true,
                HideResult=true);

  //
  // Definition of parameters regarding the geometry
  //
  replaceable parameter SorpLib.Components.Tubes.Records.GeometryTube geometry
    constrainedby SorpLib.Components.Tubes.Records.GeometryTube(
      no_fluidVolumes=no_fluidVolumes,
      no_wallVolumes=no_wallVolumes)
    "Geometry of the tube"
    annotation (Dialog(tab = "General", group = "Geoemetry"),
                choicesAllMatching = true);

  //
  // Definition of parameters regarding the medium
  //
  parameter Integer no_components = 1
    "Number of components"
    annotation (Dialog(tab="General", group="Medium"),
                Evaluate=true,
                HideResult=true);

  //
  // Definition of models describing transport phenomena
  //
  parameter Boolean useConductionPerpendicularFlowDirection = true
    " = true, if thermal conduction perpendicular to the flow direction is
    considered within the wall"
    annotation (Dialog(tab="Transport Phenomena", group="General"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);
  parameter Boolean useConductionFlowDirection = false
    " = true, if thermal conduction in the flow direction is considered within 
    the wall"
    annotation (Dialog(tab="Transport Phenomena", group="General"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);
  parameter Boolean calcFluidTransportProperties = true
    " = true, if any transport model needs fluid or transport properties"
    annotation (Dialog(tab="Transport Phenomena", group="General"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);
  parameter Integer fluidPropertyPosition = 1
    "Defines the position for fluid porperty calculation is discretization number
    is different"
    annotation (Dialog(tab="Transport Phenomena", group="General",
                enable=no_fluidVolumes<>no_wallVolumes),
                choices(choice=1 "First volume near port a",
                        choice=2 "First volume near port b",
                        choice=3 "Properties averaged over volumes"),
                Evaluate=true,
                HideResult=true);

  replaceable model InnerWallThermalConductionPerpendicularFlowDirection =
    SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.Conduction.CylindricalWall
      ( l_wall=geometry.l/geometry.no_wallVolumes,
        d_inner=geometry.d_hydInner,
        d_outer=(geometry.d_hydInner + geometry.d_hydOuter)/2)
    constrainedby
    SorpLib.Components.HeatTransfer.BaseClasses.PartialConductiveHeatTransferCoefficient
    "Heat transfer correlation describing thermal conduction perpendicular to the
    flow direction within the wall near the fluid"
    annotation (Dialog(tab="Transport Phenomena", group="Thermal Conduction",
                enable = useConductionPerpendicularFlowDirection),
                choicesAllMatching = true,
                Evaluate=true,
                HideResult=true);
  replaceable model OuterWallThermalConductionPerpendicularFlowDirection =
    SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.Conduction.CylindricalWall
      ( l_wall=geometry.l/geometry.no_wallVolumes,
        d_inner=(geometry.d_hydInner + geometry.d_hydOuter)/2,
        d_outer=geometry.d_hydOuter)
    constrainedby
    SorpLib.Components.HeatTransfer.BaseClasses.PartialConductiveHeatTransferCoefficient
    "Heat transfer correlation describing thermal conduction perpendicular to the
    flow direction within the wall near the heat port"
    annotation (Dialog(tab="Transport Phenomena", group="Thermal Conduction",
                enable = useConductionPerpendicularFlowDirection),
                choicesAllMatching = true,
                Evaluate=true,
                HideResult=true);
  replaceable model WallThermalConductionFlowDirection =
    SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.Conduction.PlainWall
      (
      A_cross=geometry.A_crossWall,
      delta_wall=(geometry.l/geometry.no_wallVolumes)/2)
    constrainedby
    SorpLib.Components.HeatTransfer.BaseClasses.PartialConductiveHeatTransferCoefficient
    "Heat transfer correlation describing thermal conduction in the flow direction
    within the wall"
    annotation (Dialog(tab="Transport Phenomena", group="Thermal Conduction",
                enable = useConductionFlowDirection),
                choicesAllMatching = true,
                Evaluate=true,
                HideResult=true);

  replaceable model FluidThermalConvectionTubeInside =
    SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.TubeInside.GnielinskiDittusBoelter
    constrainedby
    SorpLib.Components.HeatTransfer.BaseClasses.PartialTubeInsideHeatTransferCoefficient
    "Heat transfer correlation describing thermal convection at the tube inside
    (i.e., form the fluid to the wall)"
    annotation (Dialog(tab="Transport Phenomena", group="Thermal Convection"),
                choicesAllMatching = true,
                Evaluate=true,
                HideResult=true);

  replaceable model PressureDrop =
    SorpLib.Components.Fittings.PressureLossCorrelations.TubeInside.Konakov
    constrainedby
    SorpLib.Components.Fittings.BaseClasses.PartialTubeInsidePressureLoss
    "Pressure drop correlation describing the pressure drop of the fluid"
    annotation (Dialog(tab="Transport Phenomena", group="Pressure Drop"),
                choicesAllMatching = true,
                Evaluate=true,
                HideResult=true);

  //
  // Definition of parameters regarding start values
  //
  parameter Modelica.Units.SI.Pressure p_fluidAInitial = 1.25e5
    "Initial value of fluid pressure at port a"
    annotation (Dialog(tab="Initialisation", group="Initial Values"));
  parameter Modelica.Units.SI.Pressure p_fluidBInitial = 1e5
    "Initial value of fluid pressure at port b"
    annotation (Dialog(tab="Initialisation", group="Initial Values"));

  parameter Modelica.Units.SI.Temperature T_fluidAInitial = 283.15
    "Initial value of fluid temperature at port a"
    annotation (Dialog(tab="Initialisation", group="Initial Values"));
  parameter Modelica.Units.SI.Temperature T_fluidBInitial = 303.15
    "Initial value of fluid temperature at port b"
    annotation (Dialog(tab="Initialisation", group="Initial Values"));

  parameter Modelica.Units.SI.Temperature T_wallAInitial = T_fluidAInitial
    "Initial value of wall temperature at port a"
    annotation (Dialog(tab="Initialisation", group="Initial Values"));
  parameter Modelica.Units.SI.Temperature T_wallBInitial = T_fluidBInitial
    "Initial value of wall temperature at port b"
    annotation (Dialog(tab="Initialisation", group="Initial Values"));

  parameter Modelica.Units.SI.MassFlowRate m_flow_start = 1e-4
    "Start value for mass flow rate"
    annotation (Dialog(tab="Initialisation", group="Start Values"));

  //
  // Definition of advanced parameters
  //
  parameter SorpLib.Choices.BalanceEquations type_overallMassBalance=
    SorpLib.Choices.BalanceEquations.TransientFixedInitial
    "Handling of the overall mass balance and corresponding initialisation"
    annotation (Dialog(tab = "Advanced", group = "Conservation Equations"),
                Evaluate=true,
                HideResult=true);
  parameter SorpLib.Choices.BalanceEquations type_independentMassBalances=
    type_overallMassBalance
    "Handling of independent mass balances and corresponding initialisations"
    annotation (Dialog(tab = "Advanced", group = "Conservation Equations"),
                Evaluate=true,
                HideResult=true);
  parameter SorpLib.Choices.BalanceEquations type_energyBalance=
    SorpLib.Choices.BalanceEquations.TransientFixedInitial
    "Handling of the energy balance and corresponding initialisation"
    annotation (Dialog(tab = "Advanced", group = "Conservation Equations"),
                Evaluate=true,
                HideResult = true);

  parameter Boolean avoid_events = false
    "= true, if events are avoid by using noEvent()-operator"
    annotation (Dialog(tab = "Advanced", group = "Numerics"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult = true);
  parameter Modelica.Units.SI.MassFlowRate m_flow_small = 1e-4
    "Regularization mass flow rate"
    annotation (Dialog(tab="Advanced", group="Numerics"));
  parameter Integer noDiff = 2
    "Specification how often transition functions can be differentiated"
    annotation(Dialog(tab = "Advanced", group = "Numerics"),
              Evaluate=true,
              HideResult=true);

  //
  // Definition of protected parameters
  //
protected
  final parameter Integer factorDiscretization=
    integer(max(no_fluidVolumes,no_wallVolumes)/
    min(no_fluidVolumes,no_wallVolumes))
    "Discretization factor"
    annotation (Dialog(tab = "General", group = "Discretization"),
                Evaluate=true,
                HideResult=true);

  final parameter Modelica.Units.SI.Pressure[no_fluidVolumes] p_fluidInitial=
    linspace(p_fluidAInitial, p_fluidBInitial, no_fluidVolumes)
    "Initial value of fluid pressures for each volume"
    annotation (Dialog(tab="Initialisation", group="Initial Values"),
                HideResult = true);
  final parameter Modelica.Units.SI.Temperature[no_fluidVolumes] T_fluidInitial=
    linspace(T_fluidAInitial, T_fluidBInitial, no_fluidVolumes)
    "Initial value of fluid temperature for each fluid volume"
    annotation (Dialog(tab="Initialisation", group="Initial Values"),
                HideResult = true);
  final parameter Modelica.Units.SI.Temperature[no_wallVolumes] T_wallInitial=
    linspace(T_wallAInitial, T_wallBInitial, no_wallVolumes)
    "Initial value of wall temperatures for each wall volume"
    annotation (Dialog(tab="Initialisation", group="Initial Values"),
                HideResult = true);

  //
  // Definition of ports
  //
public
  replaceable SorpLib.Basics.Interfaces.BaseClasses.PartialFluidPort fp_a
    constrainedby Basics.Interfaces.BaseClasses.PartialFluidPort(
      final no_components=no_components,
      m_flow(start=m_flow_start))
    "Fluid port a"
    annotation (Placement(transformation(extent={{-110,-10},{-90,10}}),
                iconTransformation(extent={{-110,-10},{-90,10}})),
                choicesAllMatching=true);

  replaceable SorpLib.Basics.Interfaces.BaseClasses.PartialFluidPort fp_b
    constrainedby Basics.Interfaces.BaseClasses.PartialFluidPort(
      final no_components=no_components,
      m_flow(start=-m_flow_start))
    "Fluid port b"
    annotation (Placement(transformation(extent={{90,-10},{110,10}}),
                iconTransformation(extent={{90,-10},{110,10}})),
                choicesAllMatching=true);

  SorpLib.Basics.Interfaces.HeatPorts.HeatPort_in[no_wallVolumes] hp_wall
    "Heat ports at the wall"
    annotation (Placement(transformation(extent={{-10,30},{10,50}}),
                iconTransformation(extent={{-10,30},{10,50}})));

  //
  // Definition and instanziation of models
  //
  replaceable SorpLib.Basics.Volumes.BaseClasses.PartialFluidVolume[no_fluidVolumes] fluidVolumes(
    each independentStateVariables=SorpLib.Choices.IndependentVariablesVolume.pTX,
    each final calculateAdditionalProperties=calcFluidTransportProperties,
    each final useHeatPorts=true,
    each final useHeatPortsX=false,
    each final useHeatPortsY=true,
    each final useHeatPortsZ=false,
    final p_initial=p_fluidInitial,
    final T_initial=T_fluidInitial,
    each final mc_flow_initialX=m_flow_start,
    each final type_energyBalance=type_energyBalance,
    each final avoid_events=avoid_events,
    each final type_overallMassBalance=type_overallMassBalance,
    each final nPorts_cfp_xMinus=1,
    each final nPorts_cfp_xPlus=1)
    "Fluid volumes"
    annotation (Placement(transformation(extent={{-10,-74},{10,-54}})));

  replaceable SorpLib.Basics.Volumes.BaseClasses.PartialVolume[no_wallVolumes] wallVolumes(
    each final useHeatPortsX=useConductionFlowDirection,
    each final useHeatPortsY=true,
    each final useHeatPortsZ=false,
    each final type_energyBalance=type_energyBalance,
    each final avoid_events=avoid_events)
    "Wall volumes"
    annotation (Placement(transformation(extent={{-10,-10},{10,10}})));

  SorpLib.Components.HeatTransfer.TubeInsideHeatTransfer[min(no_fluidVolumes,no_wallVolumes)] thermalConvection_fluidToWall(
    each final n_a=if no_wallVolumes > no_fluidVolumes then 1 elseif
        no_wallVolumes < no_fluidVolumes then factorDiscretization else 1,
    each final n_b=if useConductionPerpendicularFlowDirection then 1 elseif
      no_wallVolumes > no_fluidVolumes then factorDiscretization elseif
      no_wallVolumes < no_fluidVolumes then 1 else 1,
    each final geometry=geometry,
    each final calculateFluidProperties=calcFluidTransportProperties,
    redeclare each final model HeatTransferCoefficient =
        FluidThermalConvectionTubeInside,
    final fluidProperties=propertiesFluid,
    final m_hyd_xMinus=m_flow_hyd_xMinus/geometry.no_hydraulicParallelTubes,
    final m_hyd_xPlus=m_flow_hyd_xPlus/geometry.no_hydraulicParallelTubes)
    "Thermal convection from fluids to walls"
    annotation (Placement(transformation(
                extent={{-10,-10},{10,10}},
                rotation=90,
                origin={0,-42})));

  SorpLib.Components.HeatTransfer.ConductionHeatTransfer[min(no_fluidVolumes,no_wallVolumes)] thermalConduction_fluidToWall(
    each final n_a=1,
    each final n_b=if no_wallVolumes < no_fluidVolumes then 1 elseif
        no_wallVolumes > no_fluidVolumes then factorDiscretization else 1,
    each final calculateFluidProperties=calcFluidTransportProperties,
    redeclare each final model HeatTransferCoefficient =
        InnerWallThermalConductionPerpendicularFlowDirection,
    each final no_hydraulicParallelFlows=geometry.no_hydraulicParallelTubes,
    final fluidProperties=propertiesWall) if
    useConductionPerpendicularFlowDirection
    "Thermal conduction from fluids to walls"
    annotation (Placement(transformation(
                extent={{-10,-10},{10,10}},
                rotation=90,
                origin={0,-22})));

  SorpLib.Components.HeatTransfer.ConductionHeatTransfer[no_wallVolumes] thermalConduction_wallToHeatPort(
    each final n_a=1,
    each final n_b=1,
    each final calculateFluidProperties=calcFluidTransportProperties,
    redeclare each final model HeatTransferCoefficient =
        OuterWallThermalConductionPerpendicularFlowDirection,
    each final no_hydraulicParallelFlows=geometry.no_hydraulicParallelTubes) if
    useConductionPerpendicularFlowDirection
    "Thermal conduction from wall to heat ports"
    annotation (Placement(transformation(
                extent={{-10,-10},{10,10}},
                rotation=90,
                origin={0,24})));

  SorpLib.Components.HeatTransfer.ConductionHeatTransfer[no_wallVolumes-1] thermalConduction_wall1(
    each final n_a=1,
    each final n_b=1,
    each final calculateFluidProperties=calcFluidTransportProperties,
    redeclare each final model HeatTransferCoefficient =
        WallThermalConductionFlowDirection,
    each final no_hydraulicParallelFlows=geometry.no_hydraulicParallelTubes) if
    useConductionFlowDirection
    "Thermal conduction of the first wall halfs"
    annotation (Placement(transformation(extent={{18,-10},{38,10}})));

  SorpLib.Components.HeatTransfer.ConductionHeatTransfer[no_wallVolumes-1] thermalConduction_wall2(
    each final n_a=1,
    each final n_b=1,
    each final calculateFluidProperties=calcFluidTransportProperties,
    redeclare each final model HeatTransferCoefficient =
        WallThermalConductionFlowDirection,
    each final no_hydraulicParallelFlows=geometry.no_hydraulicParallelTubes) if
    useConductionFlowDirection
    "Thermal conduction of the second wall halfs"
    annotation (Placement(transformation(extent={{-38,-10},{-18,10}})));

  replaceable SorpLib.Components.Fittings.BaseClasses.PartialResistance[no_fluidVolumes-1] pressureDrop(
    each final instreamingPropertiesByInput=true,
    each final dynamicPressureLoss=false,
    each final geodeticPressureLoss=false,
    each final frictionPressureLoss=true,
    final p_a=fluidVolumes[1:no_fluidVolumes-1].fluidProperties.p,
    final T_a=fluidVolumes[1:no_fluidVolumes-1].fluidProperties.T,
    final rho_a=1 ./ fluidVolumes[1:no_fluidVolumes-1].fluidProperties.v,
    final eta_a=fluidVolumes[1:no_fluidVolumes-1].fluidProperties.eta,
    final p_b_ad=fluidVolumes[2:no_fluidVolumes].fluidProperties.p,
    final T_b_ad=fluidVolumes[2:no_fluidVolumes].fluidProperties.T,
    final rho_b_ad=1 ./ fluidVolumes[2:no_fluidVolumes].fluidProperties.v,
    final eta_b_ad=fluidVolumes[2:no_fluidVolumes].fluidProperties.eta,
    each final m_flow_start=m_flow_start,
    each final avoid_events=avoid_events,
    each final m_flow_small=m_flow_small,
    each positionFluidProperties=SorpLib.Choices.ResistorFluidProperties.PortAInlet,
    each fittingPressureLosss=false)
    "Pressure drop between fluid volumes"
    annotation (Placement(transformation(extent={{10,-100},{-10,-80}})));

  //
  // Definition of variables
  //
  Modelica.Units.SI.PressureDifference dp = fp_a.p - fp_b.p
    "Pressure difference between port a and b";

  Modelica.Units.SI.MassFlowRate m_flow_a = fp_a.m_flow
    "Mass flow rate at port a";
  Modelica.Units.SI.MassFlowRate m_flow_b = fp_b.m_flow
    "Mass flow rate at port b";

  Modelica.Units.SI.HeatFlowRate DH_flow
    "Difference enthalpy flow rate between port a and port b";
  Modelica.Units.SI.HeatFlowRate Q_flow_fluidWall=
    sum(thermalConvection_fluidToWall.Q_flow)
    "Heat flow rate transferred from the fluid to the eall";
  Modelica.Units.SI.HeatFlowRate Q_flow_wallHP=
    -sum(hp_wall.Q_flow)
    "Heat flow rate transferred from the wall to the heat port";

  //
  // Definition of protected variables
  //
protected
  Modelica.Units.SI.Pressure[no_wallVolumes] p_wall
    "Pressures used to calculate properteis of the wall";

  Modelica.Units.SI.MassFlowRate[min(no_fluidVolumes,no_wallVolumes)] m_flow_hyd_xMinus
    "Mass flow rates at first volume near port a";
  Modelica.Units.SI.MassFlowRate[min(no_fluidVolumes,no_wallVolumes)] m_flow_hyd_xPlus
    "Mass flow rates at first volume near port b";

  SorpLib.Components.HeatTransfer.Records.FluidProperties[min(no_fluidVolumes,no_wallVolumes)] propertiesWall
    "Media properties used to calcualte conductive heat transfer at fluid side";
  SorpLib.Components.HeatTransfer.Records.FluidProperties[min(no_fluidVolumes,no_wallVolumes)] propertiesFluid
    "Media properties used to calcualte convective heat transfer at fluid side";

equation
  //
  // Assertations
  //
  if no_wallVolumes < no_fluidVolumes then
    assert(rem(no_fluidVolumes, no_wallVolumes) == 0,
      "Number of wall volumes must be a factor of the number of fluid volumes!");

  elseif no_wallVolumes > no_fluidVolumes then
    assert(rem(no_wallVolumes, no_fluidVolumes) == 0,
      "Number of wall volumes must be a factor of the number of fluid volumes!");

  end if;

  //
  // Connection of fluid ports
  //
  connect(fp_a, fluidVolumes[1].cfp_xMinus[1]) annotation (Line(points={{-100,0},
          {-60,0},{-60,-62.2},{-4.2,-62.2}}, color={0,0,0}));

  for i in 1:no_fluidVolumes-1 loop
    connect(pressureDrop[i].port_a, fluidVolumes[i].cfp_xPlus[1]) annotation (Line(
          points={{8,-90},{20,-90},{20,-62.2},{7.8,-62.2}},       color={0,0,0}));
    connect(pressureDrop[i].port_b, fluidVolumes[i+1].cfp_xMinus[1]) annotation (Line(
          points={{-8,-90},{-20,-90},{-20,-62.2},{-4.2,-62.2}},       color={0,0,0}));
  end for;

  connect(fp_b, fluidVolumes[no_fluidVolumes].cfp_xPlus[1]) annotation (Line(points={{100,0},
          {80,0},{80,-62.2},{7.8,-62.2}},
                                      color={0,0,0}));

  //
  // Connection of heat ports
  //
  if no_fluidVolumes == no_wallVolumes then
    //
    // Identical discretization number
    //
    connect(fluidVolumes.hp_yPlus, thermalConvection_fluidToWall.hp_a[1])
      annotation (Line(
        points={{0,-58},{0,-50}},
        color={238,46,47},
        thickness=1));

    if useConductionPerpendicularFlowDirection then
      connect(thermalConvection_fluidToWall.hp_b[1],
        thermalConduction_fluidToWall.hp_a[1]) annotation (Line(
          points={{0,-34},{0,-30}},
          color={238,46,47},
          thickness=1));
      connect(thermalConduction_fluidToWall.hp_b[1], wallVolumes.hp_yMinus)
        annotation (Line(
          points={{0,-14},{0,-6}},
          color={238,46,47},
          thickness=1));

    else
      connect(thermalConvection_fluidToWall.hp_b[1], wallVolumes.hp_yMinus);

    end if;

  elseif no_fluidVolumes > no_wallVolumes then
    //
    // More fluid volumes than wall volumes
    //
    for i in 1:no_wallVolumes loop
      connect(fluidVolumes[1+(i-1)*factorDiscretization:i*factorDiscretization].hp_yPlus,
      thermalConvection_fluidToWall[i].hp_a[1:factorDiscretization])
        annotation (Line(
          points={{0,-58},{0,-50}},
          color={238,46,47},
          thickness=1));
    end for;

    if useConductionPerpendicularFlowDirection then
      connect(thermalConvection_fluidToWall.hp_b[1],
        thermalConduction_fluidToWall.hp_a[1]) annotation (Line(
          points={{0,-34},{0,-30}},
          color={238,46,47},
          thickness=1));
      connect(thermalConduction_fluidToWall.hp_b[1], wallVolumes.hp_yMinus)
        annotation (Line(
          points={{0,-14},{0,-6}},
          color={238,46,47},
          thickness=1));

    else
      connect(thermalConvection_fluidToWall.hp_b[1], wallVolumes.hp_yMinus);

    end if;

  else
    //
    // More wall volumes than fluid volumes
    //
    connect(fluidVolumes.hp_yPlus, thermalConvection_fluidToWall.hp_a[1])
      annotation (Line(
        points={{0,-58},{0,-50}},
        color={238,46,47},
        thickness=1));

    if useConductionPerpendicularFlowDirection then
      connect(thermalConvection_fluidToWall.hp_b[1],
        thermalConduction_fluidToWall.hp_a[1]) annotation (Line(
          points={{0,-34},{0,-30}},
          color={238,46,47},
          thickness=1));

      for i in 1:no_fluidVolumes loop
        connect(thermalConduction_fluidToWall[i].hp_b[1:factorDiscretization],
        wallVolumes[1+(i-1)*factorDiscretization:i*factorDiscretization].hp_yMinus)
          annotation (Line(
            points={{0,-14},{0,-6}},
            color={238,46,47},
            thickness=1));
      end for;

    else
      for i in 1:no_fluidVolumes loop
        connect(thermalConvection_fluidToWall[i].hp_b[1:factorDiscretization],
          wallVolumes[1+(i-1)*factorDiscretization:i*factorDiscretization].hp_yMinus)
          annotation (Line(
            points={{0,-34},{0,-6}},
            color={238,46,47},
            thickness=1));
      end for;
    end if;
  end if;

  if useConductionPerpendicularFlowDirection then
    connect(wallVolumes.hp_yPlus, thermalConduction_wallToHeatPort.hp_a[1])
      annotation (Line(
        points={{0,6},{0,16}},
        color={238,46,47},
        thickness=1));
    connect(thermalConduction_wallToHeatPort.hp_b[1], hp_wall) annotation (Line(
        points={{0,32},{0,40}},
        color={238,46,47},
        thickness=1));

  else
    connect(wallVolumes.hp_yPlus, hp_wall) annotation (Line(
        points={{0,6},{0,40}},
        color={238,46,47},
        thickness=1));

  end if;

  if useConductionFlowDirection then
    for i in 1:no_wallVolumes-1 loop
      connect(wallVolumes[i].hp_xPlus, thermalConduction_wall1[i].hp_a[1])
        annotation (Line(
          points={{6,0},{20,0}},
          color={238,46,47},
          thickness=1));
      connect(thermalConduction_wall1[i].hp_b[1], thermalConduction_wall2[i].hp_a[1])
        annotation (Line(
          points={{36,0},{40,0},{40,14},{-40,14},{-40,0},{-36,0}},
          color={238,46,47},
          thickness=1));
      connect(thermalConduction_wall2[i].hp_b[1], wallVolumes[i+1].hp_xMinus)
        annotation (Line(
          points={{-20,0},{-6,0}},
          color={238,46,47},
      thickness=1));
    end for;
  end if;

  //
  // Calculate fluid properties
  //
  if no_fluidVolumes == no_wallVolumes then
    //
    // Identical discretization number
    //
    p_wall = fluidVolumes.fluidProperties.p
      "Pressures used to calculate properteis of the wall";

    m_flow_hyd_xMinus = fluidVolumes.fluidProperties.mc_flow_xMinus
      "Mass flow rates at first volume near port a";
    m_flow_hyd_xPlus = fluidVolumes.fluidProperties.mc_flow_xPlus
      "Mass flow rates at first volume near port b";

    propertiesFluid.p = fluidVolumes.fluidProperties.p
      "Pressures used to calcualte convective heat transfer at fluid side";
    propertiesFluid.T = fluidVolumes.fluidProperties.T
      "Temperatures used to calcualte convective heat transfer at fluid side";
    propertiesFluid.rho = 1 ./ fluidVolumes.fluidProperties.v
      "Densities used to calcualte convective heat transfer at fluid side";
    propertiesFluid.cp = fluidVolumes.fluidProperties.cp
      "Specific heat capacities used to calcualte convective heat transfer at 
      fluid side";
    propertiesFluid.eta = fluidVolumes.fluidProperties.eta
      "Dynamic viscosities used to calcualte convective heat transfer at 
      fluid side";
    propertiesFluid.lambda = fluidVolumes.fluidProperties.lambda
      "Thermal conductivities used to calcualte convective heat transfer at 
      fluid side";

  elseif no_fluidVolumes > no_wallVolumes then
    //
    // More fluid volumes than wall volumes
    //
    if fluidPropertyPosition == 1 then
      for i in 1:no_wallVolumes loop
        p_wall[i] =
          fluidVolumes[1+(i-1)*factorDiscretization].fluidProperties.p
          "Pressures used to calculate properteis of the wall";

        m_flow_hyd_xMinus[i] =
          fluidVolumes[1+(i-1)*factorDiscretization].fluidProperties.mc_flow_xMinus
        "Mass flow rates at first volume near port a";
        m_flow_hyd_xMinus[i] =
          fluidVolumes[1+(i-1)*factorDiscretization].fluidProperties.mc_flow_xPlus
        "Mass flow rates at first volume near port b";

        propertiesFluid[i].p =
          fluidVolumes[1+(i-1)*factorDiscretization].fluidProperties.p
          "Pressures used to calcualte convective heat transfer at fluid side";
        propertiesFluid[i].T =
          fluidVolumes[1+(i-1)*factorDiscretization].fluidProperties.T
          "Temperatures used to calcualte convective heat transfer at fluid side";
        propertiesFluid[i].rho =
          1 ./ fluidVolumes[1+(i-1)*factorDiscretization].fluidProperties.v
          "Densities used to calcualte convective heat transfer at fluid side";
        propertiesFluid[i].cp =
          fluidVolumes[1+(i-1)*factorDiscretization].fluidProperties.cp
          "Specific heat capacities used to calcualte convective heat transfer at 
          fluid side";
        propertiesFluid[i].eta =
          fluidVolumes[1+(i-1)*factorDiscretization].fluidProperties.eta
          "Dynamic viscosities used to calcualte convective heat transfer at 
          fluid side";
        propertiesFluid[i].lambda =
          fluidVolumes[1+(i-1)*factorDiscretization].fluidProperties.lambda
          "Thermal conductivities used to calcualte convective heat transfer at 
          fluid side";
      end for;

    elseif fluidPropertyPosition == 2 then
      for i in 1:no_wallVolumes loop
        p_wall[i] =
          fluidVolumes[i*factorDiscretization].fluidProperties.p
          "Pressures used to calculate properteis of the wall";

        m_flow_hyd_xMinus[i] =
          fluidVolumes[i*factorDiscretization].fluidProperties.mc_flow_xMinus
        "Mass flow rates at first volume near port a";
        m_flow_hyd_xMinus[i] =
          fluidVolumes[i*factorDiscretization].fluidProperties.mc_flow_xPlus
        "Mass flow rates at first volume near port b";

        propertiesFluid[i].p =
          fluidVolumes[i*factorDiscretization].fluidProperties.p
          "Pressures used to calcualte convective heat transfer at fluid side";
        propertiesFluid[i].T =
          fluidVolumes[i*factorDiscretization].fluidProperties.T
          "Temperatures used to calcualte convective heat transfer at fluid side";
        propertiesFluid[i].rho =
          1 ./ fluidVolumes[i*factorDiscretization].fluidProperties.v
          "Densities used to calcualte convective heat transfer at fluid side";
        propertiesFluid[i].cp =
          fluidVolumes[i*factorDiscretization].fluidProperties.cp
          "Specific heat capacities used to calcualte convective heat transfer at 
          fluid side";
        propertiesFluid[i].eta =
          fluidVolumes[i*factorDiscretization].fluidProperties.eta
          "Dynamic viscosities used to calcualte convective heat transfer at 
          fluid side";
        propertiesFluid[i].lambda =
          fluidVolumes[i*factorDiscretization].fluidProperties.lambda
          "Thermal conductivities used to calcualte convective heat transfer at 
          fluid side";
      end for;

    else
      for i in 1:no_wallVolumes loop
        p_wall[i] = 1/factorDiscretization *
          sum(fluidVolumes[1+(i-1)*factorDiscretization:
          i*factorDiscretization].fluidProperties.p)
          "Pressures used to calculate properteis of the wall";

        m_flow_hyd_xMinus[i] = 1/factorDiscretization *
          sum(fluidVolumes[1+(i-1)*factorDiscretization:
          i*factorDiscretization].fluidProperties.mc_flow_xMinus)
          "Mass flow rates at first volume near port a";
        m_flow_hyd_xMinus[i] = 1/factorDiscretization *
          sum(fluidVolumes[1+(i-1)*factorDiscretization:
          i*factorDiscretization].fluidProperties.mc_flow_xPlus)
          "Mass flow rates at first volume near port b";

        propertiesFluid[i].p = 1/factorDiscretization *
          sum(fluidVolumes[1+(i-1)*factorDiscretization:
          i*factorDiscretization].fluidProperties.p)
          "Pressures used to calcualte convective heat transfer at fluid side";
        propertiesFluid[i].T = 1/factorDiscretization *
          sum(fluidVolumes[1+(i-1)*factorDiscretization:
          i*factorDiscretization].fluidProperties.T)
          "Temperatures used to calcualte convective heat transfer at fluid side";
        propertiesFluid[i].rho = 1/factorDiscretization *
          sum(1 ./ fluidVolumes[1+(i-1)*factorDiscretization:
          i*factorDiscretization].fluidProperties.v)
          "Densities used to calcualte convective heat transfer at fluid side";
        propertiesFluid[i].cp = 1/factorDiscretization *
          sum(fluidVolumes[1+(i-1)*factorDiscretization:
          i*factorDiscretization].fluidProperties.cp)
          "Specific heat capacities used to calcualte convective heat transfer at 
          fluid side";
        propertiesFluid[i].eta = 1/factorDiscretization *
          sum(fluidVolumes[1+(i-1)*factorDiscretization:
          i*factorDiscretization].fluidProperties.eta)
          "Dynamic viscosities used to calcualte convective heat transfer at 
          fluid side";
        propertiesFluid[i].lambda = 1/factorDiscretization *
          sum(fluidVolumes[1+(i-1)*factorDiscretization:
          i*factorDiscretization].fluidProperties.lambda)
          "Thermal conductivities used to calcualte convective heat transfer at 
          fluid side";
      end for;
    end if;

  else
    //
    // More wall volumes than fluid volumes
    //
    for i in 1:no_fluidVolumes loop
      p_wall[1+(i-1)*factorDiscretization:i*factorDiscretization] =
        fill(fluidVolumes[i].fluidProperties.p, factorDiscretization)
        "Pressures used to calculate properteis of the wall";
    end for;

    m_flow_hyd_xMinus = fluidVolumes.fluidProperties.mc_flow_xMinus
      "Mass flow rates at first volume near port a";
    m_flow_hyd_xPlus = fluidVolumes.fluidProperties.mc_flow_xPlus
      "Mass flow rates at first volume near port b";

    propertiesFluid.p = fluidVolumes.fluidProperties.p
      "Pressures used to calcualte convective heat transfer at fluid side";
    propertiesFluid.T = fluidVolumes.fluidProperties.T
      "Temperatures used to calcualte convective heat transfer at fluid side";
    propertiesFluid.rho = 1 ./ fluidVolumes.fluidProperties.v
      "Densities used to calcualte convective heat transfer at fluid side";
    propertiesFluid.cp = fluidVolumes.fluidProperties.cp
      "Specific heat capacities used to calcualte convective heat transfer at 
      fluid side";
    propertiesFluid.eta = fluidVolumes.fluidProperties.eta
      "Dynamic viscosities used to calcualte convective heat transfer at 
      fluid side";
    propertiesFluid.lambda = fluidVolumes.fluidProperties.lambda
      "Thermal conductivities used to calcualte convective heat transfer at 
      fluid side";

  end if;

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial model is the base model for all tubes. It defines fundamental parameters,
models, and variables required by all tubes. Models that inherit properties from this 
partial model have to redeclare all partial models (i.e., fluid ports, wall volumes,
fluid volumes, and resistors). Morover, the geometry of the redeclared models except
the fluid ports must be correctly set using the geometry record. In addition, the 
pressure drop model must be passed to the resistor model, and the inputs 'fluidProperties'
of the conductive heat transfer models must be correctly set using properties of the
wall volumes.
<br/><br/>
The following variables must be specified in the model that inherit properties:
</p>
<ul>
  <li>
  Difference enthalpy flow rate between port a and port b <i>DH_flow</i>.
  </li>
  <li>
  The fluid property records <i>propertiesWall</i> considering the disretization and
  fluid property position.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  January 29, 2024, by Mirko Engelpracht:<br/>
  Major adaptations due to restructering of the library and documentation.
  </li>
  <li>
  January 14, 2021, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"), Icon(graphics={
        Rectangle(
          extent={{-100,40},{100,-40}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-60,10},{-40,-10}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={255,255,255},
          fillPattern=FillPattern.None,
          fontSize=20,
          textString="1"),
        Text(
          extent={{40,10},{60,-10}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={255,255,255},
          fillPattern=FillPattern.None,
          fontSize=20,
          textString="n"),
        Line(
          points={{0,40},{2,14},{-4,10},{2,-6},{-2,-16},{2,-32},{0,-40}},
          color={0,0,0},
          smooth=Smooth.Bezier)}));
end PartialTube;
