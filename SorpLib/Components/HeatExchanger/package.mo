within SorpLib.Components;
package HeatExchanger "Heat exchangers to transfer heat between different fluid fows"
  extends SorpLib.Icons.HeatExchangersPackage;

  annotation (Documentation(info="<html>
<p>
This package contains heat exchangers. Ready-to-use models are based on the Modelica 
Standard library (MSL).
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end HeatExchanger;
