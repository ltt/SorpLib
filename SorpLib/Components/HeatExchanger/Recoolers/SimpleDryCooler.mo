﻿within SorpLib.Components.HeatExchanger.Recoolers;
model SimpleDryCooler "Model of a simple dry cooler"
  extends SorpLib.Components.HeatExchanger.BaseClasses.PartialSimpleDryCooler(
    final noDiff,
    final X_i_initial,
    final p_initial,
    redeclare final SorpLib.Basics.Interfaces.FluidPorts.LiquidPort_in port_a,
    redeclare final SorpLib.Basics.Interfaces.FluidPorts.LiquidPort_out port_b,
    final no_components=Medium_liq.nX);

  //
  // Definition of parameters
  //
  replaceable package Medium_liq =
      Modelica.Media.Water.ConstantPropertyLiquidWater
    constrainedby Modelica.Media.Interfaces.PartialMedium
    "Liquid medium"
    annotation (Dialog(tab = "General", group = "Medium"),
                choicesAllMatching=true,
                Evaluate=true,
                HideResult=true);
  replaceable package Medium_air =
    Modelica.Media.Air.DryAirNasa
    constrainedby Modelica.Media.Interfaces.PartialMedium
    "Air medium"
    annotation (Dialog(tab = "General", group = "Medium"),
                choicesAllMatching=true,
                Evaluate=true,
                HideResult=true);

  //
  // Definition of state records
  //
  Medium_liq.ThermodynamicState state_liq_in
    "Record describing current state properties of liquid at inlet";
  Medium_air.ThermodynamicState state_air_in = Medium_air.setState_pT(
    p = p_air,
    T = T_air_in)
    "Record describing current state properties of air at the inlet";

equation
  //
  // Calculation of fluid properties
  //
  if flowDirection == 1 then
    //
    // Flow from port a to port b
    //
    state_liq_in = Medium_liq.setState_phX(
      p = port_a.p,
      h = inStream(port_a.h_outflow),
      X = inStream(port_a.Xi_outflow))
      "Record describing current state properties of liquid at inlet";

  elseif flowDirection == 2 then
    //
    // Flow from port b to port a
    //
    state_liq_in = Medium_liq.setState_phX(
      p = port_b.p,
      h = inStream(port_b.h_outflow),
      X = inStream(port_b.Xi_outflow))
      "Record describing current state properties of liquid at inlet";

  else
    //
    // Actual flow direction
    //
    if avoid_events then
      state_liq_in = Medium_liq.setState_phX(
        p = SorpLib.Numerics.regStep_noEvent(
          x=port_a.m_flow,
          y1=port_a.p,
          y2=port_a.p,
          x_small=m_flow_small),
        h = SorpLib.Numerics.regStep_noEvent(
          x=port_a.m_flow,
          y1=inStream(port_a.h_outflow),
          y2=inStream(port_b.h_outflow),
          x_small=m_flow_small),
        X = SorpLib.Numerics.regStep_noEvent(
          x=port_a.m_flow,
          y1=inStream(port_a.Xi_outflow),
          y2=inStream(port_b.Xi_outflow),
          x_small=m_flow_small))
        "Record describing current state properties of liquid at inlet";

    else
      Medium_liq.setState_phX(
        p = SorpLib.Numerics.regStep(
          x=port_a.m_flow,
          y1=port_a.p,
          y2=port_a.p,
          x_small=m_flow_small),
        h = SorpLib.Numerics.regStep(
          x=port_a.m_flow,
          y1=inStream(port_a.h_outflow),
          y2=inStream(port_b.h_outflow),
          x_small=m_flow_small),
        X = SorpLib.Numerics.regStep(
          x=port_a.m_flow,
          y1=inStream(port_a.Xi_outflow),
          y2=inStream(port_b.Xi_outflow),
          x_small=m_flow_small))
        "Record describing current state properties of liquid at inlet";

    end if;
  end if;

  //
  // Calculate fluid properties at inlets
  //
  T_liq_in = Medium_liq.temperature(state=state_liq_in)
    "Inlet temperature of liquid";
  cp_liq_in = Medium_liq.specificHeatCapacityCp(state=state_liq_in)
    "Specific heat capacity of liquid at inlet";
  eta_liq_in = Medium_liq.dynamicViscosity(state=state_liq_in)
    "Dynamic viscosity of liquid at inlet";

  d_air_in = Medium_air.density(state=state_air_in)
    "Density of air at inlet";
  cp_air_in = Medium_air.specificHeatCapacityCp(state=state_air_in)
    "Specific heat capacity of air at inlet";

  //
  // Annotations
  //
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    Documentation(info="<html>
<p>
The model of the simple dry cooler describes the cooling of an (ideal) liquid. Air 
is used as the cooling medium. The model can be adapted to the performance of real 
dry coolers using two fitting parameters for the heat tranfer correlations.
</p>

<h4>Main equations</h4>
<p>
The model has a steady-state energy balance
</p>
<pre>
    0 = m&#x307;<sub>liq,in</sub> + m&#x307;<sub>liq,out</sub>;
</pre>
<p>
and a steady-state energy balance
</p>
<pre>
    h<sub>liq,out</sub>  = h<sub>liq,in</sub> - Q_flow / m&#x307;<sub>liq,out</sub>;
</pre>
<p>
Herein, <i>m&#x307;<sub>liq,in</sub></i>/<i>m&#x307;<sub>liq,out</sub></i> are the 
inlet/outlet liquid mass flow rates, <i>h<sub>liq,in</sub></i>/<i>h<sub>liq,out</sub></i> 
are the liquid specific enthalies at inlet/outlet, <i>Q_flow</i> is the heat flow 
rate transferred to the surrounding. The heat flow rate <i>Q_flow</i> ist calculated 
with the effectiveness <i>&epsilon;</i> of the dry cooler and the maximal heat flow 
rate <i>Q_flow<sub>max</sub></i> that can be transferred:
</p>
<pre>
    Q_flow  = &epsilon; * Q_flow<sub>max</sub> = &epsilon; * C_flow<sub>min</sub> * (T<sub>liq,in</sub> - T<sub>air,in</sub>);
</pre>
<p>
Herein, <i>T<sub>liq,in</sub></i>/<i>T<sub>air,in</sub></i> are the inlet temperatures of 
the liquid/ambient air, and <i>C_flow<sub>min</sub></i> is the minimal heat capacity rate. 
The minimal and maximal heat capacity rates are defined as follows:
</p>
<pre>
    C_flow<sub>min</sub> = <strong>min</strong>(m&#x307;<sub>liq</sub>*c;<sub>p,liq</sub>, V_flow<sub>air</sub>*&rho;;<sub>air</sub>*c;<sub>p,air</sub>);

    C_flow<sub>max</sub> = <strong>max</strong>(m&#x307;<sub>liq</sub>*c;<sub>p,liq</sub>, V_flow<sub>air</sub>*&rho;;<sub>air</sub>*c;<sub>p,air</sub>);
</pre>
<p>
Herein, <i>c;<sub>p,liq</sub></i>/<i>c;<sub>p,air</sub></i> are the specific heat 
capacities of the liquid/ambient air, <i>&rho;<sub>air</sub></i> is the density of the 
ambient air, and <i>V_flow<sub>air</sub></i> is the volume flow rate of the ambient air.
<br/><br/>
For a counter-current flow heat exchanger, the effectiveness <i>&epsilon;</i> is defined as
</p>
<pre>
    &epsilon; = [1 - <strong>exp</strong>(-NTU * (1 - C_flow<sub>min</sub>/C_flow<sub>max</sub>)] / [1 - C_flow<sub>min</sub>/C_flow<sub>max</sub> * <strong>exp</strong>(-NTU * (1 - C_flow<sub>min</sub>/C_flow<sub>max</sub>];
</pre>
<p>
where the number of transfer units <i>NTU</i> for the dry cooler was calculated as 
</p>
<pre>
    NTU = 1/C_flow<sub>min</sub> * (&alpha;A)<sub>liq</sub> * (&alpha;A)<sub>air</sub> / ((&alpha;A)<sub>liq</sub> + (&alpha;A)<sub>air</sub>);
</pre>
<p>
Herein, the products of heat transfer coefficients and areas describe the heat transfer 
on the liquid side <i>(&alpha;A)<sub>liq</sub></i> and the ambient air side 
<i>(&alpha;A)<sub>air</sub></i> . Gibelhaus et al. selected mass-flow-dependent approaches 
for the two products:
</p>
<pre>
    (&alpha;A)<sub>liq</sub> = 2 * &delta; * m&#x307;<sub>liq</sub>^(0.8) *  &eta;<sub>liq</sub>^(-0.8);

    (&alpha;A)<sub>air</sub> = 0.35 * &gamma; * (V_flow<sub>air</sub> * &rho;<sub>air</sub>)^(0.8);
</pre>
<p>
where <i>&eta;<sub>liq</sub></i> is the dynamic viscosity of the liquid, and
<i>&delta;</i>/<i>&gamma;</i> are fitting parameters. 
</p>

<h4>Assumptions and limitations</h4>
<ul>
  <li>
  Steady-state process
  </li>
  <li>
  No storage of mass or energy
  </li>
</ul>

<h4>Typical use</h4>
<p>
The model is typically used to describe the cooling of the heat transfer fluid of
the condeser or of the adsorber to the surrounding.
</p>

<h4>Important parameters and options</h4>
<ul>
  <li>
  <i>flowDirection</i>:
  Defines the flow dirction and, thus, the inlet property calculation.
  </li>
  <li>
  <i>typeDryRecooler</i>:
  Defines the type of the dry cooler.
  </li>
  <br/>
  <li>
  <i>avoid_events</i>:
  Defines if events shall be avoided via the noEvent()-operator.
  </li>
</ul>

<h4>References</h4>
<ul>
  <li>
  Engelpracht, M. (2024). Experimental demonstration and model-based optimization of adsorption heat transformation for waste heat upgrading. In: Aachener Beiträge zur technischen Thermodynamik 51. DOI: http://dx.doi.org/10.18154/RWTH-2024-06878.
  </li>
  <li>
  Gibelhaus, A. and Tangkrachang, T. and Bau, U. and Seiler, J. and Bardow, A. (2019). Integrated design and control of full sorption chiller systems. Energy 185 (2019), pp. 409-422. DOI: https://doi.org/10.1016/j.energy.2019.06.169.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  January 26, 2024, by Mirko Engelpracht:<br/>
  Minor adaptations and documentation.
  </li>
  <li>
  January 14, 2021, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end SimpleDryCooler;
