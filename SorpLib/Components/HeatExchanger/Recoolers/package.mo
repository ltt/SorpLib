within SorpLib.Components.HeatExchanger;
package Recoolers "Recoolers used in closed and open sorption systems"
  extends SorpLib.Icons.RecoolersPackage;

  annotation (Documentation(info="<html>
<p>
This package contains recooler models that are based on the open-source Modelica 
Standard Library (MSL).
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end Recoolers;
