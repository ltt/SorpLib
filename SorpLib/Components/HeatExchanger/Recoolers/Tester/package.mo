within SorpLib.Components.HeatExchanger.Recoolers;
package Tester "Models to test and varify dry cooler models"
  extends Modelica.Icons.ExamplesPackage;



  annotation (Documentation(info="<html>
<p>
This package contains executable test models for all implemented dry coolers. 
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end Tester;
