within SorpLib.Components.HeatExchanger.Recoolers.Tester;
model Test_SimpleDryCooler "Tester for the simple dry cooler"
  extends Modelica.Icons.Example;

  //
  // Definition of parameters regarding the medium
  //
  replaceable package Medium = Modelica.Media.Water.ConstantPropertyLiquidWater
    constrainedby Modelica.Media.Interfaces.PartialMedium
    "Medium model of the (ideal) liquid"
    annotation (Dialog(tab="General", group="Medium"),
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

  //
  // Definition of boundary models
  //
  SorpLib.Basics.Sources.Fluids.LiquidSource fs_a(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.VolumeFlowRate,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    V_flow_fixed(displayUnit="l/min") = -0.0016666666666667,
    use_TInput=true,
    redeclare final package Medium = Medium)
    "Fluid source a"
    annotation (Placement(transformation(extent={{-50,-10},{-30,10}})));

  SorpLib.Basics.Sources.Fluids.LiquidSource fs_b(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    redeclare final package Medium = Medium)
    "Fluid source b"
    annotation (Placement(transformation(extent={{50,-10},{30,10}})));

  SorpLib.Components.Sensors.LiquidSensors.TemperatureSensor T_liqOut(
    redeclare package Medium = Medium)
    "Outlet liquid temperature"
    annotation (Placement(transformation(extent={{10,4},{30,24}})));

  //
  // Definition of multi ports
  //
  SorpLib.Components.HeatExchanger.Recoolers.SimpleDryCooler dryCooler(
    p_air=100000,
    T_air_in(displayUnit="K") = 283.15,
    redeclare final package Medium_liq = Medium)
    "Simple dry cooler"
    annotation (Placement(transformation(extent={{-10,-10},{10,10}})));

  //
  // Definition of input signals
  //
protected
  Modelica.Blocks.Sources.Sine input_fRelAir(
    amplitude=0.45,
    f=1/500,
    offset=0.5)
    "Input signal for relative air volume flow rate"
    annotation (Placement(transformation(extent={{-40,-40},{-20,-20}})));
  Modelica.Blocks.Sources.Sine input_TLiq(
    amplitude=20,
    f=1/250,
    offset=273.15+40)
    "Input signal for liquid inlet temperature"
    annotation (Placement(transformation(extent={{-80,-10},{-60,10}})));

equation
  //
  // Connections
  //
  connect(fs_a.port, dryCooler.port_a) annotation (Line(
      points={{-40,0},{-10,0}},
      color={28,108,200},
      thickness=1));
  connect(dryCooler.port_b, fs_b.port) annotation (Line(
      points={{10,0},{40,0}},
      color={28,108,200},
      thickness=1));
  connect(T_liqOut.port, dryCooler.port_b) annotation (Line(
      points={{20,6},{20,0},{10,0}},
      color={28,108,200},
      thickness=1));

  connect(input_TLiq.y, fs_a.T_input) annotation (Line(points={{-59,0},{-50,0},{
          -50,-2},{-41.2,-2}}, color={0,0,127}));
  connect(input_fRelAir.y, dryCooler.relativeFanSpeed)
    annotation (Line(points={{-19,-30},{0,-30},{0,-6}},   color={0,0,127}));

  //
  // Annotations
  //
  annotation (experiment(StopTime=2500), Documentation(info="<html>
<p>
This model checks the simple dry cooler.
<br/><br/>
To see the model behavior, plot the variables of the models over the 
time. The simulation time is correctly preset (Start: 0 s, Stop = 2500 s). 
</p>
</html>", revisions="<html>
<ul>
  <li>
  January 26, 2024, by Mirko Engelpracht:<br/>
  Adaptations due to restructering the library and documentation.
  </li>
  <li>
  January 14, 2021, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Test_SimpleDryCooler;
