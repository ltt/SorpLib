within SorpLib.Components.HeatExchanger.Adsorbers.Tester;
model Test_SimpleAdsorberDX
  "Tester for the simple closed adsorber with loading-driven mass transfer"
  extends Modelica.Icons.Example;

  //
  // Definition of boundary models
  //
  SorpLib.Basics.Sources.Fluids.LiquidSource fs_a(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.VolumeFlowRate,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    use_mFlowInput=false,
    use_VFlowInput=false,
    V_flow_fixed=-10*10/60/1000,
    use_TInput=true,
    redeclare package Medium = Modelica.Media.Water.ConstantPropertyLiquidWater)
    "Fluid source a"
    annotation (Placement(transformation(extent={{-70,-10},{-50,10}})));
  SorpLib.Basics.Sources.Fluids.LiquidSource fs_b(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    use_pInput=false,
    redeclare final package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater)
    "Fluid source b"
    annotation (Placement(transformation(extent={{70,-10},{50,10}})));

  Basics.Sources.Fluids.VLESource evaporatorSource(
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    p_fixed(displayUnit="kPa") = 1700,
    T_fixed=288.15) "Evaporator source"
    annotation (Placement(transformation(extent={{-70,10},{-50,30}})));

  Basics.Sources.Fluids.VLESource condenserSource(
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    p_fixed(displayUnit="kPa") = 5628,
    T_fixed=308.15) "Condenser source"
    annotation (Placement(transformation(extent={{50,10},{70,30}})));

  //
  // Definition of models
  //
  SorpLib.Components.HeatExchanger.Adsorbers.SimpleAdsorberDX closedAdsorber(
    no_fluidVolumes=10,
    geometry(no_hydraulicParallelTubes=10),
    useCasing=false,
    redeclare model VV_HeatExchanger =
        SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.ClosedAdsorber.ConstantAlphaA
        (constantAlphaA=300),
    redeclare model VV_Casing =
        SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.Generic.ConstantAlphaA
        (constantAlphaA=5),
    x_sorInitial=0.15,                                                 redeclare
      model WorkingPair =
        SorpLib.Media.WorkingPairs.PureComponents.H2O.Silicagel123_DubininLorentzianCumulative_Schawe2000_VLE
        (
        stateVariables=SorpLib.Choices.IndependentVariablesPureComponentWorkingPair.xT,
        calcEntropicProperties=false,
        approachSorptionEnthalpy=SorpLib.Choices.SorptionEnthalpy.Dubinin,
        approachSpecificHeatCapacity=SorpLib.Choices.SpecificHeatCapacityAdsorpt.BoilingCurve,
        approachSpecificVolume=SorpLib.Choices.SpecificVolumeAdsorpt.BoilingCurve,
        limitLowerPressureAdsorptive=true),
    redeclare model WallMaterialCS =
        Media.Solids.MetalsAndMetalAlloys.Aluminium,
    redeclare model MassTransferCoefficientCondenser =
        MassTransfer.MassTransferCoefficientCorrelations.ClosedAdsorber.LoadingDriven.GlueckaufArrhenius)
                                            "Closed adsorber model" annotation (
     Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={0,0})));

  //
  // Definition of input signals
  //
protected
  Modelica.Blocks.Sources.Pulse input_T(
    amplitude=60,
    width=50,
    period=1000,
    offset=273.15 + 25) "Input signal for temperature"
    annotation (Placement(transformation(extent={{-100,-10},{-80,10}})));

equation
  //
  // Connections
  //
  connect(evaporatorSource.port, closedAdsorber.evaporatorPort) annotation (
      Line(
      points={{-60,20},{-10,20},{-10,4}},
      color={0,140,72},
      thickness=1));
  connect(condenserSource.port, closedAdsorber.condenserPort) annotation (Line(
      points={{60,20},{10,20},{10,4}},
      color={0,140,72},
      thickness=1));
  connect(fs_a.port, closedAdsorber.port_a) annotation (Line(
      points={{-60,0},{-10,0}},
      color={28,108,200},
      thickness=1));
  connect(closedAdsorber.port_b, fs_b.port) annotation (Line(
      points={{10,0},{60,0}},
      color={28,108,200},
      thickness=1));

  connect(input_T.y, fs_a.T_input) annotation (Line(points={{-79,0},{-70,0},{-70,
          -2},{-61.2,-2}}, color={0,0,127}));

  //
  // Annotations
  //
  annotation (experiment(
      StopTime=5000,
      __Dymola_NumberOfIntervals=2500,
      __Dymola_Algorithm="Dassl"),
                      Documentation(info="<html>
<p>
This model checks the simple closed adsorber heat exchanger with loading-driven
mass transfer.
<br/><br/>
To see the model behavior, plot the variables of the models over the 
time. The simulation time is correctly preset (Start: 0 s, Stop = 5000 s). 
</p>
</html>", revisions="<html>
<ul>
  <li>
  March 5, 2024, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Test_SimpleAdsorberDX;
