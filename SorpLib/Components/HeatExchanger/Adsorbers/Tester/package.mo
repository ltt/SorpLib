within SorpLib.Components.HeatExchanger.Adsorbers;
package Tester "Models to test and varify closed adsorbers"
  extends Modelica.Icons.ExamplesPackage;



  annotation (Documentation(info="<html>
<p>
This package contains executable test models for all closed adsorber heat exchangers.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Tester;
