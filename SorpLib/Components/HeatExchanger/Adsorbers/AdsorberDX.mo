within SorpLib.Components.HeatExchanger.Adsorbers;
model AdsorberDX
  "Closed adsorber with loading-driven mass trasnfer and vapor volume"
  extends SorpLib.Components.HeatExchanger.BaseClasses.PartialPureComponentAdsorber(
    redeclare final SorpLib.Basics.Interfaces.FluidPorts.LiquidPort_in port_a,
    redeclare final SorpLib.Basics.Interfaces.FluidPorts.LiquidPort_out port_b,
    final no_components=MediumHX.nX,
    final no_adsorptiveComponents=MediumAdsorptive.nX,
    redeclare SorpLib.Components.Tubes.LiquidTube heatExchangerTubes(
      redeclare package Medium = MediumHX,
      redeclare model WallMaterial = WallMaterialHX),
    redeclare SorpLib.Basics.Volumes.FluidVolumes.VLEVolume vaporVolume(
      redeclare final package Medium = MediumAdsorptive,
      final ms_flow_initial=m_flow_adsorptive_start,
      final h_initial,
      nPorts_cfp_xMinus=no_sorbentVolumes),
    redeclare SorpLib.Basics.Volumes.SolidVolumes.SolidVolume casing(
      redeclare final WallMaterialCS solidMedium,
      redeclare final SorpLib.Basics.Volumes.Records.VolumeGeometry geometry(
        dx=0,
        dy=0,
        dz=0,
        A_xy=0,
        A_xz=0,
        A_yz=0,
        V=geometry.V_wall_cas*geometry.no_hydraulicParallelTubes),
      final T_initial=T_casingInitial,
      final p=vaporVolume.fluidProperties.p,
      independentStateVariables=SorpLib.Choices.IndependentVariablesVolume.pTX),
    thermalConduction_casing1(final fluidProperties=
      SorpLib.Components.HeatTransfer.Records.FluidProperties(
        p=casing.solidProperties.p,
        T=casing.solidProperties.T,
        rho=1/casing.solidProperties.v,
        cp=casing.solidProperties.c,
        eta=0,
        lambda=casing.solidProperties.lambda)),
    thermalConduction_casing2(final fluidProperties=
      SorpLib.Components.HeatTransfer.Records.FluidProperties(
        p=casing.solidProperties.p,
        T=casing.solidProperties.T,
        rho=1/casing.solidProperties.v,
        cp=casing.solidProperties.c,
        eta=0,
        lambda=casing.solidProperties.lambda)));

  //
  // Definition of parameters regarding the media
  //
  replaceable model WorkingPair =
      SorpLib.Media.WorkingPairs.PureComponents.H2O.Silicagel123_DubininLorentzianCumulative_Schawe2000_VLE
    constrainedby SorpLib.Media.WorkingPairs.PureComponents.WorkingPairVLE(
        redeclare replaceable package Medium = MediumAdsorptive)
    "Working pair model"
    annotation (Dialog(tab="General", group="Medium"),
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

  replaceable package MediumHX =
      Modelica.Media.Water.ConstantPropertyLiquidWater
    constrainedby Modelica.Media.Interfaces.PartialMedium
    "Medium model of the (ideal) liquid in the heat exchanger"
    annotation (Dialog(tab="General", group="Medium"),
                choicesAllMatching = true,
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);
  replaceable package MediumAdsorptive = Modelica.Media.Water.StandardWater
    constrainedby Modelica.Media.Interfaces.PartialMedium
    "Medium model of adsorptive"
    annotation (Dialog(tab="General", group="Medium"),
                choicesAllMatching = true,
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

  replaceable model WallMaterialHX =
      SorpLib.Media.Solids.MetalsAndMetalAlloys.Copper
    constrainedby SorpLib.Media.Solids.BaseClasses.PartialSolid
    "Heat exchanger wall medium"
    annotation (Dialog(tab="General", group="Medium"),
                choicesAllMatching = true,
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);
  replaceable model WallMaterialCS =
      Media.Solids.MetalsAndMetalAlloys.StainlessSteel_X5CrNi18_10
    constrainedby SorpLib.Media.Solids.BaseClasses.PartialSolid
    "Casing wall medium"
    annotation (Dialog(tab="General", group="Medium"),
                choicesAllMatching = true,
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

  //
  // Definition of parameters regarding transport phenomena
  //
  replaceable model MassTransferCoefficient =
    SorpLib.Components.MassTransfer.MassTransferCoefficientCorrelations.ClosedAdsorber.LoadingDriven.GlueckaufArrhenius
                                                                                                                           constrainedby
    SorpLib.Components.MassTransfer.BaseClasses.PartialMassTransferCoefficientClosedAdsorberDX(
    geometry=geometry)
    "Model calculating the mass transfer coefficient"
    annotation (Dialog(tab="Transport Phenomena", group="Mass Transfer"),
                choicesAllMatching=true,
                HideResult=true,
                Evaluate=true);

  //
  // Definition and instantiation of models
  //
  SorpLib.Basics.Volumes.AdsorbateVolumes.AdsorbatePureVLEVolume[no_sorbentVolumes]
    sorbentVolumes(
    redeclare each final package Medium = MediumAdsorptive,
    redeclare each final model PureWorkingPairModel = WorkingPair,
    redeclare each final SorpLib.Basics.Volumes.Records.VolumeGeometry geometry(
      dx=0,
      dy=0,
      dz=0,
      A_xy=0,
      A_xz=0,
      A_yz=0,
      V=geometry.V_particles*geometry.no_hydraulicParallelTubes),
    each final m_sor_initial=m_sorInitial/geometry.no_sorbentVolumes,
    each final T_initial=T_sorInitial,
    each final x_initial=x_sorInitial,
    each final msor_flow_initialX,
    each final md_flow_initialX,
    each final useAdsorbatePorts=false,
    each final useDiffusivePorts=false,
    each final useHeatPorts=false,
    each final useHeatPortsX=false,
    each final useHeatPortsY=false,
    each final useHeatPortsZ=false,
    each final ms_flow_initial=m_flow_adsorptive_start,
    each final type_adsorbentMassBalance=type_overallMassBalance,
    each final type_adsorptMassBalance=type_overallMassBalance,
    each final type_energyBalance=type_energyBalance,
    each final avoid_events=avoid_events,
    each calcUptakeAveragedProperties=false,
    each independentStateVariables=SorpLib.Choices.IndependentVariablesPureComponentWorkingPair.xT,
    each nSorptionPorts=1)
    "Sorbent volumes"
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={50,-30})));

  SorpLib.Components.HeatTransfer.ClosedAdsorberHeatTransfer[min(no_sorbentVolumes,no_wallVolumes)]
    heatTransfer_HeatExchangerToSorbentVolumes(
    each final n_a=if no_wallVolumes > no_sorbentVolumes then factorDiscretization
      else 1,
    each final n_b=if no_wallVolumes < no_sorbentVolumes then factorDiscretization
      else 1,
    each final calculateFluidProperties=calcFluidTransportProperties,
    redeclare each final model HeatTransferCoefficient =
        VV_HeatExchanger,
    final fluidProperties=SorpLib.Components.HeatTransfer.Records.FluidProperties(
      p=sorbentVolumes.adsorbateProperties.p,
      T=sorbentVolumes.adsorbateProperties.T,
      rho=1 ./ sorbentVolumes.workingPair.medium_sorbent.state_variables.v,
      cp=sorbentVolumes.workingPair.medium_sorbent.additional_variables.c,
      eta=0,
      lambda=sorbentVolumes.workingPair.medium_sorbent.additional_variables.lambda),
    each final geometry=geometry)
    "Heat transfer from heat exchanger to sorbent volumes"
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={50,-54})));

  SorpLib.Components.MassTransfer.VLEMassTransfers.ClosedAdsorberMassTransferDX[no_sorbentVolumes] massTransfer(
    redeclare each final package Medium =MediumAdsorptive,
    redeclare each final model MassTransferCoefficient =MassTransferCoefficient,
    final x_adsorpt_input=sorbentVolumes.x,
    final T_adsorpt_input=sorbentVolumes.T,
    each final canBeActivated=false,
    each final isFlapValve=false,
    each final isFlowAB,
    each final offset_dp,
    each final calculateFluidProperties=calcFluidTransportProperties,
    each final m_flow_start=m_flow_adsorptive_start,
    each final avoid_events=avoid_events,
    each final m_flow_small=m_flow_small,
    each final noDiff=noDiff)
    "Mass transfer between sorbent volumes and gas volume"
    annotation (Placement(transformation(extent={{40,-20},{60,0}})));

  //
  // Definition of variables
  //
  SorpLib.Components.HeatExchanger.Records.SummaryClosedAdsorber summary(
    final p_vapor=vaporVolume.fluidProperties.p,
    final T_vapor=vaporVolume.fluidProperties.T,
    final rho_vapor=1/vaporVolume.fluidProperties.v,
    final mass_vapor=vaporVolume.m,
    final p_adsorbate_avg=sum(sorbentVolumes.adsorbateProperties.p)/
      no_sorbentVolumes,
    final T_adsorbate_avg=sum(sorbentVolumes.adsorbateProperties.T)/
      no_sorbentVolumes,
    final x_adsorbate_avg=sum(sorbentVolumes.adsorbateProperties.x_i[1])/
      no_sorbentVolumes,
    final mass_adsorpt=summary.x_adsorbate_avg*m_sorInitial,
    final p_liq_inlet=heatExchangerTubes.state_a.p,
    final p_liq_outlet=heatExchangerTubes.state_b.p,
    final p_liq_avg=sum(heatExchangerTubes.fluidVolumes.fluidProperties.p)/
        no_fluidVolumes,
    final T_liq_inlet=heatExchangerTubes.state_a.T,
    final T_liq_outlet=heatExchangerTubes.state_b.T,
    final T_liq_avg=sum(heatExchangerTubes.fluidVolumes.fluidProperties.T)/
        no_fluidVolumes,
    final T_wall_avg=sum(heatExchangerTubes.wallVolumes.T)/
      no_wallVolumes,
    final m_flow_liq_inlet=port_a.m_flow,
    final m_flow_liq_outlet=port_b.m_flow,
    final m_flow_evaporator=evaporatorPort.m_flow,
    final m_flow_condenser=condenserPort.m_flow,
    final m_flow_massRecovery=sum(massRecoveryPorts.m_flow),
    final Q_flow_wallToSorbent=heatExchangerTubes.Q_flow_wallHP,
    final Q_flow_fluidWall=heatExchangerTubes.Q_flow_fluidWall,
    final DH_liquid=heatExchangerTubes.DH_flow)
    "Summary record";

equation
  //
  // Connections of fluid ports
  //
  connect(massTransfer.port_b, sorbentVolumes.fp_sorption[1]) annotation (Line(
        points={{58,-10},{70,-10},{70,-44},{51.6,-44},{51.6,-34.4}},
                                                                 color={0,0,0}));
  connect(massTransfer.port_a, vaporVolume.cfp_xMinus) annotation (Line(points={{42.2,
          -10},{20,-10},{20,-20},{-1.8,-20},{-1.8,-4.2}},   color={0,0,0}));

  //
  // Connection of heat ports
  //
  if no_sorbentVolumes == no_wallVolumes then
    //
    // Identical discretization number
    //
    connect(heatExchangerTubes.hp_wall,
      heatTransfer_HeatExchangerToSorbentVolumes.hp_a[1]) annotation (Line(
        points={{50,-76},{50,-62}},
        color={238,46,47},
        thickness=1));
    connect(heatTransfer_HeatExchangerToSorbentVolumes.hp_b[1],
      sorbentVolumes.hp_sorption) annotation (Line(
        points={{50,-46},{50,-44},{48.4,-44},{48.4,-37.6}},
        color={238,46,47},
        thickness=1));

  elseif no_wallVolumes > no_sorbentVolumes then
    //
    // More wall volumes than sorbent volumes
    //
    for i in 1:no_sorbentVolumes loop
      connect(heatExchangerTubes.hp_wall[1+(i-1)*factorDiscretization:i*factorDiscretization],
      heatTransfer_HeatExchangerToSorbentVolumes[i].hp_a[1:factorDiscretization])
        annotation (Line(
          points={{50,-76},{50,-62}},
          color={238,46,47},
          thickness=1));
    end for;

    connect(heatTransfer_HeatExchangerToSorbentVolumes.hp_b[1],
      sorbentVolumes.hp_sorption) annotation (Line(
        points={{50,-46},{50,-44},{48.4,-44},{48.4,-37.6}},
        color={238,46,47},
        thickness=1));

  else
    //
    // More sorbent volumes than wall volumes
    //
    connect(heatExchangerTubes.hp_wall,
      heatTransfer_HeatExchangerToSorbentVolumes.hp_a[1]) annotation (Line(
        points={{50,-76},{50,-62}},
        color={238,46,47},
        thickness=1));

    for i in 1:no_wallVolumes loop
      connect(heatTransfer_HeatExchangerToSorbentVolumes[i].hp_b[1:factorDiscretization],
        sorbentVolumes[1+(i-1)*factorDiscretization:i*factorDiscretization].hp_sorption)
        annotation (Line(
          points={{50,-46},{50,-44},{48.4,-44},{48.4,-37.6}},
          color={238,46,47},
          thickness=1));
    end for;
  end if;

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
TO BE ADDED!
</p>
</html>", revisions="<html>
<ul>
  <li>
  March 5, 2024, by Mirko Engelpracht:<br/>
  Major adaptations due to restructering of the library.
  </li>
  <li>
  January 19, 2021, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end AdsorberDX;
