within SorpLib.Components.HeatExchanger.BaseClasses;
partial model PartialPureComponentAdsorber
  "Base model for all adsorber heat exchangers for pure component adsorption"
  extends SorpLib.Components.HeatExchanger.BaseClasses.PartialHeatExchanger(
    final p_initial,
    final X_i_initial);

  //
  // Definition of general parameters
  //
  parameter Integer nPortsMassTransfer = 0
    "Number of mass transfer ports"
    annotation (Dialog(connectorSizing=true),
                HideResult = true);

  parameter Integer no_fluidVolumes(min=2)=2
    "Discretization number of fluid volumes"
    annotation (Dialog(tab = "General", group = "Discretization"),
                Evaluate=true,
                HideResult=true);
  parameter Integer no_wallVolumes(min=2)=no_fluidVolumes
    "Discretization number of wall volumes (must be a factor of no_fluidVolumes)"
    annotation (Dialog(tab = "General", group = "Discretization"),
                Evaluate=true,
                HideResult=true);
  parameter Integer no_sorbentVolumes(min=1)=no_wallVolumes
    "Discretization number of sorbent volumes (must be a factor of no_wallVolumes)"
    annotation (Dialog(tab = "General", group = "Discretization"),
                Evaluate=true,
                HideResult=true);

  //
  // Definition of parameters regarding the geometry
  //
  replaceable parameter SorpLib.Components.HeatExchanger.Records.GeometryClosedAdsorber geometry
    constrainedby
    SorpLib.Components.HeatExchanger.Records.GeometryClosedAdsorber(
      no_fluidVolumes=no_fluidVolumes,
      no_wallVolumes=no_wallVolumes,
      no_sorbentVolumes=no_sorbentVolumes)
    "Geometry of the closed adsorber"
    annotation (Dialog(tab = "General", group = "Geometry"),
                choicesAllMatching = true);

  //
  // Definition of parameters regarding the medium
  //
  parameter Integer no_adsorptiveComponents = 1
    "Number of adsorptive components"
    annotation (Dialog(tab="General", group="Medium"),
                Evaluate=true,
                HideResult=true);

  //
  // Definition of models describing transport phenomena
  //
  parameter Boolean useCasing = false
    " = true, if casing is modeled"
    annotation (Dialog(tab="Transport Phenomena", group="General"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);
  parameter Boolean useConductionCasing = true
    " = true, if thermal conduction in the casing is modeled"
    annotation (Dialog(tab="Transport Phenomena", group="General",
                enable=useCasing),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);
  parameter Boolean useConductionPerpendicularFlowDirection = true
    " = true, if thermal conduction perpendicular to the flow direction is
    considered within the heat exchanger wall"
    annotation (Dialog(tab="Transport Phenomena", group="General"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);
  parameter Boolean useConductionFlowDirection = false
    " = true, if thermal conduction in the flow direction is considered within 
    the heat exchanger wall"
    annotation (Dialog(tab="Transport Phenomena", group="General"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);
  parameter Boolean calcFluidTransportProperties = true
    " = true, if any transport model needs fluid or transport properties"
    annotation (Dialog(tab="Transport Phenomena", group="General"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);

  replaceable model HX_InnerWallThermalConductionPerpendicularFlowDirection =
    SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.Conduction.CylindricalWall
      ( l_wall=geometry.l_hx/geometry.no_wallVolumes,
        d_inner=geometry.d_hydInner_hx,
        d_outer=(geometry.d_hydInner_hx + geometry.d_hydOuter_hx)/2)
    constrainedby
    SorpLib.Components.HeatTransfer.BaseClasses.PartialConductiveHeatTransferCoefficient
    "Heat transfer correlation describing thermal conduction perpendicular to the
    flow direction within the heat exchanger wall near the fluid"
    annotation (Dialog(tab="Transport Phenomena", group="Thermal Conduction",
                enable = useConductionPerpendicularFlowDirection),
                choicesAllMatching = true,
                Evaluate=true,
                HideResult=true);
  replaceable model HX_OuterWallThermalConductionPerpendicularFlowDirection =
    SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.Conduction.CylindricalWall
      ( l_wall=geometry.l_hx/geometry.no_wallVolumes,
        d_inner=(geometry.d_hydInner_hx + geometry.d_hydOuter_hx)/2,
        d_outer=geometry.d_hydOuter_hx)
    constrainedby
    SorpLib.Components.HeatTransfer.BaseClasses.PartialConductiveHeatTransferCoefficient
    "Heat transfer correlation describing thermal conduction perpendicular to the
    flow direction within the heat exchanger wall near the heat port"
    annotation (Dialog(tab="Transport Phenomena", group="Thermal Conduction",
                enable = useConductionPerpendicularFlowDirection),
                choicesAllMatching = true,
                Evaluate=true,
                HideResult=true);
  replaceable model HX_WallThermalConductionFlowDirection =
    SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.Conduction.PlainWall
      (
      A_cross=geometry.A_crossWall_hx,
      delta_wall=(geometry.l_hx/geometry.no_wallVolumes)/2)
    constrainedby
    SorpLib.Components.HeatTransfer.BaseClasses.PartialConductiveHeatTransferCoefficient
    "Heat transfer correlation describing thermal conduction in the flow direction
    within the heat exchanger wall"
    annotation (Dialog(tab="Transport Phenomena", group="Thermal Conduction",
                enable = useConductionFlowDirection),
                choicesAllMatching = true,
                Evaluate=true,
                HideResult=true);
  replaceable model CS_InnerWallThermalConduction =
    SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.Conduction.CylindricalWall
      ( l_wall=geometry.l_cas,
        d_inner=geometry.d_hydInner_cas,
        d_outer=(geometry.d_hydInner_cas + geometry.d_hydOuter_cas)/2)
    constrainedby
    SorpLib.Components.HeatTransfer.BaseClasses.PartialConductiveHeatTransferCoefficient
    "Heat transfer correlation describing thermal conduction within the casing
    near the phase separator"
    annotation (Dialog(tab="Transport Phenomena", group="Thermal Conduction",
                enable = useCasing and useConductionCasing),
                choicesAllMatching = true,
                Evaluate=true,
                HideResult=true);
  replaceable model CS_OuterWallThermalConduction =
    SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.Conduction.CylindricalWall
      ( l_wall=geometry.l_cas,
        d_inner=(geometry.d_hydInner_cas + geometry.d_hydOuter_cas)/2,
        d_outer=geometry.d_hydOuter_cas)
    constrainedby
    SorpLib.Components.HeatTransfer.BaseClasses.PartialConductiveHeatTransferCoefficient
    "Heat transfer correlation describing thermal conduction within the casing
    near the heat port"
    annotation (Dialog(tab="Transport Phenomena", group="Thermal Conduction",
                enable = useCasing and useConductionCasing),
                choicesAllMatching = true,
                Evaluate=true,
                HideResult=true);

  replaceable model HX_FluidThermalConvectionTubeInside =
    SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.TubeInside.GnielinskiDittusBoelter
    constrainedby
    SorpLib.Components.HeatTransfer.BaseClasses.PartialTubeInsideHeatTransferCoefficient
    "Heat transfer correlation describing thermal convection at the tube inside
    within the heat exchanger (i.e., form the fluid to the wall)"
    annotation (Dialog(tab="Transport Phenomena", group="Thermal Convection"),
                choicesAllMatching = true,
                Evaluate=true,
                HideResult=true);
  replaceable model VV_HeatExchanger =
    HeatTransfer.HeatTransferCoefficientCorrelations.ClosedAdsorber.ConstantAlphaA
    constrainedby
    SorpLib.Components.HeatTransfer.BaseClasses.PartialClosedAdsorberHeatTransferCoefficient
    "Heat transfer correlation describing the heat transfer from the sorbent volumes
    to heat exchanger tubes"
    annotation (Dialog(tab="Transport Phenomena", group="Thermal Convection"),
                choicesAllMatching = true,
                Evaluate=true,
                HideResult=true);

  replaceable model VV_Casing =
    SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.Generic.ConstantAlphaA
    constrainedby
    SorpLib.Components.HeatTransfer.BaseClasses.PartialGenericHeatTransferCoefficient
    "Heat transfer correlation describing the heat transfer between the vapor
    volume and casing"
    annotation (Dialog(tab="Transport Phenomena", group="Generic",
                enable = useCasing),
                choicesAllMatching = true,
                Evaluate=true,
                HideResult=true);

  replaceable model HX_PressureDrop =
    SorpLib.Components.Fittings.PressureLossCorrelations.TubeInside.Konakov
    constrainedby
    SorpLib.Components.Fittings.BaseClasses.PartialTubeInsidePressureLoss
    "Pressure drop correlation describing the pressure drop of the fluid within
    the heat exchanger"
    annotation (Dialog(tab="Transport Phenomena", group="Pressure Drop"),
                choicesAllMatching = true,
                Evaluate=true,
                HideResult=true);

  //
  // Definition of parameters regarding initial values
  //
  parameter Modelica.Units.SI.Pressure p_fluidAInitial = 1.25e5
    "Initial value of fluid pressure at port a"
    annotation (Dialog(tab="Initialisation", group="Initial Values - HX Fluid"));
  parameter Modelica.Units.SI.Pressure p_fluidBInitial = 1e5
    "Initial value of fluid pressure at port b"
    annotation (Dialog(tab="Initialisation", group="Initial Values - HX Fluid"));
  parameter Modelica.Units.SI.Temperature T_fluidAInitial = 283.15
    "Initial value of fluid temperature at port a"
    annotation (Dialog(tab="Initialisation", group="Initial Values - HX Fluid"));
  parameter Modelica.Units.SI.Temperature T_fluidBInitial = 303.15
    "Initial value of fluid temperature at port b"
    annotation (Dialog(tab="Initialisation", group="Initial Values - HX Fluid"));

  parameter Modelica.Units.SI.Temperature T_wallAInitial = T_fluidAInitial
    "Initial value of wall temperature at port a"
    annotation (Dialog(tab="Initialisation", group="Initial Values - HX Wall"));
  parameter Modelica.Units.SI.Temperature T_wallBInitial = T_fluidBInitial
    "Initial value of wall temperature at port b"
    annotation (Dialog(tab="Initialisation", group="Initial Values - HX Wall"));

  parameter Modelica.Units.SI.Pressure p_vaporVolumeInitial = 1e3
    "Initial value of the vapor pressure"
    annotation (Dialog(tab="Initialisation", group="Initial Values - Vapor"));
  parameter Modelica.Units.SI.Temperature T_vaporVolumeInitial = 303.15
    "Initial value of vapor temperature"
    annotation (Dialog(tab="Initialisation", group="Initial Values - Vapor"));

  parameter Modelica.Units.SI.Mass m_sorInitial = 725 * geometry.V_sorbent_hx /
    geometry.no_sorbentVolumes
    "Initial value of the sorbent mass"
    annotation (Dialog(tab="Initialisation", group="Initial Values - Sorbent"));
  parameter SorpLib.Units.Uptake x_sorInitial = 0.15
    "Initial loading of the sorbent"
    annotation (Dialog(tab="Initialisation", group="Initial Values - Sorbent"));
  parameter Modelica.Units.SI.Temperature T_sorInitial = 303.15
    "Initial value of sorbent temperature"
    annotation (Dialog(tab="Initialisation", group="Initial Values - Sorbent"));


  parameter Modelica.Units.SI.Temperature T_casingInitial = 298.15
    "Initial value of casing temperature"
    annotation (Dialog(tab="Initialisation", group="Initial Values - Casing",
                enable=useCasing));

  //
  // Definition of parameters regarding start values
  //
  parameter Modelica.Units.SI.MassFlowRate m_flow_adsorptive_start = 1e-4
    "Start value for adsorptive mass flow rate"
    annotation (Dialog(tab="Initialisation", group="Start Values"));

  //
  // Definition of ports
  //
  SorpLib.Basics.Interfaces.FluidPorts.VLEPort_in evaporatorPort(final
      no_components=no_adsorptiveComponents, m_flow(start=
          m_flow_adsorptive_start)) "Evaporator port" annotation (Placement(
        transformation(extent={{-110,30},{-90,50}}), iconTransformation(extent={
            {-110,30},{-90,50}})));
  SorpLib.Basics.Interfaces.FluidPorts.VLEPort_out condenserPort(final
      no_components=no_adsorptiveComponents, m_flow(start=-
          m_flow_adsorptive_start)) "Condenser port" annotation (Placement(
        transformation(extent={{90,30},{110,50}}), iconTransformation(extent={{90,
            30},{110,50}})));
  SorpLib.Basics.Interfaces.FluidPorts.VLEPort_out[nPortsMassTransfer] massRecoveryPorts(
    each final no_components=no_adsorptiveComponents,
    each final m_flow(start=m_flow_adsorptive_start)) "Mass recovery ports"
    annotation (Placement(transformation(extent={{-10,50},{10,70}}),
        iconTransformation(extent={{-10,50},{10,70}})));

  SorpLib.Basics.Interfaces.HeatPorts.HeatPort_in hp_casing if
    useCasing
    "Heat ports at casing"
    annotation (Placement(transformation(extent={{-10,-50},{10,-70}}),
                iconTransformation(extent={{-10,-70},{10,-50}})));

  //
  // Definition and instanziation of models
  //
  replaceable Basics.Volumes.BaseClasses.PartialFluidVolume vaporVolume(
    final calculateAdditionalProperties=calcFluidTransportProperties,
    redeclare final SorpLib.Basics.Volumes.Records.VolumeGeometry
      geometry(
      dx=0,
      dy=0,
      dz=0,
      A_xy=0,
      A_xz=0,
      A_yz=0,
      V=geometry.V_vapor_cas),
    final useHeatPorts=true,
    final useHeatPortsX=false,
    final useHeatPortsY=true,
    final useHeatPortsZ=false,
    final p_initial=p_vaporVolumeInitial,
    final T_initial=T_vaporVolumeInitial,
    final mc_flow_initialX=m_flow_adsorptive_start,
    final type_energyBalance=type_energyBalance,
    final type_overallMassBalance=type_overallMassBalance,
    final avoid_events=avoid_events,
    independentStateVariables=SorpLib.Choices.IndependentVariablesVolume.phX,
    nPorts_cfp_xPlus=2+nPortsMassTransfer)
    "Vapor volume"
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={0,0})));

  replaceable SorpLib.Components.Tubes.BaseClasses.PartialTube heatExchangerTubes(
    final no_fluidVolumes=no_fluidVolumes,
    final no_wallVolumes=no_wallVolumes,
    redeclare final SorpLib.Components.Tubes.Records.GeometryTube geometry(
      no_hydraulicParallelTubes=geometry.no_hydraulicParallelTubes,
      l=geometry.l_hx,
      roughness=geometry.roughness_hx,
      d_inner=geometry.d_inner_hx,
      d_outer=geometry.d_outer_hx,
      d_hydInner=geometry.d_hydInner_hx,
      d_hydOuter=geometry.d_hydOuter_hx,
      t_wall=geometry.t_wall_hx,
      A_crossInner=geometry.A_crossInner_hx,
      A_crossOuter=geometry.A_crossOuter_hx,
      A_crossWall=geometry.A_crossWall_hx,
      A_hydCrossInner=geometry.A_hydCrossInner_hx,
      A_hydCrossOuter=geometry.A_hydCrossOuter_hx,
      A_hydCrossWall=geometry.A_hydCrossWall_hx,
      A_heatTransferInner=geometry.A_heatTransferInner_hx,
      A_heatTransferOuter=geometry.A_heatTransferOuter_hx,
      f_finAreaRatioInner=geometry.f_finAreaRatioInner_hx,
      f_finAreaRatioOuter=geometry.f_finAreaRatioOuter_hx,
      V_inner=geometry.V_inner_hx,
      V_outer=geometry.V_outer_hx,
      V_wall=geometry.V_wall_hx,
      f_finVolumeRatioInner=geometry.f_finVolumeRatioInner_hx,
      f_finVolumeRatioOuter=geometry.f_finVolumeRatioOuter_hx),
    final useConductionPerpendicularFlowDirection=
      useConductionPerpendicularFlowDirection,
    final useConductionFlowDirection=useConductionFlowDirection,
    final calcFluidTransportProperties=calcFluidTransportProperties,
    redeclare final model InnerWallThermalConductionPerpendicularFlowDirection =
      HX_InnerWallThermalConductionPerpendicularFlowDirection,
    redeclare final model OuterWallThermalConductionPerpendicularFlowDirection =
      HX_OuterWallThermalConductionPerpendicularFlowDirection,
    redeclare final model WallThermalConductionFlowDirection =
      HX_WallThermalConductionFlowDirection,
    redeclare final model FluidThermalConvectionTubeInside =
      HX_FluidThermalConvectionTubeInside,
    redeclare final model PressureDrop = HX_PressureDrop,
    final p_fluidAInitial=p_fluidAInitial,
    final p_fluidBInitial=p_fluidBInitial,
    final T_fluidAInitial=T_fluidAInitial,
    final T_fluidBInitial=T_fluidBInitial,
    final T_wallAInitial=T_wallAInitial,
    final T_wallBInitial=T_wallBInitial,
    final m_flow_start=m_flow_start,
    final type_overallMassBalance=type_overallMassBalance,
    final type_independentMassBalances=type_independentMassBalances,
    final avoid_events=avoid_events,
    final m_flow_small=m_flow_small,
    final noDiff=noDiff)
    "Heat exchanger tubes"
    annotation (Placement(transformation(extent={{40,-90},{60,-70}})));

  replaceable SorpLib.Basics.Volumes.BaseClasses.PartialVolume casing(
    final useHeatPortsX=false,
    final useHeatPortsY=true,
    final useHeatPortsZ=false,
    final type_energyBalance=type_energyBalance,
    final avoid_events=avoid_events) if useCasing
    "Casing"
    annotation (Placement(transformation(extent={{-68,10},{-48,-10}})));

  SorpLib.Components.HeatTransfer.GenericHeatTransfer heatTransfer_vaporVolumeToCasing(
    final n_a=1,
    final n_b=1,
    final calculateFluidProperties=calcFluidTransportProperties,
    redeclare final model HeatTransferCoefficient = VV_Casing) if
                    useCasing "Heat transfer from vapor volume to casing"
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={-32,0})));

  HeatTransfer.ConductionHeatTransfer thermalConduction_casing1(
    final n_a=1,
    final n_b=1,
    final calculateFluidProperties=calcFluidTransportProperties,
    redeclare final model HeatTransferCoefficient =
        CS_InnerWallThermalConduction,
    final no_hydraulicParallelFlows=geometry.no_hydraulicParallelTubes) if
    (useCasing and useConductionCasing)
    "Thermal conduction of the first casing half"
    annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={-58,22})));

  HeatTransfer.ConductionHeatTransfer thermalConduction_casing2(
    final n_a=1,
    final n_b=1,
    final calculateFluidProperties=calcFluidTransportProperties,
    redeclare final model HeatTransferCoefficient =
        CS_OuterWallThermalConduction,
    final no_hydraulicParallelFlows=geometry.no_hydraulicParallelTubes) if
    (useCasing and useConductionCasing)
    "Thermal conduction of the second casing half"
    annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={-58,-22})));

  //
  // Definition of protected parameters
  //
protected
  final parameter Integer factorDiscretization=
    integer(max(no_sorbentVolumes,no_wallVolumes)/
    min(no_sorbentVolumes,no_wallVolumes))
    "Discretization factor"
    annotation (Dialog(tab = "General", group = "Discretization"),
                Evaluate=true,
                HideResult=true);

equation
  //
  // Assertations
  //
  if no_wallVolumes < no_fluidVolumes then
    assert(rem(no_fluidVolumes, no_wallVolumes) == 0,
      "Number of wall volumes must be a factor of the number of fluid volumes!");

  elseif no_wallVolumes > no_fluidVolumes then
    assert(rem(no_wallVolumes, no_fluidVolumes) == 0,
      "Number of wall volumes must be a factor of the number of fluid volumes!");

  end if;

  if no_wallVolumes < no_sorbentVolumes then
    assert(rem(no_sorbentVolumes, no_wallVolumes) == 0,
      "Number of wall volumes must be a factor of the number of sorbent volumes!");

  elseif no_wallVolumes > no_sorbentVolumes then
    assert(rem(no_wallVolumes, no_sorbentVolumes) == 0,
      "Number of wall volumes must be a factor of the number of sorbent volumes!");

  end if;

  //
  // Connection of fluid ports
  //
  connect(port_a, heatExchangerTubes.fp_a) annotation (Line(points={{-100,0},{-80,
          0},{-80,-80},{40,-80}}, color={0,0,0}));
  connect(heatExchangerTubes.fp_b, port_b) annotation (Line(points={{60,-80},{80,
          -80},{80,0},{100,0}}, color={0,0,0}));

  connect(evaporatorPort, vaporVolume.cfp_xPlus[1]) annotation (Line(
      points={{-100,40},{-1.8,40},{-1.8,7.8}},
      color={0,140,72},
      thickness=1));
  connect(condenserPort, vaporVolume.cfp_xPlus[2]) annotation (Line(
      points={{100,40},{-1.8,40},{-1.8,7.8}},
      color={0,140,72},
      thickness=1));

  if nPortsMassTransfer > 0 then
    connect(massRecoveryPorts, vaporVolume.cfp_xPlus[3:2+nPortsMassTransfer]) annotation (Line(
        points={{0,60},{0,40},{-2,40},{-2,8},{-1.8,8},{-1.8,7.8}},
        color={0,140,72},
        thickness=1));
  end if;

  //
  // Connection of heat ports
  //
  connect(vaporVolume.hp_yPlus, heatTransfer_vaporVolumeToCasing.hp_a[1])
    annotation (Line(
      points={{-6,0},{-24,0}},
      color={238,46,47},
      thickness=1));

  if useCasing then
    if useConductionCasing then
      connect(heatTransfer_vaporVolumeToCasing.hp_b[1],
        thermalConduction_casing1.hp_a[1]) annotation (Line(
          points={{-40,4.44089e-16},{-42,4.44089e-16},{-42,32},{-58,32},{-58,30}},
          color={238,46,47},
          thickness=1));

      connect(thermalConduction_casing1.hp_b[1], casing.hp_yMinus) annotation (Line(
          points={{-58,14},{-58,6}},
          color={238,46,47},
          thickness=1));
      connect(casing.hp_yPlus, thermalConduction_casing2.hp_a[1]) annotation (Line(
          points={{-58,-6},{-58,-14}},
          color={238,46,47},
          thickness=1));
      connect(thermalConduction_casing2.hp_b[1], hp_casing) annotation (Line(
          points={{-58,-30},{-58,-32},{0,-32},{0,-60}},
          color={238,46,47},
          thickness=1));

    else
      connect(heatTransfer_vaporVolumeToCasing.hp_b[1], hp_casing);

    end if;
  end if;

  //
  // Annotations
  //
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Rectangle(
          extent={{-100,60},{100,-60}},
          lineColor={0,0,0},
          lineThickness=1),
        Line(
          points={{100,40},{-80,40},{80,0},{-80,-40},{100,-40}},
          color={0,0,0},
          thickness=1),
        Ellipse(
          extent={{-74,42},{-80,48}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-14,42},{-20,48}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-26,42},{-32,48}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-38,42},{-44,48}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-50,42},{-56,48}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-62,42},{-68,48}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{58,42},{52,48}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{46,42},{40,48}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{34,42},{28,48}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{22,42},{16,48}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{10,42},{4,48}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-2,42},{-8,48}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{70,42},{64,48}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{82,42},{76,48}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{82,-48},{76,-42}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{70,-48},{64,-42}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{58,-48},{52,-42}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{46,-48},{40,-42}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{34,-48},{28,-42}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{22,-48},{16,-42}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{10,-48},{4,-42}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-2,-48},{-8,-42}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-14,-48},{-20,-42}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-26,-48},{-32,-42}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-38,-48},{-44,-42}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-50,-48},{-56,-42}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-62,-48},{-68,-42}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-74,-48},{-80,-42}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{88,-38},{82,-32}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{76,-38},{70,-32}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{64,-38},{58,-32}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{52,-38},{46,-32}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{40,-38},{34,-32}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{28,-38},{22,-32}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{16,-38},{10,-32}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{4,-38},{-2,-32}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-8,-38},{-14,-32}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-20,-38},{-26,-32}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-32,-38},{-38,-32}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-44,-40},{-50,-34}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-44,34},{-50,40}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-32,32},{-38,38}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-20,32},{-26,38}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-8,32},{-14,38}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{4,32},{-2,38}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{16,32},{10,38}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{28,32},{22,38}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{40,32},{34,38}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{52,32},{46,38}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{64,32},{58,38}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{76,32},{70,38}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{88,32},{82,38}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-66,30},{-72,36}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-52,26},{-58,32}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-36,22},{-42,28}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-18,18},{-24,24}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-4,14},{-10,20}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{12,10},{6,16}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{30,6},{24,12}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{44,2},{38,8}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{60,-2},{54,4}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-8,24},{-14,30}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{6,20},{0,26}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{22,16},{16,22}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{38,12},{32,18}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{52,8},{46,14}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{68,4},{62,10}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-66,-36},{-72,-30}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-52,-32},{-58,-26}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-36,-28},{-42,-22}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-18,-24},{-24,-18}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-4,-20},{-10,-14}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{12,-16},{6,-10}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{30,-12},{24,-6}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{44,-8},{38,-2}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{68,-10},{62,-4}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{52,-14},{46,-8}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{38,-18},{32,-12}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{24,-22},{18,-16}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{6,-26},{0,-20}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-8,-30},{-14,-24}},
          lineColor={244,125,35},
          lineThickness=1,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-44,24},{-50,30}},
          lineColor={0,140,72},
          lineThickness=1,
          fillColor={0,140,72},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-54,34},{-60,40}},
          lineColor={0,140,72},
          lineThickness=1,
          fillColor={0,140,72},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-76,34},{-82,40}},
          lineColor={0,140,72},
          lineThickness=1,
          fillColor={0,140,72},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-8,42},{-14,48}},
          lineColor={0,140,72},
          lineThickness=1,
          fillColor={0,140,72},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-14,26},{-20,32}},
          lineColor={0,140,72},
          lineThickness=1,
          fillColor={0,140,72},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{4,12},{-2,18}},
          lineColor={0,140,72},
          lineThickness=1,
          fillColor={0,140,72},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{30,14},{24,20}},
          lineColor={0,140,72},
          lineThickness=1,
          fillColor={0,140,72},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{52,0},{46,6}},
          lineColor={0,140,72},
          lineThickness=1,
          fillColor={0,140,72},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{34,32},{28,38}},
          lineColor={0,140,72},
          lineThickness=1,
          fillColor={0,140,72},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{76,42},{70,48}},
          lineColor={0,140,72},
          lineThickness=1,
          fillColor={0,140,72},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{82,32},{76,38}},
          lineColor={0,140,72},
          lineThickness=1,
          fillColor={0,140,72},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{20,-14},{14,-8}},
          lineColor={0,140,72},
          lineThickness=1,
          fillColor={0,140,72},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-28,-26},{-34,-20}},
          lineColor={0,140,72},
          lineThickness=1,
          fillColor={0,140,72},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-52,-40},{-58,-34}},
          lineColor={0,140,72},
          lineThickness=1,
          fillColor={0,140,72},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-78,-42},{-84,-36}},
          lineColor={0,140,72},
          lineThickness=1,
          fillColor={0,140,72},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-32,-48},{-38,-42}},
          lineColor={0,140,72},
          lineThickness=1,
          fillColor={0,140,72},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{10,-38},{4,-32}},
          lineColor={0,140,72},
          lineThickness=1,
          fillColor={0,140,72},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{40,-48},{34,-42}},
          lineColor={0,140,72},
          lineThickness=1,
          fillColor={0,140,72},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{70,-38},{64,-32}},
          lineColor={0,140,72},
          lineThickness=1,
          fillColor={0,140,72},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{46,-16},{40,-10}},
          lineColor={0,140,72},
          lineThickness=1,
          fillColor={0,140,72},
          fillPattern=FillPattern.Solid)}),
    Diagram(coordinateSystem(extent={{-100,-100},{100,100}})),
    Documentation(info="<html>
<p>
This partial model is the base model for all closed adsorber heat exchanger. It defines 
fundamental parameters, models, and variables required by all closed adsorbers. Models 
that inherit properties from this partial model have to redeclare all partial models 
(i.e., fluid ports, tube, vapor volume, and casing). Moreover, sorbent volumes must be
added as well as a mass transfer model describing the mass transfer between the sorbent 
and vapor and a heat transfer model describing the heat transfer between the sorbent and
heat exchanger. These models are not inserted in the partial model to allow for designing
different heat exchanger cofigurations with various level of detal. Furthermore, 
the geometry of the redeclared heat trasnfer must be correctly set using the geometry 
record. In addition, the inputs 'fluidProperties' of the conductive heat transfer models 
must be correctly set using properties of the casing.
</p>
</html>", revisions="<html>
<ul>
  <li>
  March 5, 2024, by Mirko Engelpracht:<br/>
  Major adaptations due to restructering of the library and documentation.
  </li>
  <li>
  January 19, 2021, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end PartialPureComponentAdsorber;
