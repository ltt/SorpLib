within SorpLib.Components.HeatExchanger.BaseClasses;
partial model PartialSimpleCondenserEvaporator
  "Base model for all simple condensers and evaporators"
  extends SorpLib.Components.HeatExchanger.BaseClasses.PartialHeatExchanger(
    final p_initial,
    final X_i_initial);

  //
  // Definition of general parameters
  //
  parameter Integer nPortsLiquid = 0
    "Number of liquid VLE ports"
    annotation (Dialog(connectorSizing=true),
                HideResult = true);
  parameter Integer nPortsVapor = 0
    "Number of vapor VLE ports"
    annotation (Dialog(connectorSizing=true),
                HideResult = true);

  parameter Integer no_fluidVolumes(min=2)=10
    "Discretization number of fluid volumes"
    annotation (Dialog(tab = "General", group = "Discretization"),
                Evaluate=true,
                HideResult=true);
  parameter Integer no_wallVolumes(min=2)=no_fluidVolumes
    "Discretization number of wall volumes (must be a factor of no_fluidVolumes)"
    annotation (Dialog(tab = "General", group = "Discretization"),
                Evaluate=true,
                HideResult=true);

  //
  // Definition of parameters regarding the geometry
  //
  replaceable parameter SorpLib.Components.HeatExchanger.Records.GeometryCondenserEvaporator geometry
    constrainedby
    SorpLib.Components.HeatExchanger.Records.GeometryCondenserEvaporator(
      no_fluidVolumes=no_fluidVolumes,
      no_wallVolumes=no_wallVolumes)
    "Geometry of the condenser or evaporator"
    annotation (Dialog(tab = "General", group = "Geometry"),
                choicesAllMatching = true);

  //
  // Definition of parameters regarding the medium
  //
  parameter Integer no_VLEcomponents = 1
    "Number of components within the VLE"
    annotation (Dialog(tab="General", group="Medium"),
                Evaluate=true,
                HideResult=true);

  //
  // Definition of models describing transport phenomena
  //
  parameter Boolean useCasing = false
    " = true, if casing is modeled"
    annotation (Dialog(tab="Transport Phenomena", group="General"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);
  parameter Boolean useConductionCasing = true
    " = true, if thermal conduction in the casing is modeled"
    annotation (Dialog(tab="Transport Phenomena", group="General",
                enable=useCasing),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);
  parameter Boolean useConductionPerpendicularFlowDirection = true
    " = true, if thermal conduction perpendicular to the flow direction is
    considered within the heat exchanger wall"
    annotation (Dialog(tab="Transport Phenomena", group="General"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);
  parameter Boolean useConductionFlowDirection = false
    " = true, if thermal conduction in the flow direction is considered within 
    the heat exchanger wall"
    annotation (Dialog(tab="Transport Phenomena", group="General"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);
  parameter Boolean calcFluidTransportProperties = true
    " = true, if any transport model needs fluid or transport properties"
    annotation (Dialog(tab="Transport Phenomena", group="General"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);
  parameter Integer fluidPropertyPosition = 1
    "Defines the position for fluid porperty calculation is discretization number
    is different"
    annotation (Dialog(tab="Transport Phenomena", group="General",
                enable=no_fluidVolumes<>no_wallVolumes),
                choices(choice=1 "First volume near port a",
                        choice=2 "First volume near port b",
                        choice=3 "Properties averaged over volumes"),
                Evaluate=true,
                HideResult=true);

  replaceable model HX_InnerWallThermalConductionPerpendicularFlowDirection =
    SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.Conduction.CylindricalWall
      ( l_wall=geometry.l_hx/geometry.no_wallVolumes,
        d_inner=geometry.d_hydInner_hx,
        d_outer=(geometry.d_hydInner_hx + geometry.d_hydOuter_hx)/2)
    constrainedby
    SorpLib.Components.HeatTransfer.BaseClasses.PartialConductiveHeatTransferCoefficient
    "Heat transfer correlation describing thermal conduction perpendicular to the
    flow direction within the heat exchanger wall near the fluid"
    annotation (Dialog(tab="Transport Phenomena", group="Thermal Conduction",
                enable = useConductionPerpendicularFlowDirection),
                choicesAllMatching = true,
                Evaluate=true,
                HideResult=true);
  replaceable model HX_OuterWallThermalConductionPerpendicularFlowDirection =
    SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.Conduction.CylindricalWall
      ( l_wall=geometry.l_hx/geometry.no_wallVolumes,
        d_inner=(geometry.d_hydInner_hx + geometry.d_hydOuter_hx)/2,
        d_outer=geometry.d_hydOuter_hx)
    constrainedby
    SorpLib.Components.HeatTransfer.BaseClasses.PartialConductiveHeatTransferCoefficient
    "Heat transfer correlation describing thermal conduction perpendicular to the
    flow direction within the heat exchanger wall near the heat port"
    annotation (Dialog(tab="Transport Phenomena", group="Thermal Conduction",
                enable = useConductionPerpendicularFlowDirection),
                choicesAllMatching = true,
                Evaluate=true,
                HideResult=true);
  replaceable model HX_WallThermalConductionFlowDirection =
    SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.Conduction.PlainWall
      (
      A_cross=geometry.A_crossWall_hx,
      delta_wall=(geometry.l_hx/geometry.no_wallVolumes)/2)
    constrainedby
    SorpLib.Components.HeatTransfer.BaseClasses.PartialConductiveHeatTransferCoefficient
    "Heat transfer correlation describing thermal conduction in the flow direction
    within the heat exchanger wall"
    annotation (Dialog(tab="Transport Phenomena", group="Thermal Conduction",
                enable = useConductionFlowDirection),
                choicesAllMatching = true,
                Evaluate=true,
                HideResult=true);
  replaceable model CS_InnerWallThermalConduction =
    SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.Conduction.CylindricalWall
      ( l_wall=geometry.l_cas,
        d_inner=geometry.d_hydInner_cas,
        d_outer=(geometry.d_hydInner_cas + geometry.d_hydOuter_cas)/2)
    constrainedby
    SorpLib.Components.HeatTransfer.BaseClasses.PartialConductiveHeatTransferCoefficient
    "Heat transfer correlation describing thermal conduction within the casing
    near the phase separator"
    annotation (Dialog(tab="Transport Phenomena", group="Thermal Conduction",
                enable = useCasing and useConductionCasing),
                choicesAllMatching = true,
                Evaluate=true,
                HideResult=true);
  replaceable model CS_OuterWallThermalConduction =
    SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.Conduction.CylindricalWall
      ( l_wall=geometry.l_cas,
        d_inner=(geometry.d_hydInner_cas + geometry.d_hydOuter_cas)/2,
        d_outer=geometry.d_hydOuter_cas)
    constrainedby
    SorpLib.Components.HeatTransfer.BaseClasses.PartialConductiveHeatTransferCoefficient
    "Heat transfer correlation describing thermal conduction within the casing
    near the heat port"
    annotation (Dialog(tab="Transport Phenomena", group="Thermal Conduction",
                enable = useCasing and useConductionCasing),
                choicesAllMatching = true,
                Evaluate=true,
                HideResult=true);

  replaceable model HX_FluidThermalConvectionTubeInside =
    SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.TubeInside.GnielinskiDittusBoelter
    constrainedby
    SorpLib.Components.HeatTransfer.BaseClasses.PartialTubeInsideHeatTransferCoefficient
    "Heat transfer correlation describing thermal convection at the tube inside
    within the heat exchanger (i.e., form the fluid to the wall)"
    annotation (Dialog(tab="Transport Phenomena", group="Thermal Convection"),
                choicesAllMatching = true,
                Evaluate=true,
                HideResult=true);

  replaceable model PS_Casing =
    SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.Generic.ConstantAlphaA
    constrainedby
    SorpLib.Components.HeatTransfer.BaseClasses.PartialGenericHeatTransferCoefficient
    "Heat transfer correlation describing the heat transfer between the phase
    separator and casing"
    annotation (Dialog(tab="Transport Phenomena", group="Generic",
                enable = useCasing),
                choicesAllMatching = true,
                Evaluate=true,
                HideResult=true);

  replaceable model HX_PressureDrop =
    SorpLib.Components.Fittings.PressureLossCorrelations.TubeInside.Konakov
    constrainedby
    SorpLib.Components.Fittings.BaseClasses.PartialTubeInsidePressureLoss
    "Pressure drop correlation describing the pressure drop of the fluid within
    the heat exchanger"
    annotation (Dialog(tab="Transport Phenomena", group="Pressure Drop"),
                choicesAllMatching = true,
                Evaluate=true,
                HideResult=true);

  //
  // Definition of parameters regarding initial values
  //
  parameter Modelica.Units.SI.Pressure p_fluidAInitial = 1.25e5
    "Initial value of fluid pressure at port a"
    annotation (Dialog(tab="Initialisation", group="Initial Values - HX Fluid"));
  parameter Modelica.Units.SI.Pressure p_fluidBInitial = 1e5
    "Initial value of fluid pressure at port b"
    annotation (Dialog(tab="Initialisation", group="Initial Values - HX Fluid"));
  parameter Modelica.Units.SI.Temperature T_fluidAInitial = 283.15
    "Initial value of fluid temperature at port a"
    annotation (Dialog(tab="Initialisation", group="Initial Values - HX Fluid"));
  parameter Modelica.Units.SI.Temperature T_fluidBInitial = 303.15
    "Initial value of fluid temperature at port b"
    annotation (Dialog(tab="Initialisation", group="Initial Values - HX Fluid"));

  parameter Modelica.Units.SI.Temperature T_wallAInitial = T_fluidAInitial
    "Initial value of wall temperature at port a"
    annotation (Dialog(tab="Initialisation", group="Initial Values - HX Wall"));
  parameter Modelica.Units.SI.Temperature T_wallBInitial = T_fluidBInitial
    "Initial value of wall temperature at port b"
    annotation (Dialog(tab="Initialisation", group="Initial Values - HX Wall"));

  parameter Modelica.Units.SI.Density d_phaseSeparatorInitial = 300
    "Initial value of phase separator density"
    annotation (Dialog(tab="Initialisation", group="Initial Values - Phase Separator"));
  parameter Modelica.Units.SI.Temperature T_phaseSeparatorInitial = 288.15
    "Initial value of phase separator temperature"
    annotation (Dialog(tab="Initialisation", group="Initial Values - Phase Separator"));

  parameter Modelica.Units.SI.Temperature T_casingInitial = 298.15
    "Initial value of casing temperature"
    annotation (Dialog(tab="Initialisation", group="Initial Values - Casing",
                enable=useCasing));

  //
  // Definition of parameters regarding start values
  //
  parameter Modelica.Units.SI.MassFlowRate m_flow_VLE_start = 1e-4
    "Start value for mass flow rate"
    annotation (Dialog(tab="Initialisation", group="Start Values"));

  //
  // Definition of ports
  //
  SorpLib.Basics.Interfaces.FluidPorts.VLEPort_in[nPortsLiquid] liquidPort(
    each final no_components=no_VLEcomponents,
    each m_flow(start=m_flow_VLE_start))
    "Liquid port"
    annotation (Placement(transformation(extent={{-10,-28},{10,-48}}),
        iconTransformation(extent={{-10,-48},{10,-28}})));
  SorpLib.Basics.Interfaces.FluidPorts.VLEPort_out[nPortsVapor] vaporPort(
    each final no_components=no_VLEcomponents,
    each m_flow(start=-m_flow_VLE_start))
    "Vapor port"
    annotation (Placement(transformation(extent={{-10,32},{10,52}}),
        iconTransformation(extent={{-10,32},{10,52}})));

  SorpLib.Basics.Interfaces.HeatPorts.HeatPort_in hp_casing if
    useCasing
    "Heat ports at casing"
    annotation (Placement(transformation(extent={{-60,30},{-40,50}}),
                iconTransformation(extent={{-60,30},{-40,50}})));

  //
  // Definition and instanziation of models
  //
  replaceable SorpLib.Basics.Volumes.BaseClasses.PartialPhaseSeparatorVolume
    phaseSeparatorVolume(
    final calculateAdditionalProperties=calcFluidTransportProperties,
    final T_initial=T_phaseSeparatorInitial,
    final rho_initial=d_phaseSeparatorInitial,
    final mc_flow_initialX=m_flow_VLE_start,
    redeclare final SorpLib.Basics.Volumes.Records.PhaseSeparatorGeometry geometry(
      dx=0,
      dy=0,
      dz=0,
      A_xy=0,
      A_xz=0,
      A_yz=0,
      V=geometry.V_refrigerant,
      A_base=geometry.A_refrigerant),
    final useHeatPorts=true,
    final useHeatPortsX=false,
    final useHeatPortsY=true,
    final useHeatPortsZ=false,
    final type_energyBalance=type_energyBalance,
    final type_overallMassBalance=type_overallMassBalance,
    final avoid_events=avoid_events,
    nPorts_cfp_xMinus=nPortsLiquid,
    nPorts_cfp_xPlus=nPortsVapor)
    "Phase separator"
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={0,0})));

  replaceable SorpLib.Components.Tubes.BaseClasses.PartialTube heatExchangerTubes(
    final no_fluidVolumes=no_fluidVolumes,
    final no_wallVolumes=no_wallVolumes,
    redeclare final SorpLib.Components.Tubes.Records.GeometryTube geometry(
      no_hydraulicParallelTubes=geometry.no_hydraulicParallelTubes,
      l=geometry.l_hx,
      roughness=geometry.roughness_hx,
      d_inner=geometry.d_inner_hx,
      d_outer=geometry.d_outer_hx,
      d_hydInner=geometry.d_hydInner_hx,
      d_hydOuter=geometry.d_hydOuter_hx,
      t_wall=geometry.t_wall_hx,
      A_crossInner=geometry.A_crossInner_hx,
      A_crossOuter=geometry.A_crossOuter_hx,
      A_crossWall=geometry.A_crossWall_hx,
      A_hydCrossInner=geometry.A_hydCrossInner_hx,
      A_hydCrossOuter=geometry.A_hydCrossOuter_hx,
      A_hydCrossWall=geometry.A_hydCrossWall_hx,
      A_heatTransferInner=geometry.A_heatTransferInner_hx,
      A_heatTransferOuter=geometry.A_heatTransferOuter_hx,
      f_finAreaRatioInner=geometry.f_finAreaRatioInner_hx,
      f_finAreaRatioOuter=geometry.f_finAreaRatioOuter_hx,
      V_inner=geometry.V_inner_hx,
      V_outer=geometry.V_outer_hx,
      V_wall=geometry.V_wall_hx,
      f_finVolumeRatioInner=geometry.f_finVolumeRatioInner_hx,
      f_finVolumeRatioOuter=geometry.f_finVolumeRatioOuter_hx),
    final useConductionPerpendicularFlowDirection=
      useConductionPerpendicularFlowDirection,
    final useConductionFlowDirection=useConductionFlowDirection,
    final calcFluidTransportProperties=calcFluidTransportProperties,
    final fluidPropertyPosition=fluidPropertyPosition,
    redeclare final model InnerWallThermalConductionPerpendicularFlowDirection =
      HX_InnerWallThermalConductionPerpendicularFlowDirection,
    redeclare final model OuterWallThermalConductionPerpendicularFlowDirection =
      HX_OuterWallThermalConductionPerpendicularFlowDirection,
    redeclare final model WallThermalConductionFlowDirection =
      HX_WallThermalConductionFlowDirection,
    redeclare final model FluidThermalConvectionTubeInside =
      HX_FluidThermalConvectionTubeInside,
    redeclare final model PressureDrop = HX_PressureDrop,
    final p_fluidAInitial=p_fluidAInitial,
    final p_fluidBInitial=p_fluidBInitial,
    final T_fluidAInitial=T_fluidAInitial,
    final T_fluidBInitial=T_fluidBInitial,
    final T_wallAInitial=T_wallAInitial,
    final T_wallBInitial=T_wallBInitial,
    final m_flow_start=m_flow_start,
    final type_overallMassBalance=type_overallMassBalance,
    final type_independentMassBalances=type_independentMassBalances,
    final avoid_events=avoid_events,
    final m_flow_small=m_flow_small,
    final noDiff=noDiff)
    "Heat exchanger tubes"
    annotation (Placement(transformation(extent={{40,-60},{60,-40}})));

  replaceable SorpLib.Basics.Volumes.BaseClasses.PartialVolume casing(
    final useHeatPortsX=false,
    final useHeatPortsY=true,
    final useHeatPortsZ=false,
    final type_energyBalance=type_energyBalance,
    final avoid_events=avoid_events) if useCasing
    "Casing"
    annotation (Placement(transformation(extent={{-60,-10},{-40,10}})));

  replaceable SorpLib.Components.HeatTransfer.BaseClasses.PartialHeatTransfer
    heatTransfer_HeatExchangerToPhaseSeparator(
    final n_a=no_wallVolumes,
    final n_b=1,
    final calculateFluidProperties=calcFluidTransportProperties)
    "Heat transfer from heat exchanger to phase separator" annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={50,-20})));

  SorpLib.Components.HeatTransfer.GenericHeatTransfer heatTransfer_phaseSeparatorToCasing(
    final n_a=1,
    final n_b=1,
    final calculateFluidProperties=calcFluidTransportProperties,
    redeclare final model HeatTransferCoefficient =
        PS_Casing) if useCasing
    "Heat transfer from phase seprator to casing"
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={-24,0})));

  HeatTransfer.ConductionHeatTransfer thermalConduction_casing1(
    final n_a=1,
    final n_b=1,
    final calculateFluidProperties=calcFluidTransportProperties,
    redeclare final model HeatTransferCoefficient =
        CS_InnerWallThermalConduction,
    final no_hydraulicParallelFlows=geometry.no_hydraulicParallelTubes) if
    (useCasing and useConductionCasing)
    "Thermal conduction of the first casing half"
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-50,-24})));

  HeatTransfer.ConductionHeatTransfer thermalConduction_casing2(
    final n_a=1,
    final n_b=1,
    final calculateFluidProperties=calcFluidTransportProperties,
    redeclare final model HeatTransferCoefficient =
        CS_OuterWallThermalConduction,
    final no_hydraulicParallelFlows=geometry.no_hydraulicParallelTubes) if
    (useCasing and useConductionCasing)
    "Thermal conduction of the second casing half"
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-50,24})));

equation
  //
  // Assertations
  //
  if no_wallVolumes < no_fluidVolumes then
    assert(rem(no_fluidVolumes, no_wallVolumes) == 0,
      "Number of wall volumes must be a factor of the number of fluid volumes!");

  elseif no_wallVolumes > no_fluidVolumes then
    assert(rem(no_wallVolumes, no_fluidVolumes) == 0,
      "Number of wall volumes must be a factor of the number of fluid volumes!");

  end if;

  //
  // Connection of fluid ports
  //
  connect(port_a, heatExchangerTubes.fp_a) annotation (Line(points={{-100,0},{-80,
          0},{-80,-50},{40,-50}}, color={0,0,0}));
  connect(heatExchangerTubes.fp_b, port_b) annotation (Line(points={{60,-50},{80,
          -50},{80,0},{100,0}}, color={0,0,0}));
  connect(liquidPort, phaseSeparatorVolume.cfp_xMinus) annotation (Line(
      points={{0,-38},{0,-20},{-1.8,-20},{-1.8,-4.2}},
      color={0,140,72},
      thickness=1));
  connect(vaporPort, phaseSeparatorVolume.cfp_xPlus) annotation (Line(
      points={{0,42},{0,20},{-1.8,20},{-1.8,7.8}},
      color={0,140,72},
      thickness=1));

  //
  // Connection of heat ports
  //
  connect(heatExchangerTubes.hp_wall,
    heatTransfer_HeatExchangerToPhaseSeparator.hp_a) annotation (Line(
      points={{50,-46},{50,-28}},
      color={238,46,47},
      thickness=1));
  connect(heatTransfer_HeatExchangerToPhaseSeparator.hp_b[1],
    phaseSeparatorVolume.hp_yMinus) annotation (Line(
      points={{50,-12},{50,0},{6,0}},
      color={238,46,47},
      thickness=1));

  if useCasing then
    connect(phaseSeparatorVolume.hp_yPlus, heatTransfer_phaseSeparatorToCasing.hp_a[
      1]) annotation (Line(
        points={{-6,0},{-16,0}},
        color={238,46,47},
        thickness=1));

    if useConductionCasing then
      connect(heatTransfer_phaseSeparatorToCasing.hp_b[1],
        thermalConduction_casing1.hp_a[1]) annotation (Line(
          points={{-32,0},{-36,0},{-36,-40},{-50,-40},{-50,-32}},
          color={238,46,47},
          thickness=1));
      connect(thermalConduction_casing1.hp_b[1], casing.hp_yMinus) annotation (Line(
          points={{-50,-16},{-50,-6}},
          color={238,46,47},
          thickness=1));
      connect(casing.hp_yPlus, thermalConduction_casing2.hp_a[1]) annotation (Line(
          points={{-50,6},{-50,16}},
          color={238,46,47},
          thickness=1));
      connect(thermalConduction_casing2.hp_b[1], hp_casing) annotation (Line(
          points={{-50,32},{-50,40}},
          color={238,46,47},
          thickness=1));

    else
      connect(heatTransfer_phaseSeparatorToCasing.hp_b[1], hp_casing);

    end if;
  end if;

  //
  // Annotations
  //
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},
            {100,100}}),                                        graphics={
        Text(
          extent={{-60,30},{-40,10}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={255,255,255},
          fillPattern=FillPattern.None,
          fontSize=20,
          textString="1"),
        Text(
          extent={{40,30},{60,10}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={255,255,255},
          fillPattern=FillPattern.None,
          fontSize=20,
          textString="n"),
        Line(
          points={{0,40},{0,22},{-2,14},{2,0},{-2,-14},{2,-24},{0,-40}},
          color={0,0,0},
          smooth=Smooth.Bezier),
        Rectangle(
          extent={{-100,-2},{100,-40}},
          lineColor={28,108,200},
          lineThickness=0.5,
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{-100,-10},{-80,8},{-54,-2},{-36,0},{-6,6},{14,-2},{42,4},{74,
              4},{94,-6},{-100,-10}},
          lineColor={28,108,200},
          lineThickness=0.5,
          smooth=Smooth.Bezier,
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-100,40},{100,-40}},
          lineColor={0,0,0},
          lineThickness=1),
        Polygon(
          points={{-20,4},{-20,4},{-32,18},{-8,18},{-20,4}},
          lineColor={0,0,0},
          lineThickness=0.5),
        Ellipse(
          extent={{-66,38},{-78,26}},
          lineColor={28,108,200},
          lineThickness=0.5,
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-28,38},{-40,26}},
          lineColor={28,108,200},
          lineThickness=0.5,
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-12,38},{-24,26}},
          lineColor={28,108,200},
          lineThickness=0.5,
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{16,22},{4,10}},
          lineColor={28,108,200},
          lineThickness=0.5,
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{28,38},{16,26}},
          lineColor={28,108,200},
          lineThickness=0.5,
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{36,22},{24,10}},
          lineColor={28,108,200},
          lineThickness=0.5,
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-58,24},{-70,12}},
          lineColor={28,108,200},
          lineThickness=0.5,
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{74,38},{62,26}},
          lineColor={28,108,200},
          lineThickness=0.5,
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{48,38},{36,26}},
          lineColor={28,108,200},
          lineThickness=0.5,
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{86,24},{74,12}},
          lineColor={28,108,200},
          lineThickness=0.5,
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-84,34},{-96,22}},
          lineColor={28,108,200},
          lineThickness=0.5,
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid)}),
    Diagram(coordinateSystem(extent={{-100,-100},{100,100}})),
    Documentation(info="<html>
<p>
This partial model is the base model for all condensers and evaporators. It defines 
fundamental parameters, models, and variables required by all condensers and evaporators.
Models that inherit properties from this partial model have to redeclare all partial 
models (i.e., fluid ports, tube, phase separator, casing, and heat transfer). Morover, 
the geometry of the redeclared heat trasnfer must be correctly set using the geometry 
record. In addition, the inputs 'fluidProperties' of the conductive heat transfer models 
must be correctly set using properties of the casing.
</p>
</html>", revisions="<html>
<ul>
  <li>
  March 4, 2024, by Mirko Engelpracht:<br/>
  Major adaptations due to restructering of the library and documentation.
  </li>
  <li>
  January 14, 2021, by Mirko Engelpracht:<br/>
  First implementation, including major revisions, after restructuring of the library.
  </li>
</ul>
</html>"));
end PartialSimpleCondenserEvaporator;
