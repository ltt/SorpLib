within SorpLib.Components.HeatExchanger.BaseClasses;
partial model PartialHeatExchanger
  "Base model for all heat exchanger"

  //
  // Definition of parameters regarding the medium
  //
  parameter Integer no_components = 1
    "Number of components"
    annotation (Dialog(tab="General", group="Medium"),
                Evaluate=true,
                HideResult=true);

  //
  // Definition of parameters regarding start values
  //
  parameter Modelica.Units.SI.Pressure p_initial = 1e5
    "Initial value of pressure"
    annotation (Dialog(tab="Initialisation", group="Initial Values"));
  parameter Modelica.Units.SI.MassFraction[no_components] X_i_initial=
    fill(1/no_components, no_components)
    "Initial values of mass fractions"
    annotation (Dialog(tab="Initialisation", group="Initial Values"));

  parameter Modelica.Units.SI.MassFlowRate m_flow_start = 1e-4
    "Start value for mass flow rate"
    annotation (Dialog(tab="Initialisation", group="Start Values"));

  //
  // Definition of advanced parameters
  //
  parameter SorpLib.Choices.BalanceEquations type_overallMassBalance=
    SorpLib.Choices.BalanceEquations.TransientFixedInitial
    "Handling of the overall mass balance and corresponding initialisation"
    annotation (Dialog(tab = "Advanced", group = "Conservation Equations"),
                Evaluate=true,
                HideResult=true);
  parameter SorpLib.Choices.BalanceEquations type_independentMassBalances=
    type_overallMassBalance
    "Handling of independent mass balances and corresponding initialisations"
    annotation (Dialog(tab = "Advanced", group = "Conservation Equations"),
                Evaluate=true,
                HideResult=true);
  parameter SorpLib.Choices.BalanceEquations type_energyBalance=
    SorpLib.Choices.BalanceEquations.TransientFixedInitial
    "Handling of the energy balance and corresponding initialisation"
    annotation (Dialog(tab = "Advanced", group = "Conservation Equations"),
                Evaluate=true,
                HideResult = true);

  parameter Boolean avoid_events = false
    "= true, if events are avoid by using noEvent()-operator"
    annotation (Dialog(tab = "Advanced", group = "Numerics"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult = true);
  parameter Modelica.Units.SI.MassFlowRate m_flow_small = 1e-4
    "Regularization mass flow rate"
    annotation (Dialog(tab="Advanced", group="Numerics"));
  parameter Integer noDiff = 2
    "Specification how often transition functions can be differentiated"
    annotation(Dialog(tab = "Advanced", group = "Numerics"),
              Evaluate=true,
              HideResult=true);

  // Definition of ports
  //
  replaceable SorpLib.Basics.Interfaces.BaseClasses.PartialFluidPort port_a
    constrainedby Basics.Interfaces.BaseClasses.PartialFluidPort(
      final no_components=no_components,
      m_flow(start=m_flow_start))
    "Fluid port a"
    annotation (Placement(transformation(extent={{-110,-10},{-90,10}}),
                iconTransformation(extent={{-110,-10},{-90,10}})),
                choicesAllMatching=true);

  replaceable SorpLib.Basics.Interfaces.BaseClasses.PartialFluidPort port_b
    constrainedby Basics.Interfaces.BaseClasses.PartialFluidPort(
      final no_components=no_components,
      m_flow(start=-m_flow_start))
    "Fluid port b"
    annotation (Placement(transformation(extent={{90,-10},{110,10}}),
                iconTransformation(extent={{90,-10},{110,10}})),
                choicesAllMatching=true);

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial model is the base model for all heat exchangers. It defines some
fundamental parameters required by all heat exchangers. Models that inherit properties 
from this partial model have to redeclare the fluid ports. Moreover, the mass
and energy balances must be completed using the ports.
<br/><br/>
The following variables must be specified in the model that inherit properties:
</p>
<ul>
  <li>
  Pressure <i>port_a.p</i> at port a.
  </li>
  <li>
  Pressure <i>port_a.p</i> at port b.
  </li>
  <br/>
  <li>
  Mass flow rate <i>port_a.m_flow</i> at port a.
  </li>
  <li>
  Mass flow rate <i>port_a.m_flow</i> at port b.
  </li>
  <br/>
  <li>
  Outflowing specific enthalpy <i>port_a.h_outflow</i> at port a.
  </li>
  <li>
  Outflowing specific enthalpy <i>port_b.h_outflow</i> at port b.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  January 26, 2024, by Mirko Engelpracht:<br/>
  Minor adaptations and documentation.
  </li>
  <li>
  January 14, 2021, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end PartialHeatExchanger;
