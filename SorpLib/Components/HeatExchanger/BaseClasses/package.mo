within SorpLib.Components.HeatExchanger;
package BaseClasses "Base models and functions for all heat exchangers"
  extends Modelica.Icons.BasesPackage;

  annotation (Documentation(info="<html>
<p>
This package contains partial heat exchanger models, containing fundamental 
definitions for heat exchangers. The content of this package is only of interest 
when adding new heat exchangers to the library. 
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end BaseClasses;
