within SorpLib.Components.HeatExchanger.BaseClasses;
partial model PartialSimpleDryCooler
  "Base model for all simple dry coolers"
  extends SorpLib.Components.HeatExchanger.BaseClasses.PartialHeatExchanger(
    final type_energyBalance=SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial,
    final type_independentMassBalances=SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial,
    final type_overallMassBalance=SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial);

  //
  // Definition of general parameters
  //
  parameter Modelica.Units.SI.Pressure p_air = 1.01325e5
    "Ambient pressure (i.e., used to calculate air properties)"
    annotation (Dialog(tab="General", group="Ambient Conditions"));
  parameter Modelica.Units.SI.Temperature T_air_in = 273.15 + 25
    "Ambient temperature (i.e., used to calculate air properties)"
    annotation (Dialog(tab="General", group="Ambient Conditions"));

  parameter Integer flowDirection = 1
    "Definition of flow direction of liquid stream: Relevant for energy balance"
    annotation (Dialog(tab="General", group="Recooler Specifications"),
                choices(choice=1 "A_to_B",
                        choice=2 "B_to_A",
                        choice=3 "Actual flow direction"),
                Evaluate=true,
                HideResult=true);
  replaceable parameter SorpLib.Components.HeatExchanger.Records.ParametrizationDryCooler
    typeDryRecooler
    constrainedby
    SorpLib.Components.HeatExchanger.Records.ParametrizationDryCooler
    "Paremetrization record of recooler"
    annotation (Dialog(tab="General", group="Recooler Specifications"),
                Evaluate=true,
                choicesAllMatching=true);
  parameter Real scaleFactor = 1
    "Scaling factor for recooler: In reality, only integers are sound since
    they correspond to the number of fans"
    annotation (Dialog(tab="General", group="Recooler Specifications"));

  //
  // Definition of scaled specification parameters
  //
  final parameter Modelica.Units.SI.MassFlowRate m_flow_liq_nom=
    typeDryRecooler.m_flow_liq_nom * scaleFactor
    "Scaled nominal mass flow rate of liquid"
    annotation (HideResult=true);
  final parameter Modelica.Units.SI.VolumeFlowRate V_flow_air_max=
    typeDryRecooler.V_flow_air_max * scaleFactor
    "Scaled maximal volume flow rate of air"
    annotation (HideResult=true);
  final parameter Modelica.Units.SI.Power  P_el_fan_max=
    typeDryRecooler.P_el_fan_max * scaleFactor
    "Scaled maximal electrical power consumption of fan unit"
    annotation (HideResult=true);

  //
  // Definition of connectors
  //
  Modelica.Blocks.Interfaces.RealInput relativeFanSpeed
    "Input defining relative volume flow rate of air"
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={0,-60}), iconTransformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={0,-60})));

  //
  // Definition of variables
  //
  Modelica.Units.SI.ThermalConductance UA_internal
    "Internal effective heat trasfer coefficient (i.e., liquid side)";
  Modelica.Units.SI.ThermalConductance UA_external
    "External effective heat trasfer coefficient (i.e., air side)";

  Real C_air(unit="W/K")
    "Heat capacity rate of air";
  Real C_liquid(unit="W/K")
    "Heat capacity rate of liquid";
  Real C_min(unit="W/K") = min(C_air, C_liquid)
    "Minimum heat capacity rate";
  Real C_max(unit="W/K") = max(C_air, C_liquid)
    "Maximum heat capacity rate";
  Real C_ratio(unit="1") = C_min/C_max
    "Ratio between minimum and maximum heat capacity rates";

  Real NTU(unit="1")
    "Number of transfer units";
  Real efficiency(unit="1")
    "Efficiency of dry cooler";

  Modelica.Units.SI.HeatFlowRate Q_flow_max
    "Maximum heat flow rate that could be transferred";
  Modelica.Units.SI.HeatFlowRate Q_flow
    "Actual heat flow rate that is transferred from liquid to air";

  Modelica.Units.SI.MassFlowRate m_flow_air
    "Mass flow rate of air";
  Modelica.Units.SI.VolumeFlowRate V_flow_air
    "Volume flow rate of air";

  Modelica.Units.SI.PressureDifference dp_liq
    "Pressure drop at liquid side";

  Modelica.Units.SI.Power P_el_fans
    "Total electrical power consumption of fan unit";

  //
  // Definition of protected variables
  //
protected
  Modelica.Units.SI.Temperature T_liq_in
    "Inlet temperature of liquid";
  Modelica.Units.SI.SpecificHeatCapacity cp_liq_in
    "Specific heat capacity of liquid at inlet";
  Modelica.Units.SI.DynamicViscosity eta_liq_in
    "Dynamic viscosity of liquid at inlet";

  Modelica.Units.SI.Temperature T_air_out
    "Outlet temperature of air";
  Modelica.Units.SI.Density d_air_in
    "Density of air at inlet";
  Modelica.Units.SI.SpecificHeatCapacity cp_air_in
    "Specific heat capacity of air at inlet";

equation
  //
  // Momentum balance
  //
  dp_liq = port_a.p - port_b.p
    "Pressure drop of liquid";

  //
  // Mass balance
  //
  0 = port_a.m_flow + port_b.m_flow
    "Steady-state mass balance of liquid";

  port_a.Xi_outflow = inStream(port_b.Xi_outflow)
    "Stream variable: Trivial equation since no change of mass fractions";
  port_b.Xi_outflow = inStream(port_a.Xi_outflow)
    "Stream variable: Trivial equation since no change of mass fractions";

  //
  // Energy balance
  //
  port_a.h_outflow = inStream(port_b.h_outflow) - Q_flow / port_b.m_flow
    "Steady-state energy balance depends on the flow direction";
  port_b.h_outflow = inStream(port_a.h_outflow) - Q_flow / port_a.m_flow
    "Steady-state energy balance depends on the flow direction";

  T_air_out = T_air_in + Q_flow/scaleFactor / C_air
    "Outlet temperature of air: Rescaling (see calculation of C_air)";

  //
  // NTU-method for heat exchangers
  //
  V_flow_air = V_flow_air_max * relativeFanSpeed
    "Calculate actual volume flow rate of air (i.e., corresponds to load of dry
     cooler)";
  m_flow_air = V_flow_air * d_air_in
    "Mass flow rate of air";

  UA_internal = 2 * typeDryRecooler.delta *
    (((abs(port_a.m_flow) / scaleFactor)^0.8) / (eta_liq_in^0.5))
    "Internal effective heat transfer coefficient: Division by scaling factor
     since mass flow rate is devided equally between fan units";
  UA_external = 0.35 * typeDryRecooler.gamma * cp_air_in *
    ((m_flow_air / scaleFactor)^0.8)
    "External effective heat transfer coefficient: Division by scaling factor
     since mass flow rate is devided equally between fan units";

  C_air = m_flow_air/scaleFactor * cp_air_in
    "Heat capacity rate of air: Division by scaling factor
     since mass flow rate is devided equally between fan units";
  C_liquid = abs(port_a.m_flow)/scaleFactor * cp_liq_in
    "Heat capacity rate of liquid: Division by scaling factor
     since mass flow rate is devided equally between fan units";

  NTU = (UA_external * UA_internal / (UA_external + UA_internal)) / C_min
    "Number of transfer units";
  efficiency = (1 - exp(-NTU * (1  -C_ratio))) /
    (1 - C_ratio * exp(-NTU * (1 - C_ratio)))
    "Efficiency of dry cooler: Counter-flow arrangement";

  Q_flow_max = C_min * (T_liq_in-T_air_in) * scaleFactor
    "Maximum heat flow rate that could be transferred theoretically:
     Rescaling (see calculation of C_i)";
  Q_flow = Q_flow_max * efficiency
    "Actual heat flow rate that is transferred";

  //
  // Apply affinity laws for pressure drop and power consumption calculations
  //
  if avoid_events then
    dp_liq = typeDryRecooler.dp_liq_nom * SorpLib.Numerics.regSquare_noEvent(
      x=port_a.m_flow,
      delta_x=max(m_flow_small, 0.005 * m_flow_liq_nom)) /
      m_flow_liq_nom^2
      "Pressure drop of liquid";

  else
    dp_liq = typeDryRecooler.dp_liq_nom * SorpLib.Numerics.regSquare(
      x=port_a.m_flow,
      delta_x=max(m_flow_small, 0.005 * m_flow_liq_nom)) /
      m_flow_liq_nom^2
      "Pressure drop of liquid";

  end if;

  P_el_fans = ((V_flow_air / V_flow_air_max)^3) * P_el_fan_max
    "Total electrical power consumption of fan unit";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial model is the base model for all simple dry coolers. It defines the
required parameters and variables and adds all relevant equations. Models that 
inherit properties from this partial model have to redeclare the fluid ports. Moreover, 
fluid properties at inlets must be added.
<br/><br/>
The following variables must be specified in the model that inherit properties:
</p>
<ul>
  <li>
  Liquid inlet temperature <i>T_liq_in</i>.
  </li>
  <li>
  Specific heat capacity of the liquid at the inlet <i>cp_liq_in</i>.
  </li>
  <li>
  Dynamic viscosity of the liquid at the inlet <i>eta_liq_in</i>.
  </li>
  <br/>
  <li>
  Specific heat capacity of the air at the inlet <i>cp_air_in</i>.
  </li>
  <li>
  Density of the air at the inlet <i>d_air_in</i>.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  January 26, 2024, by Mirko Engelpracht:<br/>
  Minor adaptations and documentation.
  </li>
  <li>
  January 14, 2021, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"), Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Polygon(
          points={{-100,40},{-100,-40},{-60,-60},{60,-60},{100,-40},{100,40},{60,
              60},{-60,60},{-100,40}},
          lineColor={0,0,0},
          lineThickness=0.5,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Line(
          points={{-100,0},{-80,60},{-60,-60},{-40,60},{-20,-60},{0,60},{20,-60},
              {40,60},{60,-60},{80,60},{98,0}},
          color={28,108,200},
          thickness=1,
          smooth=Smooth.Bezier),
        Line(
          points={{0,-10},{0,10}},
          color={244,125,35},
          thickness=0.5,
          smooth=Smooth.Bezier,
          arrow={Arrow.Filled,Arrow.None},
          origin={-60,-46},
          rotation=180),
        Line(
          points={{0,-10},{0,10}},
          color={244,125,35},
          thickness=0.5,
          smooth=Smooth.Bezier,
          arrow={Arrow.Filled,Arrow.None},
          origin={-40,-46},
          rotation=180),
        Line(
          points={{0,-10},{0,10}},
          color={244,125,35},
          thickness=0.5,
          smooth=Smooth.Bezier,
          arrow={Arrow.Filled,Arrow.None},
          origin={-20,-46},
          rotation=180),
        Line(
          points={{0,-10},{0,10}},
          color={244,125,35},
          thickness=0.5,
          smooth=Smooth.Bezier,
          arrow={Arrow.Filled,Arrow.None},
          origin={0,-46},
          rotation=180),
        Line(
          points={{0,-10},{0,10}},
          color={244,125,35},
          thickness=0.5,
          smooth=Smooth.Bezier,
          arrow={Arrow.Filled,Arrow.None},
          origin={20,-46},
          rotation=180),
        Line(
          points={{0,-10},{0,10}},
          color={244,125,35},
          thickness=0.5,
          smooth=Smooth.Bezier,
          arrow={Arrow.Filled,Arrow.None},
          origin={40,-46},
          rotation=180),
        Line(
          points={{0,-10},{0,10}},
          color={244,125,35},
          thickness=0.5,
          smooth=Smooth.Bezier,
          arrow={Arrow.Filled,Arrow.None},
          origin={60,-46},
          rotation=180),
        Line(
          points={{0,-10},{0,10}},
          color={238,46,47},
          thickness=0.5,
          smooth=Smooth.Bezier,
          arrow={Arrow.Filled,Arrow.None},
          origin={-60,46},
          rotation=180),
        Line(
          points={{0,-10},{0,10}},
          color={238,46,47},
          thickness=0.5,
          smooth=Smooth.Bezier,
          arrow={Arrow.Filled,Arrow.None},
          origin={-40,46},
          rotation=180),
        Line(
          points={{0,-10},{0,10}},
          color={238,46,47},
          thickness=0.5,
          smooth=Smooth.Bezier,
          arrow={Arrow.Filled,Arrow.None},
          origin={-20,46},
          rotation=180),
        Line(
          points={{0,-10},{0,10}},
          color={238,46,47},
          thickness=0.5,
          smooth=Smooth.Bezier,
          arrow={Arrow.Filled,Arrow.None},
          origin={0,46},
          rotation=180),
        Line(
          points={{0,-10},{0,10}},
          color={238,46,47},
          thickness=0.5,
          smooth=Smooth.Bezier,
          arrow={Arrow.Filled,Arrow.None},
          origin={20,46},
          rotation=180),
        Line(
          points={{0,-10},{0,10}},
          color={238,46,47},
          thickness=0.5,
          smooth=Smooth.Bezier,
          arrow={Arrow.Filled,Arrow.None},
          origin={40,46},
          rotation=180),
        Line(
          points={{0,-10},{0,10}},
          color={238,46,47},
          thickness=0.5,
          smooth=Smooth.Bezier,
          arrow={Arrow.Filled,Arrow.None},
          origin={60,46},
          rotation=180),
        Ellipse(
          extent={{-60,64},{0,58}},
          lineColor={0,0,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{0,64},{60,58}},
          lineColor={0,0,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid)}));
end PartialSimpleDryCooler;
