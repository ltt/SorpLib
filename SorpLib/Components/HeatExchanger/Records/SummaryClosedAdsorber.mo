within SorpLib.Components.HeatExchanger.Records;
record SummaryClosedAdsorber
  "This record summarizes the most important variables of closed adsorbers"
  extends Modelica.Icons.Record;

  //
  // Variables describing the vapor
  //
  Modelica.Units.SI.Pressure p_vapor
    "Average pressure in vapor volume"
    annotation (Dialog(tab="General", group="Vapor volume"));
  Modelica.Units.SI.Temperature T_vapor
    "Average temperature in vapor volume"
    annotation (Dialog(tab="General", group="Vapor volume"));
  Modelica.Units.SI.Density rho_vapor
    "Average density in vapor volume"
    annotation (Dialog(tab="General", group="Vapor volume"));

  Modelica.Units.SI.Mass mass_vapor
    "Fluid mass in vapor volume"
    annotation (Dialog(tab="General", group="Vapor volume"));

  //
  // Variables describing the sorbent
  //
  Modelica.Units.SI.Pressure p_adsorbate_avg
    "Average adsorabte pressure"
    annotation (Dialog(tab="General", group="Adsorbate"));
  Modelica.Units.SI.Temperature T_adsorbate_avg
    "Average adsorabte temperature"
    annotation (Dialog(tab="General", group="Adsorbate"));
  SorpLib.Units.Uptake x_adsorbate_avg
    "Average adsorabte loading"
    annotation (Dialog(tab="General", group="Adsorbate"));

  Modelica.Units.SI.Mass mass_adsorpt
    "Adsorpt mass"
    annotation (Dialog(tab="General", group="Adsorbate"));

  //
  // Variables describing the heat exchanger
  //
  Modelica.Units.SI.Pressure p_liq_inlet
    "Pressure of liquid at heat exchanger inlet"
    annotation (Dialog(tab="General", group="Heat Exchanger Tubes"));
  Modelica.Units.SI.Pressure p_liq_outlet
    "Pressure of liquid at heat exchanger outlet"
    annotation (Dialog(tab="General", group="Heat Exchanger Tubes"));
  Modelica.Units.SI.Pressure p_liq_avg
    "Average pressure of liquid in heat exchanger"
    annotation (Dialog(tab="General", group="Heat Exchanger Tubes"));
  Modelica.Units.SI.PressureDifference dp_liq = p_liq_inlet - p_liq_outlet
    "Pressure difference of liquid in heat exchanger";

  Modelica.Units.SI.Temperature T_liq_inlet
    "Temperature of liquid at heat exchanger inlet"
    annotation (Dialog(tab="General", group="Heat Exchanger Tubes"));
  Modelica.Units.SI.Temperature T_liq_outlet
    "Temperature of liquid at heat exchanger outlet"
    annotation (Dialog(tab="General", group="Heat Exchanger Tubes"));
  Modelica.Units.SI.Temperature T_liq_avg
    "Average temperature of liquid in heat exchanger"
    annotation (Dialog(tab="General", group="Heat Exchanger Tubes"));

  Modelica.Units.SI.Temperature T_wall_avg
    "Average wall temperature of heat exchanger"
    annotation (Dialog(tab="General", group="Heat Exchanger Tubes"));

  //
  // Variables required for balance equations
  //
  Modelica.Units.SI.MassFlowRate m_flow_liq_inlet
    "Mass flow rate of liquid at inlet of heat exchanger"
    annotation (Dialog(tab="General", group="Balance Equations"));
  Modelica.Units.SI.MassFlowRate m_flow_liq_outlet
    "Mass flow rate of liquid at outlet of heat exchanger"
    annotation (Dialog(tab="General", group="Balance Equations"));

  Modelica.Units.SI.MassFlowRate m_flow_evaporator
    "Mass flow rate from evaporator"
    annotation (Dialog(tab="General", group="Balance Equations"));
  Modelica.Units.SI.MassFlowRate m_flow_condenser
    "Mass flow rate from condenser"
    annotation (Dialog(tab="General", group="Balance Equations"));
  Modelica.Units.SI.MassFlowRate m_flow_massRecovery
    "Mass flow rate from mass recovery ports"
    annotation (Dialog(tab="General", group="Balance Equations"));

  Modelica.Units.SI.HeatFlowRate Q_flow_wallToSorbent
    "Heat flow rate from heat exchanger walls to sorbent"
    annotation (Dialog(tab="General", group="Balance Equations"));
  Modelica.Units.SI.HeatFlowRate Q_flow_fluidWall
    "Heat flow rate from liquid to wall"
    annotation (Dialog(tab="General", group="Balance Equations"));
  Modelica.Units.SI.HeatFlowRate DH_liquid
    "Difference enthalpy flow of liquid in heat exchanger"
    annotation (Dialog(tab="General", group="Balance Equations"));

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This record summarizes the most important variables of closed adsorbers
</p>
</html>", revisions="<html>
<ul>
  <li>
  March 5, 2024, by Mirko Engelpracht:<br/>
  Minor adaptations.
  </li>
  <li>
  January 14, 2024, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end SummaryClosedAdsorber;
