within SorpLib.Components.HeatExchanger.Records;
record ParametrizationDryCooler
  "This record contains parameters describing dry coolers"
  extends Modelica.Icons.Record;

  //
  // Definition of parameters
  //
  parameter Real gamma = 11.52
    "Factor for external heat transfer coefficient"
    annotation (Dialog(tab="Genral", group="Heat Transfer"));
  parameter Real delta = 303.09
    "Factor for internal heat transfer coefficient"
    annotation (Dialog(tab="Genral", group="Heat Transfer"));

  parameter Modelica.Units.SI.MassFlowRate m_flow_liq_nom=
    28.78 / 3600 * 1000
    "Nominal mass flow rate of liquid medium that shall be cooled"
    annotation (Dialog(tab="Genral", group="Nominal Operating Point"));
  parameter Modelica.Units.SI.PressureDifference dp_liq_nom=
    0.77*10^5
    "Nominal pressure drop of liquid medium at nominal mass flow rate"
    annotation (Dialog(tab="Genral", group="Nominal Operating Point"));

  parameter Modelica.Units.SI.VolumeFlowRate V_flow_air_max = 88368/3600
    "Maximal air flow rate"
    annotation (Dialog(tab="Genral", group="Maximal Operating Point"));
  parameter Modelica.Units.SI.Power P_el_fan_max = 16.2*10^3
    "Maximal power consumption of the fan units"
    annotation (Dialog(tab="Genral", group="Maximal Operating Point"));

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This record contains parameters characterizing dry coolers. The default parameters
correspond to the dry cooler GFHC WD 063 from Guenther, fitted by Gibelhaus (2019).
</p>

<h4>References</h4>
<ul>
  <li>
  Gibelhaus, A. and Tangkrachang, T. and Bau, U. and Seiler, J. and Bardow, A. (2019). Integrated design and control of full sorption chiller systems. Energy 185 (2019), pp. 409-422. DOI: https://doi.org/10.1016/j.energy.2019.06.169.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  January 15, 2024, by Mirko Engelpracht:<br/>
  Minor adaptations and documentation.
  </li>
  <li>
  January 14, 2021, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end ParametrizationDryCooler;
