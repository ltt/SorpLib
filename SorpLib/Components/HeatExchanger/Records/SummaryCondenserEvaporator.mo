within SorpLib.Components.HeatExchanger.Records;
record SummaryCondenserEvaporator
  "This record summarizes the most important variables of condensers or evaporators"
  extends Modelica.Icons.Record;

  //
  // Variables describing the phase seperator
  //
  Modelica.Units.SI.Pressure p_VLE
    "Average pressure in phase seperator"
    annotation (Dialog(tab="General", group="Phase Seperator"));
  Modelica.Units.SI.Density rho_VLE
    "Average density in phase seperator"
    annotation (Dialog(tab="General", group="Phase Seperator"));
  Modelica.Units.SI.Temperature T_VLE
    "Average temperature in phase seperator"
    annotation (Dialog(tab="General", group="Phase Seperator"));

  Modelica.Units.SI.Mass mass
    "Fluid mass in phase seperator"
    annotation (Dialog(tab="General", group="Phase Seperator"));

  //
  // Variables describing the heat exchanger
  //
  Modelica.Units.SI.Pressure p_liq_inlet
    "Pressure of liquid at heat exchanger inlet"
    annotation (Dialog(tab="General", group="Heat Exchanger Tubes"));
  Modelica.Units.SI.Pressure p_liq_outlet
    "Pressure of liquid at heat exchanger outlet"
    annotation (Dialog(tab="General", group="Heat Exchanger Tubes"));
  Modelica.Units.SI.Pressure p_liq_avg
    "Average pressure of liquid in heat exchanger"
    annotation (Dialog(tab="General", group="Heat Exchanger Tubes"));
  Modelica.Units.SI.PressureDifference dp_liq = p_liq_inlet - p_liq_outlet
    "Pressure difference of liquid in heat exchanger";

  Modelica.Units.SI.Temperature T_liq_inlet
    "Temperature of liquid at heat exchanger inlet"
    annotation (Dialog(tab="General", group="Heat Exchanger Tubes"));
  Modelica.Units.SI.Temperature T_liq_outlet
    "Temperature of liquid at heat exchanger outlet"
    annotation (Dialog(tab="General", group="Heat Exchanger Tubes"));
  Modelica.Units.SI.Temperature T_liq_avg
    "Average temperature of liquid in heat exchanger"
    annotation (Dialog(tab="General", group="Heat Exchanger Tubes"));

  Modelica.Units.SI.Temperature T_wall_avg
    "Average wall temperature of heat exchanger"
    annotation (Dialog(tab="General", group="Heat Exchanger Tubes"));

  //
  // Variables required for balance equations
  //
  Modelica.Units.SI.MassFlowRate m_flow_liq_inlet
    "Mass flow rate of liquid at inlet of heat exchanger"
    annotation (Dialog(tab="General", group="Balance Equations"));
  Modelica.Units.SI.MassFlowRate m_flow_liq_outlet
    "Mass flow rate of liquid at outlet of heat exchanger"
    annotation (Dialog(tab="General", group="Balance Equations"));
  Modelica.Units.SI.MassFlowRate m_flow_vapor
    "Mass flow rate of vapor from heat exchanger to working pair cells"
    annotation (Dialog(tab="General", group="Balance Equations"));
  Modelica.Units.SI.MassFlowRate m_flow_liquid
    "Mass flow rate of liquid"
    annotation (Dialog(tab="General", group="Balance Equations"));

  Modelica.Units.SI.HeatFlowRate Q_flow_wallToPhaseSeparator
    "Heat flow rate from heat exchanger walls to phase seperator"
    annotation (Dialog(tab="General", group="Balance Equations"));
  Modelica.Units.SI.HeatFlowRate Q_flow_fluidWall
    "Heat flow rate from liquid to wall"
    annotation (Dialog(tab="General", group="Balance Equations"));
  Modelica.Units.SI.HeatFlowRate DH_liquid
    "Difference enthalpy flow of liquid in heat exchanger"
    annotation (Dialog(tab="General", group="Balance Equations"));

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This record summarizes the most important variables of condensers or evaporators.
</p>
</html>", revisions="<html>
<ul>
  <li>
  March 4, 2024, by Mirko Engelpracht:<br/>
  Minor adaptations.
  </li>
  <li>
  January 14, 2024, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end SummaryCondenserEvaporator;
