within SorpLib.Components.HeatExchanger.Records;
record GeometryCondenserEvaporator
  "This record contains the geometry of condensers or evaporators"
  extends Modelica.Icons.Record;

  //
  // Definition of parameters regarding the disretization
  //
  parameter Integer no_fluidVolumes(min=1) = 1
    "Number of fluid volumes"
    annotation (Dialog(tab="General", group="Discretization", enable=false));
  parameter Integer no_wallVolumes(min=1) = 1
    "Number of wall volumes"
    annotation (Dialog(tab="General", group="Discretization", enable=false));

  //
  // Definition of parameters regarding the geometry of the casing
  //
  parameter Modelica.Units.SI.Length l_cas = 1
    "Length of the casing"
    annotation (Dialog(tab="Casing", group="General"));

  parameter Modelica.Units.SI.Diameter d_inner_cas = 0.100
    "Inner diameter of the casing"
    annotation (Dialog(tab="Casing", group="Diameters"));
  parameter Modelica.Units.SI.Diameter d_outer_cas = 0.103
    "Outer diameter of the casing"
    annotation (Dialog(tab="Casing", group="Diameters"));
  parameter Modelica.Units.SI.Diameter d_hydInner_cas = d_inner_cas
    "Hydraulic inner diameter of the casing"
    annotation (Dialog(tab="Casing", group="Diameters"));
  parameter Modelica.Units.SI.Diameter d_hydOuter_cas = d_outer_cas
    "Hydraulic outer diameter of the casing"
    annotation (Dialog(tab="Casing", group="Diameters"));

  parameter Modelica.Units.SI.Thickness t_wall_cas=
    (d_outer_cas - d_inner_cas) / 2
    "Wall thickness of the casing"
    annotation (Dialog(tab="Casing", group="Diameters"));

  parameter Modelica.Units.SI.Area A_crossInner_cas=
    Modelica.Constants.pi/4 * d_inner_cas^2
    "Inner cross-sectional area of the casing"
    annotation (Dialog(tab="Casing", group="Areas"));
  parameter Modelica.Units.SI.Area A_crossOuter_cas=
    Modelica.Constants.pi/4 * d_outer_cas^2
    "Outer cross-sectional area of the casing"
    annotation (Dialog(tab="Casing", group="Areas"));
  parameter Modelica.Units.SI.Area A_crossWall_cas=
    A_crossOuter_cas - A_crossInner_cas
    "Wall cross-sectional area of the casing"
    annotation (Dialog(tab="Casing", group="Areas"));
  parameter Modelica.Units.SI.Area A_hydCrossInner_cas=
    Modelica.Constants.pi/4 * d_hydInner_cas^2
    "Hydraulic inner cross-sectional area of the casing"
    annotation (Dialog(tab="Casing", group="Areas"));
  parameter Modelica.Units.SI.Area A_hydCrossOuter_cas=
    Modelica.Constants.pi/4 * d_hydOuter_cas^2
    "Hydraulic outer cross-sectional area of the casing"
    annotation (Dialog(tab="Casing", group="Areas"));
  parameter Modelica.Units.SI.Area A_hydCrossWall_cas=
    A_hydCrossOuter_cas - A_hydCrossInner_cas
    "Hydaulic wall cross-sectional area of the casing"
    annotation (Dialog(tab="Casing", group="Areas"));
  parameter Modelica.Units.SI.Area A_heatTransferInner_cas=
    Modelica.Constants.pi * d_inner_cas * l_cas
    "Inner heat transfer area of the casing"
    annotation (Dialog(tab="Casing", group="Areas"));
  parameter Modelica.Units.SI.Area A_heatTransferOuter_cas=
    Modelica.Constants.pi * d_outer_cas * l_cas
    "Outer heat transfer area of the casing"
    annotation (Dialog(tab="Casing", group="Areas"));

  parameter Modelica.Units.SI.Volume V_inner_cas=
    Modelica.Constants.pi/4 * d_inner_cas^2 * l_cas
    "Inner volume of the casing"
    annotation (Dialog(tab="Casing", group="Volumes"));
  parameter Modelica.Units.SI.Volume V_outer_cas=
    Modelica.Constants.pi/4 * d_outer_cas^2 * l_cas
    "Outer volume of the casing"
    annotation (Dialog(tab="Casing", group="Volumes"));
  parameter Modelica.Units.SI.Volume V_wall_cas=
    V_outer_cas - V_inner_cas
    "Wall volume of the casing"
    annotation (Dialog(tab="Casing", group="Volumes"));

  //
  // Definition of parameters regarding the the heat exchanger
  //
  parameter Integer no_hydraulicParallelTubes(min=1) = 1
    "Number of hydraulically parallel tubes"
    annotation (Dialog(tab="Heat Exchanger", group="General"));
  parameter Modelica.Units.SI.Length l_hx = 1
    "Length of the tube"
    annotation (Dialog(tab="Heat Exchanger", group="General"));
  parameter Modelica.Units.SI.Length roughness_hx = 7.5e-7
    "Absolute roughness of the tube"
    annotation (Dialog(tab="Heat Exchanger", group="General"));

  parameter Modelica.Units.SI.Diameter d_inner_hx = 0.01
    "Inner diameter of the tube"
    annotation (Dialog(tab="Heat Exchanger", group="Diameters"));
  parameter Modelica.Units.SI.Diameter d_outer_hx = 0.012
    "Outer diameter of the tube"
    annotation (Dialog(tab="Heat Exchanger", group="Diameters"));
  parameter Modelica.Units.SI.Diameter d_hydInner_hx = d_inner_hx
    "Hydraulic inner diameter of the tube"
    annotation (Dialog(tab="Heat Exchanger", group="Diameters"));
  parameter Modelica.Units.SI.Diameter d_hydOuter_hx = d_outer_hx
    "Hydraulic outer diameter of the tube"
    annotation (Dialog(tab="Heat Exchanger", group="Diameters"));
  parameter Modelica.Units.SI.Thickness t_wall_hx=
    (d_outer_hx - d_inner_hx) / 2
    "Wall thickness of the tube"
    annotation (Dialog(tab="Heat Exchanger", group="Diameters"));

  parameter Modelica.Units.SI.Area A_crossInner_hx=
    Modelica.Constants.pi/4 * d_inner_hx^2
    "Inner cross-sectional area of the tube"
    annotation (Dialog(tab="Heat Exchanger", group="Areas"));
  parameter Modelica.Units.SI.Area A_crossOuter_hx=
    Modelica.Constants.pi/4 * d_outer_hx^2
    "Outer cross-sectional area of the tube"
    annotation (Dialog(tab="Heat Exchanger", group="Areas"));
  parameter Modelica.Units.SI.Area A_crossWall_hx=
    A_crossOuter_hx - A_crossInner_hx
    "Wall cross-sectional area of the tube"
    annotation (Dialog(tab="Heat Exchanger", group="Areas"));
  parameter Modelica.Units.SI.Area A_hydCrossInner_hx = A_crossInner_hx
    "Hydraulic inner cross-sectional area of the tube"
    annotation (Dialog(tab="Heat Exchanger", group="Areas"));
  parameter Modelica.Units.SI.Area A_hydCrossOuter_hx = A_crossOuter_hx
    "Hydraulic outer cross-sectional area of the tube"
    annotation (Dialog(tab="Heat Exchanger", group="Areas"));
  parameter Modelica.Units.SI.Area A_hydCrossWall_hx=
    A_hydCrossOuter_hx - A_hydCrossInner_hx
    "Hydraulic wall cross-sectional area of the tube"
    annotation (Dialog(tab="Heat Exchanger", group="Areas"));
  parameter Modelica.Units.SI.Area A_heatTransferInner_hx=
    Modelica.Constants.pi * d_inner_hx * l_hx
    "Total inner heat transfer area of the tube"
    annotation (Dialog(tab="Heat Exchanger", group="Areas"));
  parameter Modelica.Units.SI.Area A_heatTransferOuter_hx=
    Modelica.Constants.pi * d_outer_hx * l_hx
    "Total outer heat transfer area of the tube"
    annotation (Dialog(tab="Heat Exchanger", group="Areas"));
  parameter Real f_finAreaRatioInner_hx(min=0, max=1) = 0
    "Ratio of total inner fin area to total inner heat transfer area of the tube"
    annotation (Dialog(tab="Heat Exchanger", group="Areas"));
  parameter Real f_finAreaRatioOuter_hx(min=0, max=1) = 0
    "Ratio of total outer fin area to total outer heat transfer area of the tube"
    annotation (Dialog(tab="Heat Exchanger", group="Areas"));

  parameter Modelica.Units.SI.Volume V_inner_hx = A_crossInner_hx * l_hx
    "Total inner volume of the tube"
    annotation (Dialog(tab="Heat Exchanger", group="Volumes"));
  parameter Modelica.Units.SI.Volume V_outer_hx = A_crossOuter_hx * l_hx
    "Total outer volume of the tube"
    annotation (Dialog(tab="Heat Exchanger", group="Volumes"));
  parameter Modelica.Units.SI.Volume V_wall_hx = V_outer_hx - V_inner_hx
    "Total wall volume of the tube"
    annotation (Dialog(tab="Heat Exchanger", group="Volumes"));
  parameter Real f_finVolumeRatioInner_hx(min=0, max=1) = 0
    "Ratio of total inner fin volume to total wall volume of the tube"
    annotation (Dialog(tab="Heat Exchanger", group="Volumes"));
  parameter Real f_finVolumeRatioOuter_hx(min=0, max=1) = 0
    "Ratio of total outer fin volume to total wall volume of the tube"
    annotation (Dialog(tab="Heat Exchanger", group="Volumes"));

  //
  // Definition of parameters regarding the phase separator
  //
  parameter Modelica.Units.SI.Area A_refrigerant=0.05
    "Base area of the phase seprator"
    annotation (Dialog(tab="Phase Seperator", group="Areas"));
  parameter Modelica.Units.SI.Volume V_refrigerant=
    V_inner_cas - V_outer_hx
    "Total volume of the phase seperator"
    annotation (Dialog(tab="Phase Seperator", group="Volumes"));

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This record contains geometric parameters describing condensers or evaporators.
</p>
</html>", revisions="<html>
<ul>
  <li>
  March 4, 2024, by Mirko Engelpracht:<br/>
  Minor adaptations.
  </li>
  <li>
  January 14, 2024, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end GeometryCondenserEvaporator;
