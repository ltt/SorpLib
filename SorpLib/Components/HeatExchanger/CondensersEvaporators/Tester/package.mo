within SorpLib.Components.HeatExchanger.CondensersEvaporators;
package Tester "Models to test and varify condensers and evaporators"
  extends Modelica.Icons.ExamplesPackage;



  annotation (Documentation(info="<html>
<p>
This package contains executable test models for all condensres and evaporators. 
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Tester;
