within SorpLib.Components.HeatExchanger.CondensersEvaporators.Tester;
model Test_SimpleEvaporator "Tester for the simple evaporator"
  extends Modelica.Icons.Example;

  //
  // Definition of parameters regarding the medium
  //
  replaceable package Medium = Modelica.Media.Water.ConstantPropertyLiquidWater
    constrainedby Modelica.Media.Interfaces.PartialMedium
    "Medium model of the (ideal) liquid"
    annotation (Dialog(tab="General", group="Medium"),
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

  //
  // Definition of boundary models
  //
  SorpLib.Basics.Sources.Fluids.LiquidSource fs_a(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.VolumeFlowRate,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    use_mFlowInput=false,
    use_VFlowInput=true,
    V_flow_fixed=-10/60/1000,
    T_fixed=298.15,
    redeclare final package Medium = Medium)
    "Fluid source a"
    annotation (Placement(transformation(extent={{-70,-10},{-50,10}})));
  SorpLib.Basics.Sources.Fluids.LiquidSource fs_b(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    use_pInput=true,
    T_fixed=358.15,
    redeclare final package Medium = Medium)
    "Fluid source b"
    annotation (Placement(transformation(extent={{70,-10},{50,10}})));

  SorpLib.Basics.Sources.Fluids.VLESource vaporSource(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    use_mFlowInput=true,
    T_fixed=288.15) "Vapor Source" annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={0,30})));

  //
  // Definition of models
  //
  SorpLib.Components.HeatExchanger.CondensersEvaporators.SimpleEvaporator evaporator(
    geometry(d_inner_cas=0.05, d_outer_cas=0.055),
    redeclare model HX_FluidThermalConvectionTubeInside =
        HeatTransfer.HeatTransferCoefficientCorrelations.TubeInside.GnielinskiDittusBoelter,
    redeclare model HX_PressureDrop =
        Fittings.PressureLossCorrelations.TubeInside.PrandtlKarman,
    redeclare package MediumHX = Medium,
    redeclare model PS_HeatExchanger =
        SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.PoolBoiling.LinearAlphaA_fRel,
    nPortsVapor=1) "Evaporator model"
    annotation (Placement(transformation(extent={{-10,-10},{10,10}})));

  //
  // Definition of input signals
  //
protected
  Modelica.Blocks.Sources.Sine input_V_flow(
    amplitude=5/1000/60,
    f=1/500,
    offset=-10/1000/60) "Input signal for volume flow rate"
    annotation (Placement(transformation(extent={{-100,-10},{-80,10}})));
  Modelica.Blocks.Sources.Ramp input_p(
    height=1e5,
    duration=2500,
    offset=2e5) "Input signal for pressure"
    annotation (Placement(transformation(extent={{100,-10},{80,10}})));
  Modelica.Blocks.Sources.Ramp input_TWall(
    height=0.001,
    duration=2000,
    offset=0,
    startTime=500)      "Input signal for wall temperature boundary"
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={0,60})));

equation
  //
  // Connections
  //
  connect(fs_a.port, evaporator.port_a) annotation (Line(
      points={{-60,0},{-10,0}},
      color={28,108,200},
      thickness=1));
  connect(evaporator.port_b, fs_b.port) annotation (Line(
      points={{10,0},{60,0}},
      color={28,108,200},
      thickness=1));
  connect(vaporSource.port, evaporator.vaporPort[1]) annotation (Line(
      points={{0,30},{0,4.2}},
      color={0,140,72},
      thickness=1));

  connect(input_V_flow.y, fs_a.V_flow_input) annotation (Line(points={{-79,0},{-70,
          0},{-70,2},{-61.2,2}}, color={0,0,127}));
  connect(input_p.y, fs_b.p_input)
    annotation (Line(points={{79,0},{70,0},{70,5},{61.2,5}}, color={0,0,127}));
  connect(input_TWall.y, vaporSource.m_flow_input) annotation (Line(points={{0,49},
          {0,40},{-2,40},{-2,31.2}}, color={0,0,127}));

  //
  // Annotations
  //
  annotation (experiment(
      StopTime=2500), Documentation(info="<html>
<p>
This model checks the simple evaporator.
<br/><br/>
To see the model behavior, plot the variables of the models over the 
time. The simulation time is correctly preset (Start: 0 s, Stop = 2500 s). 
</p>
</html>", revisions="<html>
<ul>
  <li>
  March 4, 2024, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Test_SimpleEvaporator;
