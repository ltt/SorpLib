within SorpLib.Components.HeatExchanger.CondensersEvaporators;
model SimpleEvaporator "Simple evaporator for a pure component"
  extends
    SorpLib.Components.HeatExchanger.BaseClasses.PartialSimpleCondenserEvaporator(
    redeclare final SorpLib.Basics.Interfaces.FluidPorts.LiquidPort_in port_a,
    redeclare final SorpLib.Basics.Interfaces.FluidPorts.LiquidPort_out port_b,
    final no_components=MediumHX.nX,
    final no_VLEcomponents=MediumPS.nX,
    redeclare SorpLib.Components.Tubes.LiquidTube heatExchangerTubes(
      redeclare final package Medium = MediumHX,
      redeclare model WallMaterial = WallMaterialHX),
    redeclare SorpLib.Basics.Volumes.PhaseSeparatorVolumes.PhaseSeparatorVolume
      phaseSeparatorVolume(
        redeclare final package Medium = MediumPS),
    redeclare SorpLib.Basics.Volumes.SolidVolumes.SolidVolume casing(
      redeclare final WallMaterialCS solidMedium,
      redeclare final SorpLib.Basics.Volumes.Records.VolumeGeometry geometry(
        dx=0,
        dy=0,
        dz=0,
        A_xy=0,
        A_xz=0,
        A_yz=0,
        V=geometry.V_wall_cas),
      final T_initial=T_casingInitial,
      final p=phaseSeparatorVolume.p,
      independentStateVariables=SorpLib.Choices.IndependentVariablesVolume.pTX),
    redeclare SorpLib.Components.HeatTransfer.PoolBoilingHeatTransfer
      heatTransfer_HeatExchangerToPhaseSeparator(
      redeclare final model HeatTransferCoefficient = PS_HeatExchanger,
      final fluidProperties=SorpLib.Components.HeatTransfer.Records.FluidProperties(
        p=phaseSeparatorVolume.p,
        T=phaseSeparatorVolume.T,
        rho=phaseSeparatorVolume.rho,
        cp=phaseSeparatorVolume.phaseSepratorProperties.cp,
        eta=phaseSeparatorVolume.phaseSepratorProperties.eta,
        lambda=phaseSeparatorVolume.phaseSepratorProperties.lambda),
      final f_relativeFillingLevel=phaseSeparatorVolume.l_liq_rel),
    thermalConduction_casing1(final fluidProperties=
      SorpLib.Components.HeatTransfer.Records.FluidProperties(
        p=casing.solidProperties.p,
        T=casing.solidProperties.T,
        rho=1/casing.solidProperties.v,
        cp=casing.solidProperties.c,
        eta=0,
        lambda=casing.solidProperties.lambda)),
    thermalConduction_casing2(final fluidProperties=
      SorpLib.Components.HeatTransfer.Records.FluidProperties(
        p=casing.solidProperties.p,
        T=casing.solidProperties.T,
        rho=1/casing.solidProperties.v,
        cp=casing.solidProperties.c,
        eta=0,
        lambda=casing.solidProperties.lambda)));

  //
  // Definition of parameters
  //
  replaceable package MediumHX =
      Modelica.Media.Water.ConstantPropertyLiquidWater
    constrainedby Modelica.Media.Interfaces.PartialMedium
    "Medium model of the (ideal) liquid in the heat exchanger"
    annotation (Dialog(tab="General", group="Medium"),
                choicesAllMatching = true,
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);
  replaceable package MediumPS = Modelica.Media.Water.StandardWater
    constrainedby Modelica.Media.Interfaces.PartialMedium
    "Medium model of the real fluid in the phase separator"
    annotation (Dialog(tab="General", group="Medium"),
                choicesAllMatching = true,
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

  replaceable model WallMaterialHX =
      SorpLib.Media.Solids.MetalsAndMetalAlloys.Copper
    constrainedby SorpLib.Media.Solids.BaseClasses.PartialSolid
    "Heat exchanger wall medium"
    annotation (Dialog(tab="General", group="Medium"),
                choicesAllMatching = true,
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);
  replaceable model WallMaterialCS =
      Media.Solids.MetalsAndMetalAlloys.StainlessSteel_X5CrNi18_10
    constrainedby SorpLib.Media.Solids.BaseClasses.PartialSolid
    "Casing wall medium"
    annotation (Dialog(tab="General", group="Medium"),
                choicesAllMatching = true,
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

  //
  // Definition of tranport phenomena
  //
  replaceable model PS_HeatExchanger =
    HeatTransfer.HeatTransferCoefficientCorrelations.PoolBoiling.LinearAlphaA_fRel
    constrainedby
    SorpLib.Components.HeatTransfer.BaseClasses.PartialPoolBoilingHeatTransferCoefficient
    "Heat transfer correlation describing the boiling at the outside of the
    heat exchanger tubes"
    annotation (Dialog(tab="Transport Phenomena", group="Thermal Convection"),
                choicesAllMatching = true,
                Evaluate=true,
                HideResult=true);

  //
  // Definition of variables
  //
  SorpLib.Components.HeatExchanger.Records.SummaryCondenserEvaporator summary(
    final p_VLE=phaseSeparatorVolume.p,
    final rho_VLE=phaseSeparatorVolume.rho,
    final T_VLE=phaseSeparatorVolume.T,
    final mass=phaseSeparatorVolume.m,
    final p_liq_inlet=heatExchangerTubes.state_a.p,
    final p_liq_outlet=heatExchangerTubes.state_b.p,
    final p_liq_avg=sum(heatExchangerTubes.fluidVolumes.fluidProperties.p)/
        heatExchangerTubes.no_fluidVolumes,
    final T_liq_inlet=heatExchangerTubes.state_a.T,
    final T_liq_outlet=heatExchangerTubes.state_b.T,
    final T_liq_avg=sum(heatExchangerTubes.fluidVolumes.fluidProperties.T)/
        heatExchangerTubes.no_fluidVolumes,
    final T_wall_avg=sum(heatExchangerTubes.wallVolumes.T)/
      heatExchangerTubes.no_wallVolumes,
    final m_flow_liq_inlet=port_a.m_flow,
    final m_flow_liq_outlet=port_b.m_flow,
    final m_flow_vapor=sum(vaporPort.m_flow),
    final m_flow_liquid=sum(liquidPort.m_flow),
    final Q_flow_wallToPhaseSeparator=heatExchangerTubes.Q_flow_wallHP,
    final Q_flow_fluidWall=heatExchangerTubes.Q_flow_fluidWall,
    final DH_liquid=heatExchangerTubes.DH_flow)
    "Summary record";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
TO BE ADDED
</p>
</html>", revisions="<html>
<ul>
  <li>
  March 4, 2024, by Mirko Engelpracht:<br/>
  Minor adaptations due to restructering of the library.
  </li>
  <li>
  January 14, 2021, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end SimpleEvaporator;
