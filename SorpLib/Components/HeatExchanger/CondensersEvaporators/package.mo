within SorpLib.Components.HeatExchanger;
package CondensersEvaporators "Condensers and evaporators used in closed sorption systems"
  extends SorpLib.Icons.CondensersEvaporatorsPackage;

  annotation (Documentation(info="<html>
<p>
This package contains condensers and evaporators that are based on the Modelica
Standard Library (MSL).
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end CondensersEvaporators;
