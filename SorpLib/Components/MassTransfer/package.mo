within SorpLib.Components;
package MassTransfer "Models and correlations to calculate mass transfers"
  extends SorpLib.Icons.MassTransfersPackage;

  annotation (Documentation(info="<html>
<p>
This package includes various mass transfer models for deal gases and real 
substances (i.e., with a two-phase region). The mass transfer models can be
used within closed or open adsorbers to describe the mass transfer between
adsorbate volumes and other volumes, such as gas, VLE, or phase saparator
volumes.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end MassTransfer;
