within SorpLib.Components.MassTransfer;
package VLEMassTransfers "Mass transfer models for real fluids (i.e., with a two-phase regime)"
  extends Modelica.Icons.VariantsPackage;

  annotation (Documentation(info="<html>
<p>
This package contains mass transfer models using real fluids (i.e., with a two-
phase regime) that are based on the open-source Modelica Standard Library (MSL).
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end VLEMassTransfers;
