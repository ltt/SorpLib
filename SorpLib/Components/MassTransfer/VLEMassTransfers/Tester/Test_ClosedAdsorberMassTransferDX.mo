within SorpLib.Components.MassTransfer.VLEMassTransfers.Tester;
model Test_ClosedAdsorberMassTransferDX
  "Tester for the loading-driven mass transfer model used within closed adsorbers describing pure component adsorption"
  extends Modelica.Icons.Example;

  //
  // Definition of parameters
  //
  replaceable package Medium = Modelica.Media.Water.StandardWater
    constrainedby Modelica.Media.Interfaces.PartialTwoPhaseMedium
    "Medium model"
    annotation (Dialog(tab="General", group="Media"),
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

  replaceable model WorkingPair =
    SorpLib.Media.WorkingPairs.PureComponents.H2O.Silicagel123_DubininLorentzianCumulative_Schawe2000_VLE
    constrainedby SorpLib.Media.WorkingPairs.PureComponents.WorkingPairVLE
    "Working pair model"
    annotation (Dialog(tab="General", group="Media"),
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

  //
  // Definition of boundary models
  //
  SorpLib.Basics.Sources.Fluids.VLESource fluidSource(
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    use_pInput=true,
    T_fixed=323.15,
    redeclare package Medium = Medium)
    "Fluid source"
    annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={-40,60})));

  SorpLib.Basics.Sources.Thermal.HeatSource heatSource(
    boundaryType=SorpLib.Choices.BoundaryThermal.Temperature,
    use_TInput=true)
    "Heat source for fluid volume"
    annotation (Placement(transformation(
                extent={{-10,-10},{10,10}},
                rotation=270,
                origin={40,60})));

  //
  // Definition of heat and mass transfer models
  //
  SorpLib.Components.MassTransfer.VLEMassTransfers.ClosedAdsorberMassTransferDX
    massTransfer(
    calculateFluidProperties=true,
    fluidPropertyPosition=SorpLib.Choices.MassTransferFluidProperties.AverageInstreaming,
    p_min(displayUnit="Pa") = 1000,
    redeclare model MassTransferCoefficient =
        MassTransferCoefficientCorrelations.ClosedAdsorber.LoadingDriven.GlueckaufArrhenius
        (m_sorbent=adsorbateVolume.m_sor_initial),
    x_adsorpt_input=adsorbateVolume.x,
    T_adsorpt_input=adsorbateVolume.T,
    redeclare model WorkingPair = WorkingPair,
    redeclare package Medium = Medium) "Mass transfer model" annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={-40,40})));

  SorpLib.Components.HeatTransfer.ClosedAdsorberHeatTransfer heatTransfer(
    redeclare model HeatTransferCoefficient =
      HeatTransfer.HeatTransferCoefficientCorrelations.ClosedAdsorber.LinearAlphaA
        (
        constantAlphaA=500, b=2),
    fluidProperties=SorpLib.Components.HeatTransfer.Records.FluidProperties(
      p=adsorbateVolume.adsorbateProperties.p,
      T=adsorbateVolume.adsorbateProperties.T,
      rho=1/adsorbateVolume.workingPair.medium_sorbent.state_variables.v,
      cp=adsorbateVolume.workingPair.medium_sorbent.additional_variables.c,
      eta=0,
      lambda=adsorbateVolume.workingPair.medium_sorbent.additional_variables.lambda))
    "Heat transfer model"
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={40,40})));

  //
  // Definition of liquid volume models
  //
  Basics.Volumes.AdsorbateVolumes.AdsorbatePureVLEVolume adsorbateVolume(
    T_initial=373.15,
    useHeatPorts=false,
    useHeatPortsX=false,
    geometry(V=Modelica.Constants.pi/4*0.1^2*1),
    redeclare model PureWorkingPairModel = WorkingPair (
      approachSorptionEnthalpy=SorpLib.Choices.SorptionEnthalpy.ClausiusClapeyron,
      approachSpecificHeatCapacity=SorpLib.Choices.SpecificHeatCapacityAdsorpt.ChakrabortyElAl,
      limitLowerPressure=true,
      limitLowerPressureAdsorptive=true),
    x_initial=0.1,
    redeclare final package Medium = Medium,
    nSorptionPorts=1)
    "Model of an adsorbate volume"
    annotation (Placement(transformation(
        extent={{-20,-20},{20,20}},
        rotation=0,
        origin={0,0})));

  //
  // Definition of thermal boundaries
  //
protected
  Modelica.Blocks.Sources.Sine input_T(
    amplitude=50,
    f=1/250,
    offset=273.15 + 75) "Input for temperature"
    annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={40,80})));

  Modelica.Blocks.Sources.Sine input_p(
    amplitude=2.5e3,
    f=1/250,
    offset=5e3)
    "Input for pressure"
    annotation (Placement(transformation(
        extent={{10,10},{-10,-10}},
        rotation=90,
        origin={-40,80})));

equation
  //
  // Connections
  //
  connect(fluidSource.port, massTransfer.port_a) annotation (Line(
      points={{-40,60},{-40,47.8}},
      color={0,140,72},
      thickness=1));
  connect(massTransfer.port_b, adsorbateVolume.fp_sorption[1]) annotation (Line(
      points={{-40,32},{-40,20},{-3.2,20},{-3.2,8.8}},
      color={0,140,72},
      thickness=1));

  connect(heatSource.port, heatTransfer.hp_b[1]) annotation (Line(
      points={{40,60},{40,48}},
      color={238,46,47},
      thickness=1));
  connect(heatTransfer.hp_a[1], adsorbateVolume.hp_sorption) annotation (Line(
      points={{40,32},{40,28},{3.2,28},{3.2,15.2}},
      color={238,46,47},
      thickness=1));

  connect(input_p.y, fluidSource.p_input) annotation (Line(points={{-40,69},{-40,
          64},{-45,64},{-45,61.2}}, color={0,0,127}));
  connect(input_T.y, heatSource.T_input) annotation (Line(points={{40,69},{40,64},
          {45.2,64},{45.2,61}},
                              color={0,0,127}));

  //
  // Annotations
  //
  annotation (experiment(StopTime=2500), Documentation(info="<html>
<p>
This model checks the loading-driven mass transfer model used within closed 
adsorbers describing pure component adsorption.
<br/><br/>
To see the model behavior, plot the variables of the models over the 
time. The simulation time is correctly preset (Start: 0 s, Stop = 2500 s). 
</p>
</html>", revisions="<html>
<ul>
  <li>
  January 25, 2024, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Test_ClosedAdsorberMassTransferDX;
