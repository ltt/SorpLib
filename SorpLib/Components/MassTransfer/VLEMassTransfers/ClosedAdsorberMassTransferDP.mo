within SorpLib.Components.MassTransfer.VLEMassTransfers;
model ClosedAdsorberMassTransferDP
  "Pressure-driven mass transfer models describing pure component adsorption in closed adsorbern"
  extends SorpLib.Components.MassTransfer.BaseClasses.PartialPureMassTransferDP(
    redeclare final SorpLib.Basics.Interfaces.FluidPorts.VLEPort_in port_a,
    redeclare final SorpLib.Basics.Interfaces.FluidPorts.VLEPort_out port_b,
    final no_components=Medium.nX,
    redeclare replaceable model MassTransferCoefficient =
    SorpLib.Components.MassTransfer.MassTransferCoefficientCorrelations.ClosedAdsorber.PressureDriven.ConstantCoefficient
      constrainedby
      SorpLib.Components.MassTransfer.BaseClasses.PartialMassTransferCoefficientClosedAdsorberDP(
        fluidProperties=fluidProperties));

  //
  // Definition of parameters regarding the medium
  //
  replaceable package Medium =
    Modelica.Media.Water.StandardWater
    constrainedby Modelica.Media.Interfaces.PartialTwoPhaseMedium
    "Medium model of the real fluid (i.e., with two-phase regime)"
    annotation (Dialog(tab="Medium", group="Fluid"),
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

  //
  // Definition of state records
  //
  Medium.ThermodynamicState stateAIn = if calculateFluidProperties and not
    fluidPropertyPosition == SorpLib.Choices.MassTransferFluidProperties.PortBInlet
    then Medium.setState_phX(
      p = if limitPressureForCalculations then max(port_a.p, p_min) else port_a.p,
      h = inStream(port_a.h_outflow),
      X = inStream(port_a.Xi_outflow))
    else Medium.setState_phX(
        p=Medium.p_default,
        h=Medium.specificEnthalpy_pTX(
          p=Medium.p_default,
          T=Medium.T_default,
          X=Medium.X_default),
        X=Medium.X_default)
    "Instreaming state properties at port a";

  Medium.ThermodynamicState stateBIn = if calculateFluidProperties and not
    fluidPropertyPosition == SorpLib.Choices.MassTransferFluidProperties.PortAInlet
    then Medium.setState_phX(
      p = if limitPressureForCalculations then max(port_b.p, p_min) else port_b.p,
      h = inStream(port_b.h_outflow),
      X = inStream(port_b.Xi_outflow))
    else Medium.setState_phX(
        p=Medium.p_default,
        h=Medium.specificEnthalpy_pTX(
          p=Medium.p_default,
          T=Medium.T_default,
          X=Medium.X_default),
        X=Medium.X_default)
    "Instreaming state properties at port b";

  //
  // Definition of connectors for conditional removed models / variables
  //
protected
  Modelica.Blocks.Interfaces.RealInput T_adsorptiveA_calc(final unit="K")=
    Medium.temperature(state=stateAIn) if
    calculateFluidProperties
    "Temperature of adsorptive at port a";
  Modelica.Blocks.Interfaces.RealInput T_adsorptiveB_calc(final unit="K")=
    Medium.temperature(state=stateBIn) if
    calculateFluidProperties
    "Temperature of adsorptive at port b";

  Modelica.Blocks.Interfaces.RealInput d_adsorptiveA_calc(final unit="kg/m3")=
    Medium.density(state=stateAIn) if
    calculateFluidProperties
    "Density of adsorptive at port a";
  Modelica.Blocks.Interfaces.RealInput d_adsorptiveB_calc(final unit="kg/m3")=
    Medium.density(state=stateBIn) if
    calculateFluidProperties
    "Density of adsorptive at port b";

  Modelica.Blocks.Interfaces.RealInput eta_adsorptiveA_calc(final unit="Pa.s")=
    Medium.dynamicViscosity(state=stateAIn) if
    calculateFluidProperties
    "Dynamic viscosity of adsorptive at port a";
  Modelica.Blocks.Interfaces.RealInput eta_adsorptiveB_calc(final unit="Pa.s")=
    Medium.dynamicViscosity(state=stateBIn) if
    calculateFluidProperties
    "Dynamic viscosity of adsorptive at port b";

equation
  //
  // Connections
  //
  connect(T_adsorptiveA_internal, T_adsorptiveA_calc);
  connect(T_adsorptiveB_internal, T_adsorptiveB_calc);

  connect(d_adsorptiveA_internal, d_adsorptiveA_calc);
  connect(d_adsorptiveB_internal, d_adsorptiveB_calc);

  connect(eta_adsorptiveA_internal, eta_adsorptiveA_calc);
  connect(eta_adsorptiveB_internal, eta_adsorptiveB_calc);

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This mass transfer model is used to connect adsorbate volumes with a VLE volume,
evaporator, condenser, or other adsorbate volumes. Thus, the mass heat transfer 
model represents a hydraulic resistance between two models. Depending on the 
driving potential (i.e., pressure difference) and the chosen transport phenomena, 
this model determines the mass flow rate between the connected models. Note that
port b is located at the adsorbate volume by design.
</p>

<h4>Main equations</h4>
<p>
The model has a steady-state energy balance
</p>
<pre>
    0 = port_a.m_flow + port_b.m_flow;
</pre>
<p>
and a steady-state energy balance
</p>
<pre>
    port_a.h_outflow = port_b.h_outflow;
</pre>
<p>
The mass flow rate <i>port_a.m_flow</i> is calculated linearly dependent on the
pressure difference <i>&Delta;p = port_a.p - port_b.p</i>:
</p>
<pre>
    port_a.m_flow = &beta; * &Delta;p = &beta; * (port_a.p - port_b.p);
</pre>
<p>
The mass transport coefficient is calculated according to the selected corrlation.
</p>

<h4>Assumptions and limitations</h4>
<ul>
  <li>
  Steady-state process
  </li>
  <li>
  No storage of mass or energy
  </li>
  <li>
  Isenthalpic process
  </li>
  <li>
  Linear driving force approach
  </li>
</ul>

<h4>Typical use</h4>
<p>
This mass transfer model is used to describe the mass transfer between an
adsorbate volume and VLE volume or evaporator/condenser within closed adosrbers.
</p>

<h4>Important parameters and options</h4>
<ul>
  <li>
  <i>canBeActivated</i>:
  Defines if the mass transfer can activaly be activated or deactivated via an
  input signal.
  </li>
  <li>
  <i>isFlapValve</i>:
  Defines if the mass transfer model behaves like a flap valve.
  </li>
  <li>
  <i>isFlowAB</i>:
  Defines the flow direction if the mass transfer models behaves like a flap
  valve.
  </li>
  <li>
  <i>offset_dp</i>:
  Defines an optional offset for pressure difference if the mass transfer behaves
  likes a flap valve.
  </li>
  <br>
  <li>
  <i>useBetaInput</i>:
  Defines if mass transfer coefficient is given via an input.
  </li>
  <li>
  <i>calculateFluidProperties</i>:
  Defines if fluid properties are required to calculate the mass transfer coefficient.
  </li>
  <li>
  <i>fluidPropertyPosition</i>:
  Defines the position of the fluid properties used for calculations.
  </li>
  <br/>
  <li>
  <i>avoid_events</i>:
  Defines if events shall be avoided via the noEvent()-operator.
  </li>
  <li>
  <i>avoid_events_activating</i>:
  Defines if events shall be avoided via the noEvent()-operator when activating/
  deactivating the mass transfer.
  </li>
</ul>

<h4>References</h4>
<ul>
  <li>
  Lanzerath, F. and Bau, U. and Seiler, J. and Bardow, A. (2015). Optimal design of adsorption chillers based on a validated dynamic object-oriented model. Science and Technology for the Built Environment, 21(3), 248-257. DOI: https://doi.org/10.1080/10789669.2014.990337.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  January 24, 2024, by Mirko Engelpracht:<br/>
  Minor revisions and documentation.
  </li>
  <li>
  January 18, 2021, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end ClosedAdsorberMassTransferDP;
