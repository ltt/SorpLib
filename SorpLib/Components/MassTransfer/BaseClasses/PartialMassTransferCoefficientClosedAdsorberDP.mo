within SorpLib.Components.MassTransfer.BaseClasses;
partial model PartialMassTransferCoefficientClosedAdsorberDP
  "Base model for all models calculating the pressure-driven mass transfer coefficient of closed adsorbers"
  extends
    SorpLib.Components.MassTransfer.BaseClasses.PartialMassTransferCoefficient(
    beta(unit="m.s"));

  //
  // Definition of inputs
  //
  replaceable parameter SorpLib.Components.MassTransfer.Records.GeometryClosedAdsorber geometry
    constrainedby
    SorpLib.Components.MassTransfer.Records.GeometryClosedAdsorber
    "Geometry of the closed adsorber"
    annotation (Dialog(tab="General", group="Mass Transfer", enable=false));

  //
  // Definition of inputs
  //
  input SorpLib.Components.MassTransfer.Records.FluidProperties fluidProperties
    "Fluid properties that may be needed for calculations"
    annotation (Dialog(tab="General", group="Inputs", enable=false));

  //
  // Annotations
  //
  annotation (Icon(graphics={Ellipse(
          extent={{100,100},{-100,-100}},
          lineColor={0,0,0},
          fillColor={0,255,255},
          fillPattern=FillPattern.Solid), Text(
          extent={{-80,80},{80,-80}},
          lineColor={0,0,0},
          fillColor={0,255,255},
          fillPattern=FillPattern.Solid,
          textString="DP")}), Documentation(revisions="<html>
<ul>
  <li>
  January 24, 2024, by Mirko Engelpracht:<br/>
  Minor adaptations and documentation.
  </li>
  <li>
  January 18, 2021, by Mirko Engelpracht:<br/>
  Minor revisions after restructuring of the library.
  </li>
  <li>
  November 30, 2017, by Uwe Bau:<br/>
  Tidy up implementation and enhance documentation for publication of library.
  </li>
</ul>
</html>", info="<html>
<p>
This partial model is the base model for all models calculating the presure-driven
mass transfer coefficient <i>&beta;</i>. It defines fundamental parameters and 
variables required by all mass transfer coefficient models. Models that inherit 
properties from this partial model have to add an equation for calculating the mass 
transfer coefficient. In this context, records may be added that containg geometry 
and fluid property data.
<br/><br/>
The following variables must be specified in the model that inherit properties:
</p>
<ul>
  <li>
  Meat transfer coefficient <i>beta</i>.
  </li>
</ul>
</html>"));
end PartialMassTransferCoefficientClosedAdsorberDP;
