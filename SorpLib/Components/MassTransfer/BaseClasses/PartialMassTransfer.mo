within SorpLib.Components.MassTransfer.BaseClasses;
partial model PartialMassTransfer
  "Base model for all mass transfer models"

  //
  // Definition of parameters describing the kind of valve
  //
  parameter Boolean canBeActivated = false
    "= true, if mass transfer can be activated or deactiavted"
    annotation (Dialog(tab="General",group="Calculation Setup"),
                Evaluate = true,
                HideResult=true);
  parameter Boolean isFlapValve = false
    "= true, if mass transfer behaves like a flap valve"
    annotation (Dialog(tab="General",group="Calculation Setup"),
                Evaluate = true,
                HideResult=true);
  parameter Boolean isFlowAB = true
    "= true, if flow direction is from port a->b; otherwise, flow direction is
    from port b->a"
    annotation (Dialog(tab="General",group="Calculation Setup",
                enable = isFlapValve),
                Evaluate = true,
                HideResult=true);
  parameter Modelica.Units.SI.PressureDifference offset_dp = 0
    "Optional offset for pressure difference (i.e., dp + offset_dp) to accout for,
    e.g., hydostatic pressure differences when controlling flap valve status"
    annotation (Dialog(tab="General",group="Calculation Setup",
                enable = isFlapValve));

  //
  // Definition of parameters regarding the mass transfer coefficient
  //
  parameter Boolean useBetaInput = false
    " = true, if beta is given by input; otherwise, beta is calculated by
    selected model"
    annotation (Dialog(tab="General", group="Mass Transfer Coefficient"),
                Evaluate = true,
                HideResult=true);
  parameter Boolean calculateFluidProperties = false
    "= true, to calculate instreaming fluid properties at port a and b"
    annotation (Dialog(tab="General", group="Mass Transfer Coefficient",
                enable = not useBetaInput),
                Evaluate = true,
                HideResult=true);
  parameter SorpLib.Choices.MassTransferFluidProperties fluidPropertyPosition=
    SorpLib.Choices.MassTransferFluidProperties.AverageInstreaming
    "Defines the calculation approach of fluid properties if needed"
    annotation (Dialog(tab="General", group="Mass Transfer Coefficient",
                enable = not useBetaInput),
                Evaluate = true,
                HideResult=true);

  //
  // Definition of parameters regarding the medium
  //
  parameter Integer no_components = 1
    "Number of components"
    annotation (Dialog(tab="Medium", group="Fluid"),
                Evaluate=true,
                HideResult=true);

  //
  // Definition or initialisation parameters
  //
  parameter Modelica.Units.SI.MassFlowRate m_flow_start = 1e-4
    "Start value for mass flow rate"
    annotation (Dialog(tab="Initialisation", group="Start Values"));

  //
  // Definition of advanced parameters
  //
  parameter Boolean limitPressureForCalculations = true
    "= true, if pressure is limited while fluid property calculations"
    annotation (Dialog(tab = "Advanced", group = "Limiter",
                enable=calculateFluidProperties),
                choices(checkBox=true),
                Evaluate=true,
                HideResult = true);
  parameter Modelica.Units.SI.Pressure p_min = 1000
    "Minimal pressure for fluid property calculation"
    annotation (Dialog(tab = "Advanced", group = "Limiter",
                enable=calculateFluidProperties and limitPressureForCalculations),
                Evaluate=true,
                HideResult = true);

  parameter Boolean avoid_events = false
    "= true, if events are avoid by using noEvent()-operator"
    annotation (Dialog(tab = "Advanced", group = "Numerics"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult = true);
  parameter Boolean avoid_events_activating = avoid_events
    "= true, if events are avoid by using noEvent()-operator for activating/deactivating
    the mass transfer"
    annotation (Dialog(tab = "Advanced", group = "Numerics"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult = true);

  //
  // Definition of connectors
  //
  Modelica.Blocks.Interfaces.RealInput activated_input(final unit="1") if
    canBeActivated
    " > 0.75, if mass transfer is activated; < 0.25, if mass transfer is decactivated;
    otherwise, smooth transiton regime"
    annotation (Placement(transformation(extent={{-10,-10},{10,10}},
                rotation=90,origin={10,-60}), iconTransformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={10,-60})));

  //
  // Definition of protected connectors
  //
protected
  Modelica.Blocks.Interfaces.RealOutput activated_fixed(final unit="1") = 1 if
    not canBeActivated
    "Needed for connecting to conditional connector";

  Modelica.Blocks.Interfaces.RealInput activated_internal(final unit="1")
    "Needed for connecting to conditional connector";

  //
  // Definition of variables
  //
public
  Modelica.Units.SI.PressureDifference dp
    "Pressure difference between port a and b";

equation
  //
  // Connectors
  //
  connect(activated_internal, activated_input);
  connect(activated_internal, activated_fixed);

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial model is the base model for all mass transfers. It defines 
fundamental parameters and variables required by all mass transfers. Models 
that inherit properties from this partial model have to add a mass transfer
coefficient model. In this context, records may be added that contain geometry
and fluid property data.
<br/><br/>
The following variables must be specified in the model that inherit properties:
</p>
<ul>
  <li>
  Pressure difference <i>dp</i> between port a and b.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  January 24, 2024, by Mirko Engelpracht:<br/>
  Minor revision and added documentation.
  </li>
  <li>
  January 18, 2021, by Mirko Engelpracht:<br/>
  Major revisions (e.g., object-oriented approach) after restructuring of the library.
  </li>
  <li>
  November 30, 2017, by Uwe Bau:<br/>
  Tidy up implementation and enhance documentation for publication of library.
  </li>
</ul>
</html>"), Icon(graphics={
        Ellipse(
          extent={{80,62},{-40,-58}},
          lineColor={0,0,0},
          fillColor={175,175,175},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{-12,56},{-8,48},{-8,40},{-6,34},{-2,28},{6,18},{20,16},{52,16},
              {50,10},{40,10},{18,12},{16,8},{24,4},{40,2},{46,-2},{48,-8},{46,-18},
              {42,-24},{38,-32},{28,-36},{18,-38},{12,-38},{2,-34},{4,-30},{10,-34},
              {18,-34},{26,-32},{32,-26},{38,-20},{40,-12},{38,-6},{32,-4},{26,-2},
              {22,0},{16,2},{12,6},{14,0},{20,-6},{26,-14},{20,-14},{16,-6},{10,
              0},{8,4},{10,10},{6,14},{0,18},{-4,20},{-4,16},{-6,12},{-4,10},{-4,
              4},{-4,2},{-4,0},{-2,-2},{0,-4},{4,-10},{-4,-4},{-4,-8},{-4,-10},{
              -4,-18},{-10,-24},{-8,-8},{-6,-2},{-10,0},{-24,2},{-30,6},{-28,8},
              {-16,4},{-8,2},{-8,8},{-10,12},{-14,14},{-22,22},{-28,32},{-22,28},
              {-10,16},{-8,22},{-10,28},{-14,34},{-10,42},{-12,56}},
          lineColor={0,0,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          smooth=Smooth.Bezier),
        Text(
          extent={{-40,80},{-20,60}},
          lineColor={0,0,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.None,
          textString="D",
          textStyle={TextStyle.Italic}),
        Line(
          points={{-24,66},{-18,62},{-12,54}},
          color={0,140,72},
          smooth=Smooth.Bezier,
          arrow={Arrow.None,Arrow.Filled},
          thickness=0.5),
        Line(
          points={{-10,52},{-10,44},{-6,30},{4,18},{14,10},{20,2},{34,-2},{44,-10},
              {40,-22},{28,-34},{12,-36},{4,-32}},
          color={0,140,72},
          thickness=0.5,
          smooth=Smooth.Bezier,
          arrow={Arrow.None,Arrow.Filled}),
        Ellipse(
          extent={{-12,54},{-10,52}},
          lineColor={0,140,72},
          lineThickness=0.5,
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{4,-30},{6,-32}},
          lineColor={0,140,72},
          lineThickness=0.5,
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-68,52},{-60,60}},
          lineColor={28,108,200},
          lineThickness=0.5,
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-60,40},{-52,48}},
          lineColor={28,108,200},
          lineThickness=0.5,
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-74,32},{-66,40}},
          lineColor={28,108,200},
          lineThickness=0.5,
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-60,20},{-52,28}},
          lineColor={28,108,200},
          lineThickness=0.5,
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-72,12},{-64,20}},
          lineColor={28,108,200},
          lineThickness=0.5,
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-60,2},{-52,10}},
          lineColor={28,108,200},
          lineThickness=0.5,
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-84,20},{-76,28}},
          lineColor={28,108,200},
          lineThickness=0.5,
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-84,44},{-76,52}},
          lineColor={28,108,200},
          lineThickness=0.5,
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-56,62},{-48,70}},
          lineColor={28,108,200},
          lineThickness=0.5,
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-80,64},{-72,72}},
          lineColor={28,108,200},
          lineThickness=0.5,
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-66,74},{-58,82}},
          lineColor={28,108,200},
          lineThickness=0.5,
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-82,0},{-74,8}},
          lineColor={28,108,200},
          lineThickness=0.5,
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-70,-6},{-62,2}},
          lineColor={28,108,200},
          lineThickness=0.5,
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-56,-18},{-48,-10}},
          lineColor={28,108,200},
          lineThickness=0.5,
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-74,-22},{-66,-14}},
          lineColor={28,108,200},
          lineThickness=0.5,
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-86,-34},{-78,-26}},
          lineColor={28,108,200},
          lineThickness=0.5,
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-64,-34},{-56,-26}},
          lineColor={28,108,200},
          lineThickness=0.5,
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-48,-40},{-40,-32}},
          lineColor={28,108,200},
          lineThickness=0.5,
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-76,-50},{-68,-42}},
          lineColor={28,108,200},
          lineThickness=0.5,
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-60,-52},{-52,-44}},
          lineColor={28,108,200},
          lineThickness=0.5,
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-86,-64},{-78,-56}},
          lineColor={28,108,200},
          lineThickness=0.5,
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid)}));
end PartialMassTransfer;
