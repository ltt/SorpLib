within SorpLib.Components.MassTransfer;
package BaseClasses "Base models and functions for all mass transfers"
  extends Modelica.Icons.BasesPackage;

  annotation (Documentation(info="<html>
<p>
This package contains partial mass transfer and mass transfer coefficient models, 
containing fundamental definitions of parameters and variables. The content of 
this package is only of interest when adding new mass transfer and mass transfer
coefficient models to the library. 
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end BaseClasses;
