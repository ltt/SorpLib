within SorpLib.Components.MassTransfer.BaseClasses;
partial model PartialPureMassTransferDX
  "Base model for all loading-driven mass transfer models describing pure component adsorption"
  extends SorpLib.Components.MassTransfer.BaseClasses.PartialPureMassTransfer;

  //
  // Definition of inputs
  //
  input SorpLib.Units.Uptake x_adsorpt_input
    "Actual adsorpt loading (i.e., loading at pot b)"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.Temperature T_adsorpt_input
    "Actual adsorpt temperature (i.e., temperature at port b)"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of parameters regarding the medium
  //
  replaceable model WorkingPair =
      SorpLib.Media.WorkingPairs.PureComponents.H2O.Silicagel123_DubininLorentzianCumulative_Schawe2000_VLE
    constrainedby
    SorpLib.Media.WorkingPairs.BaseClasses.PartialPureWorkingPairs(
      stateVariables=SorpLib.Choices.IndependentVariablesPureComponentWorkingPair.pT,
      calcCaloricProperties=false,
      calcEntropicProperties=false,
      calcDerivativesIsotherm=false,
      calcDerivativesMassEnergyBalance=false,
      calcDerivativesEntropyBalance=false)
    "Working pair model"
    annotation (Dialog(tab="Medium", group="Working Pair"),
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

  //
  // Definition of protected connectors
  //
protected
  Modelica.Blocks.Interfaces.RealOutput T_adsorbate_calc(final unit="K")=
    T_adsorpt_input if calculateFluidProperties
    "Needed for connecting to conditional connector";

  //
  // Instantiation of models
  //
public
  WorkingPair workingPair_a_theoretical(
    final p_adsorpt=port_a.p,
    final T_adsorpt=T_adsorpt_input,
    final x_adsorpt=x_adsorpt_theoretical)
    "Working pair model to calculate equilibrium properties at port a";

  //
  // Definition of variables
  //
  SorpLib.Units.Uptake x_adsorpt_theoretical
    "Theoretical equilibrium loading of adsorbate at port a (i.e., 
    vapor/gas phase)";

equation
  //
  // Connectors
  //
  connect(T_adsorbate_internal, T_adsorbate_calc);

  //
  // Set driving potential
  //
  drivingForce_internal = x_adsorpt_theoretical - x_adsorpt_input
    "Loading driven mass flow rate";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial model is the base model for all pressure-driven mass transfers of 
pure component adsorption. It defines fundamental parameters and variables required 
by all mass transfers. Models that inherit properties from this partial model have 
to redeclare the fluid ports. Moreover, these models must redeclare and constrain 
the model calculating the mass transfer coefficient and area as well as the working
pair In this context, records may be added that containg geometry and fluid property 
data. Furtheremore, the driving force must be defined.
<br/><br/>
The following variables must be specified in the model that inherit properties:
</p>
<ul>
  <li>
  Instreaming adsorptive temperature at port a <i>T_adsorptiveA_internal</i>.
  </li>
  <li>
  Instreaming adsorptive temperature at port b <i>T_adsorptiveA_internal</i>.
  </li>
  <li>
  Instreaming adsorptive density at port a <i>d_adsorptiveA_internal</i>.
  </li>
  <li>
  Instreaming adsorptive density at port b <i>d_adsorptiveB_internal</i>.
  </li>
  <li>
  Instreaming adsorptive dynamic viscosity at port a <i>eta_adsorptiveB_internal</i>.
  </li>
  <li>
  Instreaming adsorptive dynamic viscosity at port b <i>eta_adsorptiveA_internal</i>.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  January 25, 2024, by Mirko Engelpracht:<br/>
  Minor revisions and documentation.
  </li>
  <li>
  January 18, 2021, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end PartialPureMassTransferDX;
