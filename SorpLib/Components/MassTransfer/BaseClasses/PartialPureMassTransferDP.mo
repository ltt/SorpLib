within SorpLib.Components.MassTransfer.BaseClasses;
partial model PartialPureMassTransferDP
  "Base model for all pressure-driven mass transfer models describing pure component adsorption"
  extends SorpLib.Components.MassTransfer.BaseClasses.PartialPureMassTransfer;

  //
  // Definition of protected connectors
  //
protected
  Modelica.Blocks.Interfaces.RealOutput T_adsorbate_calc(final unit="K") = 0 if
    calculateFluidProperties
    "Needed for connecting to conditional connector: Value is not needed";

equation
  //
  // Connectors
  //
  connect(T_adsorbate_internal, T_adsorbate_calc);

  //
  // Set driving potential
  //
  drivingForce_internal = port_a.p - port_b.p
    "Pressure driven mass flow rate";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial model is the base model for all pressure-driven mass transfers of 
pure component adsorption. It defines fundamental parameters and variables required 
by all mass transfers. Models that inherit properties from this partial model have 
to redeclare the fluid ports. Moreover, these models must redeclare and constrain 
the model calculating the mass transfer coefficient and area. In this context, records 
may be added that containg geometry and fluid property data. Furtheremore, the driving 
force must be defined.
<br/><br/>
The following variables must be specified in the model that inherit properties:
</p>
<ul>
  <li>
  Instreaming adsorptive temperature at port a <i>T_adsorptiveA_internal</i>.
  </li>
  <li>
  Instreaming adsorptive temperature at port b <i>T_adsorptiveA_internal</i>.
  </li>
  <li>
  Instreaming adsorptive density at port a <i>d_adsorptiveA_internal</i>.
  </li>
  <li>
  Instreaming adsorptive density at port b <i>d_adsorptiveB_internal</i>.
  </li>
  <li>
  Instreaming adsorptive dynamic viscosity at port a <i>eta_adsorptiveB_internal</i>.
  </li>
  <li>
  Instreaming adsorptive dynamic viscosity at port b <i>eta_adsorptiveA_internal</i>.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  January 24, 2024, by Mirko Engelpracht:<br/>
  Minor revisions and documentation.
  </li>
  <li>
  January 18, 2021, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end PartialPureMassTransferDP;
