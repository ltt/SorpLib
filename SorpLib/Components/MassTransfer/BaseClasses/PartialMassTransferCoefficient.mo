within SorpLib.Components.MassTransfer.BaseClasses;
partial model PartialMassTransferCoefficient
  "Base model for all models calculating the mass transfer coefficient"

  //
  // Definition of parameters regarding the mass transfer coefficient
  //
  parameter Boolean computeTransportProperties = false
    "= true, if fluid transport properties are required and must be calculated"
    annotation (Dialog(tab = "General", group = "Mass Transfer"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult = true);

  //
  // Definition of advanced parameters
  //
  parameter Boolean avoid_events = false
    "= true, if events are avoid by using noEvent()-operator"
    annotation (Dialog(tab = "Advanced", group = "Numerics"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult = true);

  //
  // Definition of outputs
  //
  Modelica.Blocks.Interfaces.RealOutput beta
    "Mass transfer coefficient";

  //
  // Annotations
  //
  annotation (Icon(graphics={Ellipse(
          extent={{100,100},{-100,-100}},
          lineColor={0,0,0},
          fillColor={0,255,255},
          fillPattern=FillPattern.Solid)}),
                              Documentation(revisions="<html>
<ul>
  <li>
  January 24, 2024, by Mirko Engelpracht:<br/>
  Minor adaptations and documentation.
  </li>
  <li>
  January 18, 2021, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>", info="<html>
<p>
This partial model is the base model for all models calculating the mass transfer
coefficient <i>&beta;</i>. It defines fundamental parameters and variables required
by all mass transfer coefficient models. Models that inherit properties from this 
partial model have to add an equation for calculating the mass transfer coefficient.
In this context, records may be added that containg geometry and fluid property data.
<br/><br/>
The following variables must be specified in the model that inherit properties:
</p>
<ul>
  <li>
  Meat transfer coefficient <i>beta</i>.
  </li>
</ul>
</html>"));
end PartialMassTransferCoefficient;
