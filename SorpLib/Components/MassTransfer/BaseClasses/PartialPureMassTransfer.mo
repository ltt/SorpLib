within SorpLib.Components.MassTransfer.BaseClasses;
partial model PartialPureMassTransfer
  "Base model for all mass transfer models describing pure component adsorption"
  extends SorpLib.Components.MassTransfer.BaseClasses.PartialMassTransfer;

  //
  // Definition of parameters regarding the mass transfer coefficient
  //
  replaceable model MassTransferCoefficient =
    SorpLib.Components.MassTransfer.BaseClasses.PartialMassTransferCoefficient
    constrainedby
    SorpLib.Components.MassTransfer.BaseClasses.PartialMassTransferCoefficient
    "Model calculating the mass transfer coefficient"
    annotation (Dialog(tab="General", group="Mass Transfer Coefficient",
                enable=not useBetaInput),
                choicesAllMatching=true,
                HideResult=true,
                Evaluate=true);

  //
  // Definition or advanced parameters
  //
  parameter Modelica.Units.SI.MassFlowRate m_flow_small = 1e-4
    "Regularization mass flow rate"
    annotation (Dialog(tab="Advanced", group="Numerics"));
  parameter Integer noDiff = 2
    "Specification how often transition functions can be differentiated"
    annotation(Dialog(tab = "Advanced", group = "Numerics"),
              Evaluate=true,
              HideResult=true);

  //
  // Definition of connectors
  //
  Modelica.Blocks.Interfaces.RealInput beta_input if useBetaInput
    "Mass transport coefficient given by input"
    annotation (Placement(transformation(extent={{-10,-10},{10,10}},
                rotation=90,origin={30,-60}), iconTransformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={30,-60})));

  //
  // Definition of protected connectors
  //
protected
  Modelica.Blocks.Interfaces.RealOutput dummyT(final unit="K") = 0 if
    not calculateFluidProperties
    "Needed for connecting to conditional connector";
  Modelica.Blocks.Interfaces.RealOutput dummyD(final unit="kg/m3") = 0 if
    not calculateFluidProperties
    "Needed for connecting to conditional connector";
  Modelica.Blocks.Interfaces.RealOutput dummyEta(final unit="Pa.s") = 0 if
    not calculateFluidProperties
    "Needed for connecting to conditional connector";

  Modelica.Blocks.Interfaces.RealInput beta_internal
    "Needed for connecting to conditional connector";

  Modelica.Blocks.Interfaces.RealInput T_adsorbate_internal(final unit="K")
    "Needed for connecting to conditional connector";
  Modelica.Blocks.Interfaces.RealInput T_adsorptiveA_internal(final unit="K")
    "Needed for connecting to conditional connector";
  Modelica.Blocks.Interfaces.RealInput T_adsorptiveB_internal(final unit="K")
    "Needed for connecting to conditional connector";
  Modelica.Blocks.Interfaces.RealInput d_adsorptiveA_internal(final unit="kg/m3")
    "Needed for connecting to conditional connector";
  Modelica.Blocks.Interfaces.RealInput d_adsorptiveB_internal(final unit="kg/m3")
    "Needed for connecting to conditional connector";
  Modelica.Blocks.Interfaces.RealInput eta_adsorptiveA_internal(final unit="Pa.s")
    "Needed for connecting to conditional connector";
  Modelica.Blocks.Interfaces.RealInput eta_adsorptiveB_internal(final unit="Pa.s")
    "Needed for connecting to conditional connector";

  //
  // Definition of ports
  //
public
  replaceable SorpLib.Basics.Interfaces.BaseClasses.PartialFluidPort port_a
    constrainedby Basics.Interfaces.BaseClasses.PartialFluidPort(
      final no_components=no_components,
      m_flow(start=m_flow_start))
    "Fluid port a"
    annotation (Placement(transformation(extent={{-88,-10},{-68,10}}),
                iconTransformation(extent={{-88,-10},{-68,10}})),
                choicesAllMatching=true);

  replaceable SorpLib.Basics.Interfaces.BaseClasses.PartialFluidPort port_b
    constrainedby Basics.Interfaces.BaseClasses.PartialFluidPort(
      final no_components=no_components,
      m_flow(start=-m_flow_start))
    "Fluid ports b"
    annotation (Placement(transformation(extent={{70,-10},{90,10}}),
                iconTransformation(extent={{70,-10},{90,10}})),
                choicesAllMatching=true);

  //
  // Definition of models
  //
  MassTransferCoefficient massTransferCoefficient if not useBetaInput
    "Model calculating the mass transfer coefficient";

  //
  // Definition of variables
  //
  Real drivingForce
    "Driving force for mass transfer: Either dp or dx";
  Real drivingForce_status
    "Status of mass transfer according to driving force and flap valve 
    behaivior: > 0.75, if mass transfer is activated; < 0.25, if mass transfer is 
    decactivated; otherwise, smooth transiton regime";

  Real activated = activated_internal * drivingForce_status
    "Current status of mass transfer accounting for driving force and external 
    signal: > 0.75, if mass transfer is activated; < 0.25, if mass transfer is 
    decactivated; otherwise, smooth transiton regime";

protected
  Real drivingForce_internal
    "Internal value of driving force: Required to pass either pressure or
    loading difference in extended models";

  SorpLib.Components.MassTransfer.Records.FluidProperties fluidProperties
    "Fluid properties if needed for transport phenomena";

equation
  //
  // Assertions
  //
  assert(no_components == 1,
    "Mass transfer model is only valid for pure component adsorption!",
    level = AssertionLevel.error);

  //
  // Connectors
  //
  connect(beta_internal, beta_input);
  connect(beta_internal, massTransferCoefficient.beta);

  connect(T_adsorbate_internal, dummyT);
  connect(T_adsorptiveA_internal, dummyT);
  connect(T_adsorptiveB_internal, dummyT);
  connect(d_adsorptiveA_internal, dummyD);
  connect(d_adsorptiveB_internal, dummyD);
  connect(eta_adsorptiveA_internal, dummyEta);
  connect(eta_adsorptiveB_internal, dummyEta);

  //
  // Momentum balance
  //
  dp = port_a.p - port_b.p
    "Pressure drop dp required for flap valve behaiviour and/or pressure driven
     mass transfer";

  //
  // Mass balance
  //
  0 = port_a.m_flow + port_b.m_flow
    "Trivial equation: No change of mass";

  port_a.Xi_outflow = inStream(port_b.Xi_outflow)
    "Trivial equation: No change of transported substances";
  port_b.Xi_outflow = inStream(port_a.Xi_outflow)
    "Trivial equation: No change of transported substances";

  if avoid_events_activating then
    port_a.m_flow = (1 - SorpLib.Numerics.smoothTransition_noEvent(
      x=activated,
      transitionPoint=0.5,
      transitionLength=0.25,
      noDiff=noDiff)) * beta_internal * drivingForce
      "Mass flow rate depends on status of mass transfer";

  else
    port_a.m_flow = (1 - SorpLib.Numerics.smoothTransition(
      x=activated,
      transitionPoint=0.5,
      transitionLength=0.25,
      noDiff=noDiff)) * beta_internal * drivingForce
      "Mass flow rate depends on status of mass transfer";

  end if;

  //
  // Energy balance
  //
  port_a.h_outflow = inStream(port_b.h_outflow)
    "Trivial equation: No change of energy";
  port_b.h_outflow = inStream(port_a.h_outflow)
    "Trivial equation: No change of energy";

  //
  // Limit driving force and determine status of driving force
  //
  if isFlapValve then
    //
    // Flap valve: Driving force depends on flow direction
    //
    drivingForce = if isFlowAB then max(drivingForce_internal, 0)
      else min(drivingForce_internal, 0)
      "Flow from a->b: Values at port a must be greater than at port b; flow
      from b->a: vice versa";

    if isFlowAB then
      if avoid_events then
        drivingForce_status = (1 - SorpLib.Numerics.smoothTransition_noEvent(
          x=dp + offset_dp,
          transitionPoint=0,
          transitionLength=5,
          noDiff=noDiff))
          "Flow from a->b: Values at port a must be greater than at port b";

      else
        drivingForce_status = (1 - SorpLib.Numerics.smoothTransition(
          x=dp + offset_dp,
          transitionPoint=0,
          transitionLength=5,
          noDiff=noDiff))
          "Flow from a->b: Values at port a must be greater than at port b";

      end if;

    else
      if avoid_events then
        drivingForce_status = SorpLib.Numerics.smoothTransition_noEvent(
          x=dp + offset_dp,
          transitionPoint=0,
          transitionLength=5,
          noDiff=noDiff)
          "Flow from b->a: Values at port b must be greater than at port a";

      else
        drivingForce_status = SorpLib.Numerics.smoothTransition(
          x=dp + offset_dp,
          transitionPoint=0,
          transitionLength=5,
          noDiff=noDiff)
          "Flow from b->a: Values at port b must be greater than at port a";

      end if;
    end if;

  else
    //
    // No flap valve behaiviour: Use driving force as given and set status to 1
    //
    drivingForce = drivingForce_internal
      "Driving force depends on kind of mass tranfer: Pressure- or loading-driven 
      mass transfer";
    drivingForce_status = 1
      "No flap valve behaiviour: Mass transfer is always activated w.r.t. driving 
      force";

  end if;

  //
  // Calculate fluid properties if required for mass transport phenomena
  //
  if calculateFluidProperties and not useBetaInput then
    //
    // Calculate fluid properties
    //
    fluidProperties.T_adsorbate = T_adsorbate_internal
      "Temperature at the adsorbate state";

    if fluidPropertyPosition==
      SorpLib.Choices.MassTransferFluidProperties.PortAInlet then
      fluidProperties.p_adsorptive = port_a.p
        "Pressure at the adsorptive state";
      fluidProperties.T_adsorptive = T_adsorptiveA_internal
        "Temperature at the adsorptive state";
      fluidProperties.d_adsorptive = d_adsorptiveA_internal
        "Density at the adsorptive state";
      fluidProperties.eta_adsorptive = eta_adsorptiveA_internal
        "Dynamic viscosity at the adsorptive state";

    elseif fluidPropertyPosition==
      SorpLib.Choices.MassTransferFluidProperties.PortBInlet then
      fluidProperties.p_adsorptive = port_b.p
        "Pressure at the adsorptive state";
      fluidProperties.T_adsorptive = T_adsorptiveB_internal
        "Temperature at the adsorptive state";
      fluidProperties.d_adsorptive = d_adsorptiveB_internal
        "Density at the adsorptive state";
      fluidProperties.eta_adsorptive = eta_adsorptiveB_internal
        "Dynamic viscosity at the adsorptive state";

    elseif fluidPropertyPosition==
      SorpLib.Choices.MassTransferFluidProperties.ActualInlet then
      if avoid_events then
        fluidProperties.p_adsorptive = SorpLib.Numerics.regStep_noEvent(
          x=port_a.m_flow,
          y1=port_a.p,
          y2=port_b.p,
          x_small=m_flow_small)
          "Pressure at the adsorptive state";
        fluidProperties.T_adsorptive = SorpLib.Numerics.regStep_noEvent(
          x=port_a.m_flow,
          y1=T_adsorptiveA_internal,
          y2=T_adsorptiveB_internal,
          x_small=m_flow_small)
          "Temperature at the adsorptive state";
        fluidProperties.d_adsorptive = SorpLib.Numerics.regStep_noEvent(
          x=port_a.m_flow,
          y1=d_adsorptiveA_internal,
          y2=d_adsorptiveB_internal,
          x_small=m_flow_small)
          "Density at the adsorptive state";
        fluidProperties.eta_adsorptive = SorpLib.Numerics.regStep_noEvent(
          x=port_a.m_flow,
          y1=eta_adsorptiveA_internal,
          y2=eta_adsorptiveB_internal,
          x_small=m_flow_small)
          "Dynamic viscosity at the adsorptive state";

     else
        fluidProperties.p_adsorptive = SorpLib.Numerics.regStep(
          x=port_a.m_flow,
          y1=port_a.p,
          y2=port_b.p,
          x_small=m_flow_small)
          "Pressure at the adsorptive state";
        fluidProperties.T_adsorptive = SorpLib.Numerics.regStep(
          x=port_a.m_flow,
          y1=T_adsorptiveA_internal,
          y2=T_adsorptiveB_internal,
          x_small=m_flow_small)
          "Temperature at the adsorptive state";
        fluidProperties.d_adsorptive = SorpLib.Numerics.regStep(
          x=port_a.m_flow,
          y1=d_adsorptiveA_internal,
          y2=d_adsorptiveB_internal,
          x_small=m_flow_small)
          "Density at the adsorptive state";
        fluidProperties.eta_adsorptive = SorpLib.Numerics.regStep(
          x=port_a.m_flow,
          y1=eta_adsorptiveA_internal,
          y2=eta_adsorptiveB_internal,
          x_small=m_flow_small)
          "Dynamic viscosity at the adsorptive state";

      end if;

    else
      fluidProperties.p_adsorptive =
        (port_a.p+port_b.p)/2
        "Pressure at the adsorptive state";
      fluidProperties.T_adsorptive =
        (T_adsorptiveA_internal+T_adsorptiveB_internal)/2
        "Temperature at the adsorptive state";
      fluidProperties.d_adsorptive =
        (d_adsorptiveA_internal+d_adsorptiveB_internal)/2
          "Density at the adsorptive state";
      fluidProperties.eta_adsorptive =
        (eta_adsorptiveA_internal+eta_adsorptiveB_internal)/2
          "Dynamic viscosity at the adsorptive state";

    end if;

  else
    //
    // Fluid properties are not needed: Set dummy values
    //
    fluidProperties.T_adsorbate = 0
      "Pressure at the adsorbate state";
    fluidProperties.p_adsorptive = 0
      "Pressure at the adsorptive state";
    fluidProperties.T_adsorptive = 0
      "Temperature at the adsorptive state";
    fluidProperties.d_adsorptive = 0
      "Density at the adsorptive state";
    fluidProperties.eta_adsorptive = 0
      "Dynamic viscosity at the adsorptive state";

  end if;

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial model is the base model for all mass transfers of pure component
adsorption. It defines fundamental parameters and variables required by all mass
transfers. Models that inherit properties from this partial model have to redeclare 
the fluid ports. Moreover, these models must redeclare and constrain the model 
calculating the mass transfer coefficient and area. In this context, records may 
be added that containg geometry and fluid property data. Furtheremore, the driving 
force must be defined.
<br/><br/>
The following variables must be specified in the model that inherit properties:
</p>
<ul>
  <li>
  Driving force <i>drivingForce_internal</i>.
  </li>
  <li>
  Temperature at adsorbate state <i>T_adsorbate_internal</i>.
  </li>
  <li>
  Instreaming adsorptive temperature at port a <i>T_adsorptiveA_internal</i>.
  </li>
  <li>
  Instreaming adsorptive temperature at port b <i>T_adsorptiveA_internal</i>.
  </li>
  <li>
  Instreaming adsorptive density at port a <i>d_adsorptiveA_internal</i>.
  </li>
  <li>
  Instreaming adsorptive density at port b <i>d_adsorptiveB_internal</i>.
  </li>
  <li>
  Instreaming adsorptive dynamic viscosity at port a <i>eta_adsorptiveB_internal</i>.
  </li>
  <li>
  Instreaming adsorptive dynamic viscosity at port b <i>eta_adsorptiveA_internal</i>.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  January 24, 2024, by Mirko Engelpracht:<br/>
  Minor revisions and documentation.
  </li>
  <li>
  January 18, 2021, by Mirko Engelpracht:<br/>
  Major revisions (e.g., object-oriented approach) after restructuring of the library.
  </li>
  <li>
  November 30, 2017, by Uwe Bau:<br/>
  Tidy up implementation and enhance documentation for publication of library.
  </li>
</ul>
</html>"));
end PartialPureMassTransfer;
