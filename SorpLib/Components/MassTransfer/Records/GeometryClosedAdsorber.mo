within SorpLib.Components.MassTransfer.Records;
record GeometryClosedAdsorber
  "This record contains the geometry required for mass transfer coefficients of closed adsorbers"
  extends SorpLib.Components.HeatExchanger.Records.GeometryClosedAdsorber;

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This record contains geometric parameters required by models calculating the mass
transfer coefficients in closed adsorbers.
</p>
</html>", revisions="<html>
<ul>
  <li>
  January 24, 2024, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end GeometryClosedAdsorber;
