within SorpLib.Components.MassTransfer.Records;
record FluidProperties
  "This record contains fluid properties required for heat transfer coefficients"
  extends Modelica.Icons.Record;

  //
  // Definition of variables describing fluid properties
  //
  Modelica.Units.SI.Temperature T_adsorbate
    "Temperature at the adsorbate state";

  Modelica.Units.SI.Pressure p_adsorptive
    "Pressure at the adsorptive state";
  Modelica.Units.SI.Temperature T_adsorptive
    "Temperature at the adsorptive state";
  Modelica.Units.SI.Density d_adsorptive
    "Density at the adsorptive state";
  Modelica.Units.SI.DynamicViscosity eta_adsorptive
    "Dynamic viscosity at the adsorptive state";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This record contains fluid properties required for mass transfer coefficients.
</p>
</html>", revisions="<html>
<ul>
  <li>
  January 24, 2024, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end FluidProperties;
