within SorpLib.Components.MassTransfer.MassTransferCoefficientCorrelations.ClosedAdsorber;
package LoadingDriven "Correlations for loading-driven mass transfer coefficients describing the mass transfer within closed adsorbers"
extends Modelica.Icons.FunctionsPackage;


  annotation (Documentation(info="<html>
<p>
This package contains correlations for mass transfer coefficients describing
the loading-driven mass transfer within closed adsorbers:
</p>
<ul>
  <li>
  <a href=\"Modelica://SorpLib.Components.MassTransfer.MassTransferCoefficientCorrelations.ClosedAdsorber.LoadingDriven.ConstantCoefficient\">ConstantCoefficient</a>: 
  Generic mass transfer correlation with constant mass transfer coefficient.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.MassTransfer.MassTransferCoefficientCorrelations.ClosedAdsorber.LoadingDriven.ConstantSpecificCoefficient\">ConstantSpecificCoefficient</a>: 
  Generic mass transfer correlation with constant product of specific mass 
  transfer coefficient and area
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.MassTransfer.MassTransferCoefficientCorrelations.ClosedAdsorber.LoadingDriven.Glueckauf\">Glueckauf</a>: 
  Mass transfer correlation according to Glueckauf.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.MassTransfer.MassTransferCoefficientCorrelations.ClosedAdsorber.LoadingDriven.GlueckaufArrhenius\">GlueckaufArrhenius</a>: 
  Mass transfer correlation according to Glueckauf with temperature dependancy.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end LoadingDriven;
