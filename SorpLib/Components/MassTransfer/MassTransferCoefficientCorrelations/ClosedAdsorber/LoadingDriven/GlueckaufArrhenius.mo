within SorpLib.Components.MassTransfer.MassTransferCoefficientCorrelations.ClosedAdsorber.LoadingDriven;
model GlueckaufArrhenius
  "Mass transfer correlation describing mass transfer according to Glueckauf with a temperature depandancy"
  extends
    SorpLib.Components.MassTransfer.BaseClasses.PartialMassTransferCoefficientClosedAdsorberDX(
     final computeTransportProperties=false,
     final avoid_events=false);

  //
  // Definition of parameters
  //
  parameter Modelica.Units.SI.Radius r_sorbent = geometry.d_particle/2
    "Radius of sorbent material"
    annotation (Dialog(tab="General", group="Mass Transfer"));
  parameter Modelica.Units.SI.Mass m_sorbent = 2.5
    "Mass of dry sorbent material"
    annotation (Dialog(tab="General", group="Mass Transfer"));
  parameter Real D(unit="m2/s") = 1e-10
    "Diffusion coefficient"
    annotation (Dialog(tab="General", group="Mass Transfer"));
  parameter Modelica.Units.SI.SpecificEnergy E_activation = 2.33e6
    "Activation energy"
    annotation (Dialog(tab="General", group="Mass Transfer"));

  parameter Modelica.Units.SI.MolarMass M_adsorptive = 0.001801528
    "Molar mass of the adsorptive"
    annotation (Dialog(tab="General", group="Mass Transfer"));

equation
  beta = geometry.no_hydraulicParallelTubes / geometry.no_sorbentVolumes *
    15 * m_sorbent * D / r_sorbent^2 * exp(-E_activation /
    ((Modelica.Constants.R/M_adsorptive) * fluidProperties.T_adsorbate))
    "Mass transfer coefficient";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This mass transfer model calculates the product of mass transfer coefficient and
area according to Glueckauf.
</p>

<h4>Main equations</h4>
<p>
The product of mass transfer coefficient and area <i>&beta;A</i> is calculated 
according to:
</p>
<pre>
    &beta;A = no<sub>parallel flows</sub> / no<sub>sorbent volumes</sub> * 15 * D / r<sub>particle</sub>^2 * m<sub>sorbent</sub> * <strong>exp</strong>(-E<sub>activation</sub> / (R / M<sub>adsorptive</sub> * T<sub>adsorbate</sub>));
</pre>
<p>
Herein, <i>D</i> is the diffusion coefficient, <i>r<sub>particle</sub></i> is the 
radius of the particle, and <i>m<sub>sorbent</sub></i> is the sorbent mass,
<i>E<sub>activation</sub></i> is the activation energy, <i>M<sub>adsorptive</sub></i>
is the molar mass of the adsorptive, <i>T<sub>adsorbate</sub></i> is the adsorbate 
temperature.
<br/><br/>
The product is enlarged by the number of hydrualic parallel flows 
<i>no<sub>parallel flows</sub></i> to account for parallel flows modeled by just 
one flow. The area <i>A</i> accounts for the disretization due to the division by
the discretization number of the sorbent volumes. Accordingly, the total product 
of mass transfer coefficient and area <i>&beta;A</i> describes on heat exchanger
tube.
</p>
  
<h4>Typical use</h4>
<p>
This mass transfer correlation model is typically used to describe the mass transfer
within closed adsorbers.
</p>

<h4>References</h4>
<ul>
  <li>
  Glueckauf, E. (1955). Theory of Chromatography. Part 10: Formulae for Diffusion into Spheres and their Application to Chromatography. Transactions of the Faraday Society, 51(11), 1540-1551.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  January 25, 2024, by Mirko Engelpracht:<br/>
  Minor revisions and documentation.
  </li>
  <li>
  January 18, 2021, by Mirko Engelpracht:<br/>
  Minor revisions after restructuring of the library.
  </li>
  <li>
  November 30, 2017, by Uwe Bau:<br/>
  Tidy up implementation and enhance documentation for publication of library.
  </li>
</ul>
</html>"));
end GlueckaufArrhenius;
