within SorpLib.Components.MassTransfer.MassTransferCoefficientCorrelations.ClosedAdsorber.PressureDriven;
model KnudsenDiffusion
  "Mass transfer correlation describing mass transfer through a particle due to Knudsen diffusion"
  extends
    SorpLib.Components.MassTransfer.BaseClasses.PartialMassTransferCoefficientClosedAdsorberDP(
     final computeTransportProperties=true,
     final avoid_events=false);

  //
  // Definition of parameters
  //
  parameter Real f_correction = 0.5 * geometry.no_particles *
    geometry.V_particle / ((4/3) * Modelica.Constants.pi * (d_pore/2)^3)
    "Fitting factor to account for a whole bed (i.e., can be considered as number
    of porese"
    annotation (Dialog(tab="General", group="Mass Transfer"));
  parameter Real tortuosity(final unit="m") = 5^1.7
    "Tortuosity for Knudsen diffusion"
    annotation (Dialog(tab="General", group="Mass Transfer"));

  parameter Modelica.Units.SI.Diameter d_particle = geometry.d_particle
    "Diameter of one particle"
    annotation (Dialog(tab="General", group="Mass Transfer"));
  parameter Modelica.Units.SI.Diameter d_pore = 25e-9
    "Average diameter of the pores"
    annotation (Dialog(tab="General", group="Mass Transfer"));

  parameter Modelica.Units.SI.Area A_pore=
    Modelica.Constants.pi/4 * d_pore^2
    "Cross-sectional area of one pore"
    annotation (Dialog(tab="General", group="Mass Transfer"));

  parameter Modelica.Units.SI.MolarMass M_adsorptive = 0.001801528
    "Molar mass of the adsorptive"
    annotation (Dialog(tab="General", group="Mass Transfer"));

equation
  beta = geometry.no_hydraulicParallelTubes / geometry.no_sorbentVolumes *
    f_correction * 4/3 * d_pore / tortuosity *  A_pore / (d_particle/2) *
    sqrt(M_adsorptive / (2 * Modelica.Constants.pi * Modelica.Constants.R *
    fluidProperties.T_adsorptive))
    "Mass transfer coefficient";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This mass transfer model calculates the product of mass transfer coefficient and
area describing the mass transfer through a particle due to Knudsen diffusion.
</p>

<h4>Main equations</h4>
<p>
The product of mass transfer coefficient and area <i>&beta;A</i> is calculated 
according to:
</p>
<pre>
    &beta;A = no<sub>parallel flows</sub> / no<sub>sorbent volumes</sub> * f<sub>correction</sub> * 4/3 * d<sub>pore</sub> / &theta; * A<sub>pore</sub> / (d<sub>particle</sub>/2) * <strong>sqrt</strong>(M<sub>adsorptive</sub> / (2 * &pi; * R * T<sub>adsorptive</sub>));
</pre> 
<p>
Herein, <i>f<sub>correction</sub></i> is a correction factor, <i>d<sub>pore</sub></i>
is the average pore diameter, <i>A<sub>pore</sub></i> is the average cross-sectional
area of one pore, <i>d<sub>particle</sub></i> is the average particle diameter,
<i>&theta;</i> is the tortuosity factor, <i>M<sub>adsorptive</sub></i> is the molar 
mass, <i>R</i> is the ideal gas constant, and <i>T<sub>adsorptive</sub></i> is the 
adsorptive temperature.
<br/><br/>
The product is enlarged by the number of hydrualic parallel flows 
<i>no<sub>parallel flows</sub></i> to account for parallel flows modeled by just 
one flow. The area <i>A</i> accounts for the disretization due to the division by
the discretization number of the sorbent volumes. Accordingly, the total product 
of mass transfer coefficient and area <i>&beta;A</i> describes on heat exchanger
tube.
</p>
  
<h4>Typical use</h4>
<p>
This mass transfer correlation model is typically used to describe the mass transfer
through a particle that is dominated by Knudsen diffusion.
</p>

<h4>References</h4>
<ul>
  <li>
  Bathen, D. and Breitbach, M. (2001). Adsorptionstechnik (in German), 1st Edition, ISBN 3-540-41908-X, Springer-Verlag Berlin Heidelberg New York.
  </li>
  <li>
  Kast, W. (1998). Adsorption aus der Gasphase: Ingenieurwissenschaftliche Grundlagen und technische Verfahren (in German). VCH Verlagsgesellschaft, Weinheim, Basel, Cambridge, New York. DOI:  https://doi.org/10.1002/bbpc.19900940122.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  January 24, 2024, by Mirko Engelpracht:<br/>
  Minor revisions and documentation.
  </li>
  <li>
  June 06, 2021, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end KnudsenDiffusion;
