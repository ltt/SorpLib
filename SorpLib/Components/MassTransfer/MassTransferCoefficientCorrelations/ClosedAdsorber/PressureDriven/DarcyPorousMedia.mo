within SorpLib.Components.MassTransfer.MassTransferCoefficientCorrelations.ClosedAdsorber.PressureDriven;
model DarcyPorousMedia
  "Mass transfer correlation describing mass transfer through a packed bed"
  extends
    SorpLib.Components.MassTransfer.BaseClasses.PartialMassTransferCoefficientClosedAdsorberDP(
     final computeTransportProperties=true,
     final avoid_events=false);

  //
  // Definition of parameters
  //
  parameter Modelica.Units.SI.Length l_bed = 1
    "Total length the adsorptive has to flow through the bed"
    annotation (Dialog(tab="General", group="Mass Transfer"));
  parameter Modelica.Units.SI.Area A_bed = Modelica.Constants.pi/4 * 0.01^2
    "Cross-sectional area of the bed that the adsorptive has to pass"
    annotation (Dialog(tab="General", group="Mass Transfer"));
  parameter Real permeability(final unit="m2") = 0.32
    "Permeability of the bed"
    annotation (Dialog(tab="General", group="Mass Transfer"));

equation
  beta = geometry.no_hydraulicParallelTubes / geometry.no_sorbentVolumes *
    (permeability / fluidProperties.eta_adsorptive) * (2 / l_bed) * A_bed *
    fluidProperties.d_adsorptive
    "Mass transfer coefficient";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This mass transfer model calculates the product of mass transfer coefficient and
area describing the convective flow throug porous media according to Darcy's law.
</p>

<h4>Main equations</h4>
<p>
The product of mass transfer coefficient and area <i>&beta;A</i> is calculated 
according to:
</p>
<pre>
    &beta;A = no<sub>parallel flows</sub> / no<sub>sorbent volumes</sub> * &Kappa; / &eta; * A<sub>cross,bed</sub> * &rho; * l<sub>bed</sub> / 2;
</pre>
<p>
Herein, <i>&Kappa;</i> is the permeability of the bed, <i>A<sub>cross,bed</sub></i> 
is the cross-sectional area of the bed, <i>l<sub>bed</sub></i> is the total length 
of the bed, <i>&rho;</i> is the densiy of the adsorptive, and <i>&eta;</i> is the 
dynamic viscosity of the adsorptive.
<br/><br/>
The product is enlarged by the number of hydrualic parallel flows 
<i>no<sub>parallel flows</sub></i> to account for parallel flows modeled by just 
one flow. The area <i>A</i> accounts for the disretization due to the division by
the discretization number of the sorbent volumes. Accordingly, the total product 
of mass transfer coefficient and area <i>&beta;A</i> describes on heat exchanger
tube.
</p>
  
<h4>Typical use</h4>
<p>
This mass transfer correlation model is typically used to describe the mass transfer
through a packed bed.
</p>

<h4>References</h4>
<ul>
  <li>
  DBejan, A. (2013). Convection heat transfer. 4th ed. Hoboken, N.J.: Wiley. ISBN: 1118519760.
  </li>
  <li>
  Darcy, H.P.G. (1856). Les Fontaines Publiques de la villa de Dijon. Paris.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  January 24, 2024, by Mirko Engelpracht:<br/>
  Minor revisions and documentation.
  </li>
  <li>
  January 18, 2021, by Mirko Engelpracht:<br/>
  Minor revisions after restructuring of the library.
  </li>
  <li>
  November 30, 2017, by Uwe Bau:<br/>
  Tidy up implementation and enhance documentation for publication of library.
  </li>
</ul>
</html>"));
end DarcyPorousMedia;
