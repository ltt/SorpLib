within SorpLib.Components.MassTransfer.MassTransferCoefficientCorrelations.ClosedAdsorber.PressureDriven;
model ConstantSpecificCoefficient
  "Generic mass transfer correlation with constant product of specific mass transfer coefficient and area"
  extends
    SorpLib.Components.MassTransfer.BaseClasses.PartialMassTransferCoefficientClosedAdsorberDP(
     final computeTransportProperties=false,
     final avoid_events=false);

  //
  // Definition of parameters
  //
  parameter Real constantSpecificCoefficient(final unit="m.s/m2")= 1e-7
    "Constant specific mass transfer coefficient for pressure-driven mass transfer"
    annotation (Dialog(tab="General", group="Mass Transfer"));
  parameter Modelica.Units.SI.Area A = 1
    "Mass transfer area"
    annotation (Dialog(tab="General", group="Mass Transfer"));

equation
  beta = geometry.no_hydraulicParallelTubes * constantSpecificCoefficient * A /
    geometry.no_sorbentVolumes
    "Mass transfer coefficient";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This simple transfer model calculates the product of mass transfer coefficient and
area assuming a constant mass transfer coefficient and area.
</p>

<h4>Main equations</h4>
<p>
The product of mass transfer coefficient and area <i>&beta;A</i> is calculated 
assuming a constant mass transfer coefficient <i>&beta;</i> and area <i>A</i>:
</p>
<pre>
    &beta;A = no<sub>parallel flows</sub> * &beta; * A / no<sub>sorbent volumes</sub> = const.;
</pre>
<p>
The product is enlarged by the number of hydrualic parallel flows 
<i>no<sub>parallel flows</sub></i> to account for parallel flows modeled by just 
one flow. The area <i>A</i> accounts for the disretization due to the division by
the discretization number of the sorbent volumes. Accordingly, the total product 
of mass transfer coefficient and area <i>&beta;A</i> describes on heat exchanger
tube.
</p>
  
<h4>Typical use</h4>
<p>
This simple mass transfer correlation model is typically used if the mass transfer
conditions do not greatly change and a very simple models is required.
</p>
</html>", revisions="<html>
<ul>
  <li>
  January 24, 2024, by Mirko Engelpracht:<br/>
  Minor revisions and documentation.
  </li>
  <li>
  January 18, 2021, by Mirko Engelpracht:<br/>
  Minor revisions after restructuring of the library.
  </li>
  <li>
  November 30, 2017, by Uwe Bau:<br/>
  Tidy up implementation and enhance documentation for publication of library.
  </li>
</ul>
</html>"));
end ConstantSpecificCoefficient;
