within SorpLib.Components.MassTransfer.MassTransferCoefficientCorrelations.ClosedAdsorber.PressureDriven;
model DarcyPackedBedSpheresKnudsenDiffusionPoiseuilleFlow
  "Mass transfer correlation describing mass transfer through a packed bed followed trough a particle due to Knudsen diffusion and Poiseuille flow"
  extends
    SorpLib.Components.MassTransfer.BaseClasses.PartialMassTransferCoefficientClosedAdsorberDP(
     final computeTransportProperties=true,
     final avoid_events=false);

  //
  // Definition of parameters
  //
  parameter Real f_correction_bed = 1
    "Fitting factor for the bed resistance"
    annotation (Dialog(tab="General", group="Mass Transfer"));
  parameter Real f_particle = 0.5 * geometry.no_particles *
    geometry.V_particle / ((4/3) * Modelica.Constants.pi * (d_pore/2)^3)
    "Fitting factor for the particle resistance"
    annotation (Dialog(tab="General", group="Mass Transfer"));

  parameter Modelica.Units.SI.Length l_bed = 0.025
    "Total length the adsorptive has to flow through the bed"
    annotation (Dialog(tab="General", group="Mass Transfer"));
  parameter Modelica.Units.SI.Area A_bed = 0.5
    "Cross-sectional area of the bed that the adsorptive has to pass"
    annotation (Dialog(tab="General", group="Mass Transfer"));
  parameter Real psi_bed(final unit="1") = geometry.psi_particles
    "Void fraction of the bed"
    annotation (Dialog(tab="General", group="Mass Transfer"));
  final parameter Real permeability(final unit="m2")=
    (d_particle^2 * psi_bed^3) / (150 * (1 - psi_bed)^2)
    "Permeability of the bed"
    annotation (Dialog(tab="General", group="Mass Transfer"));


  parameter Real tortuosity_Knudsen(final unit="m") = 5^1.7
    "Tortuosity for Knudsen diffusion"
    annotation (Dialog(tab="General", group="Mass Transfer"));
  parameter Real tortuosity_Poiseuille(final unit="m") = 5^2.6
    "Tortuosity for Poiseuille flow"
    annotation (Dialog(tab="General", group="Mass Transfer"));

  parameter Modelica.Units.SI.Diameter d_particle = geometry.d_particle
    "Diameter of one particle"
    annotation (Dialog(tab="General", group="Mass Transfer"));
  parameter Modelica.Units.SI.Diameter d_pore = 25e-9
    "Average diameter of the pores"
    annotation (Dialog(tab="General", group="Mass Transfer"));

  parameter Modelica.Units.SI.Area A_pore=
    Modelica.Constants.pi/4 * d_pore^2
    "Cross-sectional area of one pore"
    annotation (Dialog(tab="General", group="Mass Transfer"));

  parameter Modelica.Units.SI.MolarMass M_adsorptive = 0.001801528
    "Molar mass of the adsorptive"
    annotation (Dialog(tab="General", group="Mass Transfer"));

  //
  // Definition of variables
  //
  Real R_bed
    "Mass transport resistance for the bed (i.e., Darcy's law)";
  Real R_particle
    "Mass transport resistance for the particle";

  Real D_Knudsen
    "Diffusion coefficient according to Knudsen diffusin";
  Real D_Poiseuille
    "Diffusion coefficient according to Poiseuille flow";

equation
  //
  // Calculate diffusion coefficients
  //
  R_bed = 1 / (f_correction_bed * (permeability / fluidProperties.eta_adsorptive) *
    (2 / l_bed) * A_bed * fluidProperties.d_adsorptive)
    "Mass transport resistance for the bed (i.e., Darcy's law)";
  R_particle = 1 / (f_particle * (D_Knudsen + D_Poiseuille))
    "Mass transport resistance for the particle";

  D_Knudsen = 4/3 * d_pore / tortuosity_Knudsen *  A_pore / (d_particle/2) *
    sqrt(M_adsorptive / (2 * Modelica.Constants.pi * Modelica.Constants.R *
    fluidProperties.T_adsorptive))
    "Diffusion coefficient according to Knudsen diffusin";
  D_Poiseuille =  1/32 * fluidProperties.p_adsorptive * d_pore^2 /
    fluidProperties.eta_adsorptive / tortuosity_Poiseuille * A_pore / (d_particle/2) *
    M_adsorptive / (Modelica.Constants.R * fluidProperties.T_adsorptive)
    "Diffusion coefficient according to Poiseuille flow";

  //
  // Calculate overall mass transfer coefficient
  //
  beta = geometry.no_hydraulicParallelTubes / geometry.no_sorbentVolumes /
    (R_bed + R_particle)
    "Mass transfer coefficient";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This mass transfer model calculates the product of mass transfer coefficient and
area describing the mass transfer through a packed bed according to Darcy's law
followed by a mass transfer through a particle due to a combination of Knudsen 
diffusion and Poiseuille flow.
</p>

<h4>Main equations</h4>
<p>
The product of mass transfer coefficient and area <i>&beta;A</i> results from a
serial connection of mass transfer through the bed <i>D<sub>bed</sub></i> and the
particle <i>D<sub>particle</sub></i>, with the latter being a parallel connection 
of the Knudsen diffusion <i>D<sub>Knudsen</sub></i> and Poiseuille flow 
<i>D<sub>Poiseuille</sub></i>:
</p>
<pre>
    &beta;A = no<sub>parallel flows</sub> / no<sub>sorbent volumes</sub> * 1 / [1/(f<sub>correction,bed</sub> * D<sub>bed</sub>) + 1 / (f<sub>correction,particle</sub> * (D<sub>Knudsen</sub> + D<sub>Poiseuille</sub>))];
</pre> 
<p>
with:
</p>
<pre>
    D<sub>bed</sub> = &Kappa; / &eta; * A<sub>cross,bed</sub> * &rho; * l<sub>bed</sub> / 2;

    D<sub>Knudsen</sub> = 4/3 * d<sub>pore</sub> / &theta; * A<sub>pore</sub> / (d<sub>particle</sub>/2) * <strong>sqrt</strong>(M<sub>adsorptive</sub> / (2 * &pi; * R * T<sub>adsorptive</sub>));

    D<sub>Poiseuille</sub> = 1/32 * p<sub>adsorptive</sub> * d<sub>pore</sub>^2  / &eta; / &theta; * A<sub>pore</sub> / (d<sub>particle</sub>/2) * M<sub>adsorptive</sub> / (R * T<sub>adsorptive</sub>);
</pre> 
<p>
and:
</p>
<pre>
    &Kappa;= (d<sub>particle</sub>^2 * &psi;<sub>bed</sub>^3) / (150 * (1 - &psi;<sub>bed</sub>)^2);
</pre>
<p>
Herein, <i>f<sub>correction,i</sub></i> is a correction factor, <i>d<sub>pore</sub></i>
is the average pore diameter, <i>A<sub>pore</sub></i> is the average cross-sectional
area of one pore, <i>d<sub>particle</sub></i> is the average particle diameter, 
<i>&Kappa;</i> is the permeability of the bed, <i>A<sub>cross,bed</sub></i> 
is the cross-sectional area of the bed, <i>l<sub>bed</sub></i> is the total length 
of the bed, <i>&theta;<sub>Knudsen</sub></i> is the tortuosity factor for Knudsen diffucsion, 
<i>&theta;<sub>Poiseuille</sub></i> is the tortuosity factor for Knudsen diffucsion,
<i>M<sub>adsorptive</sub></i> is the molar mass, <i>R</i> is the ideal gas constant, 
<i>p<sub>adsorptive</sub></i> is the adsorptive pressure, <i>T<sub>adsorptive</sub></i> 
is the adsorptive temperature, <i>&rho;</i> is the densiy of the adsorptive, and <i>&eta;</i> 
is the dynamic viscosity of the adsorptive
<br/><br/>
The product is enlarged by the number of hydrualic parallel flows 
<i>no<sub>parallel flows</sub></i> to account for parallel flows modeled by just 
one flow. The area <i>A</i> accounts for the disretization due to the division by
the discretization number of the sorbent volumes. Accordingly, the total product 
of mass transfer coefficient and area <i>&beta;A</i> describes on heat exchanger
tube.
</p>
  
<h4>Typical use</h4>
<p>
This mass transfer correlation model is typically used to describe the mass transfer
through a packed bed according to Darcy's law followed by a mass transfer through a 
particle due to a combination of Knudsen diffusion and Poiseuille flow.
</p>

<h4>References</h4>
<ul>
  <li>
  Bathen, D. and Breitbach, M. (2001). Adsorptionstechnik (in German), 1st Edition, ISBN 3-540-41908-X, Springer-Verlag Berlin Heidelberg New York.
  </li>
  <li>
  Kast, W. (1998). Adsorption aus der Gasphase: Ingenieurwissenschaftliche Grundlagen und technische Verfahren (in German). VCH Verlagsgesellschaft, Weinheim, Basel, Cambridge, New York. DOI:  https://doi.org/10.1002/bbpc.19900940122.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  January 24, 2024, by Mirko Engelpracht:<br/>
  Minor revisions and documentation.
  </li>
  <li>
  June 06, 2021, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end DarcyPackedBedSpheresKnudsenDiffusionPoiseuilleFlow;
