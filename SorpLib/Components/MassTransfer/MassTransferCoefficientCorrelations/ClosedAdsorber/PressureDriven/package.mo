within SorpLib.Components.MassTransfer.MassTransferCoefficientCorrelations.ClosedAdsorber;
package PressureDriven "Correlations for pressure-driven mass transfer coefficients describing the mass transfer within closed adsorbers"
extends Modelica.Icons.FunctionsPackage;


  annotation (Documentation(info="<html>
<p>
This package contains correlations for mass transfer coefficients describing
the pressure-driven mass transfer within closed adsorbers:
</p>
<ul>
  <li>
  <a href=\"Modelica://SorpLib.Components.MassTransfer.MassTransferCoefficientCorrelations.ClosedAdsorber.PressureDriven.ConstantCoefficient\">ConstantCoefficient</a>: 
  Generic mass transfer correlation with constant mass transfer coefficient.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.MassTransfer.MassTransferCoefficientCorrelations.ClosedAdsorber.PressureDriven.ConstantSpecificCoefficient\">ConstantSpecificCoefficient</a>: 
  Generic mass transfer correlation with constant product of specific mass 
  transfer coefficient and area
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.MassTransfer.MassTransferCoefficientCorrelations.ClosedAdsorber.PressureDriven.DarcyPorousMedia\">DarcyPorousMedia</a>: 
  Mass transfer correlation describing the mass transfer through a packed bed.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.MassTransfer.MassTransferCoefficientCorrelations.ClosedAdsorber.PressureDriven.DarcyPackedBedSpheres\">DarcyPackedBedSpheres</a>: 
  Mass transfer correlation describing the mass transfer through a packed bed.
  with spherical particles.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.MassTransfer.MassTransferCoefficientCorrelations.ClosedAdsorber.PressureDriven.KnudsenDiffusion\">KnudsenDiffusion</a>: 
  Mass transfer correlation describing the mass transfer through a particle due
  to Knudsen diffusion.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.MassTransfer.MassTransferCoefficientCorrelations.ClosedAdsorber.PressureDriven.PoiseuilleFlow\">PoiseuilleFlow</a>: 
  Mass transfer correlation describing the mass transfer through a particle due
  to a Poiseuille flow.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.MassTransfer.MassTransferCoefficientCorrelations.ClosedAdsorber.PressureDriven.KnudsenDiffusionPoiseuilleFlow\">KnudsenDiffusionPoiseuilleFlow</a>: 
  Mass transfer correlation describing the mass transfer through a particle due
  to Knudsen diffusion and a Poiseuille flow.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.MassTransfer.MassTransferCoefficientCorrelations.ClosedAdsorber.PressureDriven.DarcyPackedBedSpheresKnudsenDiffusionPoiseuilleFlow\">DarcyPackedBedSpheresKnudsenDiffusionPoiseuilleFlow</a>: 
  Mass transfer correlation describing the mass transfer through a packed bed
  with spherical particles and through the particles due to Knudsen diffusion and 
  a Poiseuille flow.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.MassTransfer.MassTransferCoefficientCorrelations.ClosedAdsorber.PressureDriven.LaminarConnections\">LaminarConnections</a>: 
  Mass transfer correlation describing the mass transfer as laminar flow through
  fittings.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end PressureDriven;
