within SorpLib.Components.MassTransfer.MassTransferCoefficientCorrelations.ClosedAdsorber.PressureDriven;
model LaminarConnections
  "Mass transfer correlation describing mass transfer through connections for the lamianr flow regime"
  extends
    SorpLib.Components.MassTransfer.BaseClasses.PartialMassTransferCoefficientClosedAdsorberDP(
     final computeTransportProperties=true,
     final avoid_events=false);

  //
  // Definition of parameters
  //
  parameter Real zeta_lam(min=1) = 1
    "Additional factor to account for nonideal connections"
    annotation (Dialog(tab="General", group="Mass Transfer"));

  parameter Modelica.Units.SI.Length l_connection = 0.5
    "Length of the connection"
    annotation (Dialog(tab="General", group="Mass Transfer"));
  parameter Modelica.Units.SI.Diameter d_connection = 0.04
    "Hydraulic diameter of the connection"
    annotation (Dialog(tab="General", group="Mass Transfer"));
  parameter Modelica.Units.SI.Area A_connection=
    Modelica.Constants.pi/4 * d_connection^2
    "Inner cross-sectional area of the connection"
    annotation (Dialog(tab="General", group="Mass Transfer"));

equation
  beta = A_connection^2 / (8 * Modelica.Constants.pi * l_connection) *
    fluidProperties.d_adsorptive / fluidProperties.eta_adsorptive *
    1/zeta_lam
    "Mass transfer coefficient";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This mass transfer model calculates the product of mass transfer coefficient and
area describing the mass transport through a connection for the laminar flow
regime.
</p>

<h4>Main equations</h4>
<p>
The product of mass transfer coefficient and area <i>&beta;A</i> is calculated 
according to:
</p>
<pre>
    &beta;A = no<sub>parallel flows</sub> / no<sub>sorbent volumes</sub> * A<sub>connection</sub>^2 / (8 * &pi; * l<sub>connection</sub>) * &rho / &eta; * 1/&zeta;;
</pre>
<p>
Herein, <i>A<sub>connection</sub></i> is the cross-sectional area of the connection,
<i>l<sub>connection</sub></i> is the total length of the connection, <i>&rho;</i> is
the densiy of the adsorptive, <i>&eta;</i> is the dynamic viscosity of the adsorptive,
and <i>&zeta;</i> is a loss factor.
</p>
  
<h4>Typical use</h4>
<p>
This mass transfer correlation model is typically used to describe the mass transfer
through connections, such as valves between the evaporator and adsorber.
</p>

<h4>References</h4>
<ul>
  <li>
  Lanzerath, F. and Bau, U. and Seiler, J. and Bardow, A. (2015). Optimal design of adsorption chillers based on a validated dynamic object-oriented model. Science and Technology for the Built Environment, 21(3), 248-257. DOI: https://doi.org/10.1080/10789669.2014.990337.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  January 24, 2024, by Mirko Engelpracht:<br/>
  Minor revisions and documentation.
  </li>
  <li>
  January 18, 2021, by Mirko Engelpracht:<br/>
  Minor revisions after restructuring of the library.
  </li>
  <li>
  November 30, 2017, by Uwe Bau:<br/>
  Tidy up implementation and enhance documentation for publication of library.
  </li>
</ul>
</html>"));
end LaminarConnections;
