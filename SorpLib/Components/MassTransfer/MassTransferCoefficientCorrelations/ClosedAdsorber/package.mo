within SorpLib.Components.MassTransfer.MassTransferCoefficientCorrelations;
package ClosedAdsorber "Correlations for mass transfer coefficients describing the mass transfer within closed adsorbers"
extends Modelica.Icons.FunctionsPackage;


  annotation (Documentation(info="<html>
<p>
This package contains correlations for mass transfer coefficients describing
the mass transfer within closed adsorbers. The correlations are either valid for
pressure- or loading-driven mass transfers:
</p>
<ul>
  <li>
  <a href=\"Modelica://SorpLib.Components.MassTransfer.MassTransferCoefficientCorrelations.ClosedAdsorber.PressureDriven\">PressureDriven</a>: 
  Correlations for pressure-driven mass transfers.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.MassTransfer.MassTransferCoefficientCorrelations.ClosedAdsorber.LoadingDriven\">LoadingDriven</a>: 
  Correlations for loading-driven mass transfers.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end ClosedAdsorber;
