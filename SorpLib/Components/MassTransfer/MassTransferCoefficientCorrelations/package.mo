within SorpLib.Components.MassTransfer;
package MassTransferCoefficientCorrelations "Package containing correlations for mass transfer coefficients"
extends Modelica.Icons.FunctionsPackage;

annotation (Documentation(info="<html>
<p>
This package contains correlations for mass transfer coefficients that can be used 
within mass transfer models. The correlations calculate the mass transfer coefficient
as a function of fluid property data, gemeotry, and flow regime. Correlations for mass
transfer coefficients are implemented for the following components:
</p>
<ul>
  <li>
  <a href=\"Modelica://SorpLib.Components.MassTransfer.MassTransferCoefficientCorrelations.ClosedAdsorber\">ClosedAdsorber</a>: 
  Correlations for closed adsorbers.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.MassTransfer.MassTransferCoefficientCorrelations.OpenAdsorber\">OpenAdsorber</a>: 
  Correlations for open adsorbers.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructering the library.
  </li>
</ul>
</html>"));
end MassTransferCoefficientCorrelations;
