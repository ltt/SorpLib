within SorpLib.Components.MassTransfer.MassTransferCoefficientCorrelations;
package OpenAdsorber "Correlations for mass transfer coefficients describing the mass transfer within open adsorbers"
extends Modelica.Icons.FunctionsPackage;

  annotation (Documentation(info="<html>
<p>
This package contains correlations for mass transfer coefficients describing
the mass transfer between gwithin open adsorbers.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end OpenAdsorber;
