within SorpLib.Components;
package Pumps "Pumps to move fluids"
  extends SorpLib.Icons.PumpsPackage;

  annotation (Documentation(info="<html>
<p>
This package contains pumps. Ready-to-use models are based on the Modelica 
Standard library (MSL). Pumps are used to prescripe a mass or volume flow rate
to a hydraulic fluid.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end Pumps;
