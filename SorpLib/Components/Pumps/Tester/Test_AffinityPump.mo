within SorpLib.Components.Pumps.Tester;
model Test_AffinityPump "Tester for the affinity pump"
  extends Modelica.Icons.Example;

  //
  // Definition of parameters regarding the medium
  //
  replaceable package Medium =
    Modelica.Media.Water.StandardWater
    constrainedby Modelica.Media.Interfaces.PartialMedium
    "Medium model of the (ideal) liquid"
    annotation (Dialog(tab="General", group="Medium"),
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

  //
  // Definition of boundary models
  //
  SorpLib.Basics.Sources.Fluids.LiquidSource fs_a(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    redeclare final package Medium = Medium)
    "Fluid source a"
    annotation (Placement(transformation(extent={{-70,-10},{-50,10}})));

  SorpLib.Basics.Sources.Fluids.LiquidSource fs_b(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    redeclare final package Medium = Medium)
    "Fluid source b"
    annotation (Placement(transformation(extent={{70,-10},{50,10}})));

  //
  // Definition of multi ports
  //
  SorpLib.Components.Pumps.AffinityPump pump(
    use_nInput=true,
    redeclare final package Medium = Medium) "Affinity pump"
    annotation (Placement(transformation(extent={{-40,-10},{-20,10}})));

  SorpLib.Components.Fittings.Resistors.LiquidTubeResistance pressureLoss(
    positionFluidProperties=SorpLib.Choices.ResistorFluidProperties.PortAInlet,
    frictionPressureLoss=true,
    fittingPressureLosss=false,
    redeclare model PressureLossModel =
        Fittings.PressureLossCorrelations.TubeInside.Blasius,
    redeclare final package Medium = Medium) "Pressure loss model"
    annotation (Placement(transformation(extent={{20,-10},{40,10}})));

  //
  // Definition of input signals
  //
protected
  Modelica.Blocks.Sources.Sine input_n(
    amplitude=50,
    f=1/250,
    offset=50)
    "Input signal for the rotational speed"
    annotation (Placement(transformation(extent={{-80,-40},{-60,-20}})));

equation
  //
  // Connections
  //
  connect(fs_a.port, pump.port_a) annotation (Line(
      points={{-60,0},{-38,0}},
      color={28,108,200},
      thickness=1));
  connect(pressureLoss.port_a, pump.port_b) annotation (Line(
      points={{22,0},{-22,0}},
      color={28,108,200},
      thickness=1));
  connect(fs_b.port, pressureLoss.port_b) annotation (Line(
      points={{60,0},{38,0}},
      color={28,108,200},
      thickness=1));

  connect(input_n.y, pump.n_input)
    annotation (Line(points={{-59,-30},{-30,-30},{-30,-8}}, color={0,0,127}));

  //
  // Annotations
  //
  annotation (experiment(StopTime=2500), Documentation(info="<html>
<p>
This model checks the affinity pump.
<br/><br/>
To see the model behavior, plot the variables of the models over the 
time. The simulation time is correctly preset (Start: 0 s, Stop = 2500 s). 
</p>
</html>", revisions="<html>
<ul>
  <li>
  January 9, 2024, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Test_AffinityPump;
