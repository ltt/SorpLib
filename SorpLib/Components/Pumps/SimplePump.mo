within SorpLib.Components.Pumps;
model SimplePump "Model of a simple pump with constant efficiencies"
  extends BaseClasses.PartialSimplePump(
    redeclare final Basics.Interfaces.FluidPorts.LiquidPort_out port_b,
    redeclare final Basics.Interfaces.FluidPorts.LiquidPort_in port_a,
    final no_components=Medium.nX);

  //
  // Definition of parameters regarding the medium
  //
  replaceable package Medium = Modelica.Media.Water.WaterIF97_R1pT
    constrainedby Modelica.Media.Interfaces.PartialMedium
    "Medium model of the (ideal) liquid"
    annotation (Dialog(tab="General", group="Medium"),
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

equation
  //
  // Calculation of fluid properties
  //
  rho = Medium.density_phX(
    p=port_a.p,
    h=inStream(port_a.h_outflow),
    X=cat(1, inStream(port_a.Xi_outflow), {1-sum(inStream(port_a.Xi_outflow))}))
    "Instreaming density";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
The simple pump can be used to prescribe the mass or volume flow rate in hydraulic
components, such as tubes or heat exchangers. The model assumes constant efficiencies
and can be used for (ideal) liquids.

<h4>Main equations</h4>
<p>
The model calculates the hydraulic, shaft, and drive power transmitted to the fluid
or required to drive the pump. The hydraulic power <i>P<sub>hydraulic</sub></i> 
transmitted to the fluid is defined as:
</p>
<pre>
    P<sub>hydraulic</sub> = m&#x307; / &rho;<sub>in</sub> * &Delta;p;
</pre>
<p>
Herein, <i>m&#x307;</i> is the mass flow rate, <i>&rho;<sub>in</sub></i> describes the 
fluid density at the inlet, and <i>&Delta;p = p<sub>b</sub> - p<sub>a</sub></i> is the 
pressure difference between port b and a.
<br/><br/>
The shaft power <i>P<sub>shaft</sub></i> depends on the pump efficiency 
<i>&eta;<sub>pump</sub></i> and is defined as:
</p>
<pre>
    P<sub>shaft</sub> = P<sub>hydraulic</sub> / &eta;<sub>pump</sub>;
</pre>
<p>
The shaft power is completly transmitted to the fluid, thus increasing the outflowing
specific enthalpy of the fluid.
<br/><br/>
The drive power <i>P<sub>drive</sub></i> is required to drive the pump, depends on the
drive efficienciy <i>&eta;<sub>drive</sub></i>, and is defined as:
</p>
<pre>
    P<sub>drive</sub> = P<sub>shaft</sub> / &eta;<sub>drive</sub>;
</pre>

<h4>Assumptions and limitations</h4>
<ul>
  <li>
  Steady-state process
  </li>
  <li>
  No flow reversal
  </li>
  <li>
  Constant efficiencies
  </li>
  <li>
  No storage of mass or energy
  </li>
</ul>

<h4>Typical use</h4>
<p>
The simple pump is typically used to prescripe the mass or volume flow rate
in hydraulic components, such as tubes or heat exchangers.
</p>

<h4>Important parameters and options</h4>
<ul>
  <li>
  <i>prescribedInput</i>:
  Defines the variable that is prescribed (i.e., mass flow rate or volume
  flow rate).
  </li>
  <li>
  <i>assumeIsenthalpicPump</i>:
  Defines if the hydraulic losses are considered in the energy balance.
  </li>
  <br/>
  <li>
  <i>avoid_events</i>:
  Defines if events shall be avoided via the noEvent()-operator.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  January 8, 2024, by Mirko Engelpracht:<br/>
  Adaptations due to restructering the library and documentation.
  </li>
  <li>
  January 20, 2021, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end SimplePump;
