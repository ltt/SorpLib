within SorpLib.Components.Pumps.BaseClasses;
partial model PartialPump "Base model for all pumps"

  //
  // Definition of parameters regarding the medium
  //
  parameter Integer no_components = 1
    "Number of components"
    annotation (Dialog(tab="General", group="Medium"),
                Evaluate=true,
                HideResult=true);

  //
  // Definition of paramteres describring the pump's characteristics
  //
  parameter Boolean assumeIsenthalpicPump = false
    " = true, if internal losses do not influence the energy balance"
    annotation (Dialog(tab="General", group="Pump Characteristics"));
  parameter Boolean calculateDrivePower = true
    " = true, if drive power is calcualted"
    annotation (Dialog(tab="General", group="Pump Characteristics"));

  parameter Modelica.Units.SI.Efficiency eta_drive = 0.9
    "Efficiency of drive that is used to calculate drive power"
    annotation (Dialog(tab="General", group="Pump Characteristics",
                enable=calculateDrivePower));

  //
  // Definition or initialisation parameters
  //
  parameter Modelica.Units.SI.MassFlowRate m_flow_start = 1e-4
    "Start value for mass flow rate"
    annotation (Dialog(tab="Initialisation", group="Start Values"));

  //
  // Definition of advanced parameters
  //
  parameter Boolean avoid_events = false
    "= true, if events are avoid by using noEvent()-operator"
    annotation (Dialog(tab = "Advanced", group = "Numerics"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult = true);

  //
  // Definition of ports
  //
  replaceable SorpLib.Basics.Interfaces.BaseClasses.PartialFluidPort port_a
    constrainedby Basics.Interfaces.BaseClasses.PartialFluidPort(
      final no_components=no_components,
      m_flow(start=m_flow_start))
    "Fluid port a"
    annotation (Placement(transformation(extent={{-90,-10},{-70,10}}),
                iconTransformation(extent={{-90,-10},{-70,10}})),
                choicesAllMatching=true);

  replaceable SorpLib.Basics.Interfaces.BaseClasses.PartialFluidPort port_b
    constrainedby Basics.Interfaces.BaseClasses.PartialFluidPort(
      final no_components=no_components,
      m_flow(start=-m_flow_start))
    "Fluid port b"
    annotation (Placement(transformation(extent={{70,-10},{90,10}}),
                iconTransformation(extent={{70,-10},{90,10}})),
                choicesAllMatching=true);

  //
  // Definition of variables
  //
  Modelica.Units.SI.Density rho
    "Instreaming density";

  Modelica.Units.SI.PressureDifference dp
    "Pressure difference between port a and b";
  Modelica.Units.SI.MassFlowRate m_flow
    "Mass flow rate";
  Modelica.Units.SI.VolumeFlowRate V_flow
    "Volume flow rate";

  Modelica.Units.SI.Power P_hydraulic
    "Hydraulic power consumption of the pump";
  Modelica.Units.SI.Power P_shaft
    "Shaft power consumption of the pump";
  Modelica.Units.SI.Power P_drive
    "Drive power consumption of the pump";
  Modelica.Units.SI.Power P_loss_internal
    "Internal power losses of the pump (i.e., increase of outflowing specific 
    enthalpy)";

equation
  //
  // Assertions
  //
  assert(m_flow >= 0,
         "Pump model cannot handle flow reversal!",
         level=AssertionLevel.error);

  //
  // Momentum balance
  //
  dp = port_b.p - port_a.p
    "Pressure difference between port b and a";

  //
  // Mass balance
  //
  0 = port_a.m_flow + port_b.m_flow
    "Steady-state mass balance";

  port_a.Xi_outflow = inStream(port_b.Xi_outflow)
    "Stream variable: Trivial equation since no change of mass fractions";
  port_b.Xi_outflow = inStream(port_a.Xi_outflow)
    "Stream variable: Trivial equation since no change of mass fractions";

  port_a.m_flow = m_flow
    "Mass flow rate at port a";

  //
  // Power calculations
  //
  P_hydraulic = V_flow * dp
    "Hydraulic power consumption of the pump";
  P_drive = if calculateDrivePower then P_shaft / eta_drive else 0
    "Drive power consumption of pump";
  P_loss_internal = P_shaft - P_hydraulic
    "Internal power losses of pump (i.e., increase outflowing specific enthalpy)";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial model is the base model for all pumps. It defines fundamental 
parameters and variables required by all pumps. Models that inherit properties 
from this partial model have to redeclare the fluid ports. Moreover, the mass
and energy balances must be completed, and the power calculations must be added.
In this context, appropriate fluid property models are required.
<br/><br/>
The following variables must be specified in the model that inherit properties:
</p>
<ul>
  <li>
  Instreaming density <i>rho</i>.
  </li>
  <br/>
  <li>
  Mass flow rate <i>m_flow</i> at port a.
  </li>
  <li>
  Volume flow rate <i>V_flow</i> at port a.
  </li>
  <br/>
  <li>
  Outflowing specific enthalpy <i>port_a.h_outflow</i> at port a.
  </li>
  <li>
  Outflowing specific enthalpy <i>port_b.h_outflow</i> at port b.
  </li>
  <br/>
  <li>
  Shaft power <i>P_shaft</i>.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  January 8, 2024, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"), Icon(graphics={
        Ellipse(
          extent={{-80,-80},{80,80}},
          lineColor={28,108,200},
          fillColor={28,108,200},
          fillPattern=FillPattern.Sphere),
        Polygon(
          points={{-68,42},{-68,-42},{80,0},{-68,42}},
          lineColor={0,0,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Line(
          points={{40,0},{-50,0}},
          color={0,0,0},
          arrow={Arrow.Filled,Arrow.None})}));
end PartialPump;
