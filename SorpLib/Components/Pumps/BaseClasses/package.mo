within SorpLib.Components.Pumps;
package BaseClasses "Base models and functions for all pumps"
  extends Modelica.Icons.BasesPackage;

  annotation (Documentation(info="<html>
<p>
This package contains partial pump models, containing fundamental definitions for 
pumps. The content of this package is only of interest when adding new pumps to 
the library. 
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end BaseClasses;
