within SorpLib.Components.Sensors;
package BaseClasses "Base models for all sensors"
  extends Modelica.Icons.BasesPackage;

  annotation (Documentation(info="<html>
<p>
This package contains partial sensors, containing fundamental definitions 
for sensors. The content of this package is only of interest when adding 
new sensors to the library. 
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end BaseClasses;
