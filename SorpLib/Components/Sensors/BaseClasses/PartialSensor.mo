within SorpLib.Components.Sensors.BaseClasses;
partial model PartialSensor
  "Base model for all sensors"

  //
  // Definition of setup parameters
  //
  parameter Boolean useTimeConstant = false
    " = true, if time constant is used (i.e., sensor value is returned delayed)"
    annotation (Dialog(tab="General", group="Sensor Setup"),
                choices(checkBox=true),
                HideResult=true,
                Evaluate=true);
  parameter Modelica.Units.SI.Time tau = 1
    "Time constant for delay"
    annotation (Dialog(tab="General", group="Sensor Setup",
                enable=useTimeConstant));

  //
  // Definition or initialisation parameters
  //
  parameter Integer initialisationType(min=1, max=3) = 1
    "Initialisation type: Fixed, steady-state, or free"
    annotation (Dialog(tab="Initialisation", group="Type",
                enable=useTimeConstant),
                choices(
                  choice=1 "Fixed",
                  choice=2 "Steady-state",
                  choice=3 "Free"));

  parameter Real value_initial = 1
    "Initial value of sensor value if it is delayed"
    annotation (Dialog(tab="Initialisation", group="Initial Values",
                enable=useTimeConstant));

  //
  // Definition of advanced parameters
  //
  parameter Boolean avoid_events = false
    "= true, if events are avoid by using noEvent()-operator"
    annotation (Dialog(tab = "Advanced", group = "Numerics"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult = true);

  //
  // Definition of outputs
  //
  Modelica.Blocks.Interfaces.RealOutput value(
    start=value_initial)
    "Sensor value that might be delayed by a time constant"
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={0,68}), iconTransformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={0,68})));

  //
  // Definition of variables
  //
  Real valueNonDelayed
    "Non-delayed sensor value";

initial equation
  if useTimeConstant then
    if initialisationType==1 then
      value = value_initial
        "Fixed intial value";

    elseif initialisationType==2 then
      der(value) = 0
        "Steady-state initialisation";

    end if;
  end if;

equation
  //
  // Delay sensor value if necessary
  //
  if useTimeConstant then
    der(value) = (valueNonDelayed - value) / tau
      "Delayed sensor value";

  else
    value = valueNonDelayed
      "Non-delayed sensor value";

  end if;

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This model is the base model for all sensors. It defines fundamental parameters
and variables required by all sensors. It is possible to select that the sensor 
value <i>value</i> is delayed by using a time constant <i>tau</i>:
</p>
<pre>
    (dvalue/dt) = (valueNonDelayed - value) / tau;
</pre>
<p>
Although this introduces a dynamic state variable, it can be used to break 
algebraic loops. The non-delayed sensor value can be accessed via the variable
<i>valueNonDelayed</i>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 18, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"), Icon(graphics={Ellipse(
          extent={{-50,60},{50,-40}},
          lineColor={0,0,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          lineThickness=0.5), Line(
          points={{0,-40},{0,-80}},
          color={0,0,0},
          thickness=1)}));
end PartialSensor;
