within SorpLib.Components.Sensors.BaseClasses.FluidMSL;
partial model MassFractionsSensor
  "Mass fractions sensor"

  //
  // Definition of parameters regarding the medium
  //
  final parameter Integer no_components = Medium.nX
    "Number of components"
    annotation (Dialog(tab="General", group="Medium"),
                Evaluate=true,
                HideResult=true);
  replaceable package Medium = Modelica.Media.Water.WaterIF97_R1pT
    constrainedby Modelica.Media.Interfaces.PartialMedium
    "Medium model"
    annotation (Dialog(tab="General", group="Medium"),
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

  //
  // Definition of setup parameters
  //
  parameter Boolean useTimeConstant = false
    " = true, if time constant is used (i.e., sensor value is returned delayed)"
    annotation (Dialog(tab="General", group="Sensor Setup"),
                choices(checkBox=true),
                HideResult=true,
                Evaluate=true);
  parameter Modelica.Units.SI.Time tau = 1
    "Time constant for delay"
    annotation (Dialog(tab="General", group="Sensor Setup",
                enable=useTimeConstant));

  //
  // Definition or initialisation parameters
  //
  parameter Integer initialisationType(min=1, max=3) = 1
    "Initialisation type: Fixed, steady-state, or free"
    annotation (Dialog(tab="Initialisation", group="Type",
                enable=useTimeConstant),
                choices(
                  choice=1 "Fixed",
                  choice=2 "Steady-state",
                  choice=3 "Free"));

  parameter Real[no_components] value_initial=
    fill(1/no_components, no_components)
    "Initial values of sensor value if it is delayed"
    annotation (Dialog(tab="Initialisation", group="Initial Values",
                enable=useTimeConstant));

  //
  // Definition of advanced parameters
  //
  parameter Boolean avoid_events = false
    "= true, if events are avoid by using noEvent()-operator"
    annotation (Dialog(tab = "Advanced", group = "Numerics"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult = true);

  //
  // Definition of outputs
  //
  Modelica.Blocks.Interfaces.RealOutput[no_components] value(
    start=value_initial)
    "Sensor values that might be delayed by a time constant"
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={0,68}), iconTransformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={0,68})));

  //
  // Definition of variables
  //
  Real[no_components] valueNonDelayed
    "Non-delayed sensor values";

  //
  // Definition of ports
  //
  replaceable SorpLib.Basics.Interfaces.BaseClasses.PartialFluidPort port
    constrainedby Basics.Interfaces.BaseClasses.PartialFluidPort(
      final no_components=no_components)
    "Fluid port"
    annotation (Placement(transformation(extent={{-10,-90},{10,-70}}),
                iconTransformation(extent={{-10,-90},{10,-70}})),
                choicesAllMatching=true);

initial equation
  if useTimeConstant then
    if initialisationType==1 then
      value = value_initial
        "Fixed intial values";

    elseif initialisationType==2 then
      der(value) = zeros(no_components)
        "Steady-state initialisations";

    end if;
  end if;

equation
  //
  // Delay sensor value if necessary
  //
  if useTimeConstant then
    der(value) = (valueNonDelayed .- value) ./ tau
      "Delayed sensor values";

  else
    value = valueNonDelayed
      "Non-delayed sensor values";

  end if;

  //
  // Set port values
  //
  port.m_flow = 0
    "Mass flow rate";
  port.h_outflow = 0
    "Specific enthalpy leaving the port: Dummy value";
  port.Xi_outflow = fill(1/no_components, no_components-1)
    "Independent mass fractions leaving the port: Dummy value";

  valueNonDelayed =
    cat(1, inStream(port.Xi_outflow), {1-sum(inStream(port.Xi_outflow))})
    "Mass fractions";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This mass fractions sensor reads all mass fractions in kg/kg. It is possible to 
select that the sensor values <i>value</i> are delayed by using a time constant 
<i>tau</i>:
</p>
<pre>
    (dvalue/dt) = (valueNonDelayed - value) / tau;
</pre>
<p>
Although this introduces dynamic states variable, it can be used to break 
algebraic loops. The non-delayed sensor values can be accessed via the variable
<i>valueNonDelayed</i>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 18, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"), Icon(graphics={    Line(
          points={{0,-40},{0,-80}},
          color={0,0,0},
          thickness=1), Ellipse(
          extent={{-50,60},{50,-40}},
          lineColor={0,0,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          lineThickness=0.5), Text(
          extent={{-50,60},{50,-40}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          textStyle={TextStyle.Bold},
          textString="xi")}));
end MassFractionsSensor;
