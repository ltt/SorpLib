﻿within SorpLib.Components.Sensors.BaseClasses.FluidMSL;
partial model TemperatureDifferenceSensor
  "Temperature difference sensor"
  extends SorpLib.Components.Sensors.BaseClasses.PartialFlowFluidSensor(
    final no_components=Medium.nX,
    value_initial=0);

  //
  // Definition of parameters regarding the medium
  //
  replaceable package Medium = Modelica.Media.Water.WaterIF97_R1pT
    constrainedby Modelica.Media.Interfaces.PartialMedium
    "Medium model"
    annotation (Dialog(tab="General", group="Medium"),
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

  //
  // Definition of parameters regarding the sensor setup
  //
  parameter Boolean flowDirectionAB = true
    " = true, temperature difference is calculated between port a and b (i.e., 
    positive value); otherwise, temperature difference is claculated between port
    b and a"
    annotation (Dialog(tab="General", group="Sensor Setup"),
                choices(checkBox=true),
                HideResult=true,
                Evaluate=true);

  //
  // Definition of variables
  //
protected
  Modelica.Units.SI.Temperature T_a = Medium.temperature_phX(
    p=port_a.p,
    h=inStream(port_a.h_outflow),
    X=cat(1,inStream(port_a.Xi_outflow),{1-sum(inStream(port_a.Xi_outflow))}))
    "Temperature at port a";
  Modelica.Units.SI.Temperature T_b = Medium.temperature_phX(
    p=port_b.p,
    h=inStream(port_b.h_outflow),
    X=cat(1,inStream(port_b.Xi_outflow),{1-sum(inStream(port_b.Xi_outflow))}))
    "Temperature at port b";

equation
  //
  // Set port values
  //
  valueNonDelayed = if flowDirectionAB then T_a-T_b else T_b-T_a
    "Temperature difference";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This temperature difference sensor reads the temperature difference in K. It is 
possible to select that the sensor value <i>value</i> is delayed by using a time  
constant <i>tau</i>:
</p>
<pre>
    (dvalue/dt) = (valueNonDelayed - value) / tau;
</pre>
<p>
Although this introduces a dynamic state variable, it can be used to break 
algebraic loops. The non-delayed sensor value can be accessed via the variable
<i>valueNonDelayed</i>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 18, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"), Icon(graphics={    Line(
          points={{0,-40},{0,-80}},
          color={0,0,0},
          thickness=1), Text(
          extent={{-40,64},{40,-36}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          textStyle={TextStyle.Bold},
          textString="ΔT"),
        Text(
          extent={{10,80},{90,60}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          textString=String(value,significantDigits=3) + " K")}));
end TemperatureDifferenceSensor;
