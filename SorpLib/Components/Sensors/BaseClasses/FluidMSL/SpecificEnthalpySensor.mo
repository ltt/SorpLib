within SorpLib.Components.Sensors.BaseClasses.FluidMSL;
partial model SpecificEnthalpySensor "Specific enthalpy sensor"
  extends SorpLib.Components.Sensors.BaseClasses.PartialAbsoluteFluidSensor(
    final no_components=Medium.nX,
    value_initial=1e5);

  //
  // Definition of parameters regarding the medium
  //
  replaceable package Medium = Modelica.Media.Water.WaterIF97_R1pT
    constrainedby Modelica.Media.Interfaces.PartialMedium
    "Medium model"
    annotation (Dialog(tab="General", group="Medium"),
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

equation
  //
  // Set port values
  //
  valueNonDelayed = inStream(port.h_outflow)
    "Specific enthalpy";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This specific enthalpy sensor reads the specific enthalpy in J/kg. It is possible 
to select that the sensor value <i>value</i> is delayed by using a time constant 
<i>tau</i>:
</p>
<pre>
    (dvalue/dt) = (valueNonDelayed - value) / tau;
</pre>
<p>
Although this introduces a dynamic state variable, it can be used to break 
algebraic loops. The non-delayed sensor value can be accessed via the variable
<i>valueNonDelayed</i>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 18, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"), Icon(graphics={    Line(
          points={{0,-40},{0,-80}},
          color={0,0,0},
          thickness=1), Text(
          extent={{-50,60},{50,-40}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          textStyle={TextStyle.Bold},
          textString="h"),
        Text(
          extent={{10,80},{90,60}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          textString=String(value/1000,significantDigits=3) + " kJ/kg")}));
end SpecificEnthalpySensor;
