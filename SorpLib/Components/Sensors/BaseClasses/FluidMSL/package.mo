within SorpLib.Components.Sensors.BaseClasses;
package FluidMSL "Base models for all fluid sensors based on the MSL"
  extends Modelica.Icons.BasesPackage;

  annotation (Documentation(info="<html>
<p>
This package contains partial fluid sensors, containing fundamental 
definitions for sensors. These fluid sensors are based on the open-
source Modelica Standard Library (MSL). The content of this package 
is only of interest when adding new sensors to the library. 
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 18, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end FluidMSL;
