within SorpLib.Components.Sensors.BaseClasses.FluidMSL;
partial model SpecificEntropySensor "Specific entropy sensor"
  extends SorpLib.Components.Sensors.BaseClasses.PartialAbsoluteFluidSensor(
    final no_components=Medium.nX,
    value_initial=1e3);

  //
  // Definition of parameters regarding the medium
  //
  replaceable package Medium = Modelica.Media.Water.WaterIF97_R1pT
    constrainedby Modelica.Media.Interfaces.PartialMedium
    "Medium model"
    annotation (Dialog(tab="General", group="Medium"),
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

equation
  //
  // Set port values
  //
  valueNonDelayed = Medium.specificEntropy(Medium.setState_phX(
    p=port.p,
    h=inStream(port.h_outflow),
    X=cat(1,inStream(port.Xi_outflow),{1-sum(inStream(port.Xi_outflow))})))
    "Specific entropy";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This specific entropy sensor reads the specific entropy in J/kg/K. It is possible 
to select that the sensor value <i>value</i> is delayed by using a time constant 
<i>tau</i>:
</p>
<pre>
    (dvalue/dt) = (valueNonDelayed - value) / tau;
</pre>
<p>
Although this introduces a dynamic state variable, it can be used to break 
algebraic loops. The non-delayed sensor value can be accessed via the variable
<i>valueNonDelayed</i>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 18, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"), Icon(graphics={    Line(
          points={{0,-40},{0,-80}},
          color={0,0,0},
          thickness=1), Text(
          extent={{-50,66},{50,-34}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          textStyle={TextStyle.Bold},
          textString="s"),
        Text(
          extent={{10,80},{90,60}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          textString=String(value,significantDigits=3) + " J/kg/K")}));
end SpecificEntropySensor;
