﻿within SorpLib.Components.Sensors.BaseClasses.FluidMSL;
partial model PressureDifferenceSensor "Pressure difference sensor"
  extends SorpLib.Components.Sensors.BaseClasses.PartialFlowFluidSensor(
    final no_components=Medium.nX,
    value_initial=0);

  //
  // Definition of parameters regarding the medium
  //
  replaceable package Medium = Modelica.Media.Water.WaterIF97_R1pT
    constrainedby Modelica.Media.Interfaces.PartialMedium
    "Medium model"
    annotation (Dialog(tab="General", group="Medium"),
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

  //
  // Definition of parameters regarding the sensor setup
  //
  parameter Boolean flowDirectionAB = true
    " = true, pressure difference is calculated between port a and b (i.e., 
    positive value); otherwise, pressure difference is claculated between port
    b and a"
    annotation (Dialog(tab="General", group="Sensor Setup"),
                choices(checkBox=true),
                HideResult=true,
                Evaluate=true);

equation
  //
  // Set port values
  //
  valueNonDelayed = if flowDirectionAB then port_a.p-port_b.p else
    port_b.p-port_a.p
    "Pressure difference";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This pressure difference sensor reads the pressure difference in Pa. It is 
possible to select that the sensor value <i>value</i> is delayed by using a time  
constant <i>tau</i>:
</p>
<pre>
    (dvalue/dt) = (valueNonDelayed - value) / tau;
</pre>
<p>
Although this introduces a dynamic state variable, it can be used to break 
algebraic loops. The non-delayed sensor value can be accessed via the variable
<i>valueNonDelayed</i>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 18, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"), Icon(graphics={    Line(
          points={{0,-40},{0,-80}},
          color={0,0,0},
          thickness=1), Text(
          extent={{-40,64},{40,-36}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          textStyle={TextStyle.Bold},
          textString="Δp"),
        Text(
          extent={{10,80},{90,60}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          textString=String(value/100,significantDigits=3) + " mbar")}));
end PressureDifferenceSensor;
