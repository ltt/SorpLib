﻿within SorpLib.Components.Sensors.BaseClasses.FluidMSL;
partial model VolumeFlowRateSensor "Volume flow rate sensor"
  extends SorpLib.Components.Sensors.BaseClasses.PartialFlowFluidSensor(
    final no_components=Medium.nX,
    value_initial=10/60000);

  //
  // Definition of parameters regarding the medium
  //
  replaceable package Medium = Modelica.Media.Water.WaterIF97_R1pT
    constrainedby Modelica.Media.Interfaces.PartialMedium
    "Medium model"
    annotation (Dialog(tab="General", group="Medium"),
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

  //
  // Definition of parameters regarding the sensor setup
  //
  parameter Boolean flowDirectionAB = true
    " = true, if mass flows from port a to b (i.e., positive value); otherwise,
    mass flows from port b to a"
    annotation (Dialog(tab="General", group="Sensor Setup"),
                choices(checkBox=true),
                HideResult=true,
                Evaluate=true);

  //
  // Definition of advanced parameters
  //
  parameter Modelica.Units.SI.MassFlowRate m_flow_small = 1e-5
    "Mass flow rate used to regulate flow-dependent properties (i.e., density)"
    annotation (Dialog(tab = "Advanced", group = "Numerics"),
                Evaluate=true,
                HideResult = true);

  //
  // Definition of variables
  //
protected
  Modelica.Units.SI.Density rho_a = Medium.density_phX(
    p=port_a.p,
    h=inStream(port_a.h_outflow),
    X=cat(1,inStream(port_a.Xi_outflow),{1-sum(inStream(port_a.Xi_outflow))}))
    "Density at port a";
  Modelica.Units.SI.Density rho_b = Medium.density_phX(
    p=port_b.p,
    h=inStream(port_b.h_outflow),
    X=cat(1,inStream(port_b.Xi_outflow),{1-sum(inStream(port_b.Xi_outflow))}))
    "Density at port b";
  Modelica.Units.SI.Density rho = if avoid_events then
    SorpLib.Numerics.regStep_noEvent(
      x=port_a.m_flow,
      y1=rho_a,
      y2=rho_b,
      x_small=m_flow_small) else
    SorpLib.Numerics.regStep(
      x=port_a.m_flow,
      y1=rho_a,
      y2=rho_b,
      x_small=m_flow_small)
    "Actual density";

equation
  //
  // Set port values
  //
  valueNonDelayed = if flowDirectionAB then port_a.m_flow/rho else
    port_b.m_flow/rho
    "Volume flow rate";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This volume flow rate sensor reads the volume flow rate in m<sup>3</sup>/s. It 
is possible to select that the sensor value <i>value</i> is delayed by using a 
time constant <i>tau</i>:
</p>
<pre>
    (dvalue/dt) = (valueNonDelayed - value) / tau;
</pre>
<p>
Although this introduces a dynamic state variable, it can be used to break 
algebraic loops. The non-delayed sensor value can be accessed via the variable
<i>valueNonDelayed</i>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 18, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"), Icon(graphics={    Line(
          points={{0,-40},{0,-80}},
          color={0,0,0},
          thickness=1), Text(
          extent={{-50,54},{50,-46}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          textStyle={TextStyle.Bold},
          textString="V̇"),
        Text(
          extent={{10,80},{90,60}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          textString=String(value*60000,significantDigits=3) + " l/min")}));
end VolumeFlowRateSensor;
