within SorpLib.Components.Sensors.BaseClasses.FluidMSL;
partial model ThermodynamicStateSensor
  "Thermodynamic state sensor"

  //
  // Definition of parameters regarding the medium
  //
  final parameter Integer no_components = Medium.nX
    "Number of components"
    annotation (Dialog(tab="General", group="Medium"),
                Evaluate=true,
                HideResult=true);
  replaceable package Medium = Modelica.Media.Water.WaterIF97_R1pT
    constrainedby Modelica.Media.Interfaces.PartialMedium
    "Medium model"
    annotation (Dialog(tab="General", group="Medium"),
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

  //
  // Definition of advanced parameters
  //
  parameter Boolean avoid_events = false
    "= true, if events are avoid by using noEvent()-operator"
    annotation (Dialog(tab = "Advanced", group = "Numerics"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult = true);

  //
  // Definition of variables
  //
  Modelica.Units.SI.Pressure p
    "Pressure";
  Modelica.Units.SI.Temperature T
    "Temperature";
  Modelica.Units.SI.SpecificVolume v
    "Specific volume";
  Modelica.Units.SI.SpecificEnthalpy h
    "Specific enthalpy";
  Modelica.Units.SI.SpecificInternalEnergy u
    "Specific internal energy";
  Modelica.Units.SI.SpecificEntropy s
    "Specific entropy";
  Modelica.Units.SI.SpecificGibbsFreeEnergy  g
    "Specific free enthalpy (i.e., Gibbs free energy)";
  Modelica.Units.SI.SpecificHelmholtzFreeEnergy  a
    "Specific free energy (i.e., Helmholts free energy)";

  //
  // Definition of ports
  //
  replaceable SorpLib.Basics.Interfaces.BaseClasses.PartialFluidPort port
    constrainedby Basics.Interfaces.BaseClasses.PartialFluidPort(
      final no_components=no_components)
    "Fluid port"
    annotation (Placement(transformation(extent={{-10,-90},{10,-70}}),
                iconTransformation(extent={{-10,-90},{10,-70}})),
                choicesAllMatching=true);

  //
  // Definitio of protected variables
  //
protected
  Medium.ThermodynamicState state = Medium.setState_phX(
    p=p,
    h=h,
    X=cat(1,inStream(port.Xi_outflow),{1-sum(inStream(port.Xi_outflow))}))
    "State record";

equation
  //
  // Set port values
  //
  port.m_flow = 0
    "Mass flow rate";
  port.h_outflow = 0
    "Specific enthalpy leaving the port: Dummy value";
  port.Xi_outflow = fill(1/no_components, no_components-1)
    "Independent mass fractions leaving the port: Dummy value";

  //
  // Calculate state variables
  //
  p = port.p
    "Pressure";
  T = Medium.temperature(state=state)
    "Temperature";
  v = 1/Medium.density(state=state)
    "Specific volume";
  h = inStream(port.h_outflow)
    "Specific enthalpy";
  u = h - p*v
    "Specific internal energy";
  s = Medium.specificEntropy(state=state)
    "Specific entropy";
  g = h - T*s
    "Specific free enthalpy (i.e., Gibbs free energy)";
  a = u - T*s
    "Specific free energy (i.e., Helmholts free energy)";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This thermodynamic state sensor reads all thermodynamic state variables. I.e.,
pressure <i>p</i>, temperature <i>T</i>, specific volume <i>v</i>, specific
enthalpy <i>h</i>, specific internal energy <i>u</i>, specific entropy <i>s</i>,
specific free enthalpy (i.e., Gibbs free energy <i>g</i>, and specific free 
energy (i.e., Helmholts free energy) <i>a</i>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 18, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"), Icon(graphics={    Line(
          points={{0,-40},{0,-80}},
          color={0,0,0},
          thickness=1), Ellipse(
          extent={{-50,60},{50,-40}},
          lineColor={0,0,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          lineThickness=0.5), Text(
          extent={{-40,60},{40,-40}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          textStyle={TextStyle.Bold},
          textString="state")}));
end ThermodynamicStateSensor;
