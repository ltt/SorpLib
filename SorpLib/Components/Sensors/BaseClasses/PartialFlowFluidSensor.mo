within SorpLib.Components.Sensors.BaseClasses;
partial model PartialFlowFluidSensor
  "Base model for all fluid-based flow sensors"

  //
  // Definition of parameters regarding the medium
  //
  parameter Integer no_components = 1
    "Number of components"
    annotation (Dialog(tab="General", group="Medium"),
                Evaluate=true,
                HideResult=true);

  extends SorpLib.Components.Sensors.BaseClasses.PartialSensor;

  //
  // Definition of ports
  //
  replaceable SorpLib.Basics.Interfaces.BaseClasses.PartialFluidPort port_a
    constrainedby Basics.Interfaces.BaseClasses.PartialFluidPort(
      final no_components=no_components)
    "Fluid port a"
    annotation (Placement(transformation(extent={{-60,-90},{-40,-70}}),
                iconTransformation(extent={{-60,-90},{-40,-70}})),
                choicesAllMatching=true);

  replaceable SorpLib.Basics.Interfaces.BaseClasses.PartialFluidPort port_b
    constrainedby Basics.Interfaces.BaseClasses.PartialFluidPort(
      final no_components=no_components)
    "Fluid port b"
    annotation (Placement(transformation(extent={{40,-90},{60,-70}}),
                iconTransformation(extent={{40,-90},{60,-70}})),
                choicesAllMatching=true);

equation
  //
  // Connections
  //
  connect(port_a, port_b)
    annotation (Line(points={{-50,-80},{50,-80}}, color={0,0,0}));

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This model is the base model for all fluid-based flow sensors. It defines 
fundamental parameters and variables required by all sensors. It is possible to 
select that the sensor value <i>value</i> is delayed by using a time constant 
<i>tau</i>:
</p>
<pre>
    (dvalue/dt) = (valueNonDelayed - value) / tau;
</pre>
<p>
Although this introduces a dynamic state variable, it can be used to break 
algebraic loops. The non-delayed sensor value can be accessed via the variable
<i>valueNonDelayed</i>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 18, 2023, by Mirko Engelpracht:<br/>
  Added functionality and documentation.
  </li>
  <li>
  January 20, 2021, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"), Icon(graphics={Ellipse(
          extent={{-50,60},{50,-40}},
          lineColor={0,0,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          lineThickness=0.5), Line(
          points={{0,-40},{0,-80}},
          color={0,0,0},
          thickness=1)}));
end PartialFlowFluidSensor;
