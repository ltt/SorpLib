within SorpLib.Components.Sensors.GasSensors;
model MassFractionsSensor "Mass fractions sensor"
  extends SorpLib.Components.Sensors.BaseClasses.FluidMSL.MassFractionsSensor(
    redeclare final Basics.Interfaces.FluidPorts.GasPort_in port,
    redeclare replaceable package Medium =
      SorpLib.Media.IdealGasMixtures.DryAir_N2_O2_CO2_H2O
      constrainedby Modelica.Media.Interfaces.PartialMedium);

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This mass fractions sensor reads all mass fractions in kg/kg. It is possible to 
select that the sensor values <i>value</i> are delayed by using a time constant 
<i>tau</i>:
</p>
<pre>
    (dvalue/dt) = (valueNonDelayed - value) / tau;
</pre>
<p>
Although this introduces dynamic states variable, it can be used to break 
algebraic loops. The non-delayed sensor values can be accessed via the variable
<i>valueNonDelayed</i>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 18, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end MassFractionsSensor;
