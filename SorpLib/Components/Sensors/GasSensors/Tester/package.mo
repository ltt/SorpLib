within SorpLib.Components.Sensors.GasSensors;
package Tester "Models to test and varify models for gas / gas mixture sensors"
  extends Modelica.Icons.ExamplesPackage;

  annotation (Documentation(info="<html>
<p>
This package contains executable test models for all implemented gas / gas
mixture sensors. 
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 18, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Tester;
