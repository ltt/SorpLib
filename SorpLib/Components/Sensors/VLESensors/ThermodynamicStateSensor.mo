within SorpLib.Components.Sensors.VLESensors;
model ThermodynamicStateSensor "Thermodynamic state sensor"
  extends
    SorpLib.Components.Sensors.BaseClasses.FluidMSL.ThermodynamicStateSensor(
      redeclare final Basics.Interfaces.FluidPorts.VLEPort_in port,
      redeclare replaceable package Medium =
        Modelica.Media.Water.StandardWater
        constrainedby Modelica.Media.Interfaces.PartialTwoPhaseMedium);

  //
  // Definition of variables
  //
  Real q(unit="kg/kg")
    "Vapor quality";

protected
  Medium.SaturationProperties sat = Medium.setSat_p(p=p)
    "Saturation state";

  Modelica.Units.SI.SpecificEnthalpy h_sat_liq=
    Medium.bubbleEnthalpy(sat=sat)
    "Specific bubble point enthalpy";
  Modelica.Units.SI.SpecificEnthalpy h_sat_vap=
    Medium.dewEnthalpy(sat=sat)
    "Specific dew point enthalpy";

equation
  //
  // Calculate properties
  //
  q = max(0, min((h - h_sat_liq) / (h_sat_vap - h_sat_liq), 1))
    "Vapor quality";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This thermodynamic state sensor reads all thermodynamic state variables. I.e.,
pressure <i>p</i>, temperature <i>T</i>, specific volume <i>v</i>, specific
enthalpy <i>h</i>, specific internal energy <i>u</i>, specific entropy <i>s</i>,
specific free enthalpy (i.e., Gibbs free energy <i>g</i>, and specific free 
energy (i.e., Helmholts free energy) <i>a</i>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 18, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end ThermodynamicStateSensor;
