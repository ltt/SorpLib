within SorpLib.Components.Sensors.VLESensors;
model VaporQualitySensor
  "Vapor quality (i.e., mass fraction) sensor"
  extends SorpLib.Components.Sensors.BaseClasses.PartialAbsoluteFluidSensor(
    redeclare final Basics.Interfaces.FluidPorts.VLEPort_in port,
    final no_components=Medium.nX,
    value_initial=0.1);

  //
  // Definition of parameters regarding the medium
  //
  replaceable package Medium = Modelica.Media.Water.StandardWater
    constrainedby Modelica.Media.Interfaces.PartialTwoPhaseMedium
    "Medium model"
    annotation (Dialog(tab="General", group="Medium"),
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

  //
  // Definition of variables
  //
protected
  Medium.SaturationProperties sat = Medium.setSat_p(p=port.p)
    "Saturation state";

  Modelica.Units.SI.SpecificEnthalpy h_sat_liq=
    Medium.bubbleEnthalpy(sat=sat)
    "Specific bubble point enthalpy";
  Modelica.Units.SI.SpecificEnthalpy h_sat_vap=
    Medium.dewEnthalpy(sat=sat)
    "Specific dew point enthalpy";

equation
  //
  // Set port values
  //
  valueNonDelayed = max(0,
    min((inStream(port.h_outflow) - h_sat_liq) / (h_sat_vap - h_sat_liq), 1))
    "Vapor quality";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This vapor quality sensor reads the vapor quality in kg/kg. It is possible to 
select that the sensor value <i>value</i> is delayed by using a time constant 
<i>tau</i>:
</p>
<pre>
    (dvalue/dt) = (valueNonDelayed - value) / tau;
</pre>
<p>
Although this introduces a dynamic state variable, it can be used to break 
algebraic loops. The non-delayed sensor value can be accessed via the variable
<i>valueNonDelayed</i>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 18, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"), Icon(graphics={    Line(
          points={{0,-40},{0,-80}},
          color={0,0,0},
          thickness=1), Text(
          extent={{-50,74},{50,-26}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          textStyle={TextStyle.Bold},
          textString="q"),
        Text(
          extent={{10,80},{90,60}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          textString=String(value,significantDigits=3) + " kg/kg")}));
end VaporQualitySensor;
