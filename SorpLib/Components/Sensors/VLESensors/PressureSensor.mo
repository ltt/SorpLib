within SorpLib.Components.Sensors.VLESensors;
model PressureSensor "Pressure sensor"
  extends SorpLib.Components.Sensors.BaseClasses.FluidMSL.PressureSensor(
    redeclare final Basics.Interfaces.FluidPorts.VLEPort_in port,
    redeclare replaceable package Medium =
      Modelica.Media.Water.StandardWater
      constrainedby Modelica.Media.Interfaces.PartialTwoPhaseMedium);

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This pressure sensor reads the pressure temperature in Pa. It is possible to 
select that the sensor value <i>value</i> is delayed by using a time constant 
<i>tau</i>:
</p>
<pre>
    (dvalue/dt) = (valueNonDelayed - value) / tau;
</pre>
<p>
Although this introduces a dynamic state variable, it can be used to break 
algebraic loops. The non-delayed sensor value can be accessed via the variable
<i>valueNonDelayed</i>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 18, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end PressureSensor;
