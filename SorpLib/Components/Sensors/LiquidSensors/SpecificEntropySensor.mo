within SorpLib.Components.Sensors.LiquidSensors;
model SpecificEntropySensor "Specific entropy sensor"
  extends SorpLib.Components.Sensors.BaseClasses.FluidMSL.SpecificEntropySensor(
      redeclare final Basics.Interfaces.FluidPorts.LiquidPort_in port,
      redeclare replaceable package Medium =
        Modelica.Media.Water.WaterIF97_R1pT);

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This specific entropy sensor reads the specific entropy in J/kg/K. It is possible 
to select that the sensor value <i>value</i> is delayed by using a time constant 
<i>tau</i>:
</p>
<pre>
    (dvalue/dt) = (valueNonDelayed - value) / tau;
</pre>
<p>
Although this introduces a dynamic state variable, it can be used to break 
algebraic loops. The non-delayed sensor value can be accessed via the variable
<i>valueNonDelayed</i>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 18, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end SpecificEntropySensor;
