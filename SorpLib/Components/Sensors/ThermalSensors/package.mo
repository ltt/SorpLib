within SorpLib.Components.Sensors;
package ThermalSensors "Thermal sensors"
  extends Modelica.Icons.VariantsPackage;

  annotation (Documentation(info="<html>
<p>
This package contains thermal sensors. It implements absolute sensors, which 
are only connected to one thermal port, as well as relative and flow sensors, 
which are connected between two thermal ports.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 18, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end ThermalSensors;
