within SorpLib.Components.Sensors.ThermalSensors.Tester;
model Test_ThermalSensors "Tester for thermal sensors"
  extends Modelica.Icons.Example;

  //
  // Definition of boundary models
  //
  Basics.Sources.Thermal.HeatSource hp_TVar(
    boundaryType=SorpLib.Choices.BoundaryThermal.Temperature,
    use_TInput=true)
    "Heat source with variable temperature"
    annotation (Placement(transformation(extent={{-70,-10},{-50,10}})));

  Basics.Sources.Thermal.HeatSource hp_QVar(
    boundaryType=SorpLib.Choices.BoundaryThermal.HeatFlowRate,
    use_QFlowInput=true)
    "Heat source with variable heat flow"
    annotation (Placement(transformation(extent={{70,-10},{50,10}})));

  //
  // Definition of sensors
  //
  TemperatureSensor TSensor_hp_TVar
    "Temperatur sensor at thermal source 'hp_TVar'"
    annotation (Placement(transformation(extent={{-50,-2},{-30,18}})));
  DifferenceTemperatureSensor DTSensor "Difference temperature sensor"
    annotation (Placement(transformation(extent={{-10,18},{10,38}})));
  HeatFlowRateSensor QFlowSensor "Heat flow rate sensor"
    annotation (Placement(transformation(extent={{-10,-2},{10,18}})));
  TemperatureSensor TSensor_hp_QVar(useTimeConstant=true, tau=25,
    value_initial=293.15)
    "Temperatur sensor at thermal source 'hp_QVar'"
    annotation (Placement(transformation(extent={{30,-2},{50,18}})));

  //
  // Definition of input signals
  //
protected
  Modelica.Blocks.Sources.Ramp input_TVar(
    height=80,
    duration=2500,
    offset=293.15,
    startTime=0)
    "Ramp to simulate input signal of temperrature"
    annotation (Placement(transformation(extent={{-100,-10},{-80,10}})),
                HideResult=true);
  Modelica.Blocks.Sources.Ramp input_QVar(
    height=2000,
    duration=2500,
    offset=-1000,
    startTime=0)
    "Ramp to simulate input signal of heat flow"
    annotation (Placement(transformation(extent={{100,-10},{80,10}})),
    HideResult=true);

equation
  //
  // Connections
  //
  connect(hp_TVar.port, TSensor_hp_TVar.port) annotation (Line(
      points={{-60,0},{-40,0}},
      color={238,46,47},
      thickness=1));
  connect(hp_QVar.port, TSensor_hp_QVar.port) annotation (Line(
      points={{60,0},{40,0}},
      color={238,46,47},
      thickness=1));
  connect(hp_TVar.port, QFlowSensor.port_a) annotation (Line(
      points={{-60,0},{-5,0}},
      color={238,46,47},
      thickness=1));
  connect(QFlowSensor.port_b, hp_QVar.port) annotation (Line(
      points={{5,0},{60,0}},
      color={238,46,47},
      thickness=1));
  connect(hp_TVar.port, DTSensor.port_a) annotation (Line(
      points={{-60,0},{-22,0},{-22,20},{-5,20}},
      color={238,46,47},
      thickness=1));
  connect(DTSensor.port_b, hp_QVar.port) annotation (Line(
      points={{5,20},{20,20},{20,0},{60,0}},
      color={238,46,47},
      thickness=1));

  connect(input_TVar.y, hp_TVar.T_input) annotation (Line(points={{-79,0},{-70,0},{
          -70,5.2},{-61,5.2}},      color={0,0,127}));
  connect(input_QVar.y, hp_QVar.Q_flow_input) annotation (Line(points={{79,0},{70,0},
          {70,-6},{61,-6},{61,-5}},         color={0,0,127}));

  //
  // Annotations
  //
  annotation (experiment(StopTime=2500), Documentation(info="<html>
<p>
This model checks all thermal sensors.
<br/><br/>
To see the model behavior, plot the variables of the models over the 
time. The simulation time is correctly preset (Start: 0 s, Stop = 2500 s). 
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 18, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Test_ThermalSensors;
