within SorpLib.Components.Sensors.ThermalSensors;
package Tester "Models to test and varify models for thermal sensors"
  extends Modelica.Icons.ExamplesPackage;

  annotation (Documentation(info="<html>
<p>
This package contains executable test models for all implemented thermal
sensors. 
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 18, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Tester;
