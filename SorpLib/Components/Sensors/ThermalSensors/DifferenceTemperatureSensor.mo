﻿within SorpLib.Components.Sensors.ThermalSensors;
model DifferenceTemperatureSensor
  "Difference temperature sensor"
  extends SorpLib.Components.Sensors.BaseClasses.PartialSensor(
    value_initial=0);

  //
  // Definition of parameters
  //
  parameter Boolean flowDirectionAB = true
    " = true, temperature difference is calculated between port a and b (i.e., 
    positive value); otherwise, temperature difference is claculated between port
    b and a"
    annotation (Dialog(tab="General", group="Sensor Setup"),
                choices(checkBox=true),
                HideResult=true,
                Evaluate=true);

  //
  // Definition of ports
  //
  Basics.Interfaces.HeatPorts.HeatPort_in port_a
    annotation (Placement(transformation(extent={{-60,-90},{-40,-70}}),
                iconTransformation(extent={{-60,-90},{-40,-70}})));

  Basics.Interfaces.HeatPorts.HeatPort_out port_b
    annotation (Placement(transformation(extent={{40,-90},{60,-70}}),
                iconTransformation(extent={{40,-90},{60,-70}})));

equation
  //
  // Set port values
  //
  valueNonDelayed = if flowDirectionAB then port_a.T-port_b.T else port_b.T-port_a.T
    "Temperature";

  port_a.Q_flow = 0
    "Heat flow rate";
  port_b.Q_flow = 0
    "Heat flow rate";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This difference temperature sensor reads the difference temperature between port
a and b or b and a in K. It is possible to select that the sensor value <i>value</i> 
is delayed by using a time constant <i>tau</i>:
</p>
<pre>
    (dvalue/dt) = (valueNonDelayed - value) / tau;
</pre>
<p>
Although this introduces a dynamic state variable, it can be used to break 
algebraic loops. The non-delayed sensor value can be accessed via the variable
<i>valueNonDelayed</i>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 18, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"), Icon(graphics={    Line(
          points={{0,-40},{0,-80}},
          color={0,0,0},
          thickness=1), Text(
          extent={{-40,60},{40,-40}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          textStyle={TextStyle.Bold},
          textString="ΔT"),    Line(
          points={{-52,-80},{50,-80}},
          color={238,46,47},
          thickness=1),
        Text(
          extent={{10,80},{90,60}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          textString=String(value,significantDigits=3) + " K")}));
end DifferenceTemperatureSensor;
