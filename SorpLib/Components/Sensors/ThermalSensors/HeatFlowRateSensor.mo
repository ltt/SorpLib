﻿within SorpLib.Components.Sensors.ThermalSensors;
model HeatFlowRateSensor "Heat flow rate sensor"
  extends SorpLib.Components.Sensors.BaseClasses.PartialSensor(
    value_initial=0);

  //
  // Definition of parameters
  //
  parameter Boolean flowDirectionAB = true
    " = true, if heat flows from port a to b (i.e., positive value); otherwise,
    heat flows from port b to a"
    annotation (Dialog(tab="General", group="Sensor Setup"),
                choices(checkBox=true),
                HideResult=true,
                Evaluate=true);

  //
  // Definition of ports
  //
  Basics.Interfaces.HeatPorts.HeatPort_in port_a
    annotation (Placement(transformation(extent={{-60,-90},{-40,-70}}),
                iconTransformation(extent={{-60,-90},{-40,-70}})));

  Basics.Interfaces.HeatPorts.HeatPort_out port_b
    annotation (Placement(transformation(extent={{40,-90},{60,-70}}),
                iconTransformation(extent={{40,-90},{60,-70}})));

equation
  //
  // Set port values
  //
  valueNonDelayed = if flowDirectionAB then port_a.Q_flow else port_b.Q_flow
    "Heat flow rate";

  //
  // Connections
  //
  connect(port_a, port_b) annotation (Line(
      points={{-50,-80},{50,-80}},
      color={238,46,47},
      thickness=1));

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This heat flow rate sensor reads the heat flow rate at port a or b in W. It is 
possible to select that the sensor value <i>value</i> is delayed by using a time 
constant <i>tau</i>:
</p>
<pre>
    (dvalue/dt) = (valueNonDelayed - value) / tau;
</pre>
<p>
Although this introduces a dynamic state variable, it can be used to break 
algebraic loops. The non-delayed sensor value can be accessed via the variable
<i>valueNonDelayed</i>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 18, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"), Icon(graphics={    Line(
          points={{0,-40},{0,-80}},
          color={0,0,0},
          thickness=1), Text(
          extent={{-50,50},{50,-30}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          textStyle={TextStyle.Bold},
          textString="Q̇"),    Line(
          points={{-52,-80},{50,-80}},
          color={238,46,47},
          thickness=1),
        Text(
          extent={{10,80},{90,60}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          textString=String(value,significantDigits=3) + " W")}));
end HeatFlowRateSensor;
