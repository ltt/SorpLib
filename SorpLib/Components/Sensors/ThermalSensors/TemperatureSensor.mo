﻿within SorpLib.Components.Sensors.ThermalSensors;
model TemperatureSensor
  "Temperature sensor"
  extends SorpLib.Components.Sensors.BaseClasses.PartialSensor(
    value_initial=298.15);

  //
  // Definition of ports
  //
  Basics.Interfaces.HeatPorts.HeatPort_in port
    annotation (Placement(transformation(extent={{-10,-90},{10,-70}}),
                iconTransformation(extent={{-10,-90},{10,-70}})));

equation
  //
  // Set port values
  //
  port.T = valueNonDelayed
    "Temperature";
  port.Q_flow = 0
    "Heat flow rate";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This temperature sensor reads the absolute temperature in K. It is possible to 
select that the sensor value <i>value</i> is delayed by using a time constant 
<i>tau</i>:
</p>
<pre>
    (dvalue/dt) = (valueNonDelayed - value) / tau;
</pre>
<p>
Although this introduces a dynamic state variable, it can be used to break 
algebraic loops. The non-delayed sensor value can be accessed via the variable
<i>valueNonDelayed</i>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 18, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"), Icon(graphics={    Line(
          points={{0,-40},{0,-80}},
          color={0,0,0},
          thickness=1), Text(
          extent={{-50,60},{50,-40}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          textStyle={TextStyle.Bold},
          textString="T"),
        Text(
          extent={{10,80},{90,60}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          textString=String(value-273.15,significantDigits=3) + " °C")}));
end TemperatureSensor;
