within SorpLib.Components;
package Sensors "Ideal sensors to extract signals from interfaces"
extends Modelica.Icons.SensorsPackage;

annotation (Documentation(info="<html>
<p>
This package contains ideal sensors to measure thermal, thermo-physical, or 
hydraulic values. The sensors are ideal, meaning that they do not influence the
model they are connected to. There are three types of sensors. Absolute sensors 
are connected to one port and measure state variables. Relative sensors are 
connected to two ports and measure the difference between two state variables. 
Flow sensors are connected to two ports and measure flow variables.
</p>

<h4>Main equations</h4>
<p>
It is possible to select that the sensor value <i>value</i> is delayed by using 
a time constant <i>tau</i>:
</p>
<pre>
    (dvalue/dt) = (valueNonDelayed - value) / tau;
</pre>
<p>
Although this introduces a dynamic state variable, it can be used to break 
algebraic loops. The non-delayed sensor value can be accessed via the variable
<i>valueNonDelayed</i>.
</p>

<h4>Implemented sensors</h4>
<p>
Three sensors are implemented for thermal ports:
</p>
<ul>
  <li>
  <i>Temperature</i>: Measures the temperature (absolute sensor).
  </li>
  <li>
  <i>Difference temperatures</i>: Measures the difference temperature between two
  pots (relative sensor).
  </li>
  <li>
  <i>Heat flow rate</i>: Measures the heat flow rate (flow sensor).
  </li>
</ul>
<p>
The following sensors are implemented for fluid ports:
<br/>
</p>
<table border=\"1\" cellspacing=\"0\" cellpadding=\"5\">
  <thead>
    <tr>
      <th><b>Sensor</b></th>
      <th><b>Type</b></th>
      <th><b>Liquid</b></th>
      <th><b>Gas / Gas mixture</b></th>
      <th><b>Gas-vapor mixture</b></th>
      <th><b>VLE</b></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Pressure</td>
      <td>Absolute</td>
      <td>X</td>
      <td>X</td>
      <td>X</td>
      <td>X</td>
    </tr>
    <tr>
      <td>Temperature</td>
      <td>Absolute</td>
      <td>X</td>
      <td>X</td>
      <td>X</td>
      <td>X</td>
    </tr>
    <tr>
      <td>Density</td>
      <td>Absolute</td>
      <td>X</td>
      <td>X</td>
      <td>X</td>
      <td>X</td>
    </tr>
    <tr>
      <td>Specific enthalpy</td>
      <td>Absolute</td>
      <td>X</td>
      <td>X</td>
      <td>X</td>
      <td>X</td>
    </tr>
    <tr>
      <td>Specific entropy</td>
      <td>Absolute</td>
      <td>X</td>
      <td>X</td>
      <td>X</td>
      <td>X</td>
    </tr>
    <tr>
      <td>Mass fraction</td>
      <td>Absolute</td>
      <td>X</td>
      <td>X</td>
      <td>X</td>
      <td>X</td>
    </tr>
    <tr>
      <td>Mass fractions</td>
      <td>Absolute</td>
      <td>X</td>
      <td>X</td>
      <td>X</td>
      <td>X</td>
    </tr>
    <tr>
      <td>Vapor quality</td>
      <td>Absolute</td>
      <td></td>
      <td></td>
      <td>X</td>
      <td></td>
    </tr>
    <tr>
      <td>Relative humidity</td>
      <td>Absolute</td>
      <td></td>
      <td></td>
      <td>X</td>
      <td></td>
    </tr>
    <tr>
      <td>Thermodynamic state</td>
      <td>Absolute</td>
      <td>X</td>
      <td>X</td>
      <td>X</td>
      <td>X</td>
    </tr>
    <tr>
      <td>Mass flow rate</td>
      <td>Flow</td>
      <td>X</td>
      <td>X</td>
      <td>X</td>
      <td>X</td>
    </tr>
    <tr>
      <td>Volume flow rate</td>
      <td>Flow</td>
      <td>X</td>
      <td>X</td>
      <td>X</td>
      <td>X</td>
    </tr>
    <tr>
      <td>Pressure difference</td>
      <td>Relative</td>
      <td>X</td>
      <td>X</td>
      <td>X</td>
      <td>X</td>
    </tr>
    <tr>
      <td>Temperature difference</td>
      <td>Relative</td>
      <td>X</td>
      <td>X</td>
      <td>X</td>
      <td>X</td>
    </tr>
  </tbody>
</table>

<h4>Typical use</h4>
<p>
The sensors are typically used to measure quantities for controllers or 
thermal/fluid boundaries.
</p>

<h4>Important parameters and options</h4>
<ul>
  <li>
  <i>useTimeConstant</i>:
  The sensor value is returned with a delay by using a time constant.
  </li>
</ul>

<h4>Dynamics</h4>
<p>
The sensors have a dynamic state (i.e., <i>value</i> ) when a time constant is 
used. Otherwise, there is no dynamic state.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end Sensors;
