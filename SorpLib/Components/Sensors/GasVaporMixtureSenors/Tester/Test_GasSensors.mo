within SorpLib.Components.Sensors.GasVaporMixtureSenors.Tester;
model Test_GasSensors "Tester for gas / gas mixture sensors"
  extends Modelica.Icons.Example;

  //
  // Definition of boundary models
  //
  SorpLib.Basics.Sources.Fluids.GasVaporMixtureSource fs_a(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    use_mFlowInput=true,
    use_TInput=true) "Fluid source a"
    annotation (Placement(transformation(extent={{-70,-10},{-50,10}})));

  SorpLib.Basics.Sources.Fluids.GasVaporMixtureSource fs_b(
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    use_pInput=true,
    use_TInput=true) "Fluid source b"
    annotation (Placement(transformation(extent={{70,-10},{50,10}})));

  //
  // Definition of sensors
  //
  SorpLib.Components.Sensors.GasVaporMixtureSenors.PressureSensor pSensor
    "Pressure sensor"
    annotation (Placement(transformation(extent={{-50,-2},{-30,18}})));
  SorpLib.Components.Sensors.GasVaporMixtureSenors.TemperatureSensor TSensor_a
    "Temperature sensor at port a"
    annotation (Placement(transformation(extent={{-50,58},{-30,78}})));
  SorpLib.Components.Sensors.GasVaporMixtureSenors.TemperatureSensor TSensor_b(
      useTimeConstant=true, tau=10) "Temperature sensor at port b"
    annotation (Placement(transformation(extent={{30,-82},{50,-62}})));
  SorpLib.Components.Sensors.GasVaporMixtureSenors.DensitySensor rhoSensor_a
    "Density sensor at port a"
    annotation (Placement(transformation(extent={{-30,58},{-10,78}})));
  SorpLib.Components.Sensors.GasVaporMixtureSenors.DensitySensor rhoSensor_b(
      useTimeConstant=true, tau=10) "Density sensor at port b"
    annotation (Placement(transformation(extent={{10,-82},{30,-62}})));
  SorpLib.Components.Sensors.GasVaporMixtureSenors.SpecificEnthalpySensor hSensor_a
    "Specific enthalpy sensor at port a"
    annotation (Placement(transformation(extent={{-10,58},{10,78}})));
  SorpLib.Components.Sensors.GasVaporMixtureSenors.SpecificEnthalpySensor hSensor_b(
      useTimeConstant=true, tau=10) "Specific enthalpy sensor at port b"
    annotation (Placement(transformation(extent={{-10,-82},{10,-62}})));
  SorpLib.Components.Sensors.GasVaporMixtureSenors.SpecificEntropySensor sSensor_a
    "Specific entropy sensor at port a"
    annotation (Placement(transformation(extent={{10,58},{30,78}})));
  SorpLib.Components.Sensors.GasVaporMixtureSenors.SpecificEntropySensor sSensor_b(
      useTimeConstant=true, tau=10) "Specific entropy sensor at port b"
    annotation (Placement(transformation(extent={{-30,-82},{-10,-62}})));
  SorpLib.Components.Sensors.GasVaporMixtureSenors.MassFractionsSensor xiSensor_a
    "Mass fractions sensor at port a"
    annotation (Placement(transformation(extent={{30,58},{50,78}})));
  SorpLib.Components.Sensors.GasVaporMixtureSenors.MassFractionsSensor xiSensor_b(
      useTimeConstant=true, tau=10) "Mass fractions sensor at port b"
    annotation (Placement(transformation(extent={{-50,-82},{-30,-62}})));
  SorpLib.Components.Sensors.GasVaporMixtureSenors.RelativeHumiditySensor phiSensor_a
    "Relative humidity sensor at port a"
    annotation (Placement(transformation(extent={{50,58},{70,78}})));
  SorpLib.Components.Sensors.GasVaporMixtureSenors.RelativeHumiditySensor phiSensor_b(
      useTimeConstant=true, tau=10) "Relative humidity sensor at port b"
    annotation (Placement(transformation(extent={{-70,-82},{-50,-62}})));
  SorpLib.Components.Sensors.GasVaporMixtureSenors.ThermodynamicStateSensor stateSensor_a
    "Thermodynamic state sensor at port a"
    annotation (Placement(transformation(extent={{70,58},{90,78}})));
  SorpLib.Components.Sensors.GasVaporMixtureSenors.ThermodynamicStateSensor stateSensor_b
    "Thermodynamic state sensor at port b"
    annotation (Placement(transformation(extent={{-90,-82},{-70,-62}})));

  SorpLib.Components.Sensors.GasVaporMixtureSenors.MassFlowRateSensor mFlowSensor
    "Mass flow rate sensor"
    annotation (Placement(transformation(extent={{-30,-2},{-10,18}})));
  SorpLib.Components.Sensors.GasVaporMixtureSenors.VolumeFlowRateSensor VFlowSensor
    "Volume flow rate sensor"
    annotation (Placement(transformation(extent={{-10,-2},{10,18}})));
  SorpLib.Components.Sensors.GasVaporMixtureSenors.PressureDifferenceSensor dpSensor
    "Pressure difference sensor"
    annotation (Placement(transformation(extent={{10,-2},{30,18}})));
  SorpLib.Components.Sensors.GasVaporMixtureSenors.TemperatureDifferenceSensor dTSensor
    "Temperature difference sensor"
    annotation (Placement(transformation(extent={{30,-2},{50,18}})));

  //
  // Definition of input signals
  //
protected
  Modelica.Blocks.Sources.Ramp input_mFlowVar(
    height=60/60,
    duration=2500,
    offset=-30/60,
    startTime=0)
    "Ramp to simulate input signal of mass flow rate"
    annotation (Placement(transformation(extent={{-100,10},{-80,30}})),
                HideResult=true);
  Modelica.Blocks.Sources.Ramp input_TVar_a(
    height=80,
    duration=2500,
    offset=293.15,
    startTime=0)
    "Ramp to simulate input signal of temperature"
    annotation (Placement(transformation(extent={{-100,-10},{-80,-30}})),
                HideResult=true);

  Modelica.Blocks.Sources.Ramp input_pVar(
    height=-(10e5 - 1e3),
    duration=2500,
    offset=10e5,
    startTime=0)
    "Ramp to simulate input signal of pressure"
    annotation (Placement(transformation(extent={{100,10},{80,30}})),
    HideResult=true);
  Modelica.Blocks.Sources.Sine input_TVar_b(
    amplitude=25,
    f=1/250,
    offset=273.15 + 50)
    "Ramp to simulate input signal of temperature"
    annotation (Placement(transformation(extent={{100,-10},{80,-30}})),
    HideResult=true);

equation
  //
  // Connections
  //
  connect(input_mFlowVar.y, fs_a.m_flow_input) annotation (Line(points={{-79,20},
          {-70,20},{-70,2},{-61.2,2}}, color={0,0,127}));
  connect(input_TVar_a.y, fs_a.T_input) annotation (Line(points={{-79,-20},{-70,
          -20},{-70,-2},{-61.2,-2}}, color={0,0,127}));
  connect(input_pVar.y, fs_b.p_input) annotation (Line(points={{79,20},{70,20},{
          70,5},{61.2,5}}, color={0,0,127}));
  connect(input_TVar_b.y, fs_b.T_input) annotation (Line(points={{79,-20},{70,-20},
          {70,-2},{61.2,-2}}, color={0,0,127}));

  connect(fs_a.port, TSensor_a.port) annotation (Line(
      points={{-60,0},{-50,0},{-50,60},{-40,60}},
      color={244,125,35},
      thickness=1));
  connect(fs_a.port, rhoSensor_a.port) annotation (Line(
      points={{-60,0},{-50,0},{-50,60},{-20,60}},
      color={244,125,35},
      thickness=1));
  connect(fs_a.port, hSensor_a.port) annotation (Line(
      points={{-60,0},{-50,0},{-50,60},{0,60}},
      color={244,125,35},
      thickness=1));
  connect(fs_a.port, sSensor_a.port) annotation (Line(
      points={{-60,0},{-50,0},{-50,60},{20,60}},
      color={244,125,35},
      thickness=1));
  connect(fs_a.port, xiSensor_a.port) annotation (Line(
      points={{-60,0},{-50,0},{-50,60},{40,60}},
      color={244,125,35},
      thickness=1));
  connect(fs_a.port, stateSensor_a.port) annotation (Line(
      points={{-60,0},{-50,0},{-50,60},{80,60}},
      color={244,125,35},
      thickness=1));
  connect(fs_b.port, TSensor_b.port) annotation (Line(
      points={{60,0},{50,0},{50,-80},{40,-80}},
      color={244,125,35},
      thickness=1));
  connect(fs_b.port, rhoSensor_b.port) annotation (Line(
      points={{60,0},{50,0},{50,-80},{20,-80}},
      color={244,125,35},
      thickness=1));
  connect(fs_b.port, hSensor_b.port) annotation (Line(
      points={{60,0},{50,0},{50,-80},{0,-80}},
      color={244,125,35},
      thickness=1));
  connect(fs_b.port, sSensor_b.port) annotation (Line(
      points={{60,0},{50,0},{50,-80},{-20,-80}},
      color={244,125,35},
      thickness=1));
  connect(fs_b.port, xiSensor_b.port) annotation (Line(
      points={{60,0},{50,0},{50,-80},{-40,-80}},
      color={244,125,35},
      thickness=1));
  connect(fs_b.port, stateSensor_b.port) annotation (Line(
      points={{60,0},{50,0},{50,-80},{-80,-80}},
      color={244,125,35},
      thickness=1));
  connect(fs_a.port, pSensor.port) annotation (Line(
      points={{-60,0},{-40,0}},
      color={244,125,35},
      thickness=1));
  connect(pSensor.port, mFlowSensor.port_a) annotation (Line(
      points={{-40,0},{-25,0}},
      color={244,125,35},
      thickness=1));
  connect(mFlowSensor.port_b, VFlowSensor.port_a) annotation (Line(
      points={{-15,0},{-5,0}},
      color={244,125,35},
      thickness=1));
  connect(VFlowSensor.port_b, dpSensor.port_a) annotation (Line(
      points={{5,0},{15,0}},
      color={244,125,35},
      thickness=1));
  connect(dpSensor.port_b, dTSensor.port_a) annotation (Line(
      points={{25,0},{35,0}},
      color={244,125,35},
      thickness=1));
  connect(dTSensor.port_b, fs_b.port) annotation (Line(
      points={{45,0},{60,0}},
      color={244,125,35},
      thickness=1));
  connect(fs_a.port, phiSensor_a.port) annotation (Line(
      points={{-60,0},{-50,0},{-50,60},{60,60}},
      color={244,125,35},
      thickness=1));
  connect(fs_b.port, phiSensor_b.port) annotation (Line(
      points={{60,0},{50,0},{50,-80},{-60,-80}},
      color={244,125,35},
      thickness=1));

  //
  // Annotations
  //
  annotation (experiment(StopTime=2500), Documentation(info="<html>
<p>
This model checks all gas-vapor mixture sensors.
<br/><br/>
To see the model behavior, plot the variables of the models over the 
time. The simulation time is correctly preset (Start: 0 s, Stop = 2500 s). 
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 18, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Test_GasSensors;
