﻿within SorpLib.Components.Sensors.GasVaporMixtureSenors;
model RelativeHumiditySensor "Relative humidity sensor"
  extends SorpLib.Components.Sensors.BaseClasses.PartialAbsoluteFluidSensor(
    redeclare final Basics.Interfaces.FluidPorts.GasPort_in port,
    final no_components=Medium.nX,
    value_initial=0.1);

  //
  // Definition of parameters regarding the medium
  //
  replaceable package Medium =
    SorpLib.Media.IdealGasVaporMixtures.MoistAir_N2_O2_CO2_H2O
    constrainedby
    SorpLib.Media.IdealGasVaporMixtures.Interfaces.PartialIdealGasWaterVaporMixture
    "Medium model"
    annotation (Dialog(tab="General", group="Medium"),
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

protected
  Medium.ThermodynamicState state = Medium.setState_phX(
    p=port.p,
    h=inStream(port.h_outflow),
    X=cat(1,inStream(port.Xi_outflow),{1-sum(inStream(port.Xi_outflow))}))
    "State record";

equation
  //
  // Calculate properties
  //
  valueNonDelayed = Medium.relativeHumidity(state=state)
     "Relative humidity";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This relative humidity sensor reads the relative humidity quality in 0-1. It is 
possible to select that the sensor value <i>value</i> is delayed by using a time 
constant <i>tau</i>:
</p>
<pre>
    (dvalue/dt) = (valueNonDelayed - value) / tau;
</pre>
<p>
Although this introduces a dynamic state variable, it can be used to break 
algebraic loops. The non-delayed sensor value can be accessed via the variable
<i>valueNonDelayed</i>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 18, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"), Icon(graphics={    Line(
          points={{0,-40},{0,-80}},
          color={0,0,0},
          thickness=1), Text(
          extent={{-50,74},{50,-26}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          textStyle={TextStyle.Bold},
          textString="φ"),
        Text(
          extent={{10,80},{90,60}},
          lineColor={0,0,0},
          lineThickness=1,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          textString=String(value*100,significantDigits=3) + " %%rF")}));
end RelativeHumiditySensor;
