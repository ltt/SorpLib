within SorpLib.Components.Sensors.GasVaporMixtureSenors;
model ThermodynamicStateSensor "Thermodynamic state sensor"
  extends
    SorpLib.Components.Sensors.BaseClasses.FluidMSL.ThermodynamicStateSensor(
      redeclare final Basics.Interfaces.FluidPorts.GasPort_in port,
      redeclare replaceable package Medium =
      SorpLib.Media.IdealGasVaporMixtures.MoistAir_N2_O2_CO2_H2O
      constrainedby
      SorpLib.Media.IdealGasVaporMixtures.Interfaces.PartialIdealGasWaterVaporMixture);

  //
  // Definition of variables
  //
  Real phi(min=0, max=1)
     "Relative humidity";

equation
  //
  // Calculate properties
  //
  phi = Medium.relativeHumidity(state=state)
     "Relative humidity";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This thermodynamic state sensor reads all thermodynamic state variables. I.e.,
pressure <i>p</i>, temperature <i>T</i>, specific volume <i>v</i>, specific
enthalpy <i>h</i>, specific internal energy <i>u</i>, specific entropy <i>s</i>,
specific free enthalpy (i.e., Gibbs free energy <i>g</i>, and specific free 
energy (i.e., Helmholts free energy) <i>a</i>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 18, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end ThermodynamicStateSensor;
