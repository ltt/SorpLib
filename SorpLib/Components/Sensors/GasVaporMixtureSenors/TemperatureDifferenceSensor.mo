within SorpLib.Components.Sensors.GasVaporMixtureSenors;
model TemperatureDifferenceSensor "Temperature difference sensor"
  extends
    SorpLib.Components.Sensors.BaseClasses.FluidMSL.TemperatureDifferenceSensor(
      redeclare final Basics.Interfaces.FluidPorts.GasPort_in port_a,
      redeclare final Basics.Interfaces.FluidPorts.GasPort_out port_b,
      redeclare replaceable package Medium =
      SorpLib.Media.IdealGasVaporMixtures.MoistAir_N2_O2_CO2_H2O
      constrainedby
      SorpLib.Media.IdealGasVaporMixtures.Interfaces.PartialIdealGasWaterVaporMixture);

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This temperature difference sensor reads the temperature difference in K. It is 
possible to select that the sensor value <i>value</i> is delayed by using a time  
constant <i>tau</i>:
</p>
<pre>
    (dvalue/dt) = (valueNonDelayed - value) / tau;
</pre>
<p>
Although this introduces a dynamic state variable, it can be used to break 
algebraic loops. The non-delayed sensor value can be accessed via the variable
<i>valueNonDelayed</i>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 18, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"), Icon(graphics={Line(
          points={{-50,-80},{50,-80}},
          color={244,125,35},
          thickness=1)}));
end TemperatureDifferenceSensor;
