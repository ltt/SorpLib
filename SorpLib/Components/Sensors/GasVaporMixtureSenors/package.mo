within SorpLib.Components.Sensors;
package GasVaporMixtureSenors "Gas-vapor mixture sensors"
  extends Modelica.Icons.VariantsPackage;

  annotation (Documentation(info="<html>
<p>
This package contains gas-vapor mixture sensors based on the open-source Modelica 
Standard Library (MSL). It implements absolute sensors, which are only connected to  
one fluid port, as well as relative and flow sensors, which are connected between 
two fluid ports.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 18, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end GasVaporMixtureSenors;
