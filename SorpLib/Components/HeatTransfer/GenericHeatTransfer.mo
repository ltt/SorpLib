within SorpLib.Components.HeatTransfer;
model GenericHeatTransfer
  "Model calculating a generic heat transfer"
  extends BaseClasses.PartialHeatTransfer(
    final exponetTemperature=1,
    redeclare replaceable model HeatTransferCoefficient =
      SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.Generic.ConstantAlpha
      constrainedby
      SorpLib.Components.HeatTransfer.BaseClasses.PartialGenericHeatTransferCoefficient);

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
The generic heat transfer model is used to thermally connect models which can 
exchange heat. Thus, the generic heat transfer model represents a thermal 
resistance betweed two models. Depending on the attached temperatures (i.e., 
attached potential) and the chosen transport phenomena, this model determines the 
heat flow rate between the connected models. 
<br/><br/>
The model has two heat ports, which sizes can be defined via a parameter. The 
scalable heat ports enable the connection of various models. For example, this 
enables to connect a discretized model to a lumped model, or two different
disretized models.
</p>

<h4>Main equations</h4>
<p>
The model has steady-state energy balance:
</p>
<pre>
    0 = &sum; (hp_a.Q_flow) + &sum; (hp_b.Q_flow);
</pre>
<p>
The heat flow rate <i>Q_flow</i> is proportional to the driving temperature
difference <i>&Delta;T = hp_a.T - hp_b.T</i> and the production of heat transfer
coefficient and area <i>&alpha;A</i>. This product can be given as an input or
calculated using models describing heat transfer coefficient correlations.
</p>
<pre>
    hp_a.Q_flow = &alpha;A * &Delta;T = &alpha;A * (hp_a.T - hp_b.T);
</pre>

<h4>Assumptions and limitations</h4>
<ul>
  <li>
  Steady-state process
  </li>
  <li>
  No storage of mass or energy
  </li>
  <li>
  The number of ports a and b must be an even integer multiple of each other.
  </li>
</ul>

<h4>Typical use</h4>
<p>
The generic heat trasnfer is typically used to desribe the heat transfer between
two components via simple models for the heat transfer coefficient.
</p>

<h4>Important parameters and options</h4>
<ul>
  <li>
  <i>useAlphaAInput</i>:
  Defines if the product of heat transfer coefficient and area is given via
  an input or calculated using an approproate correlation model for the heat
  transfer coefficient.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  January 12, 2024, by Mirko Engelpracht:<br/>
  Separation into a partial model and documentation.
  </li>
  <li>
  January 13, 2021, by Mirko Engelpracht:<br/>
  Major revision allowing different discretizations for both ports.
  </li>
  <li>
  December 06, 2017, by Andrej Gibelhaus:<br/>
  Tidy up implementation and enhance documentation for publication of library.
  </li>
</ul>
</html>"), Icon(graphics={Text(
          extent={{-60,20},{60,-20}},
          lineColor={0,0,0},
          fillPattern=FillPattern.HorizontalCylinder,
          fillColor={238,46,47},
          textString="Q_flow = kA * DT")}));
end GenericHeatTransfer;
