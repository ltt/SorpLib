within SorpLib.Components.HeatTransfer.Tester;
model Test_GenericHeatTransfer
  "Tester for the generic heat transfer"
  extends Modelica.Icons.Example;

  //
  // Definition of boundary models
  //
  SorpLib.Basics.Sources.Thermal.HeatSource hs_a1(use_TInput=true)
    "Heat source a"
    annotation (Placement(transformation(extent={{-70,30},{-50,50}})));

  SorpLib.Basics.Sources.Thermal.HeatSource hs_b1(use_TInput=true)
    "Heat source b"
    annotation (Placement(transformation(extent={{70,30},{50,50}})));
  SorpLib.Basics.Sources.Thermal.HeatSource hs_a2(use_TInput=true)
    "Heat source a"
    annotation (Placement(transformation(extent={{-70,10},{-50,30}})));
  SorpLib.Basics.Sources.Thermal.HeatSource hs_b2(use_TInput=true)
    "Heat source b"
    annotation (Placement(transformation(extent={{70,10},{50,30}})));
  SorpLib.Basics.Sources.Thermal.HeatSource hs_a3(use_TInput=true)
    "Heat source a"
    annotation (Placement(transformation(extent={{-70,-10},{-50,10}})));
  SorpLib.Basics.Sources.Thermal.HeatSource hs_b3(use_TInput=true)
    "Heat source b"
    annotation (Placement(transformation(extent={{70,-10},{50,10}})));
  SorpLib.Basics.Sources.Thermal.HeatSource hs_a4(use_TInput=true)
    "Heat source a"
    annotation (Placement(transformation(extent={{-70,-30},{-50,-10}})));
  SorpLib.Basics.Sources.Thermal.HeatSource hs_b4(use_TInput=true)
    "Heat source b"
    annotation (Placement(transformation(extent={{70,-30},{50,-10}})));
  SorpLib.Basics.Sources.Thermal.HeatSource hs_a5(use_TInput=true)
    "Heat source a"
    annotation (Placement(transformation(extent={{-70,-50},{-50,-30}})));
  SorpLib.Basics.Sources.Thermal.HeatSource hs_b5(use_TInput=true)
    "Heat source b"
    annotation (Placement(transformation(extent={{70,-50},{50,-30}})));

  //
  // Definition of heat transfer models
  //
  SorpLib.Components.HeatTransfer.GenericHeatTransfer heatTransfer_1(
    redeclare model HeatTransferCoefficient =
      HeatTransferCoefficientCorrelations.Generic.ConstantAlpha (
        constantAlpha=25, A=1))
    "Heat transfer model 1"
    annotation (Placement(transformation(extent={{-10,30},{10,50}})));
  SorpLib.Components.HeatTransfer.GenericHeatTransfer heatTransfer_2(redeclare
      model HeatTransferCoefficient =
        HeatTransferCoefficientCorrelations.Generic.ConstantAlphaA (
          constantAlphaA=25))
    "Heat transfer model 2"
    annotation (Placement(transformation(extent={{-10,10},{10,30}})));
  SorpLib.Components.HeatTransfer.GenericHeatTransfer heatTransfer_3(redeclare
      model HeatTransferCoefficient =
        HeatTransferCoefficientCorrelations.Generic.LinearAlphaA (
          constantAlphaA=25, b=0.1))
    "Heat transfer model 3"
    annotation (Placement(transformation(extent={{-10,-10},{10,10}})));
  SorpLib.Components.HeatTransfer.GenericHeatTransfer heatTransfer_4(redeclare
      model HeatTransferCoefficient =
        HeatTransferCoefficientCorrelations.Generic.ExponentialAlphaA (
        constantAlphaA=25,
        b=0.5,
        c=1/273.15))
    "Heat transfer model 4"
    annotation (Placement(transformation(extent={{-10,-30},{10,-10}})));
  SorpLib.Components.HeatTransfer.GenericHeatTransfer heatTransfer_5(redeclare
      model HeatTransferCoefficient =
        HeatTransferCoefficientCorrelations.Generic.PolynomialAlphaA (
        constantAlphaA=25,
        b=0.5,
        c=1/3))
    "Heat transfer model 5"
    annotation (Placement(transformation(extent={{-10,-50},{10,-30}})));

  //
  // Definition of input signals
  //
protected
  Modelica.Blocks.Sources.Sine input_T_a(
    amplitude=50,
    f=1/250,
    offset=273.15 + 50)
    "Input signal for temperature at sources a"
    annotation (Placement(transformation(extent={{-100,-10},{-80,10}})));

  Modelica.Blocks.Sources.Trapezoid input_T_b(
    amplitude=100,
    rising=50,
    width=100,
    falling=50,
    period=250,
    offset=273.15)
    "Input signal for temperature at sources b"
    annotation (Placement(transformation(extent={{100,-10},{80,10}})));

equation
  //
  // Connections
  //
  connect(hs_a1.port, heatTransfer_1.hp_a[1]) annotation (Line(
      points={{-60,40},{-8,40}},
      color={238,46,47},
      thickness=1));
  connect(heatTransfer_1.hp_b[1], hs_b1.port) annotation (Line(
      points={{8,40},{60,40}},
      color={238,46,47},
      thickness=1));
  connect(hs_a2.port, heatTransfer_2.hp_a[1]) annotation (Line(
      points={{-60,20},{-8,20}},
      color={238,46,47},
      thickness=1));
  connect(heatTransfer_2.hp_b[1], hs_b2.port) annotation (Line(
      points={{8,20},{60,20}},
      color={238,46,47},
      thickness=1));
  connect(hs_a3.port, heatTransfer_3.hp_a[1]) annotation (Line(
      points={{-60,0},{-8,0}},
      color={238,46,47},
      thickness=1));
  connect(heatTransfer_3.hp_b[1], hs_b3.port) annotation (Line(
      points={{8,0},{60,0}},
      color={238,46,47},
      thickness=1));
  connect(hs_a4.port, heatTransfer_4.hp_a[1]) annotation (Line(
      points={{-60,-20},{-8,-20}},
      color={238,46,47},
      thickness=1));
  connect(heatTransfer_4.hp_b[1], hs_b4.port) annotation (Line(
      points={{8,-20},{60,-20}},
      color={238,46,47},
      thickness=1));
  connect(hs_a5.port, heatTransfer_5.hp_a[1]) annotation (Line(
      points={{-60,-40},{-8,-40}},
      color={238,46,47},
      thickness=1));
  connect(heatTransfer_5.hp_b[1], hs_b5.port) annotation (Line(
      points={{8,-40},{60,-40}},
      color={238,46,47},
      thickness=1));

  connect(input_T_a.y, hs_a1.T_input) annotation (Line(points={{-79,0},{-70,0},{
          -70,45.2},{-61,45.2}}, color={0,0,127}));
  connect(input_T_b.y, hs_b1.T_input) annotation (Line(points={{79,0},{70,0},{70,
          45.2},{61,45.2}}, color={0,0,127}));
  connect(input_T_a.y, hs_a2.T_input) annotation (Line(points={{-79,0},{-70,0},{
          -70,25.2},{-61,25.2}}, color={0,0,127}));
  connect(input_T_a.y, hs_a3.T_input) annotation (Line(points={{-79,0},{-70,0},{
          -70,5.2},{-61,5.2}}, color={0,0,127}));
  connect(input_T_a.y, hs_a4.T_input) annotation (Line(points={{-79,0},{-70,0},{
          -70,-14.8},{-61,-14.8}}, color={0,0,127}));
  connect(input_T_a.y, hs_a5.T_input) annotation (Line(points={{-79,0},{-70,0},{
          -70,-34.8},{-61,-34.8}}, color={0,0,127}));
  connect(input_T_b.y, hs_b2.T_input) annotation (Line(points={{79,0},{70,0},{70,
          25.2},{61,25.2}}, color={0,0,127}));
  connect(input_T_b.y, hs_b3.T_input) annotation (Line(points={{79,0},{70,0},{70,
          5.2},{61,5.2}}, color={0,0,127}));
  connect(input_T_b.y, hs_b4.T_input) annotation (Line(points={{79,0},{70,0},{70,
          -14.8},{61,-14.8}}, color={0,0,127}));
  connect(input_T_b.y, hs_b5.T_input) annotation (Line(points={{79,0},{70,0},{70,
          -34.8},{61,-34.8}}, color={0,0,127}));

  //
  // Annotations
  //
  annotation (experiment(StopTime=2500), Documentation(info="<html>
<p>
This model checks the generic heat transfer.
<br/><br/>
To see the model behavior, plot the variables of the models over the 
time. The simulation time is correctly preset (Start: 0 s, Stop = 2500 s). 
</p>
</html>", revisions="<html>
<ul>
  <li>
  January 12, 2024, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Test_GenericHeatTransfer;
