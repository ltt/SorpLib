within SorpLib.Components.HeatTransfer.Tester;
model Test_RadiationHeatTransfer "Tester for the radiation heat transfer"
  extends Modelica.Icons.Example;

  //
  // Definition of parameters
  //
  parameter Modelica.Units.SI.Pressure p = 1e5
    "Constant pressure"
    annotation (Dialog(tab="General", group="Case Study"));

  //
  // Definition of boundary models
  //
  SorpLib.Basics.Sources.Thermal.HeatSource hs_a1(use_TInput=true)
    "Heat source a"
    annotation (Placement(transformation(extent={{-70,10},{-50,30}})));
  SorpLib.Basics.Sources.Thermal.HeatSource hs_b1(use_TInput=true)
    "Heat source b"
    annotation (Placement(transformation(extent={{70,10},{50,30}})));
  SorpLib.Basics.Sources.Thermal.HeatSource hs_a2(use_TInput=true)
    "Heat source a"
    annotation (Placement(transformation(extent={{-70,-10},{-50,10}})));
  SorpLib.Basics.Sources.Thermal.HeatSource hs_b2(use_TInput=true)
    "Heat source b"
    annotation (Placement(transformation(extent={{70,-10},{50,10}})));
  SorpLib.Basics.Sources.Thermal.HeatSource hs_a3(use_TInput=true)
    "Heat source a"
    annotation (Placement(transformation(extent={{-70,-30},{-50,-10}})));
  SorpLib.Basics.Sources.Thermal.HeatSource hs_b3(use_TInput=true)
    "Heat source b"
    annotation (Placement(transformation(extent={{70,-30},{50,-10}})));

  //
  // Definition of heat transfer models
  //
  SorpLib.Components.HeatTransfer.ConductionHeatTransfer ht_a1(redeclare model
      HeatTransferCoefficient =
      HeatTransferCoefficientCorrelations.Conduction.ConstantResistance,
      fluidProperties=SorpLib.Components.HeatTransfer.Records.FluidProperties(
      p=wall1.solidProperties.p,
      T=wall1.solidProperties.T,
      rho=1/wall1.solidProperties.v,
      cp=wall1.solidProperties.c,
      eta=0,
      lambda=wall1.solidProperties.lambda))
    "Heat transfer at heat source a"
    annotation (Placement(transformation(extent={{-40,10},{-20,30}})));
  SorpLib.Components.HeatTransfer.ConductionHeatTransfer ht_b1(redeclare model
      HeatTransferCoefficient =
      HeatTransferCoefficientCorrelations.Conduction.ConstantResistance,
      fluidProperties=SorpLib.Components.HeatTransfer.Records.FluidProperties(
      p=wall1.solidProperties.p,
      T=wall1.solidProperties.T,
      rho=1/wall1.solidProperties.v,
      cp=wall1.solidProperties.c,
      eta=0,
      lambda=wall1.solidProperties.lambda))
    "Heat transfer at heat source b"
    annotation (Placement(transformation(extent={{20,10},{40,30}})));

  SorpLib.Components.HeatTransfer.ConductionHeatTransfer ht_a2(redeclare model
      HeatTransferCoefficient =
        HeatTransferCoefficientCorrelations.Conduction.ConstantPlainWall,
      fluidProperties=SorpLib.Components.HeatTransfer.Records.FluidProperties(
      p=wall2.solidProperties.p,
      T=wall2.solidProperties.T,
      rho=1/wall2.solidProperties.v,
      cp=wall2.solidProperties.c,
      eta=0,
      lambda=wall2.solidProperties.lambda))
    "Heat transfer at heat source a"
    annotation (Placement(transformation(extent={{-40,-10},{-20,10}})));
  SorpLib.Components.HeatTransfer.ConductionHeatTransfer ht_b2(redeclare model
      HeatTransferCoefficient =
        HeatTransferCoefficientCorrelations.Conduction.ConstantPlainWall,
      fluidProperties=SorpLib.Components.HeatTransfer.Records.FluidProperties(
      p=wall2.solidProperties.p,
      T=wall2.solidProperties.T,
      rho=1/wall2.solidProperties.v,
      cp=wall2.solidProperties.c,
      eta=0,
      lambda=wall2.solidProperties.lambda))
    "Heat transfer at heat source b"
    annotation (Placement(transformation(extent={{20,-10},{40,10}})));

  SorpLib.Components.HeatTransfer.ConductionHeatTransfer ht_a3(redeclare model
      HeatTransferCoefficient =
        HeatTransferCoefficientCorrelations.Conduction.ConstantCylindricalWall,
      fluidProperties=SorpLib.Components.HeatTransfer.Records.FluidProperties(
      p=wall3.solidProperties.p,
      T=wall3.solidProperties.T,
      rho=1/wall3.solidProperties.v,
      cp=wall3.solidProperties.c,
      eta=0,
      lambda=wall3.solidProperties.lambda))
    "Heat transfer at heat source a"
    annotation (Placement(transformation(extent={{-40,-30},{-20,-10}})));
  SorpLib.Components.HeatTransfer.ConductionHeatTransfer ht_b3(redeclare model
      HeatTransferCoefficient =
        HeatTransferCoefficientCorrelations.Conduction.ConstantCylindricalWall,
      fluidProperties=SorpLib.Components.HeatTransfer.Records.FluidProperties(
      p=wall3.solidProperties.p,
      T=wall3.solidProperties.T,
      rho=1/wall3.solidProperties.v,
      cp=wall3.solidProperties.c,
      eta=0,
      lambda=wall3.solidProperties.lambda))
    "Heat transfer at heat source b"
    annotation (Placement(transformation(extent={{20,-30},{40,-10}})));

  //
  // Definition of wall models
  //
  SorpLib.Basics.Volumes.SolidVolumes.SolidVolume wall1(
    solidMedium(approach_c=SorpLib.Choices.SpecificHeatCapacitySolid.GeneralizedFunction,
        approach_lambda=SorpLib.Choices.ThermalConductivitySolid.GeneralizedFunction),
    T_initial=298.15,
    p=p) "Wall model"
    annotation (Placement(transformation(extent={{-10,10},{10,30}})));
  SorpLib.Basics.Volumes.SolidVolumes.SolidVolume wall2(
    solidMedium(approach_c=SorpLib.Choices.SpecificHeatCapacitySolid.GeneralizedFunction,
        approach_lambda=SorpLib.Choices.ThermalConductivitySolid.GeneralizedFunction),
    T_initial=298.15,
    p=p) "Wall model"
    annotation (Placement(transformation(extent={{-10,-10},{10,10}})));
  SorpLib.Basics.Volumes.SolidVolumes.SolidVolume wall3(
    solidMedium(approach_c=SorpLib.Choices.SpecificHeatCapacitySolid.GeneralizedFunction,
        approach_lambda=SorpLib.Choices.ThermalConductivitySolid.GeneralizedFunction),
    T_initial=298.15,
    p=p) "Wall model"
    annotation (Placement(transformation(extent={{-10,-30},{10,-10}})));

  //
  // Definition of input signals
  //
protected
  Modelica.Blocks.Sources.Sine input_T_a(
    amplitude=50,
    f=1/250,
    offset=273.15 + 50)
    "Input signal for temperature at sources a"
    annotation (Placement(transformation(extent={{-100,-10},{-80,10}})));

  Modelica.Blocks.Sources.Trapezoid input_T_b(
    amplitude=100,
    rising=50,
    width=100,
    falling=50,
    period=250,
    offset=273.15)
    "Input signal for temperature at sources b"
    annotation (Placement(transformation(extent={{100,-10},{80,10}})));

equation
  //
  // Connections
  //
  connect(hs_a1.port, ht_a1.hp_a[1]) annotation (Line(
      points={{-60,20},{-38,20}},
      color={238,46,47},
      thickness=1));
  connect(ht_a1.hp_b[1], wall1.hp_xMinus) annotation (Line(
      points={{-22,20},{-6,20}},
      color={238,46,47},
      thickness=1));
  connect(wall1.hp_xPlus, ht_b1.hp_a[1]) annotation (Line(
      points={{6,20},{22,20}},
      color={238,46,47},
      thickness=1));
  connect(ht_b1.hp_b[1], hs_b1.port) annotation (Line(
      points={{38,20},{60,20}},
      color={238,46,47},
      thickness=1));
  connect(hs_a2.port, ht_a2.hp_a[1]) annotation (Line(
      points={{-60,0},{-38,0}},
      color={238,46,47},
      thickness=1));
  connect(ht_a2.hp_b[1], wall2.hp_xMinus) annotation (Line(
      points={{-22,0},{-6,0}},
      color={238,46,47},
      thickness=1));
  connect(wall2.hp_xPlus, ht_b2.hp_a[1]) annotation (Line(
      points={{6,0},{22,0}},
      color={238,46,47},
      thickness=1));
  connect(ht_b2.hp_b[1], hs_b2.port) annotation (Line(
      points={{38,0},{60,0}},
      color={238,46,47},
      thickness=1));
  connect(hs_a3.port, ht_a3.hp_a[1]) annotation (Line(
      points={{-60,-20},{-38,-20}},
      color={238,46,47},
      thickness=1));
  connect(ht_a3.hp_b[1], wall3.hp_xMinus) annotation (Line(
      points={{-22,-20},{-6,-20}},
      color={238,46,47},
      thickness=1));
  connect(wall3.hp_xPlus, ht_b3.hp_a[1]) annotation (Line(
      points={{6,-20},{22,-20}},
      color={238,46,47},
      thickness=1));
  connect(ht_b3.hp_b[1], hs_b3.port) annotation (Line(
      points={{38,-20},{60,-20}},
      color={238,46,47},
      thickness=1));

  connect(input_T_a.y, hs_a1.T_input) annotation (Line(points={{-79,0},{-70,0},
          {-70,25.2},{-61,25.2}},color={0,0,127}));
  connect(input_T_b.y, hs_b1.T_input) annotation (Line(points={{79,0},{70,0},{
          70,25.2},{61,25.2}},
                            color={0,0,127}));
  connect(input_T_a.y, hs_a2.T_input) annotation (Line(points={{-79,0},{-70,0},
          {-70,5.2},{-61,5.2}},  color={0,0,127}));
  connect(input_T_a.y, hs_a3.T_input) annotation (Line(points={{-79,0},{-70,0},
          {-70,-14.8},{-61,-14.8}},
                               color={0,0,127}));
  connect(input_T_b.y, hs_b2.T_input) annotation (Line(points={{79,0},{70,0},{
          70,5.2},{61,5.2}},color={0,0,127}));
  connect(input_T_b.y, hs_b3.T_input) annotation (Line(points={{79,0},{70,0},{
          70,-14.8},{61,-14.8}},
                          color={0,0,127}));

  //
  // Annotations
  //
  annotation (experiment(StopTime=2500), Documentation(info="<html>
<p>
This model checks the radiation heat transfer.
<br/><br/>
To see the model behavior, plot the variables of the models over the 
time. The simulation time is correctly preset (Start: 0 s, Stop = 2500 s). 
</p>
</html>", revisions="<html>
<ul>
  <li>
  January 16, 2024, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Test_RadiationHeatTransfer;
