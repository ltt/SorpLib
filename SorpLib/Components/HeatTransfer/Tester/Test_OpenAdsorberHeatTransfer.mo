within SorpLib.Components.HeatTransfer.Tester;
model Test_OpenAdsorberHeatTransfer
  "Tester for the heat transfer between gas and sorbent/casing in open adsorbers"
  extends Modelica.Icons.Example;

  //
  // Definition of parameters
  //
  replaceable package Medium =
    SorpLib.Media.IdealGasVaporMixtures.MoistAir_N2_O2_CO2_H2O
    constrainedby
    SorpLib.Media.IdealGasVaporMixtures.Interfaces.PartialIdealGasWaterVaporMixture
    "Medium model"
    annotation (Dialog(tab="General", group="Media"),
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

  //
  // Definition of boundary models
  //
  SorpLib.Basics.Sources.Fluids.GasVaporMixtureSource fs_inlet(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    use_mFlowInput=true,
    T_fixed=323.15,
    redeclare final package Medium = Medium)
    "Inlet fluid source"
    annotation (Placement(transformation(extent={{-50,-10},{-30,10}})));
  SorpLib.Basics.Sources.Fluids.GasVaporMixtureSource fs_outlet(
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    T_fixed=373.15,
    redeclare final package Medium = Medium)
    "Outlet fluid source"
    annotation (Placement(transformation(extent={{30,-10},{50,10}})));

  //
  // Definition of heat transfer models
  //
  SorpLib.Components.HeatTransfer.OpenAdsorberHeatTransfer heatTransfer_gasCasing(
    redeclare model HeatTransferCoefficient =
      HeatTransferCoefficientCorrelations.OpenAdsorber.CasingGasKast,
    fluidProperties=SorpLib.Components.HeatTransfer.Records.FluidProperties(
      p=gasVolume.fluidProperties.p,
      T=gasVolume.fluidProperties.T,
      rho=1/gasVolume.fluidProperties.v,
      cp=gasVolume.fluidProperties.cp,
      eta=gasVolume.fluidProperties.eta,
      lambda=gasVolume.fluidProperties.lambda),
      m_hyd_xMinus=gasVolume.fluidProperties.mc_flow_xMinus,
      m_hyd_xPlus=gasVolume.fluidProperties.mc_flow_xPlus)
    "Heat transfer model between gas and casing" annotation (Placement(
        transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={0,32})));

  SorpLib.Components.HeatTransfer.OpenAdsorberHeatTransfer heatTransfer_gasSorbent(
    redeclare model HeatTransferCoefficient =
      HeatTransferCoefficientCorrelations.OpenAdsorber.CasingSorbentKast,
    fluidProperties=SorpLib.Components.HeatTransfer.Records.FluidProperties(
      p=gasVolume.fluidProperties.p,
      T=gasVolume.fluidProperties.T,
      rho=1/gasVolume.fluidProperties.v,
      cp=gasVolume.fluidProperties.cp,
      eta=gasVolume.fluidProperties.eta,
      lambda=gasVolume.fluidProperties.lambda),
      m_hyd_xMinus=gasVolume.fluidProperties.mc_flow_xMinus,
      m_hyd_xPlus=gasVolume.fluidProperties.mc_flow_xPlus)
    "Heat transfer model between gas and sorbent" annotation (Placement(
        transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={0,-32})));

  //
  // Definition of volume models
  //
  SorpLib.Basics.Volumes.AdsorbateVolumes.AdsorbatePureGasVolume adsorbateVolume(
    T_initial=323.15,
    useHeatPorts=false,
    useHeatPortsX=false,
    geometry(V=(1-heatTransfer_gasSorbent.geometry.psi_particles)*heatTransfer_gasSorbent.geometry.V_inner_cas),
    redeclare model PureWorkingPairModel =
        Media.WorkingPairs.PureComponents.H2O.Zeolith13X_Toth_WangDouglasLeVan2010_Gas,
    x_initial=0.05)
    "Model of an adsorbate volume"
    annotation (Placement(transformation(
        extent={{-20,-20},{20,20}},
        rotation=0,
        origin={0,-70})));

  SorpLib.Basics.Volumes.FluidVolumes.GasVaporMixtureVolume gasVolume(
    geometry(V=heatTransfer_gasSorbent.geometry.V_free_cas),
    calculateAdditionalProperties=true,
    type_overallMassBalance=SorpLib.Choices.BalanceEquations.TransientFreeInitial,
    nPorts_cfp_xMinus=1,
    nPorts_cfp_xPlus=1,
    redeclare final package Medium = Medium)
    "Model of the gas volume"
    annotation (Placement(transformation(extent={{-20,-20},{20,20}})));

  SorpLib.Basics.Volumes.SolidVolumes.SolidVolume wallVolume(
    p = gasVolume.fluidProperties.p,
    T_initial=323.15,
    useHeatPortsX=false,
    useHeatPortsY=true,
    geometry(V=heatTransfer_gasSorbent.geometry.V_wall_cas))
    "Model of the wall volume"
    annotation (Placement(
        transformation(
        extent={{-20,-20},{20,20}},
        rotation=0,
        origin={0,70})));

  //
  // Definition of input signals
  //
protected
  Modelica.Blocks.Sources.Sine input_mFlow(
    amplitude=1,
    f=1/250,
    startTime=250) "Input for mass flow rate" annotation (Placement(
        transformation(
        extent={{10,10},{-10,-10}},
        rotation=180,
        origin={-70,0})));

equation
  //
  // Connections
  //
  connect(fs_inlet.port, gasVolume.cfp_xMinus[1]) annotation (Line(
      points={{-40,0},{-30,0},{-30,3.6},{-8.4,3.6}},
      color={244,125,35},
      thickness=1));
  connect(fs_outlet.port, gasVolume.cfp_xPlus[1]) annotation (Line(
      points={{40,0},{30,0},{30,3.6},{15.6,3.6}},
      color={244,125,35},
      thickness=1));

  connect(wallVolume.hp_yMinus, heatTransfer_gasCasing.hp_b[1]) annotation (
      Line(
      points={{0,58},{0,40}},
      color={238,46,47},
      thickness=1));
  connect(heatTransfer_gasCasing.hp_a[1], gasVolume.hp_yPlus) annotation (Line(
      points={{0,24},{0,12}},
      color={238,46,47},
      thickness=1));
  connect(heatTransfer_gasSorbent.hp_a[1], gasVolume.hp_sorption) annotation (
      Line(
      points={{0,-24},{0,-20},{3.2,-20},{3.2,-8.8}},
      color={238,46,47},
      thickness=1));
  connect(heatTransfer_gasSorbent.hp_b[1], adsorbateVolume.hp_sorption)
    annotation (Line(
      points={{0,-40},{0,-44},{3.2,-44},{3.2,-54.8}},
      color={238,46,47},
      thickness=1));

  connect(input_mFlow.y, fs_inlet.m_flow_input) annotation (Line(points={{-59,0},
          {-50,0},{-50,2},{-41.2,2}}, color={0,0,127}));
  //
  // Annotations
  //
  annotation (experiment(StopTime=2500), Documentation(info="<html>
<p>
This model checks the heat transfer between gas and sorbent/casing within 
open adsorbers.
<br/><br/>
To see the model behavior, plot the variables of the models over the 
time. The simulation time is correctly preset (Start: 0 s, Stop = 2500 s). 
</p>
</html>", revisions="<html>
<ul>
  <li>
  January 23, 2024, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Test_OpenAdsorberHeatTransfer;
