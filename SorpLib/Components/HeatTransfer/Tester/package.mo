within SorpLib.Components.HeatTransfer;
package Tester "Models to test and varify heat transfer models"
  extends Modelica.Icons.ExamplesPackage;

  annotation (Documentation(info="<html>
<p>
This package contains executable test models for all implemented heat transfer 
models. 
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructering the library.
  </li>
</ul>
</html>"));
end Tester;
