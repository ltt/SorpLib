within SorpLib.Components.HeatTransfer.Tester;
model Test_TubeInsideHeatTransfer
  "Tester for the convective heat transfer within tubes"
  extends Modelica.Icons.Example;

  //
  // Definition of parameters
  //
  replaceable package Medium = Modelica.Media.Water.ConstantPropertyLiquidWater
    constrainedby Modelica.Media.Interfaces.PartialMedium
    "Medium model"
    annotation (Dialog(tab="General", group="Media"),
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

  //
  // Definition of boundary models
  //
  SorpLib.Basics.Sources.Fluids.LiquidSource outlet(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    use_mFlowInput=false,
    T_fixed=298.15,
    redeclare package Medium = Medium)
    "Outlet of the liquid volume"
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={50,0})));
  SorpLib.Basics.Sources.Fluids.LiquidSource inlet(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    use_mFlowInput=true,
    T_fixed=323.15,
    redeclare package Medium = Medium)
    "Inlet of liquid volume"
    annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=180,
        origin={-50,0})));

  SorpLib.Basics.Sources.Thermal.HeatSource heatSource(
    boundaryType=SorpLib.Choices.BoundaryThermal.Temperature,
    use_TInput=true,
    use_QFlowInput=false)
    "Heat source for fluid volume"
    annotation (Placement(transformation(
                extent={{-10,-10},{10,10}},
                rotation=270,
                origin={0,60})));

  //
  // Definition of heat transfer models
  //
  SorpLib.Components.HeatTransfer.TubeInsideHeatTransfer heatTransfer(
    redeclare model HeatTransferCoefficient =
        HeatTransferCoefficientCorrelations.TubeInside.Schmidt,
    fluidProperties=SorpLib.Components.HeatTransfer.Records.FluidProperties(
      p=liquidVolume.fluidProperties.p,
      T=liquidVolume.fluidProperties.T,
      rho=1/liquidVolume.fluidProperties.v,
      cp=liquidVolume.fluidProperties.cp,
      eta=liquidVolume.fluidProperties.eta,
      lambda=liquidVolume.fluidProperties.lambda),
    m_hyd_xMinus=liquidVolume.fluidProperties.mc_flow_xMinus,
    m_hyd_xPlus=liquidVolume.fluidProperties.mc_flow_xPlus)
    "Heat transfer model"
    annotation (Placement(transformation(extent={{-10,-10},{10,10}},
        rotation=90,
        origin={0,40})));

  //
  // Definition of liquid volume models
  //
  SorpLib.Basics.Volumes.FluidVolumes.LiquidVolume liquidVolume(
    T_initial=348.15,
    geometry(V=Modelica.Constants.pi/4*0.01^2*1),
    type_energyBalance=SorpLib.Choices.BalanceEquations.TransientFixedInitial,
    calculateAdditionalProperties=true,
    type_overallMassBalance=SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial,
    redeclare final package Medium = Medium,
    nPorts_cfp_xMinus=1,
    nPorts_cfp_xPlus=1)
    "Model of a liquid volume"
    annotation (Placement(transformation(
        extent={{-20,-20},{20,20}},
        rotation=0,
        origin={0,0})));

  //
  // Definition of fluid boundaries
  //
protected
  Modelica.Blocks.Sources.Sine input_mFlow_inlet(
    amplitude=10,
    f=1/250,
    startTime=50)
    "Input for mass flow rate at inlet port"
    annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={-80,0})));

  //
  // Definition of thermal boundaries
  //
  Modelica.Blocks.Sources.Ramp input_T(
    height=-75,
    duration=1500,
    offset=273.15 + 75,
    startTime=500)
    "Input for temperature"
    annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={0,80})));

equation
  //
  // Connections
  //
  connect(inlet.port, liquidVolume.cfp_xMinus[1]) annotation (Line(
      points={{-50,0},{-40,0},{-40,3.6},{-8.4,3.6}},
      color={28,108,200},
      thickness=1));
  connect(outlet.port, liquidVolume.cfp_xPlus[1]) annotation (Line(
      points={{50,0},{40,0},{40,3.6},{15.6,3.6}},
      color={28,108,200},
      thickness=1));
  connect(heatSource.port, heatTransfer.hp_b[1]) annotation (Line(
      points={{0,60},{0,48}},
      color={238,46,47},
      thickness=1));
  connect(heatTransfer.hp_a[1], liquidVolume.hp_yPlus) annotation (Line(
      points={{0,32},{0,12}},
      color={238,46,47},
      thickness=1));

  connect(input_mFlow_inlet.y, inlet.m_flow_input) annotation (Line(points={{-69,
          0},{-60,0},{-60,-2},{-51.2,-2}}, color={0,0,127}));
  connect(input_T.y, heatSource.T_input) annotation (Line(points={{0,69},{0,64},
          {5.2,64},{5.2,61}}, color={0,0,127}));

  //
  // Annotations
  //
  annotation (experiment(StopTime=2500), Documentation(info="<html>
<p>
This model checks the convective heat transfer within tubes.
<br/><br/>
To see the model behavior, plot the variables of the models over the 
time. The simulation time is correctly preset (Start: 0 s, Stop = 2500 s). 
</p>
</html>", revisions="<html>
<ul>
  <li>
  January 19, 2024, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Test_TubeInsideHeatTransfer;
