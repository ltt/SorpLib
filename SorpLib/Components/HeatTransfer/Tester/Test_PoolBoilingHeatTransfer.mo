within SorpLib.Components.HeatTransfer.Tester;
model Test_PoolBoilingHeatTransfer
  "Tester for the pool boiling heat transfer"
  extends Modelica.Icons.Example;

  //
  // Definition of parameters
  //
  replaceable package Medium = Modelica.Media.Water.StandardWater
    constrainedby Modelica.Media.Interfaces.PartialTwoPhaseMedium
    "Medium model of the VLE liquid"
    annotation (Dialog(tab="General", group="Media"),
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

  //
  // Definition of boundary models
  //
  SorpLib.Basics.Sources.Fluids.VLESource liquidInlet(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.SpecificEnthalpy,
    use_mFlowInput=true,
    m_flow_fixed=-0.001,
    h_fixed=0.5e6,
    redeclare package Medium = Medium)
    "Liquid inlet for phase seprator volume"
    annotation (Placement(transformation(
                extent={{-10,-10},{10,10}},
                rotation=90,
                origin={0,-40})));
  SorpLib.Basics.Sources.Fluids.VLESource vaporInlet(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.SpecificEnthalpy,
    use_mFlowInput=true,
    m_flow_fixed=1e-3,
    h_fixed=1e-6,
    redeclare package Medium = Medium)
    "Vapor inlet for phase separator"
    annotation (Placement(transformation(
                extent={{10,-10},{-10,10}},
                rotation=90,
                origin={0,40})));

  SorpLib.Basics.Sources.Thermal.HeatSource heatSource(
    boundaryType=SorpLib.Choices.BoundaryThermal.Temperature,
    use_TInput=true,
    use_QFlowInput=false)
    "Heat source for phase seperator volume"
    annotation (Placement(transformation(
                extent={{-10,-10},{10,10}},
                rotation=180,
                origin={60,0})));

  //
  // Definition of heat transfer models
  //
  SorpLib.Components.HeatTransfer.PoolBoilingHeatTransfer heatTransfer(
    redeclare model HeatTransferCoefficient =
        HeatTransferCoefficientCorrelations.PoolBoiling.LinearAlphaA_fRel,
    fluidProperties=SorpLib.Components.HeatTransfer.Records.FluidProperties(
        p=phaseSeparator.p,
        T=phaseSeparator.T,
        rho=phaseSeparator.rho,
        cp=phaseSeparator.phaseSepratorProperties.cp,
        eta=phaseSeparator.phaseSepratorProperties.eta,
        lambda=phaseSeparator.phaseSepratorProperties.lambda),
    f_relativeFillingLevel=phaseSeparator.l_liq_rel) "Heat transfer model"
    annotation (Placement(transformation(extent={{30,-10},{50,10}})));

  //
  // Definition of VLE models
  //
  SorpLib.Basics.Volumes.PhaseSeparatorVolumes.PhaseSeparatorVolume
    phaseSeparator(
    T_initial=293.15,
    rho_initial=500,
    type_energyBalance=SorpLib.Choices.BalanceEquations.TransientFixedInitial,
    calculateAdditionalProperties=true,
    type_overallMassBalance=SorpLib.Choices.BalanceEquations.TransientFixedInitial,
    redeclare final package Medium = Medium,
    nPorts_cfp_xMinus=1,
    nPorts_cfp_xPlus=1) "Model of a phase seperator volume" annotation (
      Placement(transformation(
        extent={{-20,-20},{20,20}},
        rotation=90,
        origin={0,0})));

  //
  // Definition of fluid boundaries
  //
protected
  Modelica.Blocks.Sources.Trapezoid input_liquidPort(
    amplitude=-0.0001,
    rising=250,
    width=400,
    falling=250,
    period=950,
    startTime=50)
    "Input for liquid port boundary"
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={-30,-70})));
  Modelica.Blocks.Sources.Ramp input_vaporPort(
    height=0.0001,
    duration=250,
    startTime=500)
    "Input for vapor Port"
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={-30,70})));

  //
  // Definition of thermal boundaries
  //
  Modelica.Blocks.Sources.Ramp input_T(
    height=50,
    duration=1500,
    offset=273.15 + 25,
    startTime=500) "Input for temperature" annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=0,
        origin={80,0})));

equation
  //
  // Connections
  //
  connect(liquidInlet.port, phaseSeparator.cfp_xMinus[1]) annotation (Line(
      points={{0,-40},{0,-8.4},{-3.6,-8.4}},
      color={0,140,72},
      thickness=1));
  connect(vaporInlet.port, phaseSeparator.cfp_xPlus[1]) annotation (Line(
      points={{0,40},{0,15.6},{-3.6,15.6}},
      color={0,140,72},
      thickness=1));
  connect(heatSource.port, heatTransfer.hp_b[1]) annotation (Line(
      points={{60,0},{48,0}},
      color={238,46,47},
      thickness=1));
  connect(heatTransfer.hp_a[1], phaseSeparator.hp_yMinus) annotation (Line(
      points={{32,0},{12,0}},
      color={238,46,47},
      thickness=1));

  connect(input_liquidPort.y, liquidInlet.m_flow_input)
    annotation (Line(points={{-19,-70},{-2,-70},{-2,-41.2}}, color={0,0,127}));
  connect(input_vaporPort.y,vaporInlet. m_flow_input)
    annotation (Line(points={{-19,70},{-2,70},{-2,41.2}}, color={0,0,127}));
  connect(input_T.y, heatSource.T_input) annotation (Line(points={{69,0},{66,0},
          {66,-5.2},{61,-5.2}}, color={0,0,127}));

  //
  // Annotations
  //
  annotation (experiment(StopTime=2500), Documentation(info="<html>
<p>
This model checks the pool boiling heat transfer.
<br/><br/>
To see the model behavior, plot the variables of the models over the 
time. The simulation time is correctly preset (Start: 0 s, Stop = 2500 s). 
</p>
</html>", revisions="<html>
<ul>
  <li>
  January 17, 2024, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Test_PoolBoilingHeatTransfer;
