within SorpLib.Components.HeatTransfer.Tester;
model Test_ClosedAdsorberHeatTransfer
  "Tester for the heat transfer between sorbent and wall in closed adsorbers"
  extends Modelica.Icons.Example;

  //
  // Definition of parameters
  //
  replaceable package Medium = Modelica.Media.Water.StandardWater
    constrainedby Modelica.Media.Interfaces.PartialTwoPhaseMedium
    "Medium model"
    annotation (Dialog(tab="General", group="Media"),
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

  //
  // Definition of boundary models
  //
  SorpLib.Basics.Sources.Thermal.HeatSource heatSource(
    boundaryType=SorpLib.Choices.BoundaryThermal.Temperature,
    use_TInput=true,
    use_QFlowInput=false)
    "Heat source for fluid volume"
    annotation (Placement(transformation(
                extent={{-10,-10},{10,10}},
                rotation=270,
                origin={0,60})));

  //
  // Definition of heat transfer models
  //
  SorpLib.Components.HeatTransfer.ClosedAdsorberHeatTransfer heatTransfer(
      redeclare model HeatTransferCoefficient =
        HeatTransferCoefficientCorrelations.ClosedAdsorber.LinearAlphaA (
          constantAlphaA=500, b=2),                           fluidProperties=
     SorpLib.Components.HeatTransfer.Records.FluidProperties(
      p=adsorbateVolume.adsorbateProperties.p,
      T=adsorbateVolume.adsorbateProperties.T,
      rho=1/adsorbateVolume.workingPair.medium_sorbent.state_variables.v,
      cp=adsorbateVolume.workingPair.medium_sorbent.additional_variables.c,
      eta=0,
      lambda=adsorbateVolume.workingPair.medium_sorbent.additional_variables.lambda))
    "Heat transfer model"
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={0,40})));

  //
  // Definition of liquid volume models
  //
  Basics.Volumes.AdsorbateVolumes.AdsorbatePureVLEVolume adsorbateVolume(
    T_initial=373.15,
    useHeatPorts=false,
    useHeatPortsX=false,
    geometry(V=Modelica.Constants.pi/4*0.1^2*1),
    redeclare model PureWorkingPairModel =
        Media.WorkingPairs.PureComponents.H2O.Silicagel123_DubininLorentzianCumulative_Schawe2000_VLE
        (
        approachSorptionEnthalpy=SorpLib.Choices.SorptionEnthalpy.ClausiusClapeyron,
        approachSpecificHeatCapacity=SorpLib.Choices.SpecificHeatCapacityAdsorpt.ChakrabortyElAl,
        limitLowerPressure=true,
        limitLowerPressureAdsorptive=true),
    x_initial=0.05,
    redeclare final package Medium = Medium)
    "Model of an adsorbate volume"
    annotation (Placement(transformation(
        extent={{-20,-20},{20,20}},
        rotation=0,
        origin={0,0})));

  //
  // Definition of thermal boundaries
  //
protected
  Modelica.Blocks.Sources.Sine input_T(
    amplitude=50,
    f=1/250,
    offset=273.15 + 75) "Input for temperature"
    annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={0,80})));

equation
  //
  // Connections
  //
  connect(heatSource.port, heatTransfer.hp_b[1]) annotation (Line(
      points={{0,60},{0,48}},
      color={238,46,47},
      thickness=1));
  connect(heatTransfer.hp_a[1], adsorbateVolume.hp_sorption) annotation (Line(
      points={{0,32},{0,28},{3.2,28},{3.2,15.2}},
      color={238,46,47},
      thickness=1));

  connect(input_T.y, heatSource.T_input) annotation (Line(points={{0,69},{0,64},
          {5.2,64},{5.2,61}}, color={0,0,127}));

  //
  // Annotations
  //
  annotation (experiment(StopTime=2500), Documentation(info="<html>
<p>
This model checks the heat transfer between sorbent and wall within closed 
adsorbers.
<br/><br/>
To see the model behavior, plot the variables of the models over the 
time. The simulation time is correctly preset (Start: 0 s, Stop = 2500 s). 
</p>
</html>", revisions="<html>
<ul>
  <li>
  January 22, 2024, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Test_ClosedAdsorberHeatTransfer;
