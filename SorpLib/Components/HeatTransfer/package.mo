within SorpLib.Components;
package HeatTransfer "Models and correlations to calculate heat transfers"
  extends SorpLib.Icons.HeatTransfersPackage;

  annotation (Documentation(info="<html>
<p>
This package includes various heat transfer models to calculate the heat transfer
between different components:
</p>
<ul>
  <li>
  <a href=\"Modelica://SorpLib.Components.HeatTransfer.GenericHeatTransfer\">GenericHeatTransfer</a>: 
  Calculates a gneric heat transfer between components.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.HeatTransfer.ConductionHeatTransfer\">ConductionHeatTransfer</a>: 
  Calculates conductive heat transfer within volume models.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.HeatTransfer.RadiationHeatTransfer\">RadiationHeatTransfer</a>: 
  Calculates radiation heat transfer between components.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.HeatTransfer.PoolBoilingHeatTransfer\">PoolBoilingHeatTransfer</a>: 
  Calculates pool boiling heat transfer within evaporators.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.HeatTransfer.PoolCondensationHeatTransfer\">PoolCondensationHeatTransfer</a>: 
  Calculates (pool) condensation heat transfer within condensers.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.HeatTransfer.TubeInsideHeatTransfer\">TubeInsideHeatTransfer</a>: 
  Calculates convective heat transfer within tubes.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.HeatTransfer.ClosedAdsorberHeatTransfer\">ClosedAdsorberHeatTransfer</a>: 
  Calculates the heat transfer between sorbent and heat exchanger within closed
  adsorbers.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.HeatTransfer.OpenAdsorberHeatTransfer\">OpenAdsorberHeatTransfer</a>: 
  Calculates the heat transfer between gas and sorbent/casing within closed
  adsorbers.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end HeatTransfer;
