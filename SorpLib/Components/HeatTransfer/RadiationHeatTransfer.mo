within SorpLib.Components.HeatTransfer;
model RadiationHeatTransfer
  "Model calculating a radiation heat transfer"
  extends BaseClasses.PartialHeatTransfer(
    final exponetTemperature=4,
    redeclare replaceable model HeatTransferCoefficient =
      SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.Radiation.ConstantResistance
      constrainedby
      SorpLib.Components.HeatTransfer.BaseClasses.PartialRadiationHeatTransferCoefficient);

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
The heat transfer model is used to thermally connect models which can exchange 
heat due to thermal radiation. Thus, the radiation heat transfer model represents 
a thermal resistance betweed two models. Depending on the attached temperatures (i.e., 
attached potential) and the chosen transport phenomena, this model determines the 
heat flow rate between the connected models. 
<br/><br/>
The model has two heat ports, which sizes can be defined via a parameter. The 
scalable heat ports enable the connection of various models. For example, this 
enables to connect a discretized model to a lumped model, or two different
disretized models.
</p>

<h4>Main equations</h4>
<p>
The model has steady-state energy balance:
</p>
<pre>
    0 = &sum; (hp_a.Q_flow) + &sum; (hp_b.Q_flow);
</pre>
<p>
The heat flow rate <i>Q_flow</i> is proportional to the driving temperature
difference <i>&Delta;T = hp_a.T<sup>4</sup> - hp_b.T<sup>4</sup></i> and the production 
of heat transfer coefficient and area <i>&alpha;A</i>, which can be formulated as 
thermal resistance <i>R<sub>&sigma;</sub></i>. This product can be given as an input 
or calculated using models describing heat transfer coefficient correlations.
</p>
<pre>
    hp_a.Q_flow = &alpha;A * &Delta;T = 1 / R<sub>&sigma;</sub> * (hp_a.T<sup>4</sup> - hp_b.T<sup>4</sup>);
</pre>

<h4>Assumptions and limitations</h4>
<ul>
  <li>
  Steady-state process
  </li>
  <li>
  No storage of mass or energy
  </li>
  <li>
  The number of ports a and b must be an even integer multiple of each other.
  </li>
</ul>

<h4>Typical use</h4>
<p>
The conductive heat transfer is typically used to desribe the heat transfer between
two components.
</p>

<h4>Important parameters and options</h4>
<ul>
  <li>
  <i>useAlphaAInput</i>:
  Defines if the product of heat transfer coefficient and area is given via
  an input or calculated using an approproate correlation model for the heat
  transfer coefficient.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  January 16, 2024, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"), Icon(graphics={Text(
          extent={{-60,20},{60,-20}},
          lineColor={0,0,0},
          fillPattern=FillPattern.HorizontalCylinder,
          fillColor={238,46,47},
          textString="Q_flow = 1/R_sigma * DT")}));
end RadiationHeatTransfer;
