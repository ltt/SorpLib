within SorpLib.Components.HeatTransfer;
model TubeInsideHeatTransfer
  "Model calculating a heat transfer describing convective heat transfer within tubes"
  extends BaseClasses.PartialHeatTransfer(
    final exponetTemperature=1,
    redeclare replaceable model HeatTransferCoefficient =
      SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.TubeInside.ConstantAlpha
      constrainedby
      SorpLib.Components.HeatTransfer.BaseClasses.PartialTubeInsideHeatTransferCoefficient(
      fluidProperties=fluidProperties,
      m_hyd_xMinus=m_hyd_xMinus,
      m_hyd_xPlus=m_hyd_xPlus,
      geometry=geometry));

  //
  // Definition of parameters
  //
  replaceable parameter SorpLib.Components.HeatTransfer.Records.GeometryTube geometry
    constrainedby SorpLib.Components.HeatTransfer.Records.GeometryTube
    "Geometry of the tube"
    annotation(Dialog(tab = "General", group = "Heat Transfer Coefficient",
               enable=false));

  //
  // Definitions of inputs
  //
  input SorpLib.Components.HeatTransfer.Records.FluidProperties fluidProperties
    "Fluid properties (i.e., fluid properties of the fluid volume)"
    annotation (Dialog(tab="General", group="Inputs", enable=false));
  input Modelica.Units.SI.MassFlowRate m_hyd_xMinus
    "Hydraulic mass flow rate at design inlet"
    annotation (Dialog(tab="General", group="Inputs", enable=false));
  input Modelica.Units.SI.MassFlowRate m_hyd_xPlus
    "Hydraulic mass flow rate at design outlet"
    annotation (Dialog(tab="General", group="Inputs", enable=false));


  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
The heat transfer model is used to describe convective heat transfer within a tube,
which is the heat transfer between the heat transfer fluid and tube wall. Thus, 
the pool condensation heat transfer model represents a thermal resistance betweed 
two models. Depending on the attached temperatures (i.e., attached potential) 
and the chosen transport phenomena, this model determines the heat flow rate 
between the connected models. 
<br/><br/>
The model has two heat ports, which sizes can be defined via a parameter. The 
scalable heat ports enable the connection of various models. For example, this 
enables to connect a discretized model to a lumped model, or two different
disretized models.
</p>

<h4>Main equations</h4>
<p>
The model has steady-state energy balance:
</p>
<pre>
    0 = &sum; (hp_a.Q_flow) + &sum; (hp_b.Q_flow);
</pre>
<p>
The heat flow rate <i>Q_flow</i> is proportional to the driving temperature
difference <i>&Delta;T = hp_a.T - hp_b.T</i> and the production of heat transfer
coefficient and area <i>&alpha;A</i>, which can be formulated as thermal resistance
<i>R<sub>tubeInside</sub></i>. This product can be given as an input or calculated using 
models describing heat transfer coefficient correlations.
</p>
<pre>
    hp_a.Q_flow = &alpha;A * &Delta;T = 1 / R<sub>tubeInside</sub> * (hp_a.T - hp_b.T);
</pre>

<h4>Assumptions and limitations</h4>
<ul>
  <li>
  Steady-state process
  </li>
  <li>
  No storage of mass or energy
  </li>
  <li>
  The number of ports a and b must be an even integer multiple of each other.
  </li>
</ul>

<h4>Typical use</h4>
<p>
The (pool) condensation heat transfer is typically used to desribe the heat transfer 
within tubes, covering laminar and turbulent flow regimes.
</p>

<h4>Important parameters and options</h4>
<ul>
  <li>
  <i>useAlphaAInput</i>:
  Defines if the product of heat transfer coefficient and area is given via
  an input or calculated using an approproate correlation model for the heat
  transfer coefficient.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  January 19, 2024, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"), Icon(graphics={Text(
          extent={{-60,20},{60,-20}},
          lineColor={0,0,0},
          fillPattern=FillPattern.HorizontalCylinder,
          fillColor={238,46,47},
          textString="Q_flow = 1/R_tubeInside * DT")}));
end TubeInsideHeatTransfer;
