within SorpLib.Components.HeatTransfer.Records;
record FluidProperties
  "This record contains fluid properties required for heat transfer coefficients"
  extends Modelica.Icons.Record;

  //
  // Definition of variables describing fluid properties
  //
  Modelica.Units.SI.Pressure p
    "Pressure";
  Modelica.Units.SI.Temperature T
    "Temperature";
  Modelica.Units.SI.Density rho
    "Density";

  Modelica.Units.SI.SpecificHeatCapacity cp
    "Isobaric specific heat capacity";
  Modelica.Units.SI.DynamicViscosity eta
    "Dynamic viscosity";
  Modelica.Units.SI.ThermalConductivity lambda
    "Thermal conductivity";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This record contains fluid properties required for heat transfer coefficients.
</p>
</html>", revisions="<html>
<ul>
  <li>
  January 15, 2024, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end FluidProperties;
