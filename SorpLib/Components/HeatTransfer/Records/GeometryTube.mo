within SorpLib.Components.HeatTransfer.Records;
record GeometryTube
  "This record contains the geometry required for heat transfer coefficients of tubes"
  extends SorpLib.Components.Tubes.Records.GeometryTube;

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This record contains geometric parameters required by models calculating the heat
transfer coefficients at the inside or outside of tubes.
</p>
</html>", revisions="<html>
<ul>
  <li>
  January 17, 2024, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end GeometryTube;
