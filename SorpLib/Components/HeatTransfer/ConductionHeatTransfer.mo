within SorpLib.Components.HeatTransfer;
model ConductionHeatTransfer
  "Model calculating a heat transfer describing conduction"
  extends BaseClasses.PartialHeatTransfer(
    final exponetTemperature=1,
    redeclare replaceable model HeatTransferCoefficient =
      SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.Conduction.ConstantResistance
      constrainedby
      SorpLib.Components.HeatTransfer.BaseClasses.PartialConductiveHeatTransferCoefficient(
      fluidProperties=fluidProperties,
      no_hydraulicParallelFlows=no_hydraulicParallelFlows));

  //
  // Definition of parameters
  //
  parameter Integer no_hydraulicParallelFlows(min=1) = 1
    "Number of hydraulically parallel flows (e.g., tubes)"
    annotation(Dialog(tab = "General", group = "Heat Transfer Coefficient"));

  //
  // Definitions of inputs
  //
  input SorpLib.Components.HeatTransfer.Records.FluidProperties fluidProperties
    "Fluid properties (i.e., fluid properties of the volume)"
    annotation (Dialog(tab="General", group="Inputs", enable=false));

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
The heat transfer model is used to thermally connect models which can exchange 
heat due to thermal conduction. Thus, the conduction heat transfer model represents 
a thermal resistance betweed two models. Depending on the attached temperatures (i.e., 
attached potential) and the chosen transport phenomena, this model determines the 
heat flow rate between the connected models. 
<br/><br/>
The model has two heat ports, which sizes can be defined via a parameter. The 
scalable heat ports enable the connection of various models. For example, this 
enables to connect a discretized model to a lumped model, or two different
disretized models.
</p>

<h4>Main equations</h4>
<p>
The model has steady-state energy balance:
</p>
<pre>
    0 = &sum; (hp_a.Q_flow) + &sum; (hp_b.Q_flow);
</pre>
<p>
The heat flow rate <i>Q_flow</i> is proportional to the driving temperature
difference <i>&Delta;T = hp_a.T - hp_b.T</i> and the production of heat transfer
coefficient and area <i>&alpha;A</i>, which can be formulated as thermal resistance
<i>R<sub>&lambda;</sub></i>. This product can be given as an input or calculated using 
models describing heat transfer coefficient correlations.
</p>
<pre>
    hp_a.Q_flow = &alpha;A * &Delta;T = 1 / R<sub>&lambda;</sub> * (hp_a.T - hp_b.T);
</pre>

<h4>Assumptions and limitations</h4>
<ul>
  <li>
  Steady-state process
  </li>
  <li>
  No storage of mass or energy
  </li>
  <li>
  The number of ports a and b must be an even integer multiple of each other.
  </li>
</ul>

<h4>Typical use</h4>
<p>
The conductive heat transfer is typically used to desribe the heat transfer within
solid volumes, such as tube walls, in axial and radial direction.
</p>

<h4>Important parameters and options</h4>
<ul>
  <li>
  <i>useAlphaAInput</i>:
  Defines if the product of heat transfer coefficient and area is given via
  an input or calculated using an approproate correlation model for the heat
  transfer coefficient.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  January 15, 2024, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"), Icon(graphics={Text(
          extent={{-60,20},{60,-20}},
          lineColor={0,0,0},
          fillPattern=FillPattern.HorizontalCylinder,
          fillColor={238,46,47},
          textString="Q_flow = 1/R_lambda * DT")}));
end ConductionHeatTransfer;
