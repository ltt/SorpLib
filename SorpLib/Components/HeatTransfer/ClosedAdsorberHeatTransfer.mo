within SorpLib.Components.HeatTransfer;
model ClosedAdsorberHeatTransfer
  "Model calculating a heat transfer describing heat transfer between sorbent and heat exchanger within closed adsorbers"
  extends BaseClasses.PartialHeatTransfer(
    final exponetTemperature=1,
    redeclare replaceable model HeatTransferCoefficient =
      SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.ClosedAdsorber.ConstantAlpha
      constrainedby
      SorpLib.Components.HeatTransfer.BaseClasses.PartialClosedAdsorberHeatTransferCoefficient(
      fluidProperties=fluidProperties,
      geometry=geometry));

  //
  // Definition of parameters
  //
  replaceable parameter SorpLib.Components.HeatTransfer.Records.GeometryClosedAdsorber geometry
    constrainedby
    SorpLib.Components.HeatTransfer.Records.GeometryClosedAdsorber
    "Geometry of the tube"
    annotation(Dialog(tab = "General", group = "Heat Transfer Coefficient",
               enable=false));

  //
  // Definitions of inputs
  //
  input SorpLib.Components.HeatTransfer.Records.FluidProperties fluidProperties
    "Fluid properties (i.e., fluid properties of the sorbent)"
    annotation (Dialog(tab="General", group="Inputs", enable=false));

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
The heat transfer model is used to describe the heat transfer between the sorbent
and heat exchanger within closed adsorbers. Thus, the closed adsorber heat transfer 
model represents a thermal resistance between two models. Depending on the attached 
temperatures (i.e., attached potential) and the chosen transport phenomena, this 
model determines the heat flow rate between the connected models. 
<br/><br/>
The model has two heat ports, which sizes can be defined via a parameter. The 
scalable heat ports enable the connection of various models. For example, this 
enables to connect a discretized model to a lumped model, or two different
disretized models.
</p>

<h4>Main equations</h4>
<p>
The model has steady-state energy balance:
</p>
<pre>
    0 = &sum; (hp_a.Q_flow) + &sum; (hp_b.Q_flow);
</pre>
<p>
The heat flow rate <i>Q_flow</i> is proportional to the driving temperature
difference <i>&Delta;T = hp_a.T - hp_b.T</i> and the production of heat transfer
coefficient and area <i>&alpha;A</i>, which can be formulated as thermal resistance
<i>R<sub>closedAdsorber</sub></i>. This product can be given as an input or calculated 
using models describing heat transfer coefficient correlations.
</p>
<pre>
    hp_a.Q_flow = &alpha;A * &Delta;T = 1 / R<sub>closedAdsorber</sub> * (hp_a.T - hp_b.T);
</pre>

<h4>Assumptions and limitations</h4>
<ul>
  <li>
  Steady-state process
  </li>
  <li>
  No storage of mass or energy
  </li>
  <li>
  The number of ports a and b must be an even integer multiple of each other.
  </li>
</ul>

<h4>Typical use</h4>
<p>
The closed adsorber heat transfer is typically used to describe the heat transfer 
between the sorbent and heat exchanger within closed adsorbers.
</p>

<h4>Important parameters and options</h4>
<ul>
  <li>
  <i>useAlphaAInput</i>:
  Defines if the product of heat transfer coefficient and area is given via
  an input or calculated using an approproate correlation model for the heat
  transfer coefficient.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  January 22, 2024, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"), Icon(graphics={Text(
          extent={{-60,20},{60,-20}},
          lineColor={0,0,0},
          fillPattern=FillPattern.HorizontalCylinder,
          fillColor={238,46,47},
          textString="Q_flow = 1/R_closedAdsorber * DT")}));
end ClosedAdsorberHeatTransfer;
