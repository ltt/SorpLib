within SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.OpenAdsorber;
model ConstantAlpha
  "Generic heat transfer correlation with constant heat transfer coefficient"
  extends
    SorpLib.Components.HeatTransfer.BaseClasses.PartialOpenAdsorberHeatTransferCoefficient(
    final computeTransportProperties=false,
    final avoid_events=false);

  //
  // Definition of parameters
  //
  parameter Modelica.Units.SI.CoefficientOfHeatTransfer constantAlpha = 1000
    "Constant heat transfer coefficient"
    annotation(Dialog(tab = "General", group = "Heat Transfer"));
  parameter Modelica.Units.SI.Area A = geometry.A_heatTransferInner_cas
    "Constant heat transfer area (i.e., geometry.A_heatTransferInner_cas or
    geometry.no_particles * geometry.A_surface_particle)"
    annotation(Dialog(tab = "General", group = "Heat Transfer"));

equation
  alphaA = geometry.no_hydraulicParallelTubes * constantAlpha * A /
    geometry.no_volumes
    "Calculation of the product of heat transfer coefficient and area";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This simple transfer model calculates the product of heat transfer coefficient and
area assuming a constant heat transfer coefficient and area.
</p>

<h4>Main equations</h4>
<p>
The product of heat transfer coefficient and area <i>&alpha;A</i> is calculated 
assuming a constant heat transfer coefficient <i>&alpha;</i> and area <i>A</i>. 
The product is enlarged by the number of hydrualic parallel flows <i>no<sub>parallel flows</sub></i>
to account for parallel flows modeled by just one flow:
</p>
<pre>
    &alpha;A = no<sub>parallel flows</sub> * &alpha; * A / no<sub>volumes</sub> = const.;
</pre>
<p>
Note that the area <i>A</i> is calculated from the geometry record taking into
account the disretization. Thus, the area <i>A</i> belongs to one adsorber column.
</p>
  
<h4>Typical use</h4>
<p>
This simple heat transfer correlation model is typically used if the heat transfer
conditions do not greatly change and a very simple models is required.
</p>
</html>", revisions="<html>
<ul>
  <li>
  January 23, 2024, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end ConstantAlpha;
