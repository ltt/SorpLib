within SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.OpenAdsorber;
model CasingGasKast
  "Heat transfer correlation describing the heat transfer between gas and casing according to Kast"
  extends
    SorpLib.Components.HeatTransfer.BaseClasses.PartialOpenAdsorberHeatTransferCoefficient(
    final computeTransportProperties=true);

  //
  // Definition of parameters
  //
  parameter SorpLib.Choices.MassFlowRateHeatTranferCorrelation calculationMassFlowRate=
    SorpLib.Choices.MassFlowRateHeatTranferCorrelation.PortXMinus
    "Defines the hydraulic mass flow rate used for calculations"
    annotation(Dialog(tab = "General", group = "Heat Transfer"),
              Evaluate=true);

  parameter Real f_particle = 12
    "Particle factor (i.e., 12 for spherical particles; 6 for cylinders)"
    annotation(Dialog(tab = "General", group = "Heat Transfer"));
  parameter Real f_length = 1.15
    "Length factor (i.e., 11.5 for spherical particles; 1.4 for broken spherical
    particles; 1.75 for cylinders)"
    annotation(Dialog(tab = "General", group = "Heat Transfer"));
  parameter Real f_correction = 1
    "Correction factor"
    annotation(Dialog(tab = "General", group = "Heat Transfer"));

  //
  // Definition of variables
  //
  Modelica.Units.SI.CoefficientOfHeatTransfer alpha
    "Heat tranfer coefficient";

  Modelica.Units.SI.Velocity v
    "Hydraulic velocity";
  Modelica.Units.SI.PecletNumber Pe
    "Peclet number";
  Modelica.Units.SI.ReynoldsNumber Nu
    "Nusselt number";


  //
  // Definition of protected variables
  //
protected
  Modelica.Units.SI.NusseltNumber Nu_length
    "Nusselt number correction to account for enhanced length";

  Modelica.Units.SI.Length l_mixture
    "Mixture length of spherical particles";

  Modelica.Units.SI.Length l_heatTransfer
    "Length of the tube accounting for discretization";
  Modelica.Units.SI.Area A_heatTransfer
    "Heat transfer area accounting for discretization";

equation
  //
  // Calculation of the velocity and Peclet number
  //
  if calculationMassFlowRate==
    SorpLib.Choices.MassFlowRateHeatTranferCorrelation.PortXMinus then
    v = abs(m_hyd_xMinus) / (fluidProperties.rho *
      geometry.A_crossInner_cas * geometry.psi_particles)
      "Hydraulic velocity";

  elseif calculationMassFlowRate==
    SorpLib.Choices.MassFlowRateHeatTranferCorrelation.PortXPlus then
    v = abs(m_hyd_xPlus) / (fluidProperties.rho *
      geometry.A_crossInner_cas * geometry.psi_particles)
      "Hydraulic velocity";

  else
    v = (abs(m_hyd_xMinus) + abs(m_hyd_xPlus)) / 2 / (fluidProperties.rho *
      geometry.A_crossInner_cas * geometry.psi_particles)
      "Hydraulic velocity";

  end if;

  Pe = v * l_mixture * fluidProperties.rho * fluidProperties.cp /
    fluidProperties.lambda
    "Peclet number";

  //
  // Calculation of the Nusselt numbers
  //
  l_mixture = f_length * geometry.d_particle
    "Mixture length of spherical particles";

  l_heatTransfer = geometry.l_cas / geometry.no_volumes
    "Length of the tube accounting for discretization";
  A_heatTransfer = geometry.A_heatTransferInner_cas / geometry.no_volumes
    "Length of the tube accounting for discretization";

  Nu_length = -1.55e-6*Pe^2 + 0.04285*Pe + 24.57
    "Nusselt number correction to account for enhanced length: Own fit for 
    spherical particles from Kast (1988), p. 136";
  Nu = f_correction * Nu_length / (1 + f_particle * l_heatTransfer /
    geometry.d_inner_cas / max(Pe, 1e-12))
    "Nusselt number";

  //
  // Calculation of the heat transfer coefficient
  //
  alpha = Nu * fluidProperties.lambda/l_mixture
    "Heat transfer coefficient";
  alphaA = geometry.no_hydraulicParallelTubes * alpha * A_heatTransfer
    "Product of heat transfer coefficient and area";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This heat transfer model calculates the product of heat transfer coefficient and
area describing the heat transfer between the gas and casing in open adsorbers.
</p>

<h4>Main equations</h4>
<p>
The product of heat transfer coefficient and area <i>&alpha;A</i> is calculated
from the Nussel number <i>Nu</i>:
</p>
<pre>
    &alpha;A = no<sub>parallel flows</sub> * Nu * &lambda; / l<sub>mixture</sub> * A<sub>heat transfer</sub> / no<sub>volumes</sub>;
</pre>
<p>
Herein, <i>&lambda;</i> is the thermal conductivity, <i>l<sub>mixture</sub></i> 
is the mixture length, and <i>A<sub>heat transfer</sub></i> is the heat transfer 
area accounting for the discretization. The product is enlarged by the number of 
hydrualic parallel flows <i>no<sub>parallel flows</sub></i> to account for parallel 
flows modeled by just one flow. 
<br/><br/>
The Nusselt number is calculated as:
</p>
<pre>
    Nu = f<sub>correction</sub> * Nu<sub>length</sub> / [1 + f<sub>correction</sub> * l<sub>heat transfer</sub> / (Pe * d<sub>inner casing</sub>)];
</pre>
<p>
with:
</p>
<pre>
    Nu<sub>length</sub> = -1.55E<sup>-6</sup> * Pe<sup></sup> + 0.04285 * Pe + 24.57;

    l<sub>mixture</sub> = f<sub>particle</sub> * d<sub>particle</sub>;
</pre>
<p>
Herein, <i>f<sub>i</sub></i> are correction factors, <i>Pe</i> is the Peclet number, 
<i>Nu<sub>length</sub></i> is the Nusselt number correction, <i>d<sub>inner casing</sub></i>
is the inner diameter of the casing, and <i>l<sub>heat transfer</sub></i> is the heat 
transfer length accounting for the discretization:
</p>
<pre>
    Pe = v * l<sub>mixture</sub> * &rho; * c<sub>p</sub> / &lambda;;

    v =  <strong>abs</strong>(m<sub>flow,hyd</sub>) / (&rho; * A<sub>cross,inner casing</sub> * &psi;);

    l<sub>heat transfer</sub> = l<sub>cas</sub> / no<sub>volumes</sub>;
</pre>
<p>
Herein, <i>v</i> is the hydraulic flow velocity, <i>m<sub>flow,hyd</sub></i> is 
the hydraulic mass flow rate, <i>l<sub>cas</sub></i> is the length of the casing,
<i>A<sub>cross,inner casing</sub></i> is the cross-sectional inner area of the 
casing, <i>&psi;</i> is the void fraction, <i>&rho;</i> is the fluid density,
<i>c<sub>p</sub></i> is the isobaric heat capacity, and <i>&lambda;</i> is the 
thermal conductivity.
</p>

<h4>Typical use</h4>
<p>
This heat transfer correlation model is typically used to calculate the heat transfer
between the gas and casing if accurate results are required.
</p>

<h4>Important parameters and options</h4>
<ul>
  <li>
  <i>calculationMassFlowRate</i>:
  Defines the hydraulic mass flow rate that is used for calculations.
  </li>
  <br/>
  <li>
  <i>avoid_events</i>:
  Defines if events shall be avoided via the noEvent()-operator.
  </li>
</ul>

<h4>References</h4>
<ul>
  <li>
  Gnielinski, V. (2010). G9 Fluid-Particle Heat Transfer in Flow Through Packed Beds of Solids. VDI-Buch. Springer, Berlin, Heidelberg. DOI: https://doi.org/10.1007/978-3-540-77877-6_42.
  </li>
  <li>
  Kast, W. (1998). Adsorption aus der Gasphase: Ingenieurwissenschaftliche Grundlagen und technische Verfahren (in German). VCH Verlagsgesellschaft, Weinheim, Basel, Cambridge, New York. DOI:  https://doi.org/10.1002/bbpc.19900940122.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  January 23, 2024, by Mirko Engelpracht:<br/>
  Minor revisions after restructering of the library.
  </li>
  <li>
  January 14, 2021, by Mirko Engelpracht:<br/>
  Minor revisions after restructering of the library.
  </li>
  <li>
  December 11, 2017, by Andrej Gibelhaus:<br/>
  Tidy up implementation and enhance documentation for publication of library.
  </li>
</ul>
</html>"));
end CasingGasKast;
