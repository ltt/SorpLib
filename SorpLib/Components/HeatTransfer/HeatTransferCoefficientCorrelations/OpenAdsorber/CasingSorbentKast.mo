within SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.OpenAdsorber;
model CasingSorbentKast
  "Heat transfer correlation describing the heat transfer between gas and sorbent according to Kast"
  extends
    SorpLib.Components.HeatTransfer.BaseClasses.PartialOpenAdsorberHeatTransferCoefficient(
    final computeTransportProperties=true);

  //
  // Definition of parameters
  //
  parameter SorpLib.Choices.MassFlowRateHeatTranferCorrelation calculationMassFlowRate=
    SorpLib.Choices.MassFlowRateHeatTranferCorrelation.PortXMinus
    "Defines the hydraulic mass flow rate used for calculations"
    annotation(Dialog(tab = "General", group = "Heat Transfer"),
              Evaluate=true);

  parameter Real f_particle = 1 + 1.5 * (1 - geometry.psi_particles)
    "Form factor for the particles (i.e., 1 + 1.5 * (1 - geometry.psi_particles)
    for spherical particles; 1.6 for cylinders)"
    annotation(Dialog(tab = "General", group = "Heat Transfer"));
  parameter Modelica.Units.SI.NusseltNumber Nu_min = 2
    "Minimum Nusselt number (i.e., 2 for spherical particles; 0.3 for cylinders)"
    annotation(Dialog(tab = "General", group = "Heat Transfer"));

  parameter Real f_correctionLaminar = 1
    "Correction factor for the laminar flow regime"
    annotation(Dialog(tab = "General", group = "Heat Transfer"));
  parameter Real f_correctionTurbulent = 1
    "Correction factor for the tubulent flow regime"
    annotation(Dialog(tab = "General", group = "Heat Transfer"));

  //
  // Definition of variables
  //
  Modelica.Units.SI.CoefficientOfHeatTransfer alpha
    "Heat tranfer coefficient";

  Modelica.Units.SI.ReynoldsNumber Re
    "Reynolds number";
  Modelica.Units.SI.PrandtlNumber Pr
    "Prandtl number";
  Modelica.Units.SI.ReynoldsNumber Nu
    "Nusselt number";

  Modelica.Units.SI.NusseltNumber Nu_lamianr
    "Nusselt number for laminar flow regime";
  Modelica.Units.SI.NusseltNumber Nu_turbulent
    "Nusselt number for turbulent regime";

  //
  // Definition of protected variables
  //
protected
  Modelica.Units.SI.Area A_heatTransfer
    "Heat transfer area accounting for discretization";

equation
  //
  // Calculation of the Reynolds and Prandtl number
  //
  if calculationMassFlowRate==
    SorpLib.Choices.MassFlowRateHeatTranferCorrelation.PortXMinus then
    Re = abs(m_hyd_xMinus) * geometry.d_particle /
      (geometry.A_crossInner_cas * geometry.psi_particles * fluidProperties.eta)
      "Reynolds number";

  elseif calculationMassFlowRate==
    SorpLib.Choices.MassFlowRateHeatTranferCorrelation.PortXPlus then
    Re = abs(m_hyd_xPlus) * geometry.d_particle /
      (geometry.A_crossInner_cas * geometry.psi_particles * fluidProperties.eta)
      "Reynolds number";

  else
    Re = (abs(m_hyd_xMinus) + abs(m_hyd_xPlus)) / 2 * geometry.d_particle /
      (geometry.A_crossInner_cas * geometry.psi_particles * fluidProperties.eta)
      "Reynolds number";

  end if;

  Pr = fluidProperties.eta * fluidProperties.cp / fluidProperties.lambda
    "Prandtl number";

  //
  // Calculation of the Nusselt numbers
  //
  A_heatTransfer = geometry.no_particles * geometry.A_surface_particle /
    geometry.no_volumes
    "Length of the tube accounting for discretization";

  Nu_lamianr = f_correctionLaminar * (0.664 * max(Re,1e-6)^0.5 * Pr^0.5)
    "Nusselt number for laminar flow regime";
  Nu_turbulent = f_correctionTurbulent * (0.037 * max(Re,1e-6)^0.8 * Pr) /
    (1 + 2.443 * max(Re,1e-6)^(-0.8) * (Pr^(2/3) - 1))
    "Nusselt number for turbulent regime";

  Nu = f_particle * (Nu_min * (Nu_lamianr^2 + Nu_turbulent^2)^0.5)
    "Nusselt number";

  //
  // Calculation of the heat transfer coefficient
  //
  alpha = Nu * fluidProperties.lambda/geometry.d_particle
    "Heat transfer coefficient";
  alphaA = geometry.no_hydraulicParallelTubes * alpha * A_heatTransfer
    "Product of heat transfer coefficient and area";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This heat transfer model calculates the product of heat transfer coefficient and
area describing the heat transfer between gas and sorbent within open adsorbers.
The model considers both, the laminar and turbulent flow regime.
</p>

<h4>Main equations</h4>
<p>
The product of heat transfer coefficient and area <i>&alpha;A</i> is calculated
from the Nussel number <i>Nu</i> using Nusselt correlations for different flow
regimes:
</p>
<pre>
    &alpha;A = no<sub>parallel flows</sub> * Nu * &lambda; / d<sub>particle</sub> * A<sub>particles</sub> / no<sub>volumes</sub>;
</pre>
<p>
Herein, <i>&lambda;</i> is the thermal conductivity, <i>d<sub>particle</sub></i> 
is the diameter of one particle, and <i>A<sub>particles</sub></i> is the heat 
transfer area of all particles accounting for the discretization. The product is 
enlarged by the number of hydrualic parallel flows <i>no<sub>parallel flows</sub></i> 
to account for parallel flows modeled by just one flow. 
<br/><br/>
The Nusselt number depends on the flow regime:
</p>
<pre>
    Nu = f<sub>particle</sub> * [Nu<sub>min</sub> * (Nu<sup>2</sup><sub>laminar</sub> + Nu<sup>2</sup><sub>turbulent</sub>)<sup>0.5</sup>];

    Nu<sub>laminar</sub> = f<sub>correction,laminar</sub> * [0.664 * Re<sup>0.5</sup> * Pr<sup>0.5</sup>];

    Nu<sub>turbulent</sub> = f<sub>correction,turbulent</sub> * 0.037 * Re<sup>0.8</sup> * Pr / (1 + 2.443 * Re<sup>-0.8</sup> * (Pr<sup>2/3</sup> - 1));
</pre>
<p>
Herein, <i>f<sub>i</sub></i> are correction factors for the Nusselt correlations
<i>Nu<sub>i</sub></i> of the different flow regimes <i>i</i>, <i>Nu<sub>min</sub>
is a minimal Nusselt number, </i><i>Re</i> is the Reynolds number, and <i>Pr</i> is 
the Prandtl number:
</p>
<pre>
    Re = <strong>abs</strong>(m<sub>flow,hyd</sub>) * d<sub>particle</sub> / A<sub>cross,inner casing</sub> / &psi; / &eta;;

    Pr = &eta; * c<sub>p</sub> / &lambda;;
</pre>
<p>
Herein, <i>m<sub>flow,hyd</sub></i> is the hydraulic mass flow rate, 
<i>A<sub>cross,inner casing</sub></i> is the hydraulic cross-sectional inner area
of the casing, <i>&psi;</i> is the void fraction, <i>&eta;</i> is the dynamic 
viscosity, <i>&lambda;</i> is the thermal conductivity, and <i>c<sub>p</sub></i>
is the isobaric heat capacity.
</p>

<h4>Typical use</h4>
<p>
This heat transfer correlation model is typically used to calculate the heat transfer
between the sorbent and casing if accurate results are required.
</p>

<h4>Important parameters and options</h4>
<ul>
  <li>
  <i>calculationMassFlowRate</i>:
  Defines the hydraulic mass flow rate that is used for calculations.
  </li>
  <br/>
  <li>
  <i>avoid_events</i>:
  Defines if events shall be avoided via the noEvent()-operator.
  </li>
</ul>

<h4>References</h4>
<ul>
  <li>
  Gnielinski, V. (2010). G1 Heat Transfer in Pipe Flow. In: VDI Heat Atlas. VDI-Buch. Springer, Berlin, Heidelberg. DOI: https://doi.org/10.1007/978-3-540-77877-6_34.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  January 23, 2024, by Mirko Engelpracht:<br/>
  Minor revisions after restructering of the library.
  </li>
  <li>
  January 14, 2021, by Mirko Engelpracht:<br/>
  Minor revisions after restructering of the library.
  </li>
  <li>
  December 11, 2017, by Andrej Gibelhaus:<br/>
  Tidy up implementation and enhance documentation for publication of library.
  </li>
</ul>
</html>"));
end CasingSorbentKast;
