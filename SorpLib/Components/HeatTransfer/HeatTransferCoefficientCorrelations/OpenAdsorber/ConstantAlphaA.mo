within SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.OpenAdsorber;
model ConstantAlphaA
  "Generic heat transfer correlation with constant product of heat transfer coefficient and area"
  extends
    SorpLib.Components.HeatTransfer.BaseClasses.PartialOpenAdsorberHeatTransferCoefficient(
    final computeTransportProperties=false,
    final avoid_events=false);
  //
  // Definition of parameters
  //
  parameter Modelica.Units.SI.ThermalConductance constantAlphaA = 1000
    "Constant product of heat transfer coefficient and area"
    annotation(Dialog(tab = "General", group = "Heat Transfer"));

equation
  alphaA = geometry.no_hydraulicParallelTubes * constantAlphaA /
    geometry.no_volumes
    "Calculation of the product of heat transfer coefficient and area";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This simple transfer model calculates the product of heat transfer coefficient and
area assuming a constant heat transfer coefficient and area.
</p>

<h4>Main equations</h4>
<p>
The product of heat transfer coefficient and area <i>&alpha;A</i> is calculated 
assuming a constant heat transfer coefficient <i>&alpha;</i> and area <i>A</i>:
</p>
<pre>
    &alpha;A = no<sub>parallel flows</sub> / no<sub>volumes</sub> * const.;
</pre>
<p>
The product is enlarged by the number of hydrualic parallel flows 
<i>no<sub>parallel flows</sub></i> to account for parallel flows modeled by just 
one flow. Furhtermore, the product is diveded by the discretization number. Thus, 
the product <i>&alpha;A</i> is valid for one adsorber column.
</p>
  
<h4>Typical use</h4>
<p>
This simple heat transfer correlation model is typically used if the heat transfer
conditions do not greatly change and a very simple models is required.
</p>
</html>", revisions="<html>
<ul>
  <li>
  January 23, 2024, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end ConstantAlphaA;
