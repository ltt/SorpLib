within SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations;
package OpenAdsorber "Correlations for heat transfer coefficients describing the heat transfer within open adsorbers"
extends Modelica.Icons.FunctionsPackage;

  annotation (Documentation(info="<html>
<p>
This package contains correlations for heat transfer coefficients describing
the heat transfer between gas and sorbent/casing within open adsorbers:
</p>
<ul>
  <li>
  <a href=\"Modelica://SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.OpenAdsorber.ConstantAlpha\">ConstantAlpha</a>: 
  Generic heat transfer correlation with constant heat transfer coefficient.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.OpenAdsorber.ConstantAlphaA\">ConstantAlphaA</a>: 
  Generic heat transfer correlation with constant product of heat transfer 
  coefficient and area.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.OpenAdsorber.CasingGasKast\">CasingGasKast</a>: 
  Heat transfer correlation describing the heat transfer between the gas volume
  and casing according to Kast.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.OpenAdsorber.CasingSorbentKast\">CasingSorbentKast</a>: 
  Heat transfer correlation describing the heat transfer between the gas volume
  and sorbent according to Kast.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end OpenAdsorber;
