within SorpLib.Components.HeatTransfer;
package HeatTransferCoefficientCorrelations "Package containing correlations for heat transfer coefficients"
extends Modelica.Icons.FunctionsPackage;

annotation (Documentation(info="<html>
<p>
This package contains correlations for heat transfer coefficients that can be used 
within heat transfer models. The correlations calculate the product of heat transfer
coefficient and area as a function of fluid property data, gemeotry, and flow regime.
Correlations for heat transfer coefficients are implemented for the following heat
transfer models:
</p>
<ul>
  <li>
  <a href=\"Modelica://SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.Generic\">Generic</a>: 
  Correlations for generic heat transfer coefficients.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.Conduction\">Conduction</a>: 
  Correlations for conduction heat transfer coefficients.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.Radiation\">Radiation</a>: 
  Correlations for radiation heat transfer coefficients.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.PoolBoiling\">PoolBoiling</a>: 
  Correlations for radiation heat transfer describing pool boiling.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.PoolCondensation\">PoolCondensation</a>: 
  Correlations for radiation heat transfer describing (pool) condensation.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.TubeInside\">TubeInside</a>: 
  Correlations for heat transfer coefficients inside of tubes (i.e., (foreced) convection).
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.ClosedAdsorber\">ClosedAdsorber</a>: 
  Correlations for heat transfer coefficients inside of closed adsorbers (i.e., (foreced) convection).
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.OpenAdsorber\">OpenAdsorber</a>: 
  Correlations for heat transfer coefficients inside of open adsorbers (i.e., (foreced) convection).
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructering the library.
  </li>
</ul>
</html>"));
end HeatTransferCoefficientCorrelations;
