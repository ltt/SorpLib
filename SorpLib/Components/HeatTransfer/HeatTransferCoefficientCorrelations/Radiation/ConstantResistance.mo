within SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.Radiation;
model ConstantResistance
  "Constant thermal resistance describing thermal radiation"
  extends
    SorpLib.Components.HeatTransfer.BaseClasses.PartialRadiationHeatTransferCoefficient(
     final computeTransportProperties=false,
     final avoid_events=false);

  //
  // Definition of parameters
  //
  parameter Real constantR(final unit="W/K4") = 1 / Modelica.Constants.sigma / 0.1
    "Constant thermal resistance describing thermal radiation"
    annotation(Dialog(tab = "General", group = "Heat Transfer"));

equation
  alphaA = 1 / (1 * constantR)
    "Calculation of the product of heat transfer coefficient and area";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This simple transfer model calculates the product of heat transfer coefficient and
area for thermal conduction assuming a constant thermal resistance.
</p>

<h4>Main equations</h4>
<p>
The product of heat transfer coefficient and area <i>&alpha;A</i> is calculated 
assuming a constant thermal resistance <i>R<sub>&sigma;</sub></i>:
</p>
<pre>
    &alpha;A = 1 / R<sub>&sigma;</sub>;
</pre>
  
<h4>Typical use</h4>
<p>
This simple heat transfer correlation model is typically used if the heat transfer
conditions do not greatly change and a very simple models is required.
</p>
</html>", revisions="<html>
<ul>
  <li>
  January 16, 2024, by Mirko Engelpracht:<br/>
  First implementation
  </li>
</ul>
</html>"));
end ConstantResistance;
