within SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations;
package Radiation "Correlations for heat transfer coefficients describing thermal radiation"
extends Modelica.Icons.FunctionsPackage;

  annotation (Documentation(info="<html>
<p>
This package contains correlations for heat transfer coefficients describing
thermal radiation:
</p>
<ul>
  <li>
  <a href=\"Modelica://SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.Radiation.ConstantResistance\">ConstantResistance</a>: 
  Constant thermal resistance.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.Radiation.ParallelGraySurfaces\">ParallelGraySurfaces</a>: 
  Constant thermal radiation between two parallel, gray surfaces.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.Radiation.GrayCylinderInGrayCylinder\">GrayCylinderInGrayCylinder</a>: 
  Constant thermal radiation between a gray cylinder within a bigger, gray cylinder.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Radiation;
