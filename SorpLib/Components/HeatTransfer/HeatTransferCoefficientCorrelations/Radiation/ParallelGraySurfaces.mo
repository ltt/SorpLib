within SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.Radiation;
model ParallelGraySurfaces
  "Heat transfer correlation describing thermal radation between two parallel, gray surfaces"
  extends
    SorpLib.Components.HeatTransfer.BaseClasses.PartialRadiationHeatTransferCoefficient(
     final computeTransportProperties=false,
     final avoid_events=false);

  //
  // Definition of parameters
  //
  parameter Real f_correction = 1
    "Correction factor to adjust the heat transfer coefficient correlation"
    annotation(Dialog(tab = "General", group = "Heat Transfer"));

  parameter Real epsilon_1(min=0, max=1) = 0.15
    "Emissivity of suraface 1"
    annotation(Dialog(tab = "General", group = "Heat Transfer"));
  parameter Real epsilon_2(min=0, max=1) = 0.15
    "Emissivity of suraface 2"
    annotation(Dialog(tab = "General", group = "Heat Transfer"));
  parameter Modelica.Units.SI.Area A_surfaces = 0.1
    "Identical area of the surfaces exchanging heat"
    annotation(Dialog(tab = "General", group = "Heat Transfer"));

equation
  alphaA = f_correction * 1 * Modelica.Constants.sigma * A_surfaces /
    (1/epsilon_1 + 1/epsilon_2 - 1)
    "Calculation of the product of heat transfer coefficient and area";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This simple transfer model calculates the product of heat transfer coefficient and
area for thermal radiation between two parallel, gray surfaces that have an equal
area.
</p>

<h4>Main equations</h4>
<p>
The product of heat transfer coefficient and area <i>&alpha;A</i> is calculated 
assuming a constant thermal resistance <i>R<sub>&sigma;</sub></i>:
</p>
<pre>
    &alpha;A = 1 / R<sub>&sigma;</sub> = &sigma; * A / (1/&epsilon;<sub>1</sub> + 1/&epsilon;<sub>2</sub> - 1);
</pre>
<p>
Herein, <i>&sigma;</i> is the Stefan-Blotzmann constant, <i>A</i> is the area of the
surfaces that exhange heat, <i>&epsilon;<sub>1</sub></i> emission constant of surface 
1, and <i>&epsilon;<sub>2</sub></i> emission constant of surface 2.
  
<h4>Typical use</h4>
<p>
This simple heat transfer correlation model is typically used if the heat transfer
conditions do not greatly change. Then, this model can be used to describe thermal
radiation between two parallel, gray surfaces.
</p>

<h4>References</h4>
<ul>
  <li>
  Kabelac, S. and Vortmeyer, D. (2010). K1 Radiation of Surfaces. In: VDI Heat Atlas. VDI-Buch. Springer, Berlin, Heidelberg. DOI: https://doi.org/10.1007/978-3-540-77877-6_64.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  January 16, 2024, by Mirko Engelpracht:<br/>
  First implementation
  </li>
</ul>
</html>"));
end ParallelGraySurfaces;
