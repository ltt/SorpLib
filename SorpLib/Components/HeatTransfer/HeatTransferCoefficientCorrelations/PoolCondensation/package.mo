within SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations;
package PoolCondensation "Correlations for heat transfer coefficients describing (pool) condensation"
extends Modelica.Icons.FunctionsPackage;

  annotation (Documentation(info="<html>
<p>
This package contains correlations for heat transfer coefficients describing
(pool) condensation:
</p>
<ul>
  <li>
  <a href=\"Modelica://SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.PoolCondensation.ConstantAlpha\">ConstantAlpha</a>: 
  Generic heat transfer correlation with constant heat transfer coefficient.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.PoolCondensation.ConstantAlphaA\">ConstantAlphaA</a>: 
  Generic heat transfer correlation with constant product of heat transfer 
  coefficient and area.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.PoolCondensation.LinearAlphaA_T\">LinearAlphaA_T</a>: 
  Generic heat transfer correlation with a product of heat transfer coefficient 
  and area that is linearly dependent on the temperature.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.PoolCondensation.LinearAlphaA_fRel\">LinearAlphaA_fRel</a>: 
  Generic heat transfer correlation with a product of heat transfer coefficient 
  and area that is linearly dependent on the relative filling level.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end PoolCondensation;
