within SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.PoolCondensation;
model LinearAlphaA_T
  "Generic heat transfer correlation with product of heat transfer coefficient and area linearly dependent on the temperature"
  extends
    SorpLib.Components.HeatTransfer.BaseClasses.PartialPoolCondensationHeatTransferCoefficient(
    final computeTransportProperties=false,
    final avoid_events=false);

  //
  // Definition of parameters
  //
  parameter SorpLib.Choices.TemperatureHeatTranferCorrelation calculationTemperature=
    SorpLib.Choices.TemperatureHeatTranferCorrelation.Difference
    "Defines the temperature used to calculate the product of heat transfer
    coefficient and area"
    annotation(Dialog(tab = "General", group = "Heat Transfer"),
              Evaluate=true);

  parameter Modelica.Units.SI.ThermalConductance constantAlphaA = 100
    "Constant product of heat transfer coefficient and area"
    annotation(Dialog(tab = "General", group = "Heat Transfer"));
  parameter Real b(final unit="W/(K2)") = 5000
    "Temperature dependancy (i.e., linear factor) of the product of heat transfer
    coefficient and area"
    annotation(Dialog(tab = "General", group = "Heat Transfer"));

equation
  //
  // Calculation of the product of heat transfer coefficient and area
  //
  if calculationTemperature ==
    SorpLib.Choices.TemperatureHeatTranferCorrelation.PortA then
    alphaA = constantAlphaA + b * abs(T_avg_port_a)
      "Average temperature at ports a";

  elseif calculationTemperature ==
    SorpLib.Choices.TemperatureHeatTranferCorrelation.PortB then
    alphaA = constantAlphaA + b * abs(T_avg_port_b)
      "Average temperature at ports b";

  elseif calculationTemperature ==
    SorpLib.Choices.TemperatureHeatTranferCorrelation.Average then
    alphaA = constantAlphaA + b * abs((T_avg_port_a + T_avg_port_b) / 2)
      "Average temperature at ports a and b";

  else
    alphaA = constantAlphaA + b * abs(T_avg_port_a - T_avg_port_b)
      "Average temperature difference between ports a and b";

  end if;

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This simple transfer model calculates the product of heat transfer coefficient and
area assuming it to be linearly dependent on the temperature. The temperature used
for the calculation can be selected.
</p>

<h4>Main equations</h4>
<p>
The product of heat transfer coefficient and area <i>&alpha;A</i> is calculated 
assuming a constant part <i>&alpha;A<sub>const</sub></i> and a part that is linear
dependent on the temperature <i>T</i>:
</p>
<pre>
    &alpha;A = &alpha;A<sub>const</sub> + b * <strong>abs</strong>(T);
</pre>
<p>
Herein, <i>&alpha;A<sub>const</sub></i> and <i>b</i> are fitting parameters.
</p>

<h4>Typical use</h4>
<p>
This simple heat transfer correlation model is typically used if the heat transfer
conditions do not greatly change and a very simple models is required.
</p>

<h4>Important parameters and options</h4>
<ul>
  <li>
  <i>calculationTemperature</i>:
  Defines the temperature that is used for calculations.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  January 18, 2024, by Mirko Engelpracht:<br/>
  First implementation
  </li>
</ul>
</html>"));
end LinearAlphaA_T;
