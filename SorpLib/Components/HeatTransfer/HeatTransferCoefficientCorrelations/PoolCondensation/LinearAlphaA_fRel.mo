within SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.PoolCondensation;
model LinearAlphaA_fRel
  "Generic heat transfer correlation with product of heat transfer coefficient and area linearly dependent on the temperature"
  extends
    SorpLib.Components.HeatTransfer.BaseClasses.PartialPoolCondensationHeatTransferCoefficient(
    final computeTransportProperties=false,
    final avoid_events=false);

  //
  // Definition of parameters
  //
  parameter Modelica.Units.SI.ThermalConductance constantAlphaA = 15000
    "Constant product of heat transfer coefficient and area"
    annotation(Dialog(tab = "General", group = "Heat Transfer"));
  parameter Modelica.Units.SI.ThermalConductance b = -15000
    "Temperature dependancy (i.e., linear factor) of the product of heat transfer
    coefficient and area"
    annotation(Dialog(tab = "General", group = "Heat Transfer"));

equation
  alphaA = constantAlphaA + b * f_relativeFillingLevel
    "Calculation of the product of heat transfer coefficient and area";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This simple transfer model calculates the product of heat transfer coefficient and
area assuming it to be linearly dependent on the relative filling level.
</p>

<h4>Main equations</h4>
<p>
The product of heat transfer coefficient and area <i>&alpha;A</i> is calculated 
assuming a constant part <i>&alpha;A<sub>const</sub></i> and a part that is linear
dependent on the relative filling level <i>f<sub>rel</sub></i>:
</p>
<pre>
    &alpha;A = &alpha;A<sub>const</sub> + b * f<sub>rel</sub>;
</pre>
<p>
Herein, <i>&alpha;A<sub>const</sub></i> and <i>b</i> are fitting parameters.
</p>

<h4>Typical use</h4>
<p>
This simple heat transfer correlation model is typically used if the heat transfer
conditions do not greatly change and a very simple models is required.
</p>

<h4>Important parameters and options</h4>
<ul>
  <li>
  <i>calculationTemperature</i>:
  Defines the temperature that is used for calculations.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  January 18, 2024, by Mirko Engelpracht:<br/>
  First implementation
  </li>
</ul>
</html>"));
end LinearAlphaA_fRel;
