within SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations;
package PoolBoiling "Correlations for heat transfer coefficients describing pool boiling"
extends Modelica.Icons.FunctionsPackage;

  annotation (Documentation(info="<html>
<p>
This package contains correlations for heat transfer coefficients describing
pool boiling:
</p>
<ul>
  <li>
  <a href=\"Modelica://SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.PoolBoiling.ConstantAlpha\">ConstantAlpha</a>: 
  Generic heat transfer correlation with constant heat transfer coefficient.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.PoolBoiling.ConstantAlphaA\">ConstantAlphaA</a>: 
  Generic heat transfer correlation with constant product of heat transfer 
  coefficient and area.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.PoolBoiling.LinearAlphaA_T\">LinearAlphaA_T</a>: 
  Generic heat transfer correlation with a product of heat transfer coefficient 
  and area that is linearly dependent on the temperature.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.PoolBoiling.LinearAlphaA_fRel\">LinearAlphaA_fRel</a>: 
  Generic heat transfer correlation with a product of heat transfer coefficient 
  and area that is linearly dependent on the relative filling level.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end PoolBoiling;
