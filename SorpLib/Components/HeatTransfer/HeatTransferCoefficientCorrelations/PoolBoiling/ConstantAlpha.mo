within SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.PoolBoiling;
model ConstantAlpha
  "Generic heat transfer correlation with constant heat transfer coefficient"
  extends
    SorpLib.Components.HeatTransfer.BaseClasses.PartialPoolBoilingHeatTransferCoefficient(
    final computeTransportProperties=false,
    final avoid_events=false);

  //
  // Definition of parameters
  //
  parameter Modelica.Units.SI.CoefficientOfHeatTransfer constantAlpha = 10000
    "Constant heat transfer coefficient"
    annotation(Dialog(tab = "General", group = "Heat Transfer"));
  parameter Modelica.Units.SI.Area A = 1
    "Constant heat transfer area"
    annotation(Dialog(tab = "General", group = "Heat Transfer"));

equation
  alphaA = constantAlpha*A
    "Calculation of the product of heat transfer coefficient and area";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This simple transfer model calculates the product of heat transfer coefficient and
area assuming a constant heat transfer coefficient and area.
</p>

<h4>Main equations</h4>
<p>
The product of heat transfer coefficient and area <i>&alpha;A</i> is calculated 
assuming a constant heat transfer coefficient <i>&alpha;</i> and area <i>A</i>:
</p>
<pre>
    &alpha;A = &alpha; * A = const.;
</pre>
  
<h4>Typical use</h4>
<p>
This simple heat transfer correlation model is typically used if the heat transfer
conditions do not greatly change and a very simple models is required.
</p>
</html>", revisions="<html>
<ul>
  <li>
  January 17, 2024, by Mirko Engelpracht:<br/>
  First implementation
  </li>
</ul>
</html>"));
end ConstantAlpha;
