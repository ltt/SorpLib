within SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.Conduction;
model ConstantResistance
  "Constant thermal resistance describing thermal conduction"
  extends
    SorpLib.Components.HeatTransfer.BaseClasses.PartialConductiveHeatTransferCoefficient(
     final computeTransportProperties=false,
     final avoid_events=false);

  //
  // Definition of parameters
  //
  parameter Modelica.Units.SI.ThermalResistance constantR = 0.6 * 0.1 / 0.25
    "Constant thermal resistance describing thermal conduction"
    annotation(Dialog(tab = "General", group = "Heat Transfer"));

equation
  alphaA = no_hydraulicParallelFlows * 1 / constantR
    "Calculation of the product of heat transfer coefficient and area";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This simple transfer model calculates the product of heat transfer coefficient and
area for thermal conduction assuming a constant thermal resistance.
</p>

<h4>Main equations</h4>
<p>
The product of heat transfer coefficient and area <i>&alpha;A</i> is calculated 
assuming a constant thermal resistance <i>R<sub>&lambda;</sub></i>. It is 
enlarged by the number of hydrualic parallel flows <i>no<sub>parallel flows</sub></i>
to account for parallel flows modeled by just one flow:
</p>
<pre>
    &alpha;A = no<sub>parallel flows</sub> * 1 / R<sub>&lambda;</sub>;
</pre>
  
<h4>Typical use</h4>
<p>
This simple heat transfer correlation model is typically used if the heat transfer
conditions do not greatly change and a very simple models is required.
</p>
</html>", revisions="<html>
<ul>
  <li>
  January 15, 2024, by Mirko Engelpracht:<br/>
  First implementation
  </li>
</ul>
</html>"));
end ConstantResistance;
