within SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations;
package Conduction "Correlations for heat transfer coefficients describing thermal conduction"
extends Modelica.Icons.FunctionsPackage;

  annotation (Documentation(info="<html>
<p>
This package contains correlations for heat transfer coefficients describing
thermal conduction:
</p>
<ul>
  <li>
  <a href=\"Modelica://SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.Conduction.ConstantResistance\">ConstantResistance</a>: 
  Constant thermal resistance.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.Conduction.ConstantPlainWall\">ConstantPlainWall</a>: 
  Constant thermal conduction through plane wall.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.Conduction.ConstantCylindricalWall\">ConstantCylindricalWall</a>: 
  Constant thermal conduction through cylindrical wall.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.Conduction.PlainWall\">PlainWall</a>: 
  Thermal conduction through plane wall based on fluid properties.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.Conduction.CylindricalWall\">CylindricalWall</a>: 
  Thermal conduction through cylindrical wall based on fluid properties.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Conduction;
