within SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.Conduction;
model CylindricalWall
  "Heat transfer correlation describing thermal conduction through a cylindrical wall"
  extends
    SorpLib.Components.HeatTransfer.BaseClasses.PartialConductiveHeatTransferCoefficient(
     final computeTransportProperties=true,
     final avoid_events=false);

  //
  // Definition of parameters
  //
  parameter Real f_correction = 1
    "Correction factor to adjust the heat transfer coefficient correlation"
    annotation(Dialog(tab = "General", group = "Heat Transfer"));

  parameter Modelica.Units.SI.Length l_wall = 0.25
    "Length of the wall"
    annotation(Dialog(tab = "General", group = "Heat Transfer"));
  parameter Modelica.Units.SI.Diameter d_inner = 0.010
    "Inner diameter of the wall"
    annotation(Dialog(tab = "General", group = "Heat Transfer"));
  parameter Modelica.Units.SI.Diameter d_outer = 0.012
    "Outer diameter of the wall"
    annotation(Dialog(tab = "General", group = "Heat Transfer"));

equation
  alphaA = f_correction *
    no_hydraulicParallelFlows * fluidProperties.lambda * 2 *
    Modelica.Constants.pi * l_wall / Modelica.Math.log(d_outer / d_inner)
    "Calculation of the product of heat transfer coefficient and area";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This simple transfer model calculates the product of heat transfer coefficient and
area for thermal conduction through a cylindrical wall.
</p>

<h4>Main equations</h4>
<p>
The product of heat transfer coefficient and area <i>&alpha;A</i> is calculated 
assuming a thermal resistance <i>R<sub>&lambda;</sub></i>. It is 
enlarged by the number of hydrualic parallel flows <i>no<sub>parallel flows</sub></i>
to account for parallel flows modeled by just one flow:
</p>
<pre>
    &alpha;A = no<sub>parallel flows</sub> * 1 / R<sub>&lambda;</sub> = no<sub>parallel flows</sub> * &lambda; * 2 * &pi; * l / <strong>ln</strong>(d<sub>outer</sub>/d<sub>inner</sub>);
</pre>
<p>
Herein, <i>&lambda;</i> is the thermal conductivity, <i>l</i> is the wall length, 
<i>d<sub>outer</sub></i> is the outer diameter of the cylinder, and <i>d<sub>inner</sub></i>
is the inner diameter of the cylinder.
  
<h4>Typical use</h4>
<p>
This simple heat transfer correlation model is typically used to describe thermal
conduction, e.g., in radial flow direction of a heat exchanger tube.
</p>

<h4>References</h4>
<ul>
  <li>
  Hahne, E. (2010). E1 Steady-State Heat Conduction. In: VDI Heat Atlas. VDI-Buch. Springer, Berlin, Heidelberg. DOI: https://doi.org/10.1007/978-3-540-77877-6_32.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  January 15, 2024, by Mirko Engelpracht:<br/>
  First implementation
  </li>
</ul>
</html>"));
end CylindricalWall;
