within SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.Conduction;
model ConstantPlainWall
  "Heat transfer correlation describing thermal conduction through a plain wall using constant fluid properties"
  extends
    SorpLib.Components.HeatTransfer.BaseClasses.PartialConductiveHeatTransferCoefficient(
     final computeTransportProperties=false,
     final avoid_events=false);

  //
  // Definition of parameters
  //
  parameter Real f_correction = 1
    "Correction factor to adjust the heat transfer coefficient correlation"
    annotation(Dialog(tab = "General", group = "Heat Transfer"));

  parameter Modelica.Units.SI.ThermalConductivity lambda = 230
    "Thermal conductivity of the wall"
    annotation(Dialog(tab = "General", group = "Heat Transfer"));
  parameter Modelica.Units.SI.Area A_cross = 0.1
    "Cross-sectional area of the wall"
    annotation(Dialog(tab = "General", group = "Heat Transfer"));
  parameter Modelica.Units.SI.Thickness delta_wall = 0.25
    "Thickness of the wall"
    annotation(Dialog(tab = "General", group = "Heat Transfer"));

equation
  alphaA = f_correction *
    no_hydraulicParallelFlows * lambda * A_cross / delta_wall
    "Calculation of the product of heat transfer coefficient and area";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This simple transfer model calculates the product of heat transfer coefficient and
area for thermal conduction through a plain wall assuming consant fluid properties.
</p>

<h4>Main equations</h4>
<p>
The product of heat transfer coefficient and area <i>&alpha;A</i> is calculated 
assuming a constant thermal resistance <i>R<sub>&lambda;</sub></i>. It is 
enlarged by the number of hydrualic parallel flows <i>no<sub>parallel flows</sub></i>
to account for parallel flows modeled by just one flow:
</p>
<pre>
    &alpha;A = no<sub>parallel flows</sub> * 1 / R<sub>&lambda;</sub> = no<sub>parallel flows</sub> * &lambda; * A<sub>cross</sub> / &delta;;
</pre>
<p>
Herein, <i>&lambda;</i> is the thermal conductivity, <i>A<sub>cross</sub></i> is the 
cross sectional area of the plain wall, and <i>&delta;</i> is the wall thickness.
  
<h4>Typical use</h4>
<p>
This simple heat transfer correlation model is typically used if the heat transfer
conditions do not greatly change. Then, this model can be used to describe thermal
conduction, e.g., in axial flow direction of a heat exchanger tube.
</p>

<h4>References</h4>
<ul>
  <li>
  Hahne, E. (2010). E1 Steady-State Heat Conduction. In: VDI Heat Atlas. VDI-Buch. Springer, Berlin, Heidelberg. DOI: https://doi.org/10.1007/978-3-540-77877-6_32.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  January 15, 2024, by Mirko Engelpracht:<br/>
  First implementation
  </li>
</ul>
</html>"));
end ConstantPlainWall;
