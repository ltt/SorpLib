within SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.ClosedAdsorber;
model ExponentialAlphaA
  "Generic heat transfer correlation with product of heat transfer coefficient and area exponentially dependent on the temperature"
  extends
    SorpLib.Components.HeatTransfer.BaseClasses.PartialClosedAdsorberHeatTransferCoefficient(
    final computeTransportProperties=false,
    final avoid_events=false);

  //
  // Definition of parameters
  //
  parameter SorpLib.Choices.TemperatureHeatTranferCorrelation calculationTemperature=
    SorpLib.Choices.TemperatureHeatTranferCorrelation.PortA
    "Defines the temperature used to calculate the product of heat transfer
    coefficient and area"
    annotation(Dialog(tab = "General", group = "Heat Transfer"),
              Evaluate=true);

  parameter Modelica.Units.SI.ThermalConductance constantAlphaA = 25
    "Constant product of heat transfer coefficient and area"
    annotation(Dialog(tab = "General", group = "Heat Transfer"));
  parameter Modelica.Units.SI.ThermalConductance b = 0.1
    "Temperature dependancy (i.e., factor) of the exponential part"
    annotation(Dialog(tab = "General", group = "Heat Transfer"));
  parameter Real c(final unit="1/K") = 1/273.15
    "Temperature dependancy (i.e., exponential factor) of the exponential part"
    annotation(Dialog(tab = "General", group = "Heat Transfer"));

equation
  //
  // Calculation of the product of heat transfer coefficient and area
  //
  if calculationTemperature ==
    SorpLib.Choices.TemperatureHeatTranferCorrelation.PortA then
    alphaA = geometry.no_hydraulicParallelTubes * (
      constantAlphaA + b * exp(c * abs(T_avg_port_a))) /
      min(geometry.no_wallVolumes, geometry.no_sorbentVolumes)
      "Average temperature at ports a";

  elseif calculationTemperature ==
    SorpLib.Choices.TemperatureHeatTranferCorrelation.PortB then
    alphaA = geometry.no_hydraulicParallelTubes * (
      constantAlphaA + b * exp(c * abs(T_avg_port_b))) /
      min(geometry.no_wallVolumes, geometry.no_sorbentVolumes)
      "Average temperature at ports b";

  elseif calculationTemperature ==
    SorpLib.Choices.TemperatureHeatTranferCorrelation.Average then
    alphaA = geometry.no_hydraulicParallelTubes * (
      constantAlphaA + b * exp(c * abs((T_avg_port_a + T_avg_port_b) / 2))) /
      min(geometry.no_wallVolumes, geometry.no_sorbentVolumes)
      "Average temperature at ports a and b";

  else
    alphaA = geometry.no_hydraulicParallelTubes * (
      constantAlphaA + b * exp(c * abs(T_avg_port_a - T_avg_port_b))) /
      min(geometry.no_wallVolumes, geometry.no_sorbentVolumes)
      "Average temperature difference between ports a and b";

  end if;

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This simple transfer model calculates the product of heat transfer coefficient and
area assuming it to be exponentially dependent on the temperature. The temperature 
used for the calculation can be selected.
</p>

<h4>Main equations</h4>
<p>
The product of heat transfer coefficient and area <i>&alpha;A</i> is calculated 
assuming a constant part <i>&alpha;A<sub>const</sub></i> and a part that is exponential
dependent on the temperature <i>T</i>:
</p>
<pre>
    &alpha;A = no<sub>parallel flows</sub> * [&alpha;A<sub>const</sub> + b * <strong>exp</strong>(c * <strong>abs</strong>(T))] / <strong>min</strong>(no<sub>sorben volumes</sub>, no<sub>wall volumes</sub>);
</pre>
<p>
Herein, <i>&alpha;A<sub>const</sub></i> and <i>b</i> are fitting parameters.
<br/><br/>
The product is enlarged by the number of hydrualic parallel flows 
<i>no<sub>parallel flows</sub></i> to account for parallel flows modeled by just 
one flow. Furhtermore, the product is diveded by the smaller discretization number
of sorbent or wall volumes to account for discretization: The smaller discretization 
number determines the number of heat transfer models that are used. Thus, the product
<i>&alpha;A</i> is valid for one heat exchanger tube.
</p>


<h4>Typical use</h4>
<p>
This simple heat transfer correlation model is typically used if the heat transfer
conditions do not greatly change and a very simple models is required.
</p>

<h4>Important parameters and options</h4>
<ul>
  <li>
  <i>calculationTemperature</i>:
  Defines the temperature that is used for calculations.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  January 22, 2024, by Mirko Engelpracht:<br/>
  First implementation
  </li>
</ul>
</html>"));
end ExponentialAlphaA;
