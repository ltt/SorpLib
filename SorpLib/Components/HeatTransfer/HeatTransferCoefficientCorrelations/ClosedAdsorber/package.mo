within SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations;
package ClosedAdsorber "Correlations for heat transfer coefficients describing the heat transfer within closed adsorbers"
extends Modelica.Icons.FunctionsPackage;

  annotation (Documentation(info="<html>
<p>
This package contains correlations for heat transfer coefficients describing
the heat transfer between sorbent and heat exchanger within closed adsorbers:
</p>
<ul>
  <li>
  <a href=\"Modelica://SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.ClosedAdsorber.ConstantAlpha\">ConstantAlpha</a>: 
  Generic heat transfer correlation with constant heat transfer coefficient.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.ClosedAdsorber.ConstantAlphaA\">ConstantAlphaA</a>: 
  Generic heat transfer correlation with constant product of heat transfer 
  coefficient and area.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.ClosedAdsorber.LinearAlphaA\">LinearAlphaA</a>: 
  Generic heat transfer correlation with a product of heat transfer coefficient 
  and area that is linearly dependent on the temperature.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.ClosedAdsorber.ExponentialAlphaA\">ExponentialAlphaA</a>: 
  Generic heat transfer correlation with a product of heat transfer coefficient 
  and area that is exponentially dependent on the temperature.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.ClosedAdsorber.PolynomialAlphaA\">PolynomialAlphaA</a>: 
  Generic heat transfer correlation with a product of heat transfer coefficient 
  and area that is polynomially dependent on the temperature.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end ClosedAdsorber;
