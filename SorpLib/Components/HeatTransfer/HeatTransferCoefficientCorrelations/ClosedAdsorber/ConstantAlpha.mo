within SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.ClosedAdsorber;
model ConstantAlpha
  "Generic heat transfer correlation with constant heat transfer coefficient"
  extends
    SorpLib.Components.HeatTransfer.BaseClasses.PartialClosedAdsorberHeatTransferCoefficient(
    final computeTransportProperties=false,
    final avoid_events=false);

  //
  // Definition of parameters
  //
  parameter Modelica.Units.SI.CoefficientOfHeatTransfer constantAlpha = 1000
    "Constant heat transfer coefficient"
    annotation(Dialog(tab = "General", group = "Heat Transfer"));
  parameter Modelica.Units.SI.Area A = geometry.A_heatTransferOuter_hx
    "Constant heat transfer area"
    annotation(Dialog(tab = "General", group = "Heat Transfer"));

equation
  alphaA = geometry.no_hydraulicParallelTubes * constantAlpha * A /
    min(geometry.no_wallVolumes, geometry.no_sorbentVolumes)
    "Calculation of the product of heat transfer coefficient and area";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This simple transfer model calculates the product of heat transfer coefficient and
area assuming a constant heat transfer coefficient and area.
</p>

<h4>Main equations</h4>
<p>
The product of heat transfer coefficient and area <i>&alpha;A</i> is calculated 
assuming a constant heat transfer coefficient <i>&alpha;</i> and area <i>A</i>. 
The product is enlarged by the number of hydrualic parallel flows <i>no<sub>parallel flows</sub></i>
to account for parallel flows modeled by just one flow:
</p>
<pre>
    &alpha;A = no<sub>parallel flows</sub> * &alpha; * A / <strong>min</strong>(no<sub>sorbent volumes</sub>, no<sub>wall volumes</sub>) = const.;
</pre>
<p>
Note that the area <i>A</i> is calculated from the geometry record taking into
account the disretization: The smaller number of discretization volumes used for 
the wall and the sorbent determines the number of heat transfer models that are used. 
Accordingly, the total heat transfer surface is divided by the smaller number.
</p>
  
<h4>Typical use</h4>
<p>
This simple heat transfer correlation model is typically used if the heat transfer
conditions do not greatly change and a very simple models is required.
</p>
</html>", revisions="<html>
<ul>
  <li>
  January 22, 2024, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end ConstantAlpha;
