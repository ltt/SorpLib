within SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations;
package TubeInside "Correlations for heat transfer coefficients describing convective heat transfer within tubes"
extends Modelica.Icons.FunctionsPackage;

  annotation (Documentation(info="<html>
<p>
This package contains correlations for heat transfer coefficients describing
convective heat transfer within tubes:
</p>
<ul>
  <li>
  <a href=\"Modelica://SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.TubeInside.ConstantAlpha\">ConstantAlpha</a>: 
  Generic heat transfer correlation with constant heat transfer coefficient.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.TubeInside.ConstantAlphaA\">ConstantAlphaA</a>: 
  Generic heat transfer correlation with constant product of heat transfer 
  coefficient and area.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.TubeInside.GnielinskiDittusBoelter\">GnielinskiDittusBoelter</a>: 
  Heat transfer correlation according to Gielinski, Dittus, and Boelter for
  laminar and turbulent flow regimes within straigt tubes.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.TubeInside.Schmidt\">Schmidt</a>: 
  Heat transfer correlation according to Schmidt for laminar and turbulent 
  flow regimes within helix tubes.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end TubeInside;
