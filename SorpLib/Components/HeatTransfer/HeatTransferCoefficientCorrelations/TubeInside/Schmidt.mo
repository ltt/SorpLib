within SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.TubeInside;
model Schmidt
  "Heat transfer correlation according to Schmidt for helix tubes"
  extends
    SorpLib.Components.HeatTransfer.BaseClasses.PartialTubeInsideHeatTransferCoefficient(
    final computeTransportProperties=true);

  //
  // Definition of parameters
  //
  parameter SorpLib.Choices.MassFlowRateHeatTranferCorrelation calculationMassFlowRate=
    SorpLib.Choices.MassFlowRateHeatTranferCorrelation.PortXMinus
    "Defines the hydraulic mass flow rate used for calculations"
    annotation(Dialog(tab = "General", group = "Heat Transfer"),
              Evaluate=true);

  parameter Modelica.Units.SI.Length h_coil = 0.015
    "Gradient of the helix (i.e., pitch between two coils)"
    annotation(Dialog(tab = "General", group = "Heat Transfer"));
  parameter Modelica.Units.SI.Diameter d_coil = 0.1
    "Average diameter of the helix (i.e., top-view projection)"
    annotation(Dialog(tab = "General", group = "Heat Transfer"));
  parameter Modelica.Units.SI.Diameter d_helix=
    d_coil * (1 + (h_coil / (Modelica.Constants.pi * d_coil))^2)
    "Average beinding diameter of the tube"
    annotation(Dialog(tab = "General", group = "Heat Transfer"));

  parameter Real f_correctionLaminar = 1
    "Correction factor to adjust the Nusselt-correlation for the laminar regime"
    annotation(Dialog(tab = "General", group = "Heat Transfer"));
  parameter Real f_correctionTurbulent = 1
    "Correction factor to adjust the Nusselt-correlation for the turbulent regime"
    annotation(Dialog(tab = "General", group = "Heat Transfer"));

  parameter Modelica.Units.SI.ReynoldsNumber Re_critLaminar=
    2300 * (1 + 8.6 * (geometry.d_hydInner / d_helix)^(0.45))
    "Critical Reynolds number for the laminar regime"
    annotation(Dialog(tab = "General", group = "Heat Transfer"),
              Evaluate=true,
              HideResult=true);
  parameter Modelica.Units.SI.ReynoldsNumber Re_critTurbulent = 2.2e4
    "Critical Reynolds number for the turbulent regime"
    annotation(Dialog(tab = "General", group = "Heat Transfer"),
              Evaluate=true,
              HideResult=true);

  parameter Integer noDiff = 2
    "Specification how often transition functions can be differentiated"
    annotation(Dialog(tab = "Advanced", group = "Numerics"),
              Evaluate=true,
              HideResult=true);
  parameter Modelica.Units.SI.ReynoldsNumber Re_transitionLaminar = 10
    "Transition length for the laminar regime"
    annotation(Dialog(tab = "Advanced", group = "Numerics"),
              Evaluate=true,
              HideResult=true);
  parameter Modelica.Units.SI.ReynoldsNumber Re_transitionTurbulent = 100
    "Transition length for the turbulent regime"
    annotation(Dialog(tab = "Advanced", group = "Numerics"),
              Evaluate=true,
              HideResult=true);

  //
  // Definition of variables
  //
  Modelica.Units.SI.CoefficientOfHeatTransfer alpha
    "Heat tranfer coefficient";

  Modelica.Units.SI.ReynoldsNumber Re
    "Reynolds number";
  Modelica.Units.SI.PrandtlNumber Pr
    "Prandtl number";
  Modelica.Units.SI.ReynoldsNumber Nu
    "Nusselt number";

  Modelica.Units.SI.NusseltNumber Nu_lamianr
    "Nusselt number for laminar flow regime";
  Modelica.Units.SI.NusseltNumber Nu_transition
    "Nusselt number for transition regime";
  Modelica.Units.SI.NusseltNumber Nu_turbulent
    "Nusselt number for turbulent flow regime";

  //
  // Definition of protected variables
  //
protected
  Modelica.Units.SI.NusseltNumber Nu_laminar_transition
    "Nusselt number for laminar regime at the transition point";
  Modelica.Units.SI.NusseltNumber Nu_turbulent_transition
    "Nusselt number for turbulent regime at the transition point";

  Modelica.Units.SI.NusseltNumber Nu_aux
    "Nusselt number according to transition between laminar and turbulent
    regime";

  Modelica.Units.SI.Area A_heatTransfer
    "Heat transfer area accounting for discretization";

  Real m
    "Auxillary variable for the laminar Nusselt correlation";
  Real zeta
    "Auxillary variable for the turbulent Nusselt correlation";
  Real zeta_transition
    "Auxillary variable for the turbulent Nusselt correlation at the transition 
    point";

  Real wf_laminarTransition
    "Transition factor for change between laminar and transition regime";
  Real gamma
    "Transition fractor transition regime";
  Real wf_transitionTurbulent
    "Transition factor for change between transition regime and turbulent regime";

equation
  //
  // Calculation of transition functions
  //
  gamma = (Re_critTurbulent - Re) / (Re_critTurbulent - Re_critLaminar)
    "Transition fractor transition regime";

  if avoid_events then
    wf_laminarTransition  = SorpLib.Numerics.smoothTransition_noEvent(
      x=Re,
      transitionPoint=Re_critLaminar,
      transitionLength=Re_transitionLaminar,
      noDiff=noDiff)
      "Transition factor for change between laminar and transition regime";
    wf_transitionTurbulent = SorpLib.Numerics.smoothTransition_noEvent(
      x=Re,
      transitionPoint=Re_critTurbulent,
      transitionLength=Re_transitionTurbulent,
      noDiff=noDiff)
      "Transition factor for change between transition regime and turbulent 
      regime";

  else
    wf_laminarTransition  = SorpLib.Numerics.smoothTransition(
      x=Re,
      transitionPoint=Re_critLaminar,
      transitionLength=Re_transitionLaminar,
      noDiff=noDiff)
      "Transition factor for change between laminar and transition regime";
    wf_transitionTurbulent = SorpLib.Numerics.smoothTransition(
      x=Re,
      transitionPoint=Re_critTurbulent,
      transitionLength=Re_transitionTurbulent,
      noDiff=noDiff)
      "Transition factor for change between transition regime and turbulent 
      regime";

  end if;

  //
  // Calculation of the Reynolds and Prandtl number
  //
  if calculationMassFlowRate==
    SorpLib.Choices.MassFlowRateHeatTranferCorrelation.PortXMinus then
    Re = abs(m_hyd_xMinus) * geometry.d_hydInner /
      (geometry.A_hydCrossInner * fluidProperties.eta)
      "Reynolds number";

  elseif calculationMassFlowRate==
    SorpLib.Choices.MassFlowRateHeatTranferCorrelation.PortXPlus then
    Re = abs(m_hyd_xPlus) * geometry.d_hydInner /
      (geometry.A_hydCrossInner * fluidProperties.eta)
      "Reynolds number";

  else
    Re = (abs(m_hyd_xMinus) + abs(m_hyd_xPlus)) / 2 * geometry.d_hydInner /
      (geometry.A_hydCrossInner * fluidProperties.eta)
      "Reynolds number";

  end if;

  Pr = fluidProperties.eta * fluidProperties.cp / fluidProperties.lambda
    "Prandtl number";

  //
  // Calculation of the Nusselt numbers
  //
  A_heatTransfer = geometry.A_heatTransferInner /
    min(geometry.no_fluidVolumes, geometry.no_wallVolumes)
    "Length of the tube accounting for discretization";

  m = 0.5 + 0.2903 * (geometry.d_hydInner / d_helix)^0.194
    "Auxillary variable for the laminar Nusselt correlation";
  zeta = 0.3164 / max(Re,1e-12)^0.25 +
    0.03 * (geometry.d_hydInner/d_helix)^0.5
    "Auxillary variable for Gnielinski's Nusselt correlation";
  zeta_transition = 0.3164 / max(Re_critTurbulent,1e-12)^0.25 +
    0.03 * (geometry.d_hydInner/d_helix)^0.5
    "Auxillary variable for Gnielinski's Nusselt correlation at the transition 
    point";

  Nu_lamianr = f_correctionLaminar * (3.66 + 0.08 * (1 + 0.8 *
    (geometry.d_hydInner / d_helix)^0.9) * Re^m * Pr^(1/3))
    "Nusselt number for laminar flow regime";
  Nu_laminar_transition = f_correctionLaminar * (3.66 + 0.08 * (1 + 0.8 *
    (geometry.d_hydInner / d_helix)^0.9) * Re_critLaminar^m * Pr^(1/3))
    "Nusselt number for laminar regime at the transition point";
  Nu_transition = gamma*Nu_laminar_transition +
    (1-gamma) * Nu_turbulent_transition
    "Nusselt number for transition regime";
  Nu_turbulent_transition = f_correctionTurbulent * (zeta_transition/8) *
    Re_critTurbulent * Pr / (1 + 12.7*sqrt(zeta_transition/8) * (Pr^(2/3) - 1))
    "Nusselt number for turbulent regime at the transition point";
  Nu_turbulent = f_correctionTurbulent * (zeta/8) * Re * Pr /
    (1 + 12.7*sqrt(zeta/8) * (Pr^(2/3) - 1))
    "Nusselt number for turbulent flow regime";

  Nu_aux = wf_laminarTransition * Nu_lamianr +
    (1-wf_laminarTransition) * Nu_transition
    "Nusselt number according to transition between laminar and turbulent
    regime";
  Nu = wf_transitionTurbulent * Nu_transition +
    (1-wf_transitionTurbulent) * Nu_turbulent
    "Nusselt number";

  //
  // Calculation of the heat transfer coefficient
  //
  alpha = Nu * fluidProperties.lambda/geometry.d_hydInner
    "Heat transfer coefficient";
  alphaA = geometry.no_hydraulicParallelTubes * alpha * A_heatTransfer
    "Product of heat transfer coefficient and area";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This heat transfer model calculates the product of heat transfer coefficient and
area describing convective heat transfer within helix tubes. The model considers 
both, the laminar and turbulent flow regime.
</p>

<h4>Main equations</h4>
<p>
The product of heat transfer coefficient and area <i>&alpha;A</i> is calculated
from the Nussel number <i>Nu</i> using Nusselt correlations for different flow
regimes:
</p>
<pre>
    &alpha;A = no<sub>parallel flows</sub> * Nu * &lambda; / d<sub>hyd,inner</sub> * A<sub>heat transfer</sub> / <strong>min</strong>(no<sub>fluid volumes</sub>, no<sub>wall volumes</sub>);
</pre>
<p>
Herein, <i>&lambda;</i> is the thermal conductivity, <i>d<sub>hyd,inner</sub></i> 
is the hydraulic inner diameter, and <i>A<sub>heat transfer</sub></i> is the heat 
transfer area accounting for the discretization. The product is enlarged by the
number of hydrualic parallel flows <i>no<sub>parallel flows</sub></i> to account 
for parallel flows modeled by just one flow. 
<br/><br/>
The Nusselt number depends on the flow regime:
</p>
<pre>
    Nu<sub>laminar</sub> = f<sub>correction,laminar</sub> * [3.66 + 0.08 * (1 + 0.8 * (d<sub>hyd,inner</sub>/d<sub>helix</sub>)<sup>0.9</sup>) * Re^m * Pr^(1/3)];

    Nu<sub>transition</sub> = (1-&gamma;) * Nu<sub>turbulent</sub>(Re<sub>crit,turbulent</sub>) + &gamma; * Nu<sub>laminar</sub>(Re<sub>crit,laminar</sub>);

    Nu<sub>turbulent</sub> = f<sub>correction,turbulent</sub> * [(&zeta;/8) * Re * Pr / (1 + 12.7 * <strong>sqrt</strong>(&zeta;/8) * (Pr<sup>(2/3)</sup> - 1))];
</pre>
<p>
with:
</p>
<pre>
    m = 0.5 + 0.2903 * (d<sub>hyd,inner</sub>/d<sub>helix</sub>)<sup>0.194</sup>;

    &zeta; = 0.3164 / Re^(0.25) + 0.03 * (d<sub>hyd,inner</sub>/d<sub>helix</sub>)<sup>0.5</sup>;

    &gamma; = (Re<sub>crit,turbulent</sub> - Re) / (Re<sub>crit,turbulent</sub> - Re<sub>crit,laminar</sub>);
</pre>
<p>
Herein, <i>f<sub>i</sub></i> are correction factors for the Nusselt correlations
<i>Nu<sub>i</sub></i> of the different flow regimes <i>i</i>, <i>Re</i> is the
Reynolds number, <i>Pr</i> is the Prandtl number, <i>m</i> is a auxillary variable
for the laminar flow regime, <i>&zeta;</i> is a auxillary variable for the turbulent
flow regime, <i>&gamma;</i> is a transition factor, and <i>d<sub>helix</sub></i> 
is the average bending diameter of the helix tube:
</p>
<pre>
    Re = <strong>abs</strong>(m<sub>flow,hyd</sub>) * d<sub>hyd,inner</sub> / A<sub>cross,inner</sub> / &eta;;

    Pr = &eta; * c<sub>p</sub> / &lambda;;

    d<sub>helix</sub> = d<sub>coil</sub> * [1 + (h<sub>coil</sub> / &pi; / d<sub>coil</sub>)<sup>2</sup>];
</pre>
<p>
Herein, <i>m<sub>flow,hyd</sub></i> is the hydraulic mass flow rate, 
<i>A<sub>cross,inner</sub></i> is the hydraulic cross-sectional inner area, 
<i>&eta;</i> is the dynamic viscosity, <i>&lambda;</i> is the thermal conductivity, 
<i>c<sub>p</sub></i> is ther isobaric heat capacity, <i>d<sub>coil</sub></i> is 
the diameter of one coil (i.e., top-down view), and <i>h<sub>coil</sub></i> is the 
gradient of the helix (i.e., pitch between two coils).
<br/><br/>
Smooth transition is applied using the function
<a href=\"Modelica://SorpLib.Numerics.smoothTransition\">SorpLib.Numerics.smoothTransition</a>
to change between the different regimes using the corresponding critical Reynold
numbers <i>Re<sub>i</sub></i>.
</p>

<h4>Typical use</h4>
<p>
This heat transfer correlation model is typically used to describe convective
heat transfer within tubes if accurate results are required.
</p>

<h4>Important parameters and options</h4>
<ul>
  <li>
  <i>calculationMassFlowRate</i>:
  Defines the hydraulic mass flow rate that is used for calculations.
  </li>
  <br/>
  <li>
  <i>avoid_events</i>:
  Defines if events shall be avoided via the noEvent()-operator.
  </li>
</ul>

<h4>References</h4>
<ul>
  <li>
  Gnielinski, V. (2010). G3 Heat Transfer in Helically Coiled Tubes. In: VDI Heat Atlas. VDI-Buch. Springer, Berlin, Heidelberg. DOI: https://doi.org/10.1007/978-3-540-77877-6_36.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  January 19, 2024, by Mirko Engelpracht:<br/>
  Major extensions (more flow regimes, documentation, stability).
  </li>
  <li>
  January 14, 2021, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Schmidt;
