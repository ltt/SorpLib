within SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.TubeInside;
model GnielinskiDittusBoelter
  "Heat transfer correlation according to Gnielinski, Dittus, and Boelter for straight tubes"
  extends
    SorpLib.Components.HeatTransfer.BaseClasses.PartialTubeInsideHeatTransferCoefficient(
    final computeTransportProperties=true);

  //
  // Definition of parameters
  //
  parameter SorpLib.Choices.MassFlowRateHeatTranferCorrelation calculationMassFlowRate=
    SorpLib.Choices.MassFlowRateHeatTranferCorrelation.PortXMinus
    "Defines the hydraulic mass flow rate used for calculations"
    annotation(Dialog(tab = "General", group = "Heat Transfer"),
              Evaluate=true);

  parameter Real f_correctionNoFlow = 1
    "Correction factor to adjust the Nusselt-correlation for the no-flow regime"
    annotation(Dialog(tab = "General", group = "Heat Transfer"));
  parameter Real f_correctionLaminar = 1
    "Correction factor to adjust the Nusselt-correlation for the laminar regime"
    annotation(Dialog(tab = "General", group = "Heat Transfer"));
  parameter Real f_correctionGnielinski = 1
    "Correction factor to adjust the Nusselt-correlation for the turbulent regime
    described by the correlation of Gnielinski"
    annotation(Dialog(tab = "General", group = "Heat Transfer"));
  parameter Real f_correctionDittusBoelter = 1.7
    "Correction factor to adjust the Nusselt-correlation for the turbulent regime
    described by the correlation of Dittus and Boelter"
    annotation(Dialog(tab = "General", group = "Heat Transfer"));

  parameter Modelica.Units.SI.ReynoldsNumber Re_critNoFlow = 10
    "Critical Reynolds number for the no-flow regime"
    annotation(Dialog(tab = "General", group = "Heat Transfer"),
              Evaluate=true,
              HideResult=true);
  parameter Modelica.Units.SI.ReynoldsNumber Re_critLaminar = 2300
    "Critical Reynolds number for the laminar regime"
    annotation(Dialog(tab = "General", group = "Heat Transfer"),
              Evaluate=true,
              HideResult=true);
  parameter Modelica.Units.SI.ReynoldsNumber Re_critGnielinski = 1e4
    "Critical Reynolds number for the turbulent regime described by the 
    correlation of Gnielinski"
    annotation(Dialog(tab = "General", group = "Heat Transfer"),
              Evaluate=true,
              HideResult=true);
  parameter Modelica.Units.SI.ReynoldsNumber Re_critDittusBoelter = 1e6
    "Critical Reynolds number for the turbulent regime described by the 
    correlation of Dittus and Boelter"
    annotation(Dialog(tab = "General", group = "Heat Transfer"),
              Evaluate=true,
              HideResult=true);

  parameter Integer noDiff = 2
    "Specification how often transition functions can be differentiated"
    annotation(Dialog(tab = "Advanced", group = "Numerics"),
              Evaluate=true,
              HideResult=true);
  parameter Modelica.Units.SI.ReynoldsNumber Re_transitionNoFlow = 1
    "Transition length for the no-flow regime"
    annotation(Dialog(tab = "Advanced", group = "Numerics"),
              Evaluate=true,
              HideResult=true);
  parameter Modelica.Units.SI.ReynoldsNumber Re_transitionLaminar = 10
    "Transition length for the laminar regime"
    annotation(Dialog(tab = "Advanced", group = "Numerics"),
              Evaluate=true,
              HideResult=true);
  parameter Modelica.Units.SI.ReynoldsNumber Re_transitionTurbulent = 100
    "Transition length for the turbulent regime"
    annotation(Dialog(tab = "Advanced", group = "Numerics"),
              Evaluate=true,
              HideResult=true);

  //
  // Definition of variables
  //
  Modelica.Units.SI.CoefficientOfHeatTransfer alpha
    "Heat tranfer coefficient";

  Modelica.Units.SI.ReynoldsNumber Re
    "Reynolds number";
  Modelica.Units.SI.PrandtlNumber Pr
    "Prandtl number";
  Modelica.Units.SI.ReynoldsNumber Nu
    "Nusselt number";

  Modelica.Units.SI.NusseltNumber Nu_noFlow
    "Nusselt number for no-flow regime";
  Modelica.Units.SI.NusseltNumber Nu_lamianr
    "Nusselt number for laminar flow regime";
  Modelica.Units.SI.NusseltNumber Nu_transition
    "Nusselt number for transition regime";
  Modelica.Units.SI.NusseltNumber Nu_Gnielinski
    "Nusselt number for turbulent flow regime according to Gnielinski";
  Modelica.Units.SI.NusseltNumber Nu_DittusBoelter
    "Nusselt number for turbulent flow regime according to Dittus and Boelter";

  //
  // Definition of protected variables
  //
protected
  Modelica.Units.SI.NusseltNumber Nu_lamianrFlow_transition
    "Nusselt number for laminar flow at the transition point";
  Modelica.Units.SI.NusseltNumber Nu_Gnielinski_transition
    "Nusselt number for turbulent flow according to Gnielinski at the
    transition point";

  Modelica.Units.SI.NusseltNumber Nu_aux1
    "Nusselt number according to transition between no-flow and laminar flow 
    regime";
  Modelica.Units.SI.NusseltNumber Nu_aux2
    "Nusselt number according to transition between laminar flow regime and 
    transition regime";
  Modelica.Units.SI.NusseltNumber Nu_aux3
    "Nusselt number according to transition between transition regime and 
    turbulent regime according to Gnielinski";

  Modelica.Units.SI.Length l_heatTransfer
    "Length of the tube accounting for discretization";
  Modelica.Units.SI.Area A_heatTransfer
    "Heat transfer area accounting for discretization";

  Real zeta
    "Auxillary variable for Gnielinski's Nusselt correlation";
  Real zeta_transition
    "Auxillary variable for Gnielinski's Nusselt correlation at the transition 
    point";

  Real wf_noFlowLaminar
    "Transition factor to change between no-flow and laminar flow regime";
  Real wf_laminarTransition
    "Transition factor for change between laminar and transition regime";
  Real gamma
    "Transition fractor transition regime";
  Real wf_transitionGnielinski
    "Transition factor for change between first transition regime and turbulent
    regime according to Gnielinski";
  Real wf_GnielinskiDittusBoelter
    "Transition factor for change between turbulent regime according to Gnielinski
    and turbulent regime according to Dittus and Boelter";

equation
  //
  // Calculation of transition functions
  //
  gamma = (Re - Re_critLaminar) / (Re_critGnielinski - Re_critLaminar)
    "Transition fractor transition regime";

  if avoid_events then
    wf_noFlowLaminar = SorpLib.Numerics.smoothTransition_noEvent(
      x=Re,
      transitionPoint=Re_critNoFlow,
      transitionLength=Re_transitionNoFlow,
      noDiff=noDiff)
      "Transition factor to change between no-flow and laminar flow regime";
    wf_laminarTransition  = SorpLib.Numerics.smoothTransition_noEvent(
      x=Re,
      transitionPoint=Re_critLaminar,
      transitionLength=Re_transitionLaminar,
      noDiff=noDiff)
      "Transition factor for change between laminar and transition regime";
    wf_transitionGnielinski = SorpLib.Numerics.smoothTransition_noEvent(
      x=Re,
      transitionPoint=Re_critGnielinski,
      transitionLength=Re_transitionTurbulent,
      noDiff=noDiff)
      "Transition factor for change between first transition regime and turbulent
      regime according to Gnielinski";
    wf_GnielinskiDittusBoelter = SorpLib.Numerics.smoothTransition_noEvent(
      x=Re,
      transitionPoint=Re_critDittusBoelter,
      transitionLength=Re_transitionTurbulent,
      noDiff=noDiff)
      "Transition factor for change between turbulent regime according to Gnielinski
      and turbulent regime according to Dittus and Boelter";

  else
    wf_noFlowLaminar = SorpLib.Numerics.smoothTransition(
      x=Re,
      transitionPoint=Re_critNoFlow,
      transitionLength=Re_transitionNoFlow,
      noDiff=noDiff)
      "Transition factor to change between no-flow and laminar flow regime";
    wf_laminarTransition  = SorpLib.Numerics.smoothTransition(
      x=Re,
      transitionPoint=Re_critLaminar,
      transitionLength=Re_transitionLaminar,
      noDiff=noDiff)
      "Transition factor for change between laminar and transition regime";
    wf_transitionGnielinski = SorpLib.Numerics.smoothTransition(
      x=Re,
      transitionPoint=Re_critGnielinski,
      transitionLength=Re_transitionTurbulent,
      noDiff=noDiff)
      "Transition factor for change between first transition regime and turbulent
      regime according to Gnielinski";
    wf_GnielinskiDittusBoelter = SorpLib.Numerics.smoothTransition(
      x=Re,
      transitionPoint=Re_critDittusBoelter,
      transitionLength=Re_transitionTurbulent,
      noDiff=noDiff)
      "Transition factor for change between turbulent regime according to Gnielinski
      and turbulent regime according to Dittus and Boelter";

  end if;

  //
  // Calculation of the Reynolds and Prandtl number
  //
  if calculationMassFlowRate==
    SorpLib.Choices.MassFlowRateHeatTranferCorrelation.PortXMinus then
    Re = abs(m_hyd_xMinus) * geometry.d_hydInner /
      (geometry.A_hydCrossInner * fluidProperties.eta)
      "Reynolds number";

  elseif calculationMassFlowRate==
    SorpLib.Choices.MassFlowRateHeatTranferCorrelation.PortXPlus then
    Re = abs(m_hyd_xPlus) * geometry.d_hydInner /
      (geometry.A_hydCrossInner * fluidProperties.eta)
      "Reynolds number";

  else
    Re = (abs(m_hyd_xMinus) + abs(m_hyd_xPlus)) / 2 * geometry.d_hydInner /
      (geometry.A_hydCrossInner * fluidProperties.eta)
      "Reynolds number";

  end if;

  Pr = fluidProperties.eta * fluidProperties.cp / fluidProperties.lambda
    "Prandtl number";

  //
  // Calculation of the Nusselt numbers
  //
  l_heatTransfer = geometry.l /
    min(geometry.no_fluidVolumes, geometry.no_wallVolumes)
    "Length of the tube accounting for discretization";
  A_heatTransfer = geometry.A_heatTransferInner /
    min(geometry.no_fluidVolumes, geometry.no_wallVolumes)
    "Length of the tube accounting for discretization";

  zeta =
    (1.8 * Modelica.Math.log10(max(Re,1e-12)) - 1.5)^(-2)
    "Auxillary variable for Gnielinski's Nusselt correlation";
  zeta_transition =
    (1.8 * Modelica.Math.log10(max(Re_critGnielinski,1e-12)) - 1.5)^(-2)
    "Auxillary variable for Gnielinski's Nusselt correlation at the transition 
    point";

  Nu_noFlow = f_correctionNoFlow * 3.66
    "Nusselt number for laminar flow at the transition point";
  Nu_lamianr = f_correctionLaminar * (
    3.66^3 +
    0.7^3 +
    (1.615 * (Re * Pr * geometry.d_hydInner/l_heatTransfer)^(1/3) - 0.7)^3 +
    ((2 / (1 + 22 * Pr))^(1/6) * (Re * Pr * geometry.d_hydInner/l_heatTransfer)^(1/2))^3)^(1/3)
    "Nusselt number for laminar flow regime";
  Nu_lamianrFlow_transition = f_correctionLaminar * (
    3.66^3 +
    0.7^3 +
    (1.615 * (Re_critLaminar * Pr * geometry.d_hydInner/l_heatTransfer)^(1/3) - 0.7)^3 +
    ((2 / (1 + 22 * Pr))^(1/6) * (Re_critLaminar * Pr * geometry.d_hydInner/l_heatTransfer)^(1/2))^3)^(1/3)
    "Nusselt number for laminar flow at the transition point";
  Nu_transition = (1-gamma) * Nu_lamianrFlow_transition +
    gamma * Nu_Gnielinski_transition
    "Nusselt number for transition regime";
  Nu_Gnielinski_transition = f_correctionGnielinski * (zeta_transition/8) * Re_critGnielinski * Pr /
    (1 + 12.7*sqrt(zeta_transition/8) * (Pr^(2/3) - 1)) *
    (1 + (geometry.d_hydInner/l_heatTransfer)^(2/3))
    "Nusselt number for turbulent flow according to Gnielinski at the
    transition point";
  Nu_Gnielinski = f_correctionGnielinski * (zeta/8) * Re * Pr /
    (1 + 12.7*sqrt(zeta/8) * (Pr^(2/3) - 1)) *
    (1 + (geometry.d_hydInner/l_heatTransfer)^(2/3))
    "Nusselt number for turbulent flow regime according to Gnielinski";
  Nu_DittusBoelter = f_correctionDittusBoelter * 0.023 * Re^(4/5) * Pr^(1/3)
    "Nusselt number for turbulent flow regime according to Dittus and Boelter";

  Nu_aux1 = wf_noFlowLaminar * Nu_noFlow +
    (1-wf_noFlowLaminar) * Nu_lamianr
    "Nusselt number according to transition between no-flow and laminar flow 
    regime";
  Nu_aux2 = wf_laminarTransition * Nu_aux1 +
    (1-wf_laminarTransition) * Nu_transition
    "Nusselt number according to transition between laminar flow regime and 
    transition regime";
  Nu_aux3 = wf_transitionGnielinski * Nu_aux2 +
    (1-wf_transitionGnielinski) * Nu_Gnielinski
    "Nusselt number according to transition between transition regime and 
    turbulent regime according to Gnielinski";
  Nu = wf_GnielinskiDittusBoelter * Nu_aux3 +
    (1-wf_GnielinskiDittusBoelter) * Nu_DittusBoelter
    "Nusselt number";

  //
  // Calculation of the heat transfer coefficient
  //
  alpha = Nu * fluidProperties.lambda/geometry.d_hydInner
    "Heat transfer coefficient";
  alphaA = geometry.no_hydraulicParallelTubes * alpha * A_heatTransfer
    "Product of heat transfer coefficient and area";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This heat transfer model calculates the product of heat transfer coefficient and
area describing convective heat transfer within tubes. The model considers both,
the laminar and turbulent flow regime.
</p>

<h4>Main equations</h4>
<p>
The product of heat transfer coefficient and area <i>&alpha;A</i> is calculated
from the Nussel number <i>Nu</i> using Nusselt correlations for different flow
regimes:
</p>
<pre>
    &alpha;A = no<sub>parallel flows</sub> * Nu * &lambda; / d<sub>hyd,inner</sub> * A<sub>heat transfer</sub> / <strong>min</strong>(no<sub>fluid volumes</sub>, no<sub>wall volumes</sub>);
</pre>
<p>
Herein, <i>&lambda;</i> is the thermal conductivity, <i>d<sub>hyd,inner</sub></i> 
is the hydraulic inner diameter, and <i>A<sub>heat transfer</sub></i> is the heat 
transfer area accounting for the discretization. The product is enlarged by the
number of hydrualic parallel flows <i>no<sub>parallel flows</sub></i> to account 
for parallel flows modeled by just one flow. 
<br/><br/>
The Nusselt number depends on the flow regime:
</p>
<pre>
    Nu<sub>no flow</sub> = f<sub>correction,no flow</sub> * 3.66;

    Nu<sub>laminar</sub> = f<sub>correction,laminar</sub> * [3.66<sup>3</sup> + 0.7<sup>3</sup> + (1.615 * (Re * Pr * d<sub>hyd,inner</sub>/l<sub>heat transfer</sub>)<sup>(1/3)</sup> - 0.7)<sup>3</sup> + ((2 / (1 + 22 * Pr))<sup>(1/6)</sup> * (Re * Pr * d<sub>hyd,inner</sub>/l<sub>heat transfer</sub>)<sup>(1/2)</sup>)<sup>3</sup>]<sup>(1/3)</sup>;

    Nu<sub>transition</sub> = (1-&gamma;) * Nu<sub>laminar</sub>(Re<sub>crit,laminar</sub>) + &gamma; * Nu<sub>Gnielinski</sub>(Re<sub>crit,Gnielinski</sub>);

    Nu<sub>Gnielinski</sub> = f<sub>correction,Gnielinski</sub> * [(&zeta;/8) * Re * Pr / (1 + 12.7 * <strong>sqrt</strong>(&zeta;/8) * (Pr<sup>(2/3)</sup> - 1)) * (1 + (d<sub>hyd,inner</sub>/l<sub>heat transfer</sub>)<sup>(2/3)</sup>)];

    Nu<sub>Dittus,Boelter</sub> = f<sub>correction,Dittus,Boelter</sub> * [0.023 * Re<sup>(4/5)</sup> * Pr<sup>(1/3)</sup>];
</pre>
<p>
with:
</p>
<pre>
    &zeta; = 1 / [1.8 * <strong>log<sub>10</sub></strong>(Re) - 1.5]<sup>2</sup>;

    &gamma; = (Re - Re<sub>crit,laminar</sub>) / (Re<sub>crit,Gnielinski</sub> - Re<sub>crit,laminar</sub>);
</pre>
<p>
Herein, <i>f<sub>i</sub></i> are correction factors for the Nusselt correlations
<i>Nu<sub>i</sub></i> of the different flow regimes <i>i</i>, <i>Re</i> is the
Reynolds number, <i>Pr</i> is the Prandtl number, <i>&zeta;</i> is a auxillary
variable, <i>&gamma;</i> is a transition factor, and <i>l<sub>heat transfer</sub></i> 
is the heat transfer length accounting for the discretization:
</p>
<pre>
    Re = <strong>abs</strong>(m<sub>flow,hyd</sub>) * d<sub>hyd,inner</sub> / A<sub>cross,inner</sub> / &eta;;

    Pr = &eta; * c<sub>p</sub> / &lambda;;

    l<sub>heat transfer</sub> = l<sub>tube</sub> / <strong>min</strong>(no<sub>fluid volumes</sub>, no<sub>wall volumes</sub>);
</pre>
<p>
Herein, <i>m<sub>flow,hyd</sub></i> is the hydraulic mass flow rate, 
<i>A<sub>cross,inner</sub></i> is the hydraulic cross-sectional inner area, <i>&eta;</i> 
is the dynamic viscosity, <i>&lambda;</i> is the thermal conductivity, and 
<i>l<sub>tube</sub></i> is the tube length.
<br/><br/>
Smooth transition is applied using the function
<a href=\"Modelica://SorpLib.Numerics.smoothTransition\">SorpLib.Numerics.smoothTransition</a>
to change between the different regimes using the corresponding critical Reynold
numbers <i>Re<sub>i</sub></i>.
</p>

<h4>Typical use</h4>
<p>
This heat transfer correlation model is typically used to describe convective
heat transfer within tubes if accurate results are required.
</p>

<h4>Important parameters and options</h4>
<ul>
  <li>
  <i>calculationMassFlowRate</i>:
  Defines the hydraulic mass flow rate that is used for calculations.
  </li>
  <br/>
  <li>
  <i>avoid_events</i>:
  Defines if events shall be avoided via the noEvent()-operator.
  </li>
</ul>

<h4>References</h4>
<ul>
  <li>
  Gnielinski, V. (2010). G1 Heat Transfer in Pipe Flow. In: VDI Heat Atlas. VDI-Buch. Springer, Berlin, Heidelberg. DOI: https://doi.org/10.1007/978-3-540-77877-6_34.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  January 19, 2024, by Mirko Engelpracht:<br/>
  Major extensions (more flow regimes, documentation, stability).
  </li>
  <li>
  January 14, 2021, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end GnielinskiDittusBoelter;
