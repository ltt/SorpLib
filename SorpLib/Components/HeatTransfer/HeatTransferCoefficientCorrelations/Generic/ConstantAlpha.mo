within SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.Generic;
model ConstantAlpha
  "Generic heat transfer correlation with constant heat transfer coefficient"
  extends
    SorpLib.Components.HeatTransfer.BaseClasses.PartialGenericHeatTransferCoefficient(
    final avoid_events=false);

  //
  // Definition of parameters
  //
  parameter Modelica.Units.SI.CoefficientOfHeatTransfer constantAlpha = 25
    "Constant heat transfer coefficient"
    annotation(Dialog(tab = "General", group = "Heat Transfer"));
  parameter Modelica.Units.SI.Area A = 1
    "Constant heat transfer area"
    annotation(Dialog(tab = "General", group = "Heat Transfer"));

equation
  alphaA = constantAlpha*A
    "Calculation of the product of heat transfer coefficient and area";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This simple transfer model calculates the product of heat transfer coefficient and
area assuming a constant heat transfer coefficient and area.
</p>

<h4>Main equations</h4>
<p>
The product of heat transfer coefficient and area <i>&alpha;A</i> is calculated 
assuming a constant heat transfer coefficient <i>&alpha;</i> and area <i>A</i>:
</p>
<pre>
    &alpha;A = &alpha; * A = const.;
</pre>
  
<h4>Typical use</h4>
<p>
This simple heat transfer correlation model is typically used if the heat transfer
conditions do not greatly change and a very simple models is required.
</p>
</html>", revisions="<html>
<ul>
  <li>
  January 12, 2024, by Mirko Engelpracht:<br/>
  Completed documentation.
  </li>
  <li>
  January 13, 2021, by Mirko Engelpracht:<br/>
  Smaller revision after resctructering of the library.
  </li>
  <li>
  December 06, 2017, by Andrej Gibelhaus:<br/>
  Tidy up implementation and enhance documentation for publication of library.
  </li>
</ul>
</html>"));
end ConstantAlpha;
