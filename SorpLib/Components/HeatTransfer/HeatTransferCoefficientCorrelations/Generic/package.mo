within SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations;
package Generic "Generic correlations for heat transfer coefficients"
extends Modelica.Icons.FunctionsPackage;

  annotation (Documentation(info="<html>
<p>
This package contains generic correlations for heat transfer coefficients:
</p>
<ul>
  <li>
  <a href=\"Modelica://SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.Generic.ConstantAlpha\">ConstantAlpha</a>: 
  Generic heat transfer correlation with constant heat transfer coefficient.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.Generic.ConstantAlphaA\">ConstantAlphaA</a>: 
  Generic heat transfer correlation with constant product of heat transfer 
  coefficient and area.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.Generic.LinearAlphaA\">LinearAlphaA</a>: 
  Generic heat transfer correlation with a product of heat transfer coefficient 
  and area that is linearly dependent on the temperature.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.Generic.ExponentialAlphaA\">ExponentialAlphaA</a>: 
  Generic heat transfer correlation with a product of heat transfer coefficient 
  and area that is exponentially dependent on the temperature.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Components.HeatTransfer.HeatTransferCoefficientCorrelations.Generic.PolynomialAlphaA\">PolynomialAlphaA</a>: 
  Generic heat transfer correlation with a product of heat transfer coefficient 
  and area that is polynomially dependent on the temperature.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructering the library.
  </li>
</ul>
</html>"));
end Generic;
