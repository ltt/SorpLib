within SorpLib.Components.HeatTransfer.BaseClasses;
partial model PartialTubeInsideHeatTransferCoefficient
  "Base model for all models calculating the product of heat transfer coefficient and area for the tube-inside heat transfer"
  extends
    SorpLib.Components.HeatTransfer.BaseClasses.PartialHeatTransferCoefficient;

  //
  // Definitions of inputs
  //
  input SorpLib.Components.HeatTransfer.Records.FluidProperties fluidProperties
    "Fluid properties (i.e., fluid properties of the fluid volume)"
    annotation (Dialog(tab="General", group="Inputs", enable=false));
  input Modelica.Units.SI.MassFlowRate m_hyd_xMinus
    "Hydraulic mass flow rate at design inlet"
    annotation (Dialog(tab="General", group="Inputs", enable=false));
  input Modelica.Units.SI.MassFlowRate m_hyd_xPlus
    "Hydraulic mass flow rate at design outlet"
    annotation (Dialog(tab="General", group="Inputs", enable=false));

  //
  // Definition of parameters
  //
  replaceable parameter SorpLib.Components.HeatTransfer.Records.GeometryTube geometry
    constrainedby SorpLib.Components.HeatTransfer.Records.GeometryTube
    "Geometry of the (heat exchanger) tubes"
    annotation(Dialog(tab = "General", group = "Heat Transfer", enable=false));

  //
  // Annotations
  //
  annotation (Documentation(revisions="<html>
<ul>
  <li>
  January 19, 2024, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>", info="<html>
<p>
This partial model is the base model for all models calculating the product of
heat transfer coefficient and area <i>alphaA</i> describing convective heat transfer
inside tubes. It defines fundamental parameters and variables required by all heat 
transfer coefficient models. Models that inherit  properties from this partial model 
have to add an equation for calculating the product of heat transfer coefficient 
and area.
<br/><br/>
The following variables must be specified in the model that inherit properties:
</p>
<ul>
  <li>
  Product of heat transfer coefficient and area <i>alphaA</i>.
  </li>
</ul>
</html>"));
end PartialTubeInsideHeatTransferCoefficient;
