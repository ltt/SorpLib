within SorpLib.Components.HeatTransfer;
package BaseClasses "Base models and functions for all heat transfers"
  extends Modelica.Icons.BasesPackage;

  annotation (Documentation(info="<html>
<p>
This package contains partial heat transfer and heat transfer coefficient models, 
containing fundamental definitions of parameters and variables. The content of 
this package is only of interest when adding new heat transfer and heat transfer
coefficient models to the library. 
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end BaseClasses;
