within SorpLib.Components.HeatTransfer.BaseClasses;
partial model PartialPoolCondensationHeatTransferCoefficient
  "Base model for all models calculating the product of heat transfer coefficient and area for (pool) condensation"
  extends
    SorpLib.Components.HeatTransfer.BaseClasses.PartialHeatTransferCoefficient;

  //
  // Definitions of inputs
  //
  input SorpLib.Components.HeatTransfer.Records.FluidProperties fluidProperties
    "Fluid properties (i.e., fluid properties of the phase saperator fluid)"
    annotation (Dialog(tab="General", group="Inputs", enable=false));
  input Real f_relativeFillingLevel(min=0, max=1, final unit="1")
    "Relative filling level of the condenser"
    annotation (Dialog(tab="General", group="Inputs", enable=false));

  //
  // Definition of parameters
  //
  replaceable parameter SorpLib.Components.HeatTransfer.Records.GeometryTube geometry
    constrainedby SorpLib.Components.HeatTransfer.Records.GeometryTube
    "Geometry of the heat exchanger tubes providing the heat flow rate for (pool)
    condensation"
    annotation(Dialog(tab = "General", group = "Heat Transfer", enable=false));

  //
  // Annotations
  //
  annotation (Documentation(revisions="<html>
<ul>
  <li>
  January 18, 2024, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>", info="<html>
<p>
This partial model is the base model for all models calculating the product of
heat transfer coefficient and area <i>alphaA</i> describing (pool) condensation,
such as film condensation. It defines fundamental parameters and variables required 
by all heat transfer coefficient models. Models that inherit  properties from this 
partial model have to add an equation for calculating the product of heat transfer 
coefficient and area.
<br/><br/>
The following variables must be specified in the model that inherit properties:
</p>
<ul>
  <li>
  Product of heat transfer coefficient and area <i>alphaA</i>.
  </li>
</ul>
</html>"));
end PartialPoolCondensationHeatTransferCoefficient;
