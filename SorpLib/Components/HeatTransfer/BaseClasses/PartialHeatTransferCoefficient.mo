within SorpLib.Components.HeatTransfer.BaseClasses;
partial model PartialHeatTransferCoefficient
  "Base model for all models calculating the product of heat transfer coefficient and area"

  //
  // Definition of parameters regarding the heat transfer
  //
  parameter Boolean computeTransportProperties = false
    "= true, if fluid transport properties are required and must be calculated"
    annotation (Dialog(tab = "General", group = "Heat Transfer"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult = true);

  //
  // Definition of advanced parameters
  //
  parameter Boolean avoid_events = false
    "= true, if events are avoid by using noEvent()-operator"
    annotation (Dialog(tab = "Advanced", group = "Numerics"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult = true);

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.Temperature T_avg_port_a
    "Average temperature at port a"
    annotation (Dialog(tab="General",group="Inputs", enable=false),
                Evaluate=true);
  input Modelica.Units.SI.Temperature T_avg_port_b
    "Average temperature at port b"
    annotation (Dialog(tab="General",group="Inputs", enable=false),
                Evaluate=true);

  //
  // Definition of outputs
  //
  Modelica.Blocks.Interfaces.RealOutput alphaA(final unit="W/K")
    "Value for alphaA for entire heat transfer resistance";

  //
  // Annotations
  //
  annotation (Icon(graphics={Ellipse(
          extent={{100,100},{-100,-100}},
          lineColor={0,0,0},
          fillColor={238,46,47},
          fillPattern=FillPattern.Solid), Text(
          extent={{-80,80},{80,-80}},
          lineColor={0,0,0},
          fillColor={238,46,47},
          fillPattern=FillPattern.Solid,
          textString="kA")}), Documentation(revisions="<html>
<ul>
  <li>
  January 12, 2024, by Mirko Engelpracht:<br/>
  Smaller revision after resctructering of the library and documentation.
  </li>
  <li>
  January 13, 2021, by Mirko Engelpracht:<br/>
  Smaller revision after resctructering of the library.
  </li>
  <li>
  December 06, 2017, by Andrej Gibelhaus:<br/>
  Tidy up implementation and enhance documentation for publication of library.
  </li>
</ul>
</html>", info="<html>
<p>
This partial model is the base model for all models calculating the product of heat
transfer coefficient and area <i>alphaA</i>. It defines fundamental parameters and 
variables required by all heat transfer coefficient models. Models that inherit 
properties from this partial model have to add an equation for calculating the 
product of heat transfer coefficient and area. In this context, records may be added
that containg geometry and fluid property data.
<br/><br/>
The following variables must be specified in the model that inherit properties:
</p>
<ul>
  <li>
  Product of heat transfer coefficient and area <i>alphaA</i>.
  </li>
</ul>
</html>"));
end PartialHeatTransferCoefficient;
