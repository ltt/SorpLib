within SorpLib.Components.HeatTransfer.BaseClasses;
partial model PartialGenericHeatTransferCoefficient
  "Base model for all feneric models calculating the product of heat transfer coefficient and area"
  extends SorpLib.Components.HeatTransfer.BaseClasses.PartialHeatTransferCoefficient(
    final computeTransportProperties=false);

  //
  // Annotations
  //
  annotation (Documentation(revisions="<html>
<ul>
  <li>
  January 12, 2024, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>", info="<html>
<p>
This partial model is the base model for all generic models calculating the product 
of heat transfer coefficient and area <i>alphaA</i>. It defines fundamental parameters  
and variables required by all heat transfer coefficient models. Models that inherit 
properties from this partial model have to add an equation for calculating the 
product of heat transfer coefficient and area.
<br/><br/>
The following variables must be specified in the model that inherit properties:
</p>
<ul>
  <li>
  Product of heat transfer coefficient and area <i>alphaA</i>.
  </li>
</ul>
</html>"));
end PartialGenericHeatTransferCoefficient;
