within SorpLib.Components.HeatTransfer.BaseClasses;
partial model PartialClosedAdsorberHeatTransferCoefficient
  "Base model for all models calculating the product of heat transfer coefficient and area for closed adsorbers"
  extends
    SorpLib.Components.HeatTransfer.BaseClasses.PartialHeatTransferCoefficient;

  //
  // Definitions of inputs
  //
  input SorpLib.Components.HeatTransfer.Records.FluidProperties fluidProperties
    "Fluid properties (i.e., fluid properties of the sorbent)"
    annotation (Dialog(tab="General", group="Inputs", enable=false));

  //
  // Definition of parameters
  //
  replaceable parameter SorpLib.Components.HeatTransfer.Records.GeometryClosedAdsorber geometry
    constrainedby
    SorpLib.Components.HeatTransfer.Records.GeometryClosedAdsorber
    "Geometry of the closed adsorber"
    annotation(Dialog(tab = "General", group = "Heat Transfer", enable=false));

  //
  // Annotations
  //
  annotation (Documentation(revisions="<html>
<ul>
  <li>
  January 22, 2024, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>", info="<html>
<p>
This partial model is the base model for all models calculating the product of
heat transfer coefficient and area <i>alphaA</i> describing the heat transfer
between the sorbent and heat exchanger tubes. It defines fundamental parameters 
and variables required by all heat transfer coefficient models. Models that inherit 
properties from this partial model have to add an equation for calculating the 
product of heat transfer coefficient and area.
<br/><br/>
The following variables must be specified in the model that inherit properties:
</p>
<ul>
  <li>
  Product of heat transfer coefficient and area <i>alphaA</i>.
  </li>
</ul>
</html>"));
end PartialClosedAdsorberHeatTransferCoefficient;
