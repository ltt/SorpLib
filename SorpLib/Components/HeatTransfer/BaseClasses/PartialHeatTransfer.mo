within SorpLib.Components.HeatTransfer.BaseClasses;
partial model PartialHeatTransfer
  "Base model for all heat transfer models"

  //
  // Definition of parameters regarding the calculation setup
  //
  parameter Integer n_a(min=1) = 1
    "Number of heat ports a"
    annotation (Dialog(tab="General", group="Calculation Setup"),
                Evaluate=true,
                HideResult=true);
  parameter Integer n_b(min=1) = 1
    "Number of heat ports b"
    annotation (Dialog(tab="General", group="Calculation Setup"),
                Evaluate=true,
                HideResult=true);

  parameter Integer exponetTemperature(min=1) = 1
    "Exponent of the temperature when calculating the heat flow rate (i.e., 1 for
    convection and conduction; 4 for radiation"
    annotation (Dialog(tab="General", group="Calculation Setup", enable=false),
                Evaluate=true,
                HideResult=true);

  //
  // Definition of parameters regarding the heat transfer coefficient
  //
  parameter Boolean useAlphaAInput = false
    " = true, if alphaA is given by input; otherwise, alphaA is calculated by
    selected model"
    annotation (Dialog(tab="General", group="Heat Transfer Coefficient"));
  parameter Boolean calculateFluidProperties = false
    "= true, to calculate fluid properties including transport properties"
    annotation (Dialog(tab="General", group="Heat Transfer Coefficient",
                enable=not useAlphaAInput),
                choicesAllMatching=true,
                HideResult=true,
                Evaluate=true);
  replaceable model HeatTransferCoefficient =
    SorpLib.Components.HeatTransfer.BaseClasses.PartialHeatTransferCoefficient
    constrainedby
    SorpLib.Components.HeatTransfer.BaseClasses.PartialHeatTransferCoefficient(
     T_avg_port_a = sum(hp_a.T)/n_a,
     T_avg_port_b = sum(hp_b.T)/n_b)
    "Model calculating the product of the heat transfer coefficient and area"
    annotation (Dialog(tab="General", group="Heat Transfer Coefficient",
                enable=not useAlphaAInput),
                choicesAllMatching=true,
                HideResult=true,
                Evaluate=true);

  //
  // Definition of connectors
  //
  Modelica.Blocks.Interfaces.RealInput alphaA_input(final unit="W/K") if
    useAlphaAInput
    "Input for the product of the heat transfer coefficient and area"
    annotation (Placement(transformation(extent={{-20,-20},{20,20}},
        rotation=270,
        origin={0,50}),
      iconTransformation(extent={{-10,-10},{10,10}},
        rotation=270,
        origin={0,40})));

  //
  // Definition of protected connectors
  //
protected
  Modelica.Blocks.Interfaces.RealInput alphaA_internal(final unit="W/K")
    "Needed for connecting to conditional connector";

  //
  // Definition of ports
  //
public
  SorpLib.Basics.Interfaces.HeatPorts.HeatPort_in[n_a] hp_a
    "Heat port a"
    annotation (Placement(transformation(extent={{-90,-10},{-70,10}}),
                iconTransformation(extent={{-90,-10},{-70,10}})));
  SorpLib.Basics.Interfaces.HeatPorts.HeatPort_out[n_b] hp_b
    "Heat port b"
    annotation (Placement(transformation(extent={{70,-10},{90,10}}),
                iconTransformation(extent={{70,-10},{90,10}})));

  //
  // Definition of models
  //
  HeatTransferCoefficient heatTransferCoefficient if not useAlphaAInput
    "Model calculating the product of the heat transfer coefficient and area";

  //
  // Definition of variables
  //
  Modelica.Units.SI.TemperatureDifference DT_avg=
    (sum(hp_a.T)/n_a)^exponetTemperature - (sum(hp_b.T)/n_b)^exponetTemperature
    "Average driving potential (i.e., temperature difference) between ports a 
    and b";
  Modelica.Units.SI.HeatFlowRate Q_flow=
    sum(hp_a.Q_flow)
    "Total heat flow rate transferred from ports a to b";

equation
  //
  // Assertions
  //
  if n_a < n_b then
    assert(rem(n_b, n_a) == 0,
      "Number of heat ports a must be a factor of the number of heat ports b!");

  elseif n_a > n_b then
    assert(rem(n_a, n_b) == 0,
      "Number of heat ports a must be a factor of the number of heat ports b!");

  end if;

  //
  // Connectors
  //
  connect(alphaA_internal, alphaA_input);
  connect(alphaA_internal, heatTransferCoefficient.alphaA);

  //
  // Calculation of heat port properties depending on the discretization
  //
  0 = sum(hp_a.Q_flow) + sum(hp_b.Q_flow)
    "Overall energy balance";

  if n_a < n_b then
    //
    // Number of heat ports a is less than number of heat ports b:
    // Connect multiple heat ports b with one heat port a!
    //
    for i in 1:n_a-1 loop
      0 = hp_a[i].Q_flow +
        sum(hp_b[1 + (i-1) * integer(n_b/n_a):i * integer(n_b/n_a)].Q_flow)
        "N-1 energy balances";
    end for;

    for i in 1:n_a loop
      for j in 1:integer(n_b/n_a) loop
        hp_b[j + (i-1) * integer(n_b/n_a)].Q_flow = alphaA_internal/n_b * (
          hp_b[j + (i-1) * integer(n_b/n_a)].T^exponetTemperature -
          hp_a[i].T^exponetTemperature)
          "Heat flow calculation depending on discretization";
      end for;
    end for;

  elseif n_a > n_b then
    //
    // Number of heat ports a is greater than number of heat ports b:
    // Connect multiple heat ports a with one heat port b!
    //
    for i in 1:n_b-1 loop
      0 = hp_b[i].Q_flow +
        sum(hp_a[1 + (i-1) * integer(n_a/n_b):i * integer(n_a/n_b)].Q_flow)
        "N-1 energy balances";
    end for;

    for i in 1:n_b loop
      for j in 1:integer(n_a/n_b) loop
        hp_a[j + (i-1) * integer(n_a/n_b)].Q_flow = alphaA_internal/n_a * (
          hp_a[j + (i-1) * integer(n_a/n_b)].T^exponetTemperature -
          hp_b[i].T^exponetTemperature)
          "Heat flow calculation depending on discretization";
      end for;
    end for;

  else
    //
    // Number of heat ports a equals number of heat ports b:
    // Connect one heat port a with one heat port b!
    //
    for i in 1:n_a-1 loop
      0 = hp_a[i].Q_flow + hp_b[i].Q_flow
        "N-1 energy balances";
    end for;

    for i in 1:n_a loop
      hp_b[i].Q_flow = alphaA_internal/n_a * (hp_b[i].T^exponetTemperature -
        hp_a[i].T^exponetTemperature)
        "Heat flow calculation depending on discretisation";
    end for;

  end if;

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial model is the base model for all heat transfers. It defines 
fundamental parameters and variables required by all heat transfers. Models 
that inherit properties from this partial model have to redeclare and constrain 
the model calculating the product of heat transfer coefficient and area. In 
this context, records may be added that containg geometry and fluid property 
data. Furtheremore, it must be specified if the model calcualtes convective/conductive
heat transfer or radiation heat transfer.
<br/><br/>
The following variables must be specified in the model that inherit properties:
</p>
<ul>
  <li>
  Exponent of the temperatures defining the driving temperatures <i>exponetTemperature</i>.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  January 12, 2024, by Mirko Engelpracht:<br/>
  Separation into a partial model and documentation.
  </li>
  <li>
  January 13, 2021, by Mirko Engelpracht:<br/>
  Major revision allowing different discretizations for both ports.
  </li>
  <li>
  December 06, 2017, by Andrej Gibelhaus:<br/>
  Tidy up implementation and enhance documentation for publication of library.
  </li>
</ul>
</html>"), Icon(graphics={
        Line(
          points={{60,-50},{-60,-50}},
          color={0,0,0},
          arrow={Arrow.Filled,Arrow.Filled}), Rectangle(
          extent={{-80,-40},{80,40}},
          lineColor={238,46,47},
          fillPattern=FillPattern.HorizontalCylinder,
          fillColor={238,46,47})}));
end PartialHeatTransfer;
