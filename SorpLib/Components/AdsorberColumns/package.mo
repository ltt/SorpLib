within SorpLib.Components;
package AdsorberColumns "Adsorbers columns used in open sorption systems"
  extends SorpLib.Icons.AdsorberColumnsPackage;

  annotation (Documentation(info="<html>
<p>
TO BE ADDED!
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end AdsorberColumns;
