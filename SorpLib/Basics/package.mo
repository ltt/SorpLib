within SorpLib;
package Basics "Library containing basic modeles used to aggregate advanced models"
  extends Modelica.Icons.VariantsPackage;

  annotation (Documentation(info="<html>
<p>
This package contains basic models required to aggregate advanced models:
</p>
<ul>
  <li>
  <a href=\"Modelica://SorpLib.Basics.Interfaces\">Interfaces</a>: 
  Thermal and fluid connectors that serve as interfaces between models.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Basics.Sources\">Sources</a>: 
  Thermal and fluid sources that define potential, flow, and stream variables
  at connectors.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Basics.Volumes\">Volumes</a>: 
  Finite volume models with (transient) mass and energy balances. These finite
  volume models cover adsorbates, fluids, solids, and phase separators and can
  be used to create spatial distributed models.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end Basics;
