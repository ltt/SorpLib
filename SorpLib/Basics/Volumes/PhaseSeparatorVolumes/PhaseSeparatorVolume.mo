within SorpLib.Basics.Volumes.PhaseSeparatorVolumes;
model PhaseSeparatorVolume
  "Homogenous phase separator of a real fluid (i.e., with a two-phase regime)"
  extends SorpLib.Basics.Volumes.BaseClasses.PartialPhaseSeparatorVolume(
    final no_components = Medium.nX,
    useHeatPorts=true,
    useHeatPortsY=true,
    type_overallMassBalance=
      SorpLib.Choices.BalanceEquations.TransientFixedInitial,
    type_energyBalance=
      SorpLib.Choices.BalanceEquations.TransientFixedInitial);

  //
  // Definition of parameters regarding the medium
  //
  replaceable package Medium = Modelica.Media.Water.StandardWater
    constrainedby Modelica.Media.Interfaces.PartialTwoPhaseMedium
    "Medium model of the real fluid (i.e., with a two-phase regime)"
    annotation (Dialog(tab="General", group="Media"),
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

  //
  // Definition of protected variables
  //
protected
  Medium.ThermodynamicState state
    "Thermodynamic state required to calculate medium properties";
  Medium.SaturationProperties stateSaturation
    "Thermodynamic saturation state required to calculate medium properties";

equation
  //
  // Calculation of properties
  //
  state = Medium.setState_dTX(d=rho, T=T, X=Medium.reference_X)
    "Thermodynamic state required to calculate medium properties";
  stateSaturation = Medium.setSat_T(T=T)
    "Thermodynamic saturation state required to calculate medium properties";

  p = Medium.saturationPressure_sat(sat=stateSaturation)
    "Pressure";

  rho_liq = Medium.bubbleDensity(sat=stateSaturation)
    "Density at saturated liquid line";
  rho_vap = Medium.dewDensity(sat=stateSaturation)
    "Density at saturated vapor line";
  h_liq = Medium.bubbleEnthalpy(sat=stateSaturation)
    "Specific enthalpy at saturated liquid line";
  h_vap = Medium.dewEnthalpy(sat=stateSaturation)
    "Specific enthalpy at saturated vapor line";

  //
  // Mass balance
  //
  dm_dtau = mc_flow
    "Overall mass balance";

  mc_flow = sum(cfp_xMinus.m_flow) + sum(cfp_xPlus.m_flow)
    "Sum of all convective mass flow rates across boundaries";

  //
  // Energy balance
  //
  dU_dtau = Hb_flow + Qb_flow
    "Energy balane";

  if avoid_events then
    Hb_flow =
      sum(cfp_xMinus.m_flow .* noEvent(actualStream(cfp_xMinus.h_outflow))) +
      sum(cfp_xPlus.m_flow .* noEvent(actualStream(cfp_xPlus.h_outflow)))
      "Sum of all enthalpy flow rates across boundaries";

  else
    Hb_flow =
      sum(cfp_xMinus.m_flow .* actualStream(cfp_xMinus.h_outflow)) +
      sum(cfp_xPlus.m_flow .* actualStream(cfp_xPlus.h_outflow))
      "Sum of all enthalpy flow rates across boundaries";

  end if;

  Qb_flow = Q_flow_xMinus + Q_flow_xPlus +
    Q_flow_yMinus + Q_flow_yPlus +
    Q_flow_zMinus + Q_flow_zPlus
    "Sum of all heat flow rates across boundaries";

  //
  // Summary record
  //
  phaseSepratorProperties.cp = if calculateAdditionalProperties then
    Medium.specificHeatCapacityCp(state=state) else 0
    "Specific heat capacity";
  phaseSepratorProperties.cv =
    Medium.specificHeatCapacityCv(state=state)
    "Specific heat capacity";
  phaseSepratorProperties.lambda = if calculateAdditionalProperties then
    Medium.thermalConductivity(state=state) else 0
    "Thermal conductivity";
  phaseSepratorProperties.eta = if calculateAdditionalProperties then
    Medium.dynamicViscosity(state=state) else 0
    "Dynamic viscosity";

  phaseSepratorProperties.beta = if calculateAdditionalProperties then
    Medium.isobaricExpansionCoefficient(state=state) else 0
    "Isobaric expnasion coefficient";
  phaseSepratorProperties.kappa = if calculateAdditionalProperties then
    Medium.isothermalCompressibility(state=state) else 0
    "Isothermal compressibility";
  phaseSepratorProperties.my = if calculateAdditionalProperties then
    v / phaseSepratorProperties.cp * (phaseSepratorProperties.beta * T - 1) else 0
    "Joule-Thomson coefficient";

  phaseSepratorProperties.mc_flow_yMinus = 0
    "Convective mass flow rate at port '-dy/2'";
  phaseSepratorProperties.mc_flow_yPlus = 0
    "Convective mass flow rate at port '+dy/2'";
  phaseSepratorProperties.mc_flow_zMinus = 0
    "Convective mass flow rate at port '-dz/2'";
  phaseSepratorProperties.mc_flow_zPlus = 0
    "Convective mass flow rate at port '+dz/2'";

  phaseSepratorProperties.md_flow_xMinus = zeros(no_components)
    "Diffusive mass flow rate at port '-dx/2'";
  phaseSepratorProperties.md_flow_xPlus = zeros(no_components)
    "Diffusive mass flow rate at port '+dx/2'";
  phaseSepratorProperties.md_flow_yMinus = zeros(no_components)
    "Diffusive mass flow rate at port '-dy/2'";
  phaseSepratorProperties.md_flow_yPlus = zeros(no_components)
    "Diffusive mass flow rate at port '+dy/2'";
  phaseSepratorProperties.md_flow_zMinus = zeros(no_components)
    "Diffusive mass flow rate at port '-dz/2'";
  phaseSepratorProperties.md_flow_zPlus = zeros(no_components)
    "Diffusive mass flow rate at port '+dz/2'";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This model represents a phase separator volume, applying a lumped modeling approach. 
Depending on the volume setup, this model may have up to six heat ports (i.e., two 
for each spatial direction of a cartesian coordinate system). Furthermore, this model 
has convective fluid ports in y-direction, following the 'connectorSizing' principle.
The fluid port at position 'cfp_xMinus' is used as liquid fluid port, meaning that the
outflowing fluid is set at saturated liquid state. In contrast, the fluid port at 
position 'cfp_XPlus' is used as vapor fluid port, meaning that the outflowing fluid is 
set to saturated vapor state.
</p>

<h4>Main equations</h4>
<p>
The most important equations are the momentum, mass, and energy balance. According to
the staggered grid approach, the momentum balance is not solved within the volume but 
at the volume's boundaries via so-called 
<a href=\"Modelica://SorpLib.Components.Fittings\">flow models</a>. Hennce, no pressure 
losses occur within the volume:
</p>
<pre>
    p = cfp_xMinus.p;
</pre>
<pre>
    p = cfp_xPlus.p;
</pre>

<p>
Regarding the mass and energy balances, either steady-state or tansient balnaces
can be selected. The mass balance is defined as
</p>
<pre>
    (dm/d&tau;) = V * (d&rho;/d&tau;) = &sum; m<sub>c,flow</sub>;
</pre>
<p>
and the energy balance is defined as
</p>
<pre>
    (dU/d&tau;) = u * (dm/d&tau;) + m * (du/d&tau;) = u * (dm/d&tau;) + m * [c<sub>v</sub> * (dT/d&tau;) + 1/&rho;<sup>2</sup> * (-p + (h<sub>vap</sub> - h<sub>liq</sub>) / (1/&rho;<sub>vap</sub> - 1/&rho;<sub>liq</sub>)) (d&rho;/d&tau;)] = &sum; H<sub>b,flow</sub> + &sum; Q<sub>b,flow</sub>;
</pre>

<p>
Herein, <i>(dm/d&tau;)</i> is the derivative of the mass w.r.t. time, <i>(dU/d&tau;)</i> 
is the derivative of the internal energy w.r.t. time, <i>(du/d&tau;)</i> is the derivative 
of the specific internal energy w.r.t.time, <i>(dT/d&tau;)</i> is the derivative of the 
temperature w.r.t. time, <i>(d&rho;/d&tau;)</i> is the derivative of the density w.r.t. 
time, <i>V</i> is the volume, <i>m</i> is the mass, <i>p</i> ist the pressure, <i>&rho;</i> 
is the density, <i>&rho;<sub>liq</sub></i> is the density at the saturated liquid line, 
<i>&rho;<sub>vap</sub></i> is the density at the saturated vapor line, <i>h<sub>liq</sub></i> 
is the specific enthalpy at the saturated liquid line, <i>h<sub>vap</sub></i> is the specific
enthalpy at the saturated vapor line, <i>u</i> is the specific internal energy, <i>c<sub>v</sub></i> 
is the specific heat capacity at constant volume,<i>m<sub>c,flow</sub></i> is the sum of the 
convective mass flow rates, <i>H<sub>b,flow</sub></i> is the sum of the enthalpy flow rates, 
and <i>Q<sub>b,flow</sub></i> is the sum of the heat flow rates.
</p>

<h4>Assumptions and limitations</h4>
<ul>
  <li>
  Constant volume <i>V</i>
  </li>
  <li>
  Homogenoues properties within the volume
  </li>
  <li>
  Mechanical, thermal, and chemical equilibrium between the liquid and vapor phase
  </li>
</ul>

<h4>Typical use</h4>
<p>
This model is typically used as ideal phase saperator to model an evaporator od condenser.
</p>

<h4>Important parameters and options</h4>
<ul>
  <li>
  <i>useHeatPorts</i>:
  Defines if heat ports in the spatial direction <i>i</i> are required.
  </li>
  <li>
  <i>calculateAdditionalProperties</i>:
  Defines if additional properties like transport properties shall be calculated.
  </li>
  <br/>
  <li>
  <i>type_overallMassBalance</i>:
  Defines the type of the overall mass balance.
  </li>
  <li>
  <i>type_energyBalance</i>:
  Defines the type of the energy balance.
  </li>
  <br/>
  <li>
  <i>useStateLimiter</i>:
  Defines if specific enthalpy of vapor or liquid is limited at ports (i.e., vapor port is 
  slightly overheated and liquid port is slightly supercooled) to increase numerical stability.
  </li>
  <li>
  <i>avoid_events</i>:
  Defines if events shall be avoided via the noEvent()-operator.
  </li>
</ul>
<p>
Note that not all combinations of govering equation types are reasonable. Typically,
a transient mass balance is combined with a transient energy balance.
</p>

<h4>Dynamics</h4>
<p>
This model has two dynamic states:
</p>
<ul>
  <li>
  Temperature <i>T</i> and density <i>&rho;</i>.
  </li>
</ul>

<h4>References</h4>
<ul>
  <li>
  Tummescheit, H (2002). Design and Implementation of Object-Oriented Model Libraries using Modelica. PhD Thesis. URL: https://lucris.lub.lu.se/ws/files/4779081/8571901.pdf.
  </li>
  <li>
  Gr&auml;ber, M. and Kirches, C. and Bock, H.G. and Schl&ouml;der, J.P. and Tegethoff, W. and K&ouml;hler, J. (2011). Determining the optimum cyclic operation of adsorption chillers by a direct method for periodic optimal control. International Journal of Refrigeration, 34(4):902-913. DOI: https://doi.org/10.1016/j.ijrefrig.2010.12.021.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  December 14, 2023, by Mirko Engelpracht:<br/>
  Major revisions due to restructering finite volumes.
  </li>
  <li>
  January 13, 2021, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end PhaseSeparatorVolume;
