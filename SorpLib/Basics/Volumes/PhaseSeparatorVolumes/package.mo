within SorpLib.Basics.Volumes;
package PhaseSeparatorVolumes "Package containing finte volume models of phase separators"
  extends Modelica.Icons.VariantsPackage;

  annotation (Documentation(info="<html>
<p>
This package contains finite volume models of phase separators. This package 
calculates fluid properties based on the open-source Modelica Standard Library 
(MSL).
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end PhaseSeparatorVolumes;
