within SorpLib.Basics.Volumes.PhaseSeparatorVolumes.Tester;
model Test_PhaseSeparator "Tester for phase separator volume"
  extends Modelica.Icons.Example;

  //
  // Definition of parameters
  //
  replaceable package Medium = Modelica.Media.Water.StandardWater
    constrainedby Modelica.Media.Interfaces.PartialTwoPhaseMedium
    "Medium model of the VLE liquid"
    annotation (Dialog(tab="General", group="Media"),
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

  //
  // Definition of VLE models
  //
  SorpLib.Basics.Volumes.PhaseSeparatorVolumes.PhaseSeparatorVolume phaseSeparator(
    T_initial=293.15,
    rho_initial=500,
    type_energyBalance=SorpLib.Choices.BalanceEquations.TransientFixedInitial,
    calculateAdditionalProperties=true,
    type_overallMassBalance=SorpLib.Choices.BalanceEquations.TransientFixedInitial,
    redeclare final package Medium = Medium,
    nPorts_cfp_xMinus=1,
    nPorts_cfp_xPlus=1) "Model of a phase seperator volume" annotation (
      Placement(transformation(
        extent={{-20,-20},{20,20}},
        rotation=90,
        origin={0,0})));

  //
  // Definition of fluid boundaries
  //
protected
  Modelica.Blocks.Sources.Trapezoid input_liquidPort(
    amplitude=-0.0001,
    rising=250,
    width=400,
    falling=250,
    period=950,
    startTime=50)
    "Input for liquid port boundary"
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={-30,-70})));
  Modelica.Blocks.Sources.Ramp input_vaporPort(
    height=0.0001,
    duration=250,
    startTime=500)
    "Input for vapor Port"
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={-30,70})));

  SorpLib.Basics.Sources.Fluids.VLESource liquidInlet(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.SpecificEnthalpy,
    use_mFlowInput=true,
    m_flow_fixed=-0.001,
    h_fixed=0.5e6,
    redeclare package Medium = Medium)
    "Liquid inlet for phase seprator volume"
    annotation (Placement(transformation(
                extent={{-10,-10},{10,10}},
                rotation=90,
                origin={0,-40})));
  SorpLib.Basics.Sources.Fluids.VLESource VaporInlet(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.SpecificEnthalpy,
    use_mFlowInput=true,
    m_flow_fixed=1e-3,
    h_fixed=1e-6,
    redeclare package Medium = Medium)
    "Vapor inlet for phase separator"
    annotation (Placement(transformation(
                extent={{10,-10},{-10,10}},
                rotation=90,
                origin={0,40})));

  //
  // Definition of thermal boundaries
  //
  Modelica.Blocks.Sources.Ramp input_heatFlow(
    height=-350,
    duration=250,
    startTime=250)
    "Input for heat flow"
    annotation (Placement(transformation(
                extent={{10,-10},{-10,10}},
                rotation=0,
                origin={70,0})));

  SorpLib.Basics.Sources.Thermal.HeatSource heatSource(
    boundaryType=SorpLib.Choices.BoundaryThermal.HeatFlowRate,
    use_QFlowInput=true,
    Q_flow_fixed=-25)
    "Heat source for phase seprator volume"
    annotation (Placement(transformation(
                extent={{-10,-10},{10,10}},
                rotation=180,
                origin={40,0})));

equation
  //
  // Connections
  //
  connect(liquidInlet.port, phaseSeparator.cfp_xMinus[1]) annotation (Line(
      points={{0,-40},{0,-8.4},{-3.6,-8.4}},
      color={0,140,72},
      thickness=1));
  connect(VaporInlet.port, phaseSeparator.cfp_xPlus[1]) annotation (Line(
      points={{0,40},{0,15.6},{-3.6,15.6}},
      color={0,140,72},
      thickness=1));
  connect(heatSource.port, phaseSeparator.hp_yMinus) annotation (Line(
      points={{40,0},{12,0}},
      color={238,46,47},
      thickness=1));

  connect(input_liquidPort.y, liquidInlet.m_flow_input)
    annotation (Line(points={{-19,-70},{-2,-70},{-2,-41.2}}, color={0,0,127}));
  connect(input_vaporPort.y, VaporInlet.m_flow_input)
    annotation (Line(points={{-19,70},{-2,70},{-2,41.2}}, color={0,0,127}));
  connect(input_heatFlow.y, heatSource.Q_flow_input)
    annotation (Line(points={{59,0},{50,0},{50,5},{41,5}}, color={0,0,127}));

  //
  // Annotations
  //
  annotation (experiment(StopTime=1000), Documentation(info="<html>
<p>
This model checks the model of the phase separator volume.
<br/><br/>
To see the model behavior, plot the variables of the models over the 
time. The simulation time is correctly preset (Start: 0 s, Stop = 1000 s). 
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 14, 2023, by Mirko Engelpracht:<br/>
  Added documentation.
  </li>
  <li>
  January 13, 2021, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Test_PhaseSeparator;
