within SorpLib.Basics.Volumes.PhaseSeparatorVolumes;
package Tester "Models to test and varify models of phase separators"
  extends Modelica.Icons.ExamplesPackage;

  annotation (Documentation(info="<html>
<p>
This package contains executable test models for all implemented phase separators.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end Tester;
