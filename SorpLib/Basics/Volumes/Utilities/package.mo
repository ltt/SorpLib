within SorpLib.Basics.Volumes;
package Utilities "Package containing utility functions and models for modelling finite volumes"
  extends Modelica.Icons.UtilitiesPackage;

annotation (Documentation(info="<html>
<p>
This package contains utility functions and models required to model finite volumes.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Utilities;
