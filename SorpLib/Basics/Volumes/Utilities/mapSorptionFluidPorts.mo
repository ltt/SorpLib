within SorpLib.Basics.Volumes.Utilities;
function mapSorptionFluidPorts
  "Maps sorption fluid ports to the component array of a mixture fluid"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Integer no_components
    "Number of components"
    annotation (Dialog(tab="General", group="Inputs"));
  input Integer[:] ind_adsorptivs
    "Indeces of adsorptivs in the ideal gas mixture"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of outputs
  //
  output Integer[no_components] mappedFluidPorts
    "Mapped fluid ports"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of variables
  //
protected
  Integer fp_ind = 1
    "Actual index of fluid port";

algorithm
  //
  // Loop through components and check if they are adsorptives
  //
  mappedFluidPorts := zeros(no_components)
    "Mapped fluid ports";

  for ind_comp in 1:no_components loop
    for ind_ads in ind_adsorptivs loop
      if ind_comp == ind_ads then
        mappedFluidPorts[ind_comp] :=fp_ind
          "Map sorption fluid port index";
        fp_ind :=fp_ind + 1
          "Actual index of fluid port";
        break;

      end if;
    end for;
  end for;

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function maps the indices of sorption fluid ports to a component array of a
mixture fluid in the following way:
</p>
<ul>
  <li>
  0 if current component of the component array cannot be adsorbed/desorbed.
  </li>
  <li>
  Index of sorption fluid port if current component of the component array can be 
  adsorbed/desorbed.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  December 11, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end mapSorptionFluidPorts;
