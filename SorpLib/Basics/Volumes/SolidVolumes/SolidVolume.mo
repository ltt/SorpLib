within SorpLib.Basics.Volumes.SolidVolumes;
model SolidVolume "Homogenous solid volume"

  //
  // Definition of models
  //
  replaceable Media.Solids.MetalsAndMetalAlloys.Aluminium solidMedium
    constrainedby Media.Solids.BaseClasses.PartialSolid(
      final calcCaloricProperties=true,
      final approach_v=SorpLib.Choices.SpecificVolumeSolid.Constant,
      final p=p,
      final T=T)
    "Medium model calculating solid properties"
    annotation (Dialog(tab="General", group="Media"),
                choicesAllMatching=true,
                Placement(transformation(extent={{-8,-8},{12,12}})));

  //
  // Definition of parameters regarding calculation setup
  //
  parameter SorpLib.Choices.IndependentVariablesVolume independentStateVariables=
    SorpLib.Choices.IndependentVariablesVolume.pTX
    "Independent state variables"
    annotation (Dialog(tab="General", group="Calculation Setup"),
                Evaluate=true,
                HideResult = true);

  //
  // Definition of parameters regarding start values
  //
  parameter Modelica.Units.SI.Temperature T_initial = 298.15
    "Initial value of temperature"
    annotation (Dialog(tab="Initialisation", group="Initial Values",
                enable=(independentStateVariables==
                  SorpLib.Choices.IndependentVariablesVolume.pTX)));
  parameter Modelica.Units.SI.SpecificEnthalpy h_initial = 0
    "Initial value of specific enthalpy"
    annotation (Dialog(tab="Initialisation", group="Initial Values",
                enable=(independentStateVariables==
                  SorpLib.Choices.IndependentVariablesVolume.phX)));

  extends SorpLib.Basics.Volumes.BaseClasses.PartialVolume(
    final useHeatPorts=true,
    useHeatPortsX = true);

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.Pressure p
    "Pressure"
    annotation (Dialog(tab="General", group="Inputs", enable=false));

  //
  // Definition of variables
  //
  Modelica.Units.SI.Temperature T(
    start=T_initial,
    stateSelect= if independentStateVariables==
      SorpLib.Choices.IndependentVariablesVolume.pTX then StateSelect.prefer
      else StateSelect.avoid)
    "Temperature";
  Modelica.Units.SI.SpecificVolume v
    "Specific volume";
  Modelica.Units.SI.Density rho
    "Density";

  Modelica.Units.SI.SpecificEnthalpy h(
    start=h_initial,
    stateSelect= if independentStateVariables==
      SorpLib.Choices.IndependentVariablesVolume.phX then StateSelect.prefer
      else StateSelect.avoid)
    "Specific enthalpy";
  Modelica.Units.SI.SpecificInternalEnergy u
    "Specific internal energy";

  Modelica.Units.SI.Mass m
    "Mass";

  Modelica.Units.SI.InternalEnergy U
    "Internal energy";
  Real dU_dtau(final unit="W")
    "Derivative of internal energy w.r.t. time";
  Modelica.Units.SI.HeatFlowRate Qb_flow
    "Sum of all heat flow rates across boundaries";

  SorpLib.Basics.Volumes.Records.SolidVolumeProperties solidProperties
    "Properties of solid volume often required for calculation of heat transfer";

initial equation
  if type_energyBalance ==
    SorpLib.Choices.BalanceEquations.TransientFixedInitial then
    if independentStateVariables==
      SorpLib.Choices.IndependentVariablesVolume.pTX then
      T = T_initial
        "Fixed initial value";

    else
      h = h_initial
        "Fixed initial value";

    end if;

  elseif type_energyBalance ==
    SorpLib.Choices.BalanceEquations.TransientSteadyStateInitial then
    if independentStateVariables==
      SorpLib.Choices.IndependentVariablesVolume.pTX then
      der(T) = 0
        "Steady-state initial value";

    else
      der(h) = 0
        "Steady-state initial value";

    end if;
  end if;

equation
  //
  // Assertations
  //
  assert(useHeatPortsX or useHeatPortsY or useHeatPortsZ,
    "Must use heat ports at least in one direction (e.g., x, y, or z)!",
    level = AssertionLevel.error);

  //
  // Property calculation
  //
  v = solidMedium.state_variables.v
    "Specific volume";
  rho = 1 / v
    "Density";

  h = solidMedium.state_variables.h
    "Specific enthalpy";
  u = solidMedium.state_variables.u
    "Specific internal energy";

  //
  // Mass balance
  //
  m = geometry.V / v
    "Mass";

  //
  // Energy balance
  //
  U = m * u
    "Internal energy";

  dU_dtau = Qb_flow
  "Energy balance";

  if type_energyBalance==
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial then
    dU_dtau = 0
      "Steady-state energy balance";

  else
    if independentStateVariables==
      SorpLib.Choices.IndependentVariablesVolume.pTX then
      dU_dtau = m * solidMedium.additional_variables.c * der(T)
        "Transient energy balance with p and T as independent states";

    else
      dU_dtau = m *der(h)
        "Transient energy balance with p and h as independent states";

    end if;
  end if;

  Qb_flow = Q_flow_xMinus + Q_flow_xPlus + Q_flow_yMinus + Q_flow_yPlus +
    Q_flow_zMinus + Q_flow_zPlus
    "Sum of all heat flow rates across boundaries";

  T_heatPorts = T
    "Required for conditional component";

  //
  // Summary record
  //
  solidProperties.p = p
    "Pressure";
  solidProperties.T = T
    "Temperature";
  solidProperties.v = v
    "Specific volume";

  solidProperties.c = solidMedium.additional_variables.c
    "Specific heat capacity";
  solidProperties.lambda = solidMedium.additional_variables.lambda
    "Thermal conductivity";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This model represents a solid volume, applying a lumped modeling approach. Depending on
the volume setup, this model may have up to six heat ports (i.e., two for each spatial
direction of a cartesian coordinate system). These heat ports allow for the combination 
of several solid volumes via appropriate heat transfer models to create a spatially 
distributed model.
</p>

<h4>Main equations</h4>
<p>
The most important equation is the energy balance. When using the temperature <i>T</i> as
an independent state, the energy balance is defined as
</p>
<pre>
    m * c  * (dT/d&tau;) = &sum; Q<sub>b,flow</sub>;
</pre>
<p>
When using the specific enthalpy <i>h</i> as an independent state, the energy balance is 
defined as
</p>
<pre>
    m * (dh/d&tau;) = &sum; Q<sub>b,flow</sub>;
</pre>

<p>
Herein, <i>m</i> is the volume mass, <i>T</i> is the temperature, <i>h</i> is the 
specific enthalpy, <i>c</i> is the specific heat capacity, and <i>Q<sub>b,flow</sub></i> 
is the sum of the heat flow rates over the volume boundaries.
</p>

<h4>Assumptions and limitations</h4>
<ul>
  <li>
  Constant volume <i>V</i>
  </li>
  <li>
  Incompressible medium (i.e., constant specific volume <i>v</i>)
  </li>
  <li>
  Homogenoues properties within the volume
  </li>
</ul>

<h4>Typical use</h4>
<p>
This model is typically used as wall model to represent the thermal capacity of a
metal. Thus, e.g., walls of pipes, fins of heat exchangers, or casings of components
are modeled.
</p>

<h4>Important parameters and options</h4>
<ul>
  <li>
  <i>useHeatPorts</i>:
  Defines if heat ports in the spatial direction <i>i</i> are required.
  </li>
  <li>
  <i>independentStateVariables</i>:
  Defines independent state variables of the model.
  </li>
  <li>
  <i>type_energyBalance</i>:
  Defines the type of the energy balance.
  </li>
</ul>

<h4>Dynamics</h4>
<p>
This model has one dynamic state that can be selected (see options):
</p>
<ul>
  <li>
  Temperature <i>T</i> (recommended to avoid non-linear equations), or
  </li>
  <li>
  specific enthalpy <i>h</i>.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  December 6, 2023, by Mirko Engelpracht:<br/>
  Major revisions due to restructering finite volumes.
  </li>
  <li>
  January 12, 2021, by Mirko Engelpracht:<br/>
  Revisions (e.g., object-oriented approach) after restructering of the library.
  </li>
  <li>
  November 23, 2017, by Andrej Gibelhaus:<br/>
  Tidy up implementation and enhance documentation for publication of library.
  </li>
</ul>
</html>"));
end SolidVolume;
