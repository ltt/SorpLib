within SorpLib.Basics.Volumes.SolidVolumes.Tester;
model Test_SolidVolume "Tester for solid"
  extends Modelica.Icons.Example;

  //
  // Definition of parameters
  //
  replaceable model Solid =
      SorpLib.Media.Solids.MetalsAndMetalAlloys.Aluminium
    constrainedby SorpLib.Media.Solids.BaseClasses.PartialSolid
    "Solid medium"
    annotation (Dialog(tab="General", group="Media"));

  parameter Modelica.Units.SI.Pressure p_initial = 0
    "Initial pressure"
    annotation (Dialog(tab="General", group="Start Values"));
  parameter Modelica.Units.SI.Temperature T_initial = 273.15
    "Initial temperature"
    annotation (Dialog(tab="General", group="Start Values"));

  //
  // Definition of solid models
  //
  SorpLib.Basics.Volumes.SolidVolumes.SolidVolume solid_transientFreeInitial_x(
    redeclare final Solid solidMedium,
    final p=p,
    final T_initial=T_initial,
    final type_energyBalance=SorpLib.Choices.BalanceEquations.TransientFreeInitial)
    "Solid with transient energy balance but free start values and heat ports in 
    x direction"
    annotation (Placement(transformation(extent={{-40,60},{-20,80}})));
  SorpLib.Basics.Volumes.SolidVolumes.SolidVolume solid_transientFreeInitial_xy(
    redeclare final Solid solidMedium,
    final p=p,
    final T_initial=T_initial,
    final useHeatPortsY=true,
    final type_energyBalance=SorpLib.Choices.BalanceEquations.TransientFreeInitial)
    "Solid with transient energy balance but free start values and heat ports in 
    x and y directions"
    annotation (Placement(transformation(extent={{-10,60},{10,80}})));
  SorpLib.Basics.Volumes.SolidVolumes.SolidVolume
    solid_transientFreeInitial_xyz(
    redeclare final Solid solidMedium,
    final p=p,
    final T_initial=T_initial,
    final useHeatPortsY=true,
    final useHeatPortsZ=true,
    final type_energyBalance=SorpLib.Choices.BalanceEquations.TransientFreeInitial)
    "Solid with transient energy balance but free start values and heat ports in 
    x, y, and z directions"
    annotation (Placement(transformation(extent={{20,60},{40,80}})));

  SorpLib.Basics.Volumes.SolidVolumes.SolidVolume solid_transientFixed_x(
    redeclare final Solid solidMedium,
    final p=p,
    final T_initial=T_initial,
    final type_energyBalance=SorpLib.Choices.BalanceEquations.TransientFixedInitial)
    "Solid with transient energy balance but fixed start values and heat ports in 
    x direction"
    annotation (Placement(transformation(extent={{-40,20},{-20,40}})));
  SorpLib.Basics.Volumes.SolidVolumes.SolidVolume solid_transientFixed_xy(
    redeclare final Solid solidMedium,
    final p=p,
    final T_initial=T_initial,
    final useHeatPortsY=true,
    final type_energyBalance=SorpLib.Choices.BalanceEquations.TransientFixedInitial)
    "Solid with transient energy balance but fixed start values and heat ports in 
    x and y directions"
    annotation (Placement(transformation(extent={{-10,20},{10,40}})));
  SorpLib.Basics.Volumes.SolidVolumes.SolidVolume solid_transientFixed_xyz(
    redeclare final Solid solidMedium,
    final p=p,
    final T_initial=T_initial,
    final useHeatPortsY=true,
    final useHeatPortsZ=true,
    final type_energyBalance=SorpLib.Choices.BalanceEquations.TransientFixedInitial)
    "Solid with transient energy balance but fixed start values and heat ports in 
    x, y, and z directions"
    annotation (Placement(transformation(extent={{20,20},{40,40}})));

  SorpLib.Basics.Volumes.SolidVolumes.SolidVolume
    solid_transientFreeInitial_boundaries_x(
    redeclare final Solid solidMedium,
    final p=p,
    final T_initial=T_initial,
    final type_energyBalance=SorpLib.Choices.BalanceEquations.TransientFreeInitial)
    "Solid with transient energy balance but free start values and heat ports in 
    x direction connected to thermal boundaries"
    annotation (Placement(transformation(extent={{-10,-30},{10,-10}})));
  SorpLib.Basics.Volumes.SolidVolumes.SolidVolume
    solid_transientFixedInitial_boundaries_x(
    redeclare final Solid solidMedium,
    final p=p,
    final T_initial=T_initial,
    final type_energyBalance=SorpLib.Choices.BalanceEquations.TransientFixedInitial)
    "Solid with transient energy balance but fixed start values and heat ports in 
    x direction connected to thermal boundaries"
    annotation (Placement(transformation(extent={{-10,-60},{10,-40}})));
  SorpLib.Basics.Volumes.SolidVolumes.SolidVolume
    solid_steadyStateFreeInitial_boundaries_x(
    redeclare final Solid solidMedium,
    final p=p,
    final T_initial=T_initial,
    final type_energyBalance=SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial)
    "Solid with steady-state energy balance but free start values and heat ports 
    in x direction connected to thermal boundaries"
    annotation (Placement(transformation(extent={{-10,-90},{10,-70}})));

  //
  // Definition of boundaries
  //
protected
  Sources.Thermal.HeatSource heatSource_transientFixedInitial_xMinus(
      boundaryType=SorpLib.Choices.BoundaryThermal.HeatFlowRate, use_QFlowInput=
       true) "Heat source for heat ports '-dx/2'"
    annotation (Placement(transformation(extent={{-60,-60},{-40,-40}})));
  Sources.Thermal.HeatSource heatSource_transientFreeInitial_xMinus(
    boundaryType=SorpLib.Choices.BoundaryThermal.HeatFlowRate,
    use_QFlowInput=true)
    "Heat source for heat ports '-dx/2'"
    annotation (Placement(transformation(extent={{-60,-30},{-40,-10}})));
  Sources.Thermal.HeatSource heatSource_steadyStateFreeInitial_xMinus(
    boundaryType=SorpLib.Choices.BoundaryThermal.HeatFlowRate,
    use_QFlowInput= true)
    "Heat source for heat ports '-dx/2'"
    annotation (Placement(transformation(extent={{-60,-90},{-40,-70}})));
  Sources.Thermal.HeatSource heatSource_steadyStateFreeInitial_xPlus(
    boundaryType=SorpLib.Choices.BoundaryThermal.Temperature,
    use_TInput=true)
    "Heat source for heat ports '+dx/2'"
    annotation (Placement(transformation(extent={{50,-90},{30,-70}})));

  //
  // Definition of input signals
  //
  Modelica.Blocks.Sources.Ramp input_QFlowVar(
    height=10000,
    duration=25,
    offset=-5000,
    startTime=0) "Ramp to simulate input signal of heat flow rate" annotation (
      Placement(transformation(extent={{-90,-60},{-70,-40}})),  HideResult=true);

  Modelica.Blocks.Sources.Ramp input_TVar(
    height=200,
    duration=25,
    offset=173.15,
    startTime=0) "Ramp to simulate input signal of temperature" annotation (
      Placement(transformation(extent={{90,-60},{70,-40}})),  HideResult=true);
  //
  // Definition of variables
  //
  Modelica.Units.SI.Pressure p(start=p_initial, fixed=true)
    "Pressure";

equation
  //
  // Change of state variables
  //
  der(p) = (10e5 - p_initial) / 25
    "Pressure";

  //
  // Connections
  //
  connect(heatSource_transientFreeInitial_xMinus.port,
    solid_transientFreeInitial_boundaries_x.hp_xMinus) annotation (Line(
      points={{-50,-20},{-6,-20}},
      color={238,46,47},
      thickness=1));
  connect(heatSource_transientFixedInitial_xMinus.port,
    solid_transientFixedInitial_boundaries_x.hp_xMinus) annotation (Line(
      points={{-50,-50},{-6,-50}},
      color={238,46,47},
      thickness=1));
  connect(heatSource_steadyStateFreeInitial_xMinus.port,
    solid_steadyStateFreeInitial_boundaries_x.hp_xMinus) annotation (Line(
      points={{-50,-80},{-6,-80}},
      color={238,46,47},
      thickness=1));
  connect(solid_steadyStateFreeInitial_boundaries_x.hp_xPlus,
    heatSource_steadyStateFreeInitial_xPlus.port) annotation (Line(
      points={{6,-80},{40,-80}},
      color={238,46,47},
      thickness=1));

  connect(input_TVar.y, heatSource_steadyStateFreeInitial_xPlus.T_input)
    annotation (Line(points={{69,-50},{60,-50},{60,-74.8},{41,-74.8}}, color={0,
          0,127}));
  connect(input_QFlowVar.y, heatSource_transientFreeInitial_xMinus.Q_flow_input)
    annotation (Line(points={{-69,-50},{-60,-50},{-60,-25},{-51,-25}}, color={0,
          0,127}));
  connect(input_QFlowVar.y, heatSource_transientFixedInitial_xMinus.Q_flow_input)
    annotation (Line(points={{-69,-50},{-60,-50},{-60,-55},{-51,-55}}, color={0,
          0,127}));
  connect(input_QFlowVar.y, heatSource_steadyStateFreeInitial_xMinus.Q_flow_input)
    annotation (Line(points={{-69,-50},{-60,-50},{-60,-85},{-51,-85}}, color={0,
          0,127}));

  //
  // Annotations
  //
  annotation (experiment(StopTime=25), Documentation(info="<html>
<p>
This model checks the solid volume model.
<br/><br/>
To see the model behavior, plot the variables of the models over the 
time. The simulation time is correctly preset (Start: 0 s, Stop = 25 s). 
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 6, 2023, by Mirko Engelpracht:<br/>
  Added documentation.
  </li>
  <li>
  January 12, 2021, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Test_SolidVolume;
