within SorpLib.Basics.Volumes;
package SolidVolumes "Package containing finte volume models of solids"
extends Modelica.Icons.VariantsPackage;

annotation (Documentation(info="<html>
<p>
This package contains finite volume models of solids.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end SolidVolumes;
