within SorpLib.Basics;
package Volumes "Finite volumes used to aggregate complex models"
extends SorpLib.Icons.VolumesPackage;

annotation (Documentation(info="<html>
<p>
This package provides finite volume models of solids, fluids, phase separators,
and adsorbates. These models calculate thermodynamic properties as homogenous 
properties within the volume. Thus, these models serve as the basis for aggregating 
complex  models discretized in several spatial directions via an upwind finite 
volume approach.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end Volumes;
