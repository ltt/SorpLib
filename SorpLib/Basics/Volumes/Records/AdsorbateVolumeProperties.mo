within SorpLib.Basics.Volumes.Records;
record AdsorbateVolumeProperties
  "This record summarizes important thermodynamic properties of an adsorbate volume"
  extends Modelica.Icons.Record;

  //
  // Definition of parameters
  //
  parameter Integer no_adsorptivs = 1
    "Number of adsorptivs (i.e., components that can be adsorbed/desorbed)"
    annotation (Dialog(tab="General", group="Media"));

  //
  // Definition of state properties
  //
  Modelica.Units.SI.Pressure p
    "Pressure";
  Modelica.Units.SI.Temperature T
    "Temperature";
  SorpLib.Units.Uptake x
    "Loading";
  SorpLib.Units.Uptake[no_adsorptivs] x_i
    "Loading of each component";

  //
  // Definition of additional properties
  //
  Modelica.Units.SI.SpecificHeatCapacity cp
    "Specific heat capacity of the adsorpt";
  Modelica.Units.SI.SpecificEnthalpy dh_ads
    "Specific enthalpy of adsorption";

  //
  // Definition of flow-specific variables
  //
  Modelica.Units.SI.MassFlowRate m_flow_sorption
    "Total mass flow rate at port 'sorption'";

  Modelica.Units.SI.MassFlowRate ma_flow_xMinus
    "Adsorbate mass flow rate at port '-dx/2'";
  Modelica.Units.SI.MassFlowRate ma_flow_xPlus
    "Adsorbate mass flow rate at port '+dx/2'";

  Modelica.Units.SI.MassFlowRate ma_flow_yMinus
    "Adsorbate mass flow rate at port '-dy/2'";
  Modelica.Units.SI.MassFlowRate ma_flow_yPlus
    "Adsorbate mass flow rate at port '+dy/2'";

  Modelica.Units.SI.MassFlowRate ma_flow_zMinus
    "Adsorbate mass flow rate at port '-dz/2'";
  Modelica.Units.SI.MassFlowRate ma_flow_zPlus
    "Adsorbate mass flow rate at port '+dz/2'";

  Modelica.Units.SI.MassFlowRate[no_adsorptivs] md_flow_xMinus
    "Diffusive mass flow rates at port '-dx/2'";
  Modelica.Units.SI.MassFlowRate[no_adsorptivs] md_flow_xPlus
    "Diffusive mass flow rates at port '+dx/2'";

  Modelica.Units.SI.MassFlowRate[no_adsorptivs] md_flow_yMinus
    "Diffusive mass flow rates at port '-dy/2'";
  Modelica.Units.SI.MassFlowRate[no_adsorptivs] md_flow_yPlus
    "Diffusive mass flow rates at port '+dy/2'";

  Modelica.Units.SI.MassFlowRate[no_adsorptivs] md_flow_zMinus
    "Diffusive mass flow rates at port '-dz/2'";
  Modelica.Units.SI.MassFlowRate[no_adsorptivs] md_flow_zPlus
    "Diffusive mass flow rates at port '+dz/2'";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This record contains important thermodynamic properties of an adsorbate volume.
These properties may be required to calculate heat transfer or mass transfer 
phenomena.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 15, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end AdsorbateVolumeProperties;
