within SorpLib.Basics.Volumes.Records;
record VolumeGeometry
  "This record defines the geometry of a finite volume"
  extends Modelica.Icons.Record;

  //
  // Definition of parameters
  //
  parameter Modelica.Units.SI.Length dx = 0.1
    "Length in x direction"
    annotation (Dialog(tab="General", group="Geometry - Length"));
  parameter Modelica.Units.SI.Length dy = 0.1
    "Length in y direction"
    annotation (Dialog(tab="General", group="Geometry - Length"));
  parameter Modelica.Units.SI.Length dz = 0.1
    "Length in z direction"
    annotation (Dialog(tab="General", group="Geometry - Length"));

  parameter Modelica.Units.SI.Area A_xy = dx * dy
    "Cross-sectional area of x-y direction"
    annotation (Dialog(tab="General", group="Geometry - Area"));
  parameter Modelica.Units.SI.Area A_xz = dx * dz
    "Cross-sectional area of x-z direction"
    annotation (Dialog(tab="General", group="Geometry - Area"));
  parameter Modelica.Units.SI.Area A_yz = dy * dz
    "Cross-sectional area of y-z direction"
    annotation (Dialog(tab="General", group="Geometry - Area"));

  parameter Modelica.Units.SI.Volume V = dx * dy * dz
    "Volume"
    annotation (Dialog(tab="General", group="Geometry - Volume"));

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This record contains parameters defining the geometry of a finite volume.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 6, 2023, by Mirko Engelpracht:<br/>
  Update due to major restructering of finite volume cells.
  </li>
  <li>
  January 12, 2021, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end VolumeGeometry;
