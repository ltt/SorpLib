within SorpLib.Basics.Volumes.Records;
record PhaseSeparatorGeometry
  "This record defines the geometry of a phase separator volume"
  extends SorpLib.Basics.Volumes.Records.VolumeGeometry;

  //
  // Definition of parameters
  //
  parameter Modelica.Units.SI.Area A_base = A_yz
    "Base area used to calculate relative filling level"
    annotation (Dialog(tab="General", group="Geometry - Area"));

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This record contains parameters defining the geometry of a phase separator volume.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 14, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end PhaseSeparatorGeometry;
