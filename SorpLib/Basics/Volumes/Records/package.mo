within SorpLib.Basics.Volumes;
package Records "Records describing geometry and summarizing properties of finite volumes"
  extends Modelica.Icons.RecordsPackage;

  annotation (Documentation(info="<html>
<p>
This package contains definitions of records. These records are used to cluster 
parameters and variables to tidy up the model implementation.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end Records;
