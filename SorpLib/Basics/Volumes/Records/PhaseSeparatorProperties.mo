within SorpLib.Basics.Volumes.Records;
record PhaseSeparatorProperties
  "This record summarizes important thermodynamic properties of a phase separator volume"
  extends Modelica.Icons.Record;

  //
  // Definition of parameters
  //
  parameter Integer no_components = 1
    "Number of components"
    annotation (Dialog(tab="General", group="Media"));

  //
  // Definition of state properties
  //
  Modelica.Units.SI.Pressure p
    "Pressure";
  Modelica.Units.SI.Temperature T
    "Temperature";
  Modelica.Units.SI.SpecificVolume v
    "Specific volume";

  //
  // Definition of additional properties
  //
  Modelica.Units.SI.Length l_liq
    "Height of liquid phase";
  Real l_liq_rel(unit="1")
    "Relative height of liquid phase";

  //
  // Definition of additional properties
  //
  Modelica.Units.SI.SpecificHeatCapacity cp
    "Specific heat capacity at constant pressure";
  Modelica.Units.SI.SpecificHeatCapacity cv
    "Specific heat capacity at constant volume";
  Modelica.Units.SI.ThermalConductivity lambda
    "Thermal conductivity";
  Modelica.Units.SI.DynamicViscosity eta
    "Dynamic viscosity";

  Modelica.Media.Common.IsobaricVolumeExpansionCoefficient beta
    "Isobaric expnasion coefficient";
  Modelica.Media.Common.IsothermalCompressibility kappa
    "Isothermal compressibility";
  Modelica.Media.Common.JouleThomsonCoefficient my
    "Joule-Thomson coefficient";

  Modelica.Units.SI.PrandtlNumber Pr
    "Prandtl number";

  //
  // Definition of flow-specific variables
  //
  Modelica.Units.SI.MassFlowRate mc_flow_xMinus
    "Total convective mass flow rate at port '-dx/2'";
  Modelica.Units.SI.MassFlowRate mc_flow_xPlus
    "Total convective mass flow rate at port '+dx/2'";

  Modelica.Units.SI.MassFlowRate mc_flow_yMinus
    "Total convective mass flow rate at port '-dy/2'";
  Modelica.Units.SI.MassFlowRate mc_flow_yPlus
    "Total convective mass flow rate at port '+dy/2'";

  Modelica.Units.SI.MassFlowRate mc_flow_zMinus
    "Total convective mass flow rate at port '-dz/2'";
  Modelica.Units.SI.MassFlowRate mc_flow_zPlus
    "Total convective mass flow rate at port '+dz/2'";

  Modelica.Units.SI.MassFlowRate[no_components] md_flow_xMinus
    "Diffusive mass flow rates at port '-dx/2'";
  Modelica.Units.SI.MassFlowRate[no_components] md_flow_xPlus
    "Diffusive mass flow rates at port '+dx/2'";

  Modelica.Units.SI.MassFlowRate[no_components] md_flow_yMinus
    "Diffusive mass flow rates at port '-dy/2'";
  Modelica.Units.SI.MassFlowRate[no_components] md_flow_yPlus
    "Diffusive mass flow rates at port '+dy/2'";

  Modelica.Units.SI.MassFlowRate[no_components] md_flow_zMinus
    "Diffusive mass flow rates at port '-dz/2'";
  Modelica.Units.SI.MassFlowRate[no_components] md_flow_zPlus
    "Diffusive mass flow rates at port '+dz/2'";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This record contains important thermodynamic properties of a phase seprator 
volume. These properties may be required to calculate heat transfer or mass 
transfer phenomena.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 14, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end PhaseSeparatorProperties;
