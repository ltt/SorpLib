within SorpLib.Basics.Volumes.Records;
record SolidVolumeProperties
  "This record summarizes important thermodynamic properties of a solide volume"
  extends Modelica.Icons.Record;

  //
  // Definition of state properties
  //
  Modelica.Units.SI.Pressure p
    "Pressure";
  Modelica.Units.SI.Temperature T
    "Temperature";
  Modelica.Units.SI.SpecificVolume v
    "Specific volume";

  //
  // Definition of additional properties
  //
  Modelica.Units.SI.SpecificHeatCapacity c
    "Specific heat capacity";
  Modelica.Units.SI.ThermalConductivity lambda
    "Thermal conductivity";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This record contains important thermodynamic properties of a solid volume. These
properties may be required to calculate heat transfer phenomena.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 6, 2023, by Mirko Engelpracht:<br/>
  Update due to major restructering of finite volume cells.
  </li>
  <li>
  January 12, 2021, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end SolidVolumeProperties;
