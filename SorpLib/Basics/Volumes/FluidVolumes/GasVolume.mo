within SorpLib.Basics.Volumes.FluidVolumes;
model GasVolume "Homogenous volume of an ideal gas"
  extends SorpLib.Basics.Volumes.BaseClasses.PartialFluidVolume(
    final no_components = Medium.nX,
    final pressureNoStateVariable = Medium.singleState,
    final neglectTermVp = false,
    redeclare final connector FluidPortsIn =
      SorpLib.Basics.Interfaces.FluidPorts.GasPort_in,
    redeclare final connector FluidPortsOut =
      SorpLib.Basics.Interfaces.FluidPorts.GasPort_out,
    independentStateVariables=SorpLib.Choices.IndependentVariablesVolume.pTX,
    useHeatPorts=true,
    useHeatPortsY=true,
    h_initial=Medium.specificEnthalpy_pTX(p=p_initial, T=T_initial, X=Medium.reference_X),
    type_overallMassBalance=
      SorpLib.Choices.BalanceEquations.TransientFixedInitial,
    type_energyBalance=
      SorpLib.Choices.BalanceEquations.TransientFixedInitial);

  //
  // Definition of parameters regarding the medium
  //
  replaceable package Medium = SorpLib.Media.IdealGases.N2
    constrainedby Modelica.Media.IdealGases.Common.SingleGasNasa
    "Medium model of the ideal gas"
    annotation (Dialog(tab="General", group="Media"),
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

  //
  // Definition of parameters regarding the calculation setup
  //
  parameter Boolean idealGas = true
    " = true, if medium is an ideal gas and govering equations are explicitly 
    written for an ideal gas"
    annotation (Dialog(tab="General", group="Calculation Setup"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult = true);

  //
  // Definition of parameters regarding start values
  //
  parameter Modelica.Units.SI.MassFlowRate ms_flow_initial = 1e-4
    "Start value for sorption mass flow rate"
    annotation (Dialog(tab="Initialisation", group="Start Values"));

  //
  // Definition of ports
  //
  SorpLib.Basics.Interfaces.HeatPorts.HeatPort_in hp_sorption
    "Sorption heat port"
    annotation (Placement(transformation(extent={{10,-50},{22,-38}}),
                iconTransformation(extent={{10,-50},{22,-38}})));

  FluidPortsIn fp_sorption(
    final no_components=no_components,
    m_flow(start=ms_flow_initial))
    "Sorption fluid port"
    annotation (Placement(transformation(extent={{-20,-80},{-12,-72}}),
                iconTransformation(extent={{-20,-80},{-12,-72}})));

  //
  // Definition of variables
  //
  Modelica.Units.SI.MassFlowRate ms_flow
    "Sum of all sorption mass flow rates across boundaries";

  //
  // Definition of protected variables
  //
protected
  Medium.ThermodynamicState state
    "Thermodynamic state required to calculate medium properties";

equation
  //
  // Assertations
  //
  assert(no_components <= 1,
    "The gas volume model can only handel pure fluids (i.e., with one component)!",
    level = AssertionLevel.error);

  assert(pressureNoStateVariable or not (
    type_energyBalance<>SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial and
    type_overallMassBalance==SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial),
    "Steady-state mass balance combined with transient energy balance is not " +
    "sound if the fluid volume is fixed!",
    level = AssertionLevel.warning);

  //
  // Calculation of properties
  //
  if independentStateVariables ==
    SorpLib.Choices.IndependentVariablesVolume.pTX then
    state = Medium.setState_pTX(p=p, T=T, X=Medium.reference_X)
      "Thermodynamic state required to calculate medium properties";
    h = Medium.specificEnthalpy(state=state)
      "Specific enthalpy";

  else
    state = Medium.setState_phX(p=p, h=h, X=Medium.reference_X)
      "Thermodynamic state required to calculate medium properties";
    T = Medium.temperature(state=state)
      "Temperature";

  end if;

  v = 1 / rho
    "Specific volume";
  rho = Medium.density(state=state)
    "Density";

  //
  // Momentum balance
  //
  fp_sorption.p = p
    "Total pressure at the port (i.e., homogenous volume)";

  //
  // Mass balance
  //
  dm_dtau = mc_flow + ms_flow
    "Overall mass balance";

  if type_overallMassBalance==
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial then
    dm_dtau = 0
      "Steady-state overall mass balance";

  else
    if independentStateVariables==
      SorpLib.Choices.IndependentVariablesVolume.pTX then
      dm_dtau = geometry.V * rho *
        (fluidProperties.kappa *der(p) - fluidProperties.beta * der(T))
        "Transient overall mass balance";

    else
      if idealGas then
        dm_dtau = geometry.V * rho * (
          fluidProperties.kappa * der(p) -
          fluidProperties.beta / fluidProperties.cp * der(h))
          "Transient overall mass balance";

      else
        dm_dtau = geometry.V * rho * ((fluidProperties.kappa -
        fluidProperties.beta * fluidProperties.my) * der(p) -
        fluidProperties.beta / fluidProperties.cp * der(h))
        "Transient overall mass balance";

      end if;
    end if;
  end if;

  mc_flow = sum(cfp_xMinus.m_flow) + sum(cfp_xPlus.m_flow)
    "Sum of all convective mass flow rates across boundaries";
  ms_flow = fp_sorption.m_flow
    "Sum of all sorption mass flow rates across boundaries";

  //
  // Energy balance
  //
  dU_dtau = Hb_flow + Qb_flow
    "Energy balane";

  if type_energyBalance==
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial then
    dU_dtau = 0
      "Steady-state energy balance";

  else
    if independentStateVariables==
      SorpLib.Choices.IndependentVariablesVolume.pTX then
      if idealGas then
        dU_dtau = u * dm_dtau + m * (
          (fluidProperties.cp - p * v * fluidProperties.beta) * der(T))
          "Transient energy balance";

      else
        dU_dtau = u * dm_dtau + m * (
        v * (p * fluidProperties.kappa - T * fluidProperties.beta) * der(p) +
        (fluidProperties.cp - p * v * fluidProperties.beta) * der(T))
        "Transient energy balance";

      end if;

    else
      if idealGas then
        dU_dtau = u * dm_dtau + m * (
          (1 - p * v * fluidProperties.beta / fluidProperties.cp) * der(h))
          "Transient energy balance";

      else
        dU_dtau = u * dm_dtau + m * (
        (1 - p * v * fluidProperties.beta / fluidProperties.cp) * der(h) +
        v * (p * (fluidProperties.kappa - fluidProperties.beta *
        fluidProperties.my) - 1) * der(p))
        "Transient energy balance";

      end if;
    end if;
  end if;

  if avoid_events then
    Hb_flow =
      sum(cfp_xMinus.m_flow .* noEvent(actualStream(cfp_xMinus.h_outflow))) +
      sum(cfp_xPlus.m_flow .* noEvent(actualStream(cfp_xPlus.h_outflow))) +
      fp_sorption.m_flow * noEvent(actualStream(fp_sorption.h_outflow))
      "Sum of all enthalpy flow rates across boundaries";

  else
    Hb_flow =
      sum(cfp_xMinus.m_flow .* actualStream(cfp_xMinus.h_outflow)) +
      sum(cfp_xPlus.m_flow .* actualStream(cfp_xPlus.h_outflow)) +
      fp_sorption.m_flow * actualStream(fp_sorption.h_outflow)
      "Sum of all enthalpy flow rates across boundaries";

  end if;

  Qb_flow = Q_flow_xMinus + Q_flow_xPlus +
    Q_flow_yMinus + Q_flow_yPlus +
    Q_flow_zMinus + Q_flow_zPlus +
    hp_sorption.Q_flow
    "Sum of all heat flow rates across boundaries";

  fp_sorption.h_outflow = h
    "Specific enthalpy leaving the port (i.e., homogenous volume)";

  hp_sorption.T = T
    "Temperature at sorption heat port";

  //
  // Summary record
  //
  fluidProperties.cp = if (calculateAdditionalProperties or
    (independentStateVariables==
    SorpLib.Choices.IndependentVariablesVolume.phX and
    type_overallMassBalance<>
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial) or
    (independentStateVariables==
    SorpLib.Choices.IndependentVariablesVolume.pTX and
    type_energyBalance<>
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial) or
    (independentStateVariables==
    SorpLib.Choices.IndependentVariablesVolume.phX and
    type_energyBalance<>
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial and
    not neglectTermVp)) then
    Medium.specificHeatCapacityCp(state=state) else 0
    "Specific heat capacity";
  fluidProperties.lambda = if calculateAdditionalProperties then
    Medium.thermalConductivity(state=state) else 0
    "Thermal conductivity";
  fluidProperties.eta = if calculateAdditionalProperties then
    Medium.dynamicViscosity(state=state) else 0
    "Dynamic viscosity";

  fluidProperties.beta = if (calculateAdditionalProperties or
    type_overallMassBalance<>
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial or
    (independentStateVariables==
    SorpLib.Choices.IndependentVariablesVolume.pTX and
    type_energyBalance<>
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial) or
    (independentStateVariables==
    SorpLib.Choices.IndependentVariablesVolume.phX and
    type_energyBalance<>
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial and
    not neglectTermVp)) then
    Medium.isobaricExpansionCoefficient(state=state) else 0
    "Isobaric expnasion coefficient";
  fluidProperties.kappa = if (calculateAdditionalProperties or
    type_overallMassBalance<>
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial or
    (type_energyBalance<>
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial and
    not neglectTermVp)) then
    Medium.isothermalCompressibility(state=state) else 0
    "Isothermal compressibility";
  fluidProperties.my = if (calculateAdditionalProperties or
    (independentStateVariables==
    SorpLib.Choices.IndependentVariablesVolume.phX and
    (type_overallMassBalance<>
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial or
    (type_energyBalance<>
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial and
    not neglectTermVp)))) then
    v / fluidProperties.cp * (fluidProperties.beta * T - 1) else 0
    "Joule-Thomson coefficient";

  fluidProperties.Pr = if calculateAdditionalProperties then
    fluidProperties.eta * fluidProperties.cp / fluidProperties.lambda else 0
    "Prandtl number";

  fluidProperties.m_flow_sorption = ms_flow
    "Total mass flow rate at port 'sorption'";

  fluidProperties.mc_flow_yMinus = 0
    "Convective mass flow rate at port '-dy/2'";
  fluidProperties.mc_flow_yPlus = 0
    "Convective mass flow rate at port '+dy/2'";
  fluidProperties.mc_flow_zMinus = 0
    "Convective mass flow rate at port '-dz/2'";
  fluidProperties.mc_flow_zPlus = 0
    "Convective mass flow rate at port '+dz/2'";

  fluidProperties.md_flow_xMinus = zeros(no_components)
    "Diffusive mass flow rate at port '-dx/2'";
  fluidProperties.md_flow_xPlus = zeros(no_components)
    "Diffusive mass flow rate at port '+dx/2'";
  fluidProperties.md_flow_yMinus = zeros(no_components)
    "Diffusive mass flow rate at port '-dy/2'";
  fluidProperties.md_flow_yPlus = zeros(no_components)
    "Diffusive mass flow rate at port '+dy/2'";
  fluidProperties.md_flow_zMinus = zeros(no_components)
    "Diffusive mass flow rate at port '-dz/2'";
  fluidProperties.md_flow_zPlus = zeros(no_components)
    "Diffusive mass flow rate at port '+dz/2'";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This model represents a gas volume, applying a lumped modeling approach. Depending 
on the volume setup, this model may have up to seven heat ports (i.e., two for each 
spatial direction of a cartesian coordinate system and one for an adsorbate volume).
Furthermore, this model has convective fluid ports in y-direction, following the 
'connectorSizing' principle, and a sorption fluid port. These ports allow for the 
combination of several fluid volumes to create a spatially distributed model.
</p>

<h4>Main equations</h4>
<p>
The most important equations are the momentum, mass, and energy balance. According to
the staggered grid approach, the momentum balance is not solved within the volume but 
at the volume's boundaries via so-called 
<a href=\"Modelica://SorpLib.Components.Fittings\">flow models</a>. Hennce, no pressure 
losses occur within the volume:
</p>
<pre>
    p = cfp_xMinus.p;
</pre>
<pre>
    p = cfp_xPlus.p;
</pre>
<pre>
    p = fp_sorption.p;
</pre>

<p>
Regarding the mass and energy balances, either steady-state or tansient balnaces
can be selected. When using the pressure <i>p</i> and temperature <i>T</i> as  
independent states, the mass balance is defined as
</p>
<pre>
    (dm/d&tau;) = V * (d&rho;/d&tau;) = V * &rho; * [&kappa; * (dp/d&tau;) - &beta; * (dT/d&tau;)] = &sum; m<sub>c,flow</sub>;
</pre>
<p>
and the energy balance is defined as
</p>
<pre>
    (dU/d&tau;) = u * (dm/d&tau;) + m * (du/d&tau;) = u * (dm/d&tau;) + m * [v * (p * &kappa; - T * &beta;) * (dp/d&tau;) + (c<sub>p</sub> - p * v * &beta;) * (dT/d&tau;)] = &sum; H<sub>b,flow</sub> + &sum; Q<sub>b,flow</sub>;
</pre>

<p>
When using the pressure <i>p</i> and specific enthalpy <i>h</i> as independent 
states, the mass balance is defined as
</p>
<pre>
    (dm/d&tau;) = V * (d&rho;/d&tau;) = V * &rho; * [(&kappa; - &beta; * &mu;) * (dp/d&tau;) - &beta; / c<sub>p</sub> * (dh/d&tau;)] = &sum; m<sub>c,flow</sub>;
</pre>
<p>
and the energy balance is defined as
</p>
<pre>
    (dU/d&tau;) = u * (dm/d&tau;) + m * (du/d&tau;) = u * (dm/d&tau;) + m * [v * (p * (&kappa; - &beta; * &mu;) - 1) * (dp/d&tau;) + (1 - p * v * &beta; / c<sub>p</sub>) * (dh/d&tau;)] = &sum; H<sub>b,flow</sub> + &sum; Q<sub>b,flow</sub>;
</pre>

<p>
Herein, <i>(dm/d&tau;)</i> is the derivative of the mass w.r.t. time, <i>(dU/d&tau;)</i> 
is the derivative of the internal energy w.r.t. time, <i>(d&rho;/d&tau;)</i> is the 
derivative of the density w.r.t. time, <i>(du/d&tau;)</i> is the derivative of the specific 
internal energy w.r.t.time, <i>(dp/d&tau;)</i> is the derivative of the pressure w.r.t. 
time, <i>(dT/d&tau;)</i> is the derivative of the temperature w.r.t. time, <i>(dh/d&tau;)</i> 
is the derivative of the specific enthalpy w.r.t. time,  <i>V</i> is the volume, <i>m</i> is 
the mass, <i>v</i> is the specific volume, <i>&rho;</i> is the density, <i>&kappa;</i> is 
the isothermal compressibility, <i>&beta;</i> is the isobaric expansion coefficient, <i>&mu;</i> 
is the Joule-Thomson coefficient, <i>c<sub>p</sub></i> is the specific heat capacity at 
constant pressure,<i>m<sub>c,flow</sub></i> is the sum of the convective mass flow rates, 
<i>H<sub>b,flow</sub></i> is the sum of the enthalpy flow rates, and <i>Q<sub>b,flow</sub></i> 
is the sum of the heat flow rates.
</p>

<h4>Main equations for an ideal gas</h4>
<p>
For an ideal gas, the isothermal compressibility <i>&kappa;</i> is <i>1/p</i> and the
isbaric expansion coefficient <i>&beta;</i> is <i>1/T</i>, leading to a Joule-Thomson 
coefficient <i>&mu;</i> of zero. Hence, the mass and energy balances can be simplified. 
When using the pressure <i>p</i> and temperature <i>T</i> as independent states, the mass 
balance still reads as
</p>
<pre>
    (dm/d&tau;) = V * &rho; * [&kappa; * (dp/d&tau;) - &beta; * (dT/d&tau;)] = &sum; m<sub>c,flow</sub>;
</pre>
<p>
but the energy balance now reads as
</p>
<pre>
    (dU/d&tau;) = u * (dm/d&tau;) + m * (c<sub>p</sub> - p * v * &beta;) * (dT/d&tau;) = &sum; H<sub>b,flow</sub> + &sum; Q<sub>b,flow</sub>;
</pre>

<p>
When using the pressure <i>p</i> and specific enthalpy <i>h</i> as independent 
states, the mass balance now reads as
</p>
<pre>
    (dm/d&tau;) = V * &rho; * [&kappa; * (dp/d&tau;) - &beta; / c<sub>p</sub> * (dh/d&tau;)] = &sum; m<sub>c,flow</sub>;
</pre>
<p>
and the energy balance now reads as
</p>
<pre>
    (dU/d&tau;) = u * (dm/d&tau;) + m * (1 - p * v * &beta; / c<sub>p</sub>) * (dh/d&tau;) = &sum; H<sub>b,flow</sub> + &sum; Q<sub>b,flow</sub>;
</pre>

<h4>Assumptions and limitations</h4>
<ul>
  <li>
  Constant volume <i>V</i>
  </li>
  <li>
  Homogenoues properties within the volume
  </li>
  <li>
  Ideal gas
  </li>
</ul>

<h4>Typical use</h4>
<p>
This model is typically used to model the adsorptive of open adsorption systems.
</p>

<h4>Important parameters and options</h4>
<ul>
  <li>
  <i>useHeatPorts</i>:
  Defines if heat ports in the spatial direction <i>i</i> are required.
  </li>
  <li>
  <li>
  <i>independentStateVariables</i>:
  Defines independent state variables.
  </li>
  <li>
  <i>idealGas</i>:
  Defines if the medium is an ideal gas and, thus, the governing equations can
  be simplified.
  </li>
  <li>
  <i>calculateAdditionalProperties</i>:
  Defines if additional properties like transport properties shall be calculated.
  </li>
  <br/>
  <li>
  <i>type_overallMassBalance</i>:
  Defines the type of the overall mass balance.
  </li>
  <li>
  <i>type_energyBalance</i>:
  Defines the type of the energy balance.
  </li>
  <br/>
  <li>
  <i>avoid_events</i>:
  Defines if events shall be avoided via the noEvent()-operator.
  </li>
</ul>
<p>
Note that not all combinations of govering equation types are reasonable. Typically,
a transient mass balance is combined with a transient energy balance.
</p>

<h4>Dynamics</h4>
<p>
This model has two dynamic state that can be selected (see options):
</p>
<ul>
  <li>
  Pressure <i>p</i> and temperature <i>T</i> (recommended), or
  </li>
  <li>
  pressure <i>p</i> and specific enthalpy <i>h</i>.
  </li>
</ul>
<p>
Note that this model does not have the pressure <i>p</i> as dynamic state if the
fluid property model is a single state model.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 11, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end GasVolume;
