within SorpLib.Basics.Volumes.FluidVolumes.Tester;
model Test_GasMixtureVolume "Tester for gas mixture volume"
  extends Modelica.Icons.Example;

  //
  // Definition of parameters
  //
  replaceable package Medium =
    SorpLib.Media.IdealGasMixtures.DryAir_N2_O2_CO2_H2O
    constrainedby
    SorpLib.Media.IdealGasMixtures.Interfaces.PartialIdealGasMixture
    "Medium model of the ideal gas mixture"
    annotation (Dialog(tab="General", group="Media"),
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

  //
  // Definition of liquid models
  //
  SorpLib.Basics.Volumes.FluidVolumes.GasMixtureVolume
    gas_pTX_transientMassBalance_transientEnergyBalance(
    no_adsorptivs=2,
    ind_adsorptivs={3,4},
    nPorts_cfp_xMinus=1,
    nPorts_cfp_xPlus=1,
    p_initial=25000,
    T_initial=293.15,
    independentStateVariables=SorpLib.Choices.IndependentVariablesVolume.pTX,
    type_energyBalance=SorpLib.Choices.BalanceEquations.TransientFixedInitial,
    type_overallMassBalance=SorpLib.Choices.BalanceEquations.TransientFixedInitial,
    type_independentMassBalances=SorpLib.Choices.BalanceEquations.TransientFixedInitial,
    redeclare final package Medium = Medium) "Gas-mixture volume with p, T, and X as independent states, transient mass 
    balance with fixed initial values, and transient mass balance with fixed 
    initial values"
    annotation (Placement(transformation(extent={{-60,-10},{-40,10}})));

  SorpLib.Basics.Volumes.FluidVolumes.GasMixtureVolume
    gas_phX_transientMassBalance_transientEnergyBalance(
    independentStateVariables=SorpLib.Choices.IndependentVariablesVolume.phX,
    no_adsorptivs=2,
    ind_adsorptivs={3,4},
    nPorts_cfp_xMinus=1,
    nPorts_cfp_xPlus=1,
    p_initial=25000,
    h_initial=295427,
    type_energyBalance=SorpLib.Choices.BalanceEquations.TransientFixedInitial,
    type_overallMassBalance=SorpLib.Choices.BalanceEquations.TransientFixedInitial,
    type_independentMassBalances=SorpLib.Choices.BalanceEquations.TransientFixedInitial,
    redeclare final package Medium = Medium) "Gas-mixture volume with p, h, and X as independent states, transient mass 
    balance with fixed initial values, and transient mass balance with fixed 
    initial values"
    annotation (Placement(transformation(extent={{40,-10},{60,10}})));

  //
  // Definition of fluid boundaries
  //
protected
  SorpLib.Basics.Sources.Fluids.GasSource
    inlet_pTX_transientMassBalance_transientEnergyBalance(
      boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
      boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
      m_flow_fixed=-0.001,
      T_fixed=323.15,
      redeclare package Medium = Medium) "Inlet for Gas-mixture volume"
    annotation (Placement(transformation(extent={{-72,-10},{-52,10}})));
  SorpLib.Basics.Sources.Fluids.GasSource
    outlet_pTX_transientMassBalance_transientEnergyBalance(
      boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
      boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
      m_flow_fixed=1e-3,
      T_fixed=323.15,
      redeclare package Medium = Medium)
    "Outlet for Gas-mixture volume"
    annotation (Placement(transformation(extent={{-28,-10},{-48,10}})));
  SorpLib.Basics.Sources.Fluids.GasSource
    inlet_pTX_transientMassBalance_transientEnergyBalance_ads1(
      boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
      boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    m_flow_fixed=-1e-6,
    T_fixed=393.15,
      redeclare package Medium = Media.IdealGases.CO2)
    "Inlet for Gas-mixture volume"
    annotation (Placement(transformation(extent={{-76,-12},{-56,8}})));
  SorpLib.Basics.Sources.Fluids.GasSource
    inlet_pTX_transientMassBalance_transientEnergyBalance_ads2(
      boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
      boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    m_flow_fixed=-1e-5,
    T_fixed=393.15,
    redeclare package Medium = Media.IdealGases.H2O)
    "Inlet for Gas-mixture volume"
    annotation (Placement(transformation(extent={{-80,-14},{-60,6}})));

  SorpLib.Basics.Sources.Fluids.GasSource
    inlet_phX_transientMassBalance_transientEnergyBalance(
      boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
      boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
      m_flow_fixed=-0.001,
      T_fixed=323.15,
      redeclare package Medium = Medium)
    "Inlet for Gas-mixture volume"
    annotation (Placement(transformation(extent={{28,-10},{48,10}})));
  SorpLib.Basics.Sources.Fluids.GasSource
    outlet_phX_transientMassBalance_transientEnergyBalance(
      boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
      boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
      m_flow_fixed=1e-3,
      T_fixed=323.15,
      redeclare package Medium = Medium)
    "Outlet for Gas-mixture volume"
    annotation (Placement(transformation(extent={{72,-10},{52,10}})));
  SorpLib.Basics.Sources.Fluids.GasSource
    inlet_phX_transientMassBalance_transientEnergyBalance_ads1(
      boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
      boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    m_flow_fixed=-1e-6,
    T_fixed=393.15,
      redeclare package Medium = Media.IdealGases.CO2)
    "Inlet for Gas-mixture volume"
    annotation (Placement(transformation(extent={{24,-12},{44,8}})));
  SorpLib.Basics.Sources.Fluids.GasSource
    inlet_phX_transientMassBalance_transientEnergyBalance_ads2(
      boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
      boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    m_flow_fixed=-1e-5,
    T_fixed=393.15,
      redeclare package Medium = Media.IdealGases.H2O)
    "Inlet for Gas-mixture volume"
    annotation (Placement(transformation(extent={{20,-14},{40,6}})));

  //
  // Definition of thermal boundaries
  //
  SorpLib.Basics.Sources.Thermal.HeatSource
    heatSource_pTX_transientMassBalance_transientEnergyBalance(
      boundaryType=SorpLib.Choices.BoundaryThermal.HeatFlowRate,
      Q_flow_fixed=-25)
    "Heat source for Gas-mixture volume"
    annotation (Placement(transformation(extent={{-10,-10},{10,10}}, rotation=90,
                origin={-40,-12})));

  SorpLib.Basics.Sources.Thermal.HeatSource
    heatSource_phX_transientMassBalance_transientEnergyBalance(
      boundaryType=SorpLib.Choices.BoundaryThermal.HeatFlowRate,
      Q_flow_fixed=-25)
    "Heat source for Gas-mixture volume"
    annotation (Placement(transformation(extent={{-10,-10},{10,10}}, rotation=90,
                origin={60,-12})));

equation
  //
  // Connections
  //
  connect(inlet_pTX_transientMassBalance_transientEnergyBalance.port,
    gas_pTX_transientMassBalance_transientEnergyBalance.cfp_xMinus[1])
    annotation (Line(
      points={{-62,0},{-60,0},{-60,1.8},{-54.2,1.8}},
      color={244,125,35},
      thickness=1));
  connect(outlet_pTX_transientMassBalance_transientEnergyBalance.port,
    gas_pTX_transientMassBalance_transientEnergyBalance.cfp_xPlus[1])
    annotation (Line(
      points={{-38,0},{-40,0},{-40,1.8},{-42.2,1.8}},
      color={244,125,35},
      thickness=1));
  connect(inlet_phX_transientMassBalance_transientEnergyBalance.port,
    gas_phX_transientMassBalance_transientEnergyBalance.cfp_xMinus[1])
    annotation (Line(
      points={{38,0},{40,0},{40,1.8},{45.8,1.8}},
      color={244,125,35},
      thickness=1));
  connect(outlet_phX_transientMassBalance_transientEnergyBalance.port,
    gas_phX_transientMassBalance_transientEnergyBalance.cfp_xPlus[1])
    annotation (Line(
      points={{62,0},{60,0},{60,1.8},{57.8,1.8}},
      color={244,125,35},
      thickness=1));
  connect(inlet_pTX_transientMassBalance_transientEnergyBalance_ads1.port,
    gas_pTX_transientMassBalance_transientEnergyBalance.fp_sorption[1])
    annotation (Line(
      points={{-66,-2},{-64,-2},{-64,-10},{-51.6,-10},{-51.6,-7.8}},
      color={244,125,35},
      thickness=1));
  connect(inlet_pTX_transientMassBalance_transientEnergyBalance_ads2.port,
    gas_pTX_transientMassBalance_transientEnergyBalance.fp_sorption[2])
    annotation (Line(
      points={{-70,-4},{-68,-4},{-68,-12},{-51.6,-12},{-51.6,-7.4}},
      color={244,125,35},
      thickness=1));
  connect(inlet_phX_transientMassBalance_transientEnergyBalance_ads1.port,
    gas_phX_transientMassBalance_transientEnergyBalance.fp_sorption[1])
    annotation (Line(
      points={{34,-2},{36,-2},{36,-10},{48.4,-10},{48.4,-7.8}},
      color={244,125,35},
      thickness=1));
  connect(inlet_phX_transientMassBalance_transientEnergyBalance_ads2.port,
    gas_phX_transientMassBalance_transientEnergyBalance.fp_sorption[2])
    annotation (Line(
      points={{30,-4},{32,-4},{32,-12},{48.4,-12},{48.4,-7.4}},
      color={244,125,35},
      thickness=1));

  connect(heatSource_pTX_transientMassBalance_transientEnergyBalance.port,
    gas_pTX_transientMassBalance_transientEnergyBalance.hp_yMinus)
    annotation (Line(
      points={{-40,-12},{-40,-8},{-50,-8},{-50,-6}},
      color={238,46,47},
      thickness=1));
  connect(heatSource_phX_transientMassBalance_transientEnergyBalance.port,
    gas_phX_transientMassBalance_transientEnergyBalance.hp_yMinus)
    annotation (Line(
      points={{60,-12},{60,-8},{50,-8},{50,-6}},
      color={238,46,47},
      thickness=1));

  //
  // Annotations
  //
  annotation (experiment(StopTime=25), Documentation(info="<html>
<p>
This model checks the gas mixture volume model.
<br/><br/>
To see the model behavior, plot the variables of the models over the 
time. The simulation time is correctly preset (Start: 0 s, Stop = 25 s). 
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 12, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Test_GasMixtureVolume;
