within SorpLib.Basics.Volumes.FluidVolumes.Tester;
model Test_GasVaporMixtureVolume "Tester for gas-vapor-mixture volume"
  extends Modelica.Icons.Example;

  //
  // Definition of parameters
  //
  replaceable package Medium =
    SorpLib.Media.IdealGasVaporMixtures.MoistAir_N2_O2_CO2_H2O
    constrainedby
    SorpLib.Media.IdealGasVaporMixtures.Interfaces.PartialIdealGasWaterVaporMixture
    "Medium model of the ideal gas-vapor mixture"
    annotation (Dialog(tab="General", group="Media"),
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

  //
  // Definition of liquid models
  //
  SorpLib.Basics.Volumes.FluidVolumes.GasVaporMixtureVolume
    gas_pTX_transientMassBalance_transientEnergyBalance(
    no_adsorptivs=2,
    ind_adsorptivs={1,3},
    idealGasVaporMixture=false,
    nPorts_cfp_xMinus=1,
    nPorts_cfp_xPlus=1,
    p_initial=15000,
    T_initial=293.15,
    independentStateVariables=SorpLib.Choices.IndependentVariablesVolume.pTX,
    type_energyBalance=SorpLib.Choices.BalanceEquations.TransientFixedInitial,
    type_overallMassBalance=SorpLib.Choices.BalanceEquations.TransientFixedInitial,
    type_independentMassBalances=SorpLib.Choices.BalanceEquations.TransientFixedInitial,
    redeclare final package Medium = Medium) "Gas-mixture volume with p, T, and X as independent states, transient mass 
    balance with fixed initial values, and transient mass balance with fixed 
    initial values"
    annotation (Placement(transformation(extent={{-60,-10},{-40,10}})));

  SorpLib.Basics.Volumes.FluidVolumes.GasVaporMixtureVolume
    gas_phX_transientMassBalance_transientEnergyBalance(
    independentStateVariables=SorpLib.Choices.IndependentVariablesVolume.phX,
    no_adsorptivs=2,
    ind_adsorptivs={1,3},
    idealGasVaporMixture=false,
    nPorts_cfp_xMinus=1,
    nPorts_cfp_xPlus=1,
    p_initial=15000,
    h_initial=20223.1,
    type_energyBalance=SorpLib.Choices.BalanceEquations.TransientFixedInitial,
    type_overallMassBalance=SorpLib.Choices.BalanceEquations.TransientFixedInitial,
    type_independentMassBalances=SorpLib.Choices.BalanceEquations.TransientFixedInitial,
    redeclare final package Medium = Medium) "Gas-mixture volume with p, h, and X as independent states, transient mass 
    balance with fixed initial values, and transient mass balance with fixed 
    initial values"
    annotation (Placement(transformation(extent={{40,-10},{60,10}})));

  //
  // Definition of fluid boundaries
  //
protected
  Modelica.Blocks.Sources.Sine input_m_N2_flow(
    amplitude=1e-5,
    f=1/5)
    "Input signal for mass flow rate of N2"
    annotation (Placement(transformation(extent={{-100,60},{-80,80}})));
  Modelica.Blocks.Sources.Sine input_m_CO2_flow(
    amplitude=1e-8,
    f=1/5)
    "Input signal for mass flow rate of CO2"
    annotation (Placement(transformation(extent={{-100,20},{-80,40}})));
  Modelica.Blocks.Sources.Ramp input_m_flow(
    height=-1e-4,
    duration=15,
    offset=-1e-3,
    startTime=10)
    "Input signal for mass flow rate"
    annotation (Placement(transformation(extent={{100,10},{80,30}})));
  Modelica.Blocks.Sources.Sine input_T(
    amplitude=40,
    f=1/5,
    offset=273.15 + 50)
    "Input signal for temperature"
    annotation (Placement(transformation(extent={{100,50},{80,70}})));

  SorpLib.Basics.Sources.Fluids.GasVaporMixtureSource
    inlet_pTX_transientMassBalance_transientEnergyBalance(
      boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
      boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    boundaryTypeStreamMassFractions=SorpLib.Choices.BoundaryFluidStreamMassFractions.MassFractions,
    use_mFlowInput=true,
      m_flow_fixed=-0.001,
    use_TInput=true,
    T_fixed=323.15,
    X_fixed={0.6,0.1,0.1,0.2},
      redeclare package Medium = Medium) "Inlet for Gas-mixture volume"
    annotation (Placement(transformation(extent={{-72,-10},{-52,10}})));
  SorpLib.Basics.Sources.Fluids.GasVaporMixtureSource
    outlet_pTX_transientMassBalance_transientEnergyBalance(
      boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
      boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
      m_flow_fixed=1e-3,
      T_fixed=323.15,
      redeclare package Medium = Medium)
    "Outlet for Gas-mixture volume"
    annotation (Placement(transformation(extent={{-28,-10},{-48,10}})));
  SorpLib.Basics.Sources.Fluids.GasSource
    inlet_pTX_transientMassBalance_transientEnergyBalance_ads1(
      boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
      boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    use_mFlowInput=true,
    m_flow_fixed=-1e-6,
    T_fixed=393.15,
      redeclare package Medium = Media.IdealGases.N2)
    "Inlet for Gas-mixture volume"
    annotation (Placement(transformation(extent={{-82,-12},{-62,8}})));
  SorpLib.Basics.Sources.Fluids.GasSource
    inlet_pTX_transientMassBalance_transientEnergyBalance_ads2(
      boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
      boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    use_mFlowInput=true,
    m_flow_fixed=-1e-5,
    T_fixed=393.15,
    redeclare package Medium = Media.IdealGases.CO2)
    "Inlet for Gas-mixture volume"
    annotation (Placement(transformation(extent={{-86,-14},{-66,6}})));

  SorpLib.Basics.Sources.Fluids.GasVaporMixtureSource
    inlet_phX_transientMassBalance_transientEnergyBalance(
      boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
      boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    boundaryTypeStreamMassFractions=SorpLib.Choices.BoundaryFluidStreamMassFractions.MassFractions,
    use_mFlowInput=true,
      m_flow_fixed=-0.001,
    use_TInput=true,
    T_fixed=323.15,
    X_fixed={0.6,0.1,0.1,0.2},
      redeclare package Medium = Medium)
    "Inlet for Gas-mixture volume"
    annotation (Placement(transformation(extent={{28,-10},{48,10}})));
  SorpLib.Basics.Sources.Fluids.GasVaporMixtureSource
    outlet_phX_transientMassBalance_transientEnergyBalance(
      boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
      boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
      m_flow_fixed=1e-3,
      T_fixed=323.15,
      redeclare package Medium = Medium)
    "Outlet for Gas-mixture volume"
    annotation (Placement(transformation(extent={{72,-10},{52,10}})));
  SorpLib.Basics.Sources.Fluids.GasSource
    inlet_phX_transientMassBalance_transientEnergyBalance_ads1(
      boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
      boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    use_mFlowInput=true,
    m_flow_fixed=-1e-6,
    T_fixed=393.15,
      redeclare package Medium = Media.IdealGases.N2)
    "Inlet for Gas-mixture volume"
    annotation (Placement(transformation(extent={{18,-12},{38,8}})));
  SorpLib.Basics.Sources.Fluids.GasSource
    inlet_phX_transientMassBalance_transientEnergyBalance_ads2(
      boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
      boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    use_mFlowInput=true,
    m_flow_fixed=-1e-5,
    T_fixed=393.15,
      redeclare package Medium = Media.IdealGases.CO2)
    "Inlet for Gas-mixture volume"
    annotation (Placement(transformation(extent={{14,-14},{34,6}})));

  //
  // Definition of thermal boundaries
  //
  Modelica.Blocks.Sources.Sine input_Q_flow(amplitude=25, f=1/5)
    "Input signal for heat flow rate"
    annotation (Placement(transformation(extent={{-100,-40},{-80,-20}})));

  SorpLib.Basics.Sources.Thermal.HeatSource
    heatSource_pTX_transientMassBalance_transientEnergyBalance(
      boundaryType=SorpLib.Choices.BoundaryThermal.HeatFlowRate,
    use_QFlowInput=true,
      Q_flow_fixed=-25)
    "Heat source for Gas-mixture volume"
    annotation (Placement(transformation(extent={{-10,-10},{10,10}}, rotation=90,
                origin={-40,-12})));

  SorpLib.Basics.Sources.Thermal.HeatSource
    heatSource_phX_transientMassBalance_transientEnergyBalance(
      boundaryType=SorpLib.Choices.BoundaryThermal.HeatFlowRate,
    use_QFlowInput=true,
      Q_flow_fixed=-25)
    "Heat source for Gas-mixture volume"
    annotation (Placement(transformation(extent={{-10,-10},{10,10}}, rotation=90,
                origin={60,-12})));

equation
  //
  // Connections
  //
  connect(inlet_pTX_transientMassBalance_transientEnergyBalance.port,
    gas_pTX_transientMassBalance_transientEnergyBalance.cfp_xMinus[1])
    annotation (Line(
      points={{-62,0},{-60,0},{-60,1.8},{-54.2,1.8}},
      color={244,125,35},
      thickness=1));
  connect(outlet_pTX_transientMassBalance_transientEnergyBalance.port,
    gas_pTX_transientMassBalance_transientEnergyBalance.cfp_xPlus[1])
    annotation (Line(
      points={{-38,0},{-40,0},{-40,1.8},{-42.2,1.8}},
      color={244,125,35},
      thickness=1));
  connect(inlet_phX_transientMassBalance_transientEnergyBalance.port,
    gas_phX_transientMassBalance_transientEnergyBalance.cfp_xMinus[1])
    annotation (Line(
      points={{38,0},{40,0},{40,1.8},{45.8,1.8}},
      color={244,125,35},
      thickness=1));
  connect(outlet_phX_transientMassBalance_transientEnergyBalance.port,
    gas_phX_transientMassBalance_transientEnergyBalance.cfp_xPlus[1])
    annotation (Line(
      points={{62,0},{60,0},{60,1.8},{57.8,1.8}},
      color={244,125,35},
      thickness=1));
  connect(inlet_pTX_transientMassBalance_transientEnergyBalance_ads1.port,
    gas_pTX_transientMassBalance_transientEnergyBalance.fp_sorption[1])
    annotation (Line(
      points={{-72,-2},{-70,-2},{-70,-10},{-51.6,-10},{-51.6,-7.8}},
      color={244,125,35},
      thickness=1));
  connect(inlet_pTX_transientMassBalance_transientEnergyBalance_ads2.port,
    gas_pTX_transientMassBalance_transientEnergyBalance.fp_sorption[2])
    annotation (Line(
      points={{-76,-4},{-74,-4},{-74,-12},{-51.6,-12},{-51.6,-7.4}},
      color={244,125,35},
      thickness=1));
  connect(inlet_phX_transientMassBalance_transientEnergyBalance_ads1.port,
    gas_phX_transientMassBalance_transientEnergyBalance.fp_sorption[1])
    annotation (Line(
      points={{28,-2},{30,-2},{30,-10},{48.4,-10},{48.4,-7.8}},
      color={244,125,35},
      thickness=1));
  connect(inlet_phX_transientMassBalance_transientEnergyBalance_ads2.port,
    gas_phX_transientMassBalance_transientEnergyBalance.fp_sorption[2])
    annotation (Line(
      points={{24,-4},{26,-4},{26,-12},{48.4,-12},{48.4,-7.4}},
      color={244,125,35},
      thickness=1));

  connect(heatSource_pTX_transientMassBalance_transientEnergyBalance.port,
    gas_pTX_transientMassBalance_transientEnergyBalance.hp_yMinus)
    annotation (Line(
      points={{-40,-12},{-40,-8},{-50,-8},{-50,-6}},
      color={238,46,47},
      thickness=1));
  connect(heatSource_phX_transientMassBalance_transientEnergyBalance.port,
    gas_phX_transientMassBalance_transientEnergyBalance.hp_yMinus)
    annotation (Line(
      points={{60,-12},{60,-8},{50,-8},{50,-6}},
      color={238,46,47},
      thickness=1));

  connect(input_Q_flow.y,
    heatSource_pTX_transientMassBalance_transientEnergyBalance.Q_flow_input)
    annotation (Line(points={{-79,-30},{-35,-30},{-35,-13}}, color={0,0,127}));
  connect(input_Q_flow.y,
    heatSource_phX_transientMassBalance_transientEnergyBalance.Q_flow_input)
    annotation (Line(points={{-79,-30},{65,-30},{65,-13}}, color={0,0,127}));
  connect(input_m_N2_flow.y,
    inlet_pTX_transientMassBalance_transientEnergyBalance_ads1.m_flow_input)
    annotation (Line(points={{-79,70},{-73.2,70},{-73.2,0}}, color={0,0,127}));
  connect(input_m_N2_flow.y,
    inlet_phX_transientMassBalance_transientEnergyBalance_ads1.m_flow_input)
    annotation (Line(points={{-79,70},{26.8,70},{26.8,0}}, color={0,0,127}));
  connect(input_m_CO2_flow.y,
    inlet_pTX_transientMassBalance_transientEnergyBalance_ads2.m_flow_input)
    annotation (Line(points={{-79,30},{-77.2,30},{-77.2,-2}}, color={0,0,127}));
  connect(input_m_CO2_flow.y,
    inlet_phX_transientMassBalance_transientEnergyBalance_ads2.m_flow_input)
    annotation (Line(points={{-79,30},{22.8,30},{22.8,-2}}, color={0,0,127}));
  connect(input_m_flow.y, inlet_phX_transientMassBalance_transientEnergyBalance.m_flow_input)
    annotation (Line(points={{79,20},{36.8,20},{36.8,2}}, color={0,0,127}));
  connect(input_m_flow.y, inlet_pTX_transientMassBalance_transientEnergyBalance.m_flow_input)
    annotation (Line(points={{79,20},{-63.2,20},{-63.2,2}}, color={0,0,127}));
  connect(input_T.y, inlet_phX_transientMassBalance_transientEnergyBalance.T_input)
    annotation (Line(points={{79,60},{34,60},{34,-2},{36.8,-2}}, color={0,0,127}));
  connect(input_T.y, inlet_pTX_transientMassBalance_transientEnergyBalance.T_input)
    annotation (Line(points={{79,60},{-66,60},{-66,-2},{-63.2,-2}}, color={0,0,127}));
  //
  // Annotations
  //
  annotation (experiment(
      StopTime=25,
      Tolerance=1e-06),     Documentation(info="<html>
<p>
This model checks the gas-vapor-mixture volume model.
<br/><br/>
To see the model behavior, plot the variables of the models over the 
time. The simulation time is correctly preset (Start: 0 s, Stop = 25 s). 
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 13, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Test_GasVaporMixtureVolume;
