within SorpLib.Basics.Volumes.FluidVolumes.Tester;
model Test_LiquidVolume "Tester for liquid volume"
  extends Modelica.Icons.Example;

  //
  // Definition of parameters
  //
  replaceable package Medium = Modelica.Media.Water.WaterIF97_R1pT
    constrainedby Modelica.Media.Interfaces.PartialMedium
    "Medium model of the (ideal) liquid"
    annotation (Dialog(tab="General", group="Media"),
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

  //
  // Definition of liquid models
  //
  SorpLib.Basics.Volumes.FluidVolumes.LiquidVolume
    liquid_pTX_transientMassBalance_transientEnergyBalance(
    nPorts_cfp_xMinus=1,
    nPorts_cfp_xPlus=1,
    T_initial=293.15,
    geometry(V=Modelica.Constants.pi/4*0.01^2*1),
    independentStateVariables=SorpLib.Choices.IndependentVariablesVolume.pTX,
    type_energyBalance=SorpLib.Choices.BalanceEquations.TransientFixedInitial,
    type_overallMassBalance=SorpLib.Choices.BalanceEquations.TransientFixedInitial,
    redeclare final package Medium = Medium) "Liquid volume with p, T, and X as independent states, transient mass 
    balance with fixed initial values, and transient mass balance with fixed 
    initial values"
    annotation (Placement(transformation(extent={{-60,70},{-40,90}})));

  SorpLib.Basics.Volumes.FluidVolumes.LiquidVolume
    liquid_pTX_steadyStateMassBalance_transientEnergyBalance(
    nPorts_cfp_xMinus=1,
    nPorts_cfp_xPlus=1,
    T_initial=293.15,
    geometry(V=Modelica.Constants.pi/4*0.01^2*1),
    independentStateVariables=SorpLib.Choices.IndependentVariablesVolume.pTX,
    type_energyBalance=SorpLib.Choices.BalanceEquations.TransientFixedInitial,
    type_overallMassBalance=SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial,
    redeclare final package Medium = Medium) "Liquid volume with p, T, and X as independent states, steady-state mass 
    balance with free initial values, and transient mass balance with fixed 
    initial values"
    annotation (Placement(transformation(extent={{-60,40},{-40,60}})));

  SorpLib.Basics.Volumes.FluidVolumes.LiquidVolume
    liquid_pTX_steadyStateMassBalance_steadyStateEnergyBalance(
    nPorts_cfp_xMinus=1,
    nPorts_cfp_xPlus=1,
    T_initial=293.15,
    geometry(V=Modelica.Constants.pi/4*0.01^2*1),
    independentStateVariables=SorpLib.Choices.IndependentVariablesVolume.pTX,
    type_energyBalance=SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial,
    type_overallMassBalance=SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial,
    redeclare final package Medium = Medium) "Liquid volume with p, T, and X as independent states, steady-state mass 
    balance with free initial values, and steady-state mass balance with free 
    initial values"
    annotation (Placement(transformation(extent={{-60,10},{-40,30}})));

  SorpLib.Basics.Volumes.FluidVolumes.LiquidVolume
    liquid_pTX_transientMassBalance_transientEnergyBalance_woVp(
    nPorts_cfp_xMinus=1,
    nPorts_cfp_xPlus=1,
    T_initial=293.15,
    geometry(V=Modelica.Constants.pi/4*0.01^2*1),
    neglectTermVp=true,
    independentStateVariables=SorpLib.Choices.IndependentVariablesVolume.pTX,
    type_energyBalance=SorpLib.Choices.BalanceEquations.TransientFixedInitial,
    type_overallMassBalance=SorpLib.Choices.BalanceEquations.TransientFixedInitial,
    redeclare final package Medium = Medium) "Liquid volume with p, T, and X as independent states, transient mass 
    balance with fixed initial values, transient mass balance with fixed 
    initial values, and assumption that u = h"
    annotation (Placement(transformation(extent={{-62,-30},{-42,-10}})));

  SorpLib.Basics.Volumes.FluidVolumes.LiquidVolume
    liquid_pTX_steadyStateMassBalance_transientEnergyBalance_woVp(
    nPorts_cfp_xMinus=1,
    nPorts_cfp_xPlus=1,
    T_initial=293.15,
    geometry(V=Modelica.Constants.pi/4*0.01^2*1),
    neglectTermVp=true,
    independentStateVariables=SorpLib.Choices.IndependentVariablesVolume.pTX,
    type_energyBalance=SorpLib.Choices.BalanceEquations.TransientFixedInitial,
    type_overallMassBalance=SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial,
    redeclare final package Medium = Medium) "Liquid volume with p, T, and X as independent states, steady-state mass 
    balance with free initial values, transient mass balance with fixed 
    initial values, and assumption that u = h"
    annotation (Placement(transformation(extent={{-62,-60},{-42,-40}})));

  SorpLib.Basics.Volumes.FluidVolumes.LiquidVolume
    liquid_pTX_steadyStateMassBalance_steadyStateEnergyBalance_woVp(
    nPorts_cfp_xMinus=1,
    nPorts_cfp_xPlus=1,
    T_initial=293.15,
    geometry(V=Modelica.Constants.pi/4*0.01^2*1),
    neglectTermVp=true,
    independentStateVariables=SorpLib.Choices.IndependentVariablesVolume.pTX,
    type_energyBalance=SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial,
    type_overallMassBalance=SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial,
    redeclare final package Medium = Medium) "Liquid volume with p, T, and X as independent states, steady-state mass 
    balance with free initial values, steady-state mass balance with free 
    initial values, and assumption that u = h"
    annotation (Placement(transformation(extent={{-62,-90},{-42,-70}})));

  SorpLib.Basics.Volumes.FluidVolumes.LiquidVolume
    liquid_phX_transientMassBalance_transientEnergyBalance(
    nPorts_cfp_xMinus=1,
    nPorts_cfp_xPlus=1,
    independentStateVariables=SorpLib.Choices.IndependentVariablesVolume.phX,
    h_initial=84011.8,
    geometry(V=Modelica.Constants.pi/4*0.01^2*1),
    type_energyBalance=SorpLib.Choices.BalanceEquations.TransientFixedInitial,
    type_overallMassBalance=SorpLib.Choices.BalanceEquations.TransientFixedInitial,
    redeclare final package Medium = Medium) "Liquid volume with p, h, and X as independent states, transient mass 
    balance with fixed initial values, and transient mass balance with fixed 
    initial values"
    annotation (Placement(transformation(extent={{40,70},{60,90}})));

  SorpLib.Basics.Volumes.FluidVolumes.LiquidVolume
    liquid_phX_steadyStateMassBalance_transientEnergyBalance(
    nPorts_cfp_xMinus=1,
    nPorts_cfp_xPlus=1,
    independentStateVariables=SorpLib.Choices.IndependentVariablesVolume.phX,
    h_initial=84011.8,
    geometry(V=Modelica.Constants.pi/4*0.01^2*1),
    type_energyBalance=SorpLib.Choices.BalanceEquations.TransientFixedInitial,
    type_overallMassBalance=SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial,
    redeclare final package Medium = Medium) "Liquid volume with p, h, and X as independent states, steady-state mass 
    balance with free initial values, and transient mass balance with fixed 
    initial values"
    annotation (Placement(transformation(extent={{40,40},{60,60}})));

  SorpLib.Basics.Volumes.FluidVolumes.LiquidVolume
    liquid_phX_steadyStateMassBalance_steadyStateEnergyBalance(
    nPorts_cfp_xMinus=1,
    nPorts_cfp_xPlus=1,
    independentStateVariables=SorpLib.Choices.IndependentVariablesVolume.phX,
    h_initial=84011.8,
    geometry(V=Modelica.Constants.pi/4*0.01^2*1),
    type_energyBalance=SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial,
    type_overallMassBalance=SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial,
    redeclare final package Medium = Medium) "Liquid volume with p, h, and X as independent states, steady-state mass 
    balance with free initial values, and steady-state mass balance with free 
    initial values"
    annotation (Placement(transformation(extent={{40,10},{60,30}})));

  SorpLib.Basics.Volumes.FluidVolumes.LiquidVolume
    liquid_phX_transientMassBalance_transientEnergyBalance_woVp(
    nPorts_cfp_xMinus=1,
    nPorts_cfp_xPlus=1,
    independentStateVariables=SorpLib.Choices.IndependentVariablesVolume.phX,
    h_initial=84011.8,
    geometry(V=Modelica.Constants.pi/4*0.01^2*1),
    neglectTermVp=true,
    type_energyBalance=SorpLib.Choices.BalanceEquations.TransientFixedInitial,
    type_overallMassBalance=SorpLib.Choices.BalanceEquations.TransientFixedInitial,
    redeclare final package Medium = Medium) "Liquid volume with p, T, and X as independent states, transient mass 
    balance with fixed initial values, transient mass balance with fixed 
    initial values, and assumption that u = h"
    annotation (Placement(transformation(extent={{40,-30},{60,-10}})));

  SorpLib.Basics.Volumes.FluidVolumes.LiquidVolume
    liquid_phX_steadyStateMassBalance_transientEnergyBalance_woVp(
    nPorts_cfp_xMinus=1,
    nPorts_cfp_xPlus=1,
    independentStateVariables=SorpLib.Choices.IndependentVariablesVolume.phX,
    h_initial=84011.8,
    geometry(V=Modelica.Constants.pi/4*0.01^2*1),
    neglectTermVp=true,
    type_energyBalance=SorpLib.Choices.BalanceEquations.TransientFixedInitial,
    type_overallMassBalance=SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial,
    redeclare final package Medium = Medium) "Liquid volume with p, T, and X as independent states, steady-state mass 
    balance with free initial values, transient mass balance with fixed 
    initial values, and assumption that u = h"
    annotation (Placement(transformation(extent={{40,-60},{60,-40}})));

  SorpLib.Basics.Volumes.FluidVolumes.LiquidVolume
    liquid_phX_steadyStateMassBalance_steadyStateEnergyBalance_woVp(
    nPorts_cfp_xMinus=1,
    nPorts_cfp_xPlus=1,
    independentStateVariables=SorpLib.Choices.IndependentVariablesVolume.phX,
    h_initial=84011.8,
    geometry(V=Modelica.Constants.pi/4*0.01^2*1),
    neglectTermVp=true,
    type_energyBalance=SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial,
    type_overallMassBalance=SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial,
    redeclare final package Medium = Medium) "Liquid volume with p, T, and X as independent states, steady-state mass 
    balance with free initial values, steady-state mass balance with free 
    initial values, and assumption that u = h"
    annotation (Placement(transformation(extent={{40,-90},{60,-70}})));

  //
  // Definition of fluid boundaries
  //
protected
  SorpLib.Basics.Sources.Fluids.LiquidSource
    inlet_pTX_transientMassBalance_transientEnergyBalance(
      boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
      boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
      m_flow_fixed=-0.001,
      T_fixed=323.15,
      redeclare package Medium = Medium)
    "Inlet for liquid volume"
    annotation (Placement(transformation(extent={{-72,70},{-52,90}})));
  SorpLib.Basics.Sources.Fluids.LiquidSource
    outlet_pTX_transientMassBalance_transientEnergyBalance(
      boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
      boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
      m_flow_fixed=0.001,
      T_fixed=323.15,
      redeclare package Medium = Medium)
    "Outlet for liquid volume"
    annotation (Placement(transformation(extent={{-28,70},{-48,90}})));

  SorpLib.Basics.Sources.Fluids.LiquidSource
    inlet_pTX_steadyStateMassBalance_transientEnergyBalance(
      boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
      boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
      m_flow_fixed=-0.001,
      T_fixed=323.15,
      redeclare package Medium = Medium)
    "Inlet for liquid volume"
    annotation (Placement(transformation(extent={{-72,40},{-52,60}})));
  SorpLib.Basics.Sources.Fluids.LiquidSource
    outlet_pTX_steadyStateMassBalance_transientEnergyBalance(
      boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
      boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
      p_fixed=1e5,
      T_fixed=323.15,
      redeclare package Medium = Medium)
    "Outlet for liquid volume"
    annotation (Placement(transformation(extent={{-28,40},{-48,60}})));

  SorpLib.Basics.Sources.Fluids.LiquidSource
    inlet_pTX_steadyStateMassBalance_steadyStateEnergyBalance(
      boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
      boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
      m_flow_fixed=-0.001,
      T_fixed=323.15,
      redeclare package Medium = Medium)
    "Inlet for liquid volume"
    annotation (Placement(transformation(extent={{-72,10},{-52,30}})));
  SorpLib.Basics.Sources.Fluids.LiquidSource
    outlet_pTX_steadyStateMassBalance_steadyStateEnergyBalance(
      boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
      boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
      p_fixed=1e5,
      T_fixed=323.15,
      redeclare package Medium = Medium)
    "Outlet for liquid volume"
    annotation (Placement(transformation(extent={{-28,10},{-48,30}})));

  SorpLib.Basics.Sources.Fluids.LiquidSource
    inlet_pTX_transientMassBalance_transientEnergyBalance_woVp(
      boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
      boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
      m_flow_fixed=-0.001,
      T_fixed=323.15,
      redeclare package Medium = Medium)
    "Inlet for liquid volume"
    annotation (Placement(transformation(extent={{-74,-30},{-54,-10}})));
  SorpLib.Basics.Sources.Fluids.LiquidSource
    outlet_pTX_transientMassBalance_transientEnergyBalance_woVp(
      boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
      boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
      m_flow_fixed=0.001,
      T_fixed=323.15,
      redeclare package Medium = Medium)
    "Outlet for liquid volume"
    annotation (Placement(transformation(extent={{-30,-30},{-50,-10}})));

  SorpLib.Basics.Sources.Fluids.LiquidSource
    inlet_pTX_steadyStateMassBalance_transientEnergyBalance_woVp(
      boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
      boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
      m_flow_fixed=-0.001,
      T_fixed=323.15,
      redeclare package Medium = Medium)
    "Inlet for liquid volume"
    annotation (Placement(transformation(extent={{-74,-60},{-54,-40}})));
  SorpLib.Basics.Sources.Fluids.LiquidSource
    outlet_pTX_steadyStateMassBalance_transientEnergyBalance_woVp(
      boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
      boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
      p_fixed=100000,
      T_fixed=323.15,
      redeclare package Medium = Medium)
    "Outlet for liquid volume"
    annotation (Placement(transformation(extent={{-30,-60},{-50,-40}})));

  SorpLib.Basics.Sources.Fluids.LiquidSource
    inlet_pTX_steadyStateMassBalance_steadyStateEnergyBalance_woVp(
      boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
      boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
      m_flow_fixed=-0.001,
      T_fixed=323.15,
      redeclare package Medium = Medium)
    "Inlet for liquid volume"
    annotation (Placement(transformation(extent={{-74,-90},{-54,-70}})));
  SorpLib.Basics.Sources.Fluids.LiquidSource
    outlet_pTX_steadyStateMassBalance_steadyStateEnergyBalance_woVp(
      boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
      boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
      p_fixed=100000,
      T_fixed=323.15,
      redeclare package Medium = Medium)
    "Outlet for liquid volume"
    annotation (Placement(transformation(extent={{-30,-90},{-50,-70}})));

  SorpLib.Basics.Sources.Fluids.LiquidSource
    inlet_phX_transientMassBalance_transientEnergyBalance(
      boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
      boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
      m_flow_fixed=-0.001,
      T_fixed=323.15,
      redeclare package Medium = Medium)
    "Inlet for liquid volume"
    annotation (Placement(transformation(extent={{28,70},{48,90}})));
  SorpLib.Basics.Sources.Fluids.LiquidSource
    outlet_phX_transientMassBalance_transientEnergyBalance(
      boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
      boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
      m_flow_fixed=0.001,
      T_fixed=323.15,
      redeclare package Medium = Medium)
    "Outlet for liquid volume"
    annotation (Placement(transformation(extent={{72,70},{52,90}})));

  SorpLib.Basics.Sources.Fluids.LiquidSource
    inlet_phX_steadyStateMassBalance_transientEnergyBalance(
      boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
      boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
      m_flow_fixed=-0.001,
      T_fixed=323.15,
      redeclare package Medium = Medium)
    "Inlet for liquid volume"
    annotation (Placement(transformation(extent={{28,40},{48,60}})));
  SorpLib.Basics.Sources.Fluids.LiquidSource
    outlet_phX_steadyStateMassBalance_transientEnergyBalance(
      boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
      boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
      p_fixed=1e5,
      T_fixed=323.15,
      redeclare package Medium = Medium)
    "Outlet for liquid volume"
    annotation (Placement(transformation(extent={{72,40},{52,60}})));

  SorpLib.Basics.Sources.Fluids.LiquidSource
    inlet_phX_steadyStateMassBalance_steadyStateEnergyBalance(
      boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
      boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
      m_flow_fixed=-0.001,
      T_fixed=323.15,
      redeclare package Medium = Medium)
    "Inlet for liquid volume"
    annotation (Placement(transformation(extent={{28,10},{48,30}})));
  SorpLib.Basics.Sources.Fluids.LiquidSource
    outlet_phX_steadyStateMassBalance_steadyStateEnergyBalance(
      boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
      boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
      p_fixed=1e5,
      T_fixed=323.15,
      redeclare package Medium = Medium)
    "Outlet for liquid volume"
    annotation (Placement(transformation(extent={{72,10},{52,30}})));

  SorpLib.Basics.Sources.Fluids.LiquidSource
    inlet_phX_transientMassBalance_transientEnergyBalance_woVp(
      boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
      boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
      m_flow_fixed=-0.001,
      T_fixed=323.15,
      redeclare package Medium = Medium)
    "Inlet for liquid volume"
    annotation (Placement(transformation(extent={{28,-30},{48,-10}})));
  SorpLib.Basics.Sources.Fluids.LiquidSource
    outlet_phX_transientMassBalance_transientEnergyBalance_woVp(
      boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
      boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
      m_flow_fixed=0.001,
      T_fixed=323.15,
      redeclare package Medium = Medium)
    "Outlet for liquid volume"
    annotation (Placement(transformation(extent={{72,-30},{52,-10}})));

  SorpLib.Basics.Sources.Fluids.LiquidSource
    inlet_phX_steadyStateMassBalance_transientEnergyBalance_woVp(
      boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
      boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
      m_flow_fixed=-0.001,
      T_fixed=323.15,
      redeclare package Medium = Medium)
    "Inlet for liquid volume"
    annotation (Placement(transformation(extent={{28,-60},{48,-40}})));
  SorpLib.Basics.Sources.Fluids.LiquidSource
    outlet_phX_steadyStateMassBalance_transientEnergyBalance_woVp(
      boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
      boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
      p_fixed=100000,
      T_fixed=323.15,
      redeclare package Medium = Medium)
    "Outlet for liquid volume"
    annotation (Placement(transformation(extent={{72,-60},{52,-40}})));

  SorpLib.Basics.Sources.Fluids.LiquidSource
    inlet_phX_steadyStateMassBalance_steadyStateEnergyBalance_woVp(
      boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
      boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
      m_flow_fixed=-0.001,
      T_fixed=323.15,
      redeclare package Medium = Medium)
    "Inlet for liquid volume"
    annotation (Placement(transformation(extent={{28,-90},{48,-70}})));
  SorpLib.Basics.Sources.Fluids.LiquidSource
    outlet_phX_steadyStateMassBalance_steadyStateEnergyBalance_woVp(
      boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
      boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
      p_fixed=100000,
      T_fixed=323.15,
      redeclare package Medium = Medium)
    "Outlet for liquid volume"
    annotation (Placement(transformation(extent={{72,-90},{52,-70}})));

  //
  // Definition of thermal boundaries
  //
  SorpLib.Basics.Sources.Thermal.HeatSource
    heatSource_pTX_transientMassBalance_transientEnergyBalance(
      boundaryType=SorpLib.Choices.BoundaryThermal.HeatFlowRate,
      Q_flow_fixed=-25)
    "Heat source for liquid volume"
    annotation (Placement(transformation(extent={{-10,-10},{10,10}}, rotation=90,
                origin={-50,68})));
  SorpLib.Basics.Sources.Thermal.HeatSource
    heatSource_pTX_steadyStateMassBalance_transientEnergyBalance(
      boundaryType=SorpLib.Choices.BoundaryThermal.HeatFlowRate,
      Q_flow_fixed=-25)
    "Heat source for liquid volume"
    annotation (Placement(transformation(extent={{-10,-10},{10,10}}, rotation=90,
                origin={-50,38})));
  SorpLib.Basics.Sources.Thermal.HeatSource
    heatSource_pTX_steadyStateMassBalance_steadyStateEnergyBalance(
      boundaryType=SorpLib.Choices.BoundaryThermal.HeatFlowRate,
      Q_flow_fixed=-25)
    "Heat source for liquid volume"
    annotation (Placement(transformation(extent={{-10,-10},{10,10}}, rotation=90,
                origin={-50,8})));

  SorpLib.Basics.Sources.Thermal.HeatSource
    heatSource_pTX_transientMassBalance_transientEnergyBalance_woVp(
      boundaryType=SorpLib.Choices.BoundaryThermal.HeatFlowRate,
      Q_flow_fixed=-25)
    "Heat source for liquid volume" annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-52,-32})));
  SorpLib.Basics.Sources.Thermal.HeatSource
    heatSource_pTX_steadyStateMassBalance_transientEnergyBalance_woVp(
      boundaryType=SorpLib.Choices.BoundaryThermal.HeatFlowRate,
      Q_flow_fixed=-25)
    "Heat source for liquid volume" annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-52,-62})));
  SorpLib.Basics.Sources.Thermal.HeatSource
    heatSource_pTX_steadyStateMassBalance_steadyStateEnergyBalance_woVp(
      boundaryType=SorpLib.Choices.BoundaryThermal.HeatFlowRate,
      Q_flow_fixed=-25)
    "Heat source for liquid volume" annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-52,-92})));

  SorpLib.Basics.Sources.Thermal.HeatSource
    heatSource_phX_transientMassBalance_transientEnergyBalance(
      boundaryType=SorpLib.Choices.BoundaryThermal.HeatFlowRate,
      Q_flow_fixed=-25)
    "Heat source for liquid volume"
    annotation (Placement(transformation(extent={{-10,-10},{10,10}}, rotation=90,
                origin={50,68})));
  SorpLib.Basics.Sources.Thermal.HeatSource
    heatSource_phX_steadyStateMassBalance_transientEnergyBalance(
      boundaryType=SorpLib.Choices.BoundaryThermal.HeatFlowRate,
      Q_flow_fixed=-25)
    "Heat source for liquid volume"
    annotation (Placement(transformation(extent={{-10,-10},{10,10}}, rotation=90,
                origin={50,38})));
  SorpLib.Basics.Sources.Thermal.HeatSource
    heatSource_phX_steadyStateMassBalance_steadyStateEnergyBalance(
      boundaryType=SorpLib.Choices.BoundaryThermal.HeatFlowRate,
      Q_flow_fixed=-25)
    "Heat source for liquid volume"
    annotation (Placement(transformation(extent={{-10,-10},{10,10}}, rotation=90,
                origin={50,8})));

  SorpLib.Basics.Sources.Thermal.HeatSource
    heatSource_phX_transientMassBalance_transientEnergyBalance_woVp(
      boundaryType=SorpLib.Choices.BoundaryThermal.HeatFlowRate,
      Q_flow_fixed=-25)
    "Heat source for liquid volume"
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={50,-32})));
  SorpLib.Basics.Sources.Thermal.HeatSource
    heatSource_phX_steadyStateMassBalance_transientEnergyBalance_woVp(
      boundaryType=SorpLib.Choices.BoundaryThermal.HeatFlowRate,
      Q_flow_fixed=-25)
    "Heat source for liquid volume"
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={50,-62})));
  SorpLib.Basics.Sources.Thermal.HeatSource
    heatSource_phX_steadyStateMassBalance_steadyStateEnergyBalance_woVp(
      boundaryType=SorpLib.Choices.BoundaryThermal.HeatFlowRate,
      Q_flow_fixed=-25)
    "Heat source for liquid volume"
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={50,-92})));

equation
  //
  // Connections
  //
  connect(inlet_pTX_transientMassBalance_transientEnergyBalance.port,
    liquid_pTX_transientMassBalance_transientEnergyBalance.cfp_xMinus[1])
    annotation (Line(
      points={{-62,80},{-60,80},{-60,81.8},{-54.2,81.8}},
      color={28,108,200},
      thickness=1));
  connect(outlet_pTX_transientMassBalance_transientEnergyBalance.port,
    liquid_pTX_transientMassBalance_transientEnergyBalance.cfp_xPlus[1])
    annotation (Line(
      points={{-38,80},{-40,80},{-40,81.8},{-42.2,81.8}},
      color={28,108,200},
      thickness=1));
  connect(liquid_pTX_steadyStateMassBalance_transientEnergyBalance.cfp_xMinus[1],
    inlet_pTX_steadyStateMassBalance_transientEnergyBalance.port) annotation (
      Line(
      points={{-54.2,51.8},{-60,51.8},{-60,50},{-62,50}},
      color={28,108,200},
      thickness=1));
  connect(outlet_pTX_steadyStateMassBalance_transientEnergyBalance.port,
    liquid_pTX_steadyStateMassBalance_transientEnergyBalance.cfp_xPlus[1])
    annotation (Line(
      points={{-38,50},{-40,50},{-40,51.8},{-42.2,51.8}},
      color={28,108,200},
      thickness=1));
  connect(liquid_pTX_steadyStateMassBalance_steadyStateEnergyBalance.cfp_xMinus[1],
    inlet_pTX_steadyStateMassBalance_steadyStateEnergyBalance.port) annotation (
     Line(
      points={{-54.2,21.8},{-60,21.8},{-60,20},{-62,20}},
      color={28,108,200},
      thickness=1));
  connect(outlet_pTX_steadyStateMassBalance_steadyStateEnergyBalance.port,
    liquid_pTX_steadyStateMassBalance_steadyStateEnergyBalance.cfp_xPlus[1])
    annotation (Line(
      points={{-38,20},{-40,20},{-40,21.8},{-42.2,21.8}},
      color={28,108,200},
      thickness=1));
  connect(inlet_pTX_transientMassBalance_transientEnergyBalance_woVp.port,
    liquid_pTX_transientMassBalance_transientEnergyBalance_woVp.cfp_xMinus[1])
    annotation (Line(
      points={{-64,-20},{-62,-20},{-62,-18.2},{-56.2,-18.2}},
      color={28,108,200},
      thickness=1));
  connect(outlet_pTX_transientMassBalance_transientEnergyBalance_woVp.port,
    liquid_pTX_transientMassBalance_transientEnergyBalance_woVp.cfp_xPlus[1])
    annotation (Line(
      points={{-40,-20},{-42,-20},{-42,-18.2},{-44.2,-18.2}},
      color={28,108,200},
      thickness=1));
  connect(liquid_pTX_steadyStateMassBalance_transientEnergyBalance_woVp.cfp_xMinus[1],
    inlet_pTX_steadyStateMassBalance_transientEnergyBalance_woVp.port)
    annotation (Line(
      points={{-56.2,-48.2},{-62,-48.2},{-62,-50},{-64,-50}},
      color={28,108,200},
      thickness=1));
  connect(outlet_pTX_steadyStateMassBalance_transientEnergyBalance_woVp.port,
    liquid_pTX_steadyStateMassBalance_transientEnergyBalance_woVp.cfp_xPlus[1])
    annotation (Line(
      points={{-40,-50},{-42,-50},{-42,-48.2},{-44.2,-48.2}},
      color={28,108,200},
      thickness=1));
  connect(liquid_pTX_steadyStateMassBalance_steadyStateEnergyBalance_woVp.cfp_xMinus[1],
    inlet_pTX_steadyStateMassBalance_steadyStateEnergyBalance_woVp.port)
    annotation (Line(
      points={{-56.2,-78.2},{-62,-78.2},{-62,-80},{-64,-80}},
      color={28,108,200},
      thickness=1));
  connect(outlet_pTX_steadyStateMassBalance_steadyStateEnergyBalance_woVp.port,
    liquid_pTX_steadyStateMassBalance_steadyStateEnergyBalance_woVp.cfp_xPlus[1])
    annotation (Line(
      points={{-40,-80},{-42,-80},{-42,-78.2},{-44.2,-78.2}},
      color={28,108,200},
      thickness=1));
  connect(inlet_phX_transientMassBalance_transientEnergyBalance.port,
    liquid_phX_transientMassBalance_transientEnergyBalance.cfp_xMinus[1])
    annotation (Line(
      points={{38,80},{40,80},{40,81.8},{45.8,81.8}},
      color={28,108,200},
      thickness=1));
  connect(outlet_phX_transientMassBalance_transientEnergyBalance.port,
    liquid_phX_transientMassBalance_transientEnergyBalance.cfp_xPlus[1])
    annotation (Line(
      points={{62,80},{60,80},{60,81.8},{57.8,81.8}},
      color={28,108,200},
      thickness=1));
  connect(inlet_phX_steadyStateMassBalance_transientEnergyBalance.port,
    liquid_phX_steadyStateMassBalance_transientEnergyBalance.cfp_xMinus[1])
    annotation (Line(
      points={{38,50},{40,50},{40,51.8},{45.8,51.8}},
      color={28,108,200},
      thickness=1));
  connect(outlet_phX_steadyStateMassBalance_transientEnergyBalance.port,
    liquid_phX_steadyStateMassBalance_transientEnergyBalance.cfp_xPlus[1])
    annotation (Line(
      points={{62,50},{60,50},{60,51.8},{57.8,51.8}},
      color={28,108,200},
      thickness=1));
  connect(inlet_phX_steadyStateMassBalance_steadyStateEnergyBalance.port,
    liquid_phX_steadyStateMassBalance_steadyStateEnergyBalance.cfp_xMinus[1])
    annotation (Line(
      points={{38,20},{40,20},{40,21.8},{45.8,21.8}},
      color={28,108,200},
      thickness=1));
  connect(outlet_phX_steadyStateMassBalance_steadyStateEnergyBalance.port,
    liquid_phX_steadyStateMassBalance_steadyStateEnergyBalance.cfp_xPlus[1])
    annotation (Line(
      points={{62,20},{60,20},{60,21.8},{57.8,21.8}},
      color={28,108,200},
      thickness=1));
  connect(inlet_phX_transientMassBalance_transientEnergyBalance_woVp.port,
    liquid_phX_transientMassBalance_transientEnergyBalance_woVp.cfp_xMinus[1])
    annotation (Line(
      points={{38,-20},{40,-20},{40,-18.2},{45.8,-18.2}},
      color={28,108,200},
      thickness=1));
  connect(outlet_phX_transientMassBalance_transientEnergyBalance_woVp.port,
    liquid_phX_transientMassBalance_transientEnergyBalance_woVp.cfp_xPlus[1])
    annotation (Line(
      points={{62,-20},{60,-20},{60,-18.2},{57.8,-18.2}},
      color={28,108,200},
      thickness=1));
  connect(inlet_phX_steadyStateMassBalance_transientEnergyBalance_woVp.port,
    liquid_phX_steadyStateMassBalance_transientEnergyBalance_woVp.cfp_xMinus[1])
    annotation (Line(
      points={{38,-50},{40,-50},{40,-48.2},{45.8,-48.2}},
      color={28,108,200},
      thickness=1));
  connect(outlet_phX_steadyStateMassBalance_transientEnergyBalance_woVp.port,
    liquid_phX_steadyStateMassBalance_transientEnergyBalance_woVp.cfp_xPlus[1])
    annotation (Line(
      points={{62,-50},{60,-50},{60,-48.2},{57.8,-48.2}},
      color={28,108,200},
      thickness=1));
  connect(inlet_phX_steadyStateMassBalance_steadyStateEnergyBalance_woVp.port,
    liquid_phX_steadyStateMassBalance_steadyStateEnergyBalance_woVp.cfp_xMinus[1])
    annotation (Line(
      points={{38,-80},{40,-80},{40,-78.2},{45.8,-78.2}},
      color={28,108,200},
      thickness=1));
  connect(outlet_phX_steadyStateMassBalance_steadyStateEnergyBalance_woVp.port,
    liquid_phX_steadyStateMassBalance_steadyStateEnergyBalance_woVp.cfp_xPlus[1])
    annotation (Line(
      points={{62,-80},{60,-80},{60,-78.2},{57.8,-78.2}},
      color={28,108,200},
      thickness=1));

  connect(heatSource_pTX_transientMassBalance_transientEnergyBalance.port,
    liquid_pTX_transientMassBalance_transientEnergyBalance.hp_yMinus)
    annotation (Line(
      points={{-50,68},{-50,74}},
      color={238,46,47},
      thickness=1));
  connect(heatSource_pTX_steadyStateMassBalance_transientEnergyBalance.port,
    liquid_pTX_steadyStateMassBalance_transientEnergyBalance.hp_yMinus)
    annotation (Line(
      points={{-50,38},{-50,38},{-50,44}},
      color={238,46,47},
      thickness=1));
  connect(heatSource_pTX_steadyStateMassBalance_steadyStateEnergyBalance.port,
    liquid_pTX_steadyStateMassBalance_steadyStateEnergyBalance.hp_yMinus)
    annotation (Line(
      points={{-50,8},{-50,14}},
      color={238,46,47},
      thickness=1));
  connect(heatSource_pTX_transientMassBalance_transientEnergyBalance_woVp.port,
    liquid_pTX_transientMassBalance_transientEnergyBalance_woVp.hp_yMinus)
    annotation (Line(
      points={{-52,-32},{-52,-26}},
      color={238,46,47},
      thickness=1));
  connect(heatSource_pTX_steadyStateMassBalance_transientEnergyBalance_woVp.port,
    liquid_pTX_steadyStateMassBalance_transientEnergyBalance_woVp.hp_yMinus)
    annotation (Line(
      points={{-52,-62},{-52,-56}},
      color={238,46,47},
      thickness=1));
  connect(heatSource_pTX_steadyStateMassBalance_steadyStateEnergyBalance_woVp.port,
    liquid_pTX_steadyStateMassBalance_steadyStateEnergyBalance_woVp.hp_yMinus)
    annotation (Line(
      points={{-52,-92},{-52,-86}},
      color={238,46,47},
      thickness=1));
  connect(heatSource_phX_steadyStateMassBalance_steadyStateEnergyBalance.port,
    liquid_phX_steadyStateMassBalance_steadyStateEnergyBalance.hp_yMinus)
    annotation (Line(
      points={{50,8},{50,14}},
      color={238,46,47},
      thickness=1));
  connect(heatSource_phX_steadyStateMassBalance_transientEnergyBalance.port,
    liquid_phX_steadyStateMassBalance_transientEnergyBalance.hp_yMinus)
    annotation (Line(
      points={{50,38},{50,44}},
      color={238,46,47},
      thickness=1));
  connect(heatSource_phX_transientMassBalance_transientEnergyBalance.port,
    liquid_phX_transientMassBalance_transientEnergyBalance.hp_yMinus)
    annotation (Line(
      points={{50,68},{50,74},{50,74}},
      color={238,46,47},
      thickness=1));
  connect(heatSource_phX_transientMassBalance_transientEnergyBalance_woVp.port,
    liquid_phX_transientMassBalance_transientEnergyBalance_woVp.hp_yMinus)
    annotation (Line(
      points={{50,-32},{50,-26},{50,-26}},
      color={238,46,47},
      thickness=1));
  connect(heatSource_phX_steadyStateMassBalance_transientEnergyBalance_woVp.port,
    liquid_phX_steadyStateMassBalance_transientEnergyBalance_woVp.hp_yMinus)
    annotation (Line(
      points={{50,-62},{50,-56}},
      color={238,46,47},
      thickness=1));
  connect(heatSource_phX_steadyStateMassBalance_steadyStateEnergyBalance_woVp.port,
    liquid_phX_steadyStateMassBalance_steadyStateEnergyBalance_woVp.hp_yMinus)
    annotation (Line(
      points={{50,-92},{50,-86}},
      color={238,46,47},
      thickness=1));

  //
  // Annotations
  //
  annotation (experiment(StopTime=25), Documentation(info="<html>
<p>
This model checks the liquid volume model.
<br/><br/>
To see the model behavior, plot the variables of the models over the 
time. The simulation time is correctly preset (Start: 0 s, Stop = 25 s). 
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 7, 2023, by Mirko Engelpracht:<br/>
  Added documentation.
  </li>
  <li>
  January 12, 2021, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Test_LiquidVolume;
