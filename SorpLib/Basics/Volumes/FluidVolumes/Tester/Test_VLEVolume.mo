within SorpLib.Basics.Volumes.FluidVolumes.Tester;
model Test_VLEVolume "Tester for VLE volume"
  extends Modelica.Icons.Example;

  //
  // Definition of parameters
  //
  replaceable package Medium = Modelica.Media.Water.StandardWater
    constrainedby Modelica.Media.Interfaces.PartialTwoPhaseMedium
    "Medium model of the VLE liquid"
    annotation (Dialog(tab="General", group="Media"),
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

  //
  // Definition of VLE models
  //
  SorpLib.Basics.Volumes.FluidVolumes.VLEVolume
    vle_pTX_transientMassBalance_transientEnergyBalance(
    nPorts_cfp_xMinus=1,
    nPorts_cfp_xPlus=1,
    p_initial=1000,
    T_initial=293.15,
    independentStateVariables=SorpLib.Choices.IndependentVariablesVolume.pTX,
    type_energyBalance=SorpLib.Choices.BalanceEquations.TransientFixedInitial,
    type_overallMassBalance=SorpLib.Choices.BalanceEquations.TransientFixedInitial,
    redeclare final package Medium = Medium) "VLE volume with p, T, and X as independent states, transient mass 
    balance with fixed initial values, and transient mass balance with fixed 
    initial values"
    annotation (Placement(transformation(extent={{-60,40},{-40,60}})));

  SorpLib.Basics.Volumes.FluidVolumes.VLEVolume
    vle_pTX_steadyStateMassBalance_transientEnergyBalance(
    nPorts_cfp_xMinus=1,
    nPorts_cfp_xPlus=1,
    T_initial=293.15,
    independentStateVariables=SorpLib.Choices.IndependentVariablesVolume.pTX,
    type_energyBalance=SorpLib.Choices.BalanceEquations.TransientFixedInitial,
    type_overallMassBalance=SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial,
    redeclare final package Medium = Medium) "VLE volume with p, T, and X as independent states, steady-state mass 
    balance with free initial values, and transient mass balance with fixed 
    initial values"
    annotation (Placement(transformation(extent={{-60,-10},{-40,10}})));

  SorpLib.Basics.Volumes.FluidVolumes.VLEVolume
    vle_pTX_steadyStateMassBalance_steadyStateEnergyBalance(
    nPorts_cfp_xMinus=1,
    nPorts_cfp_xPlus=1,
    T_initial=293.15,
    independentStateVariables=SorpLib.Choices.IndependentVariablesVolume.pTX,
    type_energyBalance=SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial,
    type_overallMassBalance=SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial,
    redeclare final package Medium = Medium) "VLE volume with p, T, and X as independent states, steady-state mass 
    balance with free initial values, and steady-state mass balance with free 
    initial values"
    annotation (Placement(transformation(extent={{-60,-60},{-40,-40}})));

  SorpLib.Basics.Volumes.FluidVolumes.VLEVolume
    vle_phX_transientMassBalance_transientEnergyBalance(
    nPorts_cfp_xMinus=1,
    nPorts_cfp_xPlus=1,
    p_initial=1000,
    h_initial=2538185.5,
    independentStateVariables=SorpLib.Choices.IndependentVariablesVolume.phX,
    type_energyBalance=SorpLib.Choices.BalanceEquations.TransientFixedInitial,
    type_overallMassBalance=SorpLib.Choices.BalanceEquations.TransientFixedInitial,
    redeclare final package Medium = Medium) "VLE volume with p, h, and X as independent states, transient mass 
    balance with fixed initial values, and transient mass balance with fixed 
    initial values"
    annotation (Placement(transformation(extent={{40,40},{60,60}})));

  SorpLib.Basics.Volumes.FluidVolumes.VLEVolume
    vle_phX_steadyStateMassBalance_transientEnergyBalance(
    nPorts_cfp_xMinus=1,
    nPorts_cfp_xPlus=1,
    h_initial=83927.11,
    independentStateVariables=SorpLib.Choices.IndependentVariablesVolume.phX,
    type_energyBalance=SorpLib.Choices.BalanceEquations.TransientFixedInitial,
    type_overallMassBalance=SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial,
    redeclare final package Medium = Medium) "VLE volume with p, h, and X as independent states, steady-state mass 
    balance with free initial values, and transient mass balance with fixed 
    initial values"
    annotation (Placement(transformation(extent={{40,-10},{60,10}})));

  SorpLib.Basics.Volumes.FluidVolumes.VLEVolume
    vle_phX_steadyStateMassBalance_steadyStateEnergyBalance(
    nPorts_cfp_xMinus=1,
    nPorts_cfp_xPlus=1,
    independentStateVariables=SorpLib.Choices.IndependentVariablesVolume.phX,
    h_initial=2616993.5,
    type_energyBalance=SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial,
    type_overallMassBalance=SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial,
    redeclare final package Medium = Medium) "VLE volume with p, h, and X as independent states, steady-state mass 
    balance with free initial values, and steady-state mass balance with free 
    initial values"
    annotation (Placement(transformation(extent={{40,-60},{60,-40}})));

  //
  // Definition of fluid boundaries
  //
protected
  SorpLib.Basics.Sources.Fluids.VLESource
    inlet_pTX_transientMassBalance_transientEnergyBalance(
      boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
      boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
      m_flow_fixed=-0.001,
      T_fixed=323.15,
      redeclare package Medium = Medium) "Inlet for VLE volume"
    annotation (Placement(transformation(extent={{-72,40},{-52,60}})));
  SorpLib.Basics.Sources.Fluids.VLESource
    outlet_pTX_transientMassBalance_transientEnergyBalance(
      boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
      boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    m_flow_fixed=1e-3,
    T_fixed=323.15,
      redeclare package Medium = Medium) "Outlet for VLE volume"
    annotation (Placement(transformation(extent={{-28,40},{-48,60}})));
  Sources.Fluids.VLESource sorption_pTX_transientMassBalance_transientEnergyBalance(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    m_flow_fixed=-0.000001,
    T_fixed=423.15,
    redeclare package Medium = Medium) "Sorption inlet for VLE volume"
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-60,38})));

  SorpLib.Basics.Sources.Fluids.VLESource
    inlet_pTX_steadyStateMassBalance_transientEnergyBalance(
      boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
      boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
      m_flow_fixed=-0.001,
      T_fixed=323.15,
      redeclare package Medium = Medium)
    "Inlet for VLE volume"
    annotation (Placement(transformation(extent={{-72,-10},{-52,10}})));
  SorpLib.Basics.Sources.Fluids.VLESource
    outlet_pTX_steadyStateMassBalance_transientEnergyBalance(
      boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
      boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    p_fixed=10000,
    T_fixed=323.15,
      redeclare package Medium = Medium)
    "Outlet for VLE volume"
    annotation (Placement(transformation(extent={{-28,-10},{-48,10}})));

  SorpLib.Basics.Sources.Fluids.VLESource
    inlet_pTX_steadyStateMassBalance_steadyStateEnergyBalance(
      boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
      boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
      m_flow_fixed=-0.001,
      T_fixed=323.15,
      redeclare package Medium = Medium)
    "Inlet for VLE volume"
    annotation (Placement(transformation(extent={{-72,-60},{-52,-40}})));
  SorpLib.Basics.Sources.Fluids.VLESource
    outlet_pTX_steadyStateMassBalance_steadyStateEnergyBalance(
      boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
      boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    p_fixed=10000,
    T_fixed=323.15,
      redeclare package Medium = Medium)
    "Outlet for VLE volume"
    annotation (Placement(transformation(extent={{-28,-60},{-48,-40}})));

  SorpLib.Basics.Sources.Fluids.VLESource
    inlet_phX_transientMassBalance_transientEnergyBalance(
      boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
      boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
      m_flow_fixed=-0.001,
      T_fixed=323.15,
      redeclare package Medium = Medium)
    "Inlet for VLE volume"
    annotation (Placement(transformation(extent={{28,40},{48,60}})));
  SorpLib.Basics.Sources.Fluids.VLESource
    outlet_phX_transientMassBalance_transientEnergyBalance(
      boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
      boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
      m_flow_fixed=0.001,
      T_fixed=323.15,
      redeclare package Medium = Medium)
    "Outlet for VLE volume"
    annotation (Placement(transformation(extent={{72,40},{52,60}})));
  SorpLib.Basics.Sources.Fluids.VLESource
    sorption_phX_transientMassBalance_transientEnergyBalance(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    m_flow_fixed=-0.000001,
    T_fixed=423.15,
    redeclare package Medium = Medium)
    "Sorption inlet for VLE volume"
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={40,38})));

  SorpLib.Basics.Sources.Fluids.VLESource
    inlet_phX_steadyStateMassBalance_transientEnergyBalance(
      boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
      boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
      m_flow_fixed=-0.001,
      T_fixed=323.15,
      redeclare package Medium = Medium)
    "Inlet for VLE volume"
    annotation (Placement(transformation(extent={{28,-10},{48,10}})));
  SorpLib.Basics.Sources.Fluids.VLESource
    outlet_phX_steadyStateMassBalance_transientEnergyBalance(
      boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
      boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    p_fixed=10000,
    T_fixed=323.15,
      redeclare package Medium = Medium)
    "Outlet for VLE volume"
    annotation (Placement(transformation(extent={{72,-10},{52,10}})));

  SorpLib.Basics.Sources.Fluids.VLESource
    inlet_phX_steadyStateMassBalance_steadyStateEnergyBalance(
      boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
      boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
      m_flow_fixed=-0.001,
      T_fixed=323.15,
      redeclare package Medium = Medium)
    "Inlet for VLE volume"
    annotation (Placement(transformation(extent={{28,-60},{48,-40}})));
  SorpLib.Basics.Sources.Fluids.VLESource
    outlet_phX_steadyStateMassBalance_steadyStateEnergyBalance(
      boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
      boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    p_fixed=10000,
    T_fixed=323.15,
      redeclare package Medium = Medium)
    "Outlet for VLE volume"
    annotation (Placement(transformation(extent={{72,-60},{52,-40}})));

  //
  // Definition of thermal boundaries
  //
  SorpLib.Basics.Sources.Thermal.HeatSource
    heatSource_pTX_transientMassBalance_transientEnergyBalance(
      boundaryType=SorpLib.Choices.BoundaryThermal.HeatFlowRate,
      Q_flow_fixed=-25) "Heat source for VLE volume"
    annotation (Placement(transformation(extent={{-10,-10},{10,10}}, rotation=90,
                origin={-40,38})));
  SorpLib.Basics.Sources.Thermal.HeatSource
    heatSource_pTX_steadyStateMassBalance_transientEnergyBalance(
      boundaryType=SorpLib.Choices.BoundaryThermal.HeatFlowRate,
      Q_flow_fixed=-25)
    "Heat source for VLE volume"
    annotation (Placement(transformation(extent={{-10,-10},{10,10}}, rotation=90,
                origin={-40,-12})));
  SorpLib.Basics.Sources.Thermal.HeatSource
    heatSource_pTX_steadyStateMassBalance_steadyStateEnergyBalance(
      boundaryType=SorpLib.Choices.BoundaryThermal.HeatFlowRate,
      Q_flow_fixed=-25)
    "Heat source for VLE volume"
    annotation (Placement(transformation(extent={{-10,-10},{10,10}}, rotation=90,
                origin={-40,-62})));

  SorpLib.Basics.Sources.Thermal.HeatSource
    heatSource_phX_transientMassBalance_transientEnergyBalance(
      boundaryType=SorpLib.Choices.BoundaryThermal.HeatFlowRate,
      Q_flow_fixed=-25)
    "Heat source for VLE volume"
    annotation (Placement(transformation(extent={{-10,-10},{10,10}}, rotation=90,
                origin={60,38})));
  SorpLib.Basics.Sources.Thermal.HeatSource
    heatSource_phX_steadyStateMassBalance_transientEnergyBalance(
      boundaryType=SorpLib.Choices.BoundaryThermal.HeatFlowRate,
      Q_flow_fixed=-25)
    "Heat source for VLE volume"
    annotation (Placement(transformation(extent={{-10,-10},{10,10}}, rotation=90,
                origin={60,-12})));
  SorpLib.Basics.Sources.Thermal.HeatSource
    heatSource_phX_steadyStateMassBalance_steadyStateEnergyBalance(
      boundaryType=SorpLib.Choices.BoundaryThermal.HeatFlowRate,
      Q_flow_fixed=-25)
    "Heat source for VLE volume"
    annotation (Placement(transformation(extent={{-10,-10},{10,10}}, rotation=90,
                origin={60,-62})));

equation
  //
  // Connections
  //
  connect(inlet_pTX_transientMassBalance_transientEnergyBalance.port,
    vle_pTX_transientMassBalance_transientEnergyBalance.cfp_xMinus[1])
    annotation (Line(
      points={{-62,50},{-60,50},{-60,51.8},{-54.2,51.8}},
      color={0,140,72},
      thickness=1));
  connect(outlet_pTX_transientMassBalance_transientEnergyBalance.port,
    vle_pTX_transientMassBalance_transientEnergyBalance.cfp_xPlus[1])
    annotation (Line(
      points={{-38,50},{-40,50},{-40,51.8},{-42.2,51.8}},
      color={0,140,72},
      thickness=1));
  connect(vle_pTX_steadyStateMassBalance_transientEnergyBalance.cfp_xMinus[1],
    inlet_pTX_steadyStateMassBalance_transientEnergyBalance.port) annotation (
      Line(
      points={{-54.2,1.8},{-60,1.8},{-60,0},{-62,0}},
      color={0,140,72},
      thickness=1));
  connect(outlet_pTX_steadyStateMassBalance_transientEnergyBalance.port,
    vle_pTX_steadyStateMassBalance_transientEnergyBalance.cfp_xPlus[1])
    annotation (Line(
      points={{-38,0},{-40,0},{-40,1.8},{-42.2,1.8}},
      color={0,140,72},
      thickness=1));
  connect(vle_pTX_steadyStateMassBalance_steadyStateEnergyBalance.cfp_xMinus[1],
    inlet_pTX_steadyStateMassBalance_steadyStateEnergyBalance.port) annotation (
     Line(
      points={{-54.2,-48.2},{-60,-48.2},{-60,-50},{-62,-50}},
      color={0,140,72},
      thickness=1));
  connect(outlet_pTX_steadyStateMassBalance_steadyStateEnergyBalance.port,
    vle_pTX_steadyStateMassBalance_steadyStateEnergyBalance.cfp_xPlus[1])
    annotation (Line(
      points={{-38,-50},{-40,-50},{-40,-48.2},{-42.2,-48.2}},
      color={0,140,72},
      thickness=1));
  connect(inlet_phX_transientMassBalance_transientEnergyBalance.port,
    vle_phX_transientMassBalance_transientEnergyBalance.cfp_xMinus[1])
    annotation (Line(
      points={{38,50},{40,50},{40,51.8},{45.8,51.8}},
      color={0,140,72},
      thickness=1));
  connect(outlet_phX_transientMassBalance_transientEnergyBalance.port,
    vle_phX_transientMassBalance_transientEnergyBalance.cfp_xPlus[1])
    annotation (Line(
      points={{62,50},{60,50},{60,51.8},{57.8,51.8}},
      color={0,140,72},
      thickness=1));
  connect(inlet_phX_steadyStateMassBalance_transientEnergyBalance.port,
    vle_phX_steadyStateMassBalance_transientEnergyBalance.cfp_xMinus[1])
    annotation (Line(
      points={{38,0},{40,0},{40,1.8},{45.8,1.8}},
      color={0,140,72},
      thickness=1));
  connect(outlet_phX_steadyStateMassBalance_transientEnergyBalance.port,
    vle_phX_steadyStateMassBalance_transientEnergyBalance.cfp_xPlus[1])
    annotation (Line(
      points={{62,0},{60,0},{60,1.8},{57.8,1.8}},
      color={0,140,72},
      thickness=1));
  connect(inlet_phX_steadyStateMassBalance_steadyStateEnergyBalance.port,
    vle_phX_steadyStateMassBalance_steadyStateEnergyBalance.cfp_xMinus[1])
    annotation (Line(
      points={{38,-50},{40,-50},{40,-48.2},{45.8,-48.2}},
      color={0,140,72},
      thickness=1));
  connect(outlet_phX_steadyStateMassBalance_steadyStateEnergyBalance.port,
    vle_phX_steadyStateMassBalance_steadyStateEnergyBalance.cfp_xPlus[1])
    annotation (Line(
      points={{62,-50},{60,-50},{60,-48.2},{57.8,-48.2}},
      color={0,140,72},
      thickness=1));
  connect(sorption_pTX_transientMassBalance_transientEnergyBalance.port,
    vle_pTX_transientMassBalance_transientEnergyBalance.fp_sorption)
    annotation (Line(
      points={{-60,38},{-60,42.4},{-51.6,42.4}},
      color={0,140,72},
      thickness=1));
  connect(sorption_phX_transientMassBalance_transientEnergyBalance.port,
    vle_phX_transientMassBalance_transientEnergyBalance.fp_sorption)
    annotation (Line(
      points={{40,38},{40,42.4},{48.4,42.4}},
      color={0,140,72},
      thickness=1));

  connect(heatSource_pTX_transientMassBalance_transientEnergyBalance.port,
    vle_pTX_transientMassBalance_transientEnergyBalance.hp_yMinus)
    annotation (Line(
      points={{-40,38},{-40,42},{-50,42},{-50,44}},
      color={238,46,47},
      thickness=1));
  connect(heatSource_pTX_steadyStateMassBalance_transientEnergyBalance.port,
    vle_pTX_steadyStateMassBalance_transientEnergyBalance.hp_yMinus)
    annotation (Line(
      points={{-40,-12},{-40,-8},{-50,-8},{-50,-6}},
      color={238,46,47},
      thickness=1));
  connect(heatSource_pTX_steadyStateMassBalance_steadyStateEnergyBalance.port,
    vle_pTX_steadyStateMassBalance_steadyStateEnergyBalance.hp_yMinus)
    annotation (Line(
      points={{-40,-62},{-40,-58},{-50,-58},{-50,-56}},
      color={238,46,47},
      thickness=1));
  connect(heatSource_phX_steadyStateMassBalance_steadyStateEnergyBalance.port,
    vle_phX_steadyStateMassBalance_steadyStateEnergyBalance.hp_yMinus)
    annotation (Line(
      points={{60,-62},{60,-58},{50,-58},{50,-56}},
      color={238,46,47},
      thickness=1));
  connect(heatSource_phX_steadyStateMassBalance_transientEnergyBalance.port,
    vle_phX_steadyStateMassBalance_transientEnergyBalance.hp_yMinus)
    annotation (Line(
      points={{60,-12},{60,-8},{50,-8},{50,-6}},
      color={238,46,47},
      thickness=1));
  connect(heatSource_phX_transientMassBalance_transientEnergyBalance.port,
    vle_phX_transientMassBalance_transientEnergyBalance.hp_yMinus)
    annotation (Line(
      points={{60,38},{60,42},{50,42},{50,44}},
      color={238,46,47},
      thickness=1));

  //
  // Annotations
  //
  annotation (experiment(StopTime=25), Documentation(info="<html>
<p>
This model checks the VLE volume model.
<br/><br/>
To see the model behavior, plot the variables of the models over the 
time. The simulation time is correctly preset (Start: 0 s, Stop = 25 s). 
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 8, 2023, by Mirko Engelpracht:<br/>
  Added documentation.
  </li>
  <li>
  January 12, 2021, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Test_VLEVolume;
