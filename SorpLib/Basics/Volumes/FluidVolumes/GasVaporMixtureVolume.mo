within SorpLib.Basics.Volumes.FluidVolumes;
model GasVaporMixtureVolume "Homogenous volume of an ideal gas-vapor mixture"
  extends SorpLib.Basics.Volumes.BaseClasses.PartialFluidVolume(
    final no_components = Medium.nX,
    final pressureNoStateVariable = Medium.singleState,
    final neglectTermVp = false,
    redeclare final connector FluidPortsIn =
      SorpLib.Basics.Interfaces.FluidPorts.GasPort_in,
    redeclare final connector FluidPortsOut =
      SorpLib.Basics.Interfaces.FluidPorts.GasPort_out,
    independentStateVariables=SorpLib.Choices.IndependentVariablesVolume.phX,
    useHeatPorts=true,
    useHeatPortsY=true,
    h_initial=Medium.specificEnthalpy_pTX(p=p_initial, T=T_initial, X=X_i_initial),
    type_overallMassBalance=
      SorpLib.Choices.BalanceEquations.TransientFixedInitial,
    type_energyBalance=
      SorpLib.Choices.BalanceEquations.TransientFixedInitial);

  //
  // Definition of parameters regarding the medium
  //
  replaceable package Medium =
    SorpLib.Media.IdealGasVaporMixtures.MoistAir_N2_O2_CO2_H2O
    constrainedby
    SorpLib.Media.IdealGasVaporMixtures.Interfaces.PartialIdealGasWaterVaporMixture
    "Medium model of the ideal gas-vapor mixture"
    annotation (Dialog(tab="General", group="Media"),
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

  parameter Integer no_adsorptivs(min=1, max=no_components) = 1
    "Number of adsorptivs (i.e., components that can be adsorbed/desorbed)"
    annotation (Dialog(tab="General", group="Media"),
                Evaluate=true,
                HideResult=true);
  parameter Integer[no_adsorptivs] ind_adsorptivs=
    fill(Medium.nX, no_adsorptivs)
    "Indexes of adsorptivs in the ideal gas mixture"
    annotation (Dialog(tab="General", group="Media"),
                Evaluate=true,
                HideResult=true);
  final parameter Integer[no_components] mapped_ind_adsorptivs=
    SorpLib.Basics.Volumes.Utilities.mapSorptionFluidPorts(
      no_components=no_components,
      ind_adsorptivs=ind_adsorptivs)
    "Indices of sorption fluid ports mapped to component indices: I.e., value of
    zero if component cannot be adsorbed/desorbed; otherwise, value of sorption
    fluid port"
    annotation (Dialog(tab="General", group="Media"),
                Evaluate=true,
                HideResult=true);

  //
  // Definition of parameters regarding the calculation setup
  //
  parameter Boolean idealGasVaporMixture = true
    " = true, if medium is an ideal gas-vapoor mixture and govering equations are  
    explicitly written for an ideal gas mixture (i.e., unsaturated air)"
    annotation (Dialog(tab="General", group="Calculation Setup"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult = true);

  //
  // Definition of parameters regarding start values
  //
  parameter Modelica.Units.SI.MassFraction[no_components] X_i_initial=
    Medium.reference_X
    "Initial values of mass fractions"
    annotation (Dialog(tab="Initialisation", group="Initial Values"));

  parameter Modelica.Units.SI.MassFlowRate md_flow_initialX = 1e-4
    "Start value for diffusive mass flow rates in x direction"
    annotation (Dialog(tab="Initialisation", group="Start Values"));
  parameter Modelica.Units.SI.MassFlowRate ms_flow_initial = 1e-4
    "Start value for sorption mass flow rate"
    annotation (Dialog(tab="Initialisation", group="Start Values"));

  //
  // Definition of advanced parameters
  //
  parameter SorpLib.Choices.BalanceEquations type_independentMassBalances=
    type_overallMassBalance
    "Handling of independent mass balances and corresponding initialisations"
    annotation (Dialog(tab = "Advanced", group = "Conservation Equations"),
                Evaluate=true,
                HideResult=true);

  //
  // Definition of ports
  //
  SorpLib.Basics.Interfaces.HeatPorts.HeatPort_in hp_sorption
    "Sorption heat port"
    annotation (Placement(transformation(extent={{10,-50},{22,-38}}),
                iconTransformation(extent={{10,-50},{22,-38}})));

  FluidPortsIn[no_components] dfp_xMinus(
    each final no_components=1,
    m_flow(each start=-md_flow_initialX))
    "Diffusive fluid port at position '-dx/2'"
    annotation (Placement(transformation(extent={{-82,-22},{-74,-14}}),
                iconTransformation(extent={{-82,-22},{-74,-14}})));
  FluidPortsOut[no_components] dfp_xPlus(
    each final no_components=1,
    m_flow(each start=md_flow_initialX))
    "Diffusive fluid port at position '+dx/2'"
    annotation (Placement(transformation(extent={{38,-22},{46,-14}}),
                                iconTransformation(extent={{38,-22},{46,-14}})));

  FluidPortsIn[no_adsorptivs] fp_sorption(
    each final no_components=1,
    m_flow(each start=ms_flow_initial))
    "Sorption fluid port"
    annotation (Placement(transformation(extent={{-20,-80},{-12,-72}}),
                iconTransformation(extent={{-20,-80},{-12,-72}})));

  //
  // Definition of variables
  //
  Modelica.Units.SI.Pressure[no_components] p_i
    "Partial pressures";
  Modelica.Units.SI.MassFraction[no_components] X_i(
    start=X_i_initial,
    each stateSelect=StateSelect.prefer)
    "Mass fractions";

  Modelica.Units.SI.SpecificEnthalpy[no_components] h_i
    "Specific enthalpy of individual components";

  Modelica.Units.SI.Mass[no_components] m_i
    "Mass of individual components";

  Modelica.Units.SI.MassFlowRate[no_components] dm_i_dtau
    "Derivative of individual components' masses w.r.t. time";

  Modelica.Units.SI.MassFlowRate[no_components] mc_i_flow
    "Sum of all individual components' convective mass flow rates across 
    boundaries";
  Modelica.Units.SI.MassFlowRate md_flow
    "Sum of all diffusive mass flow rates across boundaries";
  Modelica.Units.SI.MassFlowRate[no_components] md_i_flow
    "Sum of all individual components' diffusive mass flow rates across 
    boundaries";
  Modelica.Units.SI.MassFlowRate ms_flow
    "Sum of all sorption mass flow rates across boundaries";
  Modelica.Units.SI.MassFlowRate[no_components] ms_i_flow
    "Sum of all individual components' sorption mass flow rates across 
    boundaries";

  //
  // Definition of protected variables
  //
protected
  Medium.ThermodynamicState state
    "Thermodynamic state required to calculate medium properties";

initial equation
  if type_independentMassBalances ==
    SorpLib.Choices.BalanceEquations.TransientFixedInitial then
    X_i = X_i_initial
      "Fixed initial values";

  elseif type_independentMassBalances ==
    SorpLib.Choices.BalanceEquations.TransientSteadyStateInitial then
    der(X_i) = zeros(no_components)
      "Steady-state initialisation of independent mass balances";

  end if;

equation
  //
  // Assertations
  //
  assert(pressureNoStateVariable or not (
    type_energyBalance<>SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial and
    type_overallMassBalance==SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial),
    "Steady-state mass balance combined with transient energy balance is not " +
    "sound if the fluid volume is fixed!",
    level = AssertionLevel.warning);

  if idealGasVaporMixture then
    assert(Medium.relativeHumidity(state=state) < 1.0,
      "Govering equations according to ideal gas mixture can only be applied if " +
      "dry air is not saturatred! Results may not be reasonable!",
      level = AssertionLevel.warning);

  end if;

  //
  // Calculation of properties
  //
  if independentStateVariables ==
    SorpLib.Choices.IndependentVariablesVolume.pTX then
    state = Medium.setState_pTX(p=p, T=T, X=X_i)
      "Thermodynamic state required to calculate medium properties";
    h = Medium.specificEnthalpy(state=state)
      "Specific enthalpy";

  else
    state = Medium.setState_phX(p=p, h=h, X=X_i)
      "Thermodynamic state required to calculate medium properties";
    T = Medium.temperature(state=state)
      "Temperature";

  end if;

  p_i = Medium.partialPressures(state=state)
    "Partial pressures";
  v = 1 / rho
    "Specific volume";
  rho = Medium.density(state=state)
    "Density";

  for ind in 1:no_components loop
    h_i[ind] = Medium.specificEnthalpy_i(ind_component=ind, state=state)
      "Specific enthalpy of individual components";
  end for;

  //
  // Momentum balance
  //
  dfp_xMinus.p = p_i
    "Partial pressures at the port (i.e., homogenous volume)";
  dfp_xPlus.p = p_i
    "Partial pressures at the port (i.e., homogenous volume)";

  for ind in 1:no_adsorptivs loop
    fp_sorption[ind].p = p_i[ind_adsorptivs[ind]]
      "Partial pressures at the port (i.e., homogenous volume)";
  end for;

  //
  // Mass balance
  //
  m_i = m .* X_i
    "Mass of individual components";

  dm_dtau = mc_flow + md_flow + ms_flow
    "Overall mass balance";
  dm_i_dtau = mc_i_flow + md_i_flow + ms_i_flow
    "Individual components' mass balances";

  if type_overallMassBalance==
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial then
    dm_dtau = 0
      "Steady-state overall mass balance";

  else
    if independentStateVariables==
      SorpLib.Choices.IndependentVariablesVolume.pTX then
      dm_dtau = geometry.V * (Medium.density_derX(state=state) * der(X_i) +
        rho * (fluidProperties.kappa *der(p) - fluidProperties.beta * der(T)))
        "Transient overall mass balance";
      /*dm_dtau = geometry.V * (Medium.density_derp_T(state=state) * der(p) +
        Medium.density_derT_p(state=state) * der(T) +
        Medium.density_derX(state=state) * der(X_i))
        "Transient overall mass balance";*/

    else
      if idealGasVaporMixture then
        dm_dtau = geometry.V * (rho * (fluidProperties.kappa * der(p) -
          fluidProperties.beta / fluidProperties.cp * der(h)) +
          (Medium.density_derX(state=state) .+ rho .* fluidProperties.beta .*
          Medium.dh_dX_pT(state=state) ./ fluidProperties.cp) * der(X_i))
          "Transient overall mass balance";

      else
        dm_dtau = geometry.V * (rho * ((fluidProperties.kappa -
          fluidProperties.beta * fluidProperties.my) * der(p) -
          fluidProperties.beta / fluidProperties.cp * der(h)) +
          (Medium.density_derX(state=state) .+ rho .* fluidProperties.beta .*
          Medium.dh_dX_pT(state=state) ./ fluidProperties.cp) * der(X_i))
          "Transient overall mass balance";
        /*dm_dtau = geometry.V * (Medium.density_derp_h(state=state) * der(p) +
          Medium.density_derh_p(state=state) * der(h) +
          Medium.drho_dX_ph(state=state) * der(X_i))
          "Transient overall mass balance";*/

      end if;
    end if;
  end if;

  if type_independentMassBalances==
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial then
    dm_i_dtau = zeros(no_components)
      "Steady-state independent mass balances";

  else
    dm_i_dtau = X_i .* dm_dtau .+ m .* der(X_i)
      "Transient independent mass balances";

  end if;

  mc_flow = sum(cfp_xMinus.m_flow) + sum(cfp_xPlus.m_flow)
    "Sum of all convective mass flow rates across boundaries";
  md_flow = sum(dfp_xMinus.m_flow) + sum(dfp_xPlus.m_flow)
    "Sum of all diffusive mass flow rates across boundaries";
  ms_flow = sum(fp_sorption.m_flow)
    "Sum of all sorption mass flow rates across boundaries";

  if avoid_events then
    for ind_comp in 1:no_components loop
      if ind_comp <> no_components then
        mc_i_flow[ind_comp] =
          sum(cfp_xMinus[ind_port].m_flow *
          noEvent(actualStream(cfp_xMinus[ind_port].Xi_outflow[ind_comp]))
          for ind_port in 1:nPorts_cfp_xMinus) +
          sum(cfp_xPlus[ind_port].m_flow *
          noEvent(actualStream(cfp_xPlus[ind_port].Xi_outflow[ind_comp]))
          for ind_port in 1:nPorts_cfp_xPlus)
          "Sum of all convective mass flow rates of component ind_comp across 
          boundaries";

      else
        mc_i_flow[ind_comp] =  mc_flow - sum(mc_i_flow[1:no_components-1])
          "Sum of all convective mass flow rates of last component across 
          boundaries";

      end if;
    end for;

  else
    for ind_comp in 1:no_components loop
      if ind_comp <> no_components then
        mc_i_flow[ind_comp] =
          sum(cfp_xMinus[ind_port].m_flow *
          actualStream(cfp_xMinus[ind_port].Xi_outflow[ind_comp])
          for ind_port in 1:nPorts_cfp_xMinus) +
          sum(cfp_xPlus[ind_port].m_flow *
          actualStream(cfp_xPlus[ind_port].Xi_outflow[ind_comp])
          for ind_port in 1:nPorts_cfp_xPlus)
          "Sum of all convective mass flow rates of component ind_comp across 
          boundaries";

      else
        mc_i_flow[ind_comp] =  mc_flow - sum(mc_i_flow[1:no_components-1])
          "Sum of all convective mass flow rates of last component across 
          boundaries";

      end if;
    end for;
  end if;

  md_i_flow = dfp_xMinus.m_flow .+ dfp_xPlus.m_flow
    "Sum of all individual components' diffusive mass flow rates across 
    boundaries";

  for ind in 1:no_components loop
    ms_i_flow[ind] = if mapped_ind_adsorptivs[ind] == 0 then 0 else
      fp_sorption[mapped_ind_adsorptivs[ind]].m_flow
      "Sum of all individual components' sorption mass flow rates across 
      boundaries";
  end for;

  for ind in 1:nPorts_cfp_xMinus loop
    cfp_xMinus[ind].Xi_outflow = X_i[1:no_components-1]
      "Independent mass fractions leaving the port (i.e., homogenous volume)";
  end for;
  for ind in 1:nPorts_cfp_xPlus loop
    cfp_xPlus[ind].Xi_outflow = X_i[1:no_components-1]
      "Independent mass fractions leaving the port (i.e., homogenous volume)";
  end for;

  //
  // Energy balance
  //
  dU_dtau = Hb_flow + Qb_flow
    "Energy balane";

  if type_energyBalance==
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial then
    dU_dtau = 0
      "Steady-state energy balance";

  else
    if independentStateVariables==
      SorpLib.Choices.IndependentVariablesVolume.pTX then
      if idealGasVaporMixture then
        dU_dtau = u * dm_dtau + m * (
          (fluidProperties.cp - p * v * fluidProperties.beta) * der(T) +
          (Medium.dh_dX_pT(state=state) .+ p .* v.^2 .*
          Medium.density_derX(state=state)) * der(X_i))
          "Transient energy balance";

      else
        dU_dtau = u * dm_dtau + m * (
          v * (p * fluidProperties.kappa - T * fluidProperties.beta) * der(p) +
          (fluidProperties.cp - p * v * fluidProperties.beta) * der(T) +
          (Medium.dh_dX_pT(state=state) .+ p .* v.^2 .*
          Medium.density_derX(state=state)) * der(X_i))
          "Transient energy balance";
        /*dU_dtau = u * dm_dtau + m * (
          (Medium.dh_dp_TX(state=state) * der(p) +
          Medium.dh_dT_pX(state=state) * der(T) +
          Medium.dh_dX_pT(state=state) * der(X_i)) -
          1 / rho * der(p) +
          p / rho^2 * (Medium.density_derp_T(state=state) * der(p) +
          Medium.density_derT_p(state=state) * der(T) +
          Medium.density_derX(state=state) * der(X_i)))
          "Transient energy balance";*/

      end if;

    else
      if idealGasVaporMixture then
        dU_dtau = u * dm_dtau + m * (
          (1 - p * v * fluidProperties.beta / fluidProperties.cp) * der(h) +
          p .* v.^2 .* (Medium.density_derX(state=state) .+ rho .*
          fluidProperties.beta .* Medium.dh_dX_pT(state=state) ./
          fluidProperties.cp) * der(X_i))
          "Transient energy balance";

      else
        dU_dtau = u * dm_dtau + m * (
          (1 - p * v * fluidProperties.beta / fluidProperties.cp) * der(h) +
          v * (p * (fluidProperties.kappa - fluidProperties.beta *
          fluidProperties.my) - 1) * der(p) +
          p .* v.^2 .* (Medium.density_derX(state=state) .+ rho .*
          fluidProperties.beta .* Medium.dh_dX_pT(state=state) ./
          fluidProperties.cp) * der(X_i))
          "Transient energy balance";
        /*dU_dtau = u * dm_dtau + m * (
          der(h) -
          1 / rho * der(p) +
          p / rho^2 * (Medium.density_derp_h(state=state) * der(p) +
          Medium.density_derh_p(state=state) * der(h) +
          Medium.drho_dX_ph(state=state) * der(X_i)))
          "Transient energy balance";*/

      end if;
    end if;
  end if;

  if avoid_events then
    Hb_flow =
      sum(cfp_xMinus.m_flow .* noEvent(actualStream(cfp_xMinus.h_outflow))) +
      sum(cfp_xPlus.m_flow .* noEvent(actualStream(cfp_xPlus.h_outflow))) +
      sum(dfp_xMinus.m_flow .* noEvent(actualStream(dfp_xMinus.h_outflow))) +
      sum(dfp_xPlus.m_flow .* noEvent(actualStream(dfp_xPlus.h_outflow))) +
      sum(fp_sorption.m_flow .* noEvent(actualStream(fp_sorption.h_outflow)))
      "Sum of all enthalpy flow rates across boundaries";

  else
    Hb_flow =
      sum(cfp_xMinus.m_flow .* actualStream(cfp_xMinus.h_outflow)) +
      sum(cfp_xPlus.m_flow .* actualStream(cfp_xPlus.h_outflow)) +
      sum(dfp_xMinus.m_flow .* actualStream(dfp_xMinus.h_outflow)) +
      sum(dfp_xPlus.m_flow .* actualStream(dfp_xPlus.h_outflow)) +
      sum(fp_sorption.m_flow .* actualStream(fp_sorption.h_outflow))
      "Sum of all enthalpy flow rates across boundaries";

  end if;

  Qb_flow = Q_flow_xMinus + Q_flow_xPlus +
    Q_flow_yMinus + Q_flow_yPlus +
    Q_flow_zMinus + Q_flow_zPlus +
    hp_sorption.Q_flow
    "Sum of all heat flow rates across boundaries";

  dfp_xMinus.h_outflow = h_i
    "Specific enthalpies leaving the port (i.e., homogenous volume)";
  dfp_xPlus.h_outflow = h_i
    "Specific enthalpies leaving the port (i.e., homogenous volume)";

  for ind in 1:no_adsorptivs loop
    fp_sorption[ind].h_outflow = h_i[ind_adsorptivs[ind]]
      "Specific enthalpy leaving the port (i.e., homogenous volume)";
  end for;

  hp_sorption.T = T
    "Temperature at sorption heat port";

  //
  // Summary record
  //
  fluidProperties.cp = if (calculateAdditionalProperties or
    (independentStateVariables==
    SorpLib.Choices.IndependentVariablesVolume.phX and
    type_overallMassBalance<>
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial) or
    (independentStateVariables==
    SorpLib.Choices.IndependentVariablesVolume.pTX and
    type_energyBalance<>
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial) or
    (independentStateVariables==
    SorpLib.Choices.IndependentVariablesVolume.phX and
    type_energyBalance<>
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial and
    not neglectTermVp)) then
    Medium.specificHeatCapacityCp(state=state) else 0
    "Specific heat capacity";
  fluidProperties.lambda = if calculateAdditionalProperties then
    Medium.thermalConductivity(state=state) else 0
    "Thermal conductivity";
  fluidProperties.eta = if calculateAdditionalProperties then
    Medium.dynamicViscosity(state=state) else 0
    "Dynamic viscosity";

  fluidProperties.beta = if (calculateAdditionalProperties or
    type_overallMassBalance<>
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial or
    (independentStateVariables==
    SorpLib.Choices.IndependentVariablesVolume.pTX and
    type_energyBalance<>
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial) or
    (independentStateVariables==
    SorpLib.Choices.IndependentVariablesVolume.phX and
    type_energyBalance<>
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial and
    not neglectTermVp)) then
    Medium.isobaricExpansionCoefficient(state=state) else 0
    "Isobaric expnasion coefficient";
  fluidProperties.kappa = if (calculateAdditionalProperties or
    type_overallMassBalance<>
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial or
    (type_energyBalance<>
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial and
    not neglectTermVp)) then
    Medium.isothermalCompressibility(state=state) else 0
    "Isothermal compressibility";
  fluidProperties.my = if (calculateAdditionalProperties or
    (independentStateVariables==
    SorpLib.Choices.IndependentVariablesVolume.phX and
    (type_overallMassBalance<>
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial or
    (type_energyBalance<>
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial and
    not neglectTermVp)))) then
    v / fluidProperties.cp * (fluidProperties.beta * T - 1) else 0
    "Joule-Thomson coefficient";

  fluidProperties.Pr = if calculateAdditionalProperties then
    fluidProperties.eta * fluidProperties.cp / fluidProperties.lambda else 0
    "Prandtl number";

  fluidProperties.m_flow_sorption = ms_flow
    "Total mass flow rate at port 'sorption'";

  fluidProperties.mc_flow_yMinus = 0
    "Convective mass flow rate at port '-dy/2'";
  fluidProperties.mc_flow_yPlus = 0
    "Convective mass flow rate at port '+dy/2'";
  fluidProperties.mc_flow_zMinus = 0
    "Convective mass flow rate at port '-dz/2'";
  fluidProperties.mc_flow_zPlus = 0
    "Convective mass flow rate at port '+dz/2'";

  fluidProperties.md_flow_xMinus = dfp_xMinus.m_flow
    "Diffusive mass flow rate at port '-dx/2'";
  fluidProperties.md_flow_xPlus = dfp_xPlus.m_flow
    "Diffusive mass flow rate at port '+dx/2'";
  fluidProperties.md_flow_yMinus = zeros(no_components)
    "Diffusive mass flow rate at port '-dy/2'";
  fluidProperties.md_flow_yPlus = zeros(no_components)
    "Diffusive mass flow rate at port '+dy/2'";
  fluidProperties.md_flow_zMinus = zeros(no_components)
    "Diffusive mass flow rate at port '-dz/2'";
  fluidProperties.md_flow_zPlus = zeros(no_components)
    "Diffusive mass flow rate at port '+dz/2'";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This model represents a gas-vapor-mixture volume, applying a lumped modeling approach. 
Depending on the volume setup, this model may have up to seven heat ports (i.e., two 
for each spatial direction of a cartesian coordinate system and one for an adsorbate
volume). Furthermore, this model has convective fluid ports in y-direction, following 
the 'connectorSizing' principle, diffusive fluid ports in y-direction, and a sorption
fluid port. These ports allow for the combination of several fluid volumes to create a 
spatially distributed model.
</p>

<h4>Main equations</h4>
<p>
The most important equations are the momentum, mass, and energy balance. According to
the staggered grid approach, the momentum balance is not solved within the volume but 
at the volume's boundaries via so-called 
<a href=\"Modelica://SorpLib.Components.Fittings\">flow models</a>. Hennce, no pressure 
losses occur within the volume:
</p>
<pre>
    p = cfp_xMinus.p;
</pre>
<pre>
    p = cfp_xPlus.p;
</pre>
<p>
The pressure at the diffusive fluid ports and sorption fluid ports correspnds to the
correct partial pressure:
</p>
<pre>
    p_i[i] = dfp_xMinus[i].p;
</pre>
<pre>
    p_i[i] = dfp_xPlus[i].p;
</pre>
<pre>
    p_i[i] = fp_sorption[i].p;
</pre>

<br/>
<p>
Regarding the mass and energy balances, either steady-state or tansient balnaces can be 
selected. When using the pressure <i>p</i>, temperature <i>T</i>, and mass fractions 
<i>X<sub>i</sub></i> as independent states, the overall mass balance is defined as
</p>
<pre>
    (dm/d&tau;) = V * (d&rho;/d&tau;) = V * [&rho; * (&kappa; * (dp/d&tau;) - &beta; * (dT/d&tau;)) + &sum; (&part;&rho;/&part;X<sub>i</sub>|<sub>p,T</sub>) * (dX<sub>i</sub>/d&tau;)] = &sum; m<sub>c,flow</sub> + &sum; m<sub>d,flow</sub>+ &sum; m<sub>s,flow</sub>;
</pre>
<p>
the component-specific mass balances are defined as
</p>
<pre>
    (dm<sub>i</sub>/d&tau;) = X<sub>i</sub> * (dm/d&tau;) + m * (dX<sub>i</sub>/d&tau;) = &sum; m<sub>c,i,flow</sub> + &sum; m<sub>d,i,flow</sub>+ &sum; m<sub>s,i,flow</sub>;
</pre>
<p>
and the energy balance is defined as
</p>
<pre>
    (dU/d&tau;) = u * (dm/d&tau;) + m * (du/d&tau;) = u * (dm/d&tau;) + m * [v * (p * &kappa; - T * &beta;) * (dp/d&tau;) + (c<sub>p</sub> - p * v * &beta;) * (dT/d&tau;) + &sum; ((&part;h/&part;X<sub>i</sub>|<sub>p,T</sub>) + p * v<sup>2</sup> * (&part;&rho;/&part;X<sub>i</sub>|<sub>p,T</sub>)) * (dX<sub>i</sub>/d&tau;)] = &sum; H<sub>b,flow</sub> + &sum; Q<sub>b,flow</sub>;
</pre>

<br/>
<p>
When using the pressure <i>p</i>, specific enthalpy <i>h</i>, and mass fractions 
<i>X<sub>i</sub></i> as independent states, the overall mass balance is defined as
</p>
<pre>
    (dm/d&tau;) = V * (d&rho;/d&tau;) = V * [&rho; * ((&kappa; - &beta; * &mu;) * (dp/d&tau;) - &beta; / c<sub>p</sub> * (dh/d&tau;)) + &sum; ((&part;&rho;/&part;X<sub>i</sub>|<sub>p,T</sub>) + &rho; * &beta; * (&part;h/&part;X<sub>i</sub>|<sub>p,T</sub>) / c<sub>p</sub>) * (dX<sub>i</sub>/d&tau;)] = &sum; m<sub>c,flow</sub> + &sum; m<sub>d,flow</sub>+ &sum; m<sub>s,flow</sub>;
</pre>
<p>
the component-specific mass balances are defined as
</p>
<pre>
    (dm<sub>i</sub>/d&tau;) = X<sub>i</sub> * (dm/d&tau;) + m * (dX<sub>i</sub>/d&tau;) = &sum; m<sub>c,i,flow</sub> + &sum; m<sub>d,i,flow</sub>+ &sum; m<sub>s,i,flow</sub>;
</pre>
<p>
and the energy balance is defined as
</p>
<pre>
    (dU/d&tau;) = u * (dm/d&tau;) + m * (du/d&tau;) = u * (dm/d&tau;) + m * [v * (p * (&kappa; - &beta; * &mu;) - 1) * (dp/d&tau;) + (1 - p * v * &beta; / c<sub>p</sub>) * (dh/d&tau;) + &sum; p * v<sup>2</sup> * ((&part;&rho;/&part;X<sub>i</sub>|<sub>p,T</sub>) + &rho; * &beta; * (&part;h/&part;X<sub>i</sub>|<sub>p,T</sub>) / c<sub>p</sub>) * (dX<sub>i</sub>/d&tau;)] = &sum; H<sub>b,flow</sub> + &sum; Q<sub>b,flow</sub>;
</pre>

<br/>
<p>
Herein, <i>(dm/d&tau;)</i> is the derivative of the mass w.r.t. time, <i>(dU/d&tau;)</i> 
is the derivative of the internal energy w.r.t. time, <i>(d&rho;/d&tau;)</i> is the 
derivative of the density w.r.t. time, <i>(du/d&tau;)</i> is the derivative of the specific 
internal energy w.r.t.time, <i>(dp/d&tau;)</i> is the derivative of the pressure w.r.t. 
time, <i>(dT/d&tau;)</i> is the derivative of the temperature w.r.t. time, <i>(dh/d&tau;)</i> 
is the derivative of the specific enthalpy w.r.t. time, <i>(dX<sub>i</sub>/d&tau;)</i> are the 
derivatives of the mass fractions w.r.t. time, <i>(&part;&rho;/&part;X<sub>i</sub>|<sub>p,T</sub>)</i> is 
the partial derivative of the density w.r.t. mass fractions at constant pressure and temperatre,
<i>(&part;h/&part;X<sub>i</sub>|<sub>p,T</sub>)</i> is the partial derivative of the specific enthaly w.r.t. 
mass fractions at constant pressure and temperature, <i>V</i> is the volume, <i>m</i> is the mass, 
<i>v</i> is the specific volume, <i>&rho;</i> is the density, <i>&kappa;</i> is the isothermal 
compressibility, <i>&beta;</i> is the isobaric expansion coefficient, <i>&mu;</i> is the Joule-Thomson 
coefficient, <i>c<sub>p</sub></i> is the specific heat capacity at constant pressure, 
<i>m<sub>c,flow</sub></i> is the sum of the convective mass flow rates, <i>m<sub>d,flow</sub></i> is 
the sum of the diffusive mass flow rates, <i>m<sub>s,flow</sub></i> is the sum of the sorption mass 
flow rates, <i>H<sub>b,flow</sub></i> is the sum of the enthalpy flow rates, and <i>Q<sub>b,flow</sub></i> 
is the sum of the heat flow rates. Mass flow rates with index <i>i</i> describe component-specific 
mass flow rates.
</p>

<h4>Main equations for an ideal gas mixture</h4>
<p>
For an ideal gas mixture, the isothermal compressibility <i>&kappa;</i> is <i>1/p</i> and the
isbaric expansion coefficient <i>&beta;</i> is <i>1/T</i>, leading to a Joule-Thomson coefficient 
<i>&mu;</i> of zero. Hence, the overall mass and energy balances can be simplified. When using the 
pressure <i>p</i>, temperature <i>T</i>, and mass fractions <i>X<sub>i</sub></i> as independent 
states, the overall mass balance still reads as
</p>
<pre>
    (dm/d&tau;) = V * [&rho; * (&kappa; * (dp/d&tau;) - &beta; * (dT/d&tau;)) + &sum; (&part;&rho;/&part;X<sub>i</sub>|<sub>p,T</sub>) * (dX<sub>i</sub>/d&tau;)] = &sum; m<sub>c,flow</sub> + &sum; m<sub>d,flow</sub>+ &sum; m<sub>s,flow</sub>;
</pre>
<p>
but the energy balance now reads as
</p>
<pre>
    (dU/d&tau;) = u * (dm/d&tau;) + m * [(c<sub>p</sub> - p * v * &beta;) * (dT/d&tau;) + &sum; ((&part;h/&part;X<sub>i</sub>|<sub>p,T</sub>) + p * v<sup>2</sup> * (&part;&rho;/&part;X<sub>i</sub>|<sub>p,T</sub>)) * (dX<sub>i</sub>/d&tau;)] = &sum; H<sub>b,flow</sub> + &sum; Q<sub>b,flow</sub>;
</pre>

<br/>
<p>
When using the pressure <i>p</i>, specific enthalpy <i>h</i>, and mass fractions 
<i>X<sub>i</sub></i> as independent states, the overall mass balance now reads as
</p>
<pre>
    (dm/d&tau;) = V * [&rho; * (&kappa; * (dp/d&tau;) - &beta; / c<sub>p</sub> * (dh/d&tau;)) + &sum; ((&part;&rho;/&part;X<sub>i</sub>|<sub>p,T</sub>) + &rho; * &beta; * (&part;h/&part;X<sub>i</sub>|<sub>p,T</sub>) / c<sub>p</sub>) * (dX<sub>i</sub>/d&tau;)] = &sum; m<sub>c,flow</sub> + &sum; m<sub>d,flow</sub>+ &sum; m<sub>s,flow</sub>;
</pre>
<p>
and the energy balance now reads as
</p>
<pre>
    (dU/d&tau;) = u * (dm/d&tau;) + m * [(1 - p * v * &beta; / c<sub>p</sub>) * (dh/d&tau;) + &sum; p * v<sup>2</sup> * ((&part;&rho;/&part;X<sub>i</sub>|<sub>p,T</sub>) + &rho; * &beta; * (&part;h/&part;X<sub>i</sub>|<sub>p,T</sub>) / c<sub>p</sub>) * (dX<sub>i</sub>/d&tau;)] = &sum; H<sub>b,flow</sub> + &sum; Q<sub>b,flow</sub>;
</pre>

<h4>Assumptions and limitations</h4>
<ul>
  <li>
  Constant volume <i>V</i>
  </li>
  <li>
  Homogenoues properties within the volume
  </li>
  <li>
  Ideal gas-vapor mixture
  </li>
</ul>

<h4>Typical use</h4>
<p>
This model is typically used to model the adsorptive of open adsorption systems.
</p>

<h4>Important parameters and options</h4>
<ul>
  <li>
  <i>useHeatPorts</i>:
  Defines if heat ports in the spatial direction <i>i</i> are required.
  </li>
  <li>
  <li>
  <i>independentStateVariables</i>:
  Defines independent state variables.
  </li>
  <li>
  <i>idealGasVaporMixture</i>:
  Defines if the medium is an ideal gas-vapor mixture and, thus, the governing  
  equations can be simplified.
  </li>
  <li>
  <i>calculateAdditionalProperties</i>:
  Defines if additional properties like transport properties shall be calculated.
  </li>
  <br/>
  <li>
  <i>type_overallMassBalance</i>:
  Defines the type of the overall mass balance.
  </li>
  <li>
  <i>type_independentMassBalances</i>:
  Defines the type of the independent mass balances.
  </li>
  <li>
  <i>type_energyBalance</i>:
  Defines the type of the energy balance.
  </li>
  <br/>
  <li>
  <i>avoid_events</i>:
  Defines if events shall be avoided via the noEvent()-operator.
  </li>
</ul>
<p>
Note that not all combinations of govering equation types are reasonable. Typically,
a transient mass balance is combined with a transient energy balance.
</p>

<h4>Dynamics</h4>
<p>
This model has two dynamic state that can be selected (see options):
</p>
<ul>
  <li>
  Pressure <i>p</i>, temperature <i>T</i>, and mass fractions <i>X<sub>i</sub></i> (recommended), or
  </li>
  <li>
  pressure <i>p</i>, specific enthalpy <i>h</i>, and mass fractions <i>X<sub>i</sub></i>.
  </li>
</ul>
<p>
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 13, 2023, by Mirko Engelpracht:<br/>
  Major revisions due to restructering finite volumes.
  </li>
  <li>
  January 12, 2021, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end GasVaporMixtureVolume;
