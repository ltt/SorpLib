within SorpLib.Basics.Volumes.BaseClasses;
partial model PartialPhaseSeparatorVolume
  "Base model for all phase saprator volumes"

  //
  // Definition of parameters describing media
  //
  parameter Integer no_components = 1
    "Number of components in the medium"
    annotation (Dialog(tab="General", group="Media"),
                Evaluate=true,
                HideResult=true);

  //
  // Definition of parameters regarding classes
  //
  parameter Integer nPorts_cfp_xMinus=0
    "Number of ports at cfp_xMinus"
    annotation (Dialog(connectorSizing=true),
                HideResult=true);
  parameter Integer nPorts_cfp_xPlus=0
    "Number of ports at cfp_xPlus"
    annotation (Dialog(connectorSizing=true),
                HideResult=true);

  //
  // Definition of parameters regarding start values
  //
  parameter Modelica.Units.SI.Temperature T_initial = 298.15
    "Initial value of temperature"
    annotation (Dialog(tab="Initialisation", group="Initial Values"));
  parameter Modelica.Units.SI.Density rho_initial = 200
    "Initial value of density"
    annotation (Dialog(tab="Initialisation", group="Initial Values"));

  parameter Modelica.Units.SI.MassFlowRate mc_flow_initialX = 1e-4
    "Start value for convective mass flow rate in x direction"
    annotation (Dialog(tab="Initialisation", group="Start Values"));

  extends SorpLib.Basics.Volumes.BaseClasses.PartialVolume(
    redeclare replaceable SorpLib.Basics.Volumes.Records.PhaseSeparatorGeometry geometry
    constrainedby SorpLib.Basics.Volumes.Records.PhaseSeparatorGeometry);

  //
  // Definition of parameters regarding calculation setup
  //
  parameter Boolean calculateAdditionalProperties = false
    "= true, if additional properties required for tranport models are calculated"
    annotation (Dialog(tab="General", group="Calculation Setup"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult = true);

  //
  // Definition of advanced parameters
  //
  parameter SorpLib.Choices.BalanceEquations type_overallMassBalance=
    SorpLib.Choices.BalanceEquations.TransientFixedInitial
    "Handling of the overall mass balance and corresponding initialisation"
    annotation (Dialog(tab = "Advanced", group = "Conservation Equations"),
                Evaluate=true,
                HideResult=true);

  parameter Boolean useStateLimiter = true
    "= true, if specific enthalpy of vapor or liquid is limited at ports (i.e.,
    vapor port is slightly overheated and liquid port is slightly supercooled)"
    annotation (Dialog(tab = "Advanced", group = "Limits"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult = true);
  parameter Modelica.Units.SI.SpecificEnthalpy dh = 10
    "Specific enthalpy used for superheating and supercooling"
    annotation (Dialog(tab = "Advanced", group = "Limits",
                enable = useStateLimiter),
                HideResult = true);

  //
  // Definition of ports
  //
  SorpLib.Basics.Interfaces.FluidPorts.VLEPort_in[nPorts_cfp_xMinus] cfp_xMinus(
    each final no_components=no_components,
    m_flow(each start=mc_flow_initialX))
    "Convective fluid port at position '-dx/2' (i.e., liquid port)"
    annotation (Placement(transformation(extent={{-46,14},{-38,22}}),
                iconTransformation(extent={{-46,14},{-38,22}})));
  SorpLib.Basics.Interfaces.FluidPorts.VLEPort_out[nPorts_cfp_xPlus] cfp_xPlus(
    each final no_components=no_components,
    m_flow(each start=-mc_flow_initialX))
    "Convective fluid port at position '+dx/2' (i.e., vapor port)"
    annotation (Placement(transformation(extent={{74,14},{82,22}}),
                iconTransformation(extent={{74,14},{82,22}})));

  //
  // Definition of variables
  //
  Modelica.Units.SI.Pressure p(
    stateSelect=StateSelect.avoid)
    "Pressure";
  Modelica.Units.SI.Temperature T(
    start=T_initial,
    stateSelect=StateSelect.prefer)
    "Temperature";
  Modelica.Units.SI.SpecificVolume v
    "Specific volume";
  Modelica.Units.SI.Density rho(
    start=rho_initial,
    stateSelect=StateSelect.prefer)
    "Density";

  Modelica.Units.SI.SpecificEnthalpy h(
    stateSelect=StateSelect.avoid)
    "Specific enthalpy";
  Modelica.Units.SI.SpecificInternalEnergy u
    "Specific internal energy";

  Modelica.Units.SI.Density rho_liq
    "Density at saturated liquid line";
  Modelica.Units.SI.Density rho_vap
    "Density at saturated vapor line";
  Modelica.Units.SI.SpecificEnthalpy h_liq
    "Specific enthalpy at saturated liquid line";
  Modelica.Units.SI.SpecificEnthalpy h_vap
    "Specific enthalpy at saturated vapor line";
  Real q(unit="kg/kg")
    "Vapor mass fraction in phase seperator";

  Modelica.Units.SI.Length l_liq
    "Height of liquid phase";
  Real l_liq_rel(unit="1")
    "Relative height of liquid phase";

  Modelica.Units.SI.Mass m
    "Mass";
  Modelica.Units.SI.Mass m_liq
    "Liquid mass";
  Modelica.Units.SI.Mass m_vap
    "Vapor mass";

  Modelica.Units.SI.MassFlowRate dm_dtau
    "Derivative of mass w.r.t. time";
  Modelica.Units.SI.MassFlowRate mc_flow
    "Sum of all convective mass flow rates across boundaries";

  Modelica.Units.SI.InternalEnergy U
    "Internal energy";

  Real dU_dtau(final unit="W")
    "Derivative of internal energy w.r.t. time";
  Modelica.Units.SI.EnthalpyFlowRate Hb_flow
    "Sum of all enthalpy flow rates across boundaries";
  Modelica.Units.SI.HeatFlowRate Qb_flow
    "Sum of all heat flow rates across boundaries";

  SorpLib.Basics.Volumes.Records.PhaseSeparatorProperties phaseSepratorProperties(
    final no_components=no_components)
    "Properties of phase separator volume often required for calculation of 
    heat transfer";

initial equation
  if type_overallMassBalance ==
    SorpLib.Choices.BalanceEquations.TransientFixedInitial then
    rho = rho_initial
      "Fixed initial value";

  elseif type_overallMassBalance ==
    SorpLib.Choices.BalanceEquations.TransientSteadyStateInitial then
    der(rho) = 0
      "Steady-state initialisation of overall mass balance";

  end if;

  if type_energyBalance ==
    SorpLib.Choices.BalanceEquations.TransientFixedInitial then
    T = T_initial
      "Fixed initial value";

  elseif type_energyBalance ==
    SorpLib.Choices.BalanceEquations.TransientSteadyStateInitial then
    der(T) = 0
      "Steady-state initial value";

  end if;

equation
  //
  // Assertations
  //
  assert(no_components <= 1,
    "The liquid volume model can only handel pure fluids (i.e., with one component)!",
    level = AssertionLevel.error);

  assert(not (
    type_energyBalance<>SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial and
    type_overallMassBalance==SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial),
    "Steady-state mass balance combined with transient energy balance is not " +
    "sound if the fluid volume is fixed!",
    level = AssertionLevel.warning);

  //
  // Property calculation
  //
  v = 1 / rho
    "Specific volume";

  h = h_liq + q * (h_vap - h_liq)
    "Specific enthalpy";
  u = h - p * v
    "Specific internal energy";

  //
  // Additional property calculation
  //
  q = (1/rho - 1/rho_liq) / (1/rho_vap - 1/rho_liq)
    "Vapor mass fraction";

  l_liq = m_liq / (rho_liq * geometry.A_base)
    "Liquid filling height";
  l_liq_rel = l_liq / (geometry.V / geometry.A_base)
    "Relative height of liquid phase";

  //
  // Momentum balance
  //
  cfp_xMinus.p = fill(p, nPorts_cfp_xMinus)
    "Total pressure at the port (i.e., homogenous volume)";
  cfp_xPlus.p = fill(p, nPorts_cfp_xPlus)
    "Total pressure at the port (i.e., homogenous volume)";

  //
  // Mass balance
  //
  m = geometry.V / v
    "Mass";
  m_liq = (1 - q) * m
    "Liquid mass";
  m_vap = q * m
    "Vapor mass";

  if type_overallMassBalance==
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial then
    dm_dtau = 0
      "Steady-state overall mass balance";

  else
    dm_dtau = geometry.V * der(rho)
      "Transient overall mass balance";

  end if;

  //
  // Energy balance
  //
  U = m * u
    "Internal energy";

  if type_energyBalance==
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial then
    dU_dtau = 0
      "Steady-state energy balance";

  else
    dU_dtau = u * dm_dtau + m * (
      phaseSepratorProperties.cv * der(T) +
      1/rho^2 * (-p + (h_vap - h_liq) / (1/rho_vap - 1/rho_liq)) * der(rho))
      "Transient energy balance";

  end if;

  if useStateLimiter then
    cfp_xMinus.h_outflow = fill(h_liq - dh, nPorts_cfp_xMinus)
      "Specific enthalpy leaving the port (i.e., homogenous volume)";
    cfp_xPlus.h_outflow = fill(h_vap + dh, nPorts_cfp_xPlus)
      "Specific enthalpy leaving the port (i.e., homogenous volume)";

  else
    cfp_xMinus.h_outflow = fill(h_liq, nPorts_cfp_xMinus)
      "Specific enthalpy leaving the port (i.e., homogenous volume)";
    cfp_xPlus.h_outflow = fill(h_vap, nPorts_cfp_xPlus)
      "Specific enthalpy leaving the port (i.e., homogenous volume)";

  end if;

  T_heatPorts = T
    "Required for conditional component";

  //
  // Summary record
  //
  phaseSepratorProperties.p = p
    "Pressure";
  phaseSepratorProperties.T = T
    "Temperature";
  phaseSepratorProperties.v = v
    "Specific volume";

  phaseSepratorProperties.l_liq = l_liq
    "Height of liquid phase";
  phaseSepratorProperties.l_liq_rel = l_liq_rel
    "Relative height of liquid phase";

  phaseSepratorProperties.Pr = if calculateAdditionalProperties then
    phaseSepratorProperties.eta * phaseSepratorProperties.cp /
    phaseSepratorProperties.lambda else 0
    "Prandtl number";

  phaseSepratorProperties.mc_flow_xMinus = sum(cfp_xMinus.m_flow)
    "Total convective mass flow rate at port '-dx/2'";
  phaseSepratorProperties.mc_flow_xPlus = sum(cfp_xPlus.m_flow)
    "Total convective mass flow rate at port '+dx/2'";

  //
  // Annotations
  //
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Rectangle(extent={{-90,30},{30,-90}}, lineColor={0,0,0}),
        Line(points={{-90,30},{-30,90}}, color={0,0,0}),
        Line(
          points={{-30,-30},{-90,-90}},
          color={0,0,0},
          pattern=LinePattern.Dash),
        Line(points={{90,-30},{30,-90}}, color={0,0,0}),
        Line(points={{90,90},{30,30}}, color={0,0,0}),
        Line(points={{-30,90},{90,90},{90,-30}}, color={0,0,0}),
        Line(
          points={{90,-30},{-30,-30}},
          color={0,0,0},
          pattern=LinePattern.Dash),
        Line(
          points={{-30,90},{-30,-30}},
          color={0,0,0},
          pattern=LinePattern.Dash),
                          Polygon(
          points={{-90,-90},{-30,-30},{-30,90},{-90,30},{-90,-90}},
          lineColor={28,108,200},
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid)}),                      Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    Documentation(info="<html>
<p>
This partial model is the base model of all phase separatpr volumes. It defines the 
volume geometry, heat ports in all spatial directions, and convective fluid ports 
in x-direction. The required heat ports can be adjusted via the volume setup. The
implementation of the fluid ports follows the 'connectorSizing' principle. Furthermore, 
this model defines fundamental variables for the momentum, mass, and energy balance, 
as well as the initialization of these balances.
<br/><br/>
Models that inherit properties from this partial model may have to add further 
convective fluid ports (e.g., y- or z-direction) and diffusive fluid ports in the 
required spatial direction. Furthermore, conservation equations must be finalized.  
In this context, appropriate fluid property models are required.
<br/><br/>
The following variables must be specified in the model that inherit properties:
</p>
<ul>
  <li>
  Pressure <i>p</i>.
  </li>
  <li>
  Density at saturated liquid line <i>&rho;<sub>liq</sub></i>.
  </li>
  <li>
  Density at saturated vapor line <i>&rho;<sub>vap</sub></i>.
  </li>
  <li>
  Specific enthalpy at saturated liquid line <i>h<sub>liq</sub></i>.
  </li>
  <li>
  Specific enthalpy at saturated vapor line <i>h<sub>vap</sub></i>.
  </li>
  <br/>
  <li>
  Mass balance using at least the variables <i>dm_dtau</i> and <i>mc_flow</i>.
  </li>
  <li>
  Sum of all convective mass flow rates <i>mc_flow</i>.
  </li>
  <br/>
  <li>
  Energy balance using at least the variables <i>dU_dtau</i>, <i>Hb_flow</i>,
  and <i>Qb_flow</i>.
  </li>
  <li>
  Sum of all enthalpy flow rates across boundaries <i>Hb_flow</i>.
  </li>
  <li>
  Sum of all heat flow rates across boundaries <i>Qb_flow</i>.
  </li>
  <br/>
  <li>
  Variables of the summary record except for pressure <i>p</i>, temperature <i>T</i>, 
  specific volume <i>v</i>, height of liquid phase <i>l_liq</i>, relative heigt of
  liquid phase <i>l_liq_rel</i>, Prandtl number <i>Pr</i>, and mass flow rates in 
  x-direction <i>mc_flow_xMinus</i>/<i>mc_flow_xPlus</i>.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  December 14, 2023, by Mirko Engelpracht:<br/>
  Major revisions due to restructering finite volumes.
  </li>
  <li>
  January 13, 2021, by Mirko Engelpracht:<br/>
  Major revisions (e.g., object-oriented approach, options) after restructuring 
  of the library.
  </li>
  <li>
  November 22, 2017, by Uwe Bau:<br/>
  Tidy up implementation and enhance documentation for publication of library.
  </li>
</ul>
</html>"));
end PartialPhaseSeparatorVolume;
