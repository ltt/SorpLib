within SorpLib.Basics.Volumes;
package BaseClasses "Base classes used to create new finite volume models"
  extends Modelica.Icons.BasesPackage;

  annotation (Documentation(info="<html>
<p>
This package contains partial basic functions and models. These partial functions
and models contain fundamental definitions of all volume models. The content of 
this package is only of interest when adding new volume models to the library.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end BaseClasses;
