within SorpLib.Basics.Volumes.BaseClasses;
partial model PartialVolume
  "Base model for all volumes"

  //
  // Definition of parameters regarding volume setup
  //
  parameter Boolean useHeatPorts = false
    " = true, if heat ports are required"
    annotation (Dialog(tab="Volume Setup", group="Heat Ports"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);
  parameter Boolean useHeatPortsX = false
    " = true, if heat ports are required in the x direction"
    annotation (Dialog(tab="Volume Setup", group="Heat Ports",
                enable=useHeatPorts),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);
  parameter Boolean useHeatPortsY = false
    " = true, if heat ports are required in the y direction"
    annotation (Dialog(tab="Volume Setup", group="Heat Ports",
                enable=useHeatPorts),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);
  parameter Boolean useHeatPortsZ = false
    " = true, if heat ports are required in the z direction"
    annotation (Dialog(tab="Volume Setup", group="Heat Ports",
                enable=useHeatPorts),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);

  //
  // Definition of parameters regarding geometry
  //
  replaceable parameter SorpLib.Basics.Volumes.Records.VolumeGeometry geometry
    constrainedby SorpLib.Basics.Volumes.Records.VolumeGeometry
    "Volume geometry"
    annotation (Dialog(tab = "General", group = "Geometry"),
                choicesAllMatching=true);

  //
  // Definition of advanced parameters
  //
  parameter SorpLib.Choices.BalanceEquations type_energyBalance=
    SorpLib.Choices.BalanceEquations.TransientFixedInitial
    "Handling of the energy balance and corresponding initialisation"
    annotation (Dialog(tab = "Advanced", group = "Conservation Equations"),
                Evaluate=true,
                HideResult = true);

  parameter Boolean avoid_events = false
    "= true, if events are avoid by using noEvent()-operator"
    annotation (Dialog(tab = "Advanced", group = "Numerics"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult = true);

  //
  // Definition of ports
  //
  SorpLib.Basics.Interfaces.HeatPorts.HeatPort_in hp_xMinus(
    final T=T_xMinus,
    final Q_flow=Q_flow_xMinus) if
    useHeatPorts and useHeatPortsX
    "Heat port at position '-dx/2'"
    annotation (Placement(transformation(extent={{-66,-6},{-54,6}}),
                                  iconTransformation(extent={{-66,-6},{-54,6}})));
  SorpLib.Basics.Interfaces.HeatPorts.HeatPort_out hp_xPlus(
    final T=T_xPlus,
    final Q_flow=Q_flow_xPlus) if
    useHeatPorts and useHeatPortsX
    "Heat port at position '+dx/2'"
    annotation (Placement(transformation(extent={{54,-6},{66,6}}),
                                iconTransformation(extent={{54,-6},{66,6}})));

  SorpLib.Basics.Interfaces.HeatPorts.HeatPort_in hp_yMinus(
    final T=T_yMinus,
    final Q_flow=Q_flow_yMinus) if
    useHeatPorts and useHeatPortsY "Heat port at position '-dy/2'"
    annotation (Placement(transformation(extent={{-6,-66},{6,-54}}),
                                 iconTransformation(extent={{-6,-66},{6,-54}})));
  SorpLib.Basics.Interfaces.HeatPorts.HeatPort_out hp_yPlus(
    final T=T_yPlus,
    final Q_flow=Q_flow_yPlus) if
    useHeatPorts and useHeatPortsY
    "Heat port at position '+dy/2'"
    annotation (Placement(transformation(extent={{-6,54},{6,66}}),
                               iconTransformation(extent={{-6,54},{6,66}})));

  SorpLib.Basics.Interfaces.HeatPorts.HeatPort_in hp_zMinus(
    final T=T_zMinus,
    final Q_flow=Q_flow_zMinus) if
    useHeatPorts and useHeatPortsZ
    "Heat port at position '-dz/2'"
    annotation (Placement(transformation(extent={{24,24},{36,36}}),
                               iconTransformation(extent={{24,24},{36,36}})));
  SorpLib.Basics.Interfaces.HeatPorts.HeatPort_out hp_zPlus(
    final T=T_zPlus,
    final Q_flow=Q_flow_zPlus) if
    useHeatPorts and useHeatPortsZ
    "Heat port at position '+dz/2'"
    annotation (Placement(transformation(extent={{-36,-36},{-24,-24}}),
                                   iconTransformation(extent={{-36,-36},{-24,-24}})));

  //
  // Definition of protected variables
  //
protected
  Modelica.Units.SI.Temperature T_heatPorts
    "Required for conditional connectors";

  Modelica.Units.SI.Temperature T_xMinus
    "Required for conditional connector";
  Modelica.Units.SI.Temperature T_xPlus
    "Required for conditional connector";

  Modelica.Units.SI.Temperature T_yMinus
    "Required for conditional connector";
  Modelica.Units.SI.Temperature T_yPlus
    "Required for conditional connector";

  Modelica.Units.SI.Temperature T_zMinus
    "Required for conditional connector";
  Modelica.Units.SI.Temperature T_zPlus
    "Required for conditional connector";

  Modelica.Units.SI.HeatFlowRate Q_flow_xMinus
    "Required for conditional connector";
  Modelica.Units.SI.HeatFlowRate Q_flow_xPlus
    "Required for conditional connector";

  Modelica.Units.SI.HeatFlowRate Q_flow_yMinus
    "Required for conditional connector";
  Modelica.Units.SI.HeatFlowRate Q_flow_yPlus
    "Required for conditional connector";

  Modelica.Units.SI.HeatFlowRate Q_flow_zMinus
    "Required for conditional connector";
  Modelica.Units.SI.HeatFlowRate Q_flow_zPlus
    "Required for conditional connector";

equation
  //
  // Assertations
  //
  if not useHeatPorts then
    assert(not (useHeatPortsX or useHeatPortsY or useHeatPortsZ),
      "Deactivate individual heat ports if heat ports are not considered!",
      level = AssertionLevel.error);
  end if;

  //
  // Variables of heat ports
  //
  T_xMinus = T_heatPorts
    "Required for conditional connector";
  T_xPlus = T_heatPorts
    "Required for conditional connector";

  T_yMinus = T_heatPorts
    "Required for conditional connector";
  T_yPlus = T_heatPorts
    "Required for conditional connector";

  T_zMinus = T_heatPorts
    "Required for conditional connector";
  T_zPlus = T_heatPorts
    "Required for conditional connector";

  if not useHeatPortsX then
    Q_flow_xMinus = 0
      "Required for conditional connector";
    Q_flow_xPlus = 0
      "Required for conditional connector";
  end if;

  if not useHeatPortsY then
    Q_flow_yMinus = 0
      "Required for conditional connector";
    Q_flow_yPlus = 0
      "Required for conditional connector";
  end if;

  if not useHeatPortsZ then
    Q_flow_zMinus = 0
      "Required for conditional connector";
    Q_flow_zPlus = 0
      "Required for conditional connector";
  end if;

  //
  // Annotations
  //
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Rectangle(extent={{-90,30},{30,-90}}, lineColor={0,0,0}),
        Line(points={{-90,30},{-30,90}}, color={0,0,0}),
        Line(
          points={{-30,-30},{-90,-90}},
          color={0,0,0},
          pattern=LinePattern.Dash),
        Line(points={{90,-30},{30,-90}}, color={0,0,0}),
        Line(points={{90,90},{30,30}}, color={0,0,0}),
        Line(points={{-30,90},{90,90},{90,-30}}, color={0,0,0}),
        Line(
          points={{90,-30},{-30,-30}},
          color={0,0,0},
          pattern=LinePattern.Dash),
        Line(
          points={{-30,90},{-30,-30}},
          color={0,0,0},
          pattern=LinePattern.Dash),
        Line(
          points={{-60,-60},{60,-60},{60,60},{-60,60},{-60,-60}},
          color={0,0,0},
          pattern=LinePattern.Dot),
        Line(
          points={{30,-30},{-30,-90},{-30,30},{30,90},{30,30}},
          color={0,0,0},
          pattern=LinePattern.Dot),
        Line(
          points={{-30,30},{-90,-30},{-30,-30}},
          color={0,0,0},
          pattern=LinePattern.Dot),
        Line(
          points={{30,-30},{90,30},{30,30}},
          color={0,0,0},
          pattern=LinePattern.Dot)}),                            Diagram(
        coordinateSystem(preserveAspectRatio=false), graphics={
        Line(
          points={{80,-90},{100,-90}},
          color={0,0,0},
          arrow={Arrow.None,Arrow.Filled}),
        Line(
          points={{80,-90},{80,-70}},
          color={0,0,0},
          arrow={Arrow.None,Arrow.Filled}),
        Line(
          points={{80,-90},{70,-100}},
          color={0,0,0},
          arrow={Arrow.None,Arrow.Filled}),
        Text(
          extent={{96,-98},{100,-94}},
          lineColor={0,0,0},
          fontSize=24,
          textString="x",
          textStyle={TextStyle.Bold}),
        Text(
          extent={{84,-74},{88,-70}},
          lineColor={0,0,0},
          fontSize=24,
          textStyle={TextStyle.Bold},
          textString="y"),
        Text(
          extent={{76,-100},{80,-96}},
          lineColor={0,0,0},
          fontSize=24,
          textStyle={TextStyle.Bold},
          textString="z"),
        Rectangle(extent={{-90,30},{30,-90}}, lineColor={0,0,0}),
        Line(points={{-90,30},{-30,90}}, color={0,0,0}),
        Line(
          points={{-30,-30},{-90,-90}},
          color={0,0,0},
          pattern=LinePattern.Dash),
        Line(points={{90,-30},{30,-90}}, color={0,0,0}),
        Line(points={{90,90},{30,30}}, color={0,0,0}),
        Line(points={{-30,90},{90,90},{90,-30}}, color={0,0,0}),
        Line(
          points={{90,-30},{-30,-30}},
          color={0,0,0},
          pattern=LinePattern.Dash),
        Line(
          points={{-30,90},{-30,-30}},
          color={0,0,0},
          pattern=LinePattern.Dash),
        Line(
          points={{-60,-60},{60,-60},{60,60},{-60,60},{-60,-60}},
          color={0,0,0},
          pattern=LinePattern.Dot),
        Line(
          points={{30,-30},{-30,-90},{-30,30},{30,90},{30,30}},
          color={0,0,0},
          pattern=LinePattern.Dot),
        Line(
          points={{-30,30},{-90,-30},{-30,-30}},
          color={0,0,0},
          pattern=LinePattern.Dot),
        Line(
          points={{30,-30},{90,30},{30,30}},
          color={0,0,0},
          pattern=LinePattern.Dot)}),
    Documentation(info="<html>
<p>
This partial model is the base model of all finite volumes. It defines the volume 
geometry and heat ports in all spatial directions. The required heat ports can be 
adjusted via the volume setup.
<br/><br/>
Models that inherit properties from this partial model may have to add fluid ports
and corresponding parameters to specify the fluid port setup. Furthermore, conservation
equations and corresponding variables must be added. In this context, fluid property
models may be required. Finally, records summarizing thermodynamic volume properties 
should be added.
<br/><br/>
The following variables must be specified in the model that inherit properties:
</p>
<ul>
  <li>
  The variable <i>T_heatPorts</i> that defines the temperature at the heat ports.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  December 6, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end PartialVolume;
