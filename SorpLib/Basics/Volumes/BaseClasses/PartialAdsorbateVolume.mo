within SorpLib.Basics.Volumes.BaseClasses;
partial model PartialAdsorbateVolume
  "Base model for all adsorbate volumes"

  //
  // Definition of parameters describing media
  //
  parameter Integer no_components = 1
    "Number of components in the medium"
    annotation (Dialog(tab="General", group="Media"),
                Evaluate=true,
                HideResult=true);
  parameter Integer no_adsorptivs(min=1, max=no_components) = 1
    "Number of adsorptivs (i.e., components that can be adsorbed/desorbed)"
    annotation (Dialog(tab="General", group="Media"),
                Evaluate=true,
                HideResult=true);

  //
  // Definition of parameters regarding start values
  //
  parameter Modelica.Units.SI.Mass m_sor_initial = geometry.V * 725
    "Initial value of adsorbent mass"
    annotation (Dialog(tab="Initialisation", group="Initial Values"));
  parameter Modelica.Units.SI.Temperature T_initial = 298.15
    "Initial value of temperature"
    annotation (Dialog(tab="Initialisation", group="Initial Values"));

  parameter Modelica.Units.SI.MassFlowRate msor_flow_initialX = 1e-4
    "Start value for adsorbent mass flow rate in x direction"
    annotation (Dialog(tab="Initialisation", group="Start Values"));
  parameter Modelica.Units.SI.MassFlowRate ms_flow_initial = 1e-4
    "Start value for sorption mass flow rate"
    annotation (Dialog(tab="Initialisation", group="Start Values"));
  parameter Modelica.Units.SI.MassFlowRate md_flow_initialX = 1e-4
    "Start value for diffusive mass flow rates in x direction"
    annotation (Dialog(tab="Initialisation", group="Start Values"));

  extends SorpLib.Basics.Volumes.BaseClasses.PartialVolume;

  //
  // Definition of parameters regarding calculation setup
  //
  parameter Boolean calcUptakeAveragedProperties = false
    " = true, if uptake-averaged adsorpt and abosrabte properties are calculated"
    annotation (Dialog(tab="General", group="Calculation Setup"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);

  //
  // Definition of parameters regarding volume setup
  //
  parameter Boolean useAdsorbatePorts = false
    " = true, if adsorbate ports are connected outside the model and, thus,
    are considered within the mass and energy balance (i.e., uptake-averaged 
    adsorbate properties are required)"
    annotation (Dialog(tab="Volume Setup", group="Fluid Ports",
                enable=calcUptakeAveragedProperties),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);
  parameter Boolean useDiffusivePorts = false
    " = true, if diffusive ports are connected outside the model (i.e., uptake-
    averaged adsorpt properties are required)"
    annotation (Dialog(tab="Volume Setup", group="Fluid Ports",
                enable=calcUptakeAveragedProperties),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);

  //
  // Definition of advanced parameters
  //
  parameter SorpLib.Choices.BalanceEquations type_adsorbentMassBalance=
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial
    "Handling of the adsorbent mass balance and corresponding initialisation"
    annotation (Dialog(tab = "Advanced", group = "Conservation Equations",
                enable=useAdsorbatePorts),
                Evaluate=true,
                HideResult=true);
  parameter SorpLib.Choices.BalanceEquations type_adsorptMassBalance=
    SorpLib.Choices.BalanceEquations.TransientFixedInitial
    "Handling of the adsorpt mass balances and corresponding initialisation"
    annotation (Dialog(tab = "Advanced", group = "Conservation Equations"),
                Evaluate=true,
                HideResult=true);

  //
  // Definition of ports
  //
  SorpLib.Basics.Interfaces.SorptionPorts.AdsorbatePort_in ap_xMinus(
    final no_adsorptivs=no_adsorptivs,
    m_flow(start=msor_flow_initialX))
    "Adsorbate fluid port at position '-dx/2'"
    annotation (Placement(transformation(extent={{-46,14},{-38,22}}),
                iconTransformation(extent={{-46,14},{-38,22}})));
  SorpLib.Basics.Interfaces.SorptionPorts.AdsorbatePort_in ap_xPlus(
    final no_adsorptivs=no_adsorptivs,
    m_flow(start=-msor_flow_initialX))
    "Adsorbate fluid port at position '+dx/2'"
    annotation (Placement(transformation(extent={{74,14},{82,22}}),
                iconTransformation(extent={{74,14},{82,22}})));

  SorpLib.Basics.Interfaces.SorptionPorts.AdsorptPort_in[no_adsorptivs] dp_xMinus(
    m_flow(each start=-md_flow_initialX))
    "Diffusive fluid port at position '-dx/2'"
    annotation (Placement(transformation(extent={{-82,-22},{-74,-14}}),
                iconTransformation(extent={{-82,-22},{-74,-14}})));
  SorpLib.Basics.Interfaces.SorptionPorts.AdsorptPort_out[no_adsorptivs] dp_xPlus(
    m_flow(each start=md_flow_initialX))
    "Diffusive fluid port at position '+dx/2'"
    annotation (Placement(transformation(extent={{38,-22},{46,-14}}),
                                iconTransformation(extent={{38,-22},{46,-14}})));

  SorpLib.Basics.Interfaces.HeatPorts.HeatPort_in hp_sorption
    "Sorption heat port"
    annotation (Placement(transformation(extent={{10,70},{22,82}}),
                iconTransformation(extent={{10,70},{22,82}})));

  //
  // Definition of variables
  //
  Modelica.Units.SI.Pressure p
    "Pressure";
  Modelica.Units.SI.Temperature T(
    start=T_initial,
    stateSelect=StateSelect.prefer)
    "Temperature";
  SorpLib.Units.Uptake x
    "Uptake";

  Modelica.Units.SI.SpecificEnthalpy h(
    stateSelect=StateSelect.avoid)
    "Uptake-averaged specific enthalpy: h_sor + x * h_avg_adsorpt";
  Modelica.Units.SI.SpecificInternalEnergy u
    "Uptake-averaged specific internal energy: u_sor + x * u_avg_adsorpt";

  Modelica.Units.SI.Mass m_adsorbate
    "Adsorbate mass (i.e., total mass)";
  Modelica.Units.SI.Mass m_sor(
    start=m_sor_initial,
    stateSelect= if useAdsorbatePorts then
      StateSelect.prefer else StateSelect.avoid)
    "Adsorbent mass";
  Modelica.Units.SI.Mass m_adsorpt
    "Adsorpt mass";

  Modelica.Units.SI.MassFlowRate dm_adsorbate_dtau
    "Derivative of adsorbate mass (i.e., total mass) w.r.t. time";
  Modelica.Units.SI.MassFlowRate dm_sor_dtau
    "Derivative of adsorbent mass w.r.t. time";
  Modelica.Units.SI.MassFlowRate dm_adsorpt_dtau
    "Derivative of adsorpt mass w.r.t. time";

  Modelica.Units.SI.MassFlowRate masor_flow
    "Sum of all adsorbent mass flow rates across adsorbate boundaries";
  Modelica.Units.SI.MassFlowRate mas_flow
    "Sum of all sorption mass flow rates across adsorbate boundaries";
  Modelica.Units.SI.MassFlowRate ms_flow
    "Sum of all sorption mass flow rates across boundaries";
  Modelica.Units.SI.MassFlowRate md_flow
    "Sum of all diffusive mass flow rates across boundaries";

  Modelica.Units.SI.InternalEnergy U
    "Uptake-averaged internal energy";
  Real dU_dtau(final unit="W")
    "Derivative of uptake-averaged internal energy w.r.t. time";
  Modelica.Units.SI.EnthalpyFlowRate Hb_flow
    "Sum of all enthalpy flow rates across boundaries";
  Modelica.Units.SI.HeatFlowRate Qb_flow
    "Sum of all heat flow rates across boundaries";

  SorpLib.Basics.Volumes.Records.AdsorbateVolumeProperties adsorbateProperties(
    final no_adsorptivs=no_adsorptivs)
    "Properties of fluid volume often required for calculation of heat transfer";

initial equation
  if useAdsorbatePorts then
    if type_adsorbentMassBalance ==
      SorpLib.Choices.BalanceEquations.TransientFixedInitial then
      m_sor = m_sor_initial
        "Fixed initial value";

    elseif type_adsorbentMassBalance ==
      SorpLib.Choices.BalanceEquations.TransientSteadyStateInitial then
      der(m_sor) = 0
        "Steady-state initialisation of overall mass balance";

    end if;
  end if;

  if type_energyBalance ==
    SorpLib.Choices.BalanceEquations.TransientFixedInitial then
    T = T_initial
      "Fixed initial value";

  elseif type_energyBalance ==
    SorpLib.Choices.BalanceEquations.TransientSteadyStateInitial then
    der(T) = 0
      "Steady-state initial value";

  end if;

equation
  //
  // Momentum balance
  //
  ap_xMinus.p = p
    "Total pressure at the port (i.e., homogenous volume)";
  ap_xPlus.p = p
    "Total pressure at the port (i.e., homogenous volume)";

  //
  // Mass balance
  //
  m_adsorbate = m_sor + m_adsorpt
    "Adsorbate mass (i.e., total mass)";
  m_adsorpt = x * m_sor
    "Adsorpt mass";

  dm_adsorbate_dtau = dm_adsorpt_dtau + dm_sor_dtau
    "Adsorbate mass balance (i.e., total mass balance)";
  dm_sor_dtau = masor_flow
    "Derivative of adsorbent mass w.r.t. time";
  dm_adsorpt_dtau = ms_flow + md_flow + mas_flow
  "Adsorpt mass balance";

  if useAdsorbatePorts then
    if type_adsorbentMassBalance==
      SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial then
      dm_sor_dtau = 0
        "Steady-state adsorbent mass balance";

    else
      dm_sor_dtau = der(m_sor)
        "Transient adsorbent mass balance";

    end if;

  else
    m_sor = m_sor_initial
        "Sorbent mass cannot change as adsorbate ports are not used";

  end if;

  //
  // Energy balance
  //
  U = m_adsorbate * u
    "Uptake-averaged internal energy";

  dU_dtau = Hb_flow + Qb_flow
    "Energy balane";

  Qb_flow = Q_flow_xMinus + Q_flow_xPlus +
    Q_flow_yMinus + Q_flow_yPlus +
    Q_flow_zMinus + Q_flow_zPlus +
    hp_sorption.Q_flow
    "Sum of all heat flow rates across boundaries";

  T_heatPorts = T
    "Required for conditional component";

  ap_xMinus.h_outflow = h
    "Specific enthalpy leaving the port (i.e., homogenous volume)";
  ap_xPlus.h_outflow = h
    "Specific enthalpy leaving the port (i.e., homogenous volume)";

  hp_sorption.T = T_heatPorts
    "Temperature at sorption heat port";

  //
  // Summary record
  //
  adsorbateProperties.p = p
    "Pressure";
  adsorbateProperties.T = T
    "Temperature";
  adsorbateProperties.x = x
    "Total loading";

  adsorbateProperties.ma_flow_xMinus = if useAdsorbatePorts then ap_xMinus.m_flow *
    (1 + (if avoid_events then sum(noEvent(actualStream(ap_xMinus.x_outflow)))
    else sum(actualStream(ap_xMinus.x_outflow)))) else 0
    "Adsorbate mass flow rate at port '-dx/2'";
  adsorbateProperties.ma_flow_xPlus = if useAdsorbatePorts then ap_xPlus.m_flow *
    (1 + (if avoid_events then sum(noEvent(actualStream(ap_xPlus.x_outflow)))
    else sum(actualStream(ap_xPlus.x_outflow)))) else 0
    "Adsorbate mass flow rate at port '+dx/2'";

  adsorbateProperties.md_flow_xMinus = dp_xMinus.m_flow
    "Diffusive mass flow rate at port '-dx/2'";
  adsorbateProperties.md_flow_xPlus = dp_xPlus.m_flow
    "Diffusive mass flow rate at port '+dx/2'";

  //
  // Annotations
  //
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Rectangle(extent={{-90,30},{30,-90}}, lineColor={0,0,0}),
        Line(points={{-90,30},{-30,90}}, color={0,0,0}),
        Line(
          points={{-30,-30},{-90,-90}},
          color={0,0,0},
          pattern=LinePattern.Dash),
        Line(points={{90,-30},{30,-90}}, color={0,0,0}),
        Line(points={{90,90},{30,30}}, color={0,0,0}),
        Line(points={{-30,90},{90,90},{90,-30}}, color={0,0,0}),
        Line(
          points={{90,-30},{-30,-30}},
          color={0,0,0},
          pattern=LinePattern.Dash),
        Line(
          points={{-30,90},{-30,-30}},
          color={0,0,0},
          pattern=LinePattern.Dash)}),                           Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    Documentation(info="<html>
<p>
This partial model is the base model of all adsorbate volumes. It defines the 
volume geometry, heat ports in all spatial directions, as well as adsorbate and
diffusive (i.e., adsorpt) ports in x-direction. The required heat ports can be 
adjusted via the volume setup. Furthermore, this model defines fundamental variables 
for the momentum, mass, and energy balance, as well as the initialization of these 
balances.
<br/><br/>
Models that inherit properties from this partial model must add a sorption port. 
Further, inherting models may have to add further adsorbate and diffusive (i.e., 
adsorpt) ports in the required spatial direction (e.g., y- or z-direction). 
Furthermore, conservation equations must be added. In this context, appropriate 
working pair models are required.
<br/><br/>
The following variables must be specified in the model that inherit properties:
</p>
<ul>
  <li>
  Pressure <i>p</i> or loading <i>x</i> in dependence of the independent state
  variables.
  </li>
  <li>
  Uptake-averaged specific enthalpy <i>h</i>.
  </li>
  <li>
  Uptake-averaged specific internal energy <i>u</i>.
  </li>
  <br/>
  <li>
  Partial derivative of the adsorpt mass w.r.t. time <i>dm_adsorpt_dtau</i>.
  </li>
  <li>
  Sum of all adsorbent mass flow rates across adsorbate boundaries <i>masor_flow</i>.
  </li>
  <li>
  Sum of all adsorpt mass flow rates across adsorbate boundaries <i>mas_flow</i>.
  </li>
  <li>
  Sum of all sorption mass flow rates across sorption boundaries <i>ms_flow</i>.
  </li>
  <br/>
  <li>
  Partial derivative of the uptake-averaged internal energy w.r.t. time <i>dU_dtau</i>.
  </li>
  <li>
  Sum of all enthalpy flow rates across boundaries <i>Hb_flow</i>.
  </li>
  <br/>
  <li>
  Variables of the summary record except for pressure <i>p</i>, temperature <i>T</i>, 
  total loading <i>x</i>, adsorbate mass flow rates in x-direction 
  <i>ma_flow_xMinus</i>/<i>ma_flow_xPlus</i>, and diffusive mass flow rates in 
  x-direction <i>md_flow_xMinus</i>/<i>md_flow_xPlus</i>.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  December 15, 2023, by Mirko Engelpracht:<br/>
  Major revisions due to restructering finite volumes.
  </li>
  <li>
  January 12, 2021, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end PartialAdsorbateVolume;
