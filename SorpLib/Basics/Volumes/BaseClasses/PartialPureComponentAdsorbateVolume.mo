within SorpLib.Basics.Volumes.BaseClasses;
partial model PartialPureComponentAdsorbateVolume
  "Base model for all adsorbate models for pure component adsorption"
  extends SorpLib.Basics.Volumes.BaseClasses.PartialAdsorbateVolume(
    final no_components=Medium.nX,
    final no_adsorptivs=1,
    p(final start=p_initial,
      final stateSelect= if independentStateVariables==
      SorpLib.Choices.IndependentVariablesPureComponentWorkingPair.pT then
      StateSelect.prefer else StateSelect.avoid),
    x(final start=x_initial,
      final stateSelect= if independentStateVariables==
      SorpLib.Choices.IndependentVariablesPureComponentWorkingPair.xT then
      StateSelect.prefer else StateSelect.avoid),
    useHeatPorts=true,
    useHeatPortsX=true,
    type_adsorbentMassBalance=
      SorpLib.Choices.BalanceEquations.TransientFixedInitial,
    type_adsorptMassBalance=
      SorpLib.Choices.BalanceEquations.TransientFixedInitial,
    type_energyBalance=
      SorpLib.Choices.BalanceEquations.TransientFixedInitial);

  //
  // Definition of general parameters
  //
  parameter Integer nSorptionPorts = 0
    "Number of sorption ports"
    annotation (Dialog(connectorSizing=true),
                HideResult = true);

  //
  // Definition of parameters regarding the medium
  //
  replaceable package Medium = Modelica.Media.Water.StandardWater
    constrainedby Modelica.Media.Interfaces.PartialMedium
    "Medium model of the fluid"
    annotation (Dialog(tab="General", group="Media"),
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);
  replaceable model PureWorkingPairModel =
      SorpLib.Media.WorkingPairs.PureComponents.H2O.AQSOAZ02_DubininLorentzianCumulative_Goldsworthy2014_Bau2017_VLE
    constrainedby
    SorpLib.Media.WorkingPairs.BaseClasses.PartialPureWorkingPairs
    "Pure working pair model"
    annotation (Dialog(tab="General", group="Media"),
                choicesAllMatching = true,
                Evaluate=true,
                HideResult=true);

  //
  // Definition of parameters regarding calculation setup
  //
  parameter SorpLib.Choices.IndependentVariablesPureComponentWorkingPair independentStateVariables=
    SorpLib.Choices.IndependentVariablesPureComponentWorkingPair.xT
    "Independent state variables"
    annotation (Dialog(tab="General", group="Calculation Setup"),
                Evaluate=true,
                HideResult = true);

  //
  // Definition of parameters regarding start values
  //
  parameter Modelica.Units.SI.Pressure p_initial = 2e3
    "Initial value of pressure"
    annotation (Dialog(tab="Initialisation", group="Initial Values",
                enable=(independentStateVariables==
                  SorpLib.Choices.IndependentVariablesPureComponentWorkingPair.pT)));
  parameter SorpLib.Units.Uptake x_initial = 0.3
    "Initial value of uptake"
    annotation (Dialog(tab="Initialisation", group="Initial Values",
                enable=(independentStateVariables==
                  SorpLib.Choices.IndependentVariablesPureComponentWorkingPair.xT)));

  //
  // Definition of ports
  //
  replaceable SorpLib.Basics.Interfaces.FluidPorts.VLEPort_in[nSorptionPorts] fp_sorption
  constrainedby SorpLib.Basics.Interfaces.BaseClasses.PartialFluidPort(
    each final no_components=no_components,
    each m_flow(start=ms_flow_initial))
    "Sorption fluid port"
    annotation (Placement(transformation(extent={{-20,40},{-12,48}}),
                iconTransformation(extent={{-20,40},{-12,48}})));

  //
  // Definition of variables
  //
  PureWorkingPairModel workingPair(
    redeclare final package Medium = Medium,
    final stateVariables=independentStateVariables,
    final calcCaloricProperties=true,
    final calcAdsorptAdsorbateState=calcUptakeAveragedProperties or
      useAdsorbatePorts or useDiffusivePorts,
    final calcDerivativesIsotherm=true,
    final calcDerivativesMassEnergyBalance=true,
    final p_adsorpt=p,
    final x_adsorpt=x,
    final T_adsorpt=T,
    final avoidEvents=avoid_events)
    "Thermodynamic properties and partial derivatives of the pure working pair";

initial equation
  if type_adsorptMassBalance ==
    SorpLib.Choices.BalanceEquations.TransientFixedInitial then
    if independentStateVariables==
      SorpLib.Choices.IndependentVariablesPureComponentWorkingPair.pT then
      p = p_initial
        "Fixed initial value";

    else
      x = x_initial
        "Fixed initial value";

    end if;

  elseif type_adsorptMassBalance ==
    SorpLib.Choices.BalanceEquations.TransientSteadyStateInitial then
    if independentStateVariables==
      SorpLib.Choices.IndependentVariablesPureComponentWorkingPair.pT then
      der(p) = 0
        "Steady-state initialisation of overall mass balance";

    else
      der(x) = 0
        "Steady-state initialisation of overall mass balance";

    end if;
  end if;

equation
  //
  // Assertations
  //
  assert(no_components <= 1,
    "The VLE adsorbate volume model can only handel pure fluids (i.e., with one " +
    "component)!",
    level = AssertionLevel.error);

  assert(no_adsorptivs <= 1,
    "The VLE adsorbate volume model can only handel one adsorptive!",
    level = AssertionLevel.error);

  assert(not (
    type_energyBalance<>SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial and
    (type_adsorbentMassBalance==SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial and
    type_adsorptMassBalance==SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial)),
    "Steady-state mass balances combined with transient energy balance is not " +
    "sound if the fluid volume is fixed!",
    level = AssertionLevel.warning);

  if useAdsorbatePorts then
    assert(calcUptakeAveragedProperties,
      "When using adsorbate ports, uptake-averaged properties must be calculated!",
      level = AssertionLevel.error);
  end if;

  if useDiffusivePorts then
    assert(calcUptakeAveragedProperties,
      "When using diffusive ports, uptake-averaged properties must be calculated!",
      level = AssertionLevel.error);
  end if;

  //
  // Calculation of properties
  //
  h = workingPair.state_averageAdsorbate.h
    "Uptake-averaged specific enthalpy: h_sor + x * h_avg_adsorpt";
  u = workingPair.state_averageAdsorbate.u
    "Uptake-averaged specific internal energy: u_sor + x * u_avg_adsorpt";

  //
  // Momentum balance
  //
  dp_xMinus[1].x = x
    "Loading at the port (i.e., homogenous volume)";
  dp_xPlus[1].x = x
    "Loading at the port (i.e., homogenous volume)";

  fp_sorption.p = fill(p, nSorptionPorts)
    "Total pressure at the port (i.e., homogenous volume)";

  //
  // Mass balance
  //
  if type_adsorptMassBalance==
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial then
    dm_adsorpt_dtau = 0
      "Steady-state adsorpt mass balance";

  else
    if useAdsorbatePorts then
      if independentStateVariables==
        SorpLib.Choices.IndependentVariablesPureComponentWorkingPair.pT then
        dm_adsorpt_dtau = dm_sor_dtau * x + m_sor * (
          workingPair.derivatives_isotherm.dx_dp_T * der(p) +
          workingPair.derivatives_isotherm.dx_dT_p * der(T))
          "Transient adsorpt mass balance";

      else
        dm_adsorpt_dtau = dm_sor_dtau * x + m_sor * der(x)
          "Transient adsorpt mass balance";

      end if;

    else
      if independentStateVariables==
        SorpLib.Choices.IndependentVariablesPureComponentWorkingPair.pT then
        dm_adsorpt_dtau = m_sor * (
          workingPair.derivatives_isotherm.dx_dp_T * der(p) +
          workingPair.derivatives_isotherm.dx_dT_p * der(T))
          "Transient adsorpt mass balance";

      else
        dm_adsorpt_dtau = m_sor * der(x)
          "Transient adsorpt mass balance";

      end if;

    end if;
  end if;

  ap_xMinus.x_outflow = {x}
    "Loading leaving the port (i.e., homogenous volume)";
  ap_xPlus.x_outflow = {x}
    "Loading leaving the port (i.e., homogenous volume)";

  //
  // Energy balance
  //
  if type_energyBalance==
    SorpLib.Choices.BalanceEquations.SteadyStateFreeInitial then
    dU_dtau = 0
      "Steady-state energy balance";

  else
    if useAdsorbatePorts then
      if independentStateVariables==
        SorpLib.Choices.IndependentVariablesPureComponentWorkingPair.pT then
        dU_dtau = dm_sor_dtau * u + m_sor * der(T) * (
          (workingPair.derivatives_massEnergy.dh_sorbent_dT_p -
           p * workingPair.derivatives_massEnergy.dv_sorbent_dT_p) +
          (workingPair.derivatives_massEnergy.dxh_avg_adsorpt_dT_p -
           p * workingPair.derivatives_massEnergy.dxv_avg_adsorpt_dT_p)) +
           m_sor * der(p) * (
          (workingPair.derivatives_massEnergy.dh_sorbent_dp_T -
           p * workingPair.derivatives_massEnergy.dv_sorbent_dp_T) +
          (workingPair.derivatives_massEnergy.dxh_avg_adsorpt_dp_T -
           p * workingPair.derivatives_massEnergy.dxv_avg_adsorpt_dp_T) -
          (workingPair.state_sorbent.v + x *
          workingPair.state_lastAdsorbedMolecule.v))
          "Transient energy balance";

      else
        dU_dtau = dm_sor_dtau * u + m_sor * der(T) * (
          (workingPair.derivatives_massEnergy.dh_sorbent_dT_x -
           p * workingPair.derivatives_massEnergy.dv_sorbent_dT_x -
           workingPair.state_sorbent.v * workingPair.derivatives_isotherm.dp_dT_x) +
          (workingPair.derivatives_massEnergy.dxh_avg_adsorpt_dT_x -
           p * workingPair.derivatives_massEnergy.dxv_avg_adsorpt_dT_x -
           x * workingPair.state_lastAdsorbedMolecule.v *
           workingPair.derivatives_isotherm.dp_dT_x)) +
           m_sor * der(x) * (
          (workingPair.derivatives_massEnergy.dh_sorbent_dx_T -
           p * workingPair.derivatives_massEnergy.dv_sorbent_dx_T -
           workingPair.state_sorbent.v * workingPair.derivatives_isotherm.dp_dx_T) +
          (workingPair.derivatives_massEnergy.dxh_avg_adsorpt_dx_T -
           p * workingPair.derivatives_massEnergy.dxv_avg_adsorpt_dx_T -
           x * workingPair.state_lastAdsorbedMolecule.v *
           workingPair.derivatives_isotherm.dp_dx_T))
          "Transient energy balance";

      end if;

    else
      if independentStateVariables==
        SorpLib.Choices.IndependentVariablesPureComponentWorkingPair.pT then
        dU_dtau = m_sor * der(T) * (
          (workingPair.derivatives_massEnergy.dh_sorbent_dT_p -
           p * workingPair.derivatives_massEnergy.dv_sorbent_dT_p) +
          (workingPair.derivatives_massEnergy.dxh_avg_adsorpt_dT_p -
           p * workingPair.derivatives_massEnergy.dxv_avg_adsorpt_dT_p)) +
           m_sor * der(p) * (
          (workingPair.derivatives_massEnergy.dh_sorbent_dp_T -
           p * workingPair.derivatives_massEnergy.dv_sorbent_dp_T) +
          (workingPair.derivatives_massEnergy.dxh_avg_adsorpt_dp_T -
           p * workingPair.derivatives_massEnergy.dxv_avg_adsorpt_dp_T) -
          (workingPair.state_sorbent.v + x *
          workingPair.state_lastAdsorbedMolecule.v))
          "Transient energy balance";

      else
        dU_dtau = m_sor * der(T) * (
          (workingPair.derivatives_massEnergy.dh_sorbent_dT_x -
           p * workingPair.derivatives_massEnergy.dv_sorbent_dT_x -
           workingPair.state_sorbent.v * workingPair.derivatives_isotherm.dp_dT_x) +
          (workingPair.derivatives_massEnergy.dxh_avg_adsorpt_dT_x -
           p * workingPair.derivatives_massEnergy.dxv_avg_adsorpt_dT_x -
           x * workingPair.state_lastAdsorbedMolecule.v *
           workingPair.derivatives_isotherm.dp_dT_x)) +
           m_sor * der(x) * (
          (workingPair.derivatives_massEnergy.dh_sorbent_dx_T -
           p * workingPair.derivatives_massEnergy.dv_sorbent_dx_T -
           workingPair.state_sorbent.v * workingPair.derivatives_isotherm.dp_dx_T) +
          (workingPair.derivatives_massEnergy.dxh_avg_adsorpt_dx_T -
           p * workingPair.derivatives_massEnergy.dxv_avg_adsorpt_dx_T -
           x * workingPair.state_lastAdsorbedMolecule.v *
           workingPair.derivatives_isotherm.dp_dx_T))
          "Transient energy balance";

      end if;

    end if;
  end if;

  dp_xMinus[1].h_outflow = workingPair.state_averageAdsorpt.h
    "Specific enthalpy leaving the port (i.e., homogenous volume)";
  dp_xPlus[1].h_outflow = workingPair.state_averageAdsorpt.h
    "Specific enthalpy leaving the port (i.e., homogenous volume)";

  fp_sorption.h_outflow = fill(workingPair.state_adsorptive.h, nSorptionPorts)
    "Specific enthalpy leaving the port (i.e., homogenous volume)";

  //
  // Summary record
  //
  adsorbateProperties.x_i[1] = x
    "Loading of each component";

  adsorbateProperties.cp = workingPair.cp_adsorpt
    "Specific heat capacity";
  adsorbateProperties.dh_ads = workingPair.h_ads
    "Specific enthalpy of adsorption";

  adsorbateProperties.m_flow_sorption = ms_flow
    "Total mass flow rate at port 'sorption'";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial model is the base model of all adsorbate volumes for pure component
adsorption. It defines the volume geometry, heat ports in all spatial directions, 
as well as adsorbate and diffusive (i.e., adsorpt) ports in x-direction. The required 
heat ports can be adjusted via the volume setup. Furthermore, this model defines 
fundamental variables for the momentum, mass, and energy balance, as well as the 
initialization of these balances.
<br/><br/>
Models that inherit properties from this partial model must redeclare the fluid port
and may have to add further adsorbate and diffusive (i.e., adsorpt) ports in the 
required spatial direction (e.g., y- or z-direction). Furthermore, the working pair
model must be redeclared.
<br/><br/>
The following variables must be specified in the model that inherit properties:
</p>
<ul>
  <li>
  Sum of all adsorbent mass flow rates across adsorbate boundaries <i>masor_flow</i>.
  </li>
  <li>
  Sum of all adsorpt mass flow rates across adsorbate boundaries <i>mas_flow</i>.
  </li>
  <li>
  Sum of all sorption mass flow rates across sorption boundaries <i>ms_flow</i>.
  </li>
  <br/>
  <li>
  Sum of all enthalpy flow rates across boundaries <i>Hb_flow</i>.
  </li>
  <br/>
  <li>
  Variables of the summary record: Adsorbate mass flow rates in y-direction 
  <i>ma_flow_yMinus</i>/<i>ma_flow_yPlus</i> and in z-direction 
  <i>ma_flow_zMinus</i>/<i>ma_flow_zPlus</i>, as well as diffusive mass flow 
  rates in y-direction <i>md_flow_yMinus</i>/<i>md_flow_yPlus</i> and in
  z-direction <i>md_flow_zMinus</i>/<i>md_flow_zPlus</i>.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  December 15, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end PartialPureComponentAdsorbateVolume;
