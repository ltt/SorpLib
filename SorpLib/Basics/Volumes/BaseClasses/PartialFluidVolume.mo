within SorpLib.Basics.Volumes.BaseClasses;
partial model PartialFluidVolume
  "Base model for all fluid-based volumes"

  //
  // Definition of parameters describing media
  //
  parameter Integer no_components = 1
    "Number of components in the medium"
    annotation (Dialog(tab="General", group="Media"),
                Evaluate=true,
                HideResult=true);

  //
  // Definition of parameters regarding classes
  //
  replaceable connector FluidPortsIn =
    SorpLib.Basics.Interfaces.FluidPorts.LiquidPort_in
    constrainedby SorpLib.Basics.Interfaces.BaseClasses.PartialFluidPort
    "Fluid ports for inlet design direction"
    annotation (Dialog(tab="Volume Setup", group="Classes"),
                Evaluate=true,
                HideResult=true);
  replaceable connector FluidPortsOut =
    SorpLib.Basics.Interfaces.FluidPorts.LiquidPort_out
    constrainedby SorpLib.Basics.Interfaces.BaseClasses.PartialFluidPort
    "Fluid ports for outlet design direction"
    annotation (Dialog(tab="Volume Setup", group="Classes"),
                Evaluate=true,
                HideResult=true);

  parameter Integer nPorts_cfp_xMinus=0
    "Number of ports at cfp_xMinus"
    annotation (Dialog(connectorSizing=true),
                HideResult=true);
  parameter Integer nPorts_cfp_xPlus=0
    "Number of ports at cfp_xPlus"
    annotation (Dialog(connectorSizing=true),
                HideResult=true);

  //
  // Definition of parameters regarding start values
  //
  parameter Modelica.Units.SI.Pressure p_initial = 1e5
    "Initial value of pressure"
    annotation (Dialog(tab="Initialisation", group="Initial Values"));
  parameter Modelica.Units.SI.Temperature T_initial = 298.15
    "Initial value of temperature"
    annotation (Dialog(tab="Initialisation", group="Initial Values",
                enable=(independentStateVariables==
                  SorpLib.Choices.IndependentVariablesVolume.pTX)));
  parameter Modelica.Units.SI.SpecificEnthalpy h_initial = 0
    "Initial value of specific enthalpy"
    annotation (Dialog(tab="Initialisation", group="Initial Values",
                enable=(independentStateVariables==
                  SorpLib.Choices.IndependentVariablesVolume.phX)));

  parameter Modelica.Units.SI.MassFlowRate mc_flow_initialX = 1e-4
    "Start value for convective mass flow rate in x direction"
    annotation (Dialog(tab="Initialisation", group="Start Values"));

  extends SorpLib.Basics.Volumes.BaseClasses.PartialVolume;

  //
  // Definition of parameters regarding calculation setup
  //
  parameter SorpLib.Choices.IndependentVariablesVolume independentStateVariables=
    SorpLib.Choices.IndependentVariablesVolume.phX
    "Independent state variables"
    annotation (Dialog(tab="General", group="Calculation Setup"),
                Evaluate=true,
                HideResult = true);
  parameter Boolean pressureNoStateVariable = true
    " = true, if pressure is not a state variable and is not needed to calculate
    fluid properties like density (e.g., incompressible fluid)"
    annotation (Dialog(tab="General", group="Calculation Setup"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult = true);
  parameter Boolean neglectTermVp = false
    " = true, if term p * v is neglected for the specific internal energy (i.e., 
    u = h)"
    annotation (Dialog(tab="General", group="Calculation Setup"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult = true);
  parameter Boolean calculateAdditionalProperties = false
    "= true, if additional properties required for tranport models are calculated"
    annotation (Dialog(tab="General", group="Calculation Setup"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult = true);

  //
  // Definition of advanced parameters
  //
  parameter SorpLib.Choices.BalanceEquations type_overallMassBalance=
    SorpLib.Choices.BalanceEquations.TransientFixedInitial
    "Handling of the overall mass balance and corresponding initialisation"
    annotation (Dialog(tab = "Advanced", group = "Conservation Equations"),
                Evaluate=true,
                HideResult=true);

  //
  // Definition of ports
  //
  FluidPortsIn[nPorts_cfp_xMinus] cfp_xMinus(
    each final no_components=no_components,
    m_flow(each start=mc_flow_initialX))
    "Convective fluid port at position '-dx/2'"
    annotation (Placement(transformation(extent={{-46,14},{-38,22}}),
                iconTransformation(extent={{-46,14},{-38,22}})));
  FluidPortsOut[nPorts_cfp_xPlus] cfp_xPlus(
    each final no_components=no_components,
    m_flow(each start=-mc_flow_initialX))
    "Convective fluid port at position '+dx/2'"
    annotation (Placement(transformation(extent={{74,14},{82,22}}),
                iconTransformation(extent={{74,14},{82,22}})));

  //
  // Definition of variables
  //
  Modelica.Units.SI.Pressure p(
    start=p_initial,
    stateSelect= if not pressureNoStateVariable then StateSelect.prefer
      else StateSelect.avoid)
    "Pressure";
  Modelica.Units.SI.Temperature T(
    start=T_initial,
    stateSelect= if independentStateVariables==
      SorpLib.Choices.IndependentVariablesVolume.pTX then StateSelect.prefer
      else StateSelect.avoid)
    "Temperature";
  Modelica.Units.SI.SpecificVolume v
    "Specific volume";
  Modelica.Units.SI.Density rho
    "Density";

  Modelica.Units.SI.SpecificEnthalpy h(
    start=h_initial,
    stateSelect= if independentStateVariables==
      SorpLib.Choices.IndependentVariablesVolume.phX then StateSelect.prefer
      else StateSelect.avoid)
    "Specific enthalpy";
  Modelica.Units.SI.SpecificInternalEnergy u
    "Specific internal energy";

  Modelica.Units.SI.Mass m
    "Mass";
  Modelica.Units.SI.MassFlowRate dm_dtau
    "Derivative of mass w.r.t. time";
  Modelica.Units.SI.MassFlowRate mc_flow
    "Sum of all convective mass flow rates across boundaries";

  Modelica.Units.SI.InternalEnergy U
    "Internal energy";
  Real dU_dtau(final unit="W")
    "Derivative of internal energy w.r.t. time";
  Modelica.Units.SI.EnthalpyFlowRate Hb_flow
    "Sum of all enthalpy flow rates across boundaries";
  Modelica.Units.SI.HeatFlowRate Qb_flow
    "Sum of all heat flow rates across boundaries";

  SorpLib.Basics.Volumes.Records.FluidVolumeProperties fluidProperties(
    final no_components=no_components)
    "Properties of fluid volume often required for calculation of heat transfer";

initial equation
  if not pressureNoStateVariable and type_overallMassBalance ==
    SorpLib.Choices.BalanceEquations.TransientFixedInitial then
    p = p_initial
      "Fixed initial value";

  elseif not pressureNoStateVariable and type_overallMassBalance ==
    SorpLib.Choices.BalanceEquations.TransientSteadyStateInitial then
    der(p) = 0
      "Steady-state initialisation of overall mass balance";

  end if;

  if type_energyBalance ==
    SorpLib.Choices.BalanceEquations.TransientFixedInitial then
    if independentStateVariables==
      SorpLib.Choices.IndependentVariablesVolume.pTX then
      T = T_initial
        "Fixed initial value";

    else
      h = h_initial
        "Fixed initial value";

    end if;

  elseif type_energyBalance ==
    SorpLib.Choices.BalanceEquations.TransientSteadyStateInitial then
    if independentStateVariables==
      SorpLib.Choices.IndependentVariablesVolume.pTX then
      der(T) = 0
        "Steady-state initial value";

    else
      der(h) = 0
        "Steady-state initial value";

    end if;
  end if;

equation
  //
  // Property calculation
  //
  u = if neglectTermVp then h else h - p * v
    "Specific internal energy";

  //
  // Momentum balance
  //
  cfp_xMinus.p = fill(p, nPorts_cfp_xMinus)
    "Total pressure at the port (i.e., homogenous volume)";
  cfp_xPlus.p = fill(p, nPorts_cfp_xPlus)
    "Total pressure at the port (i.e., homogenous volume)";

  //
  // Mass balance
  //
  m = geometry.V / v
    "Mass";

  //
  // Energy balance
  //
  U = m * u
    "Internal energy";

  cfp_xMinus.h_outflow = fill(h, nPorts_cfp_xMinus)
    "Specific enthalpy leaving the port (i.e., homogenous volume)";
  cfp_xPlus.h_outflow = fill(h, nPorts_cfp_xPlus)
    "Specific enthalpy leaving the port (i.e., homogenous volume)";

  T_heatPorts = T
    "Required for conditional component";

  //
  // Summary record
  //
  fluidProperties.p = p
    "Pressure";
  fluidProperties.T = T
    "Temperature";
  fluidProperties.v = v
    "Specific volume";

  fluidProperties.mc_flow_xMinus = sum(cfp_xMinus.m_flow)
    "Total convective mass flow rate at port '-dx/2'";
  fluidProperties.mc_flow_xPlus = sum(cfp_xPlus.m_flow)
    "Total convective mass flow rate at port '+dx/2'";

  //
  // Annotations
  //
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Rectangle(extent={{-90,30},{30,-90}}, lineColor={0,0,0}),
        Line(points={{-90,30},{-30,90}}, color={0,0,0}),
        Line(
          points={{-30,-30},{-90,-90}},
          color={0,0,0},
          pattern=LinePattern.Dash),
        Line(points={{90,-30},{30,-90}}, color={0,0,0}),
        Line(points={{90,90},{30,30}}, color={0,0,0}),
        Line(points={{-30,90},{90,90},{90,-30}}, color={0,0,0}),
        Line(
          points={{90,-30},{-30,-30}},
          color={0,0,0},
          pattern=LinePattern.Dash),
        Line(
          points={{-30,90},{-30,-30}},
          color={0,0,0},
          pattern=LinePattern.Dash)}),                           Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    Documentation(info="<html>
<p>
This partial model is the base model of all fluid-based volumes. It defines the 
volume geometry, heat ports in all spatial directions, and convective fluid ports 
in x-direction. The required heat ports can be adjusted via the volume setup. The
implementation of the fluid ports follows the 'connectorSizing' principle. Furthermore, 
this model defines fundamental variables for the momentum, mass, and energy balance, 
as well as the initialization of these balances.
<br/><br/>
Models that inherit properties from this partial model must redeclare the fluid port
and may have to add further convective fluid ports (e.g., y- or z-direction) and 
diffusive fluid ports in the required spatial direction. Furthermore, conservation 
equations must be added. In this context, appropriate fluid property models are 
required.
<br/><br/>
The following variables must be specified in the model that inherit properties:
</p>
<ul>
  <li>
  Temperature <i>T</i>.
  </li>
  <li>
  Specific volume <i>v</i> and density <i>&rho;</i>.
  </li>
  <li>
  Specific enthalpy <i>h</i>.
  </li>
  <br/>
  <li>
  Mass balance using at least the variables <i>dm_dtau</i> and <i>mc_flow</i>.
  </li>
  <li>
  Partial derivative of the overall mass w.r.t. time <i>dm_dtau</i>.
  </li>
  <li>
  Sum of all convective mass flow rates <i>mc_flow</i>.
  </li>
  <br/>
  <li>
  Energy balance using at least the variables <i>dU_dtau</i>, <i>Hb_flow</i>,
  and <i>Qb_flow</i>.
  </li>
  <li>
  Partial derivative of the internal energy w.r.t. time <i>dU_dtau</i>.
  </li>
  <li>
  Sum of all enthalpy flow rates across boundaries <i>Hb_flow</i>.
  </li>
  <li>
  Sum of all heat flow rates across boundaries <i>Qb_flow</i>.
  </li>
  <br/>
  <li>
  Variables of the summary record except for pressure <i>p</i>, temperature <i>T</i>, 
  specific volume <i>v</i>, and mass flow rates in x-direction 
  <i>mc_flow_xMinus</i>/<i>mc_flow_xPlus</i>.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  December 7, 2023, by Mirko Engelpracht:<br/>
  Major revisions due to restructering finite volumes.
  </li>
  <li>
  January 12, 2021, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end PartialFluidVolume;
