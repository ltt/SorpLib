within SorpLib.Basics.Volumes.AdsorbateVolumes.Tester;
model Test_AdsorbatePureVLEVolume
  "Tester for pure-component adsorbate volume with a real fluid"
  extends Modelica.Icons.Example;

  //
  // Definition of adsorbate models
  //
  SorpLib.Basics.Volumes.AdsorbateVolumes.AdsorbatePureVLEVolume
    adsorbate_cooling_heating(
    m_sor_initial=adsorbate_cooling_heating.geometry.V/0.0013793104,
    T_initial=373.15,
    geometry(
      dx=0.2,
      dy=0.1,
      dz=0.6),
    redeclare model PureWorkingPairModel =
        Media.WorkingPairs.PureComponents.H2O.Silicagel123_DubininLorentzianCumulative_Schawe2000_VLE
        (
        approachSorptionEnthalpy=SorpLib.Choices.SorptionEnthalpy.ClausiusClapeyron,
        approachSpecificHeatCapacity=SorpLib.Choices.SpecificHeatCapacityAdsorpt.ChakrabortyElAl,
        limitLowerPressure=true,
        limitLowerPressureAdsorptive=true),
    x_initial=0.05) "Adsorbate volume that is cooled and heated"
    annotation (Placement(transformation(extent={{-10,50},{10,70}})));

  SorpLib.Basics.Volumes.AdsorbateVolumes.AdsorbatePureVLEVolume
    adsorbate_adsorption_desorption(
    m_sor_initial=adsorbate_cooling_heating.geometry.V/0.0013793104,
    T_initial=313.15,
    geometry(
      dx=0.2,
      dy=0.1,
      dz=0.6),
    redeclare model PureWorkingPairModel =
        Media.WorkingPairs.PureComponents.H2O.Silicagel123_DubininLorentzianCumulative_Schawe2000_VLE
        (
        approachSorptionEnthalpy=SorpLib.Choices.SorptionEnthalpy.ClausiusClapeyron,
        approachSpecificHeatCapacity=SorpLib.Choices.SpecificHeatCapacityAdsorpt.ChakrabortyElAl,
        limitLowerPressure=true,
        limitLowerPressureAdsorptive=true),
    x_initial=0.1,
    nSorptionPorts=1)
                   "Adsorbate volume with adsorption and desorption"
    annotation (Placement(transformation(extent={{-10,-10},{10,10}})));

  SorpLib.Basics.Volumes.AdsorbateVolumes.AdsorbatePureVLEVolume
    adsorbate_adsorbatePorts(
    m_sor_initial=adsorbate_cooling_heating.geometry.V/0.0013793104,
    T_initial=313.15,
    geometry(
      dx=0.2,
      dy=0.1,
      dz=0.6),
    calcUptakeAveragedProperties=true,
    useAdsorbatePorts=true,
    redeclare model PureWorkingPairModel =
        Media.WorkingPairs.PureComponents.H2O.Silicagel123_DubininLorentzianCumulative_Schawe2000_VLE
        (
        approachSorptionEnthalpy=SorpLib.Choices.SorptionEnthalpy.ClausiusClapeyron,
        approachSpecificHeatCapacity=SorpLib.Choices.SpecificHeatCapacityAdsorpt.ChakrabortyElAl,
        limitLowerPressure=true,
        limitLowerPressureAdsorptive=true,
        x_adsorpt_lb=1e-2),
    x_initial=0.1,
    nSorptionPorts=1)
                   "Adsorbate volume with adsorbate ports"
    annotation (Placement(transformation(extent={{-10,-70},{10,-50}})));

  //
  // Definition of fluid boundaries
  //
protected
  SorpLib.Basics.Sources.Fluids.VLESource fs_adsorbate_adsorption_desorption(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    use_mFlowInput=true,
    use_TInput=true)
    "Fluid source for adsorbate volume with adsorption and desorption"
    annotation (Placement(transformation(extent={{-30,20},{-10,40}})));

  SorpLib.Basics.Sources.Fluids.VLESource fs_adsorbate_adsorbatePorts(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    use_mFlowInput=true,
    use_TInput=true)
    "Fluid source for adsorbate volume with adsorption and desorption"
    annotation (Placement(transformation(extent={{-30,-40},{-10,-20}})));
  SorpLib.Basics.Sources.Sorption.AdsorbateSource as_adsorbate_adsorbatePorts_out(
    boundaryTypePressure=false,
    use_mFlowInput=true,
    h_fixed=74547.97,
    use_xInput=false,
    x_fixed={0.3}) "Adsorbate source for adsorbate volume with adsorbate ports"
    annotation (Placement(transformation(extent={{30,-70},{10,-50}})));
  SorpLib.Basics.Sources.Sorption.AdsorbateSource as_adsorbate_adsorbatePorts_in(
    boundaryTypePressure=false,
    use_mFlowInput=true,
    h_fixed=134547.97,
    use_xInput=false,
    x_fixed={0.1}) "Adsorbate source for adsorbate volume with adsorbate ports"
    annotation (Placement(transformation(extent={{-30,-70},{-10,-50}})));

  //
  // Definition of thermal boundaries
  //
  SorpLib.Basics.Sources.Thermal.HeatSource ts_adsorbate_cooling_heating(
     boundaryType=SorpLib.Choices.BoundaryThermal.HeatFlowRate,
    use_QFlowInput=true)
    "Thermal for adsorbate volume that is cooled and heated" annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={0,80})));

  SorpLib.Basics.Sources.Thermal.HeatSource ts_adsorbate_adsorption_desorption(boundaryType=
        SorpLib.Choices.BoundaryThermal.HeatFlowRate, use_QFlowInput=true)
    "Thermal for adsorbate volume with adsorption and desorption" annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={20,30})));

  SorpLib.Basics.Sources.Thermal.HeatSource ts_adsorbate_adsorbate_adsorbatePorts(boundaryType=
        SorpLib.Choices.BoundaryThermal.HeatFlowRate, use_QFlowInput=true)
    "Thermal for adsorbate volume with adsorption and desorption" annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={20,-30})));

  //
  // Definition of input signals
  //
  Modelica.Blocks.Sources.Ramp input_QFlow_adsorbate_cooling_heating(
    height=-2000,
    duration=2500,
    offset=1000) "Input signal for heat flow rate"
    annotation (Placement(transformation(extent={{-20,86},{-10,96}})));

  Modelica.Blocks.Sources.RealExpression
    input_mFlow_adsorbate_adsorption_desorption(y=if time < 1250 then min(-1e-3*
        (1.7e3 - adsorbate_adsorption_desorption.p), 0) else max(1e-3*(
        adsorbate_adsorption_desorption.p - 3.1e3), 0))
    "Input signal for mass flow rate"
    annotation (Placement(transformation(extent={{-52,28},{-40,40}})));
  Modelica.Blocks.Sources.RealExpression
    input_T_adsorbate_adsorption_desorption(y=if time < 1250 then 273.15 + 15
         else 273.15 + 25) "Input signal for temperature"
    annotation (Placement(transformation(extent={{-52,34},{-40,22}})));
  Modelica.Blocks.Sources.RealExpression
    input_QFlow_adsorbate_adsorption_desorption(y=if time < 1250 then 100*(
        adsorbate_adsorption_desorption.T - (273.15 + 25)) else -100*((273.15 +
        85) - adsorbate_adsorption_desorption.T))
    "Input signal for heat flow rate"
    annotation (Placement(transformation(extent={{52,28},{40,40}})));

  Modelica.Blocks.Sources.RealExpression input_mFlow_adsorbatePorts(y=if time <
        1250 then min(-1e-3*(5e3 - adsorbate_adsorbatePorts.p), 0) else max(1e-3
        *(adsorbate_adsorbatePorts.p - 1.7e3), 0))
    "Input signal for mass flow rate"
    annotation (Placement(transformation(extent={{-52,-32},{-40,-20}})));
  Modelica.Blocks.Sources.RealExpression input_T_adsorbatePorts(y=if time < 1250
         then 273.15 + 25 else 273.15 + 160) "Input signal for temperature"
    annotation (Placement(transformation(extent={{-52,-26},{-40,-38}})));
  Modelica.Blocks.Sources.RealExpression input_QFlow_adsorbatePorts(y=if time <
        1250 then 1000*(adsorbate_adsorbatePorts.T - (273.15 + 25)) else -1000*(
        (273.15 + 160) - adsorbate_adsorbatePorts.T))
    "Input signal for heat flow rate"
    annotation (Placement(transformation(extent={{52,-32},{40,-20}})));

  Modelica.Blocks.Sources.Step input_mFlow_adsorbate_adsorbatePorts_in(
    height=2*8.7,
    offset=-8.7,
    startTime=1250) "Input signal for adsorbate flow rate"
    annotation (Placement(transformation(extent={{-52,-56},{-40,-44}})));
  Modelica.Blocks.Sources.Step input_mFlow_adsorbate_adsorbatePorts_out(
    height=-2*8.7,
    offset=8.7,
    startTime=1250) "Input signal for adsorbate flow rate"
    annotation (Placement(transformation(extent={{52,-56},{40,-44}})));

equation
  //
  // Connections
  //
  connect(fs_adsorbate_adsorption_desorption.port,
    adsorbate_adsorption_desorption.fp_sorption[1]) annotation (Line(
      points={{-20,30},{-1.6,30},{-1.6,4.4}},
      color={0,140,72},
      thickness=1));
  connect(fs_adsorbate_adsorbatePorts.port, adsorbate_adsorbatePorts.fp_sorption[
    1]) annotation (Line(
      points={{-20,-30},{-1.6,-30},{-1.6,-55.6}},
      color={0,140,72},
      thickness=1));
  connect(ts_adsorbate_cooling_heating.port, adsorbate_cooling_heating.hp_sorption)
    annotation (Line(
      points={{0,80},{0,74},{1.6,74},{1.6,67.6}},
      color={238,46,47},
      thickness=1));
  connect(ts_adsorbate_adsorption_desorption.port,
    adsorbate_adsorption_desorption.hp_sorption) annotation (Line(
      points={{20,30},{1.6,30},{1.6,7.6}},
      color={238,46,47},
      thickness=1));
  connect(as_adsorbate_adsorbatePorts_in.port, adsorbate_adsorbatePorts.ap_xMinus)
    annotation (Line(
      points={{-20,-60},{-14,-60},{-14,-58.2},{-4.2,-58.2}},
      color={0,0,0},
      thickness=1));
  connect(as_adsorbate_adsorbatePorts_out.port, adsorbate_adsorbatePorts.ap_xPlus)
    annotation (Line(
      points={{20,-60},{14,-60},{14,-58.2},{7.8,-58.2}},
      color={0,0,0},
      thickness=1));

  connect(input_QFlow_adsorbate_cooling_heating.y, ts_adsorbate_cooling_heating.Q_flow_input)
    annotation (Line(points={{-9.5,91},{-5,91},{-5,81}}, color={0,0,127}));
  connect(input_mFlow_adsorbate_adsorption_desorption.y,
    fs_adsorbate_adsorption_desorption.m_flow_input) annotation (Line(points={{-39.4,
          34},{-30,34},{-30,32},{-21.2,32}}, color={0,0,127}));
  connect(input_T_adsorbate_adsorption_desorption.y,
    fs_adsorbate_adsorption_desorption.T_input)
    annotation (Line(points={{-39.4,28},{-21.2,28}}, color={0,0,127}));
  connect(input_QFlow_adsorbate_adsorption_desorption.y,
    ts_adsorbate_adsorption_desorption.Q_flow_input) annotation (Line(points={{39.4,
          34},{30,34},{30,35},{21,35}}, color={0,0,127}));
  connect(input_mFlow_adsorbate_adsorbatePorts_in.y,
    as_adsorbate_adsorbatePorts_in.m_flow_input) annotation (Line(points={{-39.4,
          -50},{-32,-50},{-32,-58},{-21.2,-58}}, color={0,0,127}));
  connect(input_mFlow_adsorbate_adsorbatePorts_out.y,
    as_adsorbate_adsorbatePorts_out.m_flow_input) annotation (Line(points={{39.4,
          -50},{30,-50},{30,-58},{21.2,-58}}, color={0,0,127}));
  connect(ts_adsorbate_adsorbate_adsorbatePorts.port, adsorbate_adsorbatePorts.hp_sorption)
    annotation (Line(
      points={{20,-30},{1.6,-30},{1.6,-52.4}},
      color={238,46,47},
      thickness=1));
  connect(input_mFlow_adsorbatePorts.y, fs_adsorbate_adsorbatePorts.m_flow_input)
    annotation (Line(points={{-39.4,-26},{-30,-26},{-30,-28},{-21.2,-28}},
        color={0,0,127}));
  connect(input_T_adsorbatePorts.y, fs_adsorbate_adsorbatePorts.T_input)
    annotation (Line(points={{-39.4,-32},{-21.2,-32}}, color={0,0,127}));
  connect(input_QFlow_adsorbatePorts.y, ts_adsorbate_adsorbate_adsorbatePorts.Q_flow_input)
    annotation (Line(points={{39.4,-26},{30,-26},{30,-25},{21,-25}}, color={0,0,
          127}));

  //
  // Annotations
  //
  annotation (experiment(StopTime=2500), Documentation(info="<html>
<p>
This model checks the model of an adsorbate volume for pure comonent adsorption
using a real fluid with a two-phase regime.
<br/><br/>
To see the model behavior, plot the variables of the models over the 
time. The simulation time is correctly preset (Start: 0 s, Stop = 2500 s). 
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 15, 2023, by Mirko Engelpracht:<br/>
  Extension and added documentation.
  </li>
  <li>
  January 12, 2021, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Test_AdsorbatePureVLEVolume;
