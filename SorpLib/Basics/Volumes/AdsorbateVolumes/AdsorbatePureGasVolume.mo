within SorpLib.Basics.Volumes.AdsorbateVolumes;
model AdsorbatePureGasVolume
  "Homogenous adsorbate volume of an ideal gas for pure component adsorption"
  extends
    SorpLib.Basics.Volumes.BaseClasses.PartialPureComponentAdsorbateVolume(
    redeclare final SorpLib.Basics.Interfaces.FluidPorts.GasPort_in fp_sorption,
    redeclare replaceable package Medium = SorpLib.Media.IdealGases.H2O
      constrainedby Modelica.Media.IdealGases.Common.SingleGasNasa,
    redeclare replaceable model PureWorkingPairModel =
      SorpLib.Media.WorkingPairs.PureComponents.H2O.SilicaGel_Toth_WangDouglasLeVan2009_Gas
    constrainedby SorpLib.Media.WorkingPairs.PureComponents.WorkingPairGas(
      redeclare package Medium = Medium,
      stateVariables=independentStateVariables,
      calcCaloricProperties=true,
      calcEntropicProperties=false,
      calcAdsorptAdsorbateState=useAdsorbatePorts,
      calcDerivativesIsotherm=true,
      calcDerivativesMassEnergyBalance=true,
      calcDerivativesEntropyBalance=false,
      p_adsorpt=p,
      x_adsorpt=x,
      T_adsorpt=T,
      avoidEvents=avoid_events));

equation
  //
  // Mass balance
  //
  masor_flow = if useAdsorbatePorts then
    ap_xMinus.m_flow + ap_xPlus.m_flow else 0
    "Sum of all adsorbent mass flow rates across adsorbate boundaries";
  mas_flow = if useAdsorbatePorts then (if avoid_events then
    ap_xMinus.m_flow * sum(noEvent(actualStream(ap_xMinus.x_outflow))) +
    ap_xPlus.m_flow * sum(noEvent(actualStream(ap_xPlus.x_outflow))) else
    ap_xMinus.m_flow * sum(actualStream(ap_xMinus.x_outflow)) +
    ap_xPlus.m_flow * sum(actualStream(ap_xPlus.x_outflow))) else 0
    "Sum of all sorption mass flow rates across adsorbate boundaries";
  md_flow = sum(dp_xMinus.m_flow) + sum(dp_xPlus.m_flow)
    "Sum of all diffusive mass flow rates across boundaries";
  ms_flow = sum(fp_sorption.m_flow)
    "Sum of all sorption mass flow rates across boundaries";

  //
  // Energy balance
  //
  if avoid_events then
    if useAdsorbatePorts then
      Hb_flow =
        ap_xMinus.m_flow * noEvent(actualStream(ap_xMinus.h_outflow)) +
        ap_xPlus.m_flow * noEvent(actualStream(ap_xPlus.h_outflow)) +
        sum(dp_xMinus.m_flow .* noEvent(actualStream(dp_xMinus.h_outflow))) +
        sum(dp_xPlus.m_flow .* noEvent(actualStream(dp_xPlus.h_outflow))) +
        sum(fp_sorption.m_flow .* noEvent(actualStream(fp_sorption.h_outflow)))
        "Sum of all enthalpy flow rates across boundaries";

    else
      Hb_flow =
        sum(dp_xMinus.m_flow .* noEvent(actualStream(dp_xMinus.h_outflow))) +
        sum(dp_xPlus.m_flow .* noEvent(actualStream(dp_xPlus.h_outflow))) +
        sum(fp_sorption.m_flow .* noEvent(actualStream(fp_sorption.h_outflow)))
        "Sum of all enthalpy flow rates across boundaries";

    end if;

  else
    if useAdsorbatePorts then
      Hb_flow =
        ap_xMinus.m_flow * actualStream(ap_xMinus.h_outflow) +
        ap_xPlus.m_flow * actualStream(ap_xPlus.h_outflow) +
        sum(dp_xMinus.m_flow .* actualStream(dp_xMinus.h_outflow)) +
        sum(dp_xPlus.m_flow .* actualStream(dp_xPlus.h_outflow)) +
        sum(fp_sorption.m_flow .* actualStream(fp_sorption.h_outflow))
        "Sum of all enthalpy flow rates across boundaries";

    else
      Hb_flow =
        sum(dp_xMinus.m_flow .* actualStream(dp_xMinus.h_outflow)) +
        sum(dp_xPlus.m_flow .* actualStream(dp_xPlus.h_outflow)) +
        sum(fp_sorption.m_flow .* actualStream(fp_sorption.h_outflow))
        "Sum of all enthalpy flow rates across boundaries";

    end if;
  end if;

  //
  // Summary record
  //
  adsorbateProperties.ma_flow_yMinus = 0
    "Adsorbate mass flow rate at port '-dy/2'";
  adsorbateProperties.ma_flow_yPlus = 0
    "Adsorbate mass flow rate at port '+dy/2'";
  adsorbateProperties.ma_flow_zMinus = 0
    "Adsorbate mass flow rate at port '-dz/2'";
  adsorbateProperties.ma_flow_zPlus = 0
    "Adsorbate mass flow rate at port '+dz/2'";

  adsorbateProperties.md_flow_yMinus = zeros(no_components)
    "Diffusive mass flow rate at port '-dy/2'";
  adsorbateProperties.md_flow_yPlus = zeros(no_components)
    "Diffusive mass flow rate at port '+dy/2'";
  adsorbateProperties.md_flow_zMinus = zeros(no_components)
    "Diffusive mass flow rate at port '-dz/2'";
  adsorbateProperties.md_flow_zPlus = zeros(no_components)
    "Diffusive mass flow rate at port '+dz/2'";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This model represents an adsorbate volume, applying a lumped modeling approach, for
pure component adsorption of an adsorptive described by an ideal gas. Depending 
on the volume setup, this model may have up to seven heat ports (i.e., two for 
each spatial direction of a cartesian coordinate system and one for sorption). 
Furthermore, this model has adsorbate and diffusive (i.e., adsorpt) ports in 
y-direction and a sorption fluid port. These ports allow for the combination of 
several adsorbate volumes to create a spatially distributed model.
</p>

<h4>Main equations</h4>
<p>
The most important equations are the momentum, mass, and energy balance. According to
the staggered grid approach, the momentum balance is not solved within the volume but 
at the volume's boundaries via so-called 
<a href=\"Modelica://SorpLib.Components.Fittings\">flow models</a> or
<a href=\"Modelica://SorpLib.Components.MassTransfer\">mass transfer models</a>. Hennce, 
no pressure losses occur within the volume:
</p>
<pre>
    p = ap_xMinus.p;
</pre>
<pre>
    p = ap_xPlus.p;
</pre>
<pre>
    p = fp_sorption.p;
</pre>
<p>
For the diffusive (i.e., adsorpt) ports, the potential variables are the loading:
</p>
<pre>
    x = dp_xMinus.x;
</pre>
<pre>
    x = dp_xPlus.x;
</pre>

<p>
Regarding the mass and energy balances, either steady-state or tansient balnaces
can be selected. When using the pressure <i>p</i>, temperature <i>T</i>, and sorbent
mass <i>m<sub>sor</sub></i> as independent states, the adsorbent mass balance is 
defined as
</p>
<pre>
    (dm<sub>sor</sub>/d&tau;) = &sum; m<sub>a,sor,flow</sub>;
</pre>
<p>
the adsorpt mass balance is defined as
</p>
<pre>
    (dm<sub>adsorpt</sub>/d&tau;) = (dm<sub>sor</sub>/d&tau;) * x + m<sub>sor</sub> * (dx/d&tau;) = (dm<sub>sor</sub>/d&tau;) * x + m<sub>sor</sub> * [(&part;x/&part;p)<sub>T</sub> * (dp/d&tau;) + (&part;x/&part;T)<sub>p</sub> * (dT/d&tau;)] = &sum; m<sub>s,flow</sub> + &sum; m<sub>a,s,flow</sub> + &sum; m<sub>d,flow</sub>;
</pre> 
<p>
and the energy balance is defined as
</p>
<pre>
    (dU<sub>adsorbate</sub>/d&tau;) = (dm<sub>sor</sub>/d&tau;) * [u<sub>sor</sub> + x<sub>adsorpt</sub> * <SPAN STYLE=\"text-decoration:overline\">u</SPAN><sub>adsorpt</sub>] + m<sub>sor</sub> * [(du<sub>sor</sub>/d&tau;) + (d(x<sub>adsorpt</sub> * <SPAN STYLE=\"text-decoration:overline\">u</SPAN><sub>adsorpt</sub>)/d&tau;] = (dm<sub>sor</sub>/d&tau;) * [u<sub>sor</sub> + x<sub>adsorpt</sub> * <SPAN STYLE=\"text-decoration:overline\">u</SPAN><sub>adsorpt</sub>] + m<sub>sor</sub> * [(v<sub>sor</sub>) + (h<sub>adsorpt</sub> - p * v<sub>adsorpt</sub>) * (&part;x/&part;p)<sub>T</sub> - (v<sub>sor</sub> + x<sub>adsorpt</sub> * v<sub>adsorpt</sub>)] * (dp/d&tau;) + m<sub>sor</sub> * [(c<sub>sor</sub>) + (c<sub>adsorpt</sub> * x<sub>adsorpt</sub> + h<sub>adsorpt</sub> * (&part;x/&part;T)<sub>p</sub> - x<sub>adsorpt</sub> * (&part;v<sub>adsorpt</sub>/&part;T)<sub>p</sub> - v<sub>adsorpt</sub> * (&part;x/&part;T)<sub>p</sub>)] * (dT/d&tau;) = &sum; H<sub>b,flow</sub> + &sum; Q<sub>b,flow</sub>;
</pre>

<p>
When using the loading <i>x</i>, temperature <i>T</i>, and sorbent mass <i>m<sub>sor</sub></i> 
as independent states, the adsorbent mass balance is defined as
</p>
</p>
<pre>
    (dm<sub>sor</sub>/d&tau;) = &sum; m<sub>a,sor,flow</sub>;
</pre>
<p>
the adsorpt mass balance is defined as
</p>
<pre>
    (dm<sub>adsorpt</sub>/d&tau;) = (dm<sub>sor</sub>/d&tau;) * x + m<sub>sor</sub> * (dx/d&tau;) = &sum; m<sub>s,flow</sub> + &sum; m<sub>a,s,flow</sub> + &sum; m<sub>d,flow</sub>;
</pre> 
<p>
and the energy balance is defined as
</p>
<pre>
    (dU<sub>adsorbate</sub>/d&tau;) = (dm<sub>sor</sub>/d&tau;) * [u<sub>sor</sub> + x<sub>adsorpt</sub> * <SPAN STYLE=\"text-decoration:overline\">u</SPAN><sub>adsorpt</sub>] + m<sub>sor</sub> * [(du<sub>sor</sub>/d&tau;) + (d(x<sub>adsorpt</sub> * <SPAN STYLE=\"text-decoration:overline\">u</SPAN><sub>adsorpt</sub>)/d&tau;] = (dm<sub>sor</sub>/d&tau;) * [u<sub>sor</sub> + x<sub>adsorpt</sub> * <SPAN STYLE=\"text-decoration:overline\">u</SPAN><sub>adsorpt</sub>] + m<sub>sor</sub> * [(h<sub>adsorpt</sub> - p * v<sub>adsorpt</sub> - x<sub>adsorpt</sub> * v<sub>adsorpt</sub> / (&part;x/&part;p)<sub>T</sub>)] * (dx/d&tau;) + m<sub>sor</sub> * [(c<sub>sor</sub>) + (c<sub>adsorpt</sub> * x<sub>adsorpt</sub> - x<sub>adsorpt</sub> * (&part;v<sub>adsorpt</sub>/&part;T)<sub>p</sub>] * (dT/d&tau;) = &sum; H<sub>b,flow</sub> + &sum; Q<sub>b,flow</sub>;
</pre>

<p>
Herein, <i>dm<sub>sor</sub>/d&tau;</i> is the derivative of the adsorbent mass w.r.t. 
time, <i>dm<sub>adsorpt</sub>/d&tau;</i> is the derivative of the adsorpt mass w.r.t. 
time, <i>dU<sub>adsorbate</sub>/d&tau;</i> is the derivative of the uptake-averaged 
internal energy w.r.t. time, <i>dp/d&tau;</i> is the derivative of the pressure w.r.t. 
time, <i>dx/d&tau;</i> is the derivative of the loading w.r.t. time, and <i>dT/d&tau;</i> 
is the derivative of the temperature w.r.t. time,

The expression <i>(&part;x/&part;p)<sub>T</sub></i> describes the partial derivative of 
the loading w.r.t. pressure at constant temperature, <i>(&part;x/&part;T)<sub>p</sub></i> 
is the partial derivative of the loading w.r.t. temperature at constant pressure, and 
<i>(&part;v<sub>adsorpt</sub>/&part;T)<sub>p</sub></i> is the partial derivative of the 
specific volume of the adsorpt w.r.t. temperature at constant pressure.

The expression <i>m<sub>a,sor,flow</sub></i> is the sum of the adsorbent mass flow rates 
across the adsorbate ports, <i>m<sub>s,flow</sub></i> is the sum of the sorption mass 
flow rates across the adsorbate ports, <i>m<sub>a,s,flow</sub></i> is the sum of the 
sorption mass flow rates, <i>m<sub>d,flow</sub></i> is the sum of the diffusive mass flow 
rates, <i>H<sub>b,flow</sub></i> is the sum of the enthalpy flow rates, and 
<i>Q<sub>b,flow</sub></i> is the sum of the heat flow rates.

The variable <i>v<sub>sor</sub></i> describes the specific volume of the adsorbent, 
<i>u<sub>sor</sub></i> is the specific internal energy of the adsorbent, <i>c<sub>sor</sub></i> 
is the specific heat capacity of the adsorbent, <i><SPAN STYLE=\"text-decoration:overline\">u</SPAN><sub>adsorpt</sub></i> 
is the uptake-averaged specific internal energy of the adsorpt, <i>v<sub>adsorpt</sub></i> 
is the specific volume of the last adsorbed molecule, <i>h<sub>adsorpt</sub></i> is the 
specific enthalpy of the last adsorbed molecule, and <i>c<sub>adsorpt</sub></i> is the 
specific heat capacity of the adsorpt.
</p>


<h4>Assumptions and limitations</h4>
<ul>
  <li>
  Constant volume <i>V</i>
  </li>
  <li>
  Homogenoues properties within the volume
  </li>
  <li>
  Adsorption equilibrium with all adsorptive beeing adsorbed and in adsorpt
  phase (i.e., chemical equilibrium).
  </li>
  <li>
  The sorbent and adsorpt have the same pressure (i.e., mechanical equilibrium).
  </li>
  <li>
  The sorbent and adsorpt have the same temperature (i.e., thermal equilibrium).
  </li>
  <li>
  Specific volume of the adsorpt may only depend on the temperature and is thus
  a macroscopic property without averaging.
  </li>
  <li>
  Specific heat capacity of the adsorpt may only depend on the temperature or is
  assumed to already be an uptake-averaged specific heat capacity.
  </li>
  <li>
  Ideal and inert sorbent with constant specific volume.
  </li>
</ul>

<h4>Typical use</h4>
<p>
This model is typically used to model the adsorbate within adsorber heat exchanger or other
adsorption modules.
</p>

<h4>Important parameters and options</h4>
<ul>
  <li>
  <i>useHeatPorts</i>:
  Defines if heat ports in the spatial direction <i>i</i> are required.
  </li>
  <li>
  <i>useAdsorbatePorts</i>:
  Defines if adsorbate ports are connected (requires uptake-averaged properties).
  </li>
  <li>
  <i>useDiffusivePorts</i>:
  Defines if diffusive ports are connected (requires uptake-averaged properties).
  </li>
  <li>
  <i>calcUptakeAveragedProperties</i>:
  Defines if duptake-averaged properties are calculated.
  </li>
  <li>
  <i>independentStateVariables</i>:
  Defines independent state variables.
  </li>
  <br/>
  <li>
  <i>type_adsorbentMassBalance</i>:
  Defines the type of the adsorbent mass balance.
  </li>
  <li>
  <i>type_adsorptMassBalance</i>:
  Defines the type of the adsorpt mass balance.
  </li>
  <li>
  <i>type_energyBalance</i>:
  Defines the type of the energy balance.
  </li>
  <br/>
  <li>
  <i>avoid_events</i>:
  Defines if events shall be avoided via the noEvent()-operator.
  </li>
</ul>
<p>
Note that not all combinations of govering equation types are reasonable. Typically,
transient mass balances are combined with a transient energy balance.
</p>

<h4>Dynamics</h4>
<p>
This model has two dynamic state that can be selected (see options):
</p>
<ul>
  <li>
  Pressure <i>p</i> and temperature <i>T</i>, or
  </li>
  <li>
  loading <i>x</i> and  temperature <i>T</i> (recommended).
  </li>
</ul>
<p>
Furthermore, this model has the adsorbent mass <i>m_sor</i> as dynamic state if the
adsorbate ports are connected to other models (see option 'useAdsorbatePorts').
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 15, 2023, by Mirko Engelpracht:<br/>
  Major revisions due to restructering finite volumes.
  </li>
  <li>
  January 12, 2021, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end AdsorbatePureGasVolume;
