within SorpLib.Basics.Volumes;
package AdsorbateVolumes "Package containing finte volume models of adsorbates"
  extends Modelica.Icons.VariantsPackage;

  annotation (Documentation(info="<html>
<p>
This package contains finite volume models of adsorbates. This package calculates 
fluid properties based on the open-source Modelica Standard Library (MSL).
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end AdsorbateVolumes;
