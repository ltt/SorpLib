within SorpLib.Basics.Sources.Thermal.Tester;
model Test_HeatSource "Tester for heat source"
  extends Modelica.Icons.Example;

  //
  // Definition of boundary models
  //
  HeatSource TFixed(
    boundaryType=SorpLib.Choices.BoundaryThermal.Temperature,
    use_TInput=false) "Heat source with fixed temperature"
    annotation (Placement(transformation(extent={{-50,20},{-30,40}})));
  HeatSource TVar(
    boundaryType=SorpLib.Choices.BoundaryThermal.Temperature,
    use_TInput=true) "Heat source with variable temperature"
    annotation (Placement(transformation(extent={{-50,-40},{-30,-20}})));

  HeatSource QFixed(
    boundaryType=SorpLib.Choices.BoundaryThermal.HeatFlowRate,
    use_QFlowInput=false) "Heat source with fixed heat flow rate"
    annotation (Placement(transformation(extent={{50,20},{30,40}})));
  HeatSource QVar(
    boundaryType=SorpLib.Choices.BoundaryThermal.HeatFlowRate,
    use_QFlowInput=true) "Heat source with variable heat flow"
    annotation (Placement(transformation(extent={{50,-40},{30,-20}})));

  Modelica.Blocks.Sources.Ramp input_TVar(
    height=80,
    duration=2500,
    offset=293.15,
    startTime=0)
    "Ramp to simulate input signal of temperrature"
    annotation (Placement(transformation(extent={{-80,-40},{-60,-20}})),
                HideResult=true);
  Modelica.Blocks.Sources.Ramp input_QVar(
    height=2000,
    duration=2500,
    offset=-1000,
    startTime=0)
    "Ramp to simulate input signal of heat flow"
    annotation (Placement(transformation(extent={{80,-40},{60,-20}})),
    HideResult=true);

equation
  //
  // Connections
  //
  connect(TFixed.port, QFixed.port) annotation (Line(
      points={{-40,30},{40,30}},
      color={238,46,47},
      thickness=1));
  connect(TVar.port, QVar.port) annotation (Line(
      points={{-40,-30},{40,-30}},
      color={238,46,47},
      thickness=1));

  connect(input_TVar.y, TVar.T_input) annotation (Line(points={{-59,-30},{-50,-30},
          {-50,-24.8},{-41,-24.8}}, color={0,0,127}));
  connect(input_QVar.y, QVar.Q_flow_input) annotation (Line(points={{59,-30},{50,
          -30},{50,-36},{41,-36},{41,-35}}, color={0,0,127}));

  //
  // Annotations
  //
  annotation (experiment(StopTime=2500), Documentation(info="<html>
<p>
This model checks the heat source model used to prescribe properties at
a heat port.
<br/><br/>
To see the model behavior, plot the variables of the models over the 
time. The simulation time is correctly preset (Start: 0 s, Stop = 2500 s). 
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 4, 2023, by Mirko Engelpracht:<br/>
  Added documentation.
  </li>
  <li>
  January 11, 2021, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Test_HeatSource;
