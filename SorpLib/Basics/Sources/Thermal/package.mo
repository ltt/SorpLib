within SorpLib.Basics.Sources;
package Thermal "Thermal sources used to prescribe heat flow rate or temperature"
  extends Modelica.Icons.VariantsPackage;

  annotation (Documentation(info="<html>
<p>
This package contains sources for thermal connectors to define fixed or prescribed 
conditions. 
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end Thermal;
