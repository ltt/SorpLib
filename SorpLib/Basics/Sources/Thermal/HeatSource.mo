within SorpLib.Basics.Sources.Thermal;
model HeatSource
  "Boundary model of a heat source"

  //
  // Definition of parameters regarding the boundary type
  //
  parameter SorpLib.Choices.BoundaryThermal boundaryType=
    SorpLib.Choices.BoundaryThermal.Temperature
    "Prescribed variable for potential or flow"
    annotation (Dialog(tab="General", group="Boundary Type"),
                Evaluate=true,
                HideResult=true);

  //
  // Definition of parameters regarding potential and flow variables
  //
  parameter Boolean use_TInput = false
    " = true, if T is defined by input; otherwise, fixed value is used"
    annotation (Dialog(tab="General",group="Potential and Flow Variables",
                enable=(boundaryType ==
                SorpLib.Choices.BoundaryThermal.Temperature)),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);
  parameter Modelica.Units.SI.Temperature T_fixed = 293.15
    "Fixed temperature at boundary"
    annotation (Dialog(tab="General",group="Potential and Flow Variables",
                enable=(boundaryType ==
                SorpLib.Choices.BoundaryThermal.Temperature)
                and not use_TInput));

  parameter Boolean use_QFlowInput = false
    "=true, if Q_flow is defined by input; otherwise, fixed value is used"
    annotation (Dialog(tab="General",group="Potential and Flow Variables",
                enable=(boundaryType ==
                SorpLib.Choices.BoundaryThermal.HeatFlowRate)),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);
  parameter Modelica.Units.SI.HeatFlowRate Q_flow_fixed = 25
    "Fixed heat flow at boundary"
    annotation (Dialog(tab="General",group="Potential and Flow Variables",
                enable=(boundaryType ==
                SorpLib.Choices.BoundaryThermal.HeatFlowRate)
                and not use_QFlowInput));

  //
  // Definition of connectors
  //
  Modelica.Blocks.Interfaces.RealInput T_input(final unit="K") if
    (boundaryType == SorpLib.Choices.BoundaryThermal.Temperature and
    use_TInput)
    "Input for temperature"
    annotation (Placement(transformation(extent={{-40,30},{0,70}}),
       iconTransformation(extent={{-20,42},{0,62}})));

  Modelica.Blocks.Interfaces.RealInput Q_flow_input(final unit="W") if
    (boundaryType == SorpLib.Choices.BoundaryThermal.HeatFlowRate and
    use_QFlowInput)
    "Input for heat flow rate"
    annotation (Placement(transformation(extent={{-40,-70},{0,-30}}),
      iconTransformation(extent={{-20,-60},{0,-40}})));

  //
  // Definition of protected connectors
  //
protected
  Modelica.Blocks.Interfaces.RealInput T_internal(final unit="K")
    "Needed for connecting to conditional connector";
  Modelica.Blocks.Interfaces.RealInput Q_flow_internal(final unit="W")
    "Needed for connecting to conditional connector";

  //
  // Definition of ports
  //
public
  SorpLib.Basics.Interfaces.HeatPorts.HeatPort_in port
    "Heat port"
    annotation (Placement(transformation(extent={{-10,-10},{10,10}})));

equation
  //
  // Connectors
  //
  connect(T_input,T_internal);
  connect(Q_flow_input,Q_flow_internal);

  if not use_TInput then
    T_internal = T_fixed
      "Needed for connecting to conditional connector";
  end if;
  if not use_QFlowInput then
    Q_flow_internal = Q_flow_fixed
      "Needed for connecting to conditional connector";
  end if;

  //
  // Properties at port
  //
  if boundaryType == SorpLib.Choices.BoundaryThermal.Temperature then
    port.T = T_internal
      "Temperature at port";

  else
    port.Q_flow = Q_flow_internal
      "Heat flow rate at port";

  end if;

  //
  // Annotations
  //
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
                             Rectangle(
          extent={{-2,80},{2,-80}},
          lineColor={238,46,47},
          lineThickness=0.5,
          fillColor={238,46,47},
          fillPattern=FillPattern.Solid)}),                      Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    Documentation(info="<html>
<p>
This model can be used to specify either the temperature or the heat flow rate 
at a heat port.
</p>

<h4>Options</h4>
<ul>
  <li>
  <i>boundaryType</i>: Defines if temperature or heat flow rate is prescribed.
  </li>
</ul>
  
</html>",revisions="<html>
<ul>
  <li>
  December 4, 2023, by Mirko Engelpracht:<br/>
  Added documentation.
  </li>
  <li>
  January 11, 2021, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end HeatSource;
