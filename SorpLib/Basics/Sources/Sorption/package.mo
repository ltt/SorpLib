within SorpLib.Basics.Sources;
package Sorption "Sorption sources used to prescribe pressure, mass flow rate, specific enthalpy, and loadings"
  extends Modelica.Icons.VariantsPackage;

  annotation (Documentation(info="<html>
<p>
This package contains sources for sorption connectors to define fixed or prescribed 
conditions. 
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 15, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Sorption;
