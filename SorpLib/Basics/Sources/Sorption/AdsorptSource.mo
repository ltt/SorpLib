within SorpLib.Basics.Sources.Sorption;
model AdsorptSource "Boundary model of an adsorpt source"

  //
  // Definition of parameters regarding the boundary type
  //
  parameter Boolean boundaryTypeLoading = true
    " = true, if loading is prescribed; otherwise, mass flow rate is prescribed"
    annotation (Dialog(tab="General", group="Boundary Type"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);

  //
  // Definition of parameters regarding potential and flow variables
  //
  parameter Boolean use_xInput = false
    " = true, if x is defined by input; otherwise, fixed value is used"
    annotation (Dialog(tab="General",group="Potential and Flow Variables",
                enable=boundaryTypeLoading),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);
  parameter SorpLib.Units.Uptake x_fixed = 0.05
    "Fixed loading at boundary"
    annotation (Dialog(tab="General",group="Potential and Flow Variables",
                enable=boundaryTypeLoading and not use_xInput));

  parameter Boolean use_mFlowInput = false
    "=true, if m_flow is defined by input; otherwise, fixed value is used"
    annotation (Dialog(tab="General",group="Potential and Flow Variables",
                enable=not boundaryTypeLoading),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);
  parameter Modelica.Units.SI.MassFlowRate m_flow_fixed = -0.001
    "Fixed adsorpt mass flow rate at boundary"
    annotation (Dialog(tab="General",group="Potential and Flow Variables",
                enable=not boundaryTypeLoading and not use_mFlowInput));

  //
  // Definition of parameters regarding stream variables of specific enthalpy
  //
  parameter Boolean use_hInput = false
    "=true, if h is defined by input; otherwise, fixed value is used"
    annotation (Dialog(tab="General",group="Stream Variables - Specific Enthalpy"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);
  parameter Modelica.Units.SI.SpecificEnthalpy h_fixed = 350e3
    "Fixed specific enthalpy at boundary"
    annotation (Dialog(tab="General",group="Stream Variables - Specific Enthalpy",
                enable = not use_hInput));

  //
  // Definition of connectors
  //
  Modelica.Blocks.Interfaces.RealInput x_input(final unit="kg/kg") if
    (boundaryTypeLoading and use_xInput)
    "Input for loading"
     annotation (Placement(transformation(extent={{-120,30},{-80,70}}),
       iconTransformation(extent={{-22,40},{-2,60}})));

  Modelica.Blocks.Interfaces.RealInput m_flow_input(final unit="kg/s") if
    (not boundaryTypeLoading and use_mFlowInput)
    "Input for mass flow rate"
    annotation (Placement(transformation(extent={{-120,0},{-80,40}}),
      iconTransformation(extent={{-22,10},{-2,30}})));

  Modelica.Blocks.Interfaces.RealInput h_input(final unit="J/kg") if
    (use_hInput)
    "Input for specific enthalpy"
    annotation (Placement(transformation(extent={{-120,0},{-80,-40}}),
      iconTransformation(extent={{-22,-30},{-2,-10}})));

  //
  // Definition of protected connectors
  //
protected
  Modelica.Blocks.Interfaces.RealInput x_internal(final unit="kg/kg")
    "Needed for connecting to conditional connector";
  Modelica.Blocks.Interfaces.RealOutput m_flow_internal(final unit="kg/s")
    "Needed for connecting to conditional connector";

  Modelica.Blocks.Interfaces.RealInput h_internal(final unit="J/kg")
    "Needed for connecting to conditional connector";

  //
  // Definition of ports
  //
public
  SorpLib.Basics.Interfaces.SorptionPorts.AdsorptPort_in port
    "Adsorpt port"
    annotation (Placement(transformation(extent={{-10,-10},{10,10}})));

equation
  //
  // Connectors
  //
  connect(x_input,x_internal);
  connect(m_flow_input,m_flow_internal);
  connect(h_input,h_internal);

  if not use_xInput then
    x_internal = x_fixed
      "Needed for connecting to conditional connector";
  end if;
  if not use_mFlowInput then
    m_flow_internal = m_flow_fixed
      "Needed for connecting to conditional connector";
  end if;
  if not use_hInput then
    h_internal = h_fixed
      "Needed for connecting to conditional connector";
  end if;

  //
  // Properties at port
  //
  if boundaryTypeLoading then
    port.x = x_internal
      "Loading at port";

  else
    port.m_flow = m_flow_internal
      "Mass flow rate at port";

  end if;

  port.h_outflow = h_internal
    "Specific enthalpy at port";

  //
  // Annotations
  //
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
                             Rectangle(
          extent={{-2,80},{2,-80}},
          lineColor={175,175,175},
          lineThickness=0.5,
          fillColor={175,175,175},
          fillPattern=FillPattern.Solid)}),                      Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    Documentation(info="<html>
<p>
This model can be used to specify either the loading or the mass flow rate as
well as the specific enthalpy at an adsorpt port.
</p>

<h4>Options</h4>
<ul>
  <li>
  <i>boundaryTypeLoading</i>: Defines if loading or mass flow rate is prescribed.
  </li>
</ul>
  
</html>",revisions="<html>
<ul>
  <li>
  December 15, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end AdsorptSource;
