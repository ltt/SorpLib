within SorpLib.Basics.Sources.Sorption;
model AdsorbateSource
  "Boundary model of an adsorbate source"

  //
  // Definition of parameters regarding the boundary type
  //
  parameter Integer no_adsorptivs = 1
    "Number of adsorptivs (i.e., components that can be adsorbed/desorbed)"
    annotation (Dialog(tab="General", group="Boundary Type"),
                Evaluate=true,
                HideResult=true);

  parameter Boolean boundaryTypePressure = true
    " = true, if pressure is prescribed; otherwise, mass flow rate is prescribed"
    annotation (Dialog(tab="General", group="Boundary Type"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);

  //
  // Definition of parameters regarding potential and flow variables
  //
  parameter Boolean use_pInput = false
    " = true, if p is defined by input; otherwise, fixed value is used"
    annotation (Dialog(tab="General",group="Potential and Flow Variables",
                enable=boundaryTypePressure),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);
  parameter Modelica.Units.SI.Pressure p_fixed = 1.01325e5
    "Fixed pressure at boundary"
    annotation (Dialog(tab="General",group="Potential and Flow Variables",
                enable=boundaryTypePressure and not use_pInput));

  parameter Boolean use_mFlowInput = false
    "=true, if m_flow is defined by input; otherwise, fixed value is used"
    annotation (Dialog(tab="General",group="Potential and Flow Variables",
                enable=not boundaryTypePressure),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);
  parameter Modelica.Units.SI.MassFlowRate m_flow_fixed = -0.1
    "Fixed sorbent mass flow rate at boundary"
    annotation (Dialog(tab="General",group="Potential and Flow Variables",
                enable=not boundaryTypePressure and not use_mFlowInput));

  //
  // Definition of parameters regarding stream variables of specific enthalpy
  //
  parameter Boolean use_hInput = false
    "=true, if h is defined by input; otherwise, fixed value is used"
    annotation (Dialog(tab="General",group="Stream Variables - Specific Enthalpy"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);
  parameter Modelica.Units.SI.SpecificEnthalpy h_fixed = 350e3
    "Fixed specific enthalpy at boundary"
    annotation (Dialog(tab="General",group="Stream Variables - Specific Enthalpy",
                enable = not use_hInput));

  //
  // Definition of parameters regarding stream variables of loadings
  //
  parameter Boolean use_xInput = false
    "=true, if x is defined by input; otherwise, fixed value is used"
    annotation (Dialog(tab="General",group="Stream Variables - Loadings"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);
  parameter SorpLib.Units.Uptake[no_adsorptivs] x_fixed=
    fill(0.05, no_adsorptivs)
    "Fixed loadings at boundary"
    annotation (Dialog(tab="General",group="Stream Variables - Loadings",
                enable=not use_xInput));

  //
  // Definition of advanced parameters
  //
  parameter Boolean avoid_events = false
    "= true, if events are avoid by using noEvent()-operator"
    annotation (Dialog(tab = "Advanced", group = "Numerics"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult = true);

  //
  // Definition of variables
  //
  Modelica.Units.SI.MassFlowRate m_flow_total
    "Total mass flow rate";
  Modelica.Units.SI.MassFlowRate[no_adsorptivs] m_flow_adsorpt_i
    "Mass flow rates of adsorpt components";

  //
  // Definition of connectors
  //
  Modelica.Blocks.Interfaces.RealInput p_input(final unit="Pa") if
    (boundaryTypePressure and use_pInput)
    "Input for pressure"
     annotation (Placement(transformation(extent={{-120,30},{-80,70}}),
       iconTransformation(extent={{-22,40},{-2,60}})));

  Modelica.Blocks.Interfaces.RealInput m_flow_input(final unit="kg/s") if
    (not boundaryTypePressure and use_mFlowInput)
    "Input for mass flow rate"
    annotation (Placement(transformation(extent={{-120,0},{-80,40}}),
      iconTransformation(extent={{-22,10},{-2,30}})));

  Modelica.Blocks.Interfaces.RealInput h_input(final unit="J/kg") if
    (use_hInput)
    "Input for specific enthalpy"
    annotation (Placement(transformation(extent={{-120,0},{-80,-40}}),
      iconTransformation(extent={{-22,-30},{-2,-10}})));

  Modelica.Blocks.Interfaces.RealInput[no_adsorptivs] x_input(each final unit="kg/kg") if
    (use_xInput)
    "Input for loadings"
    annotation (Placement(transformation(extent={{-120,-30},{-80,-70}}),
      iconTransformation(extent={{-22,-60},{-2,-40}})));

  //
  // Definition of protected connectors
  //
protected
  Modelica.Blocks.Interfaces.RealInput p_internal(final unit="Pa")
    "Needed for connecting to conditional connector";
  Modelica.Blocks.Interfaces.RealOutput m_flow_internal(final unit="kg/s")
    "Needed for connecting to conditional connector";

  Modelica.Blocks.Interfaces.RealInput h_internal(final unit="J/kg")
    "Needed for connecting to conditional connector";
  Modelica.Blocks.Interfaces.RealInput[no_adsorptivs] x_internal(each final unit="kg/kg")
    "Needed for connecting to conditional connector";

  //
  // Definition of ports
  //
public
  SorpLib.Basics.Interfaces.SorptionPorts.AdsorbatePort_in port
    "Adsorbate port"
    annotation (Placement(transformation(extent={{-10,-10},{10,10}})));

equation
  //
  // Connectors
  //
  connect(p_input,p_internal);
  connect(m_flow_input,m_flow_internal);

  connect(h_input,h_internal);
  connect(x_input,x_internal);

  if not use_pInput then
    p_internal = p_fixed
      "Needed for connecting to conditional connector";
  end if;
  if not use_mFlowInput then
    m_flow_internal = m_flow_fixed
      "Needed for connecting to conditional connector";
  end if;

  if not use_hInput then
    h_internal = h_fixed
      "Needed for connecting to conditional connector";
  end if;
  if not use_xInput then
    x_internal = x_fixed
      "Needed for connecting to conditional connector";
  end if;

  //
  // Properties at port
  //
  if boundaryTypePressure then
    port.p = p_internal
      "Pressure at port";

  else
    port.m_flow = m_flow_internal
      "Mass flow rate at port";

  end if;

  port.h_outflow = h_internal
    "Specific enthalpy at port";
  port.x_outflow = x_internal
    "Loadings at port";

  //
  // Calculate mass flow rates
  //
  m_flow_total = port.m_flow + sum(m_flow_adsorpt_i)
    "Total mass flow rate";
  m_flow_adsorpt_i = if avoid_events then
    port.m_flow .* noEvent(actualStream(port.x_outflow)) else
    port.m_flow .* actualStream(port.x_outflow)
    "Mass flow rates of adsorpt components";

  //
  // Annotations
  //
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
                             Rectangle(
          extent={{-2,80},{2,-80}},
          lineColor={0,0,0},
          lineThickness=0.5,
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid)}),                      Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    Documentation(info="<html>
<p>
This model can be used to specify either the pressure or the mass flow rate as
well as the specific enthalpy and loadings at an adsorbate port. Note that the 
mass flow rate does not indicate the total mass flow rate, but only the mass flow 
rate of the sorbent. The mass flow rates of the adsorpt components are calculated as 
<i>port.m_flow * actualStram(port.x_outflow)</i>.
</p>

<h4>Options</h4>
<ul>
  <li>
  <i>boundaryTypePressure</i>: Defines if pressure or mass flow rate is
  prescribed.
  </li>
</ul>
  
</html>",revisions="<html>
<ul>
  <li>
  December 15, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end AdsorbateSource;
