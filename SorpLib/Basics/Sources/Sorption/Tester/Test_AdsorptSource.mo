within SorpLib.Basics.Sources.Sorption.Tester;
model Test_AdsorptSource "Tester for adsorpt source"
  extends Modelica.Icons.Example;

  //
  // Definition of boundary models
  //
  SorpLib.Basics.Sources.Sorption.AdsorptSource xFixed(
    m_flow_fixed=-0.1,
    h_fixed=151688,
    x_fixed=0.05) "Adsorbate source with fixed loading"
    annotation (Placement(transformation(extent={{-50,40},{-30,60}})));
  SorpLib.Basics.Sources.Sorption.AdsorptSource xVar(use_xInput=true,
    h_fixed=151688) "Adsorbate source with variable loading"
    annotation (Placement(transformation(extent={{-50,-60},{-30,-40}})));

  SorpLib.Basics.Sources.Sorption.AdsorptSource mFixed(boundaryTypeLoading=false,
    h_fixed=151688)
    "Adsorbate source with fixed mass flow rate"
    annotation (Placement(transformation(extent={{50,40},{30,60}})));
  SorpLib.Basics.Sources.Sorption.AdsorptSource mVar_hVar(
    boundaryTypeLoading=false,
    use_hInput=true,
    use_mFlowInput=true,
    h_fixed=151688)
    "Adsorbate source with variable mass flow rate and specific enthalpy"
    annotation (Placement(transformation(extent={{50,-60},{30,-40}})));

protected
  Modelica.Blocks.Sources.Ramp input_xVar(
    height=0.2,
    duration=2500,
    offset=0.05,
    startTime=0) "Ramp to simulate input signal of loading"
    annotation (Placement(transformation(extent={{-80,-58},{-60,-38}})),
                HideResult=true);
  Modelica.Blocks.Sources.Ramp input_mVar(
    height=0.2,
    duration=2500,
    offset=-0.1,
    startTime=0) "Ramp to simulate input signal of mass flow rate"
    annotation (Placement(transformation(extent={{80,-40},{60,-20}})),
    HideResult=true);

  Modelica.Blocks.Sources.Ramp input_hVar(
    height=-0.5e5,
    duration=2500,
    offset=1.5e5,
    startTime=0) "Ramp to simulate input signal of specific enthalpy"
    annotation (Placement(transformation(extent={{80,-80},{60,-60}})),
      HideResult=true);

equation
  //
  // Connections
  //
  connect(xFixed.port, mFixed.port) annotation (Line(
      points={{-40,50},{40,50}},
      color={175,175,175},
      thickness=1));
  connect(xVar.port, mVar_hVar.port) annotation (Line(
      points={{-40,-50},{40,-50}},
      color={175,175,175},
      thickness=1));

  connect(input_xVar.y, xVar.x_input) annotation (Line(points={{-59,-48},{-50,-48},
          {-50,-45},{-41.2,-45}}, color={0,0,127}));
  connect(input_mVar.y, mVar_hVar.m_flow_input) annotation (Line(points={{59,-30},
          {50,-30},{50,-48},{41.2,-48}}, color={0,0,127}));
  connect(input_hVar.y, mVar_hVar.h_input) annotation (Line(points={{59,-70},{50,
          -70},{50,-52},{41.2,-52}}, color={0,0,127}));

  //
  // Annotations
  //
  annotation (experiment(StopTime=2500), Documentation(info="<html>
<p>
This model checks the adsorpt source model used to prescribe properties 
at an adsorpt port.
<br/><br/>
To see the model behavior, plot the variables of the models over the 
time. The simulation time is correctly preset (Start: 0 s, Stop = 2500 s). 
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 15, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Test_AdsorptSource;
