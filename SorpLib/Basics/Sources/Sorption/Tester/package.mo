within SorpLib.Basics.Sources.Sorption;
package Tester "Models to test and varify models for sorption sources"
  extends Modelica.Icons.ExamplesPackage;

  annotation (Documentation(info="<html>
<p>
This package contains executable test models for all implemented sorption
sources. 
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 15, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Tester;
