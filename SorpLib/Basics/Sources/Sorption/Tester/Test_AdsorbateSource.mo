within SorpLib.Basics.Sources.Sorption.Tester;
model Test_AdsorbateSource "Tester for adsorbate source"
  extends Modelica.Icons.Example;

  //
  // Definition of boundary models
  //
  SorpLib.Basics.Sources.Sorption.AdsorbateSource pFixed(
    boundaryTypePressure=false,
    p_fixed(displayUnit="Pa"),
    m_flow_fixed=-0.1,
    h_fixed=151688,
    x_fixed={0.05})
    "Adsorbate source with fixed pressure"
    annotation (Placement(transformation(extent={{-50,40},{-30,60}})));
  SorpLib.Basics.Sources.Sorption.AdsorbateSource pVar(
    use_pInput=true,
    h_fixed=151688,
    x_fixed={0.05}) "Adsorbate source with variable pressure"
    annotation (Placement(transformation(extent={{-50,-42},{-30,-22}})));

  SorpLib.Basics.Sources.Sorption.AdsorbateSource mFixed(
    p_fixed=545200000,
    h_fixed=151688,
    x_fixed={0.05})
    "Adsorbate source with fixed mass flow rate"
    annotation (Placement(transformation(extent={{50,40},{30,60}})));
  SorpLib.Basics.Sources.Sorption.AdsorbateSource mVar(
    boundaryTypePressure=false,
    use_mFlowInput=true,
    h_fixed=151688,
    x_fixed={0.05}) "Adsorbate source with variable mass flow rate"
    annotation (Placement(transformation(extent={{50,-42},{30,-22}})));

  SorpLib.Basics.Sources.Sorption.AdsorbateSource pVar_hVar_X_var(
    use_pInput=true,
    use_hInput=true,
    use_xInput=true)
    "Adsorbate source with variable pressure, specific enthaly, and loadings"
    annotation (Placement(transformation(extent={{-50,-80},{-30,-60}})));
  SorpLib.Basics.Sources.Sorption.AdsorbateSource mVar_h_var(
    boundaryTypePressure=false,
    use_mFlowInput=true,
    use_hInput=true,
    x_fixed={0.05})
    "Adsorbate source with variable mass flow rate and specific enthalpy"
    annotation (Placement(transformation(extent={{50,-80},{30,-60}})));

protected
  Modelica.Blocks.Sources.Ramp input_pVar(
    height=1e5-1e3,
    duration=2500,
    offset=1e3,
    startTime=0) "Ramp to simulate input signal of pressure"
    annotation (Placement(transformation(extent={{-80,-40},{-60,-20}})),
                HideResult=true);
  Modelica.Blocks.Sources.Ramp input_mVar(
    height=0.2,
    duration=2500,
    offset=-0.1,
    startTime=0) "Ramp to simulate input signal of mass flow rate"
    annotation (Placement(transformation(extent={{80,-40},{60,-20}})),
    HideResult=true);

  Modelica.Blocks.Sources.Ramp input_hVar(
    height=-0.5e5,
    duration=2500,
    offset=1.5e5,
    startTime=0) "Ramp to simulate input signal of specific enthalpy"
    annotation (Placement(transformation(extent={{80,-80},{60,-60}})),
      HideResult=true);
  Modelica.Blocks.Sources.Ramp input_xVar(
    height=0.2,
    duration=2500,
    offset=0.05,
    startTime=0) "Ramp to simulate input signal of loading" annotation (
      Placement(transformation(extent={{-80,-80},{-60,-60}})), HideResult=true);

equation
  //
  // Connections
  //
  connect(pFixed.port, mFixed.port) annotation (Line(
      points={{-40,50},{40,50}},
      color={0,0,0},
      thickness=1));
  connect(pVar.port, mVar.port) annotation (Line(
      points={{-40,-32},{40,-32}},
      color={0,0,0},
      thickness=1));
  connect(pVar_hVar_X_var.port, mVar_h_var.port) annotation (Line(
      points={{-40,-70},{40,-70}},
      color={0,0,0},
      thickness=1));

  connect(input_pVar.y, pVar.p_input) annotation (Line(points={{-59,-30},{-50,-30},
          {-50,-27},{-41.2,-27}}, color={0,0,127}));
  connect(input_mVar.y, mVar.m_flow_input) annotation (Line(points={{59,-30},{41.2,
          -30}},                     color={0,0,127}));
  connect(input_pVar.y, pVar_hVar_X_var.p_input) annotation (Line(points={{-59,-30},
          {-50,-30},{-50,-65},{-41.2,-65}}, color={0,0,127}));
  connect(input_mVar.y, mVar_h_var.m_flow_input) annotation (Line(points={{59,-30},
          {50,-30},{50,-68},{41.2,-68}}, color={0,0,127}));
  connect(input_hVar.y, mVar_h_var.h_input) annotation (Line(points={{59,-70},{50,
          -70},{50,-72},{41.2,-72}}, color={0,0,127}));
  connect(input_hVar.y, pVar_hVar_X_var.h_input) annotation (Line(points={{59,-70},
          {50,-70},{50,-90},{-50,-90},{-50,-72},{-41.2,-72}}, color={0,0,127}));
  connect(input_xVar.y, pVar_hVar_X_var.x_input[1]) annotation (Line(points={{-59,
          -70},{-52,-70},{-52,-75},{-41.2,-75}}, color={0,0,127}));

  //
  // Annotations
  //
  annotation (experiment(StopTime=2500), Documentation(info="<html>
<p>
This model checks the adsorbate source model used to prescribe properties 
at an adsorbate port.
<br/><br/>
To see the model behavior, plot the variables of the models over the 
time. The simulation time is correctly preset (Start: 0 s, Stop = 2500 s). 
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 15, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Test_AdsorbateSource;
