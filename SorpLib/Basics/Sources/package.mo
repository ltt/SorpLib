within SorpLib.Basics;
package Sources "Library containing models defining fixed or prescribed boundary conditions"
  extends Modelica.Icons.SourcesPackage;

annotation (Documentation(info="<html>
<p>
This package contains generic sources for thermal or fluid connectors to define 
fixed or prescribed conditions. 
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end Sources;
