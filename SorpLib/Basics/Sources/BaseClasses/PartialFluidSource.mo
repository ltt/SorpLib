within SorpLib.Basics.Sources.BaseClasses;
partial model PartialFluidSource
  "Base model for all fluid sources"

  //
  // Definition of parameters regarding the boundary type
  //
  parameter Integer no_components = 1
    "Number of components within the medium"
    annotation (Dialog(tab="General", group="Boundary Type"),
                Evaluate=true,
                HideResult=true);

  parameter SorpLib.Choices.BoundaryFluidPotentialFlow boundaryTypePotentialFlow=
    SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure
    "Boundary type for potential or flow variable"
    annotation (Dialog(tab="General", group="Boundary Type"),
                Evaluate=true,
                HideResult=true);
  parameter SorpLib.Choices.BoundaryFluidStreamEnthalpy boundaryTypeStreamEnthalpy=
    SorpLib.Choices.BoundaryFluidStreamEnthalpy.SpecificEnthalpy
    "Boundary type for stream variable of specific enthalpy"
    annotation (Dialog(tab="General", group="Boundary Type"),
                Evaluate=true,
                HideResult=true);
  parameter SorpLib.Choices.BoundaryFluidStreamMassFractions boundaryTypeStreamMassFractions=
    SorpLib.Choices.BoundaryFluidStreamMassFractions.MassFractions
    "Boundary type for stream variable of mass fractions"
    annotation (Dialog(tab="General", group="Boundary Type"),
                Evaluate=true,
                HideResult=true);

  //
  // Definition of parameters regarding potential and flow variables
  //
  parameter Boolean use_pInput = false
    " = true, if p is defined by input; otherwise, fixed value is used"
    annotation (Dialog(tab="General",group="Potential and Flow Variables",
                enable=(boundaryTypePotentialFlow ==
                SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure)),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);
  parameter Modelica.Units.SI.Pressure p_fixed = 1.01325e5
    "Fixed pressure at boundary"
    annotation (Dialog(tab="General",group="Potential and Flow Variables",
                enable=(boundaryTypePotentialFlow ==
                SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure)
                and not use_pInput));

  parameter Boolean use_mFlowInput = false
    "=true, if m_flow is defined by input; otherwise, fixed value is used"
    annotation (Dialog(tab="General",group="Potential and Flow Variables",
                enable=(boundaryTypePotentialFlow ==
                SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate)),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);
  parameter Modelica.Units.SI.MassFlowRate m_flow_fixed = -0.001
    "Fixed mass flow rate at boundary"
    annotation (Dialog(tab="General",group="Potential and Flow Variables",
                enable=(boundaryTypePotentialFlow ==
                SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate)
                and not use_mFlowInput));

  parameter Boolean use_VFlowInput = false
    "=true, if V_flow is defined by input; otherwise, fixed value is used"
    annotation (Dialog(tab="General",group="Potential and Flow Variables",
                enable=(boundaryTypePotentialFlow ==
                SorpLib.Choices.BoundaryFluidPotentialFlow.VolumeFlowRate)),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);
  parameter Modelica.Units.SI.VolumeFlowRate V_flow_fixed = -10/60
    "Fixed volume flow rate at boundary"
    annotation (Dialog(tab="General",group="Potential and Flow Variables",
                enable=(boundaryTypePotentialFlow ==
                SorpLib.Choices.BoundaryFluidPotentialFlow.VolumeFlowRate)
                and not use_VFlowInput));

  //
  // Definition of parameters regarding stream variables of specific enthalpy
  //
  parameter Boolean use_hInput = false
    "=true, if h is defined by input; otherwise, fixed value is used"
    annotation (Dialog(tab="General",group="Stream Variables - Specific Enthalpy",
                enable=(boundaryTypeStreamEnthalpy ==
                SorpLib.Choices.BoundaryFluidStreamEnthalpy.SpecificEnthalpy)),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);
  parameter Modelica.Units.SI.SpecificEnthalpy h_fixed = 350e3
    "Fixed specific enthalpy at boundary"
    annotation (Dialog(tab="General",group="Stream Variables - Specific Enthalpy",
                enable=(boundaryTypeStreamEnthalpy ==
                SorpLib.Choices.BoundaryFluidStreamEnthalpy.SpecificEnthalpy)
                and not use_hInput));

  parameter Boolean use_TInput = false
    "=true, if T is defined by input; otherwise, fixed value is used"
    annotation (Dialog(tab="General",group="Stream Variables - Specific Enthalpy",
                enable=(boundaryTypeStreamEnthalpy ==
                SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature)),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);
  parameter Modelica.Units.SI.Temperature T_fixed = 298.15
    "Fixed temperature at boundary"
    annotation (Dialog(tab="General",group="Stream Variables - Specific Enthalpy",
                enable=(boundaryTypeStreamEnthalpy ==
                SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature)
                and not use_TInput));

  //
  // Definition of parameters regarding stream variables of mass fractions
  //
  parameter Boolean use_XInput = false
    "=true, if X is defined by input; otherwise, fixed value is used"
    annotation (Dialog(tab="General",group="Stream Variables - Mass Fractions",
                enable=(boundaryTypeStreamMassFractions ==
                SorpLib.Choices.BoundaryFluidStreamMassFractions.MassFractions)),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);
  parameter Modelica.Units.SI.MassFraction[no_components] X_fixed=
    fill(1/no_components, no_components)
    "Fixed mass fractions at boundary"
    annotation (Dialog(tab="General",group="Stream Variables - Mass Fractions",
                enable=(boundaryTypeStreamMassFractions ==
                SorpLib.Choices.BoundaryFluidStreamMassFractions.MassFractions)
                and not use_XInput));

  //
  // Definition of advanced parameters
  //
  parameter Modelica.Units.SI.MassFlowRate m_flow_small = 1e-5
    "Regularization value for mass flow rate to handle flow reversal and division
    by zero"
    annotation (Dialog(tab = "Advanced", group = "Limiter"),
                Evaluate=true,
                HideResult=true);

  parameter Boolean avoid_events = false
    "= true, if events are avoid by using noEvent()-operator"
    annotation (Dialog(tab = "Advanced", group = "Numerics"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult = true);

  //
  // Definition of connectors
  //
  Modelica.Blocks.Interfaces.RealInput p_input(final unit="Pa") if
    (boundaryTypePotentialFlow ==
    SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure and
    use_pInput)
    "Input for pressure"
     annotation (Placement(transformation(extent={{-120,30},{-80,70}}),
       iconTransformation(extent={{-22,40},{-2,60}})));

  Modelica.Blocks.Interfaces.RealInput m_flow_input(final unit="kg/s") if
    (boundaryTypePotentialFlow ==
    SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate and
    use_mFlowInput)
    "Input for mass flow rate"
    annotation (Placement(transformation(extent={{-120,0},{-80,40}}),
      iconTransformation(extent={{-22,10},{-2,30}})));

  Modelica.Blocks.Interfaces.RealInput V_flow_input(final unit="m3/s") if
    (boundaryTypePotentialFlow ==
    SorpLib.Choices.BoundaryFluidPotentialFlow.VolumeFlowRate and
    use_VFlowInput)
    "Input for volume flow rate"
    annotation (Placement(transformation(extent={{-120,0},{-80,40}}),
      iconTransformation(extent={{-22,10},{-2,30}})));

  Modelica.Blocks.Interfaces.RealInput h_input(final unit="J/kg") if
    (boundaryTypeStreamEnthalpy ==
    SorpLib.Choices.BoundaryFluidStreamEnthalpy.SpecificEnthalpy and
    use_hInput)
    "Input for specific enthalpy"
    annotation (Placement(transformation(extent={{-120,0},{-80,-40}}),
      iconTransformation(extent={{-22,-30},{-2,-10}})));

  Modelica.Blocks.Interfaces.RealInput T_input(final unit="K") if
    (boundaryTypeStreamEnthalpy ==
    SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature and
    use_TInput)
    "Input for temperature"
    annotation (Placement(transformation(extent={{-120,0},{-80,-40}}),
      iconTransformation(extent={{-22,-30},{-2,-10}})));

  Modelica.Blocks.Interfaces.RealInput[no_components] X_input(each final unit="kg/kg") if
    (boundaryTypeStreamMassFractions ==
    SorpLib.Choices.BoundaryFluidStreamMassFractions.MassFractions and
    use_XInput) "Input for mass fractions"
    annotation (Placement(transformation(extent={{-120,-30},{-80,-70}}),
      iconTransformation(extent={{-22,-60},{-2,-40}})));

  //
  // Definition of protected connectors
  //
protected
  Modelica.Blocks.Interfaces.RealInput p_internal(final unit="Pa")
    "Needed for connecting to conditional connector";
  Modelica.Blocks.Interfaces.RealInput m_flow_internal(final unit="kg/s")
    "Needed for connecting to conditional connector";
  Modelica.Blocks.Interfaces.RealInput V_flow_internal(final unit="m3/s")
    "Needed for connecting to conditional connector";

  Modelica.Blocks.Interfaces.RealInput h_internal(final unit="J/kg")
    "Needed for connecting to conditional connector";
  Modelica.Blocks.Interfaces.RealInput T_internal(final unit="K")
    "Needed for connecting to conditional connector";

  Modelica.Blocks.Interfaces.RealInput[no_components] X_internal(each final unit="kg/kg")
    "Needed for connecting to conditional connector";

  //
  // Definition of protected variables
  //
  Modelica.Units.SI.Density d_in
    "Density calculated with pressure, specific enthalpy, and mass fractions 
    entering the port";
  Modelica.Units.SI.Density d_out
    "Density calculated with pressure, specific enthalpy, and mass fractions 
    leaving the port";
  Modelica.Units.SI.SpecificEnthalpy h
    "Specific enthalpy calculated with pressure at port, temperature, and mass
    fractions";

  //
  // Definition of ports
  //
public
  replaceable SorpLib.Basics.Interfaces.BaseClasses.PartialFluidPort port
    constrainedby SorpLib.Basics.Interfaces.BaseClasses.PartialFluidPort(
      final no_components=no_components)
    "Fluid port"
    annotation (Placement(transformation(extent={{-4,-4},{4,4}})));

equation
  //
  // Connectors
  //
  connect(p_input,p_internal);
  connect(m_flow_input,m_flow_internal);
  connect(V_flow_input,V_flow_internal);

  connect(h_input,h_internal);
  connect(T_input,T_internal);

  connect(X_input,X_internal);

  if not use_pInput then
    p_internal = p_fixed
      "Needed for connecting to conditional connector";
  end if;
  if not use_mFlowInput then
    m_flow_internal = m_flow_fixed
      "Needed for connecting to conditional connector";
  end if;
  if not use_VFlowInput then
    V_flow_internal = V_flow_fixed
      "Needed for connecting to conditional connector";
  end if;

  if not use_hInput then
    h_internal = h_fixed
      "Needed for connecting to conditional connector";
  end if;
  if not use_TInput then
    T_internal = T_fixed
      "Needed for connecting to conditional connector";
  end if;

  if not use_XInput then
    X_internal = X_fixed
      "Needed for connecting to conditional connector";
  end if;

  //
  // Properties at port
  //
  if boundaryTypePotentialFlow ==
    SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure then
    port.p = p_internal
      "Pressure at port";

  elseif boundaryTypePotentialFlow ==
    SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate then
    port.m_flow = m_flow_internal
      "Mass flow rate at port";

  else
    if not avoid_events then
      port.m_flow = V_flow_internal *
        SorpLib.Numerics.regStep(
          x=V_flow_internal,
          y1=d_in,
          y2=d_out,
          x_small=m_flow_small*1e-3)
        "Mass flow rate at port";

    else
      port.m_flow = V_flow_internal *
        SorpLib.Numerics.regStep_noEvent(
          x=V_flow_internal,
          y1=d_in,
          y2=d_out,
          x_small=m_flow_small*1e-3)
        "Mass flow rate at port";

    end if;
  end if;

  if boundaryTypeStreamEnthalpy ==
    SorpLib.Choices.BoundaryFluidStreamEnthalpy.SpecificEnthalpy then
    port.h_outflow = h_internal
      "Specific enthalpy at port";

  else
    port.h_outflow = h
      "Specific enthalpy at port";

  end if;

  if boundaryTypeStreamMassFractions ==
    SorpLib.Choices.BoundaryFluidStreamMassFractions.MassFractions then
    port.Xi_outflow = X_internal[1:no_components-1]
      "Independent mass fractions at port";

  end if;

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial model can be used to specify either the pressure or mass flow rate as well
as the specific enthalpy and independent mass fractions at a fluid port.
<br/><br/>
Models that inherit properties from this partial model must augment equations for
the densities <i>d_in</i>/<i>d_out</i> and specific enthalpy <i>h</i>. Furthermore, 
the boundary type <i>boundaryTypeStreamMassFractions</i> must either be fixed or 
equations must be augmented for the case of given dry air mass fractions and 
relative humidity.
</p>

<h4>Options</h4>
<ul>
  <li>
  <i>boundaryTypePotentialFlow</i>: Defines if pressure, mass flow rate, or volume
  flow rate are prescribed.
  </li>
  <li>
  <i>boundaryTypeStreamEnthalpy</i>: Defines if specific enthalpy or temperature
  are prescribed.
  </li>
  <li>
  <i>boundaryTypeStreamMassFractions</i>: Defines if mass fractions or mass fractions
  of dry air and relative humidity are prescribed.
  </li>
</ul>  
</html>",revisions="<html>
<ul>
  <li>
  December 4, 2023, by Mirko Engelpracht:<br/>
  Added documentation.
  </li>
  <li>
  January 11, 2021, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end PartialFluidSource;
