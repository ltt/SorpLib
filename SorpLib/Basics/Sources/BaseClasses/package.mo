within SorpLib.Basics.Sources;
package BaseClasses "Base classes used to create new sources"
  extends Modelica.Icons.BasesPackage;

  annotation (Documentation(info="<html>
<p>
This package contains partial models. These partial models contain fundamental 
definitions for thermal and fluid source models. The content of this package is 
only of interest when adding new source models to the library. 
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end BaseClasses;
