within SorpLib.Basics.Sources.Fluids;
model GasVaporMixtureSource
  "Boundary model of an ideal gas-vapor mixture"
  extends SorpLib.Basics.Sources.BaseClasses.PartialFluidSource(
    redeclare final Interfaces.FluidPorts.GasPort_in port,
    final no_components = Medium.nX,
    X_fixed=Medium.reference_X);

  //
  // Definition of parameters regarding the medium
  //
  replaceable package Medium =
    SorpLib.Media.IdealGasVaporMixtures.MoistAir_N2_O2_CO2_H2O
    constrainedby
    SorpLib.Media.IdealGasVaporMixtures.Interfaces.PartialIdealGasWaterVaporMixture
    "Medium model of the ideal gas-vapor mixture"
    annotation (Dialog(tab="General", group="Boundary Type"),
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

  //
  // Definition of parameters regarding stream variables of mass fractions
  //
  parameter Boolean use_xDryInput = false
    "=true, if xDry is defined by input; otherwise, fixed value is used"
    annotation (Dialog(tab="General",group="Stream Variables - Mass Fractions",
                enable=(boundaryTypeStreamMassFractions ==
                SorpLib.Choices.BoundaryFluidStreamMassFractions.DryMassFractionsPhi)),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);
  parameter Modelica.Units.SI.MassFraction[no_components-1] xDry_fixed=
    Medium.reference_X[1:no_components-1]/sum(Medium.reference_X[1:no_components-1])
    "Fixed mass fractions of dry air components (per dry air component mass) at boundary"
    annotation (Dialog(tab="General",group="Stream Variables - Mass Fractions",
                enable=(boundaryTypeStreamMassFractions ==
                SorpLib.Choices.BoundaryFluidStreamMassFractions.DryMassFractionsPhi)
                and not use_xDryInput));

  parameter Boolean use_phiInput = false
    "=true, if phi is defined by input; otherwise, fixed value is used"
    annotation (Dialog(tab="General",group="Stream Variables - Mass Fractions",
                enable=(boundaryTypeStreamMassFractions ==
                SorpLib.Choices.BoundaryFluidStreamMassFractions.DryMassFractionsPhi)),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);
  parameter Real phi_fixed(unit="1") = 0.5
    "Fixed relative humidity at boundary"
    annotation (Dialog(tab="General",group="Stream Variables - Mass Fractions",
                enable=(boundaryTypeStreamMassFractions ==
                SorpLib.Choices.BoundaryFluidStreamMassFractions.DryMassFractionsPhi)
                and not use_phiInput));

  //
  // Definition of connectors
  //
  Modelica.Blocks.Interfaces.RealInput[no_components-1]
    XDry_input(each final unit="kg/kg") if
    (boundaryTypeStreamMassFractions ==
    SorpLib.Choices.BoundaryFluidStreamMassFractions.DryMassFractionsPhi and
    use_xDryInput)
    "Input for mass fractions of dry gas components"
    annotation (Placement(transformation(extent={{-120,-30},{-80,-70}}),
                iconTransformation(extent={{-22,-60},{-2,-40}})));
  Modelica.Blocks.Interfaces.RealInput phi_input(final unit="1") if
    (boundaryTypeStreamMassFractions ==
    SorpLib.Choices.BoundaryFluidStreamMassFractions.DryMassFractionsPhi and
    use_phiInput)
    "Input for relative humidity"
    annotation (Placement(transformation(extent={{-120,-60},{-80,-100}}),
                iconTransformation(extent={{-22,-90},{-2,-70}})));

  //
  // Definition of protected connectors
  //
protected
  Modelica.Blocks.Interfaces.RealInput[no_components-1] xDry_internal(each final unit="kg/kg")
    "Needed for connecting to conditional connector";
  Modelica.Blocks.Interfaces.RealInput phi_internal(final unit="1")
    "Needed for connecting to conditional connector";

  //
  // Definition of protected variables
  //
  Modelica.Units.SI.MassFraction[no_components] X
    "Mass fractions calculated with pressure and specific enthalpy leaving the 
    port as well as dry air mass fractions and relative humidity";

equation
  //
  // Assertions
  //
  if boundaryTypeStreamMassFractions ==
    SorpLib.Choices.BoundaryFluidStreamMassFractions.DryMassFractionsPhi then
    assert((boundaryTypeStreamEnthalpy ==
      SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature) and
      (boundaryTypePotentialFlow ==
      SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure),
      "Can only use mass fractions of dry air components and relative humidity " +
      "if pressure and temperature are prescribed at the boundary model!",
      level = AssertionLevel.error);

  end if;

  //
  // Connectors
  //
  connect(XDry_input,xDry_internal);
  connect(phi_input,phi_internal);

  if not use_xDryInput then
    xDry_internal = xDry_fixed
      "Needed for connecting to conditional connector";
  end if;
  if not use_phiInput then
    phi_internal = phi_fixed
      "Needed for connecting to conditional connector";
  end if;

  //
  // Calculate properties
  //
  if boundaryTypePotentialFlow ==
    SorpLib.Choices.BoundaryFluidPotentialFlow.VolumeFlowRate then
      d_in = Medium.density_phX(
        p=port.p,
        h=inStream(port.h_outflow),
        X=cat(1, inStream(port.Xi_outflow), {1-sum(inStream(port.Xi_outflow))}))
      "Density calculated with pressure, specific enthalpy, and mass fractions 
      entering the port";

      d_out =if boundaryTypeStreamEnthalpy <>
        SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature then
        Medium.density_phX(
          p=port.p,
          h=port.h_outflow,
          X=cat(1, port.Xi_outflow, {1-sum(port.Xi_outflow)})) else
        Medium.density_pTX(
          p=port.p,
          T=T_internal,
          X=cat(1, port.Xi_outflow, {1-sum(port.Xi_outflow)}))
      "Density calculated with pressure, specific enthalpy or temperature, and 
      mass fractions  leaving the port";

  else
    d_in = m_flow_fixed / V_flow_fixed
      "Density calculated with pressure, specific enthalpy, and mass fractions 
      entering the port: Not needed, so set dummy value";
    d_out = m_flow_fixed / V_flow_fixed
      "Density calculated with pressure, specific enthalpy, and mass fractions 
      leaving the port: Not needed, so set dummy value";

  end if;

  if boundaryTypeStreamEnthalpy ==
    SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature then
    h = Medium.specificEnthalpy_pTX(
      p=port.p,
      T=T_internal,
      X=X_internal)
      "Specific enthalpy calculated with pressure at port and temperature";

  else
    h = h_fixed
      "Specific enthalpy calculated with pressure at port and temperature: Not
      needed, so set dummy value";

  end if;

  if boundaryTypeStreamMassFractions ==
    SorpLib.Choices.BoundaryFluidStreamMassFractions.DryMassFractionsPhi then
    X = Medium.massFractions_pTxDryPhi(
      p = port.p,
      T=T_internal,
      x=xDry_internal,
      phi=phi_internal)
      "Mass fractions calculated with pressure and specific enthalpy leaving the 
      port as well as dry air mass fractions and relative humidity";

    port.Xi_outflow = X[1:no_components-1]
      "Independent mass fractions at port";

  else
    X = X_fixed
      "Mass fractions calculated with pressure and specific enthalpy leaving the 
      port as well as dry air mass fractions and relative humidity: Not needed, 
      so set dummy values";

  end if;

  //
  // Annotations
  //
  annotation (Icon(graphics={Rectangle(
          extent={{-2,80},{2,-80}},
          lineColor={244,125,35},
          lineThickness=0.5,
          fillColor={244,125,35},
          fillPattern=FillPattern.Solid)}),
          Documentation(info="<html>
<p>
This model can be used to specify either the pressure or mass flow rate as well as
the specific enthalpy and independent mass fractions at an ideal gas-vapor mixture
port.
</p>

<h4>Options</h4>
<ul>
  <li>
  <i>boundaryTypePotentialFlow</i>: Defines if pressure, mass flow rate, or volume
  flow rate are prescribed.
  </li>
  <li>
  <i>boundaryTypeStreamEnthalpy</i>: Defines if specific enthalpy or temperature
  are prescribed.
  </li>
  <li>
  <i>boundaryTypeStreamMassFractions</i>: Defines if mass fractions or mass fractions
  of dry air and relative humidity are prescribed.
  </li></ul>
  
</html>",revisions="<html>
<ul>
  <li>
  December 5, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end GasVaporMixtureSource;
