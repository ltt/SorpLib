within SorpLib.Basics.Sources.Fluids;
model LiquidSource
  "Boundary model of an (ideal) liquid"
  extends SorpLib.Basics.Sources.BaseClasses.PartialFluidSource(
    redeclare final Interfaces.FluidPorts.LiquidPort_in port,
    final no_components = Medium.nX,
    final boundaryTypeStreamMassFractions=
      SorpLib.Choices.BoundaryFluidStreamMassFractions.MassFractions,
    X_fixed=Medium.reference_X);

  //
  // Definition of parameters regarding the medium
  //
  replaceable package Medium = Modelica.Media.Water.WaterIF97_R1pT
    constrainedby Modelica.Media.Interfaces.PartialMedium
    "Medium model of the (ideal) liquid"
    annotation (Dialog(tab="General", group="Boundary Type"),
                Evaluate=true,
                HideResult=true,
                choicesAllMatching=true);

equation
  //
  // Calculate properties
  //
  if boundaryTypePotentialFlow ==
    SorpLib.Choices.BoundaryFluidPotentialFlow.VolumeFlowRate then
      d_in = Medium.density_phX(
        p=port.p,
        h=inStream(port.h_outflow),
        X=cat(1, inStream(port.Xi_outflow), {1-sum(inStream(port.Xi_outflow))}))
      "Density calculated with pressure, specific enthalpy, and mass fractions 
      entering the port";

      d_out =if boundaryTypeStreamEnthalpy <>
        SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature then
        Medium.density_phX(
          p=port.p,
          h=port.h_outflow,
          X=cat(1, port.Xi_outflow, {1-sum(port.Xi_outflow)})) else
        Medium.density_pTX(
          p=port.p,
          T=T_internal,
          X=cat(1, port.Xi_outflow, {1-sum(port.Xi_outflow)}))
      "Density calculated with pressure, specific enthalpy or temperature, and 
      mass fractions  leaving the port";

  else
    d_in = m_flow_fixed / V_flow_fixed
      "Density calculated with pressure, specific enthalpy, and mass fractions 
      entering the port: Not needed, so set dummy value";
    d_out = m_flow_fixed / V_flow_fixed
      "Density calculated with pressure, specific enthalpy, and mass fractions 
      leaving the port: Not needed, so set dummy value";

  end if;

  if boundaryTypeStreamEnthalpy ==
    SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature then
    h = Medium.specificEnthalpy_pTX(
      p=port.p,
      T=T_internal,
      X=X_internal)
      "Specific enthalpy calculated with pressure at port and temperature";

  else
    h = h_fixed
      "Specific enthalpy calculated with pressure at port and temperature: Not
      needed, so set dummy value";

  end if;

  //
  // Annotations
  //
  annotation (Icon(graphics={Rectangle(
          extent={{-2,80},{2,-80}},
          lineColor={28,108,200},
          lineThickness=0.5,
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid)}),
          Documentation(info="<html>
<p>
This model can be used to specify either the pressure or mass flow rate as well as
the specific enthalpy and independent mass fractions at an (ideal) liquid port.
</p>

<h4>Options</h4>
<ul>
  <li>
  <i>boundaryTypePotentialFlow</i>: Defines if pressure, mass flow rate, or volume
  flow rate are prescribed.
  </li>
  <li>
  <i>boundaryTypeStreamEnthalpy</i>: Defines if specific enthalpy or temperature
  are prescribed.
  </li>
</ul>
  
</html>",revisions="<html>
<ul>
  <li>
  December 4, 2023, by Mirko Engelpracht:<br/>
  Added documentation.
  </li>
  <li>
  January 11, 2021, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end LiquidSource;
