within SorpLib.Basics.Sources.Fluids.Tester;
model Test_GasSource
  "Tester for ideal gas or ideal gas mixture source"
  extends Modelica.Icons.Example;

  //
  // Definition of boundary models
  //
  SorpLib.Basics.Sources.Fluids.GasSource sourceA_pFixed_hFixed(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.SpecificEnthalpy,
    p_fixed=1000000) "Source A: Fixed pressure and fixed specific enthalpy"
    annotation (Placement(transformation(extent={{-50,70},{-30,90}})));

  SorpLib.Basics.Sources.Fluids.GasSource sourceB_MFlowFixed_TFixed(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    m_flow_fixed=0.2,
    T_fixed=323.15) "Source B: Fixed mass flow rate and fixed temperature"
    annotation (Placement(transformation(extent={{30,70},{50,90}})));

  SorpLib.Basics.Sources.Fluids.GasSource sourceA_pFixed_TFixed(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    p_fixed=1000000,
    T_fixed=303.15) "Source A: Fixed pressure and fixed specific enthalpy"
    annotation (Placement(transformation(extent={{-50,50},{-30,70}})));
  SorpLib.Basics.Sources.Fluids.GasSource sourceB_MFlowFixed_hFixed(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.SpecificEnthalpy,
    m_flow_fixed=-0.2,
    h_fixed=400e3) "Source B: Fixed mass flow rate and fixed specific enthalpy"
    annotation (Placement(transformation(extent={{30,50},{50,70}})));

  SorpLib.Basics.Sources.Fluids.GasSource sourceA_VFlowFixed_TFixed(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.VolumeFlowRate,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    V_flow_fixed(displayUnit="l/min") = -0.016666666666667,
    T_fixed=348.15) "Source A: Fixed volume flow rate and fixed temperature"
    annotation (Placement(transformation(extent={{-50,30},{-30,50}})));
  SorpLib.Basics.Sources.Fluids.GasSource sourceB_pFixed_TFixed(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    p_fixed=500000,
    T_fixed=283.15) "Source B: Fixed pressure and fixed temperature"
    annotation (Placement(transformation(extent={{30,30},{50,50}})));

  SorpLib.Basics.Sources.Fluids.GasSource sourceA_VFlowFixed_hFixed(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.VolumeFlowRate,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.SpecificEnthalpy,
    V_flow_fixed(displayUnit="l/min") = -0.016666666666667,
    h_fixed=275e3)
    "Source A: Fixed volume flow rate and fixed specific enthalpy"
    annotation (Placement(transformation(extent={{-50,10},{-30,30}})));
  SorpLib.Basics.Sources.Fluids.GasSource sourceB_pFixed_hFixed(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.SpecificEnthalpy,
    p_fixed=500000,
    h_fixed=300e3) "Source B: Fixed pressure and fixed specific enthalpy"
    annotation (Placement(transformation(extent={{30,10},{50,30}})));

  SorpLib.Basics.Sources.Fluids.GasSource sourceA_pVar_hFixed(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.SpecificEnthalpy,
    use_pInput=true)
    "Source A: Variable pressure and fixed specific enthalpy"
    annotation (Placement(transformation(extent={{-50,-30},{-30,-10}})));
  SorpLib.Basics.Sources.Fluids.GasSource sourceB_MFlowVar_TVar(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    use_mFlowInput=true,
    use_TInput=true)
    "Source B: Variable mass flow rate and variable temperature"
    annotation (Placement(transformation(extent={{50,-30},{30,-10}})));

  SorpLib.Basics.Sources.Fluids.GasSource sourceA_pVar_TFixed(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    use_pInput=true,
    T_fixed=303.15)
    "Source A: Variable pressure and fixed specific enthalpy"
    annotation (Placement(transformation(extent={{-50,-50},{-30,-30}})));
  SorpLib.Basics.Sources.Fluids.GasSource sourceB_MFlowVar_hFixed(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.MassFlowRate,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.SpecificEnthalpy,
    use_mFlowInput=true,
    h_fixed=400e3)
    "Source B: Variable mass flow rate and fixed specific enthalpy"
    annotation (Placement(transformation(extent={{50,-50},{30,-30}})));

  SorpLib.Basics.Sources.Fluids.GasSource sourceA_VFlowVar_TFixed(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.VolumeFlowRate,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    use_VFlowInput=true,
    V_flow_fixed(displayUnit="l/min"),
    T_fixed=348.15)
    "Source A: Variable volume flow rate and fixed temperature"
    annotation (Placement(transformation(extent={{-50,-70},{-30,-50}})));
  SorpLib.Basics.Sources.Fluids.GasSource sourceB_pFixed_TVar(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.Temperature,
    p_fixed=500000,
    use_TInput=true)
    "Source B: Fixed pressure and variable temperature"
    annotation (Placement(transformation(extent={{50,-70},{30,-50}})));

  SorpLib.Basics.Sources.Fluids.GasSource sourceA_VFlowVar_hFixed(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.VolumeFlowRate,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.SpecificEnthalpy,
    use_VFlowInput=true,
    V_flow_fixed(displayUnit="l/min"),
    h_fixed=275e3)
    "Source A: Variable volume flow rate and fixed specific enthalpy"
    annotation (Placement(transformation(extent={{-50,-90},{-30,-70}})));
  SorpLib.Basics.Sources.Fluids.GasSource sourceB_pFixed_hVar(
    boundaryTypePotentialFlow=SorpLib.Choices.BoundaryFluidPotentialFlow.Pressure,
    boundaryTypeStreamEnthalpy=SorpLib.Choices.BoundaryFluidStreamEnthalpy.SpecificEnthalpy,
    p_fixed=500000,
    use_hInput=true) "Source B: Fixed pressure and variable specific enthalpy"
    annotation (Placement(transformation(extent={{50,-90},{30,-70}})));

  //
  // Definition of input signals
  //
protected
  Modelica.Blocks.Sources.Ramp input_pVar(
    height=10e5 - 1e5,
    duration=2500,
    offset=1e5,
    startTime=0)
    "Ramp to simulate input signal of pressure"
    annotation (Placement(transformation(extent={{-80,-40},{-60,-20}})),
                HideResult=true);

  Modelica.Blocks.Sources.Ramp input_mFlowVar(
    height=2,
    duration=2500,
    offset=-1,
    startTime=0) "Ramp to simulate input signal of mass flow rate"
    annotation (Placement(transformation(extent={{80,-30},{60,-10}})),
                HideResult=true);
  Modelica.Blocks.Sources.Ramp input_VFlowVar(
    height=40000/60000,
    duration=2500,
    offset=-20000/60000,
    startTime=0) "Ramp to simulate input signal of volume flow rate"
    annotation (Placement(transformation(extent={{-80,-80},{-60,-60}})),
                HideResult=true);

  Modelica.Blocks.Sources.Ramp input_hVar(
    height=1e5,
    duration=2500,
    offset=273e3,
    startTime=0)
    "Ramp to simulate input signal of specific enthalpy"
    annotation (Placement(transformation(extent={{80,-90},{60,-70}})),
                HideResult=true);
  Modelica.Blocks.Sources.Ramp input_TVar(
    height=90,
    duration=2500,
    offset=283.15,
    startTime=0) "Ramp to simulate input signal of temperature"
    annotation (Placement(transformation(extent={{80,-60},{60,-40}})),
                HideResult=true);

equation
  //
  // Connections
  //
  connect(sourceA_pFixed_hFixed.port, sourceB_MFlowFixed_TFixed.port)
    annotation (Line(
      points={{-40,80},{40,80}},
      color={244,125,35},
      thickness=1));
  connect(sourceB_MFlowFixed_hFixed.port, sourceA_pFixed_TFixed.port)
    annotation (Line(
      points={{40,60},{-40,60}},
      color={244,125,35},
      thickness=1));
  connect(sourceA_VFlowFixed_TFixed.port, sourceB_pFixed_TFixed.port)
    annotation (Line(
      points={{-40,40},{40,40}},
      color={244,125,35},
      thickness=1));
  connect(sourceB_pFixed_hFixed.port, sourceA_VFlowFixed_hFixed.port)
    annotation (Line(
      points={{40,20},{-40,20}},
      color={244,125,35},
      thickness=1));
  connect(sourceA_pVar_hFixed.port, sourceB_MFlowVar_TVar.port) annotation (
      Line(
      points={{-40,-20},{40,-20}},
      color={244,125,35},
      thickness=1));
  connect(sourceB_MFlowVar_hFixed.port, sourceA_pVar_TFixed.port) annotation (
      Line(
      points={{40,-40},{-40,-40}},
      color={244,125,35},
      thickness=1));
  connect(sourceA_VFlowVar_TFixed.port, sourceB_pFixed_TVar.port) annotation (
      Line(
      points={{-40,-60},{40,-60}},
      color={244,125,35},
      thickness=1));
  connect(sourceB_pFixed_hVar.port, sourceA_VFlowVar_hFixed.port) annotation (
      Line(
      points={{40,-80},{-40,-80}},
      color={244,125,35},
      thickness=1));

  connect(input_pVar.y, sourceA_pVar_hFixed.p_input) annotation (Line(points={{-59,-30},
          {-50,-30},{-50,-15},{-41.2,-15}},      color={0,0,127}));
  connect(input_pVar.y, sourceA_pVar_TFixed.p_input) annotation (Line(points={{-59,-30},
          {-50,-30},{-50,-35},{-41.2,-35}},      color={0,0,127}));
  connect(input_VFlowVar.y, sourceA_VFlowVar_TFixed.V_flow_input) annotation (
      Line(points={{-59,-70},{-50,-70},{-50,-58},{-41.2,-58}}, color={0,0,127}));
  connect(input_VFlowVar.y, sourceA_VFlowVar_hFixed.V_flow_input) annotation (
      Line(points={{-59,-70},{-50,-70},{-50,-78},{-41.2,-78}}, color={0,0,127}));
  connect(input_mFlowVar.y, sourceB_MFlowVar_TVar.m_flow_input) annotation (
      Line(points={{59,-20},{54,-20},{54,-18},{41.2,-18}}, color={0,0,127}));
  connect(input_mFlowVar.y, sourceB_MFlowVar_hFixed.m_flow_input) annotation (
      Line(points={{59,-20},{54,-20},{54,-38},{41.2,-38}}, color={0,0,127}));
  connect(input_TVar.y, sourceB_MFlowVar_TVar.T_input) annotation (Line(points={{59,-50},
          {50,-50},{50,-22},{41.2,-22}},          color={0,0,127}));
  connect(input_TVar.y, sourceB_pFixed_TVar.T_input) annotation (Line(points={{59,-50},
          {50,-50},{50,-62},{41.2,-62}},      color={0,0,127}));
  connect(input_hVar.y, sourceB_pFixed_hVar.h_input) annotation (Line(points={{59,-80},
          {50,-80},{50,-82},{41.2,-82}},      color={0,0,127}));

  //
  // Annotations
  //
  annotation (experiment(StopTime=2500), Documentation(info="<html>
<p>
This model checks the ideal gas or gas mixture source model used to prescribe 
properties at a gas fluid port.
<br/><br/>
To see the model behavior, plot the variables of the models over the 
time. The simulation time is correctly preset (Start: 0 s, Stop = 2500 s). 
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 4, 2023, by Mirko Engelpracht:<br/>
  Added documentation.
  </li>
  <li>
  January 11, 2021, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Test_GasSource;
