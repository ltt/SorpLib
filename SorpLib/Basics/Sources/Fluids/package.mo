within SorpLib.Basics.Sources;
package Fluids "Fluid sources used to prescribe pressure, mass flow rate, specific enthalpy, and independent mass fractions"
  extends Modelica.Icons.VariantsPackage;

  annotation (Documentation(info="<html>
<p>
This package contains sources for fluid connectors to define fixed or prescribed 
conditions. This package calculates fluid properties based on the open-source 
Modelica Standard Library (MSL).
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end Fluids;
