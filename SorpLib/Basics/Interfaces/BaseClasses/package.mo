within SorpLib.Basics.Interfaces;
package BaseClasses "Base connectors used to create new interfaces"
  extends Modelica.Icons.BasesPackage;

  annotation (Documentation(info="<html>
<p>
This package contains partial connectors. These partial containg contain fundamental 
definitions for ports. The content of this package is only of interest when adding 
new ports to the library. 
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end BaseClasses;
