within SorpLib.Basics.Interfaces.BaseClasses;
partial connector PartialFluidPort "Base model for all fluid ports"

  //
  // Definition of parameters
  //
  parameter Integer no_components = 1
    "Number of components"
    annotation (Dialog(tab="General", group="Medium"),
                Evaluate=true,
                HideResult=true);

  //
  // Definition of connector variables
  //
  Modelica.Units.SI.Pressure p
    "Pressure in the fluid port";

  flow Modelica.Units.SI.MassFlowRate m_flow
    "Mass flow rate: If > 0, mass flows from the outside into the component.";

  stream Modelica.Units.SI.SpecificEnthalpy h_outflow
    "Specific enthalpy: If m_flow < 0, specific enthalpy equals specific enthalpy
    close to the fluid port.";
  stream Modelica.Units.SI.MassFraction Xi_outflow[no_components-1]
    "Independent mixture mass fractions m_i/m: If m_flow < 0, mass fractions equal
     mass fractions close to the fluid port.";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This connector is the base connector for all fluid ports. It defines the pressure
<i>p</i> as potential variable, the mass flow rate <i>m_flow</i> as flow variable,
and the specific enthalpy <i>h_outflow</i>, and independent mixture mass fractions
<i>Xi_outflow</i> as stream variables.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 4, 2023, by Mirko Engelpracht:<br/>
  Added documentation.
  </li>
  <li>
  January 11, 2021, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end PartialFluidPort;
