within SorpLib.Basics.Interfaces.BaseClasses;
partial connector PartialAdsorptPort
  "Base model for all adsorpt ports"

  //
  // Definition of connector variables
  //
  SorpLib.Units.Uptake x
    "Loading";

  flow Modelica.Units.SI.MassFlowRate m_flow
    "Mass flow rate: If > 0, mass flows from the outside into the component";

  stream Modelica.Units.SI.SpecificEnthalpy h_outflow
    "Specific enthalpy: If m_flow < 0, specific enthalpy equals specific enthalpy
    close to the sorption port";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This connector is the base connector for all adsorpt ports. It defines the loading
<i>x</i> as potential variable, the mass flow rate <i>m_flow</i> as flow variable, 
and the specific enthalpy <i>h_outflow</i> as stream variable.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 15, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end PartialAdsorptPort;
