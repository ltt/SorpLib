within SorpLib.Basics.Interfaces.BaseClasses;
partial connector PartialAdsorbatePort
  "Base model for all adsorbate ports"

  //
  // Definition of parameters
  //
  parameter Integer no_adsorptivs = 1
    "Number of adsorptivs (i.e., components that can be adsorbed/desorbed)"
    annotation (Dialog(tab="General", group="Medium"),
                Evaluate=true,
                HideResult=true);

  //
  // Definition of connector variables
  //
  Modelica.Units.SI.Pressure p
    "Pressure in the adsorbate port";

  flow Modelica.Units.SI.MassFlowRate m_flow
    "Mass flow rate of the sorbent: If > 0, mass flows from the outside into the 
    component";

  stream Modelica.Units.SI.SpecificEnthalpy h_outflow
    "Specific enthalpy: If m_flow < 0, specific enthalpy equals specific enthalpy
    close to the sorption port";
  stream SorpLib.Units.Uptake x_outflow[no_adsorptivs]
    "Loadings m_adsorpt_i/m_sor: If m_flow < 0, loadings equal lpadings close to
    the adsorbate port";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This connector is the base connector for all adsorbate ports. It defines the pressure
<i>p</i> as potential variable, the mass flow rate of the sorbent <i>m_flow</i> 
as flow variable, and the specific enthalpy <i>h_outflow</i> as well as the loadings
<i>x_outflow</i> as stream variables. Note that the mass flow does not indicate 
the total mass flow rate, but only the mass flow rate of the sorbent. The mass 
flow rates of the adsorpt components are calculated as 
<i>m_flow * actualStram(x_outflow)</i>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 15, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end PartialAdsorbatePort;
