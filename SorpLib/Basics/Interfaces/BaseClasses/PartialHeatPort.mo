within SorpLib.Basics.Interfaces.BaseClasses;
partial connector PartialHeatPort
  "Base model for all heat ports"

  //
  // Definition of connector variables
  //
  Modelica.Units.SI.Temperature T
    "Temperature in the heat port";

  flow Modelica.Units.SI.HeatFlowRate Q_flow
    "Heat flow rate: If > 0, heat flows from the outside into the component.";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This connector is the base connector for all heat ports. It defines the temperature
<i>T</i> as potential variable and the heat flow rate <i>Q_flow</i> as flow variable.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 4, 2023, by Mirko Engelpracht:<br/>
  Added documentation.
  </li>
  <li>
  January 11, 2021, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end PartialHeatPort;
