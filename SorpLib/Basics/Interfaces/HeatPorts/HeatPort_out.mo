within SorpLib.Basics.Interfaces.HeatPorts;
connector HeatPort_out
  "Heat port for design outlet"
  extends SorpLib.Basics.Interfaces.BaseClasses.PartialHeatPort;

  //
  // Annotations
  //
  annotation (defaultComponentName="heatPort_out",
          Icon(graphics={Rectangle(
          extent={{-90,88},{90,-92}},
          lineColor={238,46,47},
          fillColor={238,46,47},
          fillPattern=FillPattern.Solid,
          lineThickness=1), Rectangle(
          extent={{-72,72},{70,-72}},
          lineColor={255,255,255},
          lineThickness=1,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid)}),
                              Diagram(graphics={
                                          Text(extent={{-150,110},{150,50}},
            textString="%name"),
        Rectangle(
          extent={{-40,40},{40,-40}},
          lineColor={238,46,47},
          lineThickness=1,
          fillColor={238,46,47},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-36,36},{36,-36}},
          lineColor={255,255,255},
          lineThickness=1,
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid)}),
    Documentation(info="<html>
<p>
This connector is used for heat ports at the design outlet. According to the 
Modelica sign convention, a <strong>negative</strong> heat flow rate 
<strong>Q_flow</strong> is considered to flow <strong>out</strong> a component. 
This convention has to be used whenever this connector is used in a model class.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 4, 2023, by Mirko Engelpracht:<br/>
  Added documentation.
  </li>
  <li>
  January 11, 2021, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end HeatPort_out;
