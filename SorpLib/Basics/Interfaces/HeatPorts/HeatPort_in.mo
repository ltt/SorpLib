within SorpLib.Basics.Interfaces.HeatPorts;
connector HeatPort_in
  "Heat port for design inlet"
  extends SorpLib.Basics.Interfaces.BaseClasses.PartialHeatPort;

  //
  // Annotations
  //
  annotation (defaultComponentName="heatPort_in",
          Icon(graphics={Rectangle(
          extent={{-80,80},{80,-80}},
          lineColor={238,46,47},
          fillColor={238,46,47},
          fillPattern=FillPattern.Solid,
          lineThickness=1)}), Diagram(graphics={
                                          Text(extent={{-150,110},{150,50}},
            textString="%name"), Rectangle(
          extent={{-40,40},{40,-40}},
          lineColor={238,46,47},
          lineThickness=1,
          fillColor={238,46,47},
          fillPattern=FillPattern.Solid)}),
    Documentation(info="<html>
<p>
This connector is used for heat ports at the design inlet. According to the 
Modelica sign convention, a <strong>positive</strong> heat flow rate 
<strong>Q_flow</strong> is considered to flow <strong>into</strong> a component. 
This convention has to be used whenever this connector is used in a model class.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 4, 2023, by Mirko Engelpracht:<br/>
  Added documentation.
  </li>
  <li>
  January 11, 2021, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end HeatPort_in;
