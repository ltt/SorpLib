within SorpLib.Basics.Interfaces.FluidPorts;
connector GasPort_out
  "Gas port for design outlet"
  extends SorpLib.Basics.Interfaces.BaseClasses.PartialFluidPort;

  //
  // Annotations
  //
  annotation (defaultComponentName="gasPort_out",
            Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
          -100},{100,100}}), graphics={
      Ellipse(
        extent={{-40,40},{40,-40}},
        lineColor={244,125,35},
        fillColor={244,125,35},
        fillPattern=FillPattern.Solid),
      Ellipse(
        extent={{-30,30},{30,-30}},
        lineColor={0,127,255},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid),
      Text(extent={{-150,110},{150,50}}, textString="%name")}),
     Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},{
          100,100}}), graphics={
      Ellipse(
        extent={{-100,100},{100,-100}},
        lineColor={244,125,35},
        fillColor={244,125,35},
        fillPattern=FillPattern.Solid,
        lineThickness=1),
      Ellipse(
        extent={{-80,80},{80,-80}},
        lineColor={244,125,35},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid)}),
  Documentation(info="<html>
<p>
This connector is used for gas ports at the design outlet. According to the 
Modelica sign convention, a <strong>negative</strong> mass flow rate 
<strong>m_flow</strong> is considered to flow <strong>out</strong> a component. 
This convention has to be used whenever this connector is used in a model class.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 4, 2023, by Mirko Engelpracht:<br/>
  Added documentation.
  </li>
  <li>
  January 11, 2021, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end GasPort_out;
