within SorpLib.Basics;
package Interfaces "Interfaces for models"
extends Modelica.Icons.InterfacesPackage;

annotation (Documentation(info="<html>
<p>
This package provides definitions of basic connectors used to aggregate more 
complex models. 
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end Interfaces;
