within SorpLib.Units;
type DerThermalConductivityByTemperature =  Real (
  final quantity="DerThermalConductivityByTemperature",
  final unit="W/(m.K2)",
  displayUnit="W/(m.K2)")
  "First-order partial derivative of the thermal conductivity w.r.t. the 
    temperature";
