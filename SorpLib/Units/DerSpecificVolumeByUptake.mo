within SorpLib.Units;
type DerSpecificVolumeByUptake =   Real (
  final quantity="DerSpecificVolumeByUptake",
  final unit="m3.kg/(kg.kg)",
  displayUnit="m3.kg/(kg.kg)")
  "First-order partial derivative of the specific volume w.r.t. the uptake";
