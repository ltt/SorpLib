within SorpLib.Units;
type Uptake = Real (
  final quantity="Uptake",
  final unit="kg/kg",
  displayUnit="kg/kg",
  min=0,
  nominal=0.5)
  "Equilibrium uptake";
