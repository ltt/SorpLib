within SorpLib.Units;
type DerMolarAdsorptionPotentialByTemperature =Real (
  final quantity="DerMolarAdsorptionPotentialByTemperature",
  final unit="J/(mol.K)",
  displayUnit="kJ/(mol.K)")
  "First-order partial derivative of the molar adsorption potential w.r.t. 
    temperature";
