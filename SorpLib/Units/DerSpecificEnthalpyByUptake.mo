within SorpLib.Units;
type DerSpecificEnthalpyByUptake = Real (
  final quantity="DerSpecificEnthalpyByUptake",
  final unit="J.kg/(kg.kg)",
  displayUnit="kJ.kg/(kg.kg)")
  "First-order partial derivative of specific enthalpy w.r.t. uptake";
