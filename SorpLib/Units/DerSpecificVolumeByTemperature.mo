within SorpLib.Units;
type DerSpecificVolumeByTemperature = Real (
  final quantity="DerSpecificVolumeByTemperature",
  final unit="m3/(kg.K)",
  displayUnit="m3/(kg.K)")
  "First-order partial derivative of the specific volume w.r.t. the temperature";
