within SorpLib.Units;
type DerMolarEnthalpyByUptake =    Real (
  final quantity="DerMolarEnthalpyByUptake",
  final unit="J.kg/(mol.kg)",
  displayUnit="kJ.kg/(mol.kg)")
  "First-order partial derivative of the molar enthalpy w.r.t. the uptake";
