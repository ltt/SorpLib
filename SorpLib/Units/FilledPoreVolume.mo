within SorpLib.Units;
type FilledPoreVolume = Real (
  final quantity="FilledPoreVolume",
  final unit="m3/kg",
  displayUnit="l/kg",
  min=0)
  "Filled pore volume";
