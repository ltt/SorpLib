within SorpLib.Units;
type DerUptakeByTemperature = Real (
  final quantity="DerUptakeByTemperature",
  final unit="kg/(kg.K)",
  displayUnit="kg/(kg.K)")
  "First-order partial derivative of the uptake w.r.t. temperature";
