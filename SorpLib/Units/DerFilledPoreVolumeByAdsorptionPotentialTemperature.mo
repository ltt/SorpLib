within SorpLib.Units;
type DerFilledPoreVolumeByAdsorptionPotentialTemperature = Real (
  final quantity="DerFilledPoreVolumeByAdsorptionPotentialTemperature",
  final unit="m3.mol/(kg.J.K)",
  displayUnit="l.mol/(kg.kJ.K)")
  "Second-order partial derivative of the filled pore volume w.r.t. the adsorption 
  potential and temperature";
