within SorpLib.Units;
type DerSpecificEntropyByUptake = Real (
  final quantity="DerSpecificEntropyByUptake",
  final unit="J.kg/(kg2.K)",
  displayUnit="kJ.kg/(kg2.K)")
  "First-order partial derivative of specific entropy w.r.t. uptake";
