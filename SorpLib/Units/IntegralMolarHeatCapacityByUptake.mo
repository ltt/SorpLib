within SorpLib.Units;
type IntegralMolarHeatCapacityByUptake=Real (
  final quantity="IntegralMolarHeatCapacityByUptake",
  final unit="J.kg/(mol.kg.K)",
  displayUnit="kJ.kg/(mol.kg.K)")
  "Integral of the molar heat capacity w.r.t. the uptake";
