within SorpLib.Units;
type DerMolarAdsorptionPotentialByTemperatureTemperature = Real (
  final quantity="DerMolarAdsorptionPotentialByTemperatureTemperature",
  final unit="J/(mol.K2)",
  displayUnit="kJ/(mol.K2)")
  "Second-order partial derivative of the molar adsorption potential w.r.t. 
    temperature";
