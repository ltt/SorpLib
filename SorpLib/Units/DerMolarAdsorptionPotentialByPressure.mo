within SorpLib.Units;
type DerMolarAdsorptionPotentialByPressure =Real (
  final quantity="DerMolarAdsorptionPotentialByPressure",
  final unit="J/(mol.Pa)",
  displayUnit="kJ/(mol.Pa)")
  "First-order partial derivative of the molar adsorption potential w.r.t. 
    pressure";
