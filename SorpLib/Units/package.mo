within SorpLib;
package Units "Package containing definitions of units"
extends Modelica.Icons.Package;

annotation (Icon(graphics={
      Polygon(
        fillColor = {128,128,128},
        pattern = LinePattern.None,
        fillPattern = FillPattern.Solid,
        points={{-84,-42},{-84,-42},{-59,48},{-56.5,60.5},{-69,58},{-69,63},{
            -39,75.5},{-36.5,58},{-54,-2},{-54,-2},{-34,13},{-24,25.5},{-36.5,
            25.5},{-36.5,25.5},{-36.5,30.5},{-36.5,30.5},{-1.5,30.5},{-1.5,30.5},
            {-1.5,25.5},{-1.5,25.5},{-11.5,25.5},{-34,5.5},{-34,5.5},{-29,-27},
            {-21.5,-30.75},{-14,-27},{-9,-28.25},{-9,-34.5},{-20.25,-43.25},{
            -35.25,-45.75},{-44,-35.75},{-49,-7},{-49,-7},{-56.5,-12},{-56.5,
            -12},{-64,-42},{-64,-42}},
        smooth = Smooth.Bezier),
      Polygon(
        fillColor = {128,128,128},
        pattern = LinePattern.None,
        fillPattern = FillPattern.Solid,
        points={{83.5,28},{58.5,28},{58.5,28},{51,31.75},{32.25,33},{12.25,23},
            {3.5,4.25},{7.25,-9.5},{18.5,-14.5},{18.5,-14.5},{2.25,-24.5},{2.25,
            -37},{12.25,-40.75},{12.25,-40.75},{17.25,-43.25},{17.25,-43.25},{
            41,-50.75},{43.5,-63.25},{28.5,-72},{8.5,-67},{3.5,-53.25},{17.25,
            -43.25},{17.25,-43.25},{12.25,-40.75},{12.25,-40.75},{2.25,-43.25},
            {-10.25,-52},{-7.75,-70.75},{26,-78.25},{61,-64.5},{59.75,-37},{
            23.5,-28.25},{18.5,-22},{23.5,-17},{23.5,-17},{26,-9.5},{26,-9.5},{
            23.5,-4.5},{24.75,9.25},{32.25,25.5},{43.5,28},{49.75,20.5},{47.25,
            6.75},{41,-8.25},{31,-13.25},{26,-9.5},{26,-9.5},{23.5,-17},{23.5,
            -17},{39.75,-18.25},{61,-8.25},{68.5,8},{66,18},{66,18},{76,18}},
        smooth = Smooth.Bezier)}), Documentation(revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>", info="<html>
<p>
This package contains definitions of types representing units used 
within the Modelica library SorpLib. These types are used for type 
checking (i.e., units) when implementing model equations. 
</p>

<h4>Adding new units</h4>
<p>
Before adding new units, please check if the unit is already 
implemented in the 
<a href=\"modelica://Modelica.Units\">Modelica standard library</a>.
</p>
</html>"));
end Units;
