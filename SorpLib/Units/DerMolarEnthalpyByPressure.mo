within SorpLib.Units;
type DerMolarEnthalpyByPressure =  Real (
  final quantity="DerMolarEnthalpyByPressure",
  final unit="J/(mol.Pa)",
  displayUnit="kJ/(mol.Pa)")
  "First-order partial derivative of the molar enthalpy w.r.t. the presssure";
