within SorpLib.Units;
type DerUptakeSpecificEntropyByTemperature = Real (
  final quantity="DerUptakeSpecificEntropyByTemperature",
  final unit="J.kg/(kg.kg.K2)",
  displayUnit="kJ.kg/(kg.kg.K2)")
  "First-order partial derivative of specific temperature times the uptake w.r.t. pressure";
