within SorpLib.Units;
type DerSpecificVolumeByPressure = Real (
  final quantity="DerSpecificVolumeByPressure",
  final unit="m3/(kg.Pa)",
  displayUnit="m3/(kg.Pa)")
  "First-order partial derivative of the specific volume w.r.t. the pressure";
