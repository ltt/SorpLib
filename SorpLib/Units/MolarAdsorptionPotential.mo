within SorpLib.Units;
type MolarAdsorptionPotential =Real (
  final quantity="MolarAdsorptionPotential",
  final unit="J/mol",
  displayUnit="kJ/mol",
  min=0) "Molar adsorption potential";
