within SorpLib.Units;
type DerDensityByTemperature = Real (
  final quantity="DerDensityByTemperature",
  final unit="kg/(m3.K)",
  displayUnit="kg/(m3.K)")
  "First-order partial derivative of the density w.r.t. the temperature";
