within SorpLib.Units;
type DerUptakeSpecificEnthalpyByUptake = Real (
  final quantity="DerUptakeSpecificEnthalpyByUptake",
  final unit="J.kg.kg/(kg.kg.kg)",
  displayUnit="kJ.kg.kg/(kg.kg.kg)")
  "First-order partial derivative of specific enthalpy time the uptake w.r.t. uptake";
