within SorpLib.Units;
type DerFilledPoreVolumeByAdsorptionPotential = Real (
  final quantity="DerFilledPoreVolumeByAdsorptionPotential",
  final unit="m3.mol/(kg.J)",
  displayUnit="l.mol/(kg.kJ)")
  "First-order partial derivative of the filled pore volume w.r.t. the adsorption 
    potential";
