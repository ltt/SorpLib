within SorpLib.Units;
type DerSpecificVolumeByPressureTemperature = Real (
  final quantity="DerSpecificVolumeByPressureTemperature",
  final unit="m3/(kg.Pa.K)",
  displayUnit="m3/(kg.Pa.K)")
  "Second-order partial derivative of the specific volume w.r.t. the pressure 
    and temperature";
