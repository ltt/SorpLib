within SorpLib.Units;
type DerUptakeSpecificVolumeByTemperature = Real (
  final quantity="DerUptakeSpecificVolumeByTemperature",
  final unit="m3.kg/(kg.kg.K)",
  displayUnit="m3.kg/(kg.kg.K)")
  "First-order partial derivative of the specific volume time the uptake w.r.t. the temperature";
