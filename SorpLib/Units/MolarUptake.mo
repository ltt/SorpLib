within SorpLib.Units;
type MolarUptake = Real (
  final quantity="MolarUptake",
  final unit="mol/kg",
  displayUnit="mol/kg",
  min=0,
  nominal=0.5) "Equilibrium molar uptake";
