within SorpLib.Units;
type DerMolarAdsorptionPotentialByPressureTemperature = Real (
  final quantity="DerMolarAdsorptionPotentialByPressureTemperature",
  final unit="J/(mol.Pa.K)",
  displayUnit="kJ/(mol.Pa.K)")
  "Second-order partial derivative of the molar adsorption potential w.r.t. 
  pressure and temperature";
