within SorpLib.Units;
type DerUptakeByPressure = Real (
  final quantity="DerUptakeByPressure",
  final unit="kg/(kg.Pa)",
  displayUnit="kg/(kg.Pa)")
  "First-order partial derivative of the uptake w.r.t. pressure";
