within SorpLib.Units;
type DerUptakeSpecificEntropyByUptake = Real (
  final quantity="DerUptakeSpecificEntropyByUptake",
  final unit="J.kg.kg/(kg2.kg.K)",
  displayUnit="kJ.kg.kg/(kg2.kg.K)")
  "First-order partial derivative of specific entropy times the uptake w.r.t. uptake";
