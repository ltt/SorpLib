within SorpLib.Units;
type DerUptakeSpecificEnthalpyByPressure = Real (
  final quantity="DerUptakeSpecificEnthalpyByPressure",
  final unit="J.kg/(kg.kg.Pa)",
  displayUnit="kJ.kg/(kg.kg.Pa)")
  "First-order partial derivative of specific enthalpy times the uptake w.r.t. pressure";
