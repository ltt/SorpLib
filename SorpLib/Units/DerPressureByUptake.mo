within SorpLib.Units;
type DerPressureByUptake = Real (
  final quantity="DerPressureByUptake",
  final unit="Pa.kg/(kg)")
  "First-order partial derivative of the pressure w.r.t. the uptake";
