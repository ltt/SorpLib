within SorpLib.Units;
type DerMolarAdsorptionPotentialByPressurePressure = Real (
  final quantity="DerMolarAdsorptionPotentialByPressurePressure",
  final unit="J/(mol.Pa2)",
  displayUnit="kJ/(mol.Pa2)")
  "Second-order partial derivative of the molar adsorption potential w.r.t. 
    pressure";
