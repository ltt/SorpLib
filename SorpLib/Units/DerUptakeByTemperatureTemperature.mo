within SorpLib.Units;
type DerUptakeByTemperatureTemperature = Real (
  final quantity="DerUptakeByTemperatureTemperature",
  final unit="kg/(kg.K2)",
  displayUnit="kg/(kg.K2)")
  "Second-order partial derivative of the uptake w.r.t. temperature";
