within SorpLib.Units;
type DerIsobaricExpansionCoefficientByTemperature = Real (
  final quantity="DerIsobaricExpansionCoefficientByTemperature",
  final unit="1/(K2)",
  displayUnit="1/(K2)")
  "First-order partial derivative of the isobaric expansion coefficient w.r.t. 
    the temperature";
