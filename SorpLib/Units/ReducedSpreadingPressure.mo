within SorpLib.Units;
type ReducedSpreadingPressure = Real (
  final quantity="ReducedSpreadingPressure",
  final unit="mol/kg",
  displayUnit="mol/kg")
  "Reduced spreading pressure";
