within SorpLib.Units;
type DerFilledPoreVolumeByAdsorptionPotentialAdsorptionPotential = Real (
  final quantity="DerFilledPoreVolumeByAdsorptionPotentialAdsorptionPotential",
  final unit="m3.mol2/(kg.J2)",
  displayUnit="l.mol2/(kg.kJ2)")
  "Second-order partial derivative of the filled pore volume w.r.t. the adsorption 
    potential";
