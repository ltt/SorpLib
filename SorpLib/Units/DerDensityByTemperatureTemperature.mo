within SorpLib.Units;
type DerDensityByTemperatureTemperature = Real (
  final quantity="DerDensityByTemperatureTemperature",
  final unit="kg/(m3.K2)",
  displayUnit="kg/(m3.K2)")
  "Second-order partial derivative of the density w.r.t. the temperature";
