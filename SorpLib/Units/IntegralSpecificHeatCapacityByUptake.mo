within SorpLib.Units;
type IntegralSpecificHeatCapacityByUptake = Real (
  final quantity="IntegralSpecificHeatCapacityByUptake",
  final unit="J/(kg.K)",
  displayUnit="kJ/(kg.K)")
  "Integral of the specific heat capacity w.r.t. the uptake";
