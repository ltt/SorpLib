within SorpLib.Units;
type DerFilledPoreVolumeByTemperature = Real (
  final quantity="DerFilledPoreVolumeByTemperature",
  final unit="m3/(kg.K)",
  displayUnit="l/(kg.K)")
  "First-order partial derivative of the filled pore volume w.r.t. the 
    temperature";
