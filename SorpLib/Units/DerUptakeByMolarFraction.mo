within SorpLib.Units;
type DerUptakeByMolarFraction = Real (
  final quantity="DerUptakeByMolarFraction",
  final unit="kg.mol/(kg.mol)",
  displayUnit="kg.mol/(kg.mol)")
  "First-order partial derivative of the uptake w.r.t. molar fraction";
