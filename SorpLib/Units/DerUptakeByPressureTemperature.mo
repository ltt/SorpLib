within SorpLib.Units;
type DerUptakeByPressureTemperature = Real (
  final quantity="DerUptakeByPressureTemperature",
  final unit="kg/(kg.Pa.K)",
  displayUnit="kg/(kg.Pa.K)")
  "Second-order partial derivative of the uptake w.r.t. pressure and 
    temperature";
