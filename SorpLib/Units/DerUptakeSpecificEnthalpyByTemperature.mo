within SorpLib.Units;
type DerUptakeSpecificEnthalpyByTemperature = Real (
  final quantity="DerUptakeSpecificEnthalpyByTemperature",
  final unit="J.kg/(kg.kg.K)",
  displayUnit="kJ.kg/(kg.kg.K)")
  "First-order partial derivative of specific enthalpy times the uptake w.r.t. temperature";
