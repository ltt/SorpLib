within SorpLib.Units;
type DerUptakeByPressurePressure = Real (
  final quantity="DerUptakeByPressurePressure",
  final unit="kg/(kg.Pa2)",
  displayUnit="kg/(kg.Pa2)")
  "Second-order partial derivative of the uptake w.r.t. pressure";
