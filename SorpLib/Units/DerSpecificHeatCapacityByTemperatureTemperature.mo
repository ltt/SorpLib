within SorpLib.Units;
type DerSpecificHeatCapacityByTemperatureTemperature = Real (
  final quantity="DerSpecificHeatCapacityByTemperature",
  final unit="J/(kg.K3)",
  displayUnit="kJ/(kg.K3)")
  "Second-order partial derivative of the specific heat capacity w.r.t. the 
    temperature";
