within SorpLib.Units;
type DerSpecificVolumeByTemperatureTemperature = Real (
  final quantity="DerSpecificVolumeByTemperatureTemperature",
  final unit="m3/(kg.K2)",
  displayUnit="m3/(kg.K2)")
  "Second-order partial derivative of the specific volume w.r.t. the temperature";
