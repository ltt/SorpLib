within SorpLib.Units;
type AdsorptionPotential =Real (
  final quantity="AdsorptionPotential",
  final unit="J/kg",
  displayUnit="kJ/kg",
  min=0) "Adsorption potential";
