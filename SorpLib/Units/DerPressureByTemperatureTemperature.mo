within SorpLib.Units;
type DerPressureByTemperatureTemperature = Real (
  final quantity="DerPressureByTemperatureTemperature",
  final unit="Pa/(K2)")
  "Second-order partial derivative of the pressure w.r.t. the temperature";
