within SorpLib.Units;
type DerUptakeSpecificEntropyByPressure = Real (
  final quantity="DerUptakeSpecificEntropyByPressure",
  final unit="J.kg/(kg.kg.K.Pa)",
  displayUnit="kJ.kg/(kg.kg.K.Pa)")
  "First-order partial derivative of specific entropy times the uptake w.r.t. pressure";
