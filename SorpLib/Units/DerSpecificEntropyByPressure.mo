within SorpLib.Units;
type DerSpecificEntropyByPressure = Real (
  final quantity="DerSpecificEntropyByPressure",
  final unit="J/(kg.K.Pa)",
  displayUnit="kJ/(kg.K.Pa)")
  "First-order partial derivative of specific entropy w.r.t. pressure";
