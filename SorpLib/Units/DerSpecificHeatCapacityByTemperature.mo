within SorpLib.Units;
type DerSpecificHeatCapacityByTemperature = Real (
  final quantity="DerSpecificHeatCapacityByTemperature",
  final unit="J/(kg.K2)",
  displayUnit="kJ/(kg.K2)")
  "First-order partial derivative of the specific heat capacity w.r.t. the 
    temperature";
