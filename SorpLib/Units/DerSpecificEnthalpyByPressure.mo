within SorpLib.Units;
type DerSpecificEnthalpyByPressure = Real (
  final quantity="DerSpecificEnthalpyByPressure",
  final unit="J/(kg.Pa)",
  displayUnit="kJ/(kg.Pa)")
  "First-order partial derivative of specific enthalpy w.r.t. pressure";
