within SorpLib.Units;
type DerSpecificEntropyByTemperature = Real (
  final quantity="DerSpecificEntropyByTemperature",
  final unit="J/(kg.K2)",
  displayUnit="kJ/(kg.K2)")
  "First-order partial derivative of specific temperature w.r.t. pressure";
