within SorpLib.Units;
type DerThermalConductivityByTemperatureTemperature = Real (
  final quantity="DerThermalConductivityByTemperature",
  final unit="W/(m.K3)",
  displayUnit="W/(m.K3)")
  "Second-order partial derivative of the thermal conductivity w.r.t. the 
    temperature";
