within SorpLib.Units;
type DerIsobaricExpansionCoefficientByPressure = Real (
  final quantity="DerIsobaricExpansionCoefficientByPressure",
  final unit="1/(K.Pa)",
  displayUnit="1/(K.Pa)")
  "First-order partial derivative of the isobaric expansion coefficient w.r.t. 
    the pressure";
