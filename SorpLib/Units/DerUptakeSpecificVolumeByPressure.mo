within SorpLib.Units;
type DerUptakeSpecificVolumeByPressure = Real (
  final quantity="DerUptakeSpecificVolumeByPressure",
  final unit="m3.kg/(kg.kg.Pa)",
  displayUnit="m3.kg/(kg.kg.Pa)")
  "First-order partial derivative of the specific volume time the uptake w.r.t. the pressure";
