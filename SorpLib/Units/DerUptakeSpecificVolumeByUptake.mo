within SorpLib.Units;
type DerUptakeSpecificVolumeByUptake = Real (
  final quantity="DerUptakeSpecificVolumeByUptake",
  final unit="m3.kg.kg/(kg.kg.kg)",
  displayUnit="m3.kg.kg/(kg.kg.kg)")
  "First-order partial derivative of the specific volume time the uptake w.r.t. the uptake";
