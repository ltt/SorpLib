within SorpLib.Numerics;
package Testers "Models to test and/or varify functions for numerical implementations"
  extends Modelica.Icons.ExamplesPackage;

  annotation (Documentation(info="<html>
<p>
This package contains executable test models for all functions of the 'Numerics' 
package. The test models check the implementation of the functions and enable 
verification of the function behavior. In addition, the test models demonstrate 
the functions' general applicability.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end Testers;
