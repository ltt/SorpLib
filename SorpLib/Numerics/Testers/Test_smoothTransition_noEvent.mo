within SorpLib.Numerics.Testers;
model Test_smoothTransition_noEvent
  "Tester for the function 'smoothTransition_noEvent' and its derivatives"
  extends Modelica.Icons.Example;

  //
  // Definition of parameters
  //
  parameter Real transitionLength = 1
    "Defines transition length around transition point"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Integer noDiff = 1
    "Specification how often function can be differentiated (i.e., 1, 2 or 3)"
    annotation (Dialog(tab="General", group="Parameters"));

  //
  // Definition of variables
  //
  Real x(start=0, fixed=true)
    "Abscissa value used for step";

  Real weigthingFactor = SorpLib.Numerics.smoothTransition_noEvent(x=x,
    transitionPoint=1, transitionLength=transitionLength, noDiff=noDiff)
    "Smooth transition from 1 to 0 around x = 1";
  Real dweigthingFactor_dtau
    "First derivative of weigthingFactor wrt. time";
  Real d2weigthingFactor_d2tau
    "Second derivative of weigthingFactor wrt. time";

equation
  der(x) = 0.1
    "Predescribed slope of x to demonstrate function 'smoothTransition'";
  der(weigthingFactor) = dweigthingFactor_dtau
    "First derivative of weigthingFactor wrt. time";
  der(dweigthingFactor_dtau) = d2weigthingFactor_d2tau
    "Second derivative of weigthingFactor wrt. time";

  //
  // Annotations
  //
  annotation (experiment(StartTime=0, StopTime=20, Tolerance=1e-6),
Documentation(revisions="<html>
<ul>
  <li>
  October 31, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>", info="<html>
<p>
This tester shows the behavior of the 'smoothTransition_noEvent' function. For example, 
the function smoothly changes the output from 1 to 0 at <i>transitionPoint = 1</i>
within <i>transitionLength = 1</i>. The change characteristic can be
influenced using the value <i>noDiff</i>.
<br/><br/>
To see the transition behavior, plot the variable <i>weigthingFactor</i> over the 
variable <i>x</i>. The simulation time is correctly preset (Start: 0 s, Stop = 20 s).  
</p>
</html>"));
end Test_smoothTransition_noEvent;
