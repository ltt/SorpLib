within SorpLib.Numerics.Testers;
model Test_regSquare_regSquare_inv
  "Tester for the functions 'regSquare' and 'regSquare_inv'"
  extends Modelica.Icons.Example;

  //
  // Definition of parameters
  //
  parameter Real delta_x = 0.1
    "Defines region (-delta_x < x < delta_x) used to approximate square by a 3rd
    order polynomial"
    annotation (Dialog(tab="General", group="Parameters"));

  //
  // Definition of variables
  //
  Real x(start=-1.5, fixed=true)
    "Abscissa value used for square aprroximation";

  Real app_square = SorpLib.Numerics.regSquare(x=x, delta_x=delta_x)
    "Approximated square";
  Real dapp_square_dtau
    "First derivative of app_square wrt. time";
  Real d2app_square_d2tau
    "Second derivative of app_square wrt. time";

  Real x_inv = SorpLib.Numerics.regSquare_inv(y=app_square, delta_x=delta_x)
    "Calculate inverse of function 'regSquare'";

equation
  der(x) = 0.1
    "Predescribed slope of x to demonstrate function 'regSquare'";
  der(app_square) = dapp_square_dtau
    "First derivative of app_square wrt. time";
  der(dapp_square_dtau) = d2app_square_d2tau
    "Second derivative of app_square wrt. time";

  //
  // Annotations
  //
  annotation (experiment(StartTime=0, StopTime=30, Tolerance=1e-6),
Documentation(revisions="<html>
<ul>
  <li>
  October 31, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>", info="<html>
<p>
This tester shows the behavior of the functions 'regSquare' and 'regSquare_inv.'
The approximation range can be influenced using the value <i>delta_x</i>: Larger 
values cause a greater transition region than smaller values.
<br/><br/>
To see the behavior of the approximated square, plot the variable <i>app_square</i>
over the  variable <i>x</i>. To see the behavior of the inverse function, plot the 
variables <i>x</i> and <i>x_inv</i> over the  variable <i>x</i>. The simulation 
time is correctly preset (Start: 0 s, Stop = 30 s).  
</p>
</html>"));
end Test_regSquare_regSquare_inv;
