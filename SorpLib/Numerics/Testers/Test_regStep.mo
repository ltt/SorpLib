within SorpLib.Numerics.Testers;
model Test_regStep "Tester for the function 'regStep'"
  extends Modelica.Icons.Example;

  //
  // Definition of parameters
  //
  parameter Real x_small = 0
    "Defines region (-x_small < x < x_small) used to approximate step by a 2nd
    order polynomial"
    annotation (Dialog(tab="General", group="Parameters"));

  //
  // Definition of variables
  //
  Real x(start=-1, fixed=true)
    "Abscissa value used for step";

  Real reg_value = SorpLib.Numerics.regStep(x=x, y1=-1, y2=1, x_small=x_small)
    "Regulated step";

equation
  der(x) = 0.1
    "Predecsriped slope of x to demonstrate function 'regStep'";

  //
  // Annotations
  //
  annotation (experiment(StartTime=0, StopTime=20, Tolerance=1e-6),
Documentation(revisions="<html>
<ul>
  <li>
  October 31, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>", info="<html>
<p>
This tester shows the behavior of the 'regStep' function. For example, the 
function steps from <i>y1 = -1</i> (<i>x &gt; 0</i>) to <i>y2 = 1</i> (<i>x 
&lt; 0</i>) at <i>x = 0</i>. The step characteristic at <i>x = 0</i> can be
influenced using the transition value <i>x_small</i>: Larger values cause a 
smoother transition than smaller values.
<br/><br/>
To see the jump behavior, plot the variable <i>reg_value</i> over the 
variable <i>x</i>. The simulation time is correctly preset (Start: 0 s, 
Stop = 20 s).  
</p>
</html>"));
end Test_regStep;
