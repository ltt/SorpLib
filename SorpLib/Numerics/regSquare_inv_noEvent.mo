within SorpLib.Numerics;
function regSquare_inv_noEvent
  "Inverse function of the anti-symmetric square approximation with non-zero derivative in the origin (without events)"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Real y
    "Approximated value"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real delta_x = 1e-4
    "Regulation value for approximation"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of outputs
  //
  output Real x
    "Input value"
    annotation (Dialog(tab="General", group="Outputs", enable=false));

algorithm
  if noEvent(y > delta_x*delta_x) then
    x := sqrt(y)
      "No approximation";

  elseif noEvent(y < -delta_x*delta_x) then
    x := -sqrt(-y)
      "Keep sign of input";

  else
    x := delta_x^2 /
      (-27*delta_x*y + sqrt(108*delta_x^6 + 2916*delta_x^2*y^2)/2)^(1/3) -
      (-27*delta_x*y + sqrt(108*delta_x^6 + 2916*delta_x^2*y^2)/2)^(1/3) / 3
      "Transition region: Solving cubic equations and discarding complex 
      solutions";
  end if;

  //
  // Annotations
  //
  annotation (Inline=false,
InlineAfterIndexReduction=false,
LateInline=true,
smoothOrder=1,
inverse(y=regSquare_noEvent(x,delta_x)),
Documentation(info="<html>
<p>
This function is the inverse function of the function 'regSquare_noEvent.' For
full details, check the documentation of the function
<a href=\"Modelica://SorpLib.Numerics.regSquare_noEvent\">SorpLib.Numerics.regSquare_noEvent</a>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 31, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end regSquare_inv_noEvent;
