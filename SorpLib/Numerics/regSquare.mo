within SorpLib.Numerics;
function regSquare
  "Anti-symmetric square approximation with non-zero derivative in the origin"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Real x
    "Input value"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real delta_x = 1e-4
    "Regulation value for approximation"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of outputs
  //
  output Real y
    "Approximated value"
    annotation (Dialog(tab="General", group="Outputs", enable=false));

algorithm
  if x > delta_x then
    y := x*x
      "No approximation";

  elseif x < -delta_x then
    y := -x*x
      "Keep sign of input";

  else
    y := (0.5/delta_x * x^2 + 0.5*delta_x) * x
      "Transition region";

  end if;

  //
  // Annotations
  //
  annotation (Inline=false,
InlineAfterIndexReduction=false,
LateInline=true,
smoothOrder=1,
inverse(x=regSquare_inv(y,delta_x)),
Documentation(info="<html>
<p><p>
This function is used to approximate the equation
</p>
<pre>
    y = x<sup>2</sup>;
</pre>

<p>
for (1) the slope to be non-zero at the origin, (2) the function to be continuous 
and differentiable, and (3) the sign of the input to not change:
</p>

<pre>
   y = <strong>if</strong> x &gt; delta_x <strong>then</strong> x<sup>2</sup> <strong>else</strong>
       <strong>if</strong> x &lt; -delta_x <strong>then</strong> -x<sup>2</sup> <strong>else</strong>
       0.5 * x * (x<sup>2</sup> / delta_x + delta_x);
</pre>

<p>
In the region -delta_x &lt; x &lt; delta_x a 3rd order polynomial is used.
</p>

<h4>Typical use</h4>
<p>
This function is used, for example, to calculate pressure drop correlations.
</p>

<h4>Example</h4>
<p>
The following figure shows the approximated square for different values of <i>delta_x</i>.
<br/><br/>
</p>
<img src=\"Modelica://SorpLib/Resources/doc/numerics_regSquare.png\" alt=\"numerics_regSquare.png\">

<h4>References</h4>
<p>
The function is based on the MSL library
(<a href=\"Modelica://Modelica.Fluid.Utilities.regSquare\">Modelica.Fluid.Utilities.regSquare</a>)
and TIL library
(<a href=\"Modelica://TIL.Utilities.Numerics.squareFunction\">TIL.Utilities.Numerics.squareFunction</a>)
and has been slightly adapted.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 31, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end regSquare;
