within SorpLib.Numerics;
function regSquareWFactors_noEvent
  "Anti-symmetric square (with discontinuous factors) approximation with non-zero derivative in the origin (without events)"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Real x
    "Input value"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real delta_x = 1e-4
    "Regulation value for approximation"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real f_positive(min=0) = 1
    "Factor for x > 0 (must be >= 0)"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real f_negative(min=0) = 1
    "Factor for x < 0 (must be >= 0)"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of outputs
  //
  output Real y
    "Approximated value"
    annotation (Dialog(tab="General", group="Outputs", enable=false));

  //
  // Definition of variables
  //
protected
  Real f
    "Factor";

algorithm
  //
  // Calculate factor
  //
  if noEvent(x > 0) then
    f := f_positive
    "Factor";

  elseif noEvent(x < 0) then
    f := f_negative
    "Factor";

  else
    f := (f_positive + f_negative) / 2
    "Factor";
  end if;

  //
  // Approximate value
  //
  if noEvent(Modelica.Math.isEqual(s1=f, s2=0, eps=Modelica.Constants.small)) then
    y := Modelica.Constants.inf
      "No finite solution";

  elseif noEvent(x > delta_x) then
    y := x*x / f
      "No approximation";

  elseif noEvent(x < -delta_x) then
    y := -x*x / f
      "Keep sign of input";

  else
    y := (0.5/delta_x * x^2 + 0.5*delta_x) * x / f
      "Transition region";

  end if;

  //
  // Annotations
  //
  annotation (Inline=false,
InlineAfterIndexReduction=false,
LateInline=true,
smoothOrder=1,
inverse(x=regSquareWFactors_inv_noEvent(y,delta_x,f_positive,f_negative)),
Documentation(info="<html>
<p><p>
This function is used to approximate the equation
</p>
<pre>
   y = x<sup>2</sup> / f;
</pre>
<p>
with the factor <i>f</i> beeing dependent on the flow direction:
</p>
<pre>
   f = <strong>if</strong> x &gt; 0 <strong>then</strong> f_positive <strong>elseif</strong> x &lt; 0 <strong>then</strong> f_negative <strong>else</strong> (f_positive + f_negative) / 2;
</pre>
<p>
for (1) the slope to be non-zero at the origin, (2) the function to be continuous 
and differentiable, and (3) the sign of the input to not change:
</p>

<pre>
   y = <strong>if</strong> x &gt; delta_x <strong>then</strong> x<sup>2</sup> / f <strong>else</strong>
       <strong>if</strong> x &lt; -delta_x <strong>then</strong> -x<sup>2</sup> / f <strong>else</strong>
       0.5 * x * (x<sup>2</sup> / delta_x + delta_x) / f;
</pre>

<p>
In the region -delta_x &lt; x &lt; delta_x, a 3rd order polynomial is used.
</p>

<h4>Typical use</h4>
<p>
This function is used, for example, to calculate mass flow rates depending on 
prssure drops.
</p>

<h4>Example</h4>
<p>
The following figure shows the approximated square for different values of 
<i>delta_x</i>, <i>f_positive=3</i>, and <i>f_negative=2</i>.
<br/><br/>
</p>
<img src=\"Modelica://SorpLib/Resources/doc/numerics_regSquareWFactors.png\" alt=\"numerics_regSquareWFactors.png\">

<h4>References</h4>
<p>
The function is based on the MSL library
(<a href=\"Modelica://Modelica.Fluid.Utilities.regSquare2\">Modelica.Fluid.Utilities.regSquare2</a>)
and TIL library
(<a href=\"Modelica://TIL.Utilities.Numerics.inverseSquareRootFunctionWithFactorRevised\">TIL.Utilities.Numerics.inverseSquareRootFunctionWithFactorRevised</a>)
and has been slightly adapted.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 19, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end regSquareWFactors_noEvent;
