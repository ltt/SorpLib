within SorpLib.Numerics;
function regStep_noEvent
  "Approximation of a general step to get a continuous and differentiable characteristic (without events)"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Real x
    "Abscissa value"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real y1
    "Ordinate value for x > 0"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real y2
    "Ordinate value for x < 0"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real x_small(min=0) = 1e-5
    "Approximation of step for -x_small <= x <= x_small; x_small >= 0 required"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of outputs
  //
  output Real y
    "Ordinate value to approximate y = if x > 0 then y1 else y2"
    annotation (Dialog(tab="General", group="Outputs", enable=false));

algorithm
  y := smooth(1,
              noEvent(if x >  x_small then y1 else
                      if x < -x_small then y2 else
                      if x_small > 0 then
                        (x/x_small)*((x/x_small)^2 - 3)*(y2-y1)/4 + (y1+y2)/2 else
                        (y1+y2)/2))
    "Smooth transition";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function is used to approximate the equation
</p>
<pre>
    y = <strong>if</strong> x &gt; 0 <strong>then</strong> y1 <strong>else</strong> y2;
</pre>

<p>
by a smooth characteristic to get a continuous and differentiable expression
and events are avoided:
</p>

<pre>
   y = <strong>smooth</strong>(1, <strong>if</strong> x &gt;  x_small <strong>then</strong> y1 <strong>else</strong>
                 <strong>if</strong> x &lt; -x_small <strong>then</strong> y2 <strong>else</strong> f(y1, y2));
</pre>

<p>
In the region -x_small &lt; x &lt; x_small a 2nd order polynomial is used
for a smooth transition from y1 to y2.
</p>

<h4>Example</h4>
<p>
The following figure shows the smooth characteristic for different values of
<i>x_small</i>, <i>y1 = -1</i>, and <i>y2 = 1</i>.
<br/><br/>
</p>
<img src=\"Modelica://SorpLib/Resources/doc/numerics_regStep.png\" alt=\"numerics_regStep.png\">

<h4>References</h4>
<p>
The function and documentation are taken from the Modelica standard library
(<a href=\"Modelica://Modelica.Fluid.Utilities.regStep\">Modelica.Fluid.Utilities.regStep</a>)
and have been slightly adapted.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 31, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end regStep_noEvent;
