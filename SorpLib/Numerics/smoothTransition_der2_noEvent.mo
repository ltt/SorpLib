within SorpLib.Numerics;
function smoothTransition_der2_noEvent
  "Second derivative of function 'smoothTransition_noEvent' (without events)"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Real x
    "Actual value"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real transitionPoint=1
    "Transition point"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real transitionLength=1
    "Transition length"
    annotation (Dialog(tab="General", group="Inputs"));
  input Integer noDiff = 1
    "Specification how often function can be differentiated (i.e., 1, 2 or 3)"
    annotation (Dialog(tab="General", group="Numerics"));

  input Real x_der
    "First derivative of actual value wrt. time"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real x_der2
    "Second derivative of actual value wrt. time"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of outputs
  //
  output Real weigthingFactor
    "Second derivative of weighting factor"
    annotation (Dialog(tab="General", group="Outputs", enable=false));

protected
  Real phi
    "Phase";
  Real dphi_dtau
    "First derivative of phase wrt. time";
  Real d2phi_d2tau
    "Second derivative of phase wrt. time";

algorithm
  if noEvent(x < transitionPoint-0.5*transitionLength) then
    weigthingFactor := 0
    "No transition region";

  elseif noEvent(x < transitionPoint+0.5*transitionLength) then
    phi := (x - transitionPoint)*Modelica.Constants.pi/transitionLength
      "Phase";
    dphi_dtau := x_der*Modelica.Constants.pi/transitionLength
      "First derivative of weighting factor";
    d2phi_d2tau := x_der2*Modelica.Constants.pi/transitionLength
      "First derivative of weighting factor";

    if noEvent(noDiff == 1) then
      weigthingFactor := -1.0/2.0 *
        (-sin(phi) * dphi_dtau^2 + cos(phi) * d2phi_d2tau)
        "Transition region";

    elseif noEvent(noDiff == 2) then
      weigthingFactor := -d2phi_d2tau / Modelica.Constants.pi *
        (cos(phi)^2 - sin(phi)^2 + 1) +
        4 * dphi_dtau^2 / Modelica.Constants.pi * cos(phi)*sin(phi)
        "Transition region";

    else
      weigthingFactor := d2phi_d2tau / 3.0 / Modelica.Constants.pi *
        (-2.0*cos(phi)^4 + 6.0*sin(phi)^2*cos(phi)^2 - 3.0 - 3.0*cos(2.0*phi)) +
        dphi_dtau^2 / 3.0 / Modelica.Constants.pi *
        (8.0*cos(phi)^3*sin(phi) +  12.0*sin(phi)*cos(phi)^3 -
        12.0*sin(phi)^2*cos(phi)*sin(phi) + 6.0*sin(2.0*phi))
        "Transition region";

    end if;

  else
    weigthingFactor := 0
    "No transition region";

  end if;

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function is the first derivative of the function 'smoothTransition_noEvent'
with respect to time. For full details, check the documentation of the function
<a href=\"Modelica://SorpLib.Numerics.smoothTransition_noEvent\">SorpLib.Numerics.smoothTransition_noEvent</a>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 31, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end smoothTransition_der2_noEvent;
