within SorpLib.Numerics;
function regSquareWFactors_inv_noEvent
  "Inverse function of the anti-symmetric square (with discontinuous factors) approximation with non-zero derivative in the origin (without events)"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Real y
    "Approximated value"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real delta_x = 1e-4
    "Regulation value for approximation"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real f_positive(min=0) = 1
    "Factor for x > 0 (must be >= 0)"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real f_negative(min=0) = 1
    "Factor for x < 0 (must be >= 0)"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of outputs
  //
  output Real x
    "Original input value"
    annotation (Dialog(tab="General", group="Outputs", enable=false));

  //
  // Definition of variables
  //
protected
  Real f
    "Factor";

algorithm
  //
  // Calculate factor
  //
  if noEvent(y > 0) then
    f := f_positive
    "Factor";

  elseif noEvent(y < 0) then
    f := f_negative
    "Factor";

  else
    f := (f_positive + f_negative) / 2
    "Factor";
  end if;

  //
  // Approximate value
  //
  if noEvent(Modelica.Math.isEqual(s1=f, s2=0, eps=Modelica.Constants.small)) then
    x := 0
      "No finite solution";

  elseif noEvent(y > delta_x * delta_x / f) then
    x := sqrt(y * f)
      "No approximation";

  elseif noEvent(y < -delta_x * delta_x / f) then
    x := -sqrt(-y * f)
      "Keep sign of input";

  else
    x := delta_x^2 /
      (-27*delta_x*y*f + sqrt(108*delta_x^6 + 2916*delta_x^2*y^2*f^2)/2)^(1/3) -
      (-27*delta_x*y*f + sqrt(108*delta_x^6 + 2916*delta_x^2*y^2*f^2)/2)^(1/3) / 3
      "Transition region: Solving cubic equations and discarding complex 
      solutions";

  end if;

  //
  // Annotations
  //
  annotation (Inline=false,
InlineAfterIndexReduction=false,
LateInline=true,
smoothOrder=1,
inverse(y=regSquareWFactors_noEvent(x,delta_x,f_positive,f_negative)),
Documentation(info="<html>
<p>
This function is the inverse function of the function 'regSquareWFactors.' For full 
details, check the documentation of the function
<a href=\"Modelica://SorpLib.Numerics.regSquareWFactors_noEvent\">SorpLib.Numerics.regSquareWFactors_noEvent</a>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  December 19, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end regSquareWFactors_inv_noEvent;
