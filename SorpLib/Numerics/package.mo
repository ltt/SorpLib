within SorpLib;
package Numerics "Library containing utility function regarding numeric implementations"
  extends Modelica.Icons.FunctionsPackage;

annotation (Documentation(info="<html>
<p>
This package contains utility functions to implement models in a mathematically
&quot;robust&quot; way. Mathematical robustness refers to the following:
</p>
<ol>
  <li>
  Mathematical approximation of functions so that they are continuous and 
  differentiable at specific points. 
  </li>
  <li>
  Mathematical transition between function values so that the transition is 
  continuous and differentiable. 
  </li>
  <li>
  Avoidance of events.
  </li>
</ol>
<p>
A mathematically robust implementation of models is essential in the context of 
mathematical optimization.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end Numerics;
