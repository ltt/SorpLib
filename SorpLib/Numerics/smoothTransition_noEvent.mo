within SorpLib.Numerics;
function smoothTransition_noEvent
  "Smoothly changes the output from 1 to 0 around the transition point to get a continuous and differentiable transition (without events)"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Real x
    "Actual value"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real transitionPoint=1
    "Transition point"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real transitionLength=1
    "Transition length"
    annotation (Dialog(tab="General", group="Inputs"));
  input Integer noDiff = 1
    "Specification how often function can be differentiated (i.e., 1, 2 or 3)"
    annotation (Dialog(tab="General", group="Numerics"));

  //
  // Definition of outputs
  //
  output Real weigthingFactor
    "Weighting factor"
    annotation (Dialog(tab="General", group="Outputs", enable=false));

protected
  Real phi
    "Phase";

algorithm
  if noEvent(x < transitionPoint - 0.5*transitionLength) then
    weigthingFactor := 1
    "No transition region";

  elseif noEvent(x < transitionPoint + 0.5*transitionLength) then
    phi := (x - transitionPoint)*Modelica.Constants.pi/transitionLength
      "Phase";

    if noEvent(noDiff == 1) then
      weigthingFactor := -1.0/2.0 * sin(phi) + 1.0/2.0
        "Transition region";

    elseif noEvent(noDiff == 2) then
      weigthingFactor := -1.0/2.0 * (2*cos(phi)*sin(phi) +
        2*phi - Modelica.Constants.pi) / Modelica.Constants.pi
        "Transition region";

    else
      weigthingFactor := 1.0/6.0 * (-4.0*sin(phi)*cos(phi)^3 -
        6.0*phi - 3.0*sin(2.0*phi) + 3.0*Modelica.Constants.pi) /
        Modelica.Constants.pi
        "Transition region";

    end if;

  else
    weigthingFactor := 0
    "No transition region";

  end if;

  //
  // Annotations
  //
  annotation (Inline=false,
InlineAfterIndexReduction=true,
derivative(order=1,
           noDerivative=transitionPoint,
           noDerivative=transitionLength,
           noDerivative=noDiff) = smoothTransition_der_noEvent,
Documentation(info="<html>
<p>
This function is used to smoothly change a weighting factor <i>WF</i> from 1 
to 0 at the specified point <i>transitionPoint</i> within the specified length 
<i>transitionLength</i>. The change is continuous and differentiable, and the 
type of change is specified via the parameter <i>noDiff</i>. The function is
implemented to avoid events.
<br/><br/>
The change is described using trigonometric functions that depend on the current 
phase <i>&phi;</i>:
</p>

<pre>
    &phi; = (x - transitionPoint) * &pi; / transitionLength;
</pre>

<p>
For <i>noDiff = 1</i>, the change is continuously differentiable once:
</p>

<pre>
    WF = -0.5 * <strong>sin</strong>(&phi;) + 0.5;
</pre>

<p>
For <i>noDiff = 2</i>, the change can be continuously differentiated twice:
</p>

<pre>
    WF = -0.5 * (2 * <strong>cos</strong>(&phi;) * <strong>sin</strong>(&phi;) + 2 * &phi; - &pi;) / &pi;;
</pre>

<p>
For <i>noDiff = 3</i>, the change can be continuously differentiated three times:
</p>

<pre>
    WF = 1/6 * (-4 * <strong>sin</strong>(&phi;) * <strong>cos</strong><sup>3</sup>(&phi;) - 6 * &phi; - 3 * <strong>sin</strong>(2 * &phi;) + 3 * &pi;) / &pi;;
</pre>

<h4>Typical use</h4>
<p>
This function is used, for example, to switch smoothly between different flow 
regimes when calculating heat or mass transport. For this purpose, the function 
can be used as follows:
</p>

<pre>
    y = WF * y(Regime 1) + (1 - WF) * y(Regime 2);
</pre>

<h4>Example</h4>
<p>
The following figure shows the smooth transition of the weighting factor
<i>WF</i> and its derivatives for different values of <i>noDiff</i> at 
<i>transitionPoint = 1</i> for <i>transitionLength = 1</i>.
<br/><br/>
</p>
<img src=\"Modelica://SorpLib/Resources/doc/numerics_smoothTransition.png\" alt=\"numerics_smoothTransition.png\">

<h4>References</h4>
<p>
The function is based on the TIL library
(<a href=\"Modelica://TIL.Utilities.Numerics.smoothTransition\">TIL.Utilities.Numerics.smoothTransition</a>)
and has been slightly adapted.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 31, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end smoothTransition_noEvent;
