within SorpLib.Media.WorkingPairs.FluidPropertyDiagrams.PureComponents.Tester;
model Test_ChartWorkingPairDubinin
  "Tester for the fluid property diagram model using the model of Dubinin and the MSL for fluid property data calculation of a real fluid with a two-phase regime"
  extends Modelica.Icons.Example;

  //
  // Definition of parameters describing packages and models
  //
  replaceable model PureWorkingPairModel =
      SorpLib.Media.WorkingPairs.PureComponents.H2O.Silicagel123_DubininLorentzianCumulative_Schawe2000_VLE
    constrainedby SorpLib.Media.WorkingPairs.PureComponents.WorkingPairVLE
    "Pure working pair model"
    annotation (Dialog(tab="General",group="Models and Media"),
                choicesAllMatching = true,
                Evaluate=true,
                HideResult=true);

  replaceable package WorkingPair =
      Parametrizations.PureComponents.H2O.SilicaGel123_DubininLorentzianCumulative_Schawe2000
    constrainedby
    SorpLib.Media.WorkingPairs.Interfaces.PartialPureParametrizationDubinin
    "Parametrized working pair model"
    annotation (Dialog(tab="General",group="Models and Media"),
                choicesAllMatching = true,
                Evaluate=true,
                HideResult=true);

  replaceable package Medium = Modelica.Media.Water.StandardWater
    constrainedby Modelica.Media.Interfaces.PartialTwoPhaseMedium
    "Medium (i.e., adsorptive)"
    annotation (Dialog(tab="General",group="Models and Media"),
                choicesAllMatching = true);

  //
  // Instantiation of models
  //
  SorpLib.Media.WorkingPairs.FluidPropertyDiagrams.PureComponents.ChartWorkingPairDubinin
    diagramIsotherms(
    redeclare final package Medium = Medium,
    redeclare final model PureWorkingPairModel = PureWorkingPairModel,
    redeclare final package WorkingPair = WorkingPair,
    final diagramType=SorpLib.Choices.DiagramTypePureComponentWorkingPair.Isotherms,
    isoLineType=SorpLib.Choices.ChangingConstantVariableOfDiagram.Linespace,
    p_adsorpt_min=615,
    p_adsorpt_max=5e5,
    T_adsorpt_min=273.2,
    T_adsorpt_max=423.15) "Diagram of isotherms"
    annotation (Placement(transformation(extent={{-60,-12},{-40,8}})));

  SorpLib.Media.WorkingPairs.FluidPropertyDiagrams.PureComponents.ChartWorkingPairDubinin
    diagramIsosters(
    redeclare final package Medium = Medium,
    redeclare final model PureWorkingPairModel = PureWorkingPairModel,
    redeclare final package WorkingPair = WorkingPair,
    final diagramType=SorpLib.Choices.DiagramTypePureComponentWorkingPair.Isosters,
    isoLineType=SorpLib.Choices.ChangingConstantVariableOfDiagram.Linespace,
    T_adsorpt_min=273.2,
    T_adsorpt_max=423.15,
    x_adsorpt_min=0.05,
    x_adsorpt_max=0.45) "Diagram of isosters"
    annotation (Placement(transformation(extent={{-10,-12},{10,8}})));

  SorpLib.Media.WorkingPairs.FluidPropertyDiagrams.PureComponents.ChartWorkingPairDubinin
    diagramIsobars(
    redeclare final package Medium = Medium,
    redeclare final model PureWorkingPairModel = PureWorkingPairModel,
    redeclare final package WorkingPair = WorkingPair,
    final diagramType=SorpLib.Choices.DiagramTypePureComponentWorkingPair.Isobars,
    isoLineType=SorpLib.Choices.ChangingConstantVariableOfDiagram.Linespace,
    p_adsorpt_min=615,
    p_adsorpt_max=5e5,
    T_adsorpt_min=273.2,
    T_adsorpt_max=423.15) "Diagram of isobars"
    annotation (Placement(transformation(extent={{40,-12},{60,8}})));

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This models demonstrates how to create a diagram of isotherms, isosters,
or isobars for a working pair model using a real fluid with a two-regime
and the model of Dubinin.
<br/><br/>
To see the model behavior, plot the variables of the models over the 
time. The simulation time is correctly preset (Start: 0 s, Stop = 20 s). 
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 23, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"), experiment(StopTime=20, Tolerance=1e-06));
end Test_ChartWorkingPairDubinin;
