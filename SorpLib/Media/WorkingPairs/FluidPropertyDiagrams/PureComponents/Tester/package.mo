within SorpLib.Media.WorkingPairs.FluidPropertyDiagrams.PureComponents;
package Tester "Package containing testers to test and varify models of fluid property diagrams for pure component working pairs"
  extends Modelica.Icons.ExamplesPackage;

annotation (Documentation(info="<html>
<p>
This package contains executable test models for all implemented models of fluid property
diagrams. 
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 23, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Tester;
