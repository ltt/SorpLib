within SorpLib.Media.WorkingPairs.FluidPropertyDiagrams;
package PureComponents "Package containing diagram models of pure component working pairs"
extends Modelica.Icons.VariantsPackage;

annotation (Documentation(info="<html>
<p>
This package contains models to generate fluid property diagrams for
pure component working pairs. The implemented diagram types include 
isotherms, isosterics, and isobars.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 23, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end PureComponents;
