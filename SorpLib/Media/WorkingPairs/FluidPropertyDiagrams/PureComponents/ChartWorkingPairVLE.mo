within SorpLib.Media.WorkingPairs.FluidPropertyDiagrams.PureComponents;
model ChartWorkingPairVLE
  "Model that creates a fluid property diagram of a pure component working pair using the MSL for fluid property data calculation of a real fluid with a two-phase regime"
  extends SorpLib.Media.WorkingPairs.BaseClasses.PartialPureCharts(
    redeclare replaceable model PureWorkingPairModel =
      SorpLib.Media.WorkingPairs.PureComponents.H2O.Silicagel123_DubininLorentzianCumulative_Schawe2000_VLE
      constrainedby SorpLib.Media.WorkingPairs.PureComponents.WorkingPairVLE(
        redeclare package Medium=Medium,
        limitLowerPressure=true,
        p_adsorpt_min=p_adsorpt_lowerLimit));

  //
  // Definition of replaceable medium
  //
  replaceable package Medium = Modelica.Media.Water.StandardWater
      constrainedby Modelica.Media.Interfaces.PartialTwoPhaseMedium
    "Medium (i.e., adsorptive)"
    annotation (Dialog(tab="General",group="Models and Media"),
                choicesAllMatching = true);

  //
  // Definition of parameters
  //
  parameter Modelica.Units.SI.Pressure p_adsorpt_lowerLimit = 612
    "Lower limit for the pressure to calculate fluod property data"
    annotation (Dialog(tab = "Advanced", group = "Limiter"),
                Evaluate=true,
                HideResult=true);
  parameter Modelica.Units.SI.Temperature T_adsorpt_lowerLimit = 273.17
    "Lower limit for the temperature to calculate fluod property data"
    annotation (Dialog(tab = "Advanced", group = "Limiter"),
                Evaluate=true,
                HideResult=true);

  //
  // Definition of variables
  //
  Real x_axis_isotherm
    "X-axis for isotherm diagram: Pressure p_adsorpt";
  Real x_axis_isosters
    "X-axis for isosters diagram: Negative reciproce temperature -1/T_adsorpt";
  Real x_axis_isobars
    "X-axis for isobars diagram: Temperature T_adsorpt";

  //
  // Definition of final parameters
  //
protected
  Modelica.Units.SI.Pressure
    p_adsorpt_(start=max(p_adsorpt_min, p_adsorpt_lowerLimit), fixed=true)
    "Changing pressure";
  Modelica.Units.SI.Temperature
    T_adsorpt_(start=max(T_adsorpt_min, T_adsorpt_lowerLimit), fixed=true)
    "Changing temperature";

equation
  //
  // Calculate correct inputs
  //
  der(p_adsorpt_) = (p_adsorpt_max-max(p_adsorpt_min, p_adsorpt_lowerLimit)) / 20
    "Predescriped slope";
  der(T_adsorpt_) = (T_adsorpt_max-max(T_adsorpt_min, T_adsorpt_lowerLimit)) / 20
    "Changing temperature";

  if diagramType==SorpLib.Choices.DiagramTypePureComponentWorkingPair.Isotherms then
    p_adsorpt = fill(p_adsorpt_, no_isoLines)
      "Pressure";
    T_adsorpt = {max(T_adsorpt_const_[i], T_adsorpt_lowerLimit) for i in 1:no_isoLines}
      "Temperature";

  elseif diagramType==SorpLib.Choices.DiagramTypePureComponentWorkingPair.Isosters then
    x_adsorpt = x_adsorpt_const_
      "Uptake";
    T_adsorpt = fill(T_adsorpt_, no_isoLines)
      "Temperature";

  else
    p_adsorpt = {max(p_adsorpt_const_[i], p_adsorpt_lowerLimit) for i in 1:no_isoLines}
      "Pressure";
    T_adsorpt = fill(T_adsorpt_, no_isoLines)
      "Temperature";

  end if;

  //
  // Caclulate variables for plotting
  //
  x_axis_isotherm = p_adsorpt_
    "X-axis for isotherm diagram: Pressure p_adsorpt";
  x_axis_isosters = -1/T_adsorpt_
    "X-axis for isosters diagram: Negative reciproce temperature -1/T_adsorpt";
  x_axis_isobars = T_adsorpt_
    "X-axis for isobars diagram: Temperature T_adsorpt";

  //
  // Asserations
  //
  if diagramType==SorpLib.Choices.DiagramTypePureComponentWorkingPair.Isotherms then
    assert(min(T_adsorpt_const_) > T_adsorpt_lowerLimit,
      "Minimum isotherm temperature (" + String(min(T_adsorpt_const_)) +
      ") is below the minimum temperature (" + String(T_adsorpt_lowerLimit) +
      "). Thus, fluid property data calculation is impossible.",
      level = AssertionLevel.error);

  elseif diagramType==SorpLib.Choices.DiagramTypePureComponentWorkingPair.Isobars then
    assert(min(p_adsorpt_const_) > p_adsorpt_lowerLimit,
      "Minimum isotherm pressure (" + String(min(p_adsorpt_const_)) +
      ") is below the minimum pressure (" + String(p_adsorpt_lowerLimit) +
      "). Thus, fluid property data calculation is impossible.",
      level = AssertionLevel.error);

  end if;

  //
  // Annotations
  //
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    Documentation(info="<html>
<p>
This partial model is the basic model for all models creating fluid propertey diagrams
for working pair models describing adsorption of pure components and using a medium 
model of a real fluid with a two-phase regime based on the Modelica Standard Library.
Note that the pressure and temperature are limited if necessary to stay within
a feasble range.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 23, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end ChartWorkingPairVLE;
