within SorpLib.Media.WorkingPairs.FluidPropertyDiagrams.PureComponents;
model ChartWorkingPairDubinin
  "Model that creates a fluid property diagram of a pure component working pair using the MSL for fluid property data calculation of a real fluid with a two-phase regime"
  extends SorpLib.Media.WorkingPairs.BaseClasses.PartialPureCharts(
    redeclare replaceable model PureWorkingPairModel =
      SorpLib.Media.WorkingPairs.PureComponents.H2O.AQSOAZ02_DubininLorentzianCumulative_Goldsworthy2014_Bau2017_VLE
      constrainedby SorpLib.Media.WorkingPairs.PureComponents.WorkingPairVLE(
        redeclare package Medium=Medium));

  //
  // Definition of replaceable medium
  //
  replaceable package WorkingPair =
    SorpLib.Media.WorkingPairs.Parametrizations.PureComponents.H2O.AQSOAZ02_DubininLorentzianCumulative_Goldsworthy2014_Bau2017
    constrainedby
    SorpLib.Media.WorkingPairs.Interfaces.PartialPureParametrizationDubinin(
      redeclare final package MediumSpecificFunctions =
        SorpLib.Media.WorkingPairs.Parametrizations.PureComponents.MediumSpecificFunctions.VLE
        ( redeclare final package Medium = Medium))
    "Parametrized working pair model"
    annotation (Dialog(tab="General",group="Models and Media"),
                choicesAllMatching = true,
                Evaluate=true,
                HideResult=true);

  replaceable package Medium = Modelica.Media.Water.StandardWater
    constrainedby Modelica.Media.Interfaces.PartialTwoPhaseMedium
    "Medium (i.e., adsorptive)"
    annotation (Dialog(tab="General",group="Models and Media"),
                choicesAllMatching = true);

  //
  // Definition of parameters
  //
  parameter Modelica.Units.SI.Pressure p_adsorpt_lowerLimit = 612
    "Lower limit for the pressure to calculate fluod property data"
    annotation (Dialog(tab = "Advanced", group = "Limiter"),
                Evaluate=true,
                HideResult=true);
  parameter Modelica.Units.SI.Temperature T_adsorpt_lowerLimit = 273.17
    "Lower limit for the temperature to calculate fluod property data"
    annotation (Dialog(tab = "Advanced", group = "Limiter"),
                Evaluate=true,
                HideResult=true);
  final parameter Modelica.Units.SI.Pressure p_sat_T_adsorpt_lowerLimit=
    if diagramType<>
    SorpLib.Choices.DiagramTypePureComponentWorkingPair.Isosters then 0 else
    Medium.saturationPressure(T=T_adsorpt_lowerLimit)
    "Saturation pressure at T_adsorpt_lowerLimit"
    annotation (Dialog(tab = "Advanced", group = "Limiter"),
                Evaluate=true,
                HideResult=true);

  //
  // Definition of variables
  //
  SorpLib.Units.Uptake x_sat_isotherms
    "Saturation uptake as function of changing pressure (i.e., required for 
    diagram of isotherms)";
  Modelica.Units.SI.Pressure p_sat_isosters
    "Saturation pressure as function of changing temperature (i.e., required for
    diagram of isosteres)";
  SorpLib.Units.Uptake x_sat_isobars
    "Saturation uptake as function of changing temperature (i.e., required for 
    diagram of isobars)";

  Real x_axis_isotherm
    "X-axis for isotherm diagram: Pressure p_adsorpt";
  Real x_axis_isosters
    "X-axis for isosters diagram: Negative reciproce temperature -1/T_adsorpt";
  Real x_axis_isobars
    "X-axis for isobars diagram: Temperature T_adsorpt";

  //
  // Definition of protected variables
  //
protected
  Modelica.Units.SI.Pressure
    p_adsorpt_(start=max(p_adsorpt_min, p_adsorpt_lowerLimit), fixed=true)
    "Changing pressure";
  Modelica.Units.SI.Temperature
    T_adsorpt_(start=max(T_adsorpt_min, T_adsorpt_lowerLimit), fixed=true)
    "Changing temperature";

  Modelica.Units.SI.Pressure p_sat_T_adsorpt
    "Saturation pressure as function of changing temperature (i.e., required
    for diagram of isosteres or isobars)";
  Modelica.Units.SI.Temperature T_sat_p_adsorpt
    "Saturation temperature as function of changing pressure (i.e., required
    for diagram of isotherms)";

  SorpLib.Units.Uptake x_sat_T_adsorpt_lowerLimit
    "Saturation uptake at p_sat(T_adsorpt_lowerLimit) and T_adsorpt_lowerLimit";
  SorpLib.Units.Uptake x_sat_T_adsorpt
    "Saturation uptake at p_sat(T_adsorpt) and T_adsorpt";

  //
  // Instantiation of final models
  //
  WorkingPair.IsothermCoefficients isothermCoefficients(
    final T_adsorpt=if diagramType==
    SorpLib.Choices.DiagramTypePureComponentWorkingPair.Isotherms then
    T_sat_p_adsorpt else T_adsorpt_)
    "Temperature-dependent isotherm coefficients";

equation
  //
  // Calculate upper limits for different diagrams types
  //
  T_sat_p_adsorpt = if diagramType<>
    SorpLib.Choices.DiagramTypePureComponentWorkingPair.Isotherms then 0 else
    Medium.saturationTemperature(p=p_adsorpt_)
    "Saturation pressure as function of changing temperature (i.e., required
    for diagram of isotherms)";
  p_sat_T_adsorpt = if diagramType==
    SorpLib.Choices.DiagramTypePureComponentWorkingPair.Isotherms then 0 else
    Medium.saturationPressure(T=T_adsorpt_)
    "Saturation pressure as function of changing temperature (i.e., required
    for diagram of isosteres)";

  x_sat_T_adsorpt_lowerLimit = if diagramType<>
    SorpLib.Choices.DiagramTypePureComponentWorkingPair.Isosters then 0 else
    WorkingPair.IsothermModel.x_pT(
      p_adsorpt= p_sat_T_adsorpt_lowerLimit,
      T_adsorpt=max(T_adsorpt_, T_adsorpt_lowerLimit),
      c=isothermCoefficients.c,
      p_adsorpt_lb_start=1,
      p_adsorpt_ub_start=10,
      tolerance=100*Modelica.Constants.eps)
    "Saturation uptake at p_sat(T_adsorpt_lowerLimit) and T_adsorpt_lowerLimit";
  x_sat_T_adsorpt = if diagramType<>
    SorpLib.Choices.DiagramTypePureComponentWorkingPair.Isosters then 0 else
    WorkingPair.IsothermModel.x_pT(
      p_adsorpt= p_sat_T_adsorpt,
      T_adsorpt=T_adsorpt_,
      c=isothermCoefficients.c,
      p_adsorpt_lb_start=1,
      p_adsorpt_ub_start=10,
      tolerance=100*Modelica.Constants.eps)
    "Saturation uptake at p_sat(T_adsorpt) and T_adsorpt";

  //
  // Calculate correct inputs
  //
  der(p_adsorpt_) = (p_adsorpt_max-max(p_adsorpt_min, p_adsorpt_lowerLimit)) / 20
    "Predescriped slope";
  der(T_adsorpt_) = (T_adsorpt_max-max(T_adsorpt_min, T_adsorpt_lowerLimit)) / 20
    "Changing temperature";

  if diagramType==SorpLib.Choices.DiagramTypePureComponentWorkingPair.Isotherms then
    p_adsorpt = fill(p_adsorpt_, no_isoLines)
      "Pressure";
    T_adsorpt = {max(T_adsorpt_const_[i], T_sat_p_adsorpt) for i in 1:no_isoLines}
      "Temperature";

  elseif diagramType==SorpLib.Choices.DiagramTypePureComponentWorkingPair.Isosters then
    x_adsorpt = {max(min(x_adsorpt_const_[i], x_sat_T_adsorpt),
      x_sat_T_adsorpt_lowerLimit) for i in 1:no_isoLines}
      "Uptake: Limited to its lower and upper limit";
    T_adsorpt = fill(T_adsorpt_, no_isoLines)
      "Temperature: Limitied to its lower limit";

  else
    p_adsorpt = {min(p_adsorpt_const_[i], p_sat_T_adsorpt) for i in 1:no_isoLines}
      "Pressure";
    T_adsorpt = fill(T_adsorpt_, no_isoLines)
      "Temperature";

  end if;

  //
  // Caclulate variables for plotting
  //
  x_sat_isotherms = if diagramType<>
    SorpLib.Choices.DiagramTypePureComponentWorkingPair.Isotherms then 0 else
    WorkingPair.IsothermModel.x_pT(
      p_adsorpt=p_adsorpt_,
      T_adsorpt=T_sat_p_adsorpt,
      c=isothermCoefficients.c,
      p_adsorpt_lb_start=1,
      p_adsorpt_ub_start=10,
      tolerance=100*Modelica.Constants.eps)
    "Saturation uptake as function of changing pressure (i.e., required for 
    diagram of isotherms)";
  p_sat_isosters = if diagramType<>
    SorpLib.Choices.DiagramTypePureComponentWorkingPair.Isosters then 0 else
    p_sat_T_adsorpt
    "Saturation pressure as function of changing temperature (i.e., required for
    diagram of isosteres)";
  x_sat_isobars = if diagramType<>
    SorpLib.Choices.DiagramTypePureComponentWorkingPair.Isobars then 0 else
    WorkingPair.IsothermModel.x_pT(
      p_adsorpt=p_sat_T_adsorpt,
      T_adsorpt=T_adsorpt_,
      c=isothermCoefficients.c,
      p_adsorpt_lb_start=1,
      p_adsorpt_ub_start=10,
      tolerance=100*Modelica.Constants.eps)
    "Saturation uptake as function of changing temperature (i.e., required for 
    diagram of isobars)";

  x_axis_isotherm = p_adsorpt_
    "X-axis for isotherm diagram: Pressure p_adsorpt";
  x_axis_isosters = -1/T_adsorpt_
    "X-axis for isosters diagram: Negative reciproce temperature -1/T_adsorpt";
  x_axis_isobars = T_adsorpt_
    "X-axis for isobars diagram: Temperature T_adsorpt";

  //
  // Asserations
  //
  if diagramType==SorpLib.Choices.DiagramTypePureComponentWorkingPair.Isotherms then
    assert(min(T_adsorpt_const_) > T_adsorpt_lowerLimit,
      "Minimum isotherm temperature (" + String(min(T_adsorpt_const_)) +
      ") is below the minimum temperature (" + String(T_adsorpt_lowerLimit) +
      "). Thus, fluid property data calculation is impossible.",
      level = AssertionLevel.error);

  elseif diagramType==SorpLib.Choices.DiagramTypePureComponentWorkingPair.Isobars then
    assert(min(p_adsorpt_const_) > p_adsorpt_lowerLimit,
      "Minimum isotherm pressure (" + String(min(p_adsorpt_const_)) +
      ") is below the minimum pressure (" + String(p_adsorpt_lowerLimit) +
      "). Thus, fluid property data calculation is impossible.",
      level = AssertionLevel.error);

  end if;

  //
  // Annotations
  //
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    Documentation(info="<html>
<p>
This partial model is the basic model for all models creating fluid propertey diagrams
for working pair models describing adsorption of pure components. These working paur models 
use a isotherm model based on the model of Dubinin and a medium model of a rel fluid with 
a two-regime based on the Modelica Standard Library. Note that the isotherms, isosters, and 
isobars are limited to lower and upper bounds if necessary to perform calculations within a 
feasble range.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 23, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end ChartWorkingPairDubinin;
