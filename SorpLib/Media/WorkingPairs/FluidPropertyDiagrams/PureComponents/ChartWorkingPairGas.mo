within SorpLib.Media.WorkingPairs.FluidPropertyDiagrams.PureComponents;
model ChartWorkingPairGas
  "Model that creates a fluid property diagram of a pure component working pair using the MSL for fluid property data calculation of a fluid with just one-regime (i.e., gas/vapor)"
  extends SorpLib.Media.WorkingPairs.BaseClasses.PartialPureCharts(
    redeclare replaceable model PureWorkingPairModel =
      SorpLib.Media.WorkingPairs.PureComponents.CO2.ActivatedCarbon_Toth_DantasEtAl2011_Gas
      constrainedby SorpLib.Media.WorkingPairs.PureComponents.WorkingPairGas(
        redeclare package Medium=Medium));

  //
  // Definition of replaceable medium
  //
  replaceable package Medium = SorpLib.Media.IdealGases.CO2
    constrainedby Modelica.Media.IdealGases.Common.SingleGasNasa
    "Medium (i.e., adsorptive)"
    annotation (Dialog(tab="General",group="Models and Media"),
                choicesAllMatching = true);

  //
  // Definition of variables
  //
  Real x_axis_isotherm
    "X-axis for isotherm diagram: Pressure p_adsorpt";
  Real x_axis_isosters
    "X-axis for isosters diagram: Negative reciproce temperature -1/T_adsorpt";
  Real x_axis_isobars
    "X-axis for isobars diagram: Temperature T_adsorpt";

  //
  // Definition of final parameters
  //
protected
  Modelica.Units.SI.Pressure p_adsorpt_(start=p_adsorpt_min, fixed=true)
    "Changing pressure";
  Modelica.Units.SI.Temperature T_adsorpt_(start=T_adsorpt_min, fixed=true)
    "Changing temperature";

equation
  //
  // Calculate correct inputs
  //
  der(p_adsorpt_) = (p_adsorpt_max-p_adsorpt_min) / 20
    "Predescriped slope";
  der(T_adsorpt_) = (T_adsorpt_max-T_adsorpt_min) / 20
    "Changing temperature";

  if diagramType==SorpLib.Choices.DiagramTypePureComponentWorkingPair.Isotherms then
    p_adsorpt = fill(p_adsorpt_, no_isoLines)
      "Pressure";
    T_adsorpt = T_adsorpt_const_
      "Temperature";

  elseif diagramType==SorpLib.Choices.DiagramTypePureComponentWorkingPair.Isosters then
    x_adsorpt = x_adsorpt_const_
      "Uptake";
    T_adsorpt = fill(T_adsorpt_, no_isoLines)
      "Temperature";

  else
    p_adsorpt = p_adsorpt_const_
      "Pressure";
    T_adsorpt = fill(T_adsorpt_, no_isoLines)
      "Temperature";

  end if;

  //
  // Caclulate variables for plotting
  //
  x_axis_isotherm = p_adsorpt_
    "X-axis for isotherm diagram: Pressure p_adsorpt";
  x_axis_isosters = -1/T_adsorpt_
    "X-axis for isosters diagram: Negative reciproce temperature -1/T_adsorpt";
  x_axis_isobars = T_adsorpt_
    "X-axis for isobars diagram: Temperature T_adsorpt";

  //
  // Annotations
  //
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    Documentation(info="<html>
<p>
This partial model is the basic model for all models creating fluid propertey diagrams
for working pair models describing adsorption of pure components and using a medium 
model of a fluid with just one regime (i.e., gas/vapor) based on the Modelica Standard 
Library.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 23, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end ChartWorkingPairGas;
