within SorpLib.Media.WorkingPairs;
package FluidPropertyDiagrams "Package containing models to create fluid property diagrams"
extends Icons.MediaPackage;

annotation (Documentation(info="<html>
<p>
This package contains models to generate fluid property diagrams. The 
implemented diagram types include isotherms, isosterics, and isobars.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 23, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end FluidPropertyDiagrams;
