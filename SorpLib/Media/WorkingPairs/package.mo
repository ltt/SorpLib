within SorpLib.Media;
package WorkingPairs "Models to calcualte fluid property data of sorption working pairs"
  extends Modelica.Icons.VariantsPackage;

  annotation (Documentation(info="<html>
<p>
This package contains models calculating thermodynamic properties of pure component
and multi-component working pairs. Independent state variables may be (1) the pressure 
and temperature or (2) the uptake and temperature. In the models, fundamental 
thermodynamic properties can be calculated:
</p>
<ol>
  <li>
  Thermal state variables: <i>p</i>, <i>T</i>, and <i>&rho;</i>.
  </li>
  <li>
  Caloric state variables: <i>h</i> and <i>u</i>.
  </li>
  <li>
  Entropic state variables: <i>s</i>, <i>g</i>, and <i>a</i>.
  </li>
</ol>
<p>
Moreover, the model allow to calculate all partial derivatives with respect to the
independent state variables that are required for dynamic mass, energy, and entropy
balances in finite volume models.
</p>

<h4>How to add new working pair models</h4>
<p>
To add a new working pair models, three steps are required:
</p>
<ol>
  <li>
  Duplicate an existing parametrization package and save it into the correct sub-pacakge
  of the 
  <a href=\"Modelica://SorpLib.Media.WorkingPairs.Parametrizations\">Parametrizations package</a>.
  Then, parametrize the dublicated package and write the documentation, including 
  references for the thermodynamic properties.
  </li>
  <li>
  Duplicate an existing working pair model and save it into the correct sub-pacakge of the 
  <a href=\"Modelica://SorpLib.Media.WorkingPairs.PureComponents\">PureComponents package</a>
  or the
  <a href=\"Modelica://SorpLib.Media.WorkingPairs.MultiComponents\">MultiComponents package</a>.
  Then, finalize the model.
  </li>
  <li>
  Duplicate an existing test model and save it into the correct sub-pacakge of the 
  <a href=\"Modelica://SorpLib.Media.WorkingPairs.PureComponents.Testers\">PureComponents.Testers package</a>
  or the
  <a href=\"Modelica://SorpLib.Media.WorkingPairs.MultiComponents.Testers\">MultiComponents.Testers package</a>.
  Then, test the new working pair model and varify calculations.
  </li>
</ol>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end WorkingPairs;
