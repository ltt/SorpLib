within SorpLib.Media.WorkingPairs.Parametrizations.Utilities;
function linear1 "Generalized linear function 1"
  extends BaseClasses.Partial_z_T;

  //
  // Definition of inputs
  //
  input Real a
    "First parameter"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real b
    "Second parameter"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real c
    "Third parameter"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real d
    "Fourth parameter"
    annotation (Dialog(tab="General", group="Inputs"));

algorithm
  z :=(a + b*T + c/T)^d
    "Coefficient";

  //
  // Annotations
  //
  annotation (Inline=true,
  Documentation(info="<html>
<p>
This generalized linear function calculates temperature-dependent isotherm 
coefficients.
</p>

<h4>Main equations</h4>
<p>
The generalized linear function has the following form:
</p>
<pre>
    z = (a + b * T + c / T) ^ d;
</pre>
<p>
where <i>a</i>, <i>b</i>, <i>c</i>, and <i>d</i> are the coefficients.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end linear1;
