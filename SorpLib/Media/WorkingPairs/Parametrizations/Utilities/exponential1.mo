within SorpLib.Media.WorkingPairs.Parametrizations.Utilities;
function exponential1 "Generalized exponential function 1"
  extends BaseClasses.Partial_z_T;

  //
  // Definition of inputs
  //
  input Real a
    "First parameter"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real b
    "Second parameter"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real c
    "Third parameter"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real d
    "Fourth parameter"
    annotation (Dialog(tab="General", group="Inputs"));

algorithm
  z := a * exp(b + c * T + d / T)
    "Coefficient";

  //
  // Annotations
  //
  annotation (Inline=true,
  Documentation(info="<html>
<p>
This generalized exponential function calculates temperature-dependent isotherm 
coefficients.
</p>

<h4>Main equations</h4>
<p>
The generalized exponential function has the following form:
</p>
<pre>
    z = a * <strong>exp</strong>(b + c * T + d / T);
</pre>
<p>
where <i>a</i>, <i>b</i>, <i>c</i>, and <i>d</i> are the coefficients.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end exponential1;
