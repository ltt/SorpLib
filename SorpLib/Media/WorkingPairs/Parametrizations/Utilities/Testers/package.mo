within SorpLib.Media.WorkingPairs.Parametrizations.Utilities;
package Testers "Models to test and varify utility functions"
extends Modelica.Icons.ExamplesPackage;

annotation (Documentation(info="<html>
<p>
This package contains executable test models for all utility functions. The test 
models check the implementation and demonstrate the utility functions' general 
applicability.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Testers;
