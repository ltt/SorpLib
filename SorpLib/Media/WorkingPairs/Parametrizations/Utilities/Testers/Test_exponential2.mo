within SorpLib.Media.WorkingPairs.Parametrizations.Utilities.Testers;
model Test_exponential2
  "Tester for the function 'exponential2' and all corresponding functions"
  extends Modelica.Icons.Example;

  //
  // Definition of parameters
  //
  parameter Real a = 10
    "First coefficient"
    annotation (Dialog(tab="General", group="General"));
  parameter Real b = 1
    "Second coefficient"
    annotation (Dialog(tab="General", group="General"));
  parameter Real c = 0.001
    "Third coefficient"
    annotation (Dialog(tab="General", group="General"));
  parameter Real d = 100
    "Fourth coefficient"
    annotation (Dialog(tab="General", group="General"));
  parameter Real f = 5
    "Fivth coefficient"
    annotation (Dialog(tab="General", group="General"));
  parameter Real g = -0.1
    "Sixth coefficient"
    annotation (Dialog(tab="General", group="General"));
  parameter Real h = 0.1
    "Seventh coefficient"
    annotation (Dialog(tab="General", group="General"));

  parameter Modelica.Units.SI.TemperatureDifference dT = 1e-3
    "Temperature difference used to calculate derivatives numerically"
    annotation (Dialog(tab="General", group="General"));

  //
  // Definition of variables
  //
  Modelica.Units.SI.Temperature T(start=273.15, fixed=true)
    "Temperature";

  Real z
    "Temperature-dependent coefficient";

  Real dz_dT
    "Partial derivative of temperature-dependent coefficient w.r.t. temperature";
  Real dz_dT_num
    "Partial derivative of temperature-dependent coefficient w.r.t. temperature
    calculated numerically";

  Real ddz_dT_dT
    "Second-order partial derivative of temperature-dependent coefficient w.r.t. 
    temperature";
  Real ddz_dT_dT_num
    "Second-order partial derivative of temperature-dependent coefficient w.r.t. 
    temperature calculated numerically";

equation
  //
  // Definition of derivatives
  //
  der(T) = 1000/20
    "Predecsriped slope of T";

  //
  // Calculation of coefficients and their partial derivatives
  //
  z = SorpLib.Media.WorkingPairs.Parametrizations.Utilities.exponential2(
    T=T, a=a, b=b, c=c, d=d, f=f, g=g, h=h)
    "Temperature-dependent coefficient";

  dz_dT = SorpLib.Media.WorkingPairs.Parametrizations.Utilities.dexponential2_dT(
    T=T, a=a, b=b, c=c, d=d, f=f, g=g, h=h)
    "Partial derivative of temperature-dependent coefficient w.r.t. temperature";
  dz_dT_num = (SorpLib.Media.WorkingPairs.Parametrizations.Utilities.exponential2(
    T=T+dT, a=a, b=b, c=c, d=d, f=f, g=g, h=h) -
    SorpLib.Media.WorkingPairs.Parametrizations.Utilities.exponential2(
    T=T-dT, a=a, b=b, c=c, d=d, f=f, g=g, h=h)) / (2 * dT)
    "Partial derivative of temperature-dependent coefficient w.r.t. temperature
    calculated numerically";

  ddz_dT_dT = SorpLib.Media.WorkingPairs.Parametrizations.Utilities.ddexponential2_dT_dT(
    T=T, a=a, b=b, c=c, d=d, f=f, g=g, h=h)
    "Second-order partial derivative of temperature-dependent coefficient w.r.t. 
    temperature";
  ddz_dT_dT_num = (SorpLib.Media.WorkingPairs.Parametrizations.Utilities.dexponential2_dT(
    T=T+dT, a=a, b=b, c=c, d=d, f=f, g=g, h=h) -
    SorpLib.Media.WorkingPairs.Parametrizations.Utilities.dexponential2_dT(
    T=T-dT, a=a, b=b, c=c, d=d, f=f, g=g, h=h)) / (2 * dT)
    "Second-order partial derivative of temperature-dependent coefficient w.r.t. 
    temperature calculated numerically";

  //
  // Definition of assertions: Check numerical implementations
  //
  assert(abs(dz_dT-dz_dT_num) < 1e-6,
    "Partial derivative of z w.r.t. temperature is not valied: Deviation (|" +
    String(abs(dz_dT-dz_dT_num)) +
    "|) is greater than 1e-6!",
    level = AssertionLevel.warning);
  assert(abs(ddz_dT_dT-ddz_dT_dT_num) < 1e-6,
    "Second-order partial derivative of z w.r.t. temperature is not valied: Deviation (|" +
    String(abs(ddz_dT_dT-ddz_dT_dT_num)) +
    "|) is greater than 1e-6!",
    level = AssertionLevel.warning);

  //
  // Annotations
  //
  annotation (experiment(
      StopTime=20,
      Tolerance=1e-06),
Documentation(revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>", info="<html>
<p>
This tester shows the behavior of the 'exponential2' function and its partials derivatives 
with resprect to temperature.
<br/><br/>
To see the function behavior, plot the variables <i>z_i</i> over the time. The 
simulation time is correctly preset (Start: 0 s, Stop = 20 s). 
</p>
</html>"));
end Test_exponential2;
