within SorpLib.Media.WorkingPairs.Parametrizations.Utilities;
function exponential2 "Generalized exponential function 2"
  extends BaseClasses.Partial_z_T;

  //
  // Definition of inputs
  //
  input Real a
    "First parameter"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real b
    "Second parameter"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real c
    "Third parameter"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real d
    "Fourth parameter"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real f
    "Fivth parameter"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real g
    "Sixth parameter"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real h
    "Seventh parameter"
    annotation (Dialog(tab="General", group="Inputs"));

algorithm
  z := a * exp((b + c * T + d / T + f * exp(g * T)) / (h * T))
    "Coefficient";

  //
  // Annotations
  //
  annotation (Inline=true,
  Documentation(info="<html>
<p>
This generalized exponential function calculates temperature-dependent isotherm 
coefficients.
</p>

<h4>Main equations</h4>
<p>
The generalized exponential function has the following form:
</p>
<pre>
    z = a * <strong>exp</strong>((b + c * T + d / T + f * <strong>exp</strong>(g * T)) / (h * T));
</pre>
<p>
where <i>a</i>, <i>b</i>, <i>c</i>, <i>d</i>, <i>f</i>, <i>g</i>, and <i>h</i> 
are the coefficients.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end exponential2;
