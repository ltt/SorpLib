within SorpLib.Media.WorkingPairs.Parametrizations.Utilities;
function ddexponential2_dT_dT
  "Second-order partial derivative of generalized exponential function 2 w.r.t. temperature"
  extends BaseClasses.Partial_ddz_dT_dT;

  //
  // Definition of inputs
  //
  input Real a
    "First parameter"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real b
    "Second parameter"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real c
    "Third parameter"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real d
    "Fourth parameter"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real f
    "Fivth parameter"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real g
    "Sixth parameter"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real h
    "Seventh parameter"
    annotation (Dialog(tab="General", group="Inputs"));

algorithm
  ddz_dT_dT := (a * (f^2 * T^2 * (g * T - 1)^2 * exp(2 * g * T) +
    f * T * (T * (g^2 * h * T^3 - 2 * g * h * T^2 + 2 * h * T - 2 * b * g * T -
    4 * d * g + 2 * b) + 4 * d) * exp(g * T) + 2 * b * h * T^3 + (6 * d * h + b^2) *
    T^2 + 4 * b * d * T + 4 * d^2) * exp((T * (f * exp(g * T) + c * T + b) + d) /
    (h * T^2))) / (h^2 * T^6)
    "Second-order partial derivative of coefficient w.r.t. temperature";

  //
  // Annotations
  //
  annotation (Inline=true,
  Documentation(info="<html>
<p>
This function is the second-order partial derivative of the function 'exponential2' 
with respect to the temperature. For full details of the original function 'exponential2,' 
check the documentation of the function 
<a href=\"Modelica://SorpLib.Media.WorkingPairs.Parametrizations.Utilities.exponential2\">SorpLib.Media.WorkingPairs.Parametrizations.Utilities.exponential2</a>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end ddexponential2_dT_dT;
