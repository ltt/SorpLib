within SorpLib.Media.WorkingPairs.Parametrizations.Utilities;
function ddlinear1_dT_dT
  "Second-order partial derivative of generalized linear function 1 w.r.t. temperature"
  extends BaseClasses.Partial_ddz_dT_dT;

  //
  // Definition of inputs
  //
  input Real a
    "First parameter"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real b
    "Second parameter"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real c
    "Third parameter"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real d
    "Fourth parameter"
    annotation (Dialog(tab="General", group="Inputs"));

algorithm
  ddz_dT_dT := (2 * c * d * (b * T + c / T + a) ^ (d - 1)) / T^3 +
    (d - 1) * d * (b - c / T^2)^2 * (b * T + c / T + a) ^ (d - 2)
    "Second-order partial derivative of coefficient w.r.t. temperature";

  //
  // Annotations
  //
  annotation (Inline=true,
  Documentation(info="<html>
<p>
This function is the second-order partial derivative of the function 'linear1' 
with respect to the temperature. For full details of the original function 'linear1,' 
check the documentation of the function 
<a href=\"Modelica://SorpLib.Media.WorkingPairs.Parametrizations.Utilities.linear1\">SorpLib.Media.WorkingPairs.Parametrizations.Utilities.linear1</a>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end ddlinear1_dT_dT;
