within SorpLib.Media.WorkingPairs.Parametrizations.Utilities;
function dexponential1_dT
  "Partial derivative of generalized exponential function 1 w.r.t. temperature"
  extends BaseClasses.Partial_dz_dT;

  //
  // Definition of inputs
  //
  input Real a
    "First parameter"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real b
    "Second parameter"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real c
    "Third parameter"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real d
    "Fourth parameter"
    annotation (Dialog(tab="General", group="Inputs"));

algorithm
  dz_dT := a * (c - d / T^2) * exp(c * T + d / T + b)
    "Partial derivative of coefficient w.r.t. temperature";

  //
  // Annotations
  //
  annotation (Inline=true,
  Documentation(info="<html>
<p>
This function is the partial derivative of the function 'exponential1' with respect 
to the temperature. For full details of the original function 'exponential1,' check 
the documentation of the function 
<a href=\"Modelica://SorpLib.Media.WorkingPairs.Parametrizations.Utilities.exponential1\">SorpLib.Media.WorkingPairs.Parametrizations.Utilities.exponential1</a>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end dexponential1_dT;
