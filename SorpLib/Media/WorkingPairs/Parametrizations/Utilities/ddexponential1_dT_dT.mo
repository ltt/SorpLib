within SorpLib.Media.WorkingPairs.Parametrizations.Utilities;
function ddexponential1_dT_dT
  "Second-order partial derivative of generalized exponential function 1 w.r.t. temperature"
  extends BaseClasses.Partial_ddz_dT_dT;

  //
  // Definition of inputs
  //
  input Real a
    "First parameter"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real b
    "Second parameter"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real c
    "Third parameter"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real d
    "Fourth parameter"
    annotation (Dialog(tab="General", group="Inputs"));

algorithm
  ddz_dT_dT := (2 * a * d * exp(c * T + d / T + b)) / T^3 +
    a * (c - d / T^2)^2 * exp(c * T + d / T + b)
    "Second-order partial derivative of coefficient w.r.t. temperature";

  //
  // Annotations
  //
  annotation (Inline=true,
  Documentation(info="<html>
<p>
This function is the second-order partial derivative of the function 'exponential1' 
with respect to the temperature. For full details of the original function 'exponential1,' 
check the documentation of the function 
<a href=\"Modelica://SorpLib.Media.WorkingPairs.Parametrizations.Utilities.exponential1\">SorpLib.Media.WorkingPairs.Parametrizations.Utilities.exponential1</a>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end ddexponential1_dT_dT;
