within SorpLib.Media.WorkingPairs.Parametrizations;
package Utilities "Package containing utility functions used to parametrize working pairs"
extends Modelica.Icons.UtilitiesPackage;

annotation (Documentation(info="<html>
<p>
This package contains utility functions used to parametrize working pair
models. 
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Utilities;
