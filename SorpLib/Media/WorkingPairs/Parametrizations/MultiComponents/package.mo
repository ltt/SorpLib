within SorpLib.Media.WorkingPairs.Parametrizations;
package MultiComponents "Package containing parametrizations of multi-component working pairs"
  extends Modelica.Icons.VariantsPackage;

  annotation (Documentation(info="<html>
<p>
This package contains parametrized multi-component working pair models. Check
the package content to see for which adsorptive mixtures working pair models 
are already implemented.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end MultiComponents;
