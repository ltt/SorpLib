within SorpLib.Media.WorkingPairs.Parametrizations.MultiComponents;
package CO2_H2O "Package containing parametrizations for CO2 and H2O as adsorptives"
  extends Modelica.Icons.VariantsPackage;

  annotation (Documentation(info="<html>
<p>
This package contains parametrizations for CO<sub>2</sub> and H<sub>2</sub>O
as adsorptives.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end CO2_H2O;
