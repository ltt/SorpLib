within SorpLib.Media.WorkingPairs.Parametrizations.PureComponents.O2;
package ZeolithLiLSX_Langmuir_LiEtAl2021 "O2 & Zeolith LiLSX via the Langmuir isotherm model according to Li et al. (2021)"
  extends
    SorpLib.Media.WorkingPairs.Interfaces.PartialPureParametrizationNonDubinin(
    final M_adsorptive= 31.9988/1000,
    twoPhaseAdsorptive=false,
    final no_coefficients=2,
    redeclare final package IsothermModel =
      SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Langmuir,
    redeclare replaceable package MediumSpecificFunctions =
      SorpLib.Media.WorkingPairs.Parametrizations.PureComponents.MediumSpecificFunctions.Gas);

  //
  // Definition of further constants
  //
  constant SorpLib.Units.Uptake k1 = 33.947 * M_adsorptive
    "First contants of the isotherm model";
  constant Real k2(unit="kg/(kg.K)") = -0.090 * M_adsorptive
    "Second contants of the isotherm model";
  constant Real k3(unit="1/Pa") = 0.090 * 1e-5
    "Third contants of the isotherm model";
  constant Modelica.Units.SI.Temperature k4(min=-500) = -426.018
    "Fourth contants of the isotherm model";

  //
  // Redeclare functions
  //
  redeclare final function extends calc_c
    "Calculates temperature-dependent coefficients of the isotherm model"
  algorithm
    c[1] :=
      SorpLib.Media.WorkingPairs.Parametrizations.Utilities.linear1(
      T=T_adsorpt,
      a=k1,
      b=k2,
      c=0,
      d=1);
    c[2] :=
      SorpLib.Media.WorkingPairs.Parametrizations.Utilities.exponential1(
      T=T_adsorpt,
      a=k3,
      b=0,
      c=0,
      d=k4);
  end calc_c;

  redeclare final function extends calc_coefficients
    "Calculates temperature-dependent coefficients and their the partial derivatives w.r.t. temperature"
  algorithm
    //
    // Calculate coefficients
    //
    c[1] :=
      SorpLib.Media.WorkingPairs.Parametrizations.Utilities.linear1(
      T=T_adsorpt,
      a=k1,
      b=k2,
      c=0,
      d=1);
    c[2] :=
      SorpLib.Media.WorkingPairs.Parametrizations.Utilities.exponential1(
      T=T_adsorpt,
      a=k3,
      b=0,
      c=0,
      d=k4);

    //
    // Calculate partial derivatives of the coefficients w.r.t. temperature
    //
    dc_dT[1] :=
      SorpLib.Media.WorkingPairs.Parametrizations.Utilities.dlinear1_dT(
      T=T_adsorpt,
      a=k1,
      b=k2,
      c=0,
      d=1);
    dc_dT[2] :=
      SorpLib.Media.WorkingPairs.Parametrizations.Utilities.dexponential1_dT(
      T=T_adsorpt,
      a=k3,
      b=0,
      c=0,
      d=k4);

    //
    // Calculate second-order partial derivatives of the coefficients w.r.t.
    // temperature
    //
    ddc_dT_dT[1] :=
      SorpLib.Media.WorkingPairs.Parametrizations.Utilities.ddlinear1_dT_dT(
      T=T_adsorpt,
      a=k1,
      b=k2,
      c=0,
      d=1);
    ddc_dT_dT[2] :=
      SorpLib.Media.WorkingPairs.Parametrizations.Utilities.ddexponential1_dT_dT(
      T=T_adsorpt,
      a=k3,
      b=0,
      c=0,
      d=k4);
  end calc_coefficients;
  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial package describes the adsorption equilibrium of the working pair 
O<sub>2</sub> & Zeolith LiLSX using the Langmuir isotherm model according to Li 
et al. (2011). Packages that inherit properties from this partial package may 
redeclare the package <i>MediumSpecificFunctions</i>, the model <i>Sorbent</i>,
and the constant <i>twoPhaseAdsorptive</i>.
</p>

<h4>References</h4>
<ul>
  <li>
  Li, L., and Yu, M., and Zi, Y., and Dang, Y., and Wu, Q., and Wang, Z., and Xu, Y., and Yan, H, and Dang, Y. (2021). A Thermodynamic Model for Pure and Binary Adsorption Equilibria of N<sub>2</sub> and O<sub>2</sub> on Lithium-Exchanged Low Silicon-to-Aluminum Ratio X Zeolite, Journal of Chemical & Engineering Data, 60:1032-1042. DOI: https://dx.doi.org/10.1021/acs.jced.0c00830.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end ZeolithLiLSX_Langmuir_LiEtAl2021;
