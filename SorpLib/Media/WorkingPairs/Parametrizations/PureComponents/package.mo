within SorpLib.Media.WorkingPairs.Parametrizations;
package PureComponents "Package containing parametrizations of pure component working pairs"
  extends Modelica.Icons.VariantsPackage;

  annotation (Documentation(info="<html>
<p>
This package contains parametrized pure component working pair models. Check
the package content to see for which adsorptives working pair models are
already implemented.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end PureComponents;
