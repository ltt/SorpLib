within SorpLib.Media.WorkingPairs.Parametrizations.PureComponents.H2O;
package SilicaGel125_DubininLorentzianCumulative_Schawe2000 "H2O & Silica gel 125 via the Dubinin isotherm model with a Lorentzian Cumulative approach according to Schawe (2000)"
  extends
  SorpLib.Media.WorkingPairs.Interfaces.PartialPureParametrizationDubinin(
    final M_adsorptive=18.0153/1000,
    final no_coefficients=6,
    redeclare final package IsothermModel =
      SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative,
    redeclare replaceable package MediumSpecificFunctions =
      SorpLib.Media.WorkingPairs.Parametrizations.PureComponents.MediumSpecificFunctions.VLE,
    redeclare replaceable model Sorbent =
      SorpLib.Media.Solids.Sorbents.RDSilicaGel);

  //
  // Definition of further constants
  //
  constant SorpLib.Units.FilledPoreVolume char_curve_a = 4.527805e-1 / 1000
    "First contants of the isotherm model";
  constant Real char_curve_b(unit="J/mol") = 1.229005e2 * 1000 * M_adsorptive
    "Second contants of the isotherm model";
  constant Real char_curve_c(unit="J/mol") = -8.847167e1 * 1000 * M_adsorptive
    "Third contants of the isotherm model";
  constant SorpLib.Units.FilledPoreVolume char_curve_d = 6.034706e-4 / 1000
    "Fourth contants of the isotherm model";

  //
  // Redeclare functions
  //
  redeclare final function extends calc_c
    "Calculates temperature-dependent coefficients of the isotherm model"
  algorithm
    c[1] := MediumSpecificFunctions.p_sat_T(
       T=T_adsorpt);
    c[2] := MediumSpecificFunctions.rho_satLiq_T(
       T=T_adsorpt);
    c[3] :=char_curve_a;
    c[4] :=char_curve_b;
    c[5] :=char_curve_c;
    c[6] :=char_curve_d;
  end calc_c;

  redeclare final function extends calc_coefficients
    "Calculates temperature-dependent coefficients and their the partial derivatives w.r.t. temperature"
  algorithm
    //
    // Get medium-specific functions (p_sat, rho_satLiq, and their partial
    // derivatives w.r.t. temperature)
    //
    (c[1],dc_dT[1],ddc_dT_dT[1],c[2],dc_dT[2],ddc_dT_dT[2]) :=
      MediumSpecificFunctions.pRho_satLiq(T=T_adsorpt, dT=dT)
      "Calculates saturated vapor pressure, density and bubble point, and their
      first- and secon-order partial derivatives w.r.t. temperature)";

    //
    // Calculate further coefficients
    //
    c[3] :=char_curve_a;
    c[4] :=char_curve_b;
    c[5] :=char_curve_c;
    c[6] :=char_curve_d;

    //
    // Calculate further partial derivatives of the coefficients w.r.t.
    // temperature
    //
    dc_dT[3] :=0;
    dc_dT[4] :=0;
    dc_dT[5] :=0;
    dc_dT[6] :=0;

    //
    // Calculate further second-order partial derivatives of the coefficients
    // w.r.t. temperature
    //
    ddc_dT_dT[3] :=0;
    ddc_dT_dT[4] :=0;
    ddc_dT_dT[5] :=0;
    ddc_dT_dT[6] :=0;
  end calc_coefficients;
  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial package describes the adsorption equilibrium of the working pair 
H<sub>2</sub>0 & silica gel 125 using the Dubinin isotherm model with a Lorentzian 
Cumulative characteristic curve according to Schawe (2000). Packages that inherit 
properties from this partial package may redeclare the package <i>MediumSpecificFunctions</i>
and the model <i>Sorbent</i>.
</p>

<h4>References</h4>
<ul>
  <li>
  Schawe (2000). Theoretical and Experimental Investigations of an Adsorption Heat Pump with Heat Transfer between two Adsorbers, PhD thesis. DOI: http://dx.doi.org/10.18419/opus-1518.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end SilicaGel125_DubininLorentzianCumulative_Schawe2000;
