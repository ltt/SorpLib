within SorpLib.Media.WorkingPairs.Parametrizations.PureComponents.H2O;
package SilicaGelN_DubininPearsonIV_Schawe2000 "H2O & Silica gel N via the Dubinin isotherm model with a Pearson IV approach according to Schawe (2000)"
  extends
    SorpLib.Media.WorkingPairs.Interfaces.PartialPureParametrizationDubinin(
    final M_adsorptive=18.0153/1000,
    final no_coefficients=8,
    redeclare final package IsothermModel =
      SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininPearsonIV,
    redeclare replaceable package MediumSpecificFunctions =
      SorpLib.Media.WorkingPairs.Parametrizations.PureComponents.MediumSpecificFunctions.VLE,
    redeclare replaceable model Sorbent =
      SorpLib.Media.Solids.Sorbents.RDSilicaGel(lambda_constant=0.2));

  //
  // Definition of further constants
  //
  constant SorpLib.Units.FilledPoreVolume char_curve_a(min=-1) = -7.689279e-1 / 1000
    "First contants of the isotherm model";
  constant Real char_curve_b(unit="J/mol")=1.176831 / 1000
    "Second contants of the isotherm model";
  constant Real char_curve_c(unit="J/mol") = 1.485965e1 * 1000 * M_adsorptive
    "Third contants of the isotherm model";
  constant SorpLib.Units.FilledPoreVolume char_curve_d=
    4.244922e1 * 1000 * M_adsorptive
    "Fourth contants of the isotherm model";
  constant SorpLib.Units.FilledPoreVolume char_curve_e= 2.207797e-2
    "Fivth contants of the isotherm model";
  constant SorpLib.Units.FilledPoreVolume char_curve_f= 1.146067e-1
    "Sixth contants of the isotherm model";

  //
  // Redeclare functions
  //
  redeclare final function extends calc_c
    "Calculates temperature-dependent coefficients of the isotherm model"
  algorithm
    c[1] := MediumSpecificFunctions.p_sat_T(
       T=T_adsorpt);
    c[2] := MediumSpecificFunctions.rho_satLiq_T(
       T=T_adsorpt);
    c[3] :=char_curve_a;
    c[4] :=char_curve_b;
    c[5] :=char_curve_c;
    c[6] :=char_curve_d;
    c[7] :=char_curve_e;
    c[8] :=char_curve_f;
  end calc_c;

  redeclare final function extends calc_coefficients
    "Calculates temperature-dependent coefficients and their the partial derivatives w.r.t. temperature"
  algorithm
    //
    // Get medium-specific functions (p_sat, rho_satLiq, and their partial
    // derivatives w.r.t. temperature)
    //
    (c[1],dc_dT[1],ddc_dT_dT[1],c[2],dc_dT[2],ddc_dT_dT[2]) :=
      MediumSpecificFunctions.pRho_satLiq(T=T_adsorpt, dT=dT)
      "Calculates saturated vapor pressure, density and bubble point, and their
      first- and secon-order partial derivatives w.r.t. temperature)";

    //
    // Calculate further coefficients
    //
    c[3] :=char_curve_a;
    c[4] :=char_curve_b;
    c[5] :=char_curve_c;
    c[6] :=char_curve_d;
    c[7] :=char_curve_e;
    c[8] :=char_curve_f;

    //
    // Calculate further partial derivatives of the coefficients w.r.t.
    // temperature
    //
    dc_dT[3] :=0;
    dc_dT[4] :=0;
    dc_dT[5] :=0;
    dc_dT[6] :=0;
    dc_dT[7] :=0;
    dc_dT[8] :=0;

    //
    // Calculate further second-order partial derivatives of the coefficients
    // w.r.t. temperature
    //
    ddc_dT_dT[3] :=0;
    ddc_dT_dT[4] :=0;
    ddc_dT_dT[5] :=0;
    ddc_dT_dT[6] :=0;
    ddc_dT_dT[7] :=0;
    ddc_dT_dT[8] :=0;
  end calc_coefficients;
  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial package describes the adsorption equilibrium of the working pair 
H<sub>2</sub>0 & silica gel N using the Dubinin isotherm model with a Pearson IV 
characteristic curve according to Schawe (2000). Packages that inherit 
properties from this partial package may redeclare the package <i>MediumSpecificFunctions</i>
and the model <i>Sorbent</i>.
</p>

<h4>References</h4>
<ul>
  <li>
  Schawe (2000). Theoretical and Experimental Investigations of an Adsorption Heat Pump with Heat Transfer between two Adsorbers, PhD thesis. DOI: http://dx.doi.org/10.18419/opus-1518.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end SilicaGelN_DubininPearsonIV_Schawe2000;
