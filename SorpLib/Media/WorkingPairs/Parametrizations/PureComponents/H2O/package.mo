within SorpLib.Media.WorkingPairs.Parametrizations.PureComponents;
package H2O "Package containing parametrizations for H2O as adsorptive"
  extends Modelica.Icons.VariantsPackage;

  annotation (Documentation(info="<html>
<p>
This package contains parametrizations for H<sub>2</sub>O as adsorptive.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end H2O;
