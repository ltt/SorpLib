within SorpLib.Media.WorkingPairs.Parametrizations.PureComponents.H2O;
package SilicaGel_Toth_WangDouglasLeVan2009 "H2O & silica gel via the Toth isotherm model according to Wang and Douglas LeVan (2009)"
  extends
    SorpLib.Media.WorkingPairs.Interfaces.PartialPureParametrizationNonDubinin(
    final M_adsorptive=18.0153/1000,
    twoPhaseAdsorptive=false,
    final no_coefficients=3,
    redeclare final package IsothermModel =
      SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Toth,
    redeclare replaceable package MediumSpecificFunctions =
      SorpLib.Media.WorkingPairs.Parametrizations.PureComponents.MediumSpecificFunctions.Gas);

  //
  // Definition of further constants
  //
  constant SorpLib.Units.Uptake x_sat = 1.767e-1 / b_ref * M_adsorptive
    "First contants of the isotherm model";
  constant Real b_ref(unit="1/Pa") = 2.787e-8
    "Second contants of the isotherm model";
  constant Real E(unit="K") = 1.093e3
    "Third contants of the isotherm model";
  constant Real t(unit="1") = -1.190e-3
    "Fourth contants of the isotherm model";
  constant Real alpha(unit="1/K") = 2.213e1
    "Fivth contants of the isotherm model";

  //
  // Redeclare functions
  //
  redeclare final function extends calc_c
    "Calculates temperature-dependent coefficients of the isotherm model"
  algorithm
    c[1] :=x_sat;
    c[2] :=SorpLib.Media.WorkingPairs.Parametrizations.Utilities.exponential1(
      T=T_adsorpt,
      a=b_ref,
      b=0,
      c=0,
      d=E);
    c[3] :=SorpLib.Media.WorkingPairs.Parametrizations.Utilities.linear1(
      T=T_adsorpt,
      a=t,
      b=0,
      c=alpha,
      d=1);
  end calc_c;

  redeclare final function extends calc_coefficients
    "Calculates temperature-dependent coefficients and their the partial derivatives w.r.t. temperature"
  algorithm
    //
    // Calculate coefficients
    //
    c[1] :=x_sat;
    c[2] :=SorpLib.Media.WorkingPairs.Parametrizations.Utilities.exponential1(
      T=T_adsorpt,
      a=b_ref,
      b=0,
      c=0,
      d=E);
    c[3] :=SorpLib.Media.WorkingPairs.Parametrizations.Utilities.linear1(
      T=T_adsorpt,
      a=t,
      b=0,
      c=alpha,
      d=1);

    //
    // Calculate partial derivatives of the coefficients w.r.t. temperature
    //
    dc_dT[1] :=0;
    dc_dT[2] :=
    SorpLib.Media.WorkingPairs.Parametrizations.Utilities.dexponential1_dT(
      T=T_adsorpt,
      a=b_ref,
      b=0,
      c=0,
      d=E);
    dc_dT[3] :=SorpLib.Media.WorkingPairs.Parametrizations.Utilities.dlinear1_dT(
      T=T_adsorpt,
      a=t,
      b=0,
      c=alpha,
      d=1);

    //
    // Calculate second-order partial derivatives of the coefficients w.r.t.
    // temperature
    //
    ddc_dT_dT[1] :=0;
    ddc_dT_dT[2] :=
    SorpLib.Media.WorkingPairs.Parametrizations.Utilities.ddexponential1_dT_dT(
      T=T_adsorpt,
      a=b_ref,
      b=0,
      c=0,
      d=E);
    ddc_dT_dT[3] :=SorpLib.Media.WorkingPairs.Parametrizations.Utilities.ddlinear1_dT_dT(
      T=T_adsorpt,
      a=t,
      b=0,
      c=alpha,
      d=1);
  end calc_coefficients;
  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial package describes the adsorption equilibrium of the working pair 
H<sub>2</sub>0 & silica gel using the Toth isotherm model according to Wang 
and Douglas LeVan (2010). Packages that inherit properties from this partial 
package may redeclare the package <i>MediumSpecificFunctions</i>, the model 
<i>Sorbent</i>, and the constant <i>twoPhaseAdsorptive</i>.
</p>

<h4>References</h4>
<ul>
  <li>
  Wang, Y. and Douglas LeVan, M. (2009). Adsorption Equilibrium of Carbon Dioxide and Water Vapor on Zeolites 5A and 13X and Silica Gel: Pure Components, Journal of Chemical & Engineering Data, 54:2839-2844. DOI: https://doi.org/10.1021/je800900a.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end SilicaGel_Toth_WangDouglasLeVan2009;
