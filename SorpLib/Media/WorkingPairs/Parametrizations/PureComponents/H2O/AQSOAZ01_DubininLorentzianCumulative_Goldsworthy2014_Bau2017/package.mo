within SorpLib.Media.WorkingPairs.Parametrizations.PureComponents.H2O;
package AQSOAZ01_DubininLorentzianCumulative_Goldsworthy2014_Bau2017 "H2O & AQSOA-Z01 via the Dubinin isotherm model with a Lorentzian Cumulative according to measurment data from Goldsworty (2014) and fit from Bau (2017)"
  extends
    SorpLib.Media.WorkingPairs.Interfaces.PartialPureParametrizationDubinin(
    final M_adsorptive=18.0153/1000,
    final no_coefficients=6,
    redeclare final package IsothermModel =
      SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative,
    redeclare replaceable package MediumSpecificFunctions =
      SorpLib.Media.WorkingPairs.Parametrizations.PureComponents.MediumSpecificFunctions.VLE);

  //
  // Definition of further constants
  //
  constant SorpLib.Units.FilledPoreVolume char_curve_a = 0.204162691322846 / 1000
    "First contants of the isotherm model";
  constant Real char_curve_b(unit="J/mol") = 1.714393556857407e2 * 1000 * M_adsorptive
    "Second contants of the isotherm model";
  constant Real char_curve_c(unit="J/mol") = -13.888991966535533 * 1000 * M_adsorptive
    "Third contants of the isotherm model";
  constant SorpLib.Units.FilledPoreVolume char_curve_d = 0.005500438155033 / 1000
    "Fourth contants of the isotherm model";

  //
  // Redeclare functions
  //
  redeclare final function extends calc_c
    "Calculates temperature-dependent coefficients of the isotherm model"
  algorithm
    c[1] := MediumSpecificFunctions.p_sat_T(
       T=T_adsorpt);
    c[2] := MediumSpecificFunctions.rho_satLiq_T(
       T=T_adsorpt);
    c[3] :=char_curve_a;
    c[4] :=char_curve_b;
    c[5] :=char_curve_c;
    c[6] :=char_curve_d;
  end calc_c;

  redeclare final function extends calc_coefficients
    "Calculates temperature-dependent coefficients and their the partial derivatives w.r.t. temperature"
  algorithm
    //
    // Get medium-specific functions (p_sat, rho_satLiq, and their partial
    // derivatives w.r.t. temperature)
    //
    (c[1],dc_dT[1],ddc_dT_dT[1],c[2],dc_dT[2],ddc_dT_dT[2]) :=
      MediumSpecificFunctions.pRho_satLiq(T=T_adsorpt, dT=dT)
      "Calculates saturated vapor pressure, density and bubble point, and their
      first- and secon-order partial derivatives w.r.t. temperature)";

    //
    // Calculate further coefficients
    //
    c[3] :=char_curve_a;
    c[4] :=char_curve_b;
    c[5] :=char_curve_c;
    c[6] :=char_curve_d;

    //
    // Calculate further partial derivatives of the coefficients w.r.t.
    // temperature
    //
    dc_dT[3] :=0;
    dc_dT[4] :=0;
    dc_dT[5] :=0;
    dc_dT[6] :=0;

    //
    // Calculate further second-order partial derivatives of the coefficients
    // w.r.t. temperature
    //
    ddc_dT_dT[3] :=0;
    ddc_dT_dT[4] :=0;
    ddc_dT_dT[5] :=0;
    ddc_dT_dT[6] :=0;
  end calc_coefficients;
  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial package describes the adsorption equilibrium of the working pair 
H<sub>2</sub>0 & AQSOA-Z01 (FAM-Z01) using the Dubinin isotherm model with a Lorentzian 
Cumulative characteristic curve generated by Bau (2017) using measurement data from
Goldsworthy (2014). Packages that inherit properties from this partial package 
may redeclare the package <i>MediumSpecificFunctions</i> and the model <i>Sorbent</i>.
</p>

<h4>References</h4>
<ul>
  <li>
  Goldsworthy (2014). Measurements of water vapour sorption isotherms for RD silica gel, AQSOA-Z01, AQSOA-Z02, AQSOA-Z05 and CECA zeolite 3A, Microporous and Mesoporous Materials, 196:59-67. DOI: http://dx.doi.org/10.1016/j.micromeso.2014.04.046.
  </li>
  <li>
  Bau (2018). From Dynamic Simulation to Optimal Design and Control of Adsorption Energy Systems, PhD thesis. DOI: https://doi.org/10.18154/RWTH-2018-222524.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  Revision after restructering of the library.
  </li>
  <li>
  December 14, 2020, by Mirko Engelpracht:<br/>
  Revision after restructering of the library.
  </li>
  <li>
  November 17, 2017, by Uwe Bau:<br/>
  Tidy up implementation and enhance documentation for publication of library.
  </li>
</ul>
</html>"));
end AQSOAZ01_DubininLorentzianCumulative_Goldsworthy2014_Bau2017;
