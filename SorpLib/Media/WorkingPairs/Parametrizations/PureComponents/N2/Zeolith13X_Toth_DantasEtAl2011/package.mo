﻿within SorpLib.Media.WorkingPairs.Parametrizations.PureComponents.N2;
package Zeolith13X_Toth_DantasEtAl2011 "N2 & Zeolith 13X via the Toth isotherm model according to Dantas et al. (2011)"
  extends
    SorpLib.Media.WorkingPairs.Interfaces.PartialPureParametrizationNonDubinin(
    final M_adsorptive=28.0134/1000,
    twoPhaseAdsorptive=false,
    final no_coefficients=3,
    redeclare final package IsothermModel =
      SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Toth,
    redeclare replaceable package MediumSpecificFunctions =
      SorpLib.Media.WorkingPairs.Parametrizations.PureComponents.MediumSpecificFunctions.Gas);

  //
  // Definition of further constants
  //
  constant SorpLib.Units.Uptake x_sat = 3.08 * M_adsorptive
    "First contants of the isotherm model";
  constant Real b_ref(unit="1/Pa") = 0.882e-4 / (1e5)
    "Second contants of the isotherm model";
  constant Real Q_star(unit="K") = -17.19e3 / Modelica.Constants.R
    "Third contants of the isotherm model";
  constant Real t(unit="1") = 0.869
    "Fourth contants of the isotherm model";

  //
  // Redeclare functions
  //
  redeclare final function extends calc_c
    "Calculates temperature-dependent coefficients of the isotherm model"
  algorithm
    c[1] :=x_sat;
    c[2] :=SorpLib.Media.WorkingPairs.Parametrizations.Utilities.exponential1(
      T=T_adsorpt,
      a=b_ref,
      b=0,
      c=0,
      d=-Q_star);
    c[3] :=t;
  end calc_c;

  redeclare final function extends calc_coefficients
    "Calculates temperature-dependent coefficients and their the partial derivatives w.r.t. temperature"
  algorithm
    //
    // Calculate coefficients
    //
    c[1] :=x_sat;
    c[2] :=SorpLib.Media.WorkingPairs.Parametrizations.Utilities.exponential1(
      T=T_adsorpt,
      a=b_ref,
      b=0,
      c=0,
      d=-Q_star);
    c[3] :=t;

    //
    // Calculate partial derivatives of the coefficients w.r.t. temperature
    //
    dc_dT[1] :=0;
    dc_dT[2] :=
    SorpLib.Media.WorkingPairs.Parametrizations.Utilities.dexponential1_dT(
      T=T_adsorpt,
      a=b_ref,
      b=0,
      c=0,
      d=-Q_star);
    dc_dT[3] :=0;

    //
    // Calculate second-order partial derivatives of the coefficients w.r.t.
    // temperature
    //
    ddc_dT_dT[1] :=0;
    ddc_dT_dT[2] :=
    SorpLib.Media.WorkingPairs.Parametrizations.Utilities.ddexponential1_dT_dT(
      T=T_adsorpt,
      a=b_ref,
      b=0,
      c=0,
      d=-Q_star);
    ddc_dT_dT[3] :=0;
  end calc_coefficients;
  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial package describes the adsorption equilibrium of the working pair 
N<sub>2</sub> & Zeolith 13X using the Toth isotherm model according to Dantas 
et al. (2011). Packages that inherit properties from this partial package may 
redeclare the package <i>MediumSpecificFunctions</i>, the model <i>Sorbent</i>,
and the constant <i>twoPhaseAdsorptive</i>.
</p>

<h4>References</h4>
<ul>
  <li>
  Dantas, T.L.P. and Luna, F.M.T. and Silva Jr., I.J. and Torres, A.E.B. and Azevedo, D.C.S. and Rodrigues, A.E. and Moreira, R.F.P.M. (2011). Carbon dioxide–nitrogen separation through pressure swing adsorption, Chemical Engineering Journal, 172:698-704. DOI: https://doi.org/10.1016/j.cej.2011.06.037.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Zeolith13X_Toth_DantasEtAl2011;
