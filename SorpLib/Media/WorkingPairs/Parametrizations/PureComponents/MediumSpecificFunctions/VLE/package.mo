within SorpLib.Media.WorkingPairs.Parametrizations.PureComponents.MediumSpecificFunctions;
package VLE "Medium-specific functions using a real fluid with a two-phase regime"

  //
  // Definition of replaceable medium package: This is required to have access to
  // functions that change with the selected medium.
  //
  replaceable package Medium = Modelica.Media.Water.StandardWater
    constrainedby Modelica.Media.Interfaces.PartialTwoPhaseMedium
    "Medium (i.e., adsorptive)"
    annotation (Dialog(tab="General",group="Medium"),
                choicesAllMatching = true);
  //
  // Inherit from base class and finalize it
  //
  extends
  SorpLib.Media.WorkingPairs.Interfaces.PartialPureMediumSpecificFunctions;

  //
  // Redeclare models
  //
  redeclare final model extends AdsorptiveProperties
    "Calculates all properties of the adsorptive at gas/vapor phase required for the working pair model at once"

    //
    // Definition of state records
    //
protected
    Medium.SaturationProperties sat_T
      "Saturated state properties at T";
    Medium.SaturationProperties sat_T_pdT
      "Saturated state properties at T + dT";
    Medium.SaturationProperties sat_T_mdT
      "Saturated state properties at T - dT";

    Medium.ThermodynamicState state
      "Thermodynamic properties at p and T or at p_sat(T) and T";
    Medium.ThermodynamicState state_bubble
      "Thermodynamic properties at bubble point calculated with T";

  equation
    //
    // Calculate state records
    //
    if adsorptiveAtDewPoint or
      require_dp_sat_dT or
      require_rho_satLiq_T or
      require_drho_satLiq_dT or
      require_h_adsorptiveToLiquid or
      require_cp_satLiq_T then
      sat_T = Medium.setSat_T(T=T)
      "Saturated state properties at T";

    else
      sat_T = Medium.setSat_T(T=Medium.reference_T)
        "Saturated state properties at reference temperature";

    end if;

    if require_ddrho_satLiq_dT_dT or
      (adsorptiveAtDewPoint and require_dv_adsorptive_dT_p) then
      sat_T_pdT = Medium.setSat_T(T=T + dT)
        "Saturated state properties at T + dT";
      sat_T_mdT = Medium.setSat_T(T=T - dT)
        "Saturated state properties at T - dT";

    else
      sat_T_pdT = Medium.setSat_T(T=Medium.reference_T)
        "Saturated state properties at reference temperature";
      sat_T_mdT = Medium.setSat_T(T=Medium.reference_T)
        "Saturated state properties at reference temperature";

    end if;

    state = if adsorptiveAtDewPoint then Medium.setDewState(sat=sat_T) else
      Medium.setState_pTX(p=p, T=T, X=Medium.reference_X)
      "Thermodynamic properties at p and T or at p_sat(T) and T";

    if require_h_adsorptiveToLiquid or
      require_dh_adsorptiveToLiquid_dT_p or
      require_cp_satLiq_T then
      state_bubble =  Medium.setBubbleState(sat=sat_T)
        "Thermodynamic properties at bubble point calculated with T";

    else
      state_bubble =  Medium.setBubbleState(sat=sat_T)
        "Thermodynamic properties at bubble point calculated at reference temperature";

    end if;

    //
    // State variables
    //
    // v = if adsorptiveAtDewPoint then 1/Medium.dewDensity(sat=sat_T) else
    //   1/Medium.density(state=state)
    //   "Specific volume";
    v = 1/Medium.density(state=state)
      "Specific volume";

    if calcCaloricProperties then
      // h = if adsorptiveAtDewPoint then Medium.dewEnthalpy(sat=sat_T) else
      //   Medium.specificEnthalpy(state=state)
      //   "Specific enthalpy";
      h = Medium.specificEnthalpy(state=state)
        "Specific enthalpy";
      u = h - p*v
        "Specific internal energy";

    else
      h = 0
        "Specific enthalpy";
      u = 0
        "Specific internal energy";

    end if;

    if calcCaloricProperties and calcEntropicProperties then
      // s = if adsorptiveAtDewPoint then Medium.dewEntropy(sat=sat_T) else
      //   Medium.specificEntropy(state=state)
      //   "Specific entropy";
      s = Medium.specificEntropy(state=state)
        "Specific entropy";
      g = h - T*s
        "Free enthalpy (i.e., Gibbs free energy)";
      a = u - T*s
        "Free energy (i.e., Helmholts free energy)";

    else
      s = 0
        "Specific entropy";
      g = 0
        "Free enthalpy (i.e., Gibbs free energy)";
      a = 0
        "Free energy (i.e., Helmholts free energy)";

    end if;

    //
    // Additional properties of the one-phase regime
    //
    cp = if not require_cp_adsorptive then 0 else
      Medium.specificHeatCapacityCp(state=state)
      "Specific heat capacity";

    //
    // Additional properties of the two-phase regime
    //
    p_sat =  if not require_p_sat then 0 else
      Medium.saturationPressure(T)
      "Saturated vapor pressure";
    v_satLiq =  if not require_rho_satLiq_T then 0 else
      1/Medium.bubbleDensity(sat=sat_T)
      "Specific volume at the bubble point at given temperature";

    h_adsorptiveToLiquid = if not require_h_adsorptiveToLiquid then 0 else
      h - Medium.specificEnthalpy(state=state_bubble)
      "Specific enthalpy of vaporization";

    cp_satLiq = if not require_cp_satLiq_T then 0 else
      Medium.specificHeatCapacityCp(state=state_bubble)
      "Specific heat capacity at the bubble point at given temeprature";

    //
    // Partial derivatives of the one-phase regime
    //
    dv_dp_T =  if not require_dv_adsorptive_dp_T then 0 else
      (if adsorptiveAtDewPoint then 0 else
      (1/Medium.density_pTX(p=p+dp, T=T, X=Medium.reference_X) -
      1/Medium.density_pTX(p=p-dp, T=T, X=Medium.reference_X))/(2*dp))
      "Partial derivative of the specific volume w.r.t. pressure at constant
    temperature";

    dv_dT_p =  if not require_dv_adsorptive_dT_p then 0 else
      (if adsorptiveAtDewPoint then
      (1/Medium.dewDensity(sat=sat_T_pdT) -
      1 /Medium.dewDensity(sat=sat_T_mdT))/(2*dT) else
      (1/Medium.density_pTX(p=p, T=T+dT, X=Medium.reference_X) -
      1/Medium.density_pTX(p=p, T=T-dT, X=Medium.reference_X))/(2*dT))
      "Partial derivative of the specific volume w.r.t. temperature at constant
    pressure";

    //
    // Partial derivatives of the two-phase regime
    //
    dp_sat_dT =  if not require_dp_sat_dT then 0 else
      1/Medium.saturationTemperature_derp_sat(sat=sat_T)
      "Partial derivative of saturated vapor pressure w.r.t. temperature";

    dv_satLiq_dT =  if not require_drho_satLiq_dT then 0 else
      -v_satLiq^2*Medium.dBubbleDensity_dPressure(sat=sat_T)* dp_sat_dT
      "Partial derivative of the specific volume at the bubble point at given
    temperature w.r.t. temperature";
    ddv_satLiq_dT_dT =  if not require_ddrho_satLiq_dT_dT then 0 else
      ((-Medium.dBubbleDensity_dPressure(sat=sat_T_pdT)/
      Medium.saturationTemperature_derp_sat(sat=sat_T_pdT)/Medium.bubbleDensity(
       sat=sat_T_pdT)^2 +
       Medium.dBubbleDensity_dPressure(sat=sat_T_mdT)/
      Medium.saturationTemperature_derp_sat(sat=sat_T_mdT)/Medium.bubbleDensity(
       sat=sat_T_mdT)^2)/(2*dT))
      "Second-order partial derivative of the specific volume at the bubble point 
    at given temperature w.r.t. temperature";

    dh_adsorptiveToLiquid_dp_T = if not require_dh_adsorptiveToLiquid_dp_T then 0
      else v * (1 - T * Medium.isobaricExpansionCoefficient(state=state)) - 0
      "Partial derivative of specific enthalpy of vaporization w.r.t. pressure
    at constant temperature";
    dh_adsorptiveToLiquid_dT_p = if not require_dh_adsorptiveToLiquid_dT_p then 0
      else
      Medium.specificHeatCapacityCp(state=state) -
      Medium.specificHeatCapacityCp(state=state_bubble)
      "Partial derivative of specific enthalpy of vaporization w.r.t. pressure
    at constant temperature";
  end AdsorptiveProperties;
  //
  // Redeclare functions of the two-phase regime
  //
  redeclare final function extends p_sat_T
    "Calculates the vapor pressure as function of temperature"
  algorithm
    p_sat :=Medium.saturationPressure(T)
      "Saturated vapor pressure";
  end p_sat_T;

  redeclare final function extends rho_satLiq_T
    "Calculates the density at the bubble point as function of temperature"
  algorithm
    rho_satLiq:=Medium.bubbleDensity(sat=Medium.setSat_T(T=T))
      "Density at bubble point";
  end rho_satLiq_T;

  redeclare final function extends pRho_satLiq
    "Calculates the vapor pressure, density at the bubble point, and their first- and second-order partial derivatives w.r.t. temperature"
    extends Modelica.Icons.Function;

    //
    // Definition of variables
    //
protected
    Medium.SaturationProperties sat_T
      "Saturated state properties at T";
    Medium.SaturationProperties sat_T_pdT
      "Saturated state properties at T + dT";
    Medium.SaturationProperties sat_T_mdT
      "Saturated state properties at T - dT";

  algorithm
    sat_T :=Medium.setSat_T(T=T)
      "Saturated state properties at T";
    sat_T_pdT :=Medium.setSat_T(T=T + dT)
      "Saturated state properties at T";
    sat_T_mdT :=Medium.setSat_T(T=T - dT)
      "Saturated state properties at T";

    p_sat := Medium.saturationPressure(T)
      "Saturated vapor pressure";
    dp_sat_dT:= 1 / Medium.saturationTemperature_derp_sat(sat=sat_T)
      "Partial derivative of the vapor pressure w.r.t. temperature";
    ddp_sat_dT_dT :=
      (1 / Medium.saturationTemperature_derp_sat(sat=sat_T_pdT) -
      1 / Medium.saturationTemperature_derp_sat(sat=sat_T_mdT)) / (2 * dT)
      "Calculates the second-order partial derivative of the vapor pressure w.r.t. 
      temperature";

    rho_satLiq:=Medium.bubbleDensity(sat=sat_T)
      "Density at bubble point";
    drho_satLiq_dT :=Medium.dBubbleDensity_dPressure(sat=sat_T) * dp_sat_dT
      "Calculates the partial derivative of the density at the bubble point w.r.t.
      temperature";
    ddrho_satLiq_dT_dT:=
      (Medium.dBubbleDensity_dPressure(sat=sat_T_pdT) /
      Medium.saturationTemperature_derp_sat(sat=sat_T_pdT) -
      Medium.dBubbleDensity_dPressure(sat=sat_T_mdT) /
      Medium.saturationTemperature_derp_sat(sat=sat_T_mdT)) / (2 * dT)
      "Calculates the second-order partial derivative of the density at the bubble 
    point w.r.t. temperature";
  end pRho_satLiq;
  //
  // Functions of the one-phase regime
  //
  redeclare final function extends h_pT
    "Calculates the specific enthalpy of the adsorptive as function of pressure
    and temperature"
  algorithm
    h := if p > p_lb then
      Medium.specificEnthalpy_pTX(p=p, T=T, X=Medium.reference_X) else
      h_ref + (T - T_ref) * Medium.specificHeatCapacityCp(state=
        Medium.setState_pTX(p=p_lb, T=T, X=Medium.reference_X))
      "Specific enthalpy";
  end h_pT;

  redeclare final function extends s_pT
    "Calculates the specific entropy of the adsorptive as function of pressure
    and temperature"
  algorithm
    s :=if p > p_lb then
      Medium.specificEntropy_pTX(p=p, T=T, X=Medium.reference_X) else
      s_ref - Modelica.Constants.R * log(p_lb / p_ref) + (T - T_ref) / T  *
      Medium.specificHeatCapacityCp(state=
        Medium.setState_pTX(p=p_lb, T=T, X=Medium.reference_X))
      "Specific entropy";
  end s_pT;
  //
  // Functions calculating properties required for calculating the specific heat
  // capacity of the adsorpt
  //
  redeclare final function extends calc_properties
    "Calculates all properties of the adsorptive at gas/vapor phase required for the working pair model at once"
    extends Modelica.Icons.Function;

    //
    // Definition of state records
    //
protected
    Medium.SaturationProperties sat_T
      "Saturated state properties at T";
    Medium.SaturationProperties sat_T_pdT
      "Saturated state properties at T + dT";
    Medium.SaturationProperties sat_T_mdT
      "Saturated state properties at T - dT";

    Medium.ThermodynamicState state
      "Thermodynamic properties at p and T or at p_sat(T) and T";
    Medium.ThermodynamicState state_bubble
      "Thermodynamic properties at bubble point calculated with T";

  algorithm
    //
    // Calculate state records
    //
    if adsorptiveAtDewPoint and (
      require_v_adsorptive or
      require_h_adsorptive or
      require_dh_adsorptive_dT_dp or
      require_s_adsorptive or
      require_h_adsorptiveToLiquid) or
      require_h_adsorptiveToLiquid then
      sat_T :=Medium.setSat_T(T=T)
      "Saturated state properties at T";
    end if;

    if adsorptiveAtDewPoint and
      (require_v_adsorptive) then
      sat_T_pdT :=Medium.setSat_T(T=T+dT)
        "Saturated state properties at T + dT";
      sat_T_mdT :=Medium.setSat_T(T=T-dT)
        "Saturated state properties at T - dT";
    end if;

    if require_v_adsorptive or
      require_h_adsorptive or
      require_dh_adsorptive_dT_dp or
      require_s_adsorptive or
      require_h_adsorptiveToLiquid then
      state := if adsorptiveAtDewPoint then
        Medium.setDewState(sat=sat_T) else
        Medium.setState_pTX(p=max(p,p_min), T=T, X=Medium.reference_X)
        "Thermodynamic properties at p and T or at p_sat(T) and T";
    end if;

    if require_h_adsorptiveToLiquid then
      state_bubble := Medium.setBubbleState(sat=sat_T)
        "Thermodynamic properties at bubble point calculated with T";
    end if;

    //
    // Calculate specific volume
    //
    if require_h_adsorptiveToLiquid then
      v := 1 / Medium.density(state=state)
        "Specific volume";

    elseif require_v_adsorptive then
      // v := if adsorptiveAtDewPoint then 1 / Medium.dewDensity(sat=sat_T) else
      //   1 / Medium.density_pTX(p=p, T=T, X=Medium.reference_X)
      //   "Specific volume";
      v := 1 / Medium.density(state=state)
        "Specific volume";

    else
      v :=0
        "Specific volume";

    end if;

    if require_v_adsorptive then
      if adsorptiveAtDewPoint then
        dv_dp_T :=0
          "Partial derivative of the specific volume w.r.t. pressure at constant
        temperature: Corresponds to unity as dew state is determined by temperature";
        dv_dT_p :=
          (1 / Medium.dewDensity(sat=sat_T_pdT) -
          1 / Medium.dewDensity(sat=sat_T_mdT)) / (2 * dT)
          "Partial derivative of the specific volume w.r.t. temperature at constant
        pressure";

      else
        dv_dp_T := if p > p_min then
          (1 / Medium.density_pTX(p=p+dp, T=T, X=Medium.reference_X) -
          1 / Medium.density_pTX(p=p-dp, T=T, X=Medium.reference_X)) / (2 * dp) else
          (1 / Medium.density_pTX(p=p_min+dp, T=T, X=Medium.reference_X) -
          1 / Medium.density_pTX(p=p_min, T=T, X=Medium.reference_X)) / (2 * dp)
          "Partial derivative of the specific volume w.r.t. pressure at constant
        temperature";
        dv_dT_p :=
          (1 / Medium.density_pTX(p=max(p,p_min), T=T+dT, X=Medium.reference_X) -
          1 / Medium.density_pTX(p=max(p,p_min), T=T-dT, X=Medium.reference_X)) /
          (2 * dT)
          "Partial derivative of the specific volume w.r.t. temperature at constant
        pressure";

      end if;

    else
      dv_dp_T :=0
        "Partial derivative of the specific volume w.r.t. pressure at constant
      temperature";
      dv_dT_p :=0
        "Partial derivative of the specific volume w.r.t. temperature at constant
      pressure";

    end if;

    //
    // Calculate specific enthalpy
    //
    h := if not require_h_adsorptive then 0 else
      Medium.specificEnthalpy(state=state)
      "Specific enthalpy";

    dh_dT_p := if not require_dh_adsorptive_dT_dp then 0 else
      Medium.specificHeatCapacityCp(state=state)
      "Partial derivative of the specific enthalpy w.r.t. temperature at constant
    pressure";

    //
    // Calculate specific enthalpy
    //
    s := if not require_s_adsorptive then 0 else s_pT(
      p=p,
      T=T,
      p_lb=p_min,
      s_ref=Medium.specificEntropy(state=state),
      p_ref=p_min,
      T_ref=T)
      "Specific entropy";

    //
    // Calculate specific enthalpy difference between adsorptive phase and bubble phase
    //
    if require_h_adsorptiveToLiquid then
      h_atl :=
        Medium.specificEnthalpy(state=state) -
        Medium.specificEnthalpy(state=state_bubble)
        "Specific enthalpy difference between adsorptive state and saturated liquid
      state (i.e., bubble point)";
      dh_atl_dp := v * (1 - T *
        Medium.isobaricExpansionCoefficient(state=state)) - 0
        "Partial derivative of specific enthalpy difference between adsorptive state 
      and saturated liquid state (i.e., bubble point) w.r.t. pressure at constant
      temperature";
      dh_atl_dT :=
        Medium.specificHeatCapacityCp(state=state) -
        Medium.specificHeatCapacityCp(state=state_bubble)
        "Partial derivative of specific enthalpy difference between adsorptive state 
      and saturated liquid state (i.e., bubble point) w.r.t. temperature at constant
      pressure";

    else
      h_atl :=0
        "Specific enthalpy difference between adsorptive state and saturated liquid
      state (i.e., bubble point)";
      dh_atl_dp :=0
        "Partial derivative of specific enthalpy difference between adsorptive state 
      and saturated liquid state (i.e., bubble point) w.r.t. pressure at constant
      temperature";
      dh_atl_dT :=0
        "Partial derivative of specific enthalpy difference between adsorptive state 
      and saturated liquid state (i.e., bubble point) w.r.t. temperature at constant
      pressure";

    end if;

  end calc_properties;
  //
  // Annotations
  //
  annotation (Documentation(revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>", info="<html>
<p>
This package provides media-specific functions for a real fluid. The term 
\"real\" is used to indicate that the fluid has a two-phase regime. Such fluids 
are necessary, for example, when using the model of Dubinin as the isotherm 
model.
</p>
</html>"));
end VLE;
