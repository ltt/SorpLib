within SorpLib.Media.WorkingPairs.Parametrizations.PureComponents.MediumSpecificFunctions;
package Gas "Medium-specific functions using a fluid with just one regime (i.e., gas/vapor)"

  //
  // Definition of replaceable medium package: This is required to have access to
  // functions that change with the selected medium.
  //
  replaceable package Medium = Modelica.Media.IdealGases.SingleGases.H2O
    constrainedby Modelica.Media.IdealGases.Common.SingleGasNasa
    "Medium (i.e., adsorptive)"
    annotation (Dialog(tab="General",group="Medium"),
                choicesAllMatching = true);
  //
  // Inherit from base class and finalize it
  //
  extends
  SorpLib.Media.WorkingPairs.Interfaces.PartialPureMediumSpecificFunctions;

  //
  // Redeclare models
  //
  redeclare final model extends AdsorptiveProperties
    "Calculates all properties of the adsorptive at gas/vapor phase required for the working pair model at once"

    //
    // Definition of state records
    //
protected
    Medium.ThermodynamicState state
      "Thermodynamic properties at p and T or at p_sat(T) and T";

  equation
    //
    // Calculate state records
    //
    if adsorptiveAtDewPoint or
      require_dp_sat_dT or
      require_rho_satLiq_T or
      require_drho_satLiq_dT or
      require_h_adsorptiveToLiquid or
      require_cp_satLiq_T or
      require_dh_adsorptiveToLiquid_dp_T or
      require_dh_adsorptiveToLiquid_dT_p or
      require_ddrho_satLiq_dT_dT or
      (adsorptiveAtDewPoint and require_dv_adsorptive_dT_p) then
      assert(false,
        "Selected adsorptive medium model does not have a two-phase regime. " +
        "Cannot calculate state properties of bubble or dew point at T_adsorpt." +
        "Thus, the specific volume or heat capacity of the adsorpt at the bubble " +
        "point can also not be calculated.",
        level = AssertionLevel.error);
    end if;

    state = Medium.setState_pTX(p=p, T=T, X=Medium.reference_X)
      "Thermodynamic properties at p and T or at p_sat(T) and T";

    //
    // State variables
    //
    v = 1/Medium.density(state=state)
      "Specific volume";

    if calcCaloricProperties then
      h = Medium.specificEnthalpy(state=state)
        "Specific enthalpy";
      u = h - p*v
        "Specific internal energy";

    else
      h = 0
        "Specific enthalpy";
      u = 0
        "Specific internal energy";

    end if;

    if calcCaloricProperties and calcEntropicProperties then
      s = Medium.specificEntropy(state=state)
        "Specific entropy";
      g = h - T*s
        "Free enthalpy (i.e., Gibbs free energy)";
      a = u - T*s
        "Free energy (i.e., Helmholts free energy)";

    else
      s = 0
        "Specific entropy";
      g = 0
        "Free enthalpy (i.e., Gibbs free energy)";
      a = 0
        "Free energy (i.e., Helmholts free energy)";

    end if;

    //
    // Additional properties of the one-phase regime
    //
    cp = if not require_cp_adsorptive then 0 else
      Medium.specificHeatCapacityCp(state=state)
      "Specific heat capacity";

    //
    // Additional properties of the two-phase regime
    //
    p_sat =  0
      "Saturated vapor pressure";
    v_satLiq =  0
      "Specific volume at the bubble point at given temperature";

    h_adsorptiveToLiquid =  0
      "Specific enthalpy of vaporization";

    cp_satLiq = 0
      "Specific heat capacity at the bubble point at given temeprature";

    //
    // Partial derivatives of the one-phase regime
    //
    dv_dp_T =  if not require_dv_adsorptive_dp_T then 0 else
      (1/Medium.density_pTX(p=p + dp, T=T, X=Medium.reference_X) -
      1/Medium.density_pTX(p=p - dp, T=T, X=Medium.reference_X))/(2*dp)
      "Partial derivative of the specific volume w.r.t. pressure at constant
    temperature";

    dv_dT_p =  if not require_dv_adsorptive_dT_p then 0 else
      (1/Medium.density_pTX(p=p, T=T + dT, X=Medium.reference_X) -
      1/Medium.density_pTX(p=p, T=T - dT, X=Medium.reference_X))/(2*dT)
      "Partial derivative of the specific volume w.r.t. temperature at constant
    pressure";

    //
    // Partial derivatives of the two-phase regime
    //
    dp_sat_dT = 0
      "Partial derivative of saturated vapor pressure w.r.t. temperature";

    dv_satLiq_dT = 0
      "Partial derivative of the specific volume at the bubble point at given
    temperature w.r.t. temperature";
    ddv_satLiq_dT_dT = 0
      "Second-order partial derivative of the specific volume at the bubble point 
    at given temperature w.r.t. temperature";

    dh_adsorptiveToLiquid_dp_T = 0
      "Partial derivative of specific enthalpy of vaporization w.r.t. pressure
    at constant temperature";
    dh_adsorptiveToLiquid_dT_p = 0
      "Partial derivative of specific enthalpy of vaporization w.r.t. pressure
    at constant temperature";
  end AdsorptiveProperties;
  //
  // Redeclare functions of the two-phase regime
  //
  redeclare final function extends p_sat_T
    "Calculates the vapor pressure as function of temperature (i.e., not supported)"
  algorithm
    p_sat :=0
      "Saturated vapor pressure: Dummy value";
  end p_sat_T;

  redeclare final function extends rho_satLiq_T
    "Calculates the density at the bubble point as function of temperature (i.e., not supported)"
  algorithm
    rho_satLiq:=0
      "Density at bubble point: Dummy value";
  end rho_satLiq_T;

  redeclare final function extends pRho_satLiq
    "Calculates the vapor pressure, density at the bubble point, and their first- and second-order partial derivatives w.r.t. temperature (i.e., not supported)"
    extends Modelica.Icons.Function;
  algorithm
    p_sat := 0
      "Saturated vapor pressure: Dummy value";
    dp_sat_dT:= 0
      "Partial derivative of the vapor pressure w.r.t. temperature: Dummy value";
    ddp_sat_dT_dT :=0
      "Calculates the second-order partial derivative of the vapor pressure w.r.t. 
      temperature: Dummy value";

    rho_satLiq:=0
      "Density at bubble point: Dummy value";
    drho_satLiq_dT :=0
      "Calculates the partial derivative of the density at the bubble point w.r.t.
      temperature: Dummy value";
    ddrho_satLiq_dT_dT:=0
      "Calculates the second-order partial derivative of the density at the bubble 
    point w.r.t. temperature: Dummy value";
  end pRho_satLiq;
  //
  // Functions of the one-phase regime
  //
  redeclare final function extends h_pT
    "Calculates the specific enthalpy of the adsorptive as function of pressure
    and temperature"
  algorithm
    h := if p > p_lb then
      Medium.specificEnthalpy_pTX(p=p, T=T, X=Medium.reference_X) else
      h_ref + (T - T_ref) * Medium.specificHeatCapacityCp(state=
        Medium.setState_pTX(p=p_lb, T=T, X=Medium.reference_X))
      "Specific enthalpy";
  end h_pT;

  redeclare final function extends s_pT
    "Calculates the specific entropy of the adsorptive as function of pressure
    and temperature"
  algorithm
    s :=if p > p_lb then
      Medium.specificEntropy_pTX(p=p, T=T, X=Medium.reference_X) else
      s_ref - Modelica.Constants.R * log(p_lb / p_ref) + (T - T_ref) / T  *
      Medium.specificHeatCapacityCp(state=
        Medium.setState_pTX(p=p_lb, T=T, X=Medium.reference_X))
      "Specific entropy";
  end s_pT;
  //
  // Functions calculating properties required for calculating the specific heat
  // capacity of the adsorpt
  //
  redeclare final function extends calc_properties
    "Calculates all properties of the adsorptive at gas/vapor phase required for the working pair model at once"
    extends Modelica.Icons.Function;

    //
    // Definition of state records
    //
protected
    Medium.ThermodynamicState state
      "Thermodynamic properties at p and T";

  algorithm
    //
    // Calculate state records
    //
    if adsorptiveAtDewPoint or require_h_adsorptiveToLiquid then
      assert(false,
        "Selected adsorptive medium model does not have a two-phase regime. " +
        "Cannot calculate state properties of bubble or dew point at T_adsorpt.",
        level = AssertionLevel.error);
    end if;

    if require_v_adsorptive or
      require_h_adsorptive or
      require_dh_adsorptive_dT_dp or
      require_s_adsorptive then
      state := Medium.setState_pTX(p=max(p,p_min), T=T, X=Medium.reference_X)
        "Thermodynamic properties at p and T";

    end if;

    //
    // Calculate specific volume
    //
    if require_v_adsorptive then
      v := 1 / Medium.density(state=state)
        "Specific volume";
      dv_dp_T := if p > p_min then
        (1 / Medium.density_pTX(p=p+dp, T=T, X=Medium.reference_X) -
        1 / Medium.density_pTX(p=p-dp, T=T, X=Medium.reference_X)) / (2 * dp) else
        (1 / Medium.density_pTX(p=p_min+dp, T=T, X=Medium.reference_X) -
        1 / Medium.density_pTX(p=p_min, T=T, X=Medium.reference_X)) / (2 * dp)
        "Partial derivative of the specific volume w.r.t. pressure at constant
      temperature";
      dv_dT_p :=
        (1 / Medium.density_pTX(p=max(p,p_min), T=T+dT, X=Medium.reference_X) -
        1 / Medium.density_pTX(p=max(p,p_min), T=T-dT, X=Medium.reference_X)) /
        (2 * dT)
        "Partial derivative of the specific volume w.r.t. temperature at constant
      pressure";

    else
      v :=0
        "Specific volume";
      dv_dp_T :=0
        "Partial derivative of the specific volume w.r.t. pressure at constant
      temperature";
      dv_dT_p :=0
        "Partial derivative of the specific volume w.r.t. temperature at constant
      pressure";

    end if;

    //
    // Calculate specific enthalpy
    //
    h := if not require_h_adsorptive then 0 else
      Medium.specificEnthalpy(state=state)
      "Specific enthalpy";

    dh_dT_p := if not require_dh_adsorptive_dT_dp then 0 else
      Medium.specificHeatCapacityCp(state=state)
      "Partial derivative of the specific enthalpy w.r.t. temperature at constant
    pressure";

    //
    // Calculate specific enthalpy
    //
    s := if not require_s_adsorptive then 0 else s_pT(
      p=p,
      T=T,
      p_lb=p_min,
      s_ref=Medium.specificEntropy(state=state),
      p_ref=p_min,
      T_ref=T)
      "Specific entropy";

    //
    // Calculate specific enthalpy difference between adsorptive phase and bubble
    // phase: Cannot be calculated for ideal gas, so set dummy values
    //
    h_atl :=0
      "Specific enthalpy difference between adsorptive state and saturated liquid
    state (i.e., bubble point)";
    dh_atl_dp :=0
      "Partial derivative of specific enthalpy difference between adsorptive state 
    and saturated liquid state (i.e., bubble point) w.r.t. pressure at constant
    temperature";
    dh_atl_dT :=0
      "Partial derivative of specific enthalpy difference between adsorptive state 
    and saturated liquid state (i.e., bubble point) w.r.t. temperature at constant
    pressure";

  end calc_properties;
  //
  // Annotations
  //
  annotation (Documentation(revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>", info="<html>
<p>
This package provides media-specific functions for a fluid with just one regime,
i.e., the gas or vapor phase. Thus, this fluid does not support functions of
the two-phase regime and cannot be used for isotherm models based on the model
of Dubinin.
</p>
</html>"));
end Gas;
