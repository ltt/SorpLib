within SorpLib.Media.WorkingPairs.Parametrizations.PureComponents;
package MediumSpecificFunctions "Package containing packages with medium-specific functions using the Modelica standard library"
  extends Modelica.Icons.InternalPackage;

  annotation (Documentation(info="<html>
<p>
This package contains packages that provide media-specific functions. The 
provided packages are required to fully parameterize working pair models.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end MediumSpecificFunctions;
