within SorpLib.Media.WorkingPairs.Parametrizations.PureComponents;
package CO2 "Package containing parametrizations for CO2 as adsorptive"
  extends Modelica.Icons.VariantsPackage;

  annotation (Documentation(info="<html>
<p>
This package contains parametrizations for CO<sub>2</sub> as adsorptive.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end CO2;
