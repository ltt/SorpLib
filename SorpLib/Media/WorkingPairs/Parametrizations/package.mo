within SorpLib.Media.WorkingPairs;
package Parametrizations "Parametrized working pair models without fixed media models"
  extends Modelica.Icons.VariantsPackage;

  annotation (Documentation(info="<html>
<p>
This package contains parameterized working models. For this purpose, the interfaces 
of the pure and multi-component working pair models are extended by the parameterization 
of the isotherm model. Furthermore, the <i>MediumSpecificFunctions</i> package has been
selected but can still be replaced. Note that this is a design decision to enable using 
different Modelica libraries for the fluid property calculation, e.g., the open-source 
Modelica Standard Library (MSL) or the commercial library TILMedia.
</p>

<h4>Structure</h4>
<p>
The package 
<a href=\"Modelica://SorpLib.Media.WorkingPairs.Parametrizations.Utilities\">SorpLib.Media.WorkingPairs.Parametrizations.Utilities</a>
contains auxiliary functions for calculating temperature-dependent coefficients of 
the isotherm model. The <i>PureComponents</i> and <i>MultiComponents</i> packages 
contain parameterized pure component or multi-component working pair models. In each 
package, a separate package is provided for each available adsorptive or adsorptive 
mixture, sorted alphabetically. The naming convention for the parameterized working 
pair models is as follows
</p>
<pre>
    NameSorbent_NameIsothermModel_NameAuthorsYear;
</pre>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end Parametrizations;
