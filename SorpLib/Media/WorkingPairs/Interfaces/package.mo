within SorpLib.Media.WorkingPairs;
package Interfaces "Interfaces for working pair models"
extends Modelica.Icons.InterfacesPackage;

annotation (Documentation(info="<html>
<p>
This package provides definitions of basic interfaces for working pair models. 
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Interfaces;
