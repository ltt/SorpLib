within SorpLib.Media.WorkingPairs.Interfaces;
partial package PartialPureParametrizationNonDubinin "Base package defining all properties for parametrization of pure component working pairs not based on the Dubinin model"
  extends SorpLib.Media.WorkingPairs.Interfaces.PartialPureParametrization(
    final modelOfDubinin = false,
    redeclare replaceable package IsothermModel =
      SorpLib.Media.Functions.SorptionEquilibria.Interfaces.PartialPureComponents
      constrainedby
    SorpLib.Media.Functions.SorptionEquilibria.Interfaces.PartialPureComponents);
  //
  // Redeclare functions
  //
  redeclare final function extends calc_h_ads_Dubinin
    "Molar adsorption enthalpy according to the model of Dubinin (i.e., not required)"
  algorithm
    h_ads := 0
      "Specific enthalpy of adsorption: Dummy value";
  end calc_h_ads_Dubinin;

  redeclare final function extends calc_dh_ads_dp_T_Dubinin
    "Calculates the partial derivative of the molar adsorption enthalpy w.r.t. pressure at constant temperature according to the model of Dubinin (i.e., not required)"
  algorithm
    dh_ads_dp := 0
      "Partial derivative of molar adsorption potential w.r.t. pressure at
     constant temperature: Dummy value";
  end calc_dh_ads_dp_T_Dubinin;

  redeclare final function extends calc_dh_ads_dT_p_Dubinin
    "Calculates the partial derivative of the molar adsorption enthalpy w.r.t. temperature at constant pressure according to the model of Dubinin (i.e., not required)"
  algorithm
    dh_ads_dT := 0
      "Partial derivative of molar adsorption potential w.r.t. temperature at
    constant pressure: Dummy value";
  end calc_dh_ads_dT_p_Dubinin;

  redeclare final function extends calc_integrand_WaltonLeVan_Dubinin
  "Calculates the integrand required for calculating the specific heat capacity according to Walton and Le Van (2005) when using the specifc enthalpy of adsorption according to Dubinin (i.e., not required)"
  algorithm
    y := 0
      "Integrand: Partial derivative of molar sorption enthalpy w.r.t. temperature
    at constant uptake: Dummy value";
  end calc_integrand_WaltonLeVan_Dubinin;

  redeclare final function extends calc_integrand_SchwambergerSchmidt_Dubinin
  "Calculates the integrand required for calculating the specific heat capacity according to Schwamberger and Schmidt (2013) when using the specifc enthalpy of adsorption according to Dubinin (i.e., not required)"
  algorithm
    y := 0
      "Integrand: Partial derivative of molar sorption enthalpy w.r.t. temperature
    at constant uptake: Dummy value";
  end calc_integrand_SchwambergerSchmidt_Dubinin;

  redeclare final function extends calc_integrand_h_avg_adsorpt_Dubinin
  "Calculates the integrand required for calculating the uptake-averaged specific enthalpy of adsorpt using the adsorption enthalpy according to Dubinin (i.e., not required)"
  algorithm
    y := 0
      "Integrand: Specific enthalpy difference between the adsorptive and adsorpt: 
      Dummy value";
  end calc_integrand_h_avg_adsorpt_Dubinin;

  redeclare final function extends calc_integrand_s_avg_adsorpt_Dubinin
  "Calculates the integrand required for calculating the uptake-averaged specific entropy of adsorpt using the adsorption enthalpy according to Dubinin (i.e., not required)"
  algorithm
    y := 0
      "Integrand: Specific entropy difference between the adsorptive and adsorpt: 
      Dummy value";
  end calc_integrand_s_avg_adsorpt_Dubinin;

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial package contains all declaration for parametrization of a pure component working
pair not based on the model of Dubinin. This means that constants, packages, models, and functions 
are defined that every pure component working pair must support. Packages that inherit properties 
from this partial package must redeclare all constants except of <i>modelOfDubinin</i>, the two 
partial packages <i>IsothermModel</i> and <i>MediumSpecificFunctions</i>, the model <i>Sorbent</i>, 
and the two functions <i>calc_c</i> and <i>calc_coefficients</i>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end PartialPureParametrizationNonDubinin;
