within SorpLib.Media.WorkingPairs.Interfaces;
partial package PartialPureParametrizationDubinin "Base package defining all properties for parametrization of pure component working pairs based on the Dubinin model"
  extends SorpLib.Media.WorkingPairs.Interfaces.PartialPureParametrization(
    final twoPhaseAdsorptive = true,
    final modelOfDubinin = true,
    redeclare replaceable package IsothermModel =
      SorpLib.Media.Functions.SorptionEquilibria.Interfaces.PartialPureComponentsDubinin
      constrainedby
    SorpLib.Media.Functions.SorptionEquilibria.Interfaces.PartialPureComponentsDubinin);
  //
  // Redeclare functions
  //
  redeclare final function extends calc_h_ads_Dubinin
    "Molar adsorption enthalpy according to the model of Dubinin"
  algorithm
    //
    // Calculate additional properties
    //
    dW_dA :=IsothermModel.dW_dA(
      A=A,
      c=c)
      "Partial derivative of the characteristic curve w.r.t. the molar adsorption
      potential at constant pressure and temperaure";

    //
    // Calculate specific enthalpy of adsorption
    //
    h_ads := 1 / M_adsorptive *
      SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.h_ads_Dubinin(
      M_adsorptive=M_adsorptive,
      T_adsorpt=T_adsorpt,
      x_adsorpt=x_adsorpt,
      h_adsorptiveToLiquid=properties_adsorptive.h_adsorptiveToLiquid,
      v_adsorpt=v_adsorpt,
      beta_adsorpt=beta_adsorpt,
      A=A,
      dW_dA=dW_dA)
      "Specific enthalpy of adsorption";
  end calc_h_ads_Dubinin;

  redeclare final function extends calc_dh_ads_dp_T_Dubinin
    "Calculates the partial derivative of the molar adsorption enthalpy w.r.t. pressure at constant temperature according to the model of Dubinin"
  algorithm
    //
    // Calculate additional properties
    //
    dW_dA :=IsothermModel.dW_dA(
      A=A,
      c=c)
      "Partial derivative of the characteristic curve w.r.t. the molar adsorption
      potential at constant pressure and temperaure";
    ddW_dA_dA := IsothermModel.ddW_dA_dA(
      A=A,
      c=c)
      "Second-order partial derivative of filled pore volume w.r.t. molar 
      adsorption potential and temperature at constant pressure and temperature";

    //
    // Calculate specific enthalpy of adsorption
    //
    dh_ads_dp := 1 / M_adsorptive *
      SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.dh_ads_dp_Dubinin(
      M_adsorptive=M_adsorptive,
      T_adsorpt=T_adsorpt,
      x_adsorpt=x_adsorpt,
      dx_adsorpt_dp=dx_adsorpt_dp,
      h_adsorptiveToLiquid=properties_adsorptive.h_adsorptiveToLiquid,
      dh_adsorptiveToLiquid_dp=properties_adsorptive.dh_adsorptiveToLiquid_dp_T,
      v_adsorpt=v_adsorpt,
      dv_adsorpt_dp=dv_adsorpt_dp,
      beta_adsorpt=beta_adsorpt,
      dbeta_adsorpt_dp=dbeta_adsorpt_dp,
      dA_dp=dA_dp,
      dW_dA=dW_dA,
      ddW_dA_dA=ddW_dA_dA)
      "Partial derivative of molar adsorption potential w.r.t. pressure at
     constant temperature";
  end calc_dh_ads_dp_T_Dubinin;

  redeclare final function extends calc_dh_ads_dT_p_Dubinin
    "Calculates the partial derivative of the molar adsorption enthalpy w.r.t. temperature at constant pressure according to the model of Dubinin"
  algorithm
    //
    // Calculate additional properties
    //
    dW_dA :=IsothermModel.dW_dA(
      A=A,
      c=c)
      "Partial derivative of the characteristic curve w.r.t. the molar adsorption
    potential at constant pressure and temperaure";
    ddW_dA_dT := IsothermModel.ddW_dA_dT(
      p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt,
      A=A,
      c=c,
      dc_dT_adsorpt=dc_dT_adsorpt)
      "Second-order partial derivative of filled pore volume w.r.t. molar 
    adsorption potential and temperature at constant pressure";

    //
    // Calculate specific enthalpy of adsorption
    //
    dh_ads_dT := 1 / M_adsorptive *
      SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.dh_ads_dT_Dubinin(
      M_adsorptive=M_adsorptive,
      T_adsorpt=T_adsorpt,
      x_adsorpt=x_adsorpt,
      dx_adsorpt_dT=dx_adsorpt_dT,
      h_adsorptiveToLiquid=properties_adsorptive.h_adsorptiveToLiquid,
      dh_adsorptiveToLiquid_dT=properties_adsorptive.dh_adsorptiveToLiquid_dT_p,
      v_adsorpt=v_adsorpt,
      dv_adsorpt_dT=dv_adsorpt_dT,
      beta_adsorpt=beta_adsorpt,
      dbeta_adsorpt_dT=dbeta_adsorpt_dT,
      dA_dT=dA_dT,
      dW_dA=dW_dA,
      ddW_dA_dT=ddW_dA_dT)
      "Partial derivative of molar adsorption potential w.r.t. temperature at
    constant pressure";
  end calc_dh_ads_dT_p_Dubinin;

  redeclare final function extends calc_integrand_WaltonLeVan_Dubinin
  "Calculates the integrand required for calculating the specific heat capacity according to Walton and Le Van (2005) when using the specifc enthalpy of adsorption according to Dubinin"
  algorithm
    dW_dA:=IsothermModel.dW_dA(
      A=A,
      c=c)
      "Partial derivative of filled pore volume w.r.t. molar adsorption potential
      at constant pressure and temperature";
    ddW_dA_dA := IsothermModel.ddW_dA_dA(
      A=A,
      c=c)
      "Partial derivative of filled pore volume w.r.t. molar adsorption potential
      at constant pressure and temperature";
    ddW_dA_dT := IsothermModel.ddW_dA_dT(
      p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt,
      A=A,
      c=c,
      dc_dT_adsorpt=dc_dT_adsorpt)
      "Second-order partial derivative of filled pore volume w.r.t. molar adsorption 
      potential and temperature at constant pressure";

    //
    // Calculation of further properties
    //
    if p_adsorpt > p_clausiusClyperon then
      //
      // Do not use Clausius Clyperon assumptions
      //
      (,,,,,,h_adsorptiveToLiquid,dh_adsorptiveToLiquid_dp,dh_adsorptiveToLiquid_dT) :=
        MediumSpecificFunctions.calc_properties(
          p=p_adsorpt,
          T=T_adsorpt,
          dp=dp,
          dT=dT,
          p_min=p_clausiusClyperon,
          require_v_adsorptive=false,
          require_h_adsorptive=false,
          require_s_adsorptive=false,
          require_dh_adsorptive_dT_dp=false,
          require_h_adsorptiveToLiquid=true,
          adsorptiveAtDewPoint=adsorptiveAtDewPoint)
        "Specific enthalpy difference between adsorptive state and saturated liquid
       state (i.e., bubble point) and its partial derivatives";

      dh_ads_dp_T :=
        SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.dh_ads_dp_Dubinin(
          M_adsorptive=M_adsorptive,
          T_adsorpt=T_adsorpt,
          x_adsorpt=u,
          dx_adsorpt_dp=dx_adsorpt_dp,
          h_adsorptiveToLiquid=h_adsorptiveToLiquid,
          dh_adsorptiveToLiquid_dp=dh_adsorptiveToLiquid_dp,
          v_adsorpt=v_adsorpt,
          dv_adsorpt_dp=dv_adsorpt_dp,
          beta_adsorpt=beta_adsorpt,
          dbeta_adsorpt_dp=dbeta_adsorpt_dp,
          dA_dp=dA_dp,
          dW_dA=dW_dA,
          ddW_dA_dA=ddW_dA_dA)
      "Partial derivative of molar adsorption enthalpy w.r.t. pressure at constant
    temperature";
      dh_ads_dT_p :=
        SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.dh_ads_dT_Dubinin(
          M_adsorptive=M_adsorptive,
          T_adsorpt=T_adsorpt,
          x_adsorpt=u,
          dx_adsorpt_dT=dx_adsorpt_dT,
          h_adsorptiveToLiquid=h_adsorptiveToLiquid,
          dh_adsorptiveToLiquid_dT=dh_adsorptiveToLiquid_dT,
          v_adsorpt=v_adsorpt,
          dv_adsorpt_dT=dv_adsorpt_dT,
          beta_adsorpt=beta_adsorpt,
          dbeta_adsorpt_dT=dbeta_adsorpt_dT,
          dA_dT=dA_dT,
          dW_dA=dW_dA,
          ddW_dA_dT=ddW_dA_dT)
        "Partial derivative of molar adsorption enthalpy w.r.t. temperature at constant
      pressure";

    else
      //
      // Use Clausius Clyperon assumptions: No further propiertes are required
      //
      dh_ads_dp_T :=
        SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.dh_ads_dp_clausiusClapeyron(
          p_adsorpt=p_adsorpt,
          T_adsorpt=T_adsorpt,
          dx_adsorpt_dp=dx_adsorpt_dp,
          dx_adsorpt_dT=dx_adsorpt_dT,
          ddx_adsorpt_dp_dp=ddx_adsorpt_dp_dp,
          ddx_adsorpt_dp_dT=ddx_adsorpt_dp_dT)
      "Partial derivative of molar adsorption enthalpy w.r.t. pressure at constant
    temperature";
      dh_ads_dT_p :=
        SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.dh_ads_dT_clausiusClapeyron(
          p_adsorpt=p_adsorpt,
          T_adsorpt=T_adsorpt,
          dx_adsorpt_dp=dx_adsorpt_dp,
          dx_adsorpt_dT=dx_adsorpt_dT,
          ddx_adsorpt_dp_dT=ddx_adsorpt_dp_dT,
          ddx_adsorpt_dT_dT=ddx_adsorpt_dT_dT)
        "Partial derivative of molar adsorption enthalpy w.r.t. temperature at constant
      pressure";

    end if;

    //
    // Calculation of the integrand
    //
    y := dh_ads_dT_p - dh_ads_dp_T * dx_adsorpt_dT / dx_adsorpt_dp
      "Integrand: Partial derivative of molar sorption enthalpy w.r.t. temperature
    at constant uptake";
  end calc_integrand_WaltonLeVan_Dubinin;

  redeclare final function extends calc_integrand_SchwambergerSchmidt_Dubinin
  "Calculates the integrand required for calculating the specific heat capacity according to Schwamberger and Schmidt (2013) when using the specifc enthalpy of adsorption according to Dubinin"
  algorithm
    dW_dA:=IsothermModel.dW_dA(
      A=A,
      c=c)
      "Partial derivative of filled pore volume w.r.t. molar adsorption potential
      at constant pressure and temperature";
    ddW_dA_dA := IsothermModel.ddW_dA_dA(
      A=A,
      c=c)
      "Partial derivative of filled pore volume w.r.t. molar adsorption potential
      at constant pressure and temperature";
    ddW_dA_dT := IsothermModel.ddW_dA_dT(
      p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt,
      A=A,
      c=c,
      dc_dT_adsorpt=dc_dT_adsorpt)
      "Second-order partial derivative of filled pore volume w.r.t. molar adsorption 
      potential and temperature at constant pressure";

    //
    // Calculation of further properties
    //
    if p_adsorpt > p_clausiusClyperon then
      //
      // Do not use Clausius Clyperon assumptions
      //
      (,,,,,  dh_adsorptive_dT,
       h_adsorptiveToLiquid, dh_adsorptiveToLiquid_dp, dh_adsorptiveToLiquid_dT) :=
        MediumSpecificFunctions.calc_properties(
          p=p_adsorpt,
          T=T_adsorpt,
          dp=dp,
          dT=dT,
          p_min=p_clausiusClyperon,
          require_v_adsorptive=false,
          require_h_adsorptive=false,
          require_s_adsorptive=false,
          require_dh_adsorptive_dT_dp=true,
          require_h_adsorptiveToLiquid=true,
          adsorptiveAtDewPoint=adsorptiveAtDewPoint)
        "Partial derivative of the specific enthalpy of the adsorptive w.r.t.
      temperature at constant pressure and specific enthalpy difference between 
      adsorptive state and saturated liquid state (i.e., bubble point) and its 
      partial derivatives";

      dh_ads_dp_T :=
        SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.dh_ads_dp_Dubinin(
          M_adsorptive=M_adsorptive,
          T_adsorpt=T_adsorpt,
          x_adsorpt=u,
          dx_adsorpt_dp=dx_adsorpt_dp,
          h_adsorptiveToLiquid=h_adsorptiveToLiquid,
          dh_adsorptiveToLiquid_dp=dh_adsorptiveToLiquid_dp,
          v_adsorpt=v_adsorpt,
          dv_adsorpt_dp=dv_adsorpt_dp,
          beta_adsorpt=beta_adsorpt,
          dbeta_adsorpt_dp=dbeta_adsorpt_dp,
          dA_dp=dA_dp,
          dW_dA=dW_dA,
          ddW_dA_dA=ddW_dA_dA)
      "Partial derivative of molar adsorption enthalpy w.r.t. pressure at constant
    temperature";
      dh_ads_dT_p :=
        SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.dh_ads_dT_Dubinin(
          M_adsorptive=M_adsorptive,
          T_adsorpt=T_adsorpt,
          x_adsorpt=u,
          dx_adsorpt_dT=dx_adsorpt_dT,
          h_adsorptiveToLiquid=h_adsorptiveToLiquid,
          dh_adsorptiveToLiquid_dT=dh_adsorptiveToLiquid_dT,
          v_adsorpt=v_adsorpt,
          dv_adsorpt_dT=dv_adsorpt_dT,
          beta_adsorpt=beta_adsorpt,
          dbeta_adsorpt_dT=dbeta_adsorpt_dT,
          dA_dT=dA_dT,
          dW_dA=dW_dA,
          ddW_dA_dT=ddW_dA_dT)
        "Partial derivative of molar adsorption enthalpy w.r.t. temperature at constant
      pressure";

    else
      //
      // Use Clausius Clyperon assumptions: No further propiertes are required
      //
      (,,,,,  dh_adsorptive_dT,,,) :=
        MediumSpecificFunctions.calc_properties(
          p=p_adsorpt,
          T=T_adsorpt,
          dp=dp,
          dT=dT,
          p_min=p_clausiusClyperon,
          require_v_adsorptive=false,
          require_h_adsorptive=false,
          require_s_adsorptive=false,
          require_dh_adsorptive_dT_dp=true,
          require_h_adsorptiveToLiquid=false,
          adsorptiveAtDewPoint=adsorptiveAtDewPoint)
        "Partial derivative of the specific enthalpy of the adsorptive w.r.t.
      temperature at constant pressure";

      dh_ads_dp_T :=
        SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.dh_ads_dp_clausiusClapeyron(
          p_adsorpt=p_adsorpt,
          T_adsorpt=T_adsorpt,
          dx_adsorpt_dp=dx_adsorpt_dp,
          dx_adsorpt_dT=dx_adsorpt_dT,
          ddx_adsorpt_dp_dp=ddx_adsorpt_dp_dp,
          ddx_adsorpt_dp_dT=ddx_adsorpt_dp_dT)
      "Partial derivative of molar adsorption enthalpy w.r.t. pressure at constant
    temperature";
      dh_ads_dT_p :=
        SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.dh_ads_dT_clausiusClapeyron(
          p_adsorpt=p_adsorpt,
          T_adsorpt=T_adsorpt,
          dx_adsorpt_dp=dx_adsorpt_dp,
          dx_adsorpt_dT=dx_adsorpt_dT,
          ddx_adsorpt_dp_dT=ddx_adsorpt_dp_dT,
          ddx_adsorpt_dT_dT=ddx_adsorpt_dT_dT)
        "Partial derivative of molar adsorption enthalpy w.r.t. temperature at constant
      pressure";

    end if;

    //
    // Calculation of the integrand
    //
    y :=  dh_adsorptive_dT  -
      (dh_ads_dT_p - dh_ads_dp_T *  dx_adsorpt_dT / dx_adsorpt_dp) / M_adsorptive
      "Integrand: Partial derivative of molar sorption enthalpy w.r.t. temperature
    at constant uptake";
  end calc_integrand_SchwambergerSchmidt_Dubinin;

  redeclare final function extends calc_integrand_h_avg_adsorpt_Dubinin
    "Calculates the integrand required for calculating the uptake-averaged specific enthalpy of adsorpt using the adsorption enthalpy according to Dubinin"
  algorithm
    dW_dA:=IsothermModel.dW_dA(
      A=A,
      c=c)
      "Partial derivative of filled pore volume w.r.t. molar adsorption potential
    at constant pressure and temperature";

    //
    // Calculation of further properties
    //
    if p_adsorpt > p_clausiusClyperon then
      //
      // Do not use Clausius-Clyperon assumptions
      //
      (,,,h_adsorptive,,,h_adsorptiveToLiquid,,) :=
        MediumSpecificFunctions.calc_properties(
          p=p_adsorpt,
          T=T_adsorpt,
          dp=dp,
          dT=dT,
          p_min=p_clausiusClyperon,
          require_v_adsorptive=false,
          require_h_adsorptive=true,
          require_s_adsorptive=false,
          require_dh_adsorptive_dT_dp=false,
          require_h_adsorptiveToLiquid=true,
          adsorptiveAtDewPoint=adsorptiveAtDewPoint)
        "Specific enthalpy of the adsorptive and specific enthalpy difference between 
      adsorptive state and saturated liquid state (i.e., bubble point)";

      h_ads :=
        SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.h_ads_Dubinin(
          M_adsorptive=M_adsorptive,
          T_adsorpt=T_adsorpt,
          x_adsorpt=u,
          h_adsorptiveToLiquid=h_adsorptiveToLiquid,
          v_adsorpt=v_adsorpt,
          beta_adsorpt=beta_adsorpt,
          A=A,
          dW_dA=dW_dA)
        "Molar specific enthalpy of adsorption";

    else
      //
      // Use Clausius-Clyperon assumptions
      //
      (,,,h_adsorptive,,,,,)  :=
        MediumSpecificFunctions.calc_properties(
          p=p_adsorpt,
          T=T_adsorpt,
          dp=dp,
          dT=dT,
          p_min=p_clausiusClyperon,
          require_v_adsorptive=false,
          require_h_adsorptive=true,
          require_s_adsorptive=false,
          require_dh_adsorptive_dT_dp=false,
          require_h_adsorptiveToLiquid=false,
          adsorptiveAtDewPoint=adsorptiveAtDewPoint)
        "Partial enthalpy of the adsorptive";

      h_ads :=
        SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.h_ads_clausiusClapeyron(
          p_adsorpt=p_adsorpt,
          T_adsorpt=T_adsorpt,
          dx_adsorpt_dp=dx_adsorpt_dp,
          dx_adsorpt_dT=dx_adsorpt_dT)
        "Molar specific enthalpy of adsorption";

    end if;

    //
    // Calculation of the integrand
    //
    y :=  h_adsorptive - h_ads / M_adsorptive
      "Integrand: Specific enthalpy difference between the adsorptive and adsorpt";
  end calc_integrand_h_avg_adsorpt_Dubinin;

  redeclare final function extends calc_integrand_s_avg_adsorpt_Dubinin
    "Calculates the integrand required for calculating the uptake-averaged specific entropy of adsorpt using the adsorption enthalpy according to Dubinin"
  algorithm
    dW_dA:=IsothermModel.dW_dA(
      A=A,
      c=c)
      "Partial derivative of filled pore volume w.r.t. molar adsorption potential
    at constant pressure and temperature";

    //
    // Calculation of further properties
    //
    if p_adsorpt > p_clausiusClyperon then
      //
      // Do not use Clausius-Clyperon assumptions
      //
      (,,,,s_adsorptive,,h_adsorptiveToLiquid,,) :=
        MediumSpecificFunctions.calc_properties(
          p=p_adsorpt,
          T=T_adsorpt,
          dp=dp,
          dT=dT,
          p_min=p_clausiusClyperon,
          require_v_adsorptive=false,
          require_h_adsorptive=false,
          require_s_adsorptive=true,
          require_dh_adsorptive_dT_dp=false,
          require_h_adsorptiveToLiquid=true,
          adsorptiveAtDewPoint=adsorptiveAtDewPoint)
        "Specific entropy of the adsorptive and specific enthalpy difference between 
      adsorptive state and saturated liquid state (i.e., bubble point)";

      h_ads :=
        SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.h_ads_Dubinin(
          M_adsorptive=M_adsorptive,
          T_adsorpt=T_adsorpt,
          x_adsorpt=u,
          h_adsorptiveToLiquid=h_adsorptiveToLiquid,
          v_adsorpt=v_adsorpt,
          beta_adsorpt=beta_adsorpt,
          A=A,
          dW_dA=dW_dA)
        "Molar specific enthalpy of adsorption";

    else
      //
      // Use Clausius-Clyperon assumptions
      //
      (,,,,s_adsorptive,,,,)  :=
        MediumSpecificFunctions.calc_properties(
          p=p_adsorpt,
          T=T_adsorpt,
          dp=dp,
          dT=dT,
          p_min=p_clausiusClyperon,
          require_v_adsorptive=false,
          require_h_adsorptive=false,
          require_s_adsorptive=true,
          require_dh_adsorptive_dT_dp=false,
          require_h_adsorptiveToLiquid=false,
          adsorptiveAtDewPoint=adsorptiveAtDewPoint)
        "Partial entropy of the adsorptive";

      h_ads :=
        SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.h_ads_clausiusClapeyron(
          p_adsorpt=p_adsorpt,
          T_adsorpt=T_adsorpt,
          dx_adsorpt_dp=dx_adsorpt_dp,
          dx_adsorpt_dT=dx_adsorpt_dT)
        "Molar specific enthalpy of adsorption";

    end if;

    //
    // Calculation of the integrand
    //
    y :=  s_adsorptive - h_ads / M_adsorptive / T_adsorpt
      "Integrand: Specific entropy difference between the adsorptive and adsorpt";
  end calc_integrand_s_avg_adsorpt_Dubinin;
  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial package contains all declaration for parametrization of a pure component working
pair based on the model of Dubinin. This means that constants, packages, models, and functions 
are defined that every pure component working pair must support. Packages that inherit properties 
from this partial package must redeclare all constants except of <i>twoPhaseAdsorptive</i> and 
<i>modelOfDubinin</i>, the two partial packages <i>IsothermModel</i> and <i>MediumSpecificFunctions</i>, 
the model <i>Sorbent</i>, and the two functions <i>calc_c</i> and <i>calc_coefficients</i>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end PartialPureParametrizationDubinin;
