within SorpLib.Media.WorkingPairs.Interfaces.PartialPureParametrization;
model IsothermCoefficients
  "Calculates the temperature-dependent coefficients of the isotherm model"
  extends Modelica.Icons.MaterialProperty;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.Temperature T_adsorpt
    "Equilibrium temperature"
    annotation (Dialog(tab="General",group="Inputs"));
  input Modelica.Units.SI.TemperatureDifference dT = 1e-3
    "Temperature difference if derivatives are calculated numerically"
    annotation (Dialog(tab="General",group="Inputs"),
                HideResult=true);

  //
  // Definition of outputs
  //
  output Real[no_coefficients] c
    "Isotherm coefficients"
    annotation (Dialog(tab="General",group="Outputs",enable=false));
  output Real[no_coefficients] dc_dT
    "Partial derivative of isotherm coefficients w.r.t. temperature"
    annotation (Dialog(tab="General",group="Outputs",enable=false));
  output Real[no_coefficients] ddc_dT_dT
    "Second-order partial derivative of isotherm coefficients w.r.t. temperature"
    annotation (Dialog(tab="General",group="Outputs",enable=false));

equation
    (c, dc_dT, ddc_dT_dT) = calc_coefficients(T_adsorpt=T_adsorpt, dT=dT)
    "Isotherm coefficients and their partial derivatives w.r.t. temperature";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This model calculates temperature-dependent isotherm coefficients <i>c</i> and 
their partial derivatives with respect to temperature <i>dc_dT</i> (first order) 
and <i>ddc_dT_dT</i> (second order).
</p>
</html>",
        revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end IsothermCoefficients;
