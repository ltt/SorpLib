within SorpLib.Media.WorkingPairs.Interfaces.PartialPureParametrization;
function calc_integrand_s_avg_adsorpt_Formal
  "Calculates the integrand required for calculating the uptake-averaged specific entropy of adsorpt using the formal definition of the adsorption enthalpy"
  extends Modelica.Math.Nonlinear.Interfaces.partialScalarFunction;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.Temperature T_adsorpt
    "Temperature"
    annotation (Dialog(tab="General", group="Inputs"));

  input Real c[:]
    "Coefficients of the isotherm model"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real dc_dT_adsorpt[:]
    "Partial derivative of coefficients of the isotherm model w.r.t. 
    temperature"
    annotation (Dialog(tab="General", group="Inputs"));

  input Boolean adsorptiveAtDewPoint
    "= true, if adsorptive (gas/vapor phase) is assumed to be at dew point at
    T_adsorpt"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of inputs regarding the specific volume of the adsorpt
  //
  input Modelica.Units.SI.SpecificVolume v_adsorpt
    "Specific volume of the adsorpt"
    annotation (Dialog(tab="General",group="Inputs"));

  //
  // Definition of inputs regarding numerics
  //
  input Modelica.Units.SI.Pressure p_clausiusClyperon
    "Maximum pressure up to which the molar adsorption enthalpy is calculated
    according to the Clausius Clyperon assumptions"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.PressureDifference dp
    "Pressure difference used to calculate partial derivatives w.r.t. pressure
    numerially (just used if analytical solution does not exist)"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.TemperatureDifference dT
    "Temperature difference used to calculate partial derivatives w.r.t. pressure
    numerially (just used if analytical solution does not exist)"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of variables
  //
protected
  Modelica.Units.SI.Pressure p_adsorpt
    "Pressure at x_adsorpt = u and T_adsorpt";

  Modelica.Units.SI.SpecificVolume v_adsorptive
    "Specific volume of the adsorptive";
  Modelica.Units.SI.SpecificEntropy s_adsorptive
    "Specific entropy of the adsorptive";

  SorpLib.Units.DerUptakeByPressure dx_adsorpt_dp
    "Partial derivative of uptake w.r.t. pressure at constant temperature";
  SorpLib.Units.DerUptakeByTemperature dx_adsorpt_dT
    "Partial derivative of uptake w.r.t. temperature at constant pressure";

  Modelica.Units.SI.MolarEnthalpy h_ads
    "Molar adsorption enthalpy";

algorithm
  //
  // Calculation of sorption equilibrium
  //
  p_adsorpt := IsothermModel.p_xT(
      x_adsorpt=u,
      T_adsorpt=T_adsorpt,
      c=c,
      p_adsorpt_lb_start=1,
      p_adsorpt_ub_start=10,
      tolerance=100*Modelica.Constants.eps)
    "Pressure at x_adsorpt = u and T_adsorpt";

  dx_adsorpt_dp := IsothermModel.dx_dp(
      p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt,
      c=c)
    "Partial derivative of uptake w.r.t. pressure at constant temperature";
  dx_adsorpt_dT := IsothermModel.dx_dT(
      p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt,
      c=c,
      dc_dT_adsorpt=dc_dT_adsorpt)
    "Partial derivative of uptake w.r.t. temperature at constant pressure";

  //
  // Calculation of further properties
  //
  if p_adsorpt > p_clausiusClyperon then
    //
    // Do not use Clausius-Clyperon assumptions
    //
    (v_adsorptive,,,,s_adsorptive,,,,) :=
      MediumSpecificFunctions.calc_properties(
        p=p_adsorpt,
        T=T_adsorpt,
        dp=dp,
        dT=dT,
        p_min=p_clausiusClyperon,
        require_v_adsorptive=true,
        require_h_adsorptive=false,
        require_s_adsorptive=true,
        require_dh_adsorptive_dT_dp=false,
        require_h_adsorptiveToLiquid=false,
        adsorptiveAtDewPoint=adsorptiveAtDewPoint)
      "Specific volume of the adsorptive and specifc entropy of the adsorptive";

    h_ads :=
      SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.h_ads(
        M_adsorptive=M_adsorptive,
        T_adsorpt=T_adsorpt,
        v_adsorptive=v_adsorptive,
        v_adsorpt=v_adsorpt,
        dx_adsorpt_dp=dx_adsorpt_dp,
        dx_adsorpt_dT=dx_adsorpt_dT)
      "Molar specific enthalpy of adsorption";

  else
    //
    // Use Clausius-Clyperon assumptions
    //
    (,,,,s_adsorptive,,,,) :=
      MediumSpecificFunctions.calc_properties(
        p=p_adsorpt,
        T=T_adsorpt,
        dp=dp,
        dT=dT,
        p_min=p_clausiusClyperon,
        require_v_adsorptive=false,
        require_h_adsorptive=false,
        require_s_adsorptive=true,
        require_dh_adsorptive_dT_dp=false,
        require_h_adsorptiveToLiquid=false,
        adsorptiveAtDewPoint=adsorptiveAtDewPoint)
      "Specific entropy of the adsorptive";

    h_ads :=
      SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.h_ads_clausiusClapeyron(
        p_adsorpt=p_adsorpt,
        T_adsorpt=T_adsorpt,
        dx_adsorpt_dp=dx_adsorpt_dp,
        dx_adsorpt_dT=dx_adsorpt_dT)
      "Molar specific enthalpy of adsorption";

  end if;

  //
  // Calculation of the integrand
  //
  y :=  s_adsorptive - h_ads / M_adsorptive / T_adsorpt
    "Integrand: Specific entropy difference between the adsorptive and adsorpt";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function calculates the specific entropy difference between the adsorptive
and adsorpt using the formal definition of the specific enthalpy of adsorption.
</p>
</html>",
        revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end calc_integrand_s_avg_adsorpt_Formal;
