within SorpLib.Media.WorkingPairs.Interfaces.PartialPureParametrization;
function calc_integrand_SchwambergerSchmidt_CC
  "Calculates the integrand required for calculating the specific heat capacity according to Schwamberger and Schmidt (2013) when using the specifc enthalpy of adsorption according to Clausius Clayperon"
  extends Modelica.Math.Nonlinear.Interfaces.partialScalarFunction;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.Temperature T_adsorpt
    "Temperature"
    annotation (Dialog(tab="General", group="Inputs"));

  input Real c[:]
    "Coefficients of the isotherm model"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real dc_dT_adsorpt[:]
    "Partial derivative of coefficients of the isotherm model w.r.t. 
    temperature"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real ddc_dT_adsorpt_dT_adsorpt[:]
    "Second-order partial derivative of coefficients of the isotherm model w.r.t. 
    temperature"
    annotation (Dialog(tab="General", group="Inputs"));

  input Boolean adsorptiveAtDewPoint
    "= true, if adsorptive (gas/vapor phase) is assumed to be at dew point at
    T_adsorpt"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of inputs regarding numerics
  //
  input Modelica.Units.SI.Pressure p_clausiusClyperon
    "Maximum pressure up to which the molar adsorption enthalpy is calculated
    according to the Clausius Clyperon assumptions"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.PressureDifference dp
    "Pressure difference used to calculate partial derivatives w.r.t. pressure
    numerially (just used if analytical solution does not exist)"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.TemperatureDifference dT
    "Temperature difference used to calculate partial derivatives w.r.t. pressure
    numerially (just used if analytical solution does not exist)"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of variables
  //
protected
  Modelica.Units.SI.Pressure p_adsorpt
    "Pressure at x_adsorpt = u and T_adsorpt";

  Modelica.Units.SI.SpecificHeatCapacity dh_adsorptive_dT
    "Partial derivative of the specific enthalpy of the adsorptive w.r.t. temperature
    at constant pressure";

  SorpLib.Units.DerUptakeByPressure dx_adsorpt_dp
    "Partial derivative of uptake w.r.t. pressure at constant temperature";
  SorpLib.Units.DerUptakeByTemperature dx_adsorpt_dT
    "Partial derivative of uptake w.r.t. temperature at constant pressure";

  SorpLib.Units.DerUptakeByPressurePressure ddx_adsorpt_dp_dp
    "Second-order partial derivative of uptake w.r.t. pressure at contant 
    temperature";
  SorpLib.Units.DerUptakeByTemperatureTemperature ddx_adsorpt_dT_dT
    "Second-order partial derivative of uptake w.r.t. temperature at contant 
    pressure";
  SorpLib.Units.DerUptakeByPressureTemperature ddx_adsorpt_dp_dT
    "Second-order partial derivative of uptake w.r.t. pressure and temperature";

  SorpLib.Units.DerMolarEnthalpyByPressure dh_ads_dp_T
    "Partial derivative of molar adsorption enthalpy w.r.t. pressure at constant
    temperature";
  Modelica.Units.SI.MolarHeatCapacity dh_ads_dT_p
    "Partial derivative of molar adsorption enthalpy w.r.t. temperature at constant
    pressure";

algorithm
  //
  // Calculation of sorption equilibrium
  //
  p_adsorpt := IsothermModel.p_xT(
      x_adsorpt=u,
      T_adsorpt=T_adsorpt,
      c=c,
      p_adsorpt_lb_start=1,
      p_adsorpt_ub_start=10,
      tolerance=100*Modelica.Constants.eps)
    "Pressure at x_adsorpt = u and T_adsorpt";

  dx_adsorpt_dp := IsothermModel.dx_dp(
      p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt,
      c=c)
    "Partial derivative of uptake w.r.t. pressure at constant temperature";
  dx_adsorpt_dT := IsothermModel.dx_dT(
      p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt,
      c=c,
      dc_dT_adsorpt=dc_dT_adsorpt)
    "Partial derivative of uptake w.r.t. temperature at constant pressure";

  ddx_adsorpt_dp_dp := IsothermModel.ddx_dp_dp(
      p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt,
      c=c)
    "Second-order partial derivative of uptake w.r.t. pressure at constant temperature";
  ddx_adsorpt_dT_dT := IsothermModel.ddx_dT_dT(
      p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt,
      c=c,
      dc_dT_adsorpt=dc_dT_adsorpt,
      ddc_dT_adsorpt_dT_adsorpt=ddc_dT_adsorpt_dT_adsorpt)
    "Second-order partial derivative of uptake w.r.t. temperature at constant pressure";
  ddx_adsorpt_dp_dT := IsothermModel.ddx_dp_dT(
      p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt,
      c=c,
      dc_dT_adsorpt=dc_dT_adsorpt)
    "Second-order partial derivative of uptake w.r.t. pressure and temperature";

  //
  // Calculation of partial derivatives
  //
  (,,,,,  dh_adsorptive_dT,,,) :=
    MediumSpecificFunctions.calc_properties(
      p=p_adsorpt,
      T=T_adsorpt,
      dp=dp,
      dT=dT,
      p_min=p_clausiusClyperon,
      require_v_adsorptive=false,
      require_h_adsorptive=false,
      require_s_adsorptive=false,
      require_dh_adsorptive_dT_dp=true,
      require_h_adsorptiveToLiquid=false,
      adsorptiveAtDewPoint=adsorptiveAtDewPoint)
    "Partial derivative of the specific enthalpy of the adsorptive w.r.t.
    temperature at constant pressure";

  dh_ads_dp_T :=
    SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.dh_ads_dp_clausiusClapeyron(
      p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt,
      dx_adsorpt_dp=dx_adsorpt_dp,
      dx_adsorpt_dT=dx_adsorpt_dT,
      ddx_adsorpt_dp_dp=ddx_adsorpt_dp_dp,
      ddx_adsorpt_dp_dT=ddx_adsorpt_dp_dT)
  "Partial derivative of molar adsorption enthalpy w.r.t. pressure at constant
  temperature";
  dh_ads_dT_p :=
    SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.dh_ads_dT_clausiusClapeyron(
      p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt,
      dx_adsorpt_dp=dx_adsorpt_dp,
      dx_adsorpt_dT=dx_adsorpt_dT,
      ddx_adsorpt_dp_dT=ddx_adsorpt_dp_dT,
      ddx_adsorpt_dT_dT=ddx_adsorpt_dT_dT)
    "Partial derivative of molar adsorption enthalpy w.r.t. temperature at constant
    pressure";

  //
  // Calculation of the integrand
  //
  y :=  dh_adsorptive_dT -
    (dh_ads_dT_p - dh_ads_dp_T * dx_adsorpt_dT / dx_adsorpt_dp) / M_adsorptive
    "Integrand: Partial derivative of molar sorption enthalpy w.r.t. temperature
    at constant uptake";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function calculates the partial derivative of the specific enthalpy of the 
adsorpt with respect to temperature at constant uptake when using the molar adsorption
enthalpy according to Clausius Clayperon. For more information, check the model
<a href=\"Modelica://SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.Integrands.dh_adsorpt_dT_x_CC\">SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.Integrands.dh_adsorpt_dT_x_CC</a>.
</p>
</html>",
        revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end calc_integrand_SchwambergerSchmidt_CC;
