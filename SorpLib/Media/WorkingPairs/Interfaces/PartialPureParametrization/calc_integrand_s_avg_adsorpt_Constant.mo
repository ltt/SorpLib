within SorpLib.Media.WorkingPairs.Interfaces.PartialPureParametrization;
function calc_integrand_s_avg_adsorpt_Constant
  "Calculates the integrand required for calculating the uptake-averaged specific entropy of adsorpt using a constant specific enthalpy of adsorption"
  extends Modelica.Math.Nonlinear.Interfaces.partialScalarFunction;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.Temperature T_adsorpt
    "Temperature"
    annotation (Dialog(tab="General", group="Inputs"));

  input Real c[:]
    "Coefficients of the isotherm model"
    annotation (Dialog(tab="General", group="Inputs"));

  input Modelica.Units.SI.SpecificEnthalpy h_ads
    "Constant specific enthalpy of adsorption"
    annotation (Dialog(tab="General", group="Inputs"));

  input Boolean adsorptiveAtDewPoint
    "= true, if adsorptive (gas/vapor phase) is assumed to be at dew point at
    T_adsorpt"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of inputs regarding numerics
  //
  input Modelica.Units.SI.Pressure p_clausiusClyperon
    "Maximum pressure up to which the molar adsorption enthalpy is calculated
    according to the Clausius Clyperon assumptions"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.PressureDifference dp
    "Pressure difference used to calculate partial derivatives w.r.t. pressure
    numerially (just used if analytical solution does not exist)"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.TemperatureDifference dT
    "Temperature difference used to calculate partial derivatives w.r.t. pressure
    numerially (just used if analytical solution does not exist)"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of variables
  //
protected
  Modelica.Units.SI.Pressure p_adsorpt
    "Pressure at x_adsorpt = u and T_adsorpt";

  Modelica.Units.SI.SpecificEntropy s_adsorptive
    "Specific entropy of the adsorptive";

algorithm
  //
  // Calculation of sorption equilibrium
  //
  p_adsorpt := IsothermModel.p_xT(
      x_adsorpt=u,
      T_adsorpt=T_adsorpt,
      c=c,
      p_adsorpt_lb_start=1,
      p_adsorpt_ub_start=10,
      tolerance=100*Modelica.Constants.eps)
    "Pressure at x_adsorpt = u and T_adsorpt";

  //
  // Calculation of further properties
  //
  (,,,,s_adsorptive,,,,) :=
    MediumSpecificFunctions.calc_properties(
      p=p_adsorpt,
      T=T_adsorpt,
      dp=dp,
      dT=dT,
      p_min=p_clausiusClyperon,
      require_v_adsorptive=false,
      require_h_adsorptive=false,
      require_s_adsorptive=true,
      require_dh_adsorptive_dT_dp=false,
      require_h_adsorptiveToLiquid=false,
      adsorptiveAtDewPoint=adsorptiveAtDewPoint)
    "Specific entropy of the adsorptive";

  //
  // Calculation of the integrand
  //
  y :=  s_adsorptive - h_ads / T_adsorpt
    "Integrand: Specific entropy difference between the adsorptive and adsorpt";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function calculates the specific entropy difference between the adsorptive
and adsorpt using a constant specific enthalpy of adsorption.
</p>
</html>",
        revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end calc_integrand_s_avg_adsorpt_Constant;
