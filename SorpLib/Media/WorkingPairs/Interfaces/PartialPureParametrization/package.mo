within SorpLib.Media.WorkingPairs.Interfaces;
partial package PartialPureParametrization "Base package defining all properties for parametrization of pure component working pairs"
  extends Modelica.Icons.MaterialPropertiesPackage;

  //
  // Definition of constants
  //  
  constant Modelica.Units.SI.MolarMass M_adsorptive
    "Molar mass of the adsorptive"
    annotation (Dialog(tab="General",group="Constants", enable=false),
                choices(checkBox=true));
  constant Boolean twoPhaseAdsorptive
    "= true, if media model of the adsorptive has a two-phase regime"
    annotation (Dialog(tab="General",group="Constants", enable=false),
                choices(checkBox=true));

  constant Boolean modelOfDubinin
    "= true, if the isotherm model is based on the model of Dubinin"
    annotation (Dialog(tab="General",group="Constants", enable=false),
                choices(checkBox=true));
  constant Integer no_coefficients
    "Number of isotherm model coefficients"
    annotation (Dialog(tab="General",group="Constants", enable=false));
  //
  // Definition of packges
  //
  replaceable package IsothermModel =
    SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative
    constrainedby
  SorpLib.Media.Functions.SorptionEquilibria.Interfaces.PartialPureComponents
    "Package of the isotherm model providing all functions of the isotherm model"
    annotation (Dialog(tab = "General", group = "Packages", enable=false));

  replaceable package MediumSpecificFunctions =
    SorpLib.Media.WorkingPairs.Parametrizations.PureComponents.MediumSpecificFunctions.VLE
    constrainedby
  SorpLib.Media.WorkingPairs.Interfaces.PartialPureMediumSpecificFunctions
    "Package of medium specific functions"
    annotation (Dialog(tab = "General", group = "Packages", enable=false));
  //
  // Definition of models
  //
  replaceable model Sorbent =
    SorpLib.Media.Solids.Sorbents.GenericSorbent
    constrainedby SorpLib.Media.Solids.BaseClasses.PartialSolid
    "Calculates the thermodynamic properties of the sorbent"
    annotation (Dialog(tab = "General", group = "Models"),
                choicesAllMatching=true);
  //
  // Definition of functions
  //
  replaceable partial function calc_c
    "Calculates temperature-dependent coefficients of the isotherm model"
    extends Modelica.Icons.Function;

    //
    // Definition of inputs
    //
    input Modelica.Units.SI.Temperature T_adsorpt
      "Equilibrium temperature"
      annotation (Dialog(tab="General",group="Inputs"));

    //
    // Definition of outputs
    //
    output Real[no_coefficients] c
      "Isotherm coefficients"
      annotation (Dialog(tab="General",group="Outputs",enable=false));

    //
    // Annotations
    //
    annotation (Documentation(revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>", info="<html>
<p>
This function calculates the temperature-dependent isotherm coefficients <i>c</i>.
Functions that inherit from this partial function must add the equations of the
isotherm coefficients. Typical temperature dependencies of isotherm coefficients 
are provided as generalized functions in the package
<a href=\"Modelica://SorpLib.Media.WorkingPairs.Parametrizations.Utilities\">SorpLib.Media.WorkingPairs.Parametrizations.Utilities</a>.
Constants required to calculate the temperature dependencies should be implemented
as new constants in the parametrization package.</p>
</html>"));
  end calc_c;

  replaceable partial function calc_coefficients
    "Calculates temperature-dependent coefficients and their the partial derivatives w.r.t. temperature"
    extends Modelica.Icons.Function;

    //
    // Definition of inputs and outputs
    //
    input Modelica.Units.SI.Temperature T_adsorpt
      "Equilibrium temperature"
      annotation (Dialog(tab="General",group="Inputs"));
    input Modelica.Units.SI.TemperatureDifference dT = 1e-3
      "Temperature difference if derivatives are calculated numerically"
      annotation (Dialog(tab="General",group="Inputs"));

    //
    // Definition of outputs
    //
    output Real[no_coefficients] c
      "Isotherm coefficients"
      annotation (Dialog(tab="General",group="Outputs",enable=false));
    output Real[no_coefficients] dc_dT
      "Partial derivative of isotherm coefficients w.r.t. temperature"
      annotation (Dialog(tab="General",group="Outputs",enable=false));
    output Real[no_coefficients] ddc_dT_dT
      "Second-order partial derivative of isotherm coefficients w.r.t. temperature"
      annotation (Dialog(tab="General",group="Outputs",enable=false));

    //
    // Annotations
    //
    annotation (Inline=true, Documentation(revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>", info="<html>
<p>
This function calculates the temperature-dependent isotherm coefficients <i>c</i>
and their first- and second-order partial derivatives with respect to temperature
(<i>dc_dT</i> and <i>ddc_dT_dT</i>). Functions that inherit from this partial 
function must add the equations of the isotherm coefficients and their partial
derivatives. Typical temperature dependencies of isotherm coefficients are provided 
as generalized functions in the package
<a href=\"Modelica://SorpLib.Media.WorkingPairs.Parametrizations.Utilities\">SorpLib.Media.WorkingPairs.Parametrizations.Utilities</a>.
Constants required to calculate the temperature dependencies should be implemented
as new constants in the parametrization package.</p>
</html>"));
  end calc_coefficients;

  replaceable partial function calc_h_ads_Dubinin
    "Calculates the molar adsorption enthalpy according to the model of Dubinin"
    extends Modelica.Icons.Function;

    //
    // Definition of inputs
    //
    input Real[no_coefficients] c
      "Isotherm coefficients"
      annotation (Dialog(tab="General",group="Inputs"));

    input Modelica.Units.SI.Pressure p_adsorpt
      "Equilibrium pressure"
      annotation (Dialog(tab="General",group="Inputs"));
    input Modelica.Units.SI.Temperature T_adsorpt
      "Equilibrium temperature"
      annotation (Dialog(tab="General",group="Inputs"));
    input SorpLib.Units.Uptake x_adsorpt
      "Equilibrium uptake"
      annotation (Dialog(tab="General", group="Inputs"));

    input Modelica.Units.SI.SpecificVolume v_adsorpt
      "Specific volume of the adsorpt"
      annotation (Dialog(tab="General", group="Inputs"));
    input Modelica.Units.SI.RelativePressureCoefficient beta_adsorpt
      "Isobaric expansion coefficient of the adsorpt"
      annotation (Dialog(tab="General", group="Inputs"));

    input
      SorpLib.Media.WorkingPairs.Records.PropertiesPureAdsorptive properties_adsorptive
      "Record containing all properties of the adsorptive required for the working
    pair model"
      annotation (Dialog(tab="General",group="Inputs"));

    //
    // Definition of outputs
    //
    output Modelica.Units.SI.SpecificEnthalpy h_ads
      "Specific enthalpy of adsorption"
      annotation (Dialog(tab="General", group="Outputs", enable=false));

    //
    // Definition of variables
    //
protected
    SorpLib.Units.MolarAdsorptionPotential A=
      SorpLib.Media.Functions.SorptionEquilibria.Utilities.A_ppsT(
        p_adsorpt=p_adsorpt,
        p_sat=properties_adsorptive.p_sat,
        T_adsorpt=T_adsorpt)
      "Molar adsorption potential";
    SorpLib.Units.DerFilledPoreVolumeByAdsorptionPotential dW_dA
      "Partial derivative of the characteristic curve w.r.t. the molar adsorption
    potential at constant pressure and temperaure";

    //
    // Annotations
    //
    annotation (Documentation(revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>",   info="<html>
<p>
This function calculates the specific enthalpy of adsorption according to the
model of Dubinin. For more information, check the model
<a href=\"Modelica://SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.h_ads_Dubinin\">SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.h_ads_Dubinin</a>.
</p>
</html>"));
  end calc_h_ads_Dubinin;

  replaceable partial function calc_dh_ads_dp_T_Dubinin
    "Calculates the partial derivative of the molar adsorption enthalpy w.r.t. pressure at constant temperature according to the model of Dubinin"
    extends Modelica.Icons.Function;

    //
    // Definition of inputs
    //
    input Real[no_coefficients] c
      "Isotherm coefficients"
      annotation (Dialog(tab="General",group="Inputs"));

    input Modelica.Units.SI.Pressure p_adsorpt
      "Equilibrium pressure"
      annotation (Dialog(tab="General",group="Inputs"));
    input Modelica.Units.SI.Temperature T_adsorpt
      "Equilibrium temperature"
      annotation (Dialog(tab="General",group="Inputs"));

    input SorpLib.Units.Uptake x_adsorpt
      "Equilibrium uptake"
      annotation (Dialog(tab="General", group="Inputs"));
    input SorpLib.Units.DerUptakeByPressure dx_adsorpt_dp
      "Partial derivative of uptake w.r.t. pressure"
      annotation (Dialog(tab="General", group="Inputs"));

    input Modelica.Units.SI.SpecificVolume v_adsorpt
      "Specific volume of the adsorpt"
      annotation (Dialog(tab="General", group="Inputs"));
    input SorpLib.Units.DerSpecificVolumeByPressure dv_adsorpt_dp
      "Partial derivative of specific volume of the adsorpt w.r.t. pressure at
    constant temperature"
      annotation (Dialog(tab="General", group="Inputs"));

    input Modelica.Units.SI.RelativePressureCoefficient beta_adsorpt
      "Isobaric expansion coefficient of the adsorpt"
      annotation (Dialog(tab="General", group="Inputs"));
    input SorpLib.Units.DerIsobaricExpansionCoefficientByPressure dbeta_adsorpt_dp
      "Partial derivative of isobaric expansion coefficient of the adsorpt phase
    w.r.t. pressure at constant temperature"
      annotation (Dialog(tab="General", group="Inputs"));

    input
      SorpLib.Media.WorkingPairs.Records.PropertiesPureAdsorptive properties_adsorptive
      "Record containing all properties of the adsorptive required for the working
    pair model"
      annotation (Dialog(tab="General",group="Inputs"));

    //
    // Definition of outputs
    //
    output SorpLib.Units.DerSpecificEnthalpyByPressure dh_ads_dp
      "Partial derivative of molar adsorption enthalpy w.r.t. pressure at constant
    temperature"
      annotation (Dialog(tab="General", group="Outputs", enable=false));

    //
    // Definition of variables
    //
protected
    SorpLib.Units.MolarAdsorptionPotential A=
      SorpLib.Media.Functions.SorptionEquilibria.Utilities.A_ppsT(
        p_adsorpt=p_adsorpt,
        p_sat=properties_adsorptive.p_sat,
        T_adsorpt=T_adsorpt)
      "Molar adsorption potential";
    SorpLib.Units.DerMolarAdsorptionPotentialByPressure dA_dp=
      SorpLib.Media.Functions.SorptionEquilibria.Utilities.dA_dp(
        p_adsorpt=p_adsorpt,
        p_sat=properties_adsorptive.p_sat,
        T_adsorpt=T_adsorpt)
      "Partial derivative of molar adsorption potential w.r.t. pressure at
    constant temperature";

    SorpLib.Units.DerFilledPoreVolumeByAdsorptionPotential dW_dA
      "Partial derivative of the characteristic curve w.r.t. the molar adsorption
    potential at constant pressure and temperaure";
    SorpLib.Units.DerFilledPoreVolumeByAdsorptionPotentialAdsorptionPotential ddW_dA_dA
      "Second-order partial derivative of filled pore volume w.r.t. molar 
    adsorption potential and temperature at constant pressure and temperature";

    //
    // Annotations
    //
    annotation (Documentation(revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>",   info="<html>
<p>
This function calculates the partial derivative of the specific enthalpy of 
adsorption with respect to pressure at constant temperature according to the
model of Dubinin. For more information, check the model
<a href=\"Modelica://SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.dh_ads_dp_Dubinin\">SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.dh_ads_dp_Dubinin</a>.
</p>
</html>"));
  end calc_dh_ads_dp_T_Dubinin;

  replaceable partial function calc_dh_ads_dT_p_Dubinin
    "Calculates the partial derivative of the molar adsorption enthalpy w.r.t. temperature at constant pressure according to the model of Dubinin"
    extends Modelica.Icons.Function;

    //
    // Definition of inputs
    //
    input Real[no_coefficients] c
      "Isotherm coefficients"
      annotation (Dialog(tab="General",group="Inputs"));
    input Real[no_coefficients] dc_dT_adsorpt
      "Partial derivative of isotherm coefficients w.r.t. temperature"
      annotation (Dialog(tab="General",group="Inputs"));

    input Modelica.Units.SI.Pressure p_adsorpt
      "Equilibrium pressure"
      annotation (Dialog(tab="General",group="Inputs"));
    input Modelica.Units.SI.Temperature T_adsorpt
      "Equilibrium temperature"
      annotation (Dialog(tab="General",group="Inputs"));

    input SorpLib.Units.Uptake x_adsorpt
      "Equilibrium uptake"
      annotation (Dialog(tab="General", group="Inputs"));
    input SorpLib.Units.DerUptakeByTemperature dx_adsorpt_dT
      "Partial derivative of uptake w.r.t. temperature at constant pressure"
      annotation (Dialog(tab="General", group="Inputs"));

    input Modelica.Units.SI.SpecificVolume v_adsorpt
      "Specific volume of the adsorpt"
      annotation (Dialog(tab="General", group="Inputs"));
    input SorpLib.Units.DerSpecificVolumeByTemperature dv_adsorpt_dT
      "Partial derivative of specific volume of the adsorpt w.r.t. temperature at
    constant pressure"
      annotation (Dialog(tab="General", group="Inputs"));

    input Modelica.Units.SI.RelativePressureCoefficient beta_adsorpt
      "Isobaric expansion coefficient of the adsorpt"
      annotation (Dialog(tab="General", group="Inputs"));
    input SorpLib.Units.DerIsobaricExpansionCoefficientByTemperature dbeta_adsorpt_dT
      "Partial derivative of isobaric expansion coefficient of the adsorpt phase
    w.r.t. temperature at constant pressure"
      annotation (Dialog(tab="General", group="Inputs"));

    input
      SorpLib.Media.WorkingPairs.Records.PropertiesPureAdsorptive properties_adsorptive
      "Record containing all properties of the adsorptive required for the working
    pair model"
      annotation (Dialog(tab="General",group="Inputs"));

    //
    // Definition of outputs
    //
    output Modelica.Units.SI.SpecificHeatCapacity dh_ads_dT
      "Partial derivative of molar adsorption enthalpy w.r.t. temperature at constant
    pressure"
      annotation (Dialog(tab="General", group="Outputs", enable=false));

    //
    // Definition of variables
    //
protected
    SorpLib.Units.MolarAdsorptionPotential A=
      SorpLib.Media.Functions.SorptionEquilibria.Utilities.A_ppsT(
        p_adsorpt=p_adsorpt,
        p_sat=properties_adsorptive.p_sat,
        T_adsorpt=T_adsorpt)
      "Molar adsorption potential";
    SorpLib.Units.DerMolarAdsorptionPotentialByTemperature dA_dT=
      SorpLib.Media.Functions.SorptionEquilibria.Utilities.dA_dT(
        p_adsorpt=p_adsorpt,
        p_sat=properties_adsorptive.p_sat,
        T_adsorpt=T_adsorpt,
        dp_sat_dT_adsorpt=properties_adsorptive.dp_sat_dT)
      "Partial derivative of molar adsorption potential w.r.t. temperature at
    constant pressure";

    SorpLib.Units.DerFilledPoreVolumeByAdsorptionPotential dW_dA
      "Partial derivative of the characteristic curve w.r.t. the molar adsorption
    potential at constant pressure and temperaure";
    SorpLib.Units.DerFilledPoreVolumeByAdsorptionPotentialTemperature ddW_dA_dT
      "Second-order partial derivative of filled pore volume w.r.t. molar 
    adsorption potential and temperature at constant pressure";

    //
    // Annotations
    //
    annotation (Documentation(revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>",   info="<html>
<p>
This function calculates the partial derivative of the specific enthalpy of 
adsorption with respect to temperature at constant pressure according to the
model of Dubinin. For more information, check the model
<a href=\"Modelica://SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.dh_ads_dT_Dubinin\">SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.dh_ads_dT_Dubinin</a>.
</p>
</html>"));
  end calc_dh_ads_dT_p_Dubinin;

  replaceable partial function calc_integrand_WaltonLeVan_Dubinin
    "Calculates the integrand required for calculating the specific heat capacity according to Walton and Le Van (2005) when using the specifc enthalpy of adsorption according to Dubinin"
    extends Modelica.Math.Nonlinear.Interfaces.partialScalarFunction;

    //
    // Definition of inputs
    //
    input Modelica.Units.SI.Temperature T_adsorpt
      "Temperature"
      annotation (Dialog(tab="General", group="Inputs"));

    input Real c[:]
      "Coefficients of the isotherm model"
      annotation (Dialog(tab="General", group="Inputs"));
    input Real dc_dT_adsorpt[:]
      "Partial derivative of coefficients of the isotherm model w.r.t. 
    temperature"
      annotation (Dialog(tab="General", group="Inputs"));
    input Real ddc_dT_adsorpt_dT_adsorpt[:]
      "Second-order partial derivative of coefficients of the isotherm model w.r.t. 
    temperature"
      annotation (Dialog(tab="General", group="Inputs"));

    input Boolean adsorptiveAtDewPoint
      "= true, if adsorptive (gas/vapor phase) is assumed to be at dew point at
    T_adsorpt"
      annotation (Dialog(tab="General", group="Inputs"));

    //
    // Definition of inputs regarding the specific volume of the adsorpt
    //
    input Modelica.Units.SI.SpecificVolume v_adsorpt
      "Specific volume of the adsorpt"
      annotation (Dialog(tab="General",group="Inputs"));
    input SorpLib.Units.DerSpecificVolumeByPressure dv_adsorpt_dp
      "Partial derivative of the specific volume of the adsorpt w.r.t. pressure at
    constant temperature"
      annotation (Dialog(tab="General",group="Inputs"));
    input SorpLib.Units.DerSpecificVolumeByTemperature dv_adsorpt_dT
      "Partial derivative of the specific volume of the adsorpt w.r.t. temperature at
    constant pressure"
      annotation (Dialog(tab="General",group="Inputs"));

    //
    // Definition of inputs regarding the isobaric expansion coefficient of the adsorpt
    //
    input Modelica.Units.SI.RelativePressureCoefficient beta_adsorpt
      "Specific volume of the adsorpt"
      annotation (Dialog(tab="General",group="Inputs"));
    input SorpLib.Units.DerIsobaricExpansionCoefficientByPressure dbeta_adsorpt_dp
      "Partial derivative of the specific volume of the adsorpt w.r.t. pressure at
    constant temperature"
      annotation (Dialog(tab="General",group="Inputs"));
    input SorpLib.Units.DerIsobaricExpansionCoefficientByTemperature dbeta_adsorpt_dT
      "Partial derivative of the specific volume of the adsorpt w.r.t. temperature at
    constant pressure"
      annotation (Dialog(tab="General",group="Inputs"));

    //
    // Definition of inputs regarding numerics
    //
    input Modelica.Units.SI.Pressure p_clausiusClyperon
      "Maximum pressure up to which the molar adsorption enthalpy is calculated
    according to the Clausius Clyperon assumptions"
      annotation (Dialog(tab="General", group="Inputs"));
    input Modelica.Units.SI.PressureDifference dp
      "Pressure difference used to calculate partial derivatives w.r.t. pressure
    numerially (just used if analytical solution does not exist)"
      annotation (Dialog(tab="General", group="Inputs"));
    input Modelica.Units.SI.TemperatureDifference dT
      "Temperature difference used to calculate partial derivatives w.r.t. pressure
    numerially (just used if analytical solution does not exist)"
      annotation (Dialog(tab="General", group="Inputs"));

    //
    // Definition of variables
    //
protected
    Modelica.Units.SI.Pressure p_adsorpt=
      IsothermModel.p_xT(
        x_adsorpt=u,
        T_adsorpt=T_adsorpt,
        c=c,
        p_adsorpt_lb_start=1,
        p_adsorpt_ub_start=10,
        tolerance=100*Modelica.Constants.eps)
      "Pressure at x_adsorpt = u and T_adsorpt";

    SorpLib.Units.DerUptakeByPressure dx_adsorpt_dp=
      IsothermModel.dx_dp(
        p_adsorpt=p_adsorpt,
        T_adsorpt=T_adsorpt,
        c=c)
      "Partial derivative of uptake w.r.t. pressure at constant temperature";
    SorpLib.Units.DerUptakeByTemperature dx_adsorpt_dT=
      IsothermModel.dx_dT(
        p_adsorpt=p_adsorpt,
        T_adsorpt=T_adsorpt,
        c=c,
        dc_dT_adsorpt=dc_dT_adsorpt)
      "Partial derivative of uptake w.r.t. temperature at constant pressure";

    SorpLib.Units.DerUptakeByPressurePressure ddx_adsorpt_dp_dp=
      IsothermModel.ddx_dp_dp(
        p_adsorpt=p_adsorpt,
        T_adsorpt=T_adsorpt,
        c=c)
      "Second-order partial derivative of uptake w.r.t. pressure at contant 
    temperature";
    SorpLib.Units.DerUptakeByTemperatureTemperature ddx_adsorpt_dT_dT=
      IsothermModel.ddx_dT_dT(
        p_adsorpt=p_adsorpt,
        T_adsorpt=T_adsorpt,
        c=c,
        dc_dT_adsorpt=dc_dT_adsorpt,
        ddc_dT_adsorpt_dT_adsorpt=ddc_dT_adsorpt_dT_adsorpt)
      "Second-order partial derivative of uptake w.r.t. temperature at contant 
    pressure";
    SorpLib.Units.DerUptakeByPressureTemperature ddx_adsorpt_dp_dT=
      IsothermModel.ddx_dp_dT(
        p_adsorpt=p_adsorpt,
        T_adsorpt=T_adsorpt,
        c=c,
        dc_dT_adsorpt=dc_dT_adsorpt)
      "Second-order partial derivative of uptake w.r.t. pressure and temperature";

    SorpLib.Units.MolarAdsorptionPotential A=
      SorpLib.Media.Functions.SorptionEquilibria.Utilities.A_ppsT(
        p_adsorpt=p_adsorpt,
        p_sat=c[1],
        T_adsorpt=T_adsorpt)
      "Molar adsorption potential";
    SorpLib.Units.DerMolarAdsorptionPotentialByPressure dA_dp=
      SorpLib.Media.Functions.SorptionEquilibria.Utilities.dA_dp(
        p_adsorpt=p_adsorpt,
        p_sat=c[1],
        T_adsorpt=T_adsorpt)
      "Partial derivative of molar adsorption potential w.r.t. pressure at
    constant temperature";
    SorpLib.Units.DerMolarAdsorptionPotentialByTemperature dA_dT=
      SorpLib.Media.Functions.SorptionEquilibria.Utilities.dA_dT(
        p_adsorpt=p_adsorpt,
        p_sat=c[1],
        T_adsorpt=T_adsorpt,
        dp_sat_dT_adsorpt=dc_dT_adsorpt[1])
      "Partial derivative of molar adsorption potential w.r.t. temperature at
    constant pressure";

    SorpLib.Units.DerFilledPoreVolumeByAdsorptionPotential dW_dA
      "Partial derivative of filled pore volume w.r.t. molar adsorption potential
    at constant pressure and temperature";
    SorpLib.Units.DerFilledPoreVolumeByAdsorptionPotentialAdsorptionPotential ddW_dA_dA
      "Partial derivative of filled pore volume w.r.t. molar adsorption potential
    at constant pressure and temperature";
    SorpLib.Units.DerFilledPoreVolumeByAdsorptionPotentialTemperature ddW_dA_dT
      "Second-order partial derivative of filled pore volume w.r.t. molar adsorption 
    potential and temperature at constant pressure";

    Modelica.Units.SI.SpecificEnthalpy h_adsorptiveToLiquid
      "Specific enthalpy difference between adsorptive state and saturated liquid
    state (i.e., bubble point)";
    SorpLib.Units.DerSpecificEnthalpyByPressure dh_adsorptiveToLiquid_dp
      "Partial derivative of specific enthalpy difference between adsorptive state 
    and saturated liquid state (i.e., bubble point) w.r.t. pressure at constant 
    temperature";
    Modelica.Units.SI.SpecificHeatCapacity dh_adsorptiveToLiquid_dT
      "Partial derivative of specific enthalpy difference between adsorptive state 
    and saturated liquid state (i.e., bubble point) w.r.t. temperature at constant
    pressure";

    SorpLib.Units.DerMolarEnthalpyByPressure dh_ads_dp_T
      "Partial derivative of molar adsorption enthalpy w.r.t. pressure at constant
    temperature";
    Modelica.Units.SI.MolarHeatCapacity dh_ads_dT_p
      "Partial derivative of molar adsorption enthalpy w.r.t. temperature at constant
    pressure";

    //
    // Annotations
    //
    annotation (Documentation(info="<html>
<p>
This function calculates the partial derivative of the molar adsorption enthalpy
with respect to temperature at constant uptake when using the molar adsorption
enthalpy according to Dubinin. For more information, check the model
<a href=\"Modelica://SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.Integrands.dh_ads_dT_x_Dubinin\">SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.Integrands.dh_ads_dT_x_Dubinin</a>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
  end calc_integrand_WaltonLeVan_Dubinin;

  replaceable partial function calc_integrand_SchwambergerSchmidt_Dubinin
    "Calculates the integrand required for calculating the specific heat capacity according to Schwamberger and Schmidt (2013) when using the specifc enthalpy of adsorption according to Dubinin"
    extends Modelica.Math.Nonlinear.Interfaces.partialScalarFunction;

    //
    // Definition of inputs
    //
    input Modelica.Units.SI.Temperature T_adsorpt
      "Temperature"
      annotation (Dialog(tab="General", group="Inputs"));

    input Real c[:]
      "Coefficients of the isotherm model"
      annotation (Dialog(tab="General", group="Inputs"));
    input Real dc_dT_adsorpt[:]
      "Partial derivative of coefficients of the isotherm model w.r.t. 
    temperature"
      annotation (Dialog(tab="General", group="Inputs"));
    input Real ddc_dT_adsorpt_dT_adsorpt[:]
      "Second-order partial derivative of coefficients of the isotherm model w.r.t. 
    temperature"
      annotation (Dialog(tab="General", group="Inputs"));

    input Boolean adsorptiveAtDewPoint
      "= true, if adsorptive (gas/vapor phase) is assumed to be at dew point at
    T_adsorpt"
      annotation (Dialog(tab="General", group="Inputs"));

    //
    // Definition of inputs regarding the specific volume of the adsorpt
    //
    input Modelica.Units.SI.SpecificVolume v_adsorpt
      "Specific volume of the adsorpt"
      annotation (Dialog(tab="General",group="Inputs"));
    input SorpLib.Units.DerSpecificVolumeByPressure dv_adsorpt_dp
      "Partial derivative of the specific volume of the adsorpt w.r.t. pressure at
    constant temperature"
      annotation (Dialog(tab="General",group="Inputs"));
    input SorpLib.Units.DerSpecificVolumeByTemperature dv_adsorpt_dT
      "Partial derivative of the specific volume of the adsorpt w.r.t. temperature at
    constant pressure"
      annotation (Dialog(tab="General",group="Inputs"));

    //
    // Definition of inputs regarding the isobaric expansion coefficient of the adsorpt
    //
    input Modelica.Units.SI.RelativePressureCoefficient beta_adsorpt
      "Specific volume of the adsorpt"
      annotation (Dialog(tab="General",group="Inputs"));
    input SorpLib.Units.DerIsobaricExpansionCoefficientByPressure dbeta_adsorpt_dp
      "Partial derivative of the specific volume of the adsorpt w.r.t. pressure at
    constant temperature"
      annotation (Dialog(tab="General",group="Inputs"));
    input SorpLib.Units.DerIsobaricExpansionCoefficientByTemperature dbeta_adsorpt_dT
      "Partial derivative of the specific volume of the adsorpt w.r.t. temperature at
    constant pressure"
      annotation (Dialog(tab="General",group="Inputs"));

    //
    // Definition of inputs regarding numerics
    //
    input Modelica.Units.SI.Pressure p_clausiusClyperon
      "Maximum pressure up to which the molar adsorption enthalpy is calculated
    according to the Clausius Clyperon assumptions"
      annotation (Dialog(tab="General", group="Inputs"));
    input Modelica.Units.SI.PressureDifference dp
      "Pressure difference used to calculate partial derivatives w.r.t. pressure
    numerially (just used if analytical solution does not exist)"
      annotation (Dialog(tab="General", group="Inputs"));
    input Modelica.Units.SI.TemperatureDifference dT
      "Temperature difference used to calculate partial derivatives w.r.t. pressure
    numerially (just used if analytical solution does not exist)"
      annotation (Dialog(tab="General", group="Inputs"));

    //
    // Definition of variables
    //
protected
    Modelica.Units.SI.Pressure p_adsorpt=
      IsothermModel.p_xT(
        x_adsorpt=u,
        T_adsorpt=T_adsorpt,
        c=c,
        p_adsorpt_lb_start=1,
        p_adsorpt_ub_start=10,
        tolerance=100*Modelica.Constants.eps)
      "Pressure at x_adsorpt = u and T_adsorpt";

    SorpLib.Units.DerUptakeByPressure dx_adsorpt_dp=
      IsothermModel.dx_dp(
        p_adsorpt=p_adsorpt,
        T_adsorpt=T_adsorpt,
        c=c)
      "Partial derivative of uptake w.r.t. pressure at constant temperature";
    SorpLib.Units.DerUptakeByTemperature dx_adsorpt_dT=
      IsothermModel.dx_dT(
        p_adsorpt=p_adsorpt,
        T_adsorpt=T_adsorpt,
        c=c,
        dc_dT_adsorpt=dc_dT_adsorpt)
      "Partial derivative of uptake w.r.t. temperature at constant pressure";

    SorpLib.Units.DerUptakeByPressurePressure ddx_adsorpt_dp_dp=
      IsothermModel.ddx_dp_dp(
        p_adsorpt=p_adsorpt,
        T_adsorpt=T_adsorpt,
        c=c)
      "Second-order partial derivative of uptake w.r.t. pressure at contant 
    temperature";
    SorpLib.Units.DerUptakeByTemperatureTemperature ddx_adsorpt_dT_dT=
      IsothermModel.ddx_dT_dT(
        p_adsorpt=p_adsorpt,
        T_adsorpt=T_adsorpt,
        c=c,
        dc_dT_adsorpt=dc_dT_adsorpt,
        ddc_dT_adsorpt_dT_adsorpt=ddc_dT_adsorpt_dT_adsorpt)
      "Second-order partial derivative of uptake w.r.t. temperature at contant 
    pressure";
    SorpLib.Units.DerUptakeByPressureTemperature ddx_adsorpt_dp_dT=
      IsothermModel.ddx_dp_dT(
        p_adsorpt=p_adsorpt,
        T_adsorpt=T_adsorpt,
        c=c,
        dc_dT_adsorpt=dc_dT_adsorpt)
      "Second-order partial derivative of uptake w.r.t. pressure and temperature";

    Modelica.Units.SI.SpecificHeatCapacity dh_adsorptive_dT
      "Partial derivative of the specific enthalpy of the adsorptive w.r.t. temperature
    at constant pressure";

    SorpLib.Units.MolarAdsorptionPotential A=
      SorpLib.Media.Functions.SorptionEquilibria.Utilities.A_ppsT(
        p_adsorpt=p_adsorpt,
        p_sat=c[1],
        T_adsorpt=T_adsorpt)
      "Molar adsorption potential";
    SorpLib.Units.DerMolarAdsorptionPotentialByPressure dA_dp=
      SorpLib.Media.Functions.SorptionEquilibria.Utilities.dA_dp(
        p_adsorpt=p_adsorpt,
        p_sat=c[1],
        T_adsorpt=T_adsorpt)
      "Partial derivative of molar adsorption potential w.r.t. pressure at
    constant temperature";
    SorpLib.Units.DerMolarAdsorptionPotentialByTemperature dA_dT=
      SorpLib.Media.Functions.SorptionEquilibria.Utilities.dA_dT(
        p_adsorpt=p_adsorpt,
        p_sat=c[1],
        T_adsorpt=T_adsorpt,
        dp_sat_dT_adsorpt=dc_dT_adsorpt[1])
      "Partial derivative of molar adsorption potential w.r.t. temperature at
    constant pressure";

    SorpLib.Units.DerFilledPoreVolumeByAdsorptionPotential dW_dA
      "Partial derivative of filled pore volume w.r.t. molar adsorption potential
    at constant pressure and temperature";
    SorpLib.Units.DerFilledPoreVolumeByAdsorptionPotentialAdsorptionPotential ddW_dA_dA
      "Partial derivative of filled pore volume w.r.t. molar adsorption potential
    at constant pressure and temperature";
    SorpLib.Units.DerFilledPoreVolumeByAdsorptionPotentialTemperature ddW_dA_dT
      "Second-order partial derivative of filled pore volume w.r.t. molar adsorption 
    potential and temperature at constant pressure";

    Modelica.Units.SI.SpecificEnthalpy h_adsorptiveToLiquid
      "Specific enthalpy difference between adsorptive state and saturated liquid
    state (i.e., bubble point)";
    SorpLib.Units.DerSpecificEnthalpyByPressure dh_adsorptiveToLiquid_dp
      "Partial derivative of specific enthalpy difference between adsorptive state 
    and saturated liquid state (i.e., bubble point) w.r.t. pressure at constant 
    temperature";
    Modelica.Units.SI.SpecificHeatCapacity dh_adsorptiveToLiquid_dT
      "Partial derivative of specific enthalpy difference between adsorptive state 
    and saturated liquid state (i.e., bubble point) w.r.t. temperature at constant
    pressure";

    SorpLib.Units.DerMolarEnthalpyByPressure dh_ads_dp_T
      "Partial derivative of molar adsorption enthalpy w.r.t. pressure at constant
    temperature";
    Modelica.Units.SI.MolarHeatCapacity dh_ads_dT_p
      "Partial derivative of molar adsorption enthalpy w.r.t. temperature at constant
    pressure";

    //
    // Annotations
    //
    annotation (Documentation(info="<html>
<p>
This function calculates the partial derivative of the specific enthalpy of the 
adsorpt with respect to temperature at constant uptake when using the molar adsorption
enthalpy according to Dubinin. For more information, check the model
<a href=\"Modelica://SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.Integrands.dh_adsorpt_dT_x_Dubinin\">SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.Integrands.dh_adsorpt_dT_x_Dubinin</a>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
  end calc_integrand_SchwambergerSchmidt_Dubinin;

  replaceable partial function calc_integrand_h_avg_adsorpt_Dubinin
    "Calculates the integrand required for calculating the uptake-averaged specific enthalpy of adsorpt using the adsorption enthalpy according to Dubinin"
    extends Modelica.Math.Nonlinear.Interfaces.partialScalarFunction;

    //
    // Definition of inputs
    //
    input Modelica.Units.SI.Temperature T_adsorpt
      "Temperature"
      annotation (Dialog(tab="General", group="Inputs"));

    input Real c[:]
      "Coefficients of the isotherm model"
      annotation (Dialog(tab="General", group="Inputs"));
    input Real dc_dT_adsorpt[:]
      "Partial derivative of coefficients of the isotherm model w.r.t. 
    temperature"
      annotation (Dialog(tab="General", group="Inputs"));

    input Boolean adsorptiveAtDewPoint
      "= true, if adsorptive (gas/vapor phase) is assumed to be at dew point at
    T_adsorpt"
      annotation (Dialog(tab="General", group="Inputs"));

    //
    // Definition of inputs regarding the specific volume of the adsorpt
    //
    input Modelica.Units.SI.SpecificVolume v_adsorpt
      "Specific volume of the adsorpt"
      annotation (Dialog(tab="General",group="Inputs"));

    //
    // Definition of inputs regarding the isobaric expansion coefficient of the adsorpt
    //
    input Modelica.Units.SI.RelativePressureCoefficient beta_adsorpt
      "Specific volume of the adsorpt"
      annotation (Dialog(tab="General",group="Inputs"));

    //
    // Definition of inputs regarding numerics
    //
    input Modelica.Units.SI.Pressure p_clausiusClyperon
      "Maximum pressure up to which the molar adsorption enthalpy is calculated
    according to the Clausius Clyperon assumptions"
      annotation (Dialog(tab="General", group="Inputs"));
    input Modelica.Units.SI.PressureDifference dp
      "Pressure difference used to calculate partial derivatives w.r.t. pressure
    numerially (just used if analytical solution does not exist)"
      annotation (Dialog(tab="General", group="Inputs"));
    input Modelica.Units.SI.TemperatureDifference dT
      "Temperature difference used to calculate partial derivatives w.r.t. pressure
    numerially (just used if analytical solution does not exist)"
      annotation (Dialog(tab="General", group="Inputs"));

    //
    // Definition of variables
    //
protected
    Modelica.Units.SI.Pressure p_adsorpt=
      IsothermModel.p_xT(
        x_adsorpt=u,
        T_adsorpt=T_adsorpt,
        c=c,
        p_adsorpt_lb_start=1,
        p_adsorpt_ub_start=10,
        tolerance=100*Modelica.Constants.eps)
      "Pressure at x_adsorpt = u and T_adsorpt";

    Modelica.Units.SI.SpecificEnthalpy h_adsorptive
      "Specific enthalpy of the adsorptive";

    SorpLib.Units.DerUptakeByPressure dx_adsorpt_dp=
      IsothermModel.dx_dp(
        p_adsorpt=p_adsorpt,
        T_adsorpt=T_adsorpt,
        c=c)
      "Partial derivative of uptake w.r.t. pressure at constant temperature";
    SorpLib.Units.DerUptakeByTemperature dx_adsorpt_dT=
      IsothermModel.dx_dT(
        p_adsorpt=p_adsorpt,
        T_adsorpt=T_adsorpt,
        c=c,
        dc_dT_adsorpt=dc_dT_adsorpt)
      "Partial derivative of uptake w.r.t. temperature at constant pressure";

    SorpLib.Units.MolarAdsorptionPotential A=
      SorpLib.Media.Functions.SorptionEquilibria.Utilities.A_ppsT(
        p_adsorpt=p_adsorpt,
        p_sat=c[1],
        T_adsorpt=T_adsorpt)
      "Molar adsorption potential";
    SorpLib.Units.DerFilledPoreVolumeByAdsorptionPotential dW_dA
      "Partial derivative of filled pore volume w.r.t. molar adsorption potential
    at constant pressure and temperature";

    Modelica.Units.SI.SpecificEnthalpy h_adsorptiveToLiquid
      "Specific enthalpy difference between adsorptive state and saturated liquid
    state (i.e., bubble point)";

    Modelica.Units.SI.MolarEnthalpy h_ads
      "Molar adsorption enthalpy";


    //
    // Annotations
    //
    annotation (Documentation(info="<html>
<p>
This function calculates the specific enthalpy difference between the adsorptive
and adsorpt using the specific enthalpy of adsorption according to Dubinin.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
  end calc_integrand_h_avg_adsorpt_Dubinin;



  replaceable partial function calc_integrand_s_avg_adsorpt_Dubinin
    "Calculates the integrand required for calculating the uptake-averaged specific entropy of adsorpt using the adsorption enthalpy according to Dubinin"
    extends Modelica.Math.Nonlinear.Interfaces.partialScalarFunction;

    //
    // Definition of inputs
    //
    input Modelica.Units.SI.Temperature T_adsorpt
      "Temperature"
      annotation (Dialog(tab="General", group="Inputs"));

    input Real c[:]
      "Coefficients of the isotherm model"
      annotation (Dialog(tab="General", group="Inputs"));
    input Real dc_dT_adsorpt[:]
      "Partial derivative of coefficients of the isotherm model w.r.t. 
    temperature"
      annotation (Dialog(tab="General", group="Inputs"));

    input Boolean adsorptiveAtDewPoint
      "= true, if adsorptive (gas/vapor phase) is assumed to be at dew point at
    T_adsorpt"
      annotation (Dialog(tab="General", group="Inputs"));

    //
    // Definition of inputs regarding the specific volume of the adsorpt
    //
    input Modelica.Units.SI.SpecificVolume v_adsorpt
      "Specific volume of the adsorpt"
      annotation (Dialog(tab="General",group="Inputs"));

    //
    // Definition of inputs regarding the isobaric expansion coefficient of the adsorpt
    //
    input Modelica.Units.SI.RelativePressureCoefficient beta_adsorpt
      "Specific volume of the adsorpt"
      annotation (Dialog(tab="General",group="Inputs"));

    //
    // Definition of inputs regarding numerics
    //
    input Modelica.Units.SI.Pressure p_clausiusClyperon
      "Maximum pressure up to which the molar adsorption enthalpy is calculated
    according to the Clausius Clyperon assumptions"
      annotation (Dialog(tab="General", group="Inputs"));
    input Modelica.Units.SI.PressureDifference dp
      "Pressure difference used to calculate partial derivatives w.r.t. pressure
    numerially (just used if analytical solution does not exist)"
      annotation (Dialog(tab="General", group="Inputs"));
    input Modelica.Units.SI.TemperatureDifference dT
      "Temperature difference used to calculate partial derivatives w.r.t. pressure
    numerially (just used if analytical solution does not exist)"
      annotation (Dialog(tab="General", group="Inputs"));

    //
    // Definition of variables
    //
protected
    Modelica.Units.SI.Pressure p_adsorpt=
      IsothermModel.p_xT(
        x_adsorpt=u,
        T_adsorpt=T_adsorpt,
        c=c,
        p_adsorpt_lb_start=1,
        p_adsorpt_ub_start=10,
        tolerance=100*Modelica.Constants.eps)
      "Pressure at x_adsorpt = u and T_adsorpt";

    Modelica.Units.SI.SpecificEntropy s_adsorptive
      "Specific entropy of the adsorptive";

    SorpLib.Units.DerUptakeByPressure dx_adsorpt_dp=
      IsothermModel.dx_dp(
        p_adsorpt=p_adsorpt,
        T_adsorpt=T_adsorpt,
        c=c)
      "Partial derivative of uptake w.r.t. pressure at constant temperature";
    SorpLib.Units.DerUptakeByTemperature dx_adsorpt_dT=
      IsothermModel.dx_dT(
        p_adsorpt=p_adsorpt,
        T_adsorpt=T_adsorpt,
        c=c,
        dc_dT_adsorpt=dc_dT_adsorpt)
      "Partial derivative of uptake w.r.t. temperature at constant pressure";

    SorpLib.Units.MolarAdsorptionPotential A=
      SorpLib.Media.Functions.SorptionEquilibria.Utilities.A_ppsT(
        p_adsorpt=p_adsorpt,
        p_sat=c[1],
        T_adsorpt=T_adsorpt)
      "Molar adsorption potential";
    SorpLib.Units.DerFilledPoreVolumeByAdsorptionPotential dW_dA
      "Partial derivative of filled pore volume w.r.t. molar adsorption potential
    at constant pressure and temperature";

    Modelica.Units.SI.SpecificEnthalpy h_adsorptiveToLiquid
      "Specific enthalpy difference between adsorptive state and saturated liquid
    state (i.e., bubble point)";

    Modelica.Units.SI.MolarEnthalpy h_ads
      "Molar adsorption enthalpy";

    //
    // Annotations
    //
    annotation (Documentation(info="<html>
<p>
This function calculates the specific entropy difference between the adsorptive
and adsorpt using the specific enthalpy of adsorption according to Dubinin.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
  end calc_integrand_s_avg_adsorpt_Dubinin;
  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial package contains all declaration for parametrization of a pure component working
pair. This means that constants, packages, models, and functions are defined that every pure
component working pair must support. Note that only some of the medium specific functions are 
optional (see 
<a href=\"Modelica://SorpLib.Media.WorkingPairs.BaseClasses.PartialPureMediumSpecificFunctions\">SorpLib.Media.WorkingPairs.BaseClasses.PartialPureMediumSpecificFunctions</a>).
Packages that inherit properties from this partial package must redeclare all constants, the
two partial packages <i>IsothermModel</i> and <i>MediumSpecificFunctions</i>, the model <i>Sorbent</i>, 
the two functions <i>calc_c</i> and <i>calc_coefficients</i>, and optionally the five functions
<i>calc_h_ads_Dubinin</i>, <i>calc_dh_ads_dp_T_Dubinin</i>, <i>calc_dh_ads_dT_dp_Dubinin</i>, 
<i>calc_integrand_WaltonLeVan_Dubinin</i>, and <i>calc_integrand_SchwambergerSchmidt_Dubinin</i>
(if using the model of Dubinin).
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end PartialPureParametrization;
