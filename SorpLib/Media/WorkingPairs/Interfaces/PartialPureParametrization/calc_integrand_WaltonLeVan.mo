within SorpLib.Media.WorkingPairs.Interfaces.PartialPureParametrization;
function calc_integrand_WaltonLeVan
  "Calculates the integrand required for calculating the specific heat capacity according to Walton and Le Van (2005)"
  extends Modelica.Math.Nonlinear.Interfaces.partialScalarFunction;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.Temperature T_adsorpt
    "Temperature"
    annotation (Dialog(tab="General", group="Inputs"));

  input Real c[:]
    "Coefficients of the isotherm model"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real dc_dT_adsorpt[:]
    "Partial derivative of coefficients of the isotherm model w.r.t. 
    temperature"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real ddc_dT_adsorpt_dT_adsorpt[:]
    "Second-order partial derivative of coefficients of the isotherm model w.r.t. 
    temperature"
    annotation (Dialog(tab="General", group="Inputs"));

  input Boolean adsorptiveAtDewPoint
    "= true, if adsorptive (gas/vapor phase) is assumed to be at dew point at
    T_adsorpt"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of inputs regarding the specific volume of the adsorpt
  //
  input Modelica.Units.SI.SpecificVolume v_adsorpt
    "Specific volume of the adsorpt"
    annotation (Dialog(tab="General",group="Inputs"));
  input SorpLib.Units.DerSpecificVolumeByPressure dv_adsorpt_dp
    "Partial derivative of the specific volume of the adsorpt w.r.t. pressure at
    constant temperature"
    annotation (Dialog(tab="General",group="Inputs"));
  input SorpLib.Units.DerSpecificVolumeByTemperature dv_adsorpt_dT
    "Partial derivative of the specific volume of the adsorpt w.r.t. temperature at
    constant pressure"
    annotation (Dialog(tab="General",group="Inputs"));

  //
  // Definition of inputs regarding numerics
  //
  input Modelica.Units.SI.Pressure p_clausiusClyperon
    "Maximum pressure up to which the molar adsorption enthalpy is calculated
    according to the Clausius Clyperon assumptions"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.PressureDifference dp
    "Pressure difference used to calculate partial derivatives w.r.t. pressure
    numerially (just used if analytical solution does not exist)"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.TemperatureDifference dT
    "Temperature difference used to calculate partial derivatives w.r.t. pressure
    numerially (just used if analytical solution does not exist)"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of variables
  //
protected
  Modelica.Units.SI.Pressure p_adsorpt
    "Pressure at x_adsorpt = u and T_adsorpt";

  Modelica.Units.SI.SpecificVolume v_adsorptive
    "Specific volume of the adsorptive";
  SorpLib.Units.DerSpecificVolumeByPressure dv_adsorptive_dp
    "Partial derivative of specific volume of the adsorptive w.r.t. pressure at
    constant temperature";
  SorpLib.Units.DerSpecificVolumeByTemperature dv_adsorptive_dT
    "Partial derivative of specific volume of the adsorptive w.r.t. temperature at
    constant pressure";

  SorpLib.Units.DerUptakeByPressure dx_adsorpt_dp
    "Partial derivative of uptake w.r.t. pressure at constant temperature";
  SorpLib.Units.DerUptakeByTemperature dx_adsorpt_dT
    "Partial derivative of uptake w.r.t. temperature at constant pressure";

  SorpLib.Units.DerUptakeByPressurePressure ddx_adsorpt_dp_dp
    "Second-order partial derivative of uptake w.r.t. pressure at contant 
    temperature";
  SorpLib.Units.DerUptakeByTemperatureTemperature ddx_adsorpt_dT_dT
    "Second-order partial derivative of uptake w.r.t. temperature at contant 
    pressure";
  SorpLib.Units.DerUptakeByPressureTemperature ddx_adsorpt_dp_dT
    "Second-order partial derivative of uptake w.r.t. pressure and temperature";

  SorpLib.Units.DerMolarEnthalpyByPressure dh_ads_dp_T
    "Partial derivative of molar adsorption enthalpy w.r.t. pressure at constant
    temperature";
  Modelica.Units.SI.MolarHeatCapacity dh_ads_dT_p
    "Partial derivative of molar adsorption enthalpy w.r.t. temperature at constant
    pressure";

algorithm
  //
  // Calculation of sorption equilibrium
  //
  p_adsorpt := IsothermModel.p_xT(
      x_adsorpt=u,
      T_adsorpt=T_adsorpt,
      c=c,
      p_adsorpt_lb_start=1,
      p_adsorpt_ub_start=10,
      tolerance=100*Modelica.Constants.eps)
    "Pressure at x_adsorpt = u and T_adsorpt";

  dx_adsorpt_dp := IsothermModel.dx_dp(
      p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt,
      c=c)
    "Partial derivative of uptake w.r.t. pressure at constant temperature";
  dx_adsorpt_dT := IsothermModel.dx_dT(
      p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt,
      c=c,
      dc_dT_adsorpt=dc_dT_adsorpt)
    "Partial derivative of uptake w.r.t. temperature at constant pressure";

  ddx_adsorpt_dp_dp := IsothermModel.ddx_dp_dp(
      p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt,
      c=c)
    "Second-order partial derivative of uptake w.r.t. pressure at constant temperature";
  ddx_adsorpt_dT_dT := IsothermModel.ddx_dT_dT(
      p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt,
      c=c,
      dc_dT_adsorpt=dc_dT_adsorpt,
      ddc_dT_adsorpt_dT_adsorpt=ddc_dT_adsorpt_dT_adsorpt)
    "Second-order partial derivative of uptake w.r.t. temperature at constant pressure";
  ddx_adsorpt_dp_dT := IsothermModel.ddx_dp_dT(
      p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt,
      c=c,
      dc_dT_adsorpt=dc_dT_adsorpt)
    "Second-order partial derivative of uptake w.r.t. pressure and temperature";

  //
  // Calculation of further properties
  //
  if p_adsorpt > p_clausiusClyperon then
    //
    // Do not use Clausius Clyperon assumptions
    //
    (v_adsorptive, dv_adsorptive_dp, dv_adsorptive_dT,,,,,,) :=
      MediumSpecificFunctions.calc_properties(
        p=p_adsorpt,
        T=T_adsorpt,
        dp=dp,
        dT=dT,
        p_min=p_clausiusClyperon,
        require_v_adsorptive=true,
        require_h_adsorptive=false,
        require_s_adsorptive=false,
        require_dh_adsorptive_dT_dp=false,
        require_h_adsorptiveToLiquid=false,
        adsorptiveAtDewPoint=adsorptiveAtDewPoint)
      "Specific volume of the adsorptive and its partial derivatives";

    //
    // Calculation of partial derivatives of molar sorption enthalpy
    //
    dh_ads_dp_T :=
      SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.dh_ads_dp(
        M_adsorptive=M_adsorptive,
        T_adsorpt=T_adsorpt,
        v_adsorptive=v_adsorptive,
        v_adsorpt=v_adsorpt,
        dv_adsorptive_dp=dv_adsorptive_dp,
        dv_adsorpt_dp=dv_adsorpt_dp,
        dx_adsorpt_dp=dx_adsorpt_dp,
        dx_adsorpt_dT=dx_adsorpt_dT,
        ddx_adsorpt_dp_dp=ddx_adsorpt_dp_dp,
        ddx_adsorpt_dp_dT=ddx_adsorpt_dp_dT)
    "Partial derivative of molar adsorption enthalpy w.r.t. pressure at constant
    temperature";
    dh_ads_dT_p :=
      SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.dh_ads_dT(
        M_adsorptive=M_adsorptive,
        T_adsorpt=T_adsorpt,
        v_adsorptive=v_adsorptive,
        v_adsorpt=v_adsorpt,
        dv_adsorptive_dT=dv_adsorptive_dT,
        dv_adsorpt_dT=dv_adsorpt_dT,
        dx_adsorpt_dp=dx_adsorpt_dp,
        dx_adsorpt_dT=dx_adsorpt_dT,
        ddx_adsorpt_dT_dT=ddx_adsorpt_dT_dT,
        ddx_adsorpt_dp_dT=ddx_adsorpt_dp_dT)
      "Partial derivative of molar adsorption enthalpy w.r.t. temperature at constant
      pressure";

  else
    //
    // Use Clausius-Clyperon assumptions: No further propiertes are required
    //
    dh_ads_dp_T :=
      SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.dh_ads_dp_clausiusClapeyron(
        p_adsorpt=p_adsorpt,
        T_adsorpt=T_adsorpt,
        dx_adsorpt_dp=dx_adsorpt_dp,
        dx_adsorpt_dT=dx_adsorpt_dT,
        ddx_adsorpt_dp_dp=ddx_adsorpt_dp_dp,
        ddx_adsorpt_dp_dT=ddx_adsorpt_dp_dT)
    "Partial derivative of molar adsorption enthalpy w.r.t. pressure at constant
    temperature";
    dh_ads_dT_p :=
      SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.dh_ads_dT_clausiusClapeyron(
        p_adsorpt=p_adsorpt,
        T_adsorpt=T_adsorpt,
        dx_adsorpt_dp=dx_adsorpt_dp,
        dx_adsorpt_dT=dx_adsorpt_dT,
        ddx_adsorpt_dp_dT=ddx_adsorpt_dp_dT,
        ddx_adsorpt_dT_dT=ddx_adsorpt_dT_dT)
      "Partial derivative of molar adsorption enthalpy w.r.t. temperature at constant
      pressure";

  end if;

  //
  // Calculation of the integrand
  //
  y := dh_ads_dT_p - dh_ads_dp_T * dx_adsorpt_dT / dx_adsorpt_dp
    "Integrand: Partial derivative of molar sorption enthalpy w.r.t. temperature
    at constant uptake";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function calculates the partial derivative of the molar adsorption enthalpy
with respect to temperature at constant uptake. For more information, check the model
<a href=\"Modelica://SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.Integrands.dh_ads_dT_x\">SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.Integrands.dh_ads_dT_x</a>.
</p>
</html>",
        revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end calc_integrand_WaltonLeVan;
