within SorpLib.Media.WorkingPairs.Interfaces.PartialPureParametrization;
function calc_integrand_SchwambergerSchmidt_Constant
  "Calculates the integrand required for calculating the specific heat capacity according to Schwamberger and Schmidt (2013) when having a constant specific enthaloy of adsorption"
  extends Modelica.Math.Nonlinear.Interfaces.partialScalarFunction;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.Temperature T_adsorpt
    "Temperature"
    annotation (Dialog(tab="General", group="Inputs"));

  input Real c[:]
    "Coefficients of the isotherm model"
    annotation (Dialog(tab="General", group="Inputs"));

  input Boolean adsorptiveAtDewPoint
    "= true, if adsorptive (gas/vapor phase) is assumed to be at dew point at
    T_adsorpt"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of inputs regarding numerics
  //
  input Modelica.Units.SI.Pressure p_clausiusClyperon
    "Maximum pressure up to which the molar adsorption enthalpy is calculated
    according to the Clausius Clyperon assumptions"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.PressureDifference dp
    "Pressure difference used to calculate partial derivatives w.r.t. pressure
    numerially (just used if analytical solution does not exist)"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.TemperatureDifference dT
    "Temperature difference used to calculate partial derivatives w.r.t. pressure
    numerially (just used if analytical solution does not exist)"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of variables
  //
protected
  Modelica.Units.SI.Pressure p_adsorpt
    "Pressure at x_adsorpt = u and T_adsorpt";

  Modelica.Units.SI.SpecificHeatCapacity dh_adsorptive_dT
    "Partial derivative of the specific enthalpy of the adsorptive w.r.t. temperature
    at constant pressure";

algorithm
  //
  // Calculation of sorption equilibrium
  //
  p_adsorpt := IsothermModel.p_xT(
      x_adsorpt=u,
      T_adsorpt=T_adsorpt,
      c=c,
      p_adsorpt_lb_start=1,
      p_adsorpt_ub_start=10,
      tolerance=100*Modelica.Constants.eps)
    "Pressure at x_adsorpt = u and T_adsorpt";

  //
  // Calculation of further properties
  //
  (,,,,,  dh_adsorptive_dT,,,) :=
    MediumSpecificFunctions.calc_properties(
      p=p_adsorpt,
      T=T_adsorpt,
      dp=dp,
      dT=dT,
      p_min=p_clausiusClyperon,
      require_v_adsorptive=false,
      require_h_adsorptive=false,
      require_s_adsorptive=false,
      require_dh_adsorptive_dT_dp=true,
      require_h_adsorptiveToLiquid=false,
      adsorptiveAtDewPoint=adsorptiveAtDewPoint)
    "Partial derivative of the specific enthalpy of the adsorptive w.r.t.
    temperature at constant pressure";

  //
  // Calculation of the integrand
  //
  y :=  dh_adsorptive_dT
    "Integrand: Partial derivative of molar sorption enthalpy w.r.t. temperature
    at constant uptake";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function calculates the partial derivative of the specific enthalpy of the 
adsorpt with respect to temperature at constant uptake when using a constant
specific enthalpy of adsorption.
</p>
</html>",
        revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end calc_integrand_SchwambergerSchmidt_Constant;
