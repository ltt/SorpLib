within SorpLib.Media.WorkingPairs.Interfaces.PartialPureParametrization;
model AdsorptProperties
  "Calculates all properties of the adsorpt required for the working pair model at once"
  extends Modelica.Icons.MaterialProperty;

  //
  // Definition of parameters regarding the calculation approach
  //
  parameter SorpLib.Choices.IndependentVariablesPureComponentWorkingPair
    stateVariables=SorpLib.Choices.IndependentVariablesPureComponentWorkingPair.xT
    "Independent state variables of the working pair model"
    annotation (Dialog(tab="General", group="Calculation Setup"),
                choicesAllMatching=true,
                Evaluate=true,
                HideResult=true);

  parameter Boolean calcCaloricProperties = true
    "= true, if caloric properties are calculated (i.e., h, u, h_ads, and cp)"
    annotation (Dialog(tab = "General", group = "Calculation Setup"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);
  parameter Boolean calcEntropicProperties = false
    "= true, if caloric properties are calculated (i.e., s, g, a, and s_ads)"
    annotation (Dialog(tab = "General", group = "Calculation Setup",
                enable = calcCaloricProperties),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);

  parameter Boolean calcAdsorptAdsorbateState = false
    "= true, if state properties of average adsorpt phase and adosrbate phase are
    calculated (i.e., requires numerical integration)"
    annotation (Dialog(tab = "General", group = "Calculation Setup",
                enable = calcCaloricProperties),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);

  parameter Boolean calcDerivativesIsotherm = true
    "= true, if partial derivatives of isotherms required for mass, energy, and
    entropy balance of working paur volumes are calculated"
    annotation (Dialog(tab = "General", group = "Calculation Setup"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);
  parameter Boolean calcDerivativesMassEnergyBalance = true
    "= true, if partial derivatives required for mass and energy balance of
    working pair volumes are calculated"
    annotation (Dialog(tab = "General", group = "Calculation Setup",
                enable = calcDerivativesIsotherm and calcCaloricProperties),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);
  parameter Boolean calcDerivativesEntropyBalance = false
    "= true, if partial derivatives required for entropy balance of working
    pair volumes are calculated"
    annotation (Dialog(tab = "General", group = "Calculation Setup",
                enable = calcDerivativesIsotherm and
                  calcDerivativesMassEnergyBalance and calcCaloricProperties),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);

  parameter Boolean adsorptiveAtDewPoint = false
    "= true, if adsorptive (gas/vapor phase) is assumed to be at dew point at
    T_adsorpt"
    annotation (Dialog(tab = "General", group = "Calculation Setup"),
                Evaluate=true,
                HideResult=true,
                choices(checkBox=true));

  //
  // Definition of parameters regarding sorption enthalpy
  //
  parameter SorpLib.Choices.SorptionEnthalpy approachSorptionEnthalpy=
    SorpLib.Choices.SorptionEnthalpy.Constant
    "Calculation approach for the sorption enthalpy"
    annotation (Dialog(tab="Sorption Enthalpy", group="General"),
                Evaluate=false,
                HideResult=true,
                choicesAllMatching=true);

  parameter Modelica.Units.SI.SpecificEnthalpy h_ads_constant= 3000e3
    "Constant specific enthalpy of adsorption"
    annotation (Dialog(tab="Sorption Enthalpy", group="Constant",
                enable=(approachSorptionEnthalpy==
                  SorpLib.Choices.SorptionEnthalpy.Constant)),
                HideResult=true,
                Evaluate= false);

  //
  // Definition of parameters regarding adsorpt phase's specific heat capacity
  //
  parameter SorpLib.Choices.SpecificHeatCapacityAdsorpt
    approachSpecificHeatCapacity=SorpLib.Choices.SpecificHeatCapacityAdsorpt.Constant
    "Calculation approach for the specific heat capacity of the adsorpt"
    annotation (Dialog(tab="Specific Heat Capacity", group="General"),
                Evaluate=false,
                HideResult=true,
                choicesAllMatching=true);

  parameter Modelica.Units.SI.SpecificHeatCapacity cp_adsorpt_constant= 4e3
    "Constant specific heat capacity of the adsorpt"
    annotation (Dialog(tab="Specific Heat Capacity", group="Constant",
                enable=(approachSpecificHeatCapacity==
                  SorpLib.Choices.SpecificVolumeAdsorpt.Constant)),
                HideResult=true,
                Evaluate= false);

  parameter SorpLib.Choices.GeneralizedFunctionApproach cp_adsorpt_function=
      SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionTemperature
    "Generalized function used to calculate the specific heat capacity of the adsorpt"
    annotation (Dialog(tab="Specific Heat Capacity", group="Generalized Function",
                enable=(approachSpecificHeatCapacity ==
                  SorpLib.Choices.SpecificVolumeAdsorpt.GeneralizedFunction)),
                Evaluate=true,
                HideResult=true);
  parameter Modelica.Units.SI.Temperature T_ref_cp_adsorpt = 293.15
    "Reference temperature for the specific heat capacity of the adsorpt"
    annotation (Dialog(tab="Specific Heat Capacity", group="Generalized Function",
                enable=(approachSpecificHeatCapacity==
                  SorpLib.Choices.SpecificVolumeAdsorpt.GeneralizedFunction)),
                HideResult=true,
                Evaluate= true);
  parameter Modelica.Units.SI.SpecificHeatCapacity cp_adsorpt_ref = 1
    "Reference fluid property data for the specific heat capacity of the adsorpt"
    annotation (Dialog(tab="Specific Heat Capacity", group="Generalized Function",
                enable=(approachSpecificHeatCapacity==
                  SorpLib.Choices.SpecificVolumeAdsorpt.GeneralizedFunction)),
                HideResult=true,
                Evaluate= true);
  parameter Real coefficients_cp_adsorpt[:]={cp_adsorpt_constant}
    "Coefficients of generalized function for the specific heat capacity of the adsorpt"
    annotation (Dialog(tab="Specific Heat Capacity", group="Generalized Function",
                enable=(approachSpecificHeatCapacity==
                  SorpLib.Choices.SpecificVolumeAdsorpt.GeneralizedFunction)),
                HideResult=true,
                Evaluate= true);
  parameter Real exponents_cp_adsorpt[size(coefficients_cp_adsorpt,1)]={0}
    "Exponents of generalized function for the specific heat capacity of the adsorpt"
    annotation (Dialog(tab="Specific Heat Capacity", group="Generalized Function",
                enable=(approachSpecificHeatCapacity==
                  SorpLib.Choices.SpecificVolumeAdsorpt.GeneralizedFunction)),
                HideResult=true,
                Evaluate= true);

  parameter SorpLib.Choices.InterpolationApproach cp_adsorpt_interpolation=
      SorpLib.Choices.InterpolationApproach.Linear
    "Interpolation approach used to calculate the specific heat capacity of the adsorpt"
    annotation (Dialog(tab="Specific Heat Capacity", group="Interpolation",
                enable=(approachSpecificHeatCapacity ==
                  SorpLib.Choices.SpecificVolumeAdsorpt.Interpolation)),
                Evaluate=false,
                HideResult=true);
  parameter Real abscissa_cp_adsorpt[:]={0, 1000}
    "Known abscissa values for the specific heat capacity of the adsorpt"
    annotation (Dialog(tab="Specific Heat Capacity", group="Interpolation",
                enable=(approachSpecificHeatCapacity==
                  SorpLib.Choices.SpecificVolumeAdsorpt.Interpolation)),
                HideResult=true,
                Evaluate= true);
  parameter Real ordinate_cp_adsorpt[size(abscissa_cp_adsorpt,1)]=
    {cp_adsorpt_constant, cp_adsorpt_constant}
    "Known ordinate values for the specific heat capacity of the adsorpt"
    annotation (Dialog(tab="Specific Heat Capacity", group="Interpolation",
                enable=(approachSpecificHeatCapacity==
                  SorpLib.Choices.SpecificHeatCapacityAdsorpt.Interpolation)),
                HideResult=true,
                Evaluate= true);
  parameter Real coefficients_cubicSplines_cp_adsorpt[size(abscissa_cp_adsorpt,1),4]=
    SorpLib.Media.Functions.Utilities.calcCubicSplineCoefficients(
      abscissa=abscissa_cp_adsorpt,
      ordinate=ordinate_cp_adsorpt)
    "Coefficient a to d for cubic polynomials for the specific heat capacity of the adsorpt"
    annotation (Dialog(tab="Specific Heat Capacity", group="Interpolation",
                enable=false),
                HideResult=true,
                Evaluate= true);

  //
  // Definition of parameters regarding adsorpt phase's specific volume
  //
  parameter Boolean neglectSpecifcVolume = false
    "= true, if specific volume of the adsorpt is neglected (i.e., v_adsorpt = 0
    -> u_adsorpt = h_adsorpt and g_adsorpt = a_adsorpt)"
    annotation (Dialog(tab = "Specific Volume", group = "General"),
                choices(checkBox=true),
                Evaluate= false,
                HideResult=true);
  parameter SorpLib.Choices.SpecificVolumeAdsorpt approachSpecificVolume=
      SorpLib.Choices.SpecificVolumeAdsorpt.Constant
    "Calculation approach for the specific volume of the adsorpt"
    annotation (Dialog(tab="Specific Volume", group="General",
                enable=not neglectSpecifcVolume),
                Evaluate=false,
                HideResult=true,
                choicesAllMatching=true);

  parameter Modelica.Units.SI.SpecificVolume v_adsorpt_constant= 1e-3
    "Constant specific volume of the adsorpt"
    annotation (Dialog(tab="Specific Volume", group="Constant",
                enable=(not neglectSpecifcVolume and approachSpecificVolume ==
                  SorpLib.Choices.SpecificVolumeAdsorpt.Constant)),
                HideResult=true,
                Evaluate= false);

  parameter SorpLib.Choices.GeneralizedFunctionApproach v_adsorpt_function=
      SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionTemperature
    "Generalized function used to calculate the specific volume of the adsorpt"
    annotation (Dialog(tab="Specific Volume", group="Generalized Function",
                enable=(not neglectSpecifcVolume and approachSpecificVolume ==
                  SorpLib.Choices.SpecificVolumeAdsorpt.GeneralizedFunction)),
                Evaluate=true,
                HideResult=true);
  parameter Modelica.Units.SI.Temperature T_ref_v_adsorpt = 293.15
    "Reference temperature for the specific volume of the adsorpt"
    annotation (Dialog(tab="Specific Volume", group="Generalized Function",
                enable=(not neglectSpecifcVolume and approachSpecificVolume ==
                  SorpLib.Choices.SpecificVolumeAdsorpt.GeneralizedFunction)),
                HideResult=true,
                Evaluate= true);
  parameter Modelica.Units.SI.SpecificVolume v_adsorpt_ref = 1
    "Reference fluid property data for the specific volume of the adsorpt"
    annotation (Dialog(tab="Specific Volume", group="Generalized Function",
                enable=(not neglectSpecifcVolume and approachSpecificVolume ==
                  SorpLib.Choices.SpecificVolumeAdsorpt.GeneralizedFunction)),
                HideResult=true,
                Evaluate= true);
  parameter Real coefficients_v_adsorpt[:]={v_adsorpt_constant}
    "Coefficients of generalized function for the specific volume of the adsorpt"
    annotation (Dialog(tab="Specific Volume", group="Generalized Function",
                enable=(not neglectSpecifcVolume and approachSpecificVolume ==
                  SorpLib.Choices.SpecificVolumeAdsorpt.GeneralizedFunction)),
                HideResult=true,
                Evaluate= true);
  parameter Real exponents_v_adsorpt[size(coefficients_v_adsorpt,1)]={0}
    "Exponents of generalized function for the specific volume of the adsorpt"
    annotation (Dialog(tab="Specific Volume", group="Generalized Function",
                enable=(not neglectSpecifcVolume and approachSpecificVolume ==
                  SorpLib.Choices.SpecificVolumeAdsorpt.GeneralizedFunction)),
                HideResult=true,
                Evaluate= true);

  parameter SorpLib.Choices.InterpolationApproach v_adsorpt_interpolation=
      SorpLib.Choices.InterpolationApproach.Linear
    "Interpolation approach used to calculate the specific volume of the adsorpt"
    annotation (Dialog(tab="Specific Volume", group="Interpolation",
                enable=(not neglectSpecifcVolume and approachSpecificVolume ==
                  SorpLib.Choices.SpecificVolumeAdsorpt.Interpolation)),
                Evaluate=false,
                HideResult=true);
  parameter Real abscissa_v_adsorpt[:]={0, 1000}
    "Known abscissa values for the specific volume of the adsorpt"
    annotation (Dialog(tab="Specific Volume", group="Interpolation",
                enable=(not neglectSpecifcVolume and approachSpecificVolume ==
                  SorpLib.Choices.SpecificVolumeAdsorpt.Interpolation)),
                HideResult=true,
                Evaluate= true);
  parameter Real ordinate_v_adsorpt[size(abscissa_v_adsorpt,1)]=
    {v_adsorpt_constant, v_adsorpt_constant}
    "Known ordinate values for the specific volume of the adsorpt"
    annotation (Dialog(tab="Specific Volume", group="Interpolation",
                enable=(not neglectSpecifcVolume and approachSpecificVolume ==
                  SorpLib.Choices.SpecificVolumeAdsorpt.Interpolation)),
                HideResult=true,
                Evaluate= true);
  parameter Real coefficients_cubicSplines_v_adsorpt[size(abscissa_v_adsorpt,1),4]=
    SorpLib.Media.Functions.Utilities.calcCubicSplineCoefficients(
      abscissa=abscissa_v_adsorpt,
      ordinate=ordinate_v_adsorpt)
    "Coefficient a to d for cubic polynomials for the specific volume of the adsorpt"
    annotation (Dialog(tab="Specific Volume", group="Interpolation",
                enable=false),
                HideResult=true,
                Evaluate= true);

  //
  // Definition of parameters regarding advanced options
  //
  parameter Modelica.Units.SI.Pressure p_adsorpt_min = 615
    "Lower limit for the pressure p_adsorpt"
    annotation (Dialog(tab = "Advanced", group = "Limiter"),
                Evaluate=true,
                HideResult=true);

  parameter Modelica.Units.SI.PressureDifference dp = 1e-3
    "Pressure difference used when calculating partial derivatives numerically"
    annotation (Dialog(tab="Advanced", group="Numerics - Derivatives"),
                Evaluate=true,
                HideResult=true);
  parameter Modelica.Units.SI.TemperatureDifference dT = 1e-3
    "Temperature difference used when calculating partial derivatives numerically"
    annotation (Dialog(tab="Advanced", group="Numerics - Derivatives"),
                Evaluate=true,
                HideResult=true);
  parameter Real tolerance_adsorpt_int_h = 1e-6
    "Integration tolerance when calculating the specific average enthalpy of the
    adsorpt numerically"
    annotation (Dialog(tab="Advanced", group="Numerics - Integrals",
                enable=calcAdsorptAdsorbateState and calcCaloricProperties),
                Evaluate=true,
                HideResult=true);
  parameter Real tolerance_adsorpt_int_s = 1e-6
    "Integration tolerance when calculating the specific average entropy of the
    adsorpt numerically"
    annotation (Dialog(tab="Advanced", group="Numerics - Integrals",
                enable=calcAdsorptAdsorbateState and calcEntropicProperties),
                Evaluate=true,
                HideResult=true);
  parameter Real tolerance_adsorpt_int_cp = 1e-2
    "Integration tolerance when calculating the specific heat capacity numerically"
    annotation (Dialog(tab="Advanced", group="Numerics - Integrals",
                enable=calcCaloricProperties),
                Evaluate=true,
                HideResult=true);
  parameter SorpLib.Units.Uptake x_adsorpt_lb = 0
    "Lower limit for integral when calculating the specific heat capacity of the
    adsorpt or uptake-averaged porperties of the adsorpt numerically (i.e., 
    should be zero)"
    annotation (Dialog(tab="Advanced", group="Numerics - Integrals",
                enable=calcCaloricProperties),
                Evaluate=true,
                HideResult=true);

  //
  // Definition of general inputs
  //
  input Real[no_coefficients] c
    "Isotherm coefficients"
    annotation (Dialog(tab="General",group="Inputs"));
  input Real[no_coefficients] dc_dT_adsorpt
    "Partial derivatives of the isotherm coefficients w.r.t. temperature"
    annotation (Dialog(tab="General",group="Inputs"));
  input Real[no_coefficients] ddc_dT_adsorpt_dT_adsorpt
    "Second-prder partial derivatives of the isotherm coefficients w.r.t. 
    temperature"
    annotation (Dialog(tab="General",group="Inputs"));

  input Modelica.Units.SI.Pressure p_adsorpt
    "Equilibrium pressure"
    annotation (Dialog(tab="General",group="Inputs"));
  input Modelica.Units.SI.Temperature T_adsorpt
    "Equilibrium temperature"
    annotation (Dialog(tab="General",group="Inputs"));
  input SorpLib.Units.Uptake x_adsorpt
    "Equilibrium uptake"
    annotation (Dialog(tab="General", group="Inputs"));

  input
    SorpLib.Media.WorkingPairs.Records.PropertiesPureAdsorptive properties_adsorptive
    "Record containing all properties of the adsorptive required for the working
    pair model"
    annotation (Dialog(tab="General",group="Inputs"));

  //
  // Definition of outputs
  //
  output SorpLib.Media.WorkingPairs.Records.PropertiesPureAdsorpt properties=
    SorpLib.Media.WorkingPairs.Records.PropertiesPureAdsorpt(
      state = SorpLib.Media.WorkingPairs.Records.StateVariables(
        p=p_avg,
        T=T_avg,
        v=v_avg,
        h=h_avg,
        u=u_avg,
        s=s_avg,
        g=g_avg,
        a=a_avg),
      v=v,
      h_ads=h_ads,
      cp=cp,
      derivatives_isotherm=
        SorpLib.Media.WorkingPairs.Records.DerivativesPureIsothermModel(
          dx_dp_T=dx_dp_T,
          dx_dT_p=dx_dT_p,
          dp_dx_T=dp_dx_T,
          dp_dT_x=dp_dT_x),
      dxv_avg_dp_T=dxv_avg_dp_T,
      dxv_avg_dT_p=dxv_avg_dT_p,
      dxh_avg_dp_T=dxh_avg_dp_T,
      dxh_avg_dT_p=dxh_avg_dT_p,
      dxh_avg_dx_T=dxh_avg_dx_T,
      dxh_avg_dT_x=dxh_avg_dT_x,
      dxs_avg_dp_T=dxs_avg_dp_T,
      dxs_avg_dT_p=dxs_avg_dT_p,
      dxs_avg_dx_T=dxs_avg_dx_T,
      dxs_avg_dT_x=dxs_avg_dT_x)
    "Record containing all required properties of the adsorpt"
    annotation (Dialog(tab="General", group="Outputs", enable=false));

  //
  // Definition of constants
  //
  constant Modelica.Units.SI.MolarMass M_adsorpt = M_adsorptive
    "Molar mass of the adsorpt/adsorptive";

  //
  // Booleans indicating which variables must be calculated
  //
protected
  parameter Boolean require_caloricAdsorptAdsorbateState=
    (calcCaloricProperties and calcAdsorptAdsorbateState)
    "= true, if uptake-averaged thermal and caloric properties of the adsorpt and 
    adsorbate phase are required";
  parameter Boolean require_entropicAdsorptAdsorbateState=
    (require_caloricAdsorptAdsorbateState and calcEntropicProperties)
    "= true, if uptake-averaged entropic properties of the adsorpt and adsorbate 
    phase are required";

  parameter Boolean require_massEnergyBalances=
    (calcCaloricProperties and calcDerivativesIsotherm and
    calcDerivativesMassEnergyBalance)
    "= true, if properties for mass and energy balance are required";
  parameter Boolean require_entropyBalance=
    (require_massEnergyBalances and calcDerivativesEntropyBalance)
    "= true, if properties for entropy balance are required";

  parameter Boolean require_dxv_avg_dp_T=
    (require_massEnergyBalances)
    "= true, if partial derivative of uptake-averaged specific volume of the adsorpt
    times the uptake w.r.t. pressure at constant temperature is required";
  parameter Boolean require_dxv_avg_dT_p=
    (require_massEnergyBalances)
    "= true, if partial derivative of uptake-averaged specific volume of the adsorpt
    times the uptake w.r.t. temperature at constant pressure is required";

  parameter Boolean require_dxh_avg_dp_T=
    (require_massEnergyBalances and stateVariables==
    SorpLib.Choices.IndependentVariablesPureComponentWorkingPair.pT)
    "= true, if partial derivative of uptake-averaged specific enthalpy of the adsorpt
    times the uptake w.r.t. pressure at constant temperature is required";
  parameter Boolean require_dxh_avg_dT_p=
    (require_massEnergyBalances and stateVariables==
    SorpLib.Choices.IndependentVariablesPureComponentWorkingPair.pT)
    "= true, if partial derivative of uptake-averaged specific enthalpy of the adsorpt
    times the uptake w.r.t. temperature at constant pressure is required";
  parameter Boolean require_dxh_avg_dx_T=
    (require_massEnergyBalances)
    "= true, if partial derivative of uptake-averaged specific enthalpy of the adsorpt
    times the uptake w.r.t. uptake at constant temperature is required";
  parameter Boolean require_dxh_avg_dT_x=
    (require_massEnergyBalances)
    "= true, if partial derivative of uptake-averaged specific enthalpy of the adsorpt
    times the uptake w.r.t. temperature at constant uptake is required";

  parameter Boolean require_dxs_avg_dp_T=
    (require_entropyBalance and stateVariables==
    SorpLib.Choices.IndependentVariablesPureComponentWorkingPair.pT)
    "= true, if partial derivative of uptake-averaged specific entropy of the adsorpt
    times the uptake w.r.t. pressure at constant temperature is required";
  parameter Boolean require_dxs_avg_dT_p=
    (require_entropyBalance and stateVariables==
    SorpLib.Choices.IndependentVariablesPureComponentWorkingPair.pT)
    "= true, if partial derivative of uptake-averaged specific entropy of the adsorpt
    times the uptake w.r.t. temperature at constant pressure is required";
  parameter Boolean require_dxs_avg_dx_T=
    (require_entropyBalance)
    "= true, if partial derivative of uptake-averaged specific entropy of the adsorpt
    times the uptake w.r.t. uptake at constant temperature is required";
  parameter Boolean require_dxs_avg_dT_x=
    (require_entropyBalance)
    "= true, if partial derivative of uptake-averaged specific entropy of the adsorpt
    times the uptake w.r.t. temperature at constant uptake is required";

  parameter Boolean require_h_ads_Formal=
    (calcCaloricProperties and approachSorptionEnthalpy ==
    SorpLib.Choices.SorptionEnthalpy.Formal)
    "= true, if properties for the specific enthalpy of adsorption are required";
  parameter Boolean require_h_ads_CC=
    (calcCaloricProperties and approachSorptionEnthalpy ==
    SorpLib.Choices.SorptionEnthalpy.ClausiusClapeyron)
    "= true, if properties for the specific enthalpy of adsorption according to
    Clausius Clapeyron are required";
  parameter Boolean require_h_ads_Dubinin=
    (calcCaloricProperties and approachSorptionEnthalpy ==
    SorpLib.Choices.SorptionEnthalpy.Dubinin)
    "= true, if properties for the specific enthalpy of adsorption according to
    Dubinin are required";

  parameter Boolean require_cp_ChakrabortyElAl=
    (require_h_ads_Formal and approachSpecificHeatCapacity ==
    SorpLib.Choices.SpecificHeatCapacityAdsorpt.ChakrabortyElAl)
    "= true, if properties for the specific heat capacity according to Chakraborty
    et al. are required";
  parameter Boolean require_cp_ChakrabortyElAl_CC=
    (require_h_ads_CC and approachSpecificHeatCapacity ==
    SorpLib.Choices.SpecificHeatCapacityAdsorpt.ChakrabortyElAl)
    "= true, if properties for the specific heat capacity according to Chakraborty
    et al. with the adsorption enthalpy according to Clausius Clapeyron are required";
  parameter Boolean require_cp_ChakrabortyElAl_Dubinin=
    (require_h_ads_Dubinin and approachSpecificHeatCapacity ==
    SorpLib.Choices.SpecificHeatCapacityAdsorpt.ChakrabortyElAl)
    "= true, if properties for the specific heat capacity according to Chakraborty
    et al. with the adsorption enthalpy according to Dubinin are required";

  parameter Boolean require_cp_WaltonLeVan=
    (require_h_ads_Formal and approachSpecificHeatCapacity ==
    SorpLib.Choices.SpecificHeatCapacityAdsorpt.WaltonLeVan)
    "= true, if properties for the specific heat capacity according to Walton and
    Le Van are required";
  parameter Boolean require_cp_WaltonLeVan_CC=
    (require_h_ads_CC and approachSpecificHeatCapacity ==
    SorpLib.Choices.SpecificHeatCapacityAdsorpt.WaltonLeVan)
    "= true, if properties for the specific heat capacity according to Walton and
    Le Van with the adsorption enthalpy according to Clausius Clapeyron are required";
  parameter Boolean require_cp_WaltonLeVan_Dubinin=
    (require_h_ads_Dubinin and approachSpecificHeatCapacity ==
    SorpLib.Choices.SpecificHeatCapacityAdsorpt.WaltonLeVan)
    "= true, if properties for the specific heat capacity according to Walton and
    Le Van with the adsorption enthalpy according to Dubinin are required";

  parameter Boolean require_cp_SchwambergerSchmidt=
    (require_h_ads_Formal and approachSpecificHeatCapacity ==
    SorpLib.Choices.SpecificHeatCapacityAdsorpt.SchwambergerSchmidt)
    "= true, if properties for the specific heat capacity according to Schwamberger
    and Schmidt are required";
  parameter Boolean require_cp_SchwambergerSchmidt_CC=
    (require_h_ads_CC and approachSpecificHeatCapacity ==
    SorpLib.Choices.SpecificHeatCapacityAdsorpt.SchwambergerSchmidt)
    "= true, if properties for the specific heat capacity according to Schwamberger
    and Schmidt with the adsorption enthalpy according to Clausius Clapeyron are required";
  parameter Boolean require_cp_SchwambergerSchmidt_Dubinin=
    (require_h_ads_Dubinin and approachSpecificHeatCapacity ==
    SorpLib.Choices.SpecificHeatCapacityAdsorpt.SchwambergerSchmidt)
    "= true, if properties for the specific heat capacity according to Schwamberger
    and Schmidt with the adsorption enthalpy according to Dubinin are required";

  parameter Boolean require_derivativesIsotherm_pT=
    (calcDerivativesIsotherm) or
    not neglectSpecifcVolume and (require_dxv_avg_dp_T) or
    (require_h_ads_Formal) or
    (require_h_ads_CC) or
    (require_cp_ChakrabortyElAl) or
    (require_cp_ChakrabortyElAl_CC) or
    (require_cp_ChakrabortyElAl_Dubinin) or
    (require_dxh_avg_dp_T) or
    (require_dxh_avg_dT_p)
    "= true, if partial derivatives of the isotherm model w.r.t. pressure and
    temperature are required";
  parameter Boolean require_derivativesIsotherm_xT=
    (require_derivativesIsotherm_pT and stateVariables==
    SorpLib.Choices.IndependentVariablesPureComponentWorkingPair.xT) or
    (require_cp_ChakrabortyElAl) or
    (require_cp_ChakrabortyElAl_CC) or
    (require_cp_ChakrabortyElAl_Dubinin) or
    (require_dxh_avg_dp_T) or
    (require_dxh_avg_dT_p)
    "= true, if partial derivatives of the isotherm model w.r.t. uptake and
    temperature are required";
  parameter Boolean require_ddx_dp_dp_T=
    (require_derivativesIsotherm_pT and require_massEnergyBalances and
    (require_h_ads_Formal or require_h_ads_CC)) or
    (require_derivativesIsotherm_pT and require_entropyBalance and
    (require_h_ads_Formal or require_h_ads_CC)) or
    (require_cp_ChakrabortyElAl) or
    (require_cp_ChakrabortyElAl_CC)
    "= true, if second-order partial derivative of the uptake w.r.t. pressure at
    constant temperature is required";
  parameter Boolean require_ddx_dT_dT_p=
    (require_ddx_dp_dp_T)
    "= true, if second-order partial derivative of the uptake w.r.t. temperature
    at constant pressure is required";
  parameter Boolean require_ddx_dp_dT=
    (require_ddx_dp_dp_T)
    "= true, if second-order partial derivative of the uptake w.r.t. pressure and
    temperature is required";

  parameter Boolean require_v=
    not neglectSpecifcVolume or
    not neglectSpecifcVolume and
    ((require_dxv_avg_dp_T) or
    (require_dxv_avg_dT_p) or
    (require_caloricAdsorptAdsorbateState and require_h_ads_Formal) or
    (require_entropicAdsorptAdsorbateState and require_h_ads_Formal) or
    (require_h_ads_Formal) or
    (require_cp_ChakrabortyElAl) or
    (require_cp_ChakrabortyElAl_CC) or
    (require_cp_WaltonLeVan) or
    (require_cp_SchwambergerSchmidt)) or
    (require_h_ads_Dubinin) or
    (require_caloricAdsorptAdsorbateState and require_h_ads_Dubinin) or
    (require_entropicAdsorptAdsorbateState and require_h_ads_Dubinin) or
    (require_cp_ChakrabortyElAl_Dubinin) or
    (require_cp_WaltonLeVan_Dubinin) or
    (require_cp_SchwambergerSchmidt_Dubinin)
    "= true, if specific volume of last adsorbed molecule is required";
  parameter Boolean require_dv_dp_T=
    not neglectSpecifcVolume and
    ((require_dxv_avg_dp_T) or
    (require_cp_ChakrabortyElAl) or
    (require_cp_WaltonLeVan) or
    (require_cp_SchwambergerSchmidt)) or
    (require_cp_ChakrabortyElAl_Dubinin) or
    (require_cp_WaltonLeVan_Dubinin) or
    (require_cp_SchwambergerSchmidt_Dubinin)
    "= true, if the partial derivative of the specific volume of the last adsorbed
    molecule w.r.t. pressure at constant temperature is required";
  parameter Boolean require_dv_dT_p=
    (require_dv_dp_T) or
    not neglectSpecifcVolume and
    ((require_dxv_avg_dT_p) or
    (require_cp_ChakrabortyElAl) or
    (require_cp_ChakrabortyElAl_CC) or
    (require_cp_WaltonLeVan) or
    (require_cp_SchwambergerSchmidt)) or
    (require_h_ads_Dubinin) or
    (require_cp_ChakrabortyElAl_Dubinin) or
    (require_cp_WaltonLeVan_Dubinin) or
    (require_cp_SchwambergerSchmidt_Dubinin)
    "= true, if the partial derivative of the specific volume of the last adsorbed
    molecule w.r.t. temperature at pressure temperature is required";
  parameter Boolean require_ddv_dT_dT_p=
    (require_dbeta_dT_p)
    "= true, if the second-order partial derivative of the specific volume of the 
    last adsorbed molecule w.r.t. temperature at pressure temperature is required";

  parameter Boolean require_cp=
    calcCaloricProperties
    "= true, if specific heat capacity of the last adsorbed molecule or uptake-
    averaged adsorpt is required";

  parameter Boolean require_beta=
    (require_caloricAdsorptAdsorbateState and require_h_ads_Dubinin) or
    (require_entropicAdsorptAdsorbateState and require_h_ads_Dubinin) or
    (require_h_ads_Dubinin) or
    (require_cp_ChakrabortyElAl_Dubinin) or
    (require_cp_WaltonLeVan_Dubinin) or
    (require_cp_SchwambergerSchmidt_Dubinin)
    "= true, if isobaric expansion coefficient of the last adsorbed molecule is 
    required";
  parameter Boolean require_dbeta_dp_T=
    (require_cp_ChakrabortyElAl_Dubinin) or
    (require_cp_WaltonLeVan_Dubinin) or
    (require_cp_SchwambergerSchmidt_Dubinin)
    "= true, if the partial derivative of the isobaric expansion coefficient of 
    the last adsorbed molecule w.r.t. pressure at constant temperature is 
    required";
  parameter Boolean require_dbeta_dT_p=
    (require_dbeta_dp_T)
    "= true, if the partial derivative of the isobaric expansion coefficient of 
    the last adsorbed molecule w.r.t. temperature at pressure temperature is 
    required";

  parameter Boolean require_h_ads=
    (calcCaloricProperties) or
    (require_entropyBalance) or
    (require_cp_ChakrabortyElAl) or
    (require_cp_ChakrabortyElAl_CC) or
    (require_cp_ChakrabortyElAl_Dubinin)
    "= true, if the specific enthalpy of adsorption of the last adsorbed molecule
    is required";
  parameter Boolean require_dh_ads_dp_T=
    (require_cp_ChakrabortyElAl) or
    (require_cp_ChakrabortyElAl_CC) or
    (require_cp_ChakrabortyElAl_Dubinin)
    "= true, if the partial derivatieve of the specific enthalpy of adsorption 
    of the last adsorbed molecule w.r.t. pressure and constant temperature is 
    required";
  parameter Boolean require_dh_ads_dT_p=
    (require_dh_ads_dp_T)
    "= true, if the partial derivatieve of the specific enthalpy of adsorption 
    of the last adsorbed molecule w.r.t. temperature and constant pressure is 
    required";

  //
  // State variables
  //
  Modelica.Units.SI.Pressure p_avg
    "Uptake-averaged pressure of the adsorpt phase";
  Modelica.Units.SI.Temperature T_avg
    "Uptake-averaged temperature of the adsorpt phase";
  Modelica.Units.SI.SpecificVolume v_avg
    "Uptake-averaged specific volume of the adsorpt phase";
  Modelica.Units.SI.SpecificEnthalpy h_avg
    "Uptake-averaged specific enthalpy of the adsorpt phase";
  Modelica.Units.SI.SpecificInternalEnergy u_avg
    "Uptake-averaged specific internal energy of the adsorpt phase";
  Modelica.Units.SI.SpecificEntropy s_avg
    "Uptake-averaged specific entropy of the adsorpt phase";
  Modelica.Units.SI.SpecificGibbsFreeEnergy g_avg
    "Uptake-averaged specific free enthalpy (i.e., Gibbs free energy) of the 
    adsorpt phase";
  Modelica.Units.SI.SpecificHelmholtzFreeEnergy a_avg
    "Uptake-averaged specific free energy (i.e., Helmholts free energy) of the 
    adsorpt phase";

  Modelica.Units.SI.SpecificVolume v
    "Specific volume of last adsorbed molecule";

  //
  // Additional properties
  //
  Modelica.Units.SI.SpecificEnthalpy h_ads
    "Specific enthalpy of adsorption of last adsorbed molecule";

  Modelica.Units.SI.SpecificHeatCapacity cp
    "Specific heat capacity of last adsorbed molecule or the uptake-averaged 
    adsorpt, depending on selected approach)";
  Modelica.Units.SI.RelativePressureCoefficient beta
    "Isobaric expansion coefficient of last adsorbed molecule";

  //
  // Partial derivatives
  //
  SorpLib.Units.DerUptakeSpecificVolumeByPressure dxv_avg_dp_T
    "Partial derivative of the uptake-averaged specific volume times the uptake
    w.r.t. pressure at constant temperature";
  SorpLib.Units.DerUptakeSpecificVolumeByTemperature dxv_avg_dT_p
    "Partial derivative of the uptake-averaged specific volume times the uptake
    w.r.t. temperature at constant pressure";

  SorpLib.Units.DerUptakeSpecificEnthalpyByPressure dxh_avg_dp_T
    "Partial derivative of the uptake-averaged specific enthalpy times the uptake
    w.r.t. pressure at constant temperature";
  SorpLib.Units.DerUptakeSpecificEnthalpyByTemperature dxh_avg_dT_p
    "Partial derivative of the uptake-averaged specific enthalpy times the uptake
    w.r.t. temperature at constant pressure";
  SorpLib.Units.DerUptakeSpecificEnthalpyByUptake dxh_avg_dx_T
    "Partial derivative of the uptake-averaged specific enthalpy times the uptake
    w.r.t. uptake at constant temperature";
  SorpLib.Units.DerUptakeSpecificEnthalpyByTemperature dxh_avg_dT_x
    "Partial derivative of the uptake-averaged specific enthalpy times the uptake 
    w.r.t. temperature at constant uptake";

  SorpLib.Units.DerUptakeSpecificEntropyByPressure dxs_avg_dp_T
    "Partial derivative of the uptake-averaged specific entropy times the uptake 
    w.r.t. pressure at constant temperature";
  SorpLib.Units.DerUptakeSpecificEntropyByTemperature dxs_avg_dT_p
    "Partial derivative of the uptake-averaged specific entropy times the uptake 
    w.r.t. temperature at constant pressure";
  SorpLib.Units.DerUptakeSpecificEntropyByUptake dxs_avg_dx_T
    "Partial derivative of the uptake-averaged specific entropy times the uptake 
    w.r.t. uptake at constant temperature";
  SorpLib.Units.DerUptakeSpecificEntropyByTemperature dxs_avg_dT_x
    "Partial derivative of the uptake-averaged specific entropy times the uptake 
    w.r.t. temperature at constant uptake";

  SorpLib.Units.DerSpecificVolumeByPressure dv_dp_T
    "Partial derivative of the specific volume of the last adsorbed molecule 
    w.r.t. pressure at constant temperature";
  SorpLib.Units.DerSpecificVolumeByTemperature dv_dT_p
    "Partial derivative of the specific volume of the last adsorbed molecule 
    w.r.t. temperature at constant pressure";
  SorpLib.Units.DerSpecificVolumeByTemperatureTemperature ddv_dT_dT_p
    "Second-order partial derivative of specific volume of the last adsorbed 
    molecule w.r.t. temperature at constant pressure";

  SorpLib.Units.DerSpecificEnthalpyByPressure dh_ads_dp_T
    "Partial derivative of the specific enthalpy of adsorption w.r.t. pressure 
    at constant temperature";
  Modelica.Units.SI.SpecificHeatCapacity dh_ads_dT_p
    "Partial derivative of the specific enthalpy of adsorption w.r.t. temperature 
    at constant pressure";

  SorpLib.Units.DerIsobaricExpansionCoefficientByPressure dbeta_dp_T
    "Partial derivative of isobaric expansion coefficient of the adsorpt phase
    w.r.t. pressure at constant temperature";
  SorpLib.Units.DerIsobaricExpansionCoefficientByTemperature dbeta_dT_p
    "Partial derivative of isobaric expansion coefficient of the adsorpt phase
    w.r.t. temperature at constant pressure";

  SorpLib.Units.DerUptakeByPressure dx_dp_T
    "Partial derivative of the uptake w.r.t. pressure at constant temperature";
  SorpLib.Units.DerUptakeByTemperature dx_dT_p
    "Partial derivative of the uptake w.r.t. temperature at constant pressure";
  SorpLib.Units.DerPressureByUptake dp_dx_T
    "Partial derivative of the uptake w.r.t. uptake at constant temperature";
  Modelica.Units.SI.DerPressureByTemperature dp_dT_x
    "Partial derivative of the uptake w.r.t. temperature at constant uptake";
  SorpLib.Units.DerUptakeByPressurePressure ddx_adsorpt_dp_dp
    "Second-order partial derivative of uptake w.r.t. pressure at constant 
    temperature";
  SorpLib.Units.DerUptakeByTemperatureTemperature ddx_adsorpt_dT_dT
    "Second-order partial derivative of uptake w.r.t. temperature at constant 
    pressure";
  SorpLib.Units.DerUptakeByPressureTemperature ddx_adsorpt_dp_dT
    "Second-order partial derivative of uptake w.r.t. pressure and temperature";

equation
  //
  // Partial derivatives of the isotherm model
  //
  dx_dp_T =if not require_derivativesIsotherm_pT then 0 else
    IsothermModel.dx_dp(
    p_adsorpt=p_adsorpt,
    T_adsorpt=T_adsorpt,
    c=c)
    "Partial derivative of the uptake w.r.t. pressure at constant temperature";
  dx_dT_p =if not require_derivativesIsotherm_pT then 0 else
    IsothermModel.dx_dT(
    p_adsorpt=p_adsorpt,
    T_adsorpt=T_adsorpt,
    c=c,
    dc_dT_adsorpt=dc_dT_adsorpt)
    "Partial derivative of the uptake w.r.t. temperature at constant pressure";

  dp_dx_T =if not require_derivativesIsotherm_xT then 0 else 1/dx_dp_T
    "Partial derivative of the uptake w.r.t. uptake at constant temperature";
  dp_dT_x =if not require_derivativesIsotherm_xT then 0 else -dx_dT_p/dx_dp_T
    "Partial derivative of the uptake w.r.t. temperature at constant uptake";

  ddx_adsorpt_dp_dp =if not require_ddx_dp_dp_T then 0 else
    IsothermModel.ddx_dp_dp(
    p_adsorpt=p_adsorpt,
    T_adsorpt=T_adsorpt,
    c=c)
    "Second-order partial derivative of uptake w.r.t. pressure at constant 
    temperature";
  ddx_adsorpt_dT_dT =if not require_ddx_dT_dT_p then 0 else
    IsothermModel.ddx_dT_dT(
    p_adsorpt=p_adsorpt,
    T_adsorpt=T_adsorpt,
    c=c,
    dc_dT_adsorpt=dc_dT_adsorpt,
    ddc_dT_adsorpt_dT_adsorpt=ddc_dT_adsorpt_dT_adsorpt)
    "Second-order partial derivative of uptake w.r.t. temperature at constant 
    pressure";
  ddx_adsorpt_dp_dT =if not require_ddx_dp_dT then 0 else
    IsothermModel.ddx_dp_dT(
    p_adsorpt=p_adsorpt,
    T_adsorpt=T_adsorpt,
    c=c,
    dc_dT_adsorpt=dc_dT_adsorpt)
    "Second-order partial derivative of uptake w.r.t. pressure and temperature";

  //
  // Uptake-averaged state properties of the adsorpt
  //
  p_avg = if not require_caloricAdsorptAdsorbateState then 0 else
    p_adsorpt
    "Uptake-averaged pressure of the adsorpt phase";
  T_avg = if not require_caloricAdsorptAdsorbateState then 0 else
    T_adsorpt
    "Uptake-averaged temperature of the adsorpt phase";
  v_avg = if not require_caloricAdsorptAdsorbateState or
    neglectSpecifcVolume then 0 else v
    "Uptake-averaged specific volume of the adsorpt phase: Corresponds to specific
    volume of the last adsorbed molecule as specific volume is constant or only a
    function of temperature for all implemented approaches";

  if not require_caloricAdsorptAdsorbateState then
    h_avg = 0
      "Uptake-averaged specific enthalpy of the adsorpt phase";

  else
    if approachSorptionEnthalpy ==
      SorpLib.Choices.SorptionEnthalpy.Constant then
      h_avg = 1 / ((x_adsorpt-min(x_adsorpt_lb,0.5*x_adsorpt))) *
        Modelica.Math.Nonlinear.quadratureLobatto(
        f=function calc_integrand_h_avg_adsorpt_Constant(
          T_adsorpt=T_adsorpt,
          c=c,
          h_ads = h_ads_constant,
          adsorptiveAtDewPoint=adsorptiveAtDewPoint,
          p_clausiusClyperon=p_adsorpt_min,
          dp=dp,
          dT=dT),
        a=min(x_adsorpt_lb,0.5*x_adsorpt),
        b=x_adsorpt,
        tolerance=tolerance_adsorpt_int_h)
        "Uptake-averaged specific enthalpy of the adsorpt phase";

    elseif approachSorptionEnthalpy ==
      SorpLib.Choices.SorptionEnthalpy.Formal then
      h_avg = 1 / ((x_adsorpt-min(x_adsorpt_lb,0.5*x_adsorpt))) *
        Modelica.Math.Nonlinear.quadratureLobatto(
        f=function calc_integrand_h_avg_adsorpt_Formal(
          T_adsorpt=T_adsorpt,
          c=c,
          dc_dT_adsorpt=dc_dT_adsorpt,
          adsorptiveAtDewPoint=adsorptiveAtDewPoint,
          v_adsorpt=if neglectSpecifcVolume then 0 else v,
          p_clausiusClyperon=p_adsorpt_min,
          dp=dp,
          dT=dT),
        a=min(x_adsorpt_lb,0.5*x_adsorpt),
        b=x_adsorpt,
        tolerance=tolerance_adsorpt_int_h)
        "Uptake-averaged specific enthalpy of the adsorpt phase";

    elseif approachSorptionEnthalpy ==
      SorpLib.Choices.SorptionEnthalpy.ClausiusClapeyron then
      h_avg = 1 / ((x_adsorpt-min(x_adsorpt_lb,0.5*x_adsorpt))) *
        Modelica.Math.Nonlinear.quadratureLobatto(
        f=function calc_integrand_h_avg_adsorpt_CC(
          T_adsorpt=T_adsorpt,
          c=c,
          dc_dT_adsorpt=dc_dT_adsorpt,
          adsorptiveAtDewPoint=adsorptiveAtDewPoint,
          p_clausiusClyperon=p_adsorpt_min,
          dp=dp,
          dT=dT),
        a=min(x_adsorpt_lb,0.5*x_adsorpt),
        b=x_adsorpt,
        tolerance=tolerance_adsorpt_int_h)
        "Uptake-averaged specific enthalpy of the adsorpt phase";

    else
      h_avg = 1 / ((x_adsorpt-min(x_adsorpt_lb,0.5*x_adsorpt))) *
        Modelica.Math.Nonlinear.quadratureLobatto(
        f=function calc_integrand_h_avg_adsorpt_Dubinin(
          T_adsorpt=T_adsorpt,
          c=c,
          dc_dT_adsorpt=dc_dT_adsorpt,
          adsorptiveAtDewPoint=adsorptiveAtDewPoint,
          v_adsorpt=v,
          beta_adsorpt=beta,
          p_clausiusClyperon=p_adsorpt_min,
          dp=dp,
          dT=dT),
        a=min(x_adsorpt_lb,0.5*x_adsorpt),
        b=x_adsorpt,
        tolerance=tolerance_adsorpt_int_h)
        "Uptake-averaged specific enthalpy of the adsorpt phase";

    end if;
  end if;

  u_avg = if not require_caloricAdsorptAdsorbateState then 0 else
    h_avg - p_avg * v_avg
    "Uptake-averaged specific internal energy of the adsorpt phase";


  if not require_entropicAdsorptAdsorbateState then
    s_avg = 0
      "Uptake-averaged specific enthalpy of the adsorpt phase";

  else
    if approachSorptionEnthalpy ==
      SorpLib.Choices.SorptionEnthalpy.Constant then
      s_avg = 1 / ((x_adsorpt-x_adsorpt_lb)) *
        Modelica.Math.Nonlinear.quadratureLobatto(
        f=function calc_integrand_s_avg_adsorpt_Constant(
          T_adsorpt=T_adsorpt,
          c=c,
          h_ads = h_ads_constant,
          adsorptiveAtDewPoint=adsorptiveAtDewPoint,
          p_clausiusClyperon=p_adsorpt_min,
          dp=dp,
          dT=dT),
        a=x_adsorpt_lb,
        b=x_adsorpt,
        tolerance=tolerance_adsorpt_int_h)
        "Uptake-averaged specific entropy of the adsorpt phase";

    elseif approachSorptionEnthalpy ==
      SorpLib.Choices.SorptionEnthalpy.Formal then
      s_avg = 1 / ((x_adsorpt-x_adsorpt_lb)) *
        Modelica.Math.Nonlinear.quadratureLobatto(
        f=function calc_integrand_s_avg_adsorpt_Formal(
          T_adsorpt=T_adsorpt,
          c=c,
          dc_dT_adsorpt=dc_dT_adsorpt,
          adsorptiveAtDewPoint=adsorptiveAtDewPoint,
          v_adsorpt=if neglectSpecifcVolume then 0 else v,
          p_clausiusClyperon=p_adsorpt_min,
          dp=dp,
          dT=dT),
        a=x_adsorpt_lb,
        b=x_adsorpt,
        tolerance=tolerance_adsorpt_int_h)
        "Uptake-averaged specific entropy of the adsorpt phase";

    elseif approachSorptionEnthalpy ==
      SorpLib.Choices.SorptionEnthalpy.ClausiusClapeyron then
      s_avg = 1 / ((x_adsorpt-x_adsorpt_lb)) *
        Modelica.Math.Nonlinear.quadratureLobatto(
        f=function calc_integrand_s_avg_adsorpt_CC(
          T_adsorpt=T_adsorpt,
          c=c,
          dc_dT_adsorpt=dc_dT_adsorpt,
          adsorptiveAtDewPoint=adsorptiveAtDewPoint,
          p_clausiusClyperon=p_adsorpt_min,
          dp=dp,
          dT=dT),
        a=x_adsorpt_lb,
        b=x_adsorpt,
        tolerance=tolerance_adsorpt_int_h)
        "Uptake-averaged specific entropy of the adsorpt phase";

    else
      s_avg = 1 / ((x_adsorpt-x_adsorpt_lb)) *
        Modelica.Math.Nonlinear.quadratureLobatto(
        f=function calc_integrand_s_avg_adsorpt_Dubinin(
          T_adsorpt=T_adsorpt,
          c=c,
          dc_dT_adsorpt=dc_dT_adsorpt,
          adsorptiveAtDewPoint=adsorptiveAtDewPoint,
          v_adsorpt=v,
          beta_adsorpt=beta,
          p_clausiusClyperon=p_adsorpt_min,
          dp=dp,
          dT=dT),
        a=x_adsorpt_lb,
        b=x_adsorpt,
        tolerance=tolerance_adsorpt_int_h)
        "Uptake-averaged specific entropy of the adsorpt phase";

    end if;
  end if;

  g_avg = if not require_entropicAdsorptAdsorbateState then 0 else
    h_avg - T_avg * s_avg
    "Uptake-averaged specific free enthalpy (i.e., Gibbs free energy) of the 
    adsorpt phase";
  a_avg = if not require_entropicAdsorptAdsorbateState then 0 else
    u_avg - T_avg * s_avg
    "Uptake-averaged specific free energy (i.e., Helmholts free energy) of the 
    adsorpt phase";

  //
  // Specific volume of the last adsorbed molecule and its partial derivatives
  //
  if not require_v then
    v =0
      "Specific volume of the last adsorbed molecule";

  else
    if approachSpecificVolume ==
      SorpLib.Choices.SpecificVolumeAdsorpt.Constant then
      v = v_adsorpt_constant
        "Specific volume of the last adsorbed molecule";

    elseif approachSpecificVolume ==
      SorpLib.Choices.SpecificVolumeAdsorpt.BoilingCurve then
      v = properties_adsorptive.v_satLiq
        "Specific volume of the last adsorbed molecule";

    elseif approachSpecificVolume ==
      SorpLib.Choices.SpecificVolumeAdsorpt.GeneralizedFunction then
      v = SorpLib.Media.Functions.Utilities.generalizedFunction_T(
        T=T_adsorpt,
        T_ref=T_ref_v_adsorpt,
        z_ref=v_adsorpt_ref,
        coefficients=coefficients_v_adsorpt,
        exponents=exponents_v_adsorpt,
        approach=v_adsorpt_function)
        "Specific volume of the last adsorbed molecule";

    else
      if v_adsorpt_interpolation ==
        SorpLib.Choices.InterpolationApproach.Linear then
        v = SorpLib.Media.Functions.Utilities.linearInterpolation_T(
          T=T_adsorpt,
          abscissa=abscissa_v_adsorpt,
          ordinate=ordinate_v_adsorpt)
          "Specific volume of the last adsorbed molecule";

      else
        v = SorpLib.Media.Functions.Utilities.cubicSplineInterpolation_T(
          T=T_adsorpt,
          abscissa=abscissa_v_adsorpt,
          ordinate=ordinate_v_adsorpt,
          coefficients=coefficients_cubicSplines_v_adsorpt)
          "Specific volume of the last adsorbed molecule";

      end if;
    end if;
  end if;

  dv_dp_T =if not require_dv_dp_T then 0 else 0
    "Partial derivative of the specific volume of the last adsorbed molecule 
    w.r.t. pressure at constant temperature: Corresponds to unity because v_adsorpt 
    is  not a function of pressure";

  if not require_dv_dT_p then
    dv_dT_p =0
    "Partial derivative of the specific volume of the last adsorbed molecule 
    w.r.t. temperature at constant pressure";

  else
    if approachSpecificVolume ==
      SorpLib.Choices.SpecificVolumeAdsorpt.Constant then
      dv_dT_p = 0
        "Partial derivative of the specific volume of the last adsorbed molecule 
        w.r.t. temperature at constant pressure";

    elseif approachSpecificVolume ==
      SorpLib.Choices.SpecificVolumeAdsorpt.BoilingCurve then
      dv_dT_p = properties_adsorptive.dv_satLiq_dT
        "Partial derivative of the specific volume of the last adsorbed molecule 
        w.r.t. temperature at constant pressure";

    elseif approachSpecificVolume ==
      SorpLib.Choices.SpecificVolumeAdsorpt.GeneralizedFunction then
      dv_dT_p = SorpLib.Media.Functions.Utilities.dgeneralizedFunction_dT(
        T=T_adsorpt,
        T_ref=T_ref_v_adsorpt,
        z_ref=v_adsorpt_ref,
        coefficients=coefficients_v_adsorpt,
        exponents=exponents_v_adsorpt,
        approach=v_adsorpt_function)
        "Partial derivative of the specific volume of the last adsorbed molecule 
        w.r.t. temperature at constant pressure";

    else
      if v_adsorpt_interpolation ==
        SorpLib.Choices.InterpolationApproach.Linear then
        dv_dT_p = SorpLib.Media.Functions.Utilities.dlinearInterpolation_dT(
          T=T_adsorpt,
          abscissa=abscissa_v_adsorpt,
          ordinate=ordinate_v_adsorpt)
          "Partial derivative of the specific volume of the last adsorbed molecule 
          w.r.t. temperature at constant pressure";

      else
        dv_dT_p = SorpLib.Media.Functions.Utilities.dcubicSplineInterpolation_dT(
          T=T_adsorpt,
          abscissa=abscissa_v_adsorpt,
          ordinate=ordinate_v_adsorpt,
          coefficients=coefficients_cubicSplines_v_adsorpt)
          "Partial derivative of the specific volume of the last adsorbed molecule 
          w.r.t. temperature at constant pressure";

      end if;
    end if;
  end if;

  if not require_ddv_dT_dT_p then
    ddv_dT_dT_p =0
      "Second-order partial derivative of the specific volume of the last adsorbed
      molecule w.r.t. temperature at constant pressure";

  else
    if approachSpecificVolume ==
      SorpLib.Choices.SpecificVolumeAdsorpt.Constant then
      ddv_dT_dT_p = 0
        "Second-order partial derivative of the specific volume of the last 
        adsorbed molecule w.r.t. temperature at constant pressure";

    elseif approachSpecificVolume ==
      SorpLib.Choices.SpecificVolumeAdsorpt.BoilingCurve then
      ddv_dT_dT_p = properties_adsorptive.ddv_satLiq_dT_dT
        "Second-order partial derivative of the specific volume of the last 
        adsorbed molecule w.r.t. temperature at constant pressure";

    elseif approachSpecificVolume ==
      SorpLib.Choices.SpecificVolumeAdsorpt.GeneralizedFunction then
      ddv_dT_dT_p =
        SorpLib.Media.Functions.Utilities.ddgeneralizedFunction_dT_dT(
        T=T_adsorpt,
        T_ref=T_ref_v_adsorpt,
        z_ref=v_adsorpt_ref,
        coefficients=coefficients_v_adsorpt,
        exponents=exponents_v_adsorpt,
        approach=v_adsorpt_function)
        "Second-order partial derivative of the specific volume of the last 
        adsorbed molecule w.r.t. temperature at constant pressure";

    else
      if v_adsorpt_interpolation ==
        SorpLib.Choices.InterpolationApproach.Linear then
        ddv_dT_dT_p =
          SorpLib.Media.Functions.Utilities.ddlinearInterpolation_dT_dT(
          T=T_adsorpt,
          abscissa=abscissa_v_adsorpt,
          ordinate=ordinate_v_adsorpt)
          "Second-order partial derivative of the specific volume of the last 
          adsorbed molecule w.r.t. temperature at constant pressure";

      else
        ddv_dT_dT_p =
          SorpLib.Media.Functions.Utilities.ddcubicSplineInterpolation_dT_dT(
          T=T_adsorpt,
          abscissa=abscissa_v_adsorpt,
          ordinate=ordinate_v_adsorpt,
          coefficients=coefficients_cubicSplines_v_adsorpt)
          "Second-order partial derivative of the specific volume of the last 
          adsorbed molecule w.r.t. temperature at constant pressure";

      end if;
    end if;
  end if;

  //
  // Isobaric expansion coefficient of the last adsorbed molecule
  //
  beta =if not require_beta or not require_v then 0
     else 1/v*dv_dT_p
    "Isobaric expansion coefficient of the last adsorbed molecule";

  dbeta_dp_T =if not require_dbeta_dp_T or not require_v
     then 0 else 0
    "Partial derivative of isobaric expansion coefficient of the last adsorbed 
    molecule w.r.t. pressure at constant temperature: Corresponds to unity as 
    v_adsorpt is not a function of pressure";
  dbeta_dT_p =if not require_dbeta_dT_p or not require_v
     then 0 else -(1/v*dv_dT_p)^2 + 1/v*ddv_dT_dT_p
    "Partial derivative of isobaric expansion coefficient of the last adsorbed
    molecule w.r.t. temperature at constant pressure";

  //
  // Specific enthalpy of adsorption of the last adsorbed molecule and its
  // partial derivatives
  //
  if not require_h_ads then
    h_ads =0
      "Specific enthalpy of adsorption of the last adsorbed molecule";

  else
    if approachSorptionEnthalpy ==
      SorpLib.Choices.SorptionEnthalpy.Constant then
      h_ads = h_ads_constant
        "Specific enthalpy of adsorption of the last adsorbed molecule";

    elseif approachSorptionEnthalpy ==
      SorpLib.Choices.SorptionEnthalpy.Formal then
      h_ads = SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.h_ads(
        M_adsorptive=M_adsorpt,
        T_adsorpt=T_adsorpt,
        v_adsorptive=properties_adsorptive.state.v,
        v_adsorpt=if neglectSpecifcVolume then 0 else v,
        dx_adsorpt_dp=dx_dp_T,
        dx_adsorpt_dT=dx_dT_p) / M_adsorpt
        "Specific enthalpy of adsorption of the last adsorbed molecule";

    elseif approachSorptionEnthalpy ==
      SorpLib.Choices.SorptionEnthalpy.ClausiusClapeyron then
      h_ads =
        SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.h_ads_clausiusClapeyron(
        p_adsorpt=p_adsorpt,
        T_adsorpt=T_adsorpt,
        dx_adsorpt_dp=dx_dp_T,
        dx_adsorpt_dT=dx_dT_p) / M_adsorpt
        "Specific enthalpy of adsorption of the last adsorbed molecule";

    else
      h_ads = calc_h_ads_Dubinin(
        c=c,
        p_adsorpt=p_adsorpt,
        T_adsorpt=T_adsorpt,
        x_adsorpt=x_adsorpt,
        v_adsorpt=v,
        beta_adsorpt=beta,
        properties_adsorptive=properties_adsorptive)
        "Specific enthalpy of adsorption of the last adsorbed molecule";

    end if;
  end if;

  if not require_dh_ads_dp_T then
    dh_ads_dp_T = 0
    "Partial derivative of the specific enthalpy of adsorption of the last 
    adsorbed molecule w.r.t. pressure at constant temperature";

  else
    if approachSorptionEnthalpy ==
      SorpLib.Choices.SorptionEnthalpy.Constant then
      dh_ads_dp_T = 0
      "Partial derivative of the specific enthalpy of adsorption of the last 
      adsorbed molecule w.r.t. pressure at constant temperature";

    elseif approachSorptionEnthalpy ==
      SorpLib.Choices.SorptionEnthalpy.Formal then
      dh_ads_dp_T =
        SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.dh_ads_dp(
        M_adsorptive=M_adsorpt,
        T_adsorpt=T_adsorpt,
        v_adsorptive=properties_adsorptive.state.v,
        v_adsorpt=if neglectSpecifcVolume then 0 else v,
        dv_adsorptive_dp=properties_adsorptive.dv_dp_T,
        dv_adsorpt_dp=if neglectSpecifcVolume then 0 else dv_dp_T,
        dx_adsorpt_dp=dx_dp_T,
        dx_adsorpt_dT=dx_dT_p,
        ddx_adsorpt_dp_dp=ddx_adsorpt_dp_dp,
        ddx_adsorpt_dp_dT=ddx_adsorpt_dp_dT) / M_adsorpt
      "Partial derivative of the specific enthalpy of adsorption of the last 
      adsorbed molecule w.r.t. pressure at constant temperature";

    elseif approachSorptionEnthalpy ==
      SorpLib.Choices.SorptionEnthalpy.ClausiusClapeyron then
      dh_ads_dp_T =
        SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.dh_ads_dp_clausiusClapeyron(
        p_adsorpt=p_adsorpt,
        T_adsorpt=T_adsorpt,
        dx_adsorpt_dp=dx_dp_T,
        dx_adsorpt_dT=dx_dT_p,
        ddx_adsorpt_dp_dp=ddx_adsorpt_dp_dp,
        ddx_adsorpt_dp_dT=ddx_adsorpt_dp_dT) / M_adsorpt
        "Partial derivative of the specific enthalpy of adsorption of the last 
        adsorbed molecule w.r.t. pressure at constant temperature";

    else
      dh_ads_dp_T = calc_dh_ads_dp_T_Dubinin(
        c=c,
        p_adsorpt=p_adsorpt,
        T_adsorpt=T_adsorpt,
        x_adsorpt=x_adsorpt,
        dx_adsorpt_dp=dx_dp_T,
        v_adsorpt=v,
        dv_adsorpt_dp=dv_dp_T,
        beta_adsorpt=beta,
        dbeta_adsorpt_dp=dbeta_dp_T,
        properties_adsorptive=properties_adsorptive)
        "Partial derivative of the specific enthalpy of adsorption of the last 
        adsorbed molecule w.r.t. pressure at constant temperature";

    end if;
  end if;

  if not require_dh_ads_dT_p then
    dh_ads_dT_p = 0
      "Partial derivative of the specific enthalpy of adsorption of the last
      adsorbed molecule w.r.t. temperature at constant pressure";

  else
    if approachSorptionEnthalpy ==
      SorpLib.Choices.SorptionEnthalpy.Constant then
      dh_ads_dT_p = 0
        "Partial derivative of the specific enthalpy of adsorption of the last
        adsorbed molecule w.r.t. temperature at constant pressure";

    elseif approachSorptionEnthalpy ==
      SorpLib.Choices.SorptionEnthalpy.Formal then
      dh_ads_dT_p =
        SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.dh_ads_dT(
        M_adsorptive=M_adsorpt,
        T_adsorpt=T_adsorpt,
        v_adsorptive=properties_adsorptive.state.v,
        v_adsorpt=if neglectSpecifcVolume then 0 else v,
        dv_adsorptive_dT=properties_adsorptive.dv_dT_p,
        dv_adsorpt_dT=if neglectSpecifcVolume then 0 else dv_dT_p,
        dx_adsorpt_dp=dx_dp_T,
        dx_adsorpt_dT=dx_dT_p,
        ddx_adsorpt_dT_dT=ddx_adsorpt_dT_dT,
        ddx_adsorpt_dp_dT=ddx_adsorpt_dp_dT) / M_adsorpt
        "Partial derivative of the specific enthalpy of adsorption of the last
        adsorbed molecule w.r.t. temperature at constant pressure";

    elseif approachSorptionEnthalpy ==
      SorpLib.Choices.SorptionEnthalpy.ClausiusClapeyron then
      dh_ads_dT_p =
        SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.dh_ads_dT_clausiusClapeyron(
        p_adsorpt=p_adsorpt,
        T_adsorpt=T_adsorpt,
        dx_adsorpt_dp=dx_dp_T,
        dx_adsorpt_dT=dx_dT_p,
        ddx_adsorpt_dT_dT=ddx_adsorpt_dT_dT,
        ddx_adsorpt_dp_dT=ddx_adsorpt_dp_dT) / M_adsorpt
        "Partial derivative of the specific enthalpy of adsorption of the last
        adsorbed molecule w.r.t. temperature at constant pressure";

    else
      dh_ads_dT_p = calc_dh_ads_dT_p_Dubinin(
        c=c,
        dc_dT_adsorpt=dc_dT_adsorpt,
        p_adsorpt=p_adsorpt,
        T_adsorpt=T_adsorpt,
        x_adsorpt=x_adsorpt,
        dx_adsorpt_dT=dx_dT_p,
        v_adsorpt=v,
        dv_adsorpt_dT=dv_dT_p,
        beta_adsorpt=beta,
        dbeta_adsorpt_dT=dbeta_dT_p,
        properties_adsorptive=properties_adsorptive)
        "Partial derivative of the specific enthalpy of adsorption of the last
        adsorbed molecule w.r.t. temperature at constant pressure";

    end if;
  end if;

  //
  // Specific heat capacity of the last adsorbed molecule or uptake-averaged
  // adsorpt
  //
  if not require_cp then
    cp = 0
      "Specific heat capacity: Corresponds to last adsorbed molecule but is
      independent of uptake";
  else
    if approachSpecificHeatCapacity ==
      SorpLib.Choices.SpecificHeatCapacityAdsorpt.Constant then
      cp = cp_adsorpt_constant
        "Specific heat capacity: Corresponds to last adsorbed molecule but is
        independent of uptake";

    elseif approachSpecificHeatCapacity ==
      SorpLib.Choices.SpecificHeatCapacityAdsorpt.BoilingCurve then
      cp = properties_adsorptive.cp_satLiq
        "Specific heat capacity: Corresponds to last adsorbed molecule but is
        independent of uptake";

    elseif approachSpecificHeatCapacity ==
      SorpLib.Choices.SpecificHeatCapacityAdsorpt.GeneralizedFunction then
      cp = SorpLib.Media.Functions.Utilities.generalizedFunction_T(
        T=T_adsorpt,
        T_ref=T_ref_cp_adsorpt,
        z_ref=cp_adsorpt_ref,
        coefficients=coefficients_cp_adsorpt,
        exponents=exponents_cp_adsorpt,
        approach=cp_adsorpt_function)
        "Specific heat capacity: Corresponds to last adsorbed molecule but is
        independent of uptake";

    elseif approachSpecificHeatCapacity ==
      SorpLib.Choices.SpecificHeatCapacityAdsorpt.Interpolation then
      if cp_adsorpt_interpolation ==
        SorpLib.Choices.InterpolationApproach.Linear then
        cp = SorpLib.Media.Functions.Utilities.linearInterpolation_T(
          T=T_adsorpt,
          abscissa=abscissa_cp_adsorpt,
          ordinate=ordinate_cp_adsorpt)
          "Specific heat capacity: Corresponds to last adsorbed molecule but is
          independent of uptake";

      else
        cp = SorpLib.Media.Functions.Utilities.cubicSplineInterpolation_T(
          T=T_adsorpt,
          abscissa=abscissa_cp_adsorpt,
          ordinate=ordinate_cp_adsorpt,
          coefficients=coefficients_cubicSplines_cp_adsorpt)
          "Specific heat capacity: Corresponds to last adsorbed molecule but is
          independent of uptake";

      end if;

    elseif approachSpecificHeatCapacity ==
      SorpLib.Choices.SpecificHeatCapacityAdsorpt.WaltonLeVan then
      if approachSorptionEnthalpy ==
        SorpLib.Choices.SorptionEnthalpy.Constant then
        cp = properties_adsorptive.cp
          "Specific heat capacity: Corresponds to last adsorbed molecule but is
          dependent of uptake";

      elseif approachSorptionEnthalpy ==
        SorpLib.Choices.SorptionEnthalpy.Formal then
        cp =  properties_adsorptive.cp -
          1 / ((x_adsorpt-x_adsorpt_lb) * M_adsorpt) *
          Modelica.Math.Nonlinear.quadratureLobatto(
          f=function calc_integrand_WaltonLeVan(
            T_adsorpt=T_adsorpt,
            c=c,
            dc_dT_adsorpt=dc_dT_adsorpt,
            ddc_dT_adsorpt_dT_adsorpt=ddc_dT_adsorpt_dT_adsorpt,
            adsorptiveAtDewPoint=adsorptiveAtDewPoint,
            v_adsorpt=if neglectSpecifcVolume then 0 else v,
            dv_adsorpt_dp=if neglectSpecifcVolume then 0 else dv_dp_T,
            dv_adsorpt_dT=if neglectSpecifcVolume then 0 else dv_dT_p,
            p_clausiusClyperon=p_adsorpt_min,
            dp=dp,
            dT=dT),
          a=x_adsorpt_lb,
          b=x_adsorpt,
          tolerance=tolerance_adsorpt_int_cp)
          "Specific heat capacity: Corresponds to last adsorbed molecule but is
          dependent of uptake";

      elseif approachSorptionEnthalpy ==
        SorpLib.Choices.SorptionEnthalpy.ClausiusClapeyron then
        cp =  properties_adsorptive.cp -
          1 / ((x_adsorpt-x_adsorpt_lb) * M_adsorpt) *
          Modelica.Math.Nonlinear.quadratureLobatto(
          f=function calc_integrand_WaltonLeVan_CC(
            T_adsorpt=T_adsorpt,
            c=c,
            dc_dT_adsorpt=dc_dT_adsorpt,
            ddc_dT_adsorpt_dT_adsorpt=ddc_dT_adsorpt_dT_adsorpt),
          a=x_adsorpt_lb,
          b=x_adsorpt,
          tolerance=tolerance_adsorpt_int_cp)
          "Specific heat capacity: Corresponds to last adsorbed molecule but is
          dependent of uptake";

      else
        cp =  properties_adsorptive.cp -
          1 / ((x_adsorpt-x_adsorpt_lb) * M_adsorpt) *
          Modelica.Math.Nonlinear.quadratureLobatto(
          f=function calc_integrand_WaltonLeVan_Dubinin(
            T_adsorpt=T_adsorpt,
            c=c,
            dc_dT_adsorpt=dc_dT_adsorpt,
            ddc_dT_adsorpt_dT_adsorpt=ddc_dT_adsorpt_dT_adsorpt,
            adsorptiveAtDewPoint=adsorptiveAtDewPoint,
            v_adsorpt=v,
            dv_adsorpt_dp=dv_dp_T,
            dv_adsorpt_dT=dv_dT_p,
            beta_adsorpt=beta,
            dbeta_adsorpt_dp=dbeta_dp_T,
            dbeta_adsorpt_dT=dbeta_dT_p,
            p_clausiusClyperon=p_adsorpt_min,
            dp=dp,
            dT=dT),
          a=x_adsorpt_lb,
          b=x_adsorpt,
          tolerance=tolerance_adsorpt_int_cp)
          "Specific heat capacity: Corresponds to last adsorbed molecule but is
          dependent of uptake";

      end if;

    elseif approachSpecificHeatCapacity ==
      SorpLib.Choices.SpecificHeatCapacityAdsorpt.ChakrabortyElAl then
        cp =
          SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.cp_ChakrabortyEtAl(
          M_adsorptive=M_adsorpt,
          T_adsorpt=T_adsorpt,
          cp_adsorptive=properties_adsorptive.cp,
          v_adsorptive=properties_adsorptive.state.v,
          dv_adsorptive_dT=properties_adsorptive.dv_dT_p,
          v_adsorpt=if neglectSpecifcVolume then 0 else v,
          h_ads=h_ads*M_adsorpt,
          dh_ads_dT_x=M_adsorpt * (dh_ads_dT_p + dh_ads_dp_T * dp_dT_x))
          "Specific heat capacity: Corresponds to last adsorbed molecule but is
          dependent of uptake";

    else
      if approachSorptionEnthalpy ==
        SorpLib.Choices.SorptionEnthalpy.Constant then
        cp = 1 / ((x_adsorpt-x_adsorpt_lb)) *
          Modelica.Math.Nonlinear.quadratureLobatto(
          f=function calc_integrand_SchwambergerSchmidt_Constant(
            T_adsorpt=T_adsorpt,
            c=c,
            adsorptiveAtDewPoint=adsorptiveAtDewPoint,
            p_clausiusClyperon=p_adsorpt_min,
            dp=dp,
            dT=dT),
          a=x_adsorpt_lb,
          b=x_adsorpt,
          tolerance=tolerance_adsorpt_int_cp)
          "Specific heat capacity: Corresponds to uptake-averaged adsorpt";

      elseif approachSorptionEnthalpy ==
        SorpLib.Choices.SorptionEnthalpy.Formal then
        cp = 1 / ((x_adsorpt-x_adsorpt_lb)) *
          Modelica.Math.Nonlinear.quadratureLobatto(
          f=function calc_integrand_SchwambergerSchmidt(
            T_adsorpt=T_adsorpt,
            c=c,
            dc_dT_adsorpt=dc_dT_adsorpt,
            ddc_dT_adsorpt_dT_adsorpt=ddc_dT_adsorpt_dT_adsorpt,
            adsorptiveAtDewPoint=adsorptiveAtDewPoint,
            v_adsorpt=if neglectSpecifcVolume then 0 else v,
            dv_adsorpt_dp=if neglectSpecifcVolume then 0 else dv_dp_T,
            dv_adsorpt_dT=if neglectSpecifcVolume then 0 else dv_dT_p,
            p_clausiusClyperon=p_adsorpt_min,
            dp=dp,
            dT=dT),
          a=x_adsorpt_lb,
          b=x_adsorpt,
          tolerance=tolerance_adsorpt_int_cp)
          "Specific heat capacity: Corresponds to uptake-averaged adsorpt";

      elseif approachSorptionEnthalpy ==
        SorpLib.Choices.SorptionEnthalpy.ClausiusClapeyron then
        cp = 1 / ((x_adsorpt-x_adsorpt_lb)) *
          Modelica.Math.Nonlinear.quadratureLobatto(
          f=function calc_integrand_SchwambergerSchmidt_CC(
            T_adsorpt=T_adsorpt,
            c=c,
            dc_dT_adsorpt=dc_dT_adsorpt,
            ddc_dT_adsorpt_dT_adsorpt=ddc_dT_adsorpt_dT_adsorpt,
            adsorptiveAtDewPoint=adsorptiveAtDewPoint,
            p_clausiusClyperon=p_adsorpt_min,
            dp=dp,
            dT=dT),
          a=x_adsorpt_lb,
          b=x_adsorpt,
          tolerance=tolerance_adsorpt_int_cp)
          "Specific heat capacity: Corresponds to uptake-averaged adsorpt";

      else
        cp =  1 / ((x_adsorpt-x_adsorpt_lb)) *
          Modelica.Math.Nonlinear.quadratureLobatto(
          f=function calc_integrand_SchwambergerSchmidt_Dubinin(
            T_adsorpt=T_adsorpt,
            c=c,
            dc_dT_adsorpt=dc_dT_adsorpt,
            ddc_dT_adsorpt_dT_adsorpt=ddc_dT_adsorpt_dT_adsorpt,
            adsorptiveAtDewPoint=adsorptiveAtDewPoint,
            v_adsorpt=v,
            dv_adsorpt_dp=dv_dp_T,
            dv_adsorpt_dT=dv_dT_p,
            beta_adsorpt=beta,
            dbeta_adsorpt_dp=dbeta_dp_T,
            dbeta_adsorpt_dT=dbeta_dT_p,
            p_clausiusClyperon=p_adsorpt_min,
            dp=dp,
            dT=dT),
          a=x_adsorpt_lb,
          b=x_adsorpt,
          tolerance=tolerance_adsorpt_int_cp)
          "Specific heat capacity: Corresponds to uptake-averaged adsorpt";

      end if;

    end if;
  end if;

  //
  // Partial derivatives required for mass, energy, and entropy balances
  //
  dxv_avg_dp_T = if not require_dxv_avg_dp_T or neglectSpecifcVolume then 0
    else x_adsorpt * dv_dp_T + dx_dp_T * v
    "Partial derivative of the uptake-averaged specific volume times the uptake 
    w.r.t. pressure at constant temperature: Note that v_avg = v, as specific 
    volume is just a function of temperature";
  dxv_avg_dT_p = if not require_dxv_avg_dT_p or neglectSpecifcVolume then 0
    else x_adsorpt * dv_dT_p + dx_dT_p * v
    "Partial derivative of the uptake-averaged specific volume times the uptake  
    w.r.t. temperature at constant pressure: Note that v_avg = v, as specific 
    volume is just a function of temperature";

  dxh_avg_dp_T = if not require_dxh_avg_dp_T then 0 else
    dxh_avg_dx_T / dp_dx_T
    "Partial derivative of the uptake-averaged specific enthalpy times the uptake 
    w.r.t. pressure at constant temperature";
  dxh_avg_dT_p = if not require_dxh_avg_dT_p then 0 else
    dxh_avg_dT_x - dxh_avg_dp_T * dp_dT_x
    "Partial derivative of the uptake-averaged specific enthalpy times the uptake
    w.r.t. temperature at constant pressure";
  dxh_avg_dx_T = if not require_dxh_avg_dx_T then 0 else
    (properties_adsorptive.state.h - h_ads)
    "Partial derivative of the uptake-averaged specific enthalpy times the uptake 
    w.r.t. uptake at constant temperature";
  dxh_avg_dT_x = if not require_dxh_avg_dT_x then 0 else
    x_adsorpt * cp
    "Partial derivative of the uptake-averaged specific enthalpy times the uptake 
    w.r.t. temperature at constant uptake";

  dxs_avg_dp_T = if not require_dxs_avg_dp_T then 0 else
    dxs_avg_dx_T / dp_dx_T
    "Partial derivative of the uptake-averaged specific entropy times the uptake 
    w.r.t. pressure at constant temperature";
  dxs_avg_dT_p = if not require_dxs_avg_dT_p then 0 else
    dxs_avg_dT_x - dxs_avg_dp_T * dp_dT_x
    "Partial derivative of the uptake-averaged specific entropy times the uptake 
    w.r.t. temperature at constant pressure";
  dxs_avg_dx_T = if not require_dxs_avg_dx_T then 0 else
    (properties_adsorptive.state.s - h_ads / T_adsorpt)
    "Partial derivative of the uptake-averaged specific entropy times the uptake 
    w.r.t. uptake at constant temperature";
  dxs_avg_dT_x = if not require_dxs_avg_dT_x then 0 else
    x_adsorpt * cp  / T_adsorpt
    "Partial derivative of the uptake-averaged specific entropy times the uptake 
    w.r.t. temperature at constant uptake";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This model calculates all properties of the adsorpt required for the working
pair model at once. Thus, computational costs are reduced because redundant
calculations of the same properties are avoided.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end AdsorptProperties;
