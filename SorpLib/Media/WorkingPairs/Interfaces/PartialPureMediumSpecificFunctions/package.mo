within SorpLib.Media.WorkingPairs.Interfaces;
partial package PartialPureMediumSpecificFunctions "Base package defining medium specific functions for pure component adsorption"
  extends Modelica.Icons.FunctionsPackage;

  //
  // Definition of models
  //
  replaceable partial model AdsorptiveProperties
    "Calculates all properties of the adsorptive at gas/vapor phase required for the working pair model at once"
    extends Modelica.Icons.MaterialProperty;

    //
    // Definition of parameters
    //
    parameter Boolean calcCaloricProperties = true
      "= true, if caloric properties are calculated (i.e., h, u, h_ads, and cp)"
      annotation (Dialog(tab = "General", group = "Calculation Setup"),
                  choices(checkBox=true),
                  Evaluate=true,
                  HideResult=true);
    parameter Boolean calcEntropicProperties = false
      "= true, if caloric properties are calculated (i.e., s, g, a, and s_ads)"
      annotation (Dialog(tab = "General", group = "Calculation Setup",
                  enable = calcCaloricProperties),
                  choices(checkBox=true),
                  Evaluate=true,
                  HideResult=true);

    parameter Boolean calcAdsorptAdsorbateState = false
      "= true, if state properties of average adsorpt phase and adosrbate phase are
    calculated (i.e., requires numerical integration)"
      annotation (Dialog(tab = "General", group = "Calculation Setup",
                  enable = calcCaloricProperties),
                  choices(checkBox=true),
                  Evaluate=true,
                  HideResult=true);

    parameter Boolean calcDerivativesIsotherm = true
      "= true, if partial derivatives of isotherms required for mass, energy, and
    entropy balance of working paur volumes are calculated"
      annotation (Dialog(tab = "General", group = "Calculation Setup"),
                  choices(checkBox=true),
                  Evaluate=true,
                  HideResult=true);
    parameter Boolean calcDerivativesMassEnergyBalance = true
      "= true, if partial derivatives required for mass and energy balance of
    working pair volumes are calculated"
      annotation (Dialog(tab = "General", group = "Calculation Setup",
                  enable = calcDerivativesIsotherm and calcCaloricProperties),
                  choices(checkBox=true),
                  Evaluate=true,
                  HideResult=true);
    parameter Boolean calcDerivativesEntropyBalance = false
      "= true, if partial derivatives required for entropy balance of working
    pair volumes are calculated"
      annotation (Dialog(tab = "General", group = "Calculation Setup",
                  enable = calcDerivativesIsotherm and
                    calcDerivativesMassEnergyBalance and calcCaloricProperties),
                  choices(checkBox=true),
                  Evaluate=true,
                  HideResult=true);

    parameter Boolean adsorptiveAtDewPoint = false
      "= true, if adsorptive (gas/vapor phase) is assumed to be at dew point at
    T"
      annotation (Dialog(tab = "General", group = "Calculation Setup"),
                  Evaluate=true,
                  HideResult=true,
                  choices(checkBox=true));

    parameter Boolean neglectSpecifcVolume = false
      "= true, if specific volume of the adsorpt is neglected (i.e., v_adsorpt = 0
    -> u_adsorpt = h_adsorpt and g_adsorpt = a_adsorpt)"
      annotation (Dialog(tab = "General", group = "Calculation Setup"),
                  choices(checkBox=true),
                  Evaluate= false,
                  HideResult=true);
    parameter SorpLib.Choices.SpecificVolumeAdsorpt approachSpecificVolume=
        SorpLib.Choices.SpecificVolumeAdsorpt.Constant
      "Calculation approach for the specific volume of the adsorpt"
      annotation (Dialog(tab = "General", group = "Calculation Setup",
                  enable=not neglectSpecifcVolume),
                  Evaluate=false,
                  HideResult=true,
                  choicesAllMatching=true);
    parameter SorpLib.Choices.SpecificHeatCapacityAdsorpt
      approachSpecificHeatCapacity=SorpLib.Choices.SpecificHeatCapacityAdsorpt.Constant
      "Calculation approach for the specific heat capacity of the adsorpt"
      annotation (Dialog(tab = "General", group = "Calculation Setup"),
                  Evaluate=false,
                  HideResult=true,
                  choicesAllMatching=true);
    parameter SorpLib.Choices.SorptionEnthalpy approachSorptionEnthalpy=
      SorpLib.Choices.SorptionEnthalpy.Constant
      "Calculation approach for the sorption enthalpy"
      annotation (Dialog(tab = "General", group = "Calculation Setup"),
                  Evaluate=false,
                  HideResult=true,
                  choicesAllMatching=true);

    parameter Modelica.Units.SI.PressureDifference dp = 1e-3
      "Pressure difference if derivative is calculated numerically"
      annotation (Dialog(tab="Advanced", group="Numerics - Derivatives"),
                  Evaluate=true,
                  HideResult=true);
    parameter Modelica.Units.SI.TemperatureDifference dT = 1e-3
      "Temperature difference if derivative is calculated numerically"
      annotation (Dialog(tab="Advanced", group="Numerics - Derivatives"),
                  Evaluate=true,
                  HideResult=true);

    //
    // Definition of general inputs
    //
    input Modelica.Units.SI.Pressure p
      "Pressure"
      annotation (Dialog(tab="General",group="Inputs"));
    input Modelica.Units.SI.Temperature T
      "Temperature"
      annotation (Dialog(tab="General",group="Inputs"));

    //
    // Definition of outputs
    //
    output SorpLib.Media.WorkingPairs.Records.PropertiesPureAdsorptive properties=
      SorpLib.Media.WorkingPairs.Records.PropertiesPureAdsorptive(
        state = SorpLib.Media.WorkingPairs.Records.StateVariables(
          p=p,
          T=T,
          v=v,
          h=h,
          u=u,
          s=s,
          g=g,
          a=a),
        cp=cp,
        p_sat=p_sat,
        v_satLiq=v_satLiq,
        h_adsorptiveToLiquid=h_adsorptiveToLiquid,
        cp_satLiq=cp_satLiq,
        dv_dp_T=dv_dp_T,
        dv_dT_p=dv_dT_p,
        dp_sat_dT=dp_sat_dT,
        dv_satLiq_dT=dv_satLiq_dT,
        ddv_satLiq_dT_dT=ddv_satLiq_dT_dT,
        dh_adsorptiveToLiquid_dp_T=dh_adsorptiveToLiquid_dp_T,
        dh_adsorptiveToLiquid_dT_p=dh_adsorptiveToLiquid_dT_p)
      "Record containing all required properties of the adsorptive"
      annotation (Dialog(tab="General", group="Outputs", enable=false));

    //
    // Parameters indicating which parameters must be calculated
    //
protected
    parameter Boolean require_caloricAdsorptAdsorbateState=
      (calcCaloricProperties and calcAdsorptAdsorbateState)
      "= true, if uptake-averaged thermal and caloric properties of the adsorpt and 
    adsorbate phase are required";
    parameter Boolean require_entropicAdsorptAdsorbateState=
      (require_caloricAdsorptAdsorbateState and calcEntropicProperties)
      "= true, if uptake-averaged entropic properties of the adsorpt and adsorbate 
    phase are required";

    parameter Boolean require_massEnergyBalances=
      (calcCaloricProperties and calcDerivativesIsotherm and
      calcDerivativesMassEnergyBalance)
      "= true, if properties for mass and energy balance are required";
    parameter Boolean require_entropyBalance=
      (require_massEnergyBalances and calcDerivativesEntropyBalance)
      "= true, if properties for entropy balance are required";

    parameter Boolean require_dxv_avg_dp_T=
      (require_massEnergyBalances)
      "= true, if partial derivative of uptake-averaged specific volume of the adsorpt
    times the uptake w.r.t. pressure at constant temperature is required";
    parameter Boolean require_dxv_avg_dT_p=
      (require_massEnergyBalances)
      "= true, if partial derivative of uptake-averaged specific volume of the adsorpt
    times the uptake w.r.t. temperature at constant pressure is required";

    parameter Boolean require_h_ads_Formal=
      (calcCaloricProperties and approachSorptionEnthalpy ==
      SorpLib.Choices.SorptionEnthalpy.Formal)
      "= true, if properties for the specific enthalpy of adsorption are required";
    parameter Boolean require_h_ads_CC=
      (calcCaloricProperties and approachSorptionEnthalpy ==
      SorpLib.Choices.SorptionEnthalpy.ClausiusClapeyron)
      "= true, if properties for the specific enthalpy of adsorption according to
    Clausius Clapeyron are required";
    parameter Boolean require_h_ads_Dubinin=
      (calcCaloricProperties and approachSorptionEnthalpy ==
      SorpLib.Choices.SorptionEnthalpy.Dubinin)
      "= true, if properties for the specific enthalpy of adsorption according to
    Dubinin are required";

    parameter Boolean require_cp_adsorpt_ChakrabortyElAl=
      (require_h_ads_Formal and approachSpecificHeatCapacity ==
      SorpLib.Choices.SpecificHeatCapacityAdsorpt.ChakrabortyElAl)
      "= true, if properties for the specific heat capacity according to Chakraborty
    et al. are required";
    parameter Boolean require_cp_adsorpt_ChakrabortyElAl_CC=
      (require_h_ads_CC and approachSpecificHeatCapacity ==
      SorpLib.Choices.SpecificHeatCapacityAdsorpt.ChakrabortyElAl)
      "= true, if properties for the specific heat capacity according to Chakraborty
    et al. with the adsorption enthalpy according to Clausius Clapeyron are required";
    parameter Boolean require_cp_adsorpt_ChakrabortyElAl_Dubinin=
      (require_h_ads_Dubinin and approachSpecificHeatCapacity ==
      SorpLib.Choices.SpecificHeatCapacityAdsorpt.ChakrabortyElAl)
      "= true, if properties for the specific heat capacity according to Chakraborty
    et al. with the adsorption enthalpy according to Dubinin are required";

    parameter Boolean require_cp_adsorpt_WaltonLeVan=
      (require_h_ads_Formal and approachSpecificHeatCapacity ==
      SorpLib.Choices.SpecificHeatCapacityAdsorpt.WaltonLeVan)
      "= true, if properties for the specific heat capacity according to Walton and
    Le Van are required";
    parameter Boolean require_cp_adsorpt_WaltonLeVan_CC=
      (require_h_ads_CC and approachSpecificHeatCapacity ==
      SorpLib.Choices.SpecificHeatCapacityAdsorpt.WaltonLeVan)
      "= true, if properties for the specific heat capacity according to Walton and
    Le Van with the adsorption enthalpy according to Clausius Clapeyron are required";
    parameter Boolean require_cp_adsorpt_WaltonLeVan_Dubinin=
      (require_h_ads_Dubinin and approachSpecificHeatCapacity ==
      SorpLib.Choices.SpecificHeatCapacityAdsorpt.WaltonLeVan)
      "= true, if properties for the specific heat capacity according to Walton and
    Le Van with the adsorption enthalpy according to Dubinin are required";

    parameter Boolean require_cp_adsorpt_SchwambergerSchmidt=
      (require_h_ads_Formal and approachSpecificHeatCapacity ==
      SorpLib.Choices.SpecificHeatCapacityAdsorpt.SchwambergerSchmidt)
      "= true, if properties for the specific heat capacity according to Schwamberger
    and Schmidt are required";
    parameter Boolean require_cp_adsorpt_SchwambergerSchmidt_CC=
      (require_h_ads_CC and approachSpecificHeatCapacity ==
      SorpLib.Choices.SpecificHeatCapacityAdsorpt.SchwambergerSchmidt)
      "= true, if properties for the specific heat capacity according to Schwamberger
    and Schmidt with the adsorption enthalpy according to Clausius Clapeyron are required";
    parameter Boolean require_cp_adsorpt_SchwambergerSchmidt_Dubinin=
      (require_h_ads_Dubinin and approachSpecificHeatCapacity ==
      SorpLib.Choices.SpecificHeatCapacityAdsorpt.SchwambergerSchmidt)
      "= true, if properties for the specific heat capacity according to Schwamberger
    and Schmidt with the adsorption enthalpy according to Dubinin are required";

    parameter Boolean require_v_adsorpt=
      not neglectSpecifcVolume or
      not neglectSpecifcVolume and
      ((require_dxv_avg_dp_T) or
      (require_dxv_avg_dT_p) or
      (require_caloricAdsorptAdsorbateState and require_h_ads_Formal) or
      (require_entropicAdsorptAdsorbateState and require_h_ads_Formal) or
      (require_h_ads_Formal) or
      (require_cp_adsorpt_ChakrabortyElAl) or
      (require_cp_adsorpt_ChakrabortyElAl_CC) or
      (require_cp_adsorpt_WaltonLeVan) or
      (require_cp_adsorpt_SchwambergerSchmidt)) or
      (require_h_ads_Dubinin) or
      (require_caloricAdsorptAdsorbateState and require_h_ads_Dubinin) or
      (require_entropicAdsorptAdsorbateState and require_h_ads_Dubinin) or
      (require_cp_adsorpt_ChakrabortyElAl_Dubinin) or
      (require_cp_adsorpt_WaltonLeVan_Dubinin) or
      (require_cp_adsorpt_SchwambergerSchmidt_Dubinin)
      "= true, if specific volume of the adsorpt is required";
    parameter Boolean require_dv_adsorpt_dp_T=
      not neglectSpecifcVolume and
      ((require_dxv_avg_dp_T) or
      (require_cp_adsorpt_ChakrabortyElAl) or
      (require_cp_adsorpt_WaltonLeVan) or
      (require_cp_adsorpt_SchwambergerSchmidt)) or
      (require_cp_adsorpt_ChakrabortyElAl_Dubinin) or
      (require_cp_adsorpt_WaltonLeVan_Dubinin) or
      (require_cp_adsorpt_SchwambergerSchmidt_Dubinin)
      "= true, if the partial derivative of the specific volume of the adsorpt w.r.t.
    pressure at constant temperature is required";
    parameter Boolean require_dv_adsorpt_dT_p=
      (require_dv_adsorpt_dp_T) or
      not neglectSpecifcVolume and
      ((require_dxv_avg_dT_p) or
      (require_cp_adsorpt_ChakrabortyElAl) or
      (require_cp_adsorpt_ChakrabortyElAl_CC) or
      (require_cp_adsorpt_WaltonLeVan) or
      (require_cp_adsorpt_SchwambergerSchmidt)) or
      (require_h_ads_Dubinin) or
      (require_cp_adsorpt_ChakrabortyElAl_Dubinin) or
      (require_cp_adsorpt_WaltonLeVan_Dubinin) or
      (require_cp_adsorpt_SchwambergerSchmidt_Dubinin)
      "= true, if the partial derivative of the specific volume of the adsorpt w.r.t.
    temperature at pressure temperature is required";
    parameter Boolean require_ddv_adsorpt_dT_dT_p=
      (require_cp_adsorpt_ChakrabortyElAl_Dubinin) or
      (require_cp_adsorpt_WaltonLeVan_Dubinin) or
      (require_cp_adsorpt_SchwambergerSchmidt)
      "= true, if the second-order partial derivative of the specific volume of the 
    adsorpt w.r.t. temperature at pressure temperature is required";

    parameter Boolean require_dv_adsorptive_dp_T=
      (require_h_ads_Formal and require_massEnergyBalances) or
      (require_h_ads_Formal and require_entropyBalance) or
      (require_cp_adsorpt_ChakrabortyElAl)
      "= true, if the partial derivative of the specific volume of the adsorptive 
    w.r.t. pressure at constant temperature is required";
    parameter Boolean require_dv_adsorptive_dT_p=
      (require_dv_adsorptive_dp_T) or
      (require_cp_adsorpt_ChakrabortyElAl) or
      (require_cp_adsorpt_ChakrabortyElAl_CC) or
      (require_cp_adsorpt_ChakrabortyElAl_Dubinin)
      "= true, if the partial derivative of the specific volume of the adsorptive 
    w.r.t. temperature at pressure temperature is required";

    parameter Boolean require_cp_adsorptive=
      (require_massEnergyBalances) or
      (require_entropyBalance) or
      (require_cp_adsorpt_ChakrabortyElAl) or
      (require_cp_adsorpt_ChakrabortyElAl_CC) or
      (require_cp_adsorpt_ChakrabortyElAl_Dubinin) or
      (require_cp_adsorpt_WaltonLeVan) or
      (require_cp_adsorpt_WaltonLeVan_CC) or
      (require_cp_adsorpt_WaltonLeVan_Dubinin)
      "= true, if the specific heat capacity of the adsorptive is required";

    parameter Boolean require_p_sat=
      (require_v_adsorpt and approachSpecificVolume ==
      SorpLib.Choices.SpecificVolumeAdsorpt.BoilingCurve) or
      (require_h_ads_Dubinin) or
      (require_cp_adsorpt_ChakrabortyElAl_Dubinin) or
      (require_cp_adsorpt_WaltonLeVan_Dubinin) or
      (require_cp_adsorpt_SchwambergerSchmidt_Dubinin)
      "= true, if the vapor pressure is required";
    parameter Boolean require_dp_sat_dT=
      (require_massEnergyBalances and
      approachSpecificVolume == SorpLib.Choices.SpecificVolumeAdsorpt.BoilingCurve) or
      (require_massEnergyBalances and require_h_ads_Dubinin) or
      (require_entropyBalance and require_h_ads_Dubinin) or
      (require_cp_adsorpt_ChakrabortyElAl_Dubinin) or
      (require_cp_adsorpt_WaltonLeVan_Dubinin) or
      (require_cp_adsorpt_SchwambergerSchmidt_Dubinin)
      "= true, if the partial derivative of the vapor pressure w.r.. temperature is
    required";

    parameter Boolean require_rho_satLiq_T=
      (require_v_adsorpt and approachSpecificVolume ==
      SorpLib.Choices.SpecificVolumeAdsorpt.BoilingCurve)
      "= true, if the bubble point density at given temperature is required";
    parameter Boolean require_drho_satLiq_dT=
      (require_dv_adsorpt_dT_p and approachSpecificVolume ==
      SorpLib.Choices.SpecificVolumeAdsorpt.BoilingCurve)
      "= true, if the partial derivative of the bubble point density at given 
    temperature w.r.t. tempreature is required";
    parameter Boolean require_ddrho_satLiq_dT_dT=
      (require_ddv_adsorpt_dT_dT_p and approachSpecificVolume ==
      SorpLib.Choices.SpecificVolumeAdsorpt.BoilingCurve)
      "= true, if the second-order partial derivative of the bubble point density 
    at given temperature w.r.t. tempreature is required";

    parameter Boolean require_h_adsorptiveToLiquid=
      (require_h_ads_Dubinin) or
      (require_cp_adsorpt_ChakrabortyElAl_Dubinin) or
      (require_cp_adsorpt_WaltonLeVan_Dubinin) or
      (require_cp_adsorpt_SchwambergerSchmidt_Dubinin)
      "= true, if the specific enthalpy of vaporization is required";
    parameter Boolean require_dh_adsorptiveToLiquid_dp_T=
      (require_h_ads_Dubinin and require_massEnergyBalances) or
      (require_h_ads_Dubinin and require_entropyBalance) or
      (require_cp_adsorpt_ChakrabortyElAl_Dubinin) or
      (require_cp_adsorpt_WaltonLeVan_Dubinin) or
      (require_cp_adsorpt_SchwambergerSchmidt_Dubinin)
      "= true, if the partial derivatieve of the specific enthalpy of vaporization 
    w.r.t. pressure and constant temperature is required";
    parameter Boolean require_dh_adsorptiveToLiquid_dT_p=
      (require_dh_adsorptiveToLiquid_dp_T)
      "= true, if the partial derivatieve of the specific enthalpy of vaporization 
    w.r.t. temperature and constant pressure is required";

    parameter Boolean require_cp_satLiq_T=
      (calcCaloricProperties and approachSpecificHeatCapacity==
      SorpLib.Choices.SpecificHeatCapacityAdsorpt.BoilingCurve)
      "= true, if the specific heat capacity of the bubble point at given temperature
     is required";

    //
    // State variables
    //
    Modelica.Units.SI.SpecificVolume v
      "Specific volume";
    Modelica.Units.SI.SpecificEnthalpy h
      "Specific enthalpy";
    Modelica.Units.SI.SpecificInternalEnergy u
      "Specific internal energy";
    Modelica.Units.SI.SpecificEntropy s
      "Specific entropy";
    Modelica.Units.SI.SpecificGibbsFreeEnergy  g
      "Specific free enthalpy (i.e., Gibbs free energy)";
    Modelica.Units.SI.SpecificHelmholtzFreeEnergy  a
      "Specific free energy (i.e., Helmholts free energy)";

    //
    // Additional properties of the one-phase regime
    //
    Modelica.Units.SI.SpecificHeatCapacity cp
      "Specific heat capacity";

    //
    // Additional properties of the two-phase regime
    //
    Modelica.Units.SI.Pressure p_sat
      "Saturated vapor pressure";
    Modelica.Units.SI.SpecificVolume v_satLiq
      "Specific volume at the bubble point at given temperature";

    Modelica.Units.SI.SpecificEnthalpy  h_adsorptiveToLiquid
      "Specific enthalpy difference between adsorptive state and saturated liquid
    state (i.e., bubble point)";

    Modelica.Units.SI.SpecificHeatCapacity cp_satLiq
      "Specific heat capacaity at the bubble point at given temperature";

    //
    // Partial derivatives
    //
    SorpLib.Units.DerSpecificVolumeByPressure dv_dp_T
      "Partial derivative of the specific volume w.r.t. pressure at constant
    temperature";
    SorpLib.Units.DerSpecificVolumeByTemperature dv_dT_p
      "Partial derivative of the specific volume w.r.t. temperature at constant
    pressure";

    Modelica.Media.Common.DerPressureByTemperature dp_sat_dT
      "Partial derivative of saturated vapor pressure w.r.t. temperature";

    SorpLib.Units.DerSpecificVolumeByTemperature dv_satLiq_dT
      "Partial derivative of the specific volume at the bubble point at given
    temperature w.r.t. temperature";
    SorpLib.Units.DerSpecificVolumeByTemperatureTemperature ddv_satLiq_dT_dT
      "Second-order partial derivative of the specific volume at the bubble point 
    at given temperature w.r.t. temperature";

    SorpLib.Units.DerSpecificEnthalpyByPressure dh_adsorptiveToLiquid_dp_T
      "Partial derivative of specific enthalpy difference between adsorptive state 
    and saturated liquid state (i.e., bubble point) w.r.t. pressure at constant 
    temperature";
    Modelica.Units.SI.SpecificHeatCapacity dh_adsorptiveToLiquid_dT_p
      "Partial derivative of specific enthalpy difference between adsorptive state 
    and saturated liquid state (i.e., bubble point) w.r.t. temperature at constant
    pressure";

    //
    // Annotations
    //
    annotation (Documentation(revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>", info="<html>
<p>
This model calculates all properties of the adsorptive required for the working
pair model at once. Thus, computational costs are reduced because redundant
calculations of the same properties are avoided.
</p>
</html>"));
  end AdsorptiveProperties;
  //
  // Functions of the two-phase regime
  //
  replaceable partial function p_sat_T
    "Calculates the vapor pressure as function of temperature"
    extends Modelica.Icons.Function;

    //
    // Definition of inputs
    //
    input Modelica.Units.SI.Temperature T
      "Saturated vapor temperature"
      annotation (Dialog(tab="General",group="Inputs"));

    //
    // Definition of outputs
    //
    output Modelica.Units.SI.Pressure p_sat
      "Saturated vapor pressure"
      annotation (Dialog(tab="General",group="Outputs",enable=false));

    //
    // Annotations
    //
    annotation (Documentation(revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>", info="<html>
<p>
This function calculates the vapor pressure at the bubble point and its first- 
and second-order partial derivatives w.r.t. temperature as function of temperaure.
</p>
</html>"));
  end p_sat_T;

  replaceable partial function rho_satLiq_T
    "Calculates the density at the bubble point as function of temperature"
    extends Modelica.Icons.Function;

    //
    // Definition of inputs
    //
    input Modelica.Units.SI.Temperature T
      "Saturated vapor temperature"
      annotation (Dialog(tab="General",group="Inputs"));

    //
    // Definition of outputs
    //
    output Modelica.Units.SI.Density rho_satLiq
      "Density at bubble point"
      annotation (Dialog(tab="General",group="Outputs",enable=false));

    //
    // Annotations
    //
    annotation (Documentation(revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>", info="<html>
<p>
This function calculates the specific volume at the bubble point and its first- 
and second-order partial derivatives w.r.t. temperature as function of temperaure.
</p>
</html>"));
  end rho_satLiq_T;

  replaceable partial function pRho_satLiq
    "Calculates the vapor pressure, density at the bubble point, and their first- and second-order partial derivatives w.r.t. temperature"
    extends Modelica.Icons.Function;

    //
    // Definition of inputs
    //
    input Modelica.Units.SI.Temperature T
      "Saturated vapor temperature"
      annotation (Dialog(tab="General",group="Inputs"));
    input Modelica.Units.SI.TemperatureDifference dT
      "Temperature difference if derivative is calculated numerically"
      annotation (Dialog(tab="General",group="Inputs"));

    //
    // Definition of outputs
    //
    output Modelica.Units.SI.Pressure p_sat
      "Saturated vapor pressure"
      annotation (Dialog(tab="General",group="Outputs",enable=false));
    output Modelica.Units.SI.DerPressureByTemperature dp_sat_dT
      "Partial derivative of the vapor pressure w.r.t. temperature"
      annotation (Dialog(tab="General",group="Outputs",enable=false));
    output SorpLib.Units.DerPressureByTemperatureTemperature ddp_sat_dT_dT
      "Calculates the second-order partial derivative of the vapor pressure w.r.t. 
    temperature"
      annotation (Dialog(tab="General",group="Outputs",enable=false));


    output Modelica.Units.SI.Density rho_satLiq
      "Density at bubble point"
      annotation (Dialog(tab="General",group="Outputs",enable=false));
    output SorpLib.Units.DerDensityByTemperature drho_satLiq_dT
      "Calculates the partial derivative of the density at the bubble point w.r.t.
      temperature"
      annotation (Dialog(tab="General",group="Outputs",enable=false));
    output SorpLib.Units.DerDensityByTemperatureTemperature ddrho_satLiq_dT_dT
    "Calculates the second-order partial derivative of the density at the bubble 
    point w.r.t. temperature"
      annotation (Dialog(tab="General",group="Outputs",enable=false));

    //
    // Annotations
    //
    annotation (Documentation(revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>", info="<html>
<p>
This function calculates the vapor pressure and the specific volume at the
bubble point as function of temperature. For both properties, this function 
also calculates the first- and second-order partial derivatives w.r.t. temperature.
</p>
</html>"));
  end pRho_satLiq;
  //
  // Functions of the one-phase regime
  //
  replaceable partial function h_pT
    "Calculates the specific enthalpy of the adsorptive as function of pressure
    and temperature"
    extends Modelica.Icons.Function;

    //
    // Definition of inputs
    //
    input Modelica.Units.SI.Pressure p
      "Pressure"
      annotation (Dialog(tab="General",group="Inputs"));
    input Modelica.Units.SI.Temperature T
      "Temperature"
      annotation (Dialog(tab="General",group="Inputs"));

    input Modelica.Units.SI.Pressure p_lb
      "Lower bound of pressure (i.e., if gas state and p < p_lb, assume ideal gas)"
      annotation (Dialog(tab="General",group="Inputs"));

    input Modelica.Units.SI.SpecificEnthalpy h_ref
      "Specific enthalpy at reference pressure and temperature"
      annotation (Dialog(tab="General",group="Inputs"));
    input Modelica.Units.SI.Pressure p_ref
      "Reference pressure"
      annotation (Dialog(tab="General",group="Inputs"));
    input Modelica.Units.SI.Temperature T_ref
      "Reference temperature"
      annotation (Dialog(tab="General",group="Inputs"));

    //
    // Definition of outputs
    //
    output Modelica.Units.SI.SpecificEnthalpy h
      "Specific enthalpy"
      annotation (Dialog(tab="General",group="Outputs",enable=false));

    //
    // Annotations
    //
    annotation (Documentation(revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>", info="<html>
<p>
This function calculates the specific enthalpy as function of pressure and temperature.
If the pressure is below its lower bound <i>p_lb</i>, the specific enthalpy is calculated
assuming an ideal gas using the reference point and the specific heat capacity evaluated
at <i>p_lb</i> and <i>T</i>.
</p>
</html>"));
  end h_pT;

  replaceable partial function s_pT
    "Calculates the specific entropy of the adsorptive as function of pressure
    and temperature"
    extends Modelica.Icons.Function;

    //
    // Definition of inputs
    //
    input Modelica.Units.SI.Pressure p
      "Pressure"
      annotation (Dialog(tab="General",group="Inputs"));
    input Modelica.Units.SI.Temperature T
      "Temperature"
      annotation (Dialog(tab="General",group="Inputs"));

    input Modelica.Units.SI.Pressure p_lb
      "Lower bound of pressure (i.e., if gas state and p < p_lb, assume ideal gas)"
      annotation (Dialog(tab="General",group="Inputs"));

    input Modelica.Units.SI.SpecificEntropy s_ref
      "Specific entropy at reference pressure and temperature"
      annotation (Dialog(tab="General",group="Inputs"));
    input Modelica.Units.SI.Pressure p_ref
      "Reference pressure"
      annotation (Dialog(tab="General",group="Inputs"));
    input Modelica.Units.SI.Temperature T_ref
      "Reference temperature"
      annotation (Dialog(tab="General",group="Inputs"));

    //
    // Definition of outputs
    //
    output Modelica.Units.SI.SpecificEntropy s
      "Specific entropy"
      annotation (Dialog(tab="General",group="Outputs",enable=false));

    //
    // Annotations
    //
    annotation (Documentation(revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>", info="<html>
<p>
This function calculates the specific entropy as function of pressure and temperature.
If the pressure is below its lower bound <i>p_lb</i>, the specific entropy is calculated
assuming an ideal gas using the reference point and the specific heat capacity evaluated
at <i>p_lb</i> and <i>T</i>.
</p>
</html>"));
  end s_pT;
  //
  // Functions calculating properties required for calculating the specific heat
  // capacity of the adsorpt
  //
  replaceable partial function calc_properties
  "Calculates all properties of the adsorptive at gas/vapor phase required for the working pair model at once"
    extends Modelica.Icons.Function;

    input Modelica.Units.SI.Pressure p
      "Pressure"
      annotation (Dialog(tab="General",group="Inputs"));
    input Modelica.Units.SI.Temperature T
      "Temperature"
      annotation (Dialog(tab="General",group="Inputs"));

    input Modelica.Units.SI.PressureDifference dp
      "Pressure difference if derivative is calculated numerically"
      annotation (Dialog(tab="General",group="Inputs"));
    input Modelica.Units.SI.TemperatureDifference dT
      "Temperature difference if derivative is calculated numerically"
      annotation (Dialog(tab="General",group="Inputs"));
    input Modelica.Units.SI.Pressure p_min
      "Minimal pressure at which fluid properties can be calculated (i.e., assumption
    ideal gas at lower pressure)"
      annotation (Dialog(tab="General",group="Inputs"));

    input Boolean require_v_adsorptive
      "= true, if the specific volume of the adsorptive and its partial derivatives
    are required"
      annotation (Dialog(tab="General",group="Inputs"));
    input Boolean require_h_adsorptive
      "= true, if the specific enthalpy of the adsorptive is required"
      annotation (Dialog(tab="General",group="Inputs"));
    input Boolean require_s_adsorptive
      "= true, if the specific entropy of the adsorptive is required"
      annotation (Dialog(tab="General",group="Inputs"));
    input Boolean require_dh_adsorptive_dT_dp
      "= true, if the partial derivative of the specific enthalpy w.r.t. temperature
    at constant pressure is required"
      annotation (Dialog(tab="General",group="Inputs"));
    input Boolean require_h_adsorptiveToLiquid
      "= true, if specific enthalpy difference between adsorptive phase and saturated
    liquid phase is required"
      annotation (Dialog(tab="General",group="Inputs"));
    input Boolean adsorptiveAtDewPoint
      "= true, if adsorptive (gas/vapor phase) is assumed to be at dew point at
    T_adsorpt"
      annotation (Dialog(tab="General",group="Inputs"));

    //
    // Definition of outputs
    //
    output Modelica.Units.SI.SpecificVolume v
      "Specific volume"
      annotation (Dialog(tab="General", group="Outputs", enable=false));
    output SorpLib.Units.DerSpecificVolumeByPressure dv_dp_T
      "Partial derivative of the specific volume w.r.t. pressure at constant
      temperature"
      annotation (Dialog(tab="General", group="Outputs", enable=false));
    output SorpLib.Units.DerSpecificVolumeByTemperature dv_dT_p
      "Partial derivative of the specific volume w.r.t. temperature at constant
      pressure"
      annotation (Dialog(tab="General", group="Outputs", enable=false));
    output Modelica.Units.SI.SpecificEnthalpy h
      "Specific enthalpy"
      annotation (Dialog(tab="General", group="Outputs", enable=false));
    output Modelica.Units.SI.SpecificEntropy s
      "Specific enropy"
      annotation (Dialog(tab="General", group="Outputs", enable=false));
    output Modelica.Units.SI.SpecificHeatCapacity dh_dT_p
      "Partial derivative of the specific enthalpy w.r.t. temperature at constant
      pressure"
      annotation (Dialog(tab="General", group="Outputs", enable=false));
    output Modelica.Units.SI.SpecificEnthalpy h_atl
      "Specific enthalpy difference between adsorptive state and saturated liquid
    state (i.e., bubble point)"
      annotation (Dialog(tab="General", group="Outputs", enable=false));
    output SorpLib.Units.DerSpecificEnthalpyByPressure dh_atl_dp
      "Partial derivative of specific enthalpy difference between adsorptive state 
    and saturated liquid state (i.e., bubble point) w.r.t. pressure at constant
    temperature"
      annotation (Dialog(tab="General", group="Outputs", enable=false));
    output Modelica.Units.SI.SpecificHeatCapacity dh_atl_dT
      "Partial derivative of specific enthalpy difference between adsorptive state 
    and saturated liquid state (i.e., bubble point) w.r.t. temperature at constant
    pressure"
      annotation (Dialog(tab="General", group="Outputs", enable=false));

    //
    // Annotations
    //
    annotation (Documentation(revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>", info="<html>
<p>
This function calculates all properties of the adsorptive required for calculating
the specific heat capacity of the adsorpt or uptake-averaged properties of the adsorpt
and their partial derivatives. Thus, computational costs are reduced because redundant 
calculations of the same properties are avoided.
</p>
</html>"));
  end calc_properties;
  //
  // Annotations
  //
  annotation (Documentation(revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>", info="<html>
<p>
This partial package defines basic models and functions that are required to calculate all thermodynamic
properties of a pure component working pair. These basic models and functions are media-specific models
and functions, thus depending on the selected media model. Packages that inherit properties from this 
partial package must redeclare all partial models and functions. It may be that a partial model or function   
cannot be calculated with the selected media model. An example is the saturated vapor pressure as a function 
of temperature when the media model is an ideal gas. Such functions will not be accessed if the working pair 
is parameterized correctly. Therefore, these functions shall just return a dummy value when redeclaring.
</p>
</html>"));
end PartialPureMediumSpecificFunctions;
