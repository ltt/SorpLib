within SorpLib.Media.WorkingPairs.Records;
record PropertiesPureAdsorpt
  "This record contains required properties of the adsorpt for pure working pairs"
  extends Modelica.Icons.Record;

  //
  // Uptake-averaged state variables
  //
  SorpLib.Media.WorkingPairs.Records.StateVariables state
    "Thermodynamic state variables of the uptake-averaged adsorpt";

  //
  // State variables
  //
  Modelica.Units.SI.SpecificVolume v
    "Specific volume of the last adsorbed molecule";

  //
  // Additional properties
  //
  Modelica.Units.SI.SpecificEnthalpy h_ads
    "Specific enthalpy of adsorption of the last adsorbed molecule";

  Modelica.Units.SI.SpecificHeatCapacity cp
    "Specific heat capacity of the last adsorbed molecule or uptake-averaged 
    adsorpt, depending on the calculation approach";

  //
  // Partial derivatives
  //
  SorpLib.Media.WorkingPairs.Records.DerivativesPureIsothermModel derivatives_isotherm
    "Partial derivatives of the isotherm model";

  SorpLib.Units.DerUptakeSpecificVolumeByPressure dxv_avg_dp_T
    "Partial derivative of the uptake-averaged specific volume times the uptake 
    w.r.t. pressure at constant temperature";
  SorpLib.Units.DerUptakeSpecificVolumeByTemperature dxv_avg_dT_p
    "Partial derivative of the uptake-averaged specific volume times the uptake 
    w.r.t. temperature at constant pressure";

  SorpLib.Units.DerUptakeSpecificEnthalpyByPressure dxh_avg_dp_T
    "Partial derivative of the uptake-averaged specific enthalpy times the uptake 
    w.r.t. pressure at constant temperature";
  SorpLib.Units.DerUptakeSpecificEnthalpyByTemperature dxh_avg_dT_p
    "Partial derivative of the uptake-averaged specific enthalpy times the uptake 
    w.r.t. temperature at constant pressure";
  SorpLib.Units.DerUptakeSpecificEnthalpyByUptake dxh_avg_dx_T
    "Partial derivative of the uptake-averaged specific enthalpy times the uptake 
    w.r.t. uptake at constant temperature";
  SorpLib.Units.DerUptakeSpecificEnthalpyByTemperature dxh_avg_dT_x
    "Partial derivative of the uptake-averaged specific enthalpy times the uptake 
    w.r.t. temperature at constant uptake";

  SorpLib.Units.DerUptakeSpecificEntropyByPressure dxs_avg_dp_T
    "Partial derivative of the uptake-averaged specific entropy times the uptake 
    w.r.t. pressure at constant temperature";
  SorpLib.Units.DerUptakeSpecificEntropyByTemperature dxs_avg_dT_p
    "Partial derivative of the uptake-averaged specific entropy times the uptake 
    w.r.t. temperature at constant pressure";
  SorpLib.Units.DerUptakeSpecificEntropyByUptake dxs_avg_dx_T
    "Partial derivative of the uptake-averaged specific entropy times the uptake 
    w.r.t. uptake at constant temperature";
  SorpLib.Units.DerUptakeSpecificEntropyByTemperature dxs_avg_dT_x
    "Partial derivative of the uptake-averaged specific entropy times the uptake 
    w.r.t. temperature at constant uptake";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This record contains properties of the adsorption required for pure working pairs.
These properties are calculated at once if required, to keep the computational
costs low. Thus, e.g., multiple calculation of the same fluid property data is 
avoided.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end PropertiesPureAdsorpt;
