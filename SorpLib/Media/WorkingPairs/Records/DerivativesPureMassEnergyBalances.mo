within SorpLib.Media.WorkingPairs.Records;
record DerivativesPureMassEnergyBalances
  "This record contains partial derivatives required for mass and energy balances of a working pair volume"
  extends Modelica.Icons.Record;

  //
  // Definition of paramters
  //
  SorpLib.Units.DerSpecificVolumeByPressure dv_sorbent_dp_T
    "Partial derivative of the specific volume of the sorbent w.r.t. pressure at 
    constant temperature";
  SorpLib.Units.DerSpecificVolumeByTemperature dv_sorbent_dT_p
    "Partial derivative of the specific volume of the sorbent w.r.t. temperature 
    at constant pressure";
  SorpLib.Units.DerSpecificVolumeByUptake dv_sorbent_dx_T
    "Partial derivative of the specific volume of the sorbent w.r.t. uptake at 
    constant temperature";
  SorpLib.Units.DerSpecificVolumeByTemperature dv_sorbent_dT_x
    "Partial derivative of the specific volume of the sorbent w.r.t. temperature 
    at constant uptake";

  SorpLib.Units.DerSpecificEnthalpyByPressure dh_sorbent_dp_T
    "Partial derivative of the specific enthalpy of the sorbent w.r.t. pressure
    at constant temperature";
  Modelica.Units.SI.SpecificHeatCapacity dh_sorbent_dT_p
    "Partial derivative of the specific enthalpy of the sorbent w.r.t. temperature
    at constant pressure";
  SorpLib.Units.DerSpecificEnthalpyByUptake dh_sorbent_dx_T
    "Partial derivative of the specific enthalpy of the sorbent w.r.t. uptake
    at constant temperature";
  Modelica.Units.SI.SpecificHeatCapacity dh_sorbent_dT_x
    "Partial derivative of the specific enthalpy of the sorbent w.r.t. temperature
    at constant uptake";

  SorpLib.Units.DerUptakeSpecificVolumeByPressure dxv_avg_adsorpt_dp_T
    "Partial derivative of the uptake-averaged specific volume of the adsorpt  
    times the uptake w.r.t. pressure at constant temperature";
  SorpLib.Units.DerUptakeSpecificVolumeByTemperature dxv_avg_adsorpt_dT_p
    "Partial derivative of the uptake-averaged specific volume of the adsorpt
    times the uptake w.r.t. temperature at constant pressure";
  SorpLib.Units.DerUptakeSpecificVolumeByUptake dxv_avg_adsorpt_dx_T
    "Partial derivative of the uptake-averaged specific volume of the adsorpt
    times the uptake w.r.t. uptake at constant temperature";
  SorpLib.Units.DerUptakeSpecificVolumeByTemperature dxv_avg_adsorpt_dT_x
    "Partial derivative of the uptake-averaged specific volume of the adsorpt
    times the uptake w.r.t. temperature at constant uptake";

  SorpLib.Units.DerUptakeSpecificEnthalpyByPressure dxh_avg_adsorpt_dp_T
    "Partial derivative of the uptake-averaged specific enthalpy of the adsorpt
    times the uptake w.r.t. pressure at constant temperature";
  SorpLib.Units.DerUptakeSpecificEnthalpyByTemperature dxh_avg_adsorpt_dT_p
    "Partial derivative of the uptake-averaged specific enthalpy of the adsorpt 
    times the uptake w.r.t. temperature at constant pressure";
  SorpLib.Units.DerUptakeSpecificEnthalpyByUptake dxh_avg_adsorpt_dx_T
    "Partial derivative of the uptake-averaged specific enthalpy of the adsorpt
    times the uptake w.r.t. uptake at constant temperature";
  SorpLib.Units.DerUptakeSpecificEnthalpyByTemperature dxh_avg_adsorpt_dT_x
    "Partial derivative of the uptake-averaged specific enthalpy of the adsorpt
    times the uptake w.r.t. temperature at constant uptake";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This record contains partial derivatives required for the mass and energy balance 
of working pair volumes.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end DerivativesPureMassEnergyBalances;
