within SorpLib.Media.WorkingPairs.Records;
record DerivativesPureEntropyBalance
  "This record contains partial derivatives required for entropy balances of a working pair volume"
  extends Modelica.Icons.Record;

  //
  // Definition of paramters
  //
  SorpLib.Units.DerSpecificEntropyByPressure ds_sorbent_dp_T
    "Partial derivative of the specific entropy of the sorbent w.r.t. pressure
    at constant temperature";
  SorpLib.Units.DerSpecificEntropyByTemperature ds_sorbent_dT_p
    "Partial derivative of the specific entropy of the sorbent w.r.t. temperature
    at constant pressure";
  SorpLib.Units.DerSpecificEntropyByUptake ds_sorbent_dx_T
    "Partial derivative of the specific entropy of the sorbent w.r.t. uptake
    at constant temperature";
  SorpLib.Units.DerSpecificEntropyByTemperature ds_sorbent_dT_x
    "Partial derivative of the specific entropy of the sorbent w.r.t. temperature
    at constant uptake";

  SorpLib.Units.DerUptakeSpecificEntropyByPressure dxs_avg_adsorpt_dp_T
    "Partial derivative of the uptake-averaged specific entropy of the adsorpt
    times the uptake w.r.t. pressure at constant temperature";
  SorpLib.Units.DerUptakeSpecificEntropyByTemperature dxs_avg_adsorpt_dT_p
    "Partial derivative of the uptake-averaged specific entropy of the adsorpt 
    times the uptake w.r.t. temperature at constant pressure";
  SorpLib.Units.DerUptakeSpecificEntropyByUptake dxs_avg_adsorpt_dx_T
    "Partial derivative of the uptake-averaged specific entropy of the adsorpt 
    times the uptake w.r.t. uptake at constant temperature";
  SorpLib.Units.DerUptakeSpecificEntropyByTemperature dxs_avg_adsorpt_dT_x
    "Partial derivative of the uptake-averaged specific entropy of the adsorpt 
    times the uptake w.r.t. temperature at constant uptake";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This record contains partial derivatives required to calculate the entropy balance 
of working pair volumes.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end DerivativesPureEntropyBalance;
