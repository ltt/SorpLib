within SorpLib.Media.WorkingPairs.Records;
record DerivativesPureIsothermModel
  "This record contains partial derivatives of the isotherm model"
  extends Modelica.Icons.Record;

  //
  // Definition of paramters
  //
  SorpLib.Units.DerUptakeByPressure dx_dp_T
    "Partial derivative of the uptake w.r.t. pressure at constant temperature";
  SorpLib.Units.DerUptakeByTemperature dx_dT_p
    "Partial derivative of the uptake w.r.t. temperature at constant pressure";
  SorpLib.Units.DerPressureByUptake dp_dx_T
    "Partial derivative of the uptake w.r.t. uptake at constant temperature";
  Modelica.Units.SI.DerPressureByTemperature dp_dT_x
    "Partial derivative of the uptake w.r.t. temperature at constant uptake";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This record contains partial derivatives of the isotherm model. These partial 
derivatives may be required for the mass, energy, and entropy balance of working 
pair volumes.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end DerivativesPureIsothermModel;
