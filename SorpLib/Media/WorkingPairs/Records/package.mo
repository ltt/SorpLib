within SorpLib.Media.WorkingPairs;
package Records "Package containing records"
  extends Modelica.Icons.RecordsPackage;

  annotation (Documentation(info="<html>
<p>
This package contains definitions of records. These records are used to cluster 
variables and tidy up the model output.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Records;
