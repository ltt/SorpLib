within SorpLib.Media.WorkingPairs.Records;
record StateVariables
  "This record contains thermodynamic state variables"
  extends SorpLib.Media.Solids.Records.StateVariables;

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This record contains thermodynamic state variables.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end StateVariables;
