within SorpLib.Media.WorkingPairs.Records;
record PropertiesPureAdsorptive
  "This record contains required properties of the adsorptive in the gas/vapor phase for pure working pairs"
  extends Modelica.Icons.Record;

  //
  // State variables
  //
  SorpLib.Media.WorkingPairs.Records.StateVariables state
    "Thermodynamic state variables of the adsorptive";

  //
  // Additional properties of the one-phase regime
  //
  Modelica.Units.SI.SpecificHeatCapacity cp
    "Specific heat capacity";

  //
  // Additional properties of the two-phase regime
  //
  Modelica.Units.SI.Pressure p_sat
    "Saturated vapor pressure";

  Modelica.Units.SI.SpecificVolume v_satLiq
    "Specific volume at the bubble point at given temperature";

  Modelica.Units.SI.SpecificEnthalpy h_adsorptiveToLiquid
    "Specific enthalpy difference between adsorptive state and saturated liquid
    state (i.e., bubble point)";

  Modelica.Units.SI.SpecificHeatCapacity cp_satLiq
    "Specific heat capacaity at the bubble point at given temperature";

  //
  // Partial derivatives
  //
  SorpLib.Units.DerSpecificVolumeByPressure dv_dp_T
    "Partial derivative of the specific volume w.r.t. pressure at constant
    temperature";
  SorpLib.Units.DerSpecificVolumeByTemperature dv_dT_p
    "Partial derivative of the specific volume w.r.t. temperature at constant
    pressure";

  Modelica.Media.Common.DerPressureByTemperature dp_sat_dT
    "Partial derivative of saturated vapor pressure w.r.t. temperature";

  SorpLib.Units.DerSpecificVolumeByTemperature dv_satLiq_dT
    "Partial derivative of the specific volume at the bubble point at given
    temperature w.r.t. temperature";
  SorpLib.Units.DerSpecificVolumeByTemperatureTemperature ddv_satLiq_dT_dT
    "Second-order partial derivative of the specific volume at the bubble point 
    at given temperature w.r.t. temperature";

  SorpLib.Units.DerSpecificEnthalpyByPressure dh_adsorptiveToLiquid_dp_T
    "Partial derivative of specific enthalpy difference between adsorptive state 
    and saturated liquid state (i.e., bubble point) w.r.t. pressure at constant 
    temperature";
  Modelica.Units.SI.SpecificHeatCapacity dh_adsorptiveToLiquid_dT_p
    "Partial derivative of specific enthalpy difference between adsorptive state 
    and saturated liquid state (i.e., bubble point) w.r.t. temperature at constant
    pressure";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This record contains properties of the adsorptie in the gas/vapor phase required 
for pure working pairs. These properties are calculated at once if required, to 
keep the computational costs low. Thus, e.g., multiple calculation of the same 
fluid property data is avoided.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end PropertiesPureAdsorptive;
