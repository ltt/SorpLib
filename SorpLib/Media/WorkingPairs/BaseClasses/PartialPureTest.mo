within SorpLib.Media.WorkingPairs.BaseClasses;
partial model PartialPureTest
  "Base model for all testers of pure working pair models"
  extends Modelica.Icons.Example;

  //
  // Definition of parameters describing the test setup
  //
  parameter Modelica.Units.SI.Pressure p_adsorpt_start = 700
    "Start value of pressure"
    annotation (Dialog(tab="General", group="Test setup"),
                Evaluate=false,
                HideResult=false);
  parameter SorpLib.Units.Uptake x_adsorpt_start = 0.10
    "Start value of uptake"
    annotation (Dialog(tab="General", group="Test setup"),
                Evaluate=false,
                HideResult=false);
  parameter Modelica.Units.SI.Temperature T_adsorpt_start = 303.15
    "Start value of temperature"
    annotation (Dialog(tab="General", group="Test setup"),
                Evaluate=false,
                HideResult=false);

  parameter Real p_adsorpt_der(unit="Pa/s") = (0.4e5-700)/20
    "Prescriped sloped of pressure to test working pair model"
    annotation (Dialog(tab="General", group="Test setup"),
                Evaluate=false,
                HideResult=false);
  parameter Real x_adsorpt_der(unit="kg/(kg.s)") = 0.15/20
    "Prescriped sloped of uptake to test working pair model"
    annotation (Dialog(tab="General", group="Test setup"),
                Evaluate=false,
                HideResult=false);
  parameter Real T_adsorpt_der(unit="K/s") = 70/20
    "Prescriped sloped of temperature to working pair model"
    annotation (Dialog(tab="General", group="Test setup"),
                Evaluate=false,
                HideResult=false);

  parameter Choices.IndependentVariablesPureComponentWorkingPair stateVariables=
     SorpLib.Choices.IndependentVariablesPureComponentWorkingPair.xT
    "Independent state variables of the working pair model"
    annotation (Dialog(tab="General", group="Test setup"),
                Evaluate=true,
                HideResult=true);

  //
  // Definition of parameters describing packages and models
  //
  replaceable model PureWorkingPairModel =
      SorpLib.Media.WorkingPairs.PureComponents.CO2.ActivatedCarbon_Toth_DantasEtAl2011_Gas
    constrainedby
    SorpLib.Media.WorkingPairs.BaseClasses.PartialPureWorkingPairs(
      final stateVariables=stateVariables,
      final p_adsorpt=p_adsorpt,
      final x_adsorpt=x_adsorpt,
      final T_adsorpt=T_adsorpt,
      calcAdsorptAdsorbateState=false,
      approachSorptionEnthalpy=SorpLib.Choices.SorptionEnthalpy.ClausiusClapeyron,
      x_adsorpt_lb=1e-2)
    "Pure working pair model"
    annotation (Dialog(tab="General",group="Models and Media"),
                choicesAllMatching = true,
                Evaluate=true,
                HideResult=true);

  //
  // Instanziation of models
  //
  PureWorkingPairModel pureWorkingPair
    "Thermodynamic properties and partial derivatives of a pure working pair";

  //
  // Definition of variables
  //
  Modelica.Units.SI.Pressure p_adsorpt
    "Pressure";
  Modelica.Units.SI.Temperature T_adsorpt
    "Temperature";
  SorpLib.Units.Uptake x_adsorpt
    "Uptake";
  SorpLib.Units.MolarUptake x_adsorpt_m=
    x_adsorpt / pureWorkingPair.medium_adsorpt.M_adsorpt
    "Molar uptake";

initial equation
  //
  // Definition of start values
  //
  if stateVariables==
    SorpLib.Choices.IndependentVariablesPureComponentWorkingPair.pT then
    p_adsorpt = p_adsorpt_start
    "Pressure";
    T_adsorpt = T_adsorpt_start
      "Temperature";

  else
    x_adsorpt = x_adsorpt_start
      "Uptake";
    T_adsorpt = T_adsorpt_start
      "Temperature";

  end if;

equation
  //
  // Definition of slopes
  //
  if stateVariables==
    SorpLib.Choices.IndependentVariablesPureComponentWorkingPair.pT then
    der(p_adsorpt) = p_adsorpt_der
      "Predecsriped slope of p_adsorpt";
    der(T_adsorpt) = T_adsorpt_der
      "Predescriped slope of T_adsorpt";
  else
    der(x_adsorpt) = x_adsorpt_der
      "Predecsriped slope of x_adsorpt";
    der(T_adsorpt) = T_adsorpt_der
      "Predescriped slope of T_adsorpt";

  end if;

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial model is the basic model for all testers of working pair models describing
adsorption of pure components.
<br/><br/>
Models that inherit properties from this partial model have to specify the working pair
model <i>PureWorkingPairModel</i> and the test setup.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end PartialPureTest;
