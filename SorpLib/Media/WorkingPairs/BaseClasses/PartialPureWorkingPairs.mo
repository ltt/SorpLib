within SorpLib.Media.WorkingPairs.BaseClasses;
partial model PartialPureWorkingPairs
  "Base model for pure component working pairs"
  extends Modelica.Icons.MaterialProperty;

  //
  // Definition of parameters regarding the working pair and medium
  //
  replaceable package WorkingPair =
    SorpLib.Media.WorkingPairs.Interfaces.PartialPureParametrization
    constrainedby
    SorpLib.Media.WorkingPairs.Interfaces.PartialPureParametrization(
      redeclare final model Sorbent = Sorbent)
    "Parametrized working pair model"
    annotation (Dialog(tab = "General", group = "Working Pair and Medium"),
                choicesAllMatching = true,
                Evaluate=true,
                HideResult=true);

  replaceable model Sorbent =
    SorpLib.Media.Solids.Sorbents.RDSilicaGel
    constrainedby SorpLib.Media.Solids.BaseClasses.PartialSolid(
      final calcCaloricProperties=calcCaloricProperties,
      final calcEntropicProperties=calcEntropicProperties,
      final p=p_adsorpt_,
      final T=T_adsorpt,
      final approach_v=SorpLib.Choices.SpecificVolumeSolid.Constant,
      final p_ref=p_sorbent_ref,
      final T_ref=T_sorbent_ref,
      final h_ref=h_sorbent_ref,
      final s_ref=s_sorbent_ref,
      final tolerance_int_h=tolerance_sorbent_int_h,
      final tolerance_int_s=tolerance_sorbent_int_s)
    "Calculates the thermodynamic properties of the sorbent"
    annotation (Dialog(tab = "General", group = "Working Pair and Medium"),
                choicesAllMatching = true,
                Evaluate=true,
                HideResult=true);

  //
  // Definition of parameters regarding the calculation approach
  //
  parameter SorpLib.Choices.IndependentVariablesPureComponentWorkingPair
    stateVariables=SorpLib.Choices.IndependentVariablesPureComponentWorkingPair.xT
    "Independent state variables of the working pair model"
    annotation (Dialog(tab="General", group="Calculation Setup"),
                choicesAllMatching=true,
                Evaluate=true,
                HideResult=true);

  parameter Boolean calcCaloricProperties = true
    "= true, if caloric properties are calculated (i.e., h, u, h_ads, and cp)"
    annotation (Dialog(tab = "General", group = "Calculation Setup"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);
  parameter Boolean calcEntropicProperties = true
    "= true, if caloric properties are calculated (i.e., s, g, a, and s_ads)"
    annotation (Dialog(tab = "General", group = "Calculation Setup",
                enable = calcCaloricProperties),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);

  parameter Boolean calcAdsorptAdsorbateState = false
    "= true, if state properties of average adsorpt phase and adosrbate phase are
    calculated (i.e., requires numerical integration)"
    annotation (Dialog(tab = "General", group = "Calculation Setup",
                enable = calcCaloricProperties),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);

  parameter Boolean calcDerivativesIsotherm = true
    "= true, if partial derivatives of isotherms required for mass, energy, and
    entropy balance of working paur volumes are calculated"
    annotation (Dialog(tab = "General", group = "Calculation Setup"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);
  parameter Boolean calcDerivativesMassEnergyBalance = true
    "= true, if partial derivatives required for mass and energy balance of
    working pair volumes are calculated"
    annotation (Dialog(tab = "General", group = "Calculation Setup",
                enable = calcDerivativesIsotherm and calcCaloricProperties),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);
  parameter Boolean calcDerivativesEntropyBalance = true
    "= true, if partial derivatives required for entropy balance of working
    pair volumes are calculated"
    annotation (Dialog(tab = "General", group = "Calculation Setup",
                enable = calcDerivativesIsotherm and
                  calcDerivativesMassEnergyBalance and calcCaloricProperties),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);

  parameter Boolean adsorptiveAtDewPoint = false
    "= true, if adsorptive (gas/vapor phase) is assumed to be at dew point at
    T_adsorpt"
    annotation (Dialog(tab = "General", group = "Calculation Setup",
                enable = WorkingPair.twoPhaseAdsorptive),
                Evaluate=true,
                choices(checkBox=true));

  //
  // Definition of parameters regarding sorption enthalpy
  //
  parameter SorpLib.Choices.SorptionEnthalpy approachSorptionEnthalpy=
    SorpLib.Choices.SorptionEnthalpy.Constant
    "Calculation approach for the sorption enthalpy"
    annotation (Dialog(tab="Sorption Enthalpy", group="General"),
                Evaluate=false,
                choicesAllMatching=true);

  parameter Modelica.Units.SI.SpecificEnthalpy h_ads_constant= 2750e3
    "Constant specific enthalpy of adsorption"
    annotation (Dialog(tab="Sorption Enthalpy", group="Constant",
                enable=(approachSorptionEnthalpy ==
                  SorpLib.Choices.SorptionEnthalpy.Constant)),
                HideResult=true,
                Evaluate= false);

  //
  // Definition of parameters regarding adsorpt phase's specific heat capacity
  //
  parameter SorpLib.Choices.SpecificHeatCapacityAdsorpt
    approachSpecificHeatCapacity=
    SorpLib.Choices.SpecificHeatCapacityAdsorpt.Constant
    "Calculation approach for the specific heat capacity of the adsorpt"
    annotation (Dialog(tab="Specific Heat Capacity", group="General"),
                Evaluate=false,
                choicesAllMatching=true);

  parameter Modelica.Units.SI.SpecificHeatCapacity cp_adsorpt_constant= 4e3
    "Constant specific heat capacity of the adsorpt"
    annotation (Dialog(tab="Specific Heat Capacity", group="Constant",
                enable=(approachSpecificHeatCapacity ==
                  SorpLib.Choices.SpecificVolumeAdsorpt.Constant)),
                HideResult=true,
                Evaluate= false);

  parameter SorpLib.Choices.GeneralizedFunctionApproach cp_adsorpt_function=
      SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionTemperature
    "Generalized function used to calculate the specific heat capacity of the adsorpt"
    annotation (Dialog(tab="Specific Heat Capacity",
                group="Generalized Function",
                enable=(approachSpecificHeatCapacity==
                  SorpLib.Choices.SpecificVolumeAdsorpt.GeneralizedFunction)),
                Evaluate=true);
  parameter Modelica.Units.SI.Temperature T_ref_cp_adsorpt = 293.15
    "Reference temperature for the specific heat capacity of the adsorpt"
    annotation (Dialog(tab="Specific Heat Capacity", group="Generalized Function",
                enable=(approachSpecificHeatCapacity==
                  SorpLib.Choices.SpecificVolumeAdsorpt.GeneralizedFunction)),
                HideResult=true,
                Evaluate= true);
  parameter Modelica.Units.SI.SpecificHeatCapacity cp_adsorpt_ref = 1
    "Reference fluid property data for the specific heat capacity of the adsorpt"
    annotation (Dialog(tab="Specific Heat Capacity", group="Generalized Function",
                enable=(approachSpecificHeatCapacity==
                  SorpLib.Choices.SpecificVolumeAdsorpt.GeneralizedFunction)),
                HideResult=true,
                Evaluate= true);
  parameter Real coefficients_cp_adsorpt[:]={cp_adsorpt_constant}
    "Coefficients of generalized function for the specific heat capacity of the adsorpt"
    annotation (Dialog(tab="Specific Heat Capacity", group="Generalized Function",
                enable=(approachSpecificHeatCapacity==
                  SorpLib.Choices.SpecificVolumeAdsorpt.GeneralizedFunction)),
                HideResult=true,
                Evaluate= true);
  parameter Real exponents_cp_adsorpt[size(coefficients_cp_adsorpt,1)]={0}
    "Exponents of generalized function for the specific heat capacity of the adsorpt"
    annotation (Dialog(tab="Specific Heat Capacity", group="Generalized Function",
                enable=(approachSpecificHeatCapacity==
                  SorpLib.Choices.SpecificVolumeAdsorpt.GeneralizedFunction)),
                HideResult=true,
                Evaluate= true);

  parameter SorpLib.Choices.InterpolationApproach cp_adsorpt_interpolation=
      SorpLib.Choices.InterpolationApproach.Linear
    "Interpolation approach used to calculate the specific heat capacity of the adsorpt"
    annotation (Dialog(tab="Specific Heat Capacity",
                group="Interpolation",
                enable=(approachSpecificHeatCapacity==
                  SorpLib.Choices.SpecificVolumeAdsorpt.Interpolation)),
                Evaluate=false);
  parameter Real abscissa_cp_adsorpt[:]={0, 1000}
    "Known abscissa values for the specific heat capacity of the adsorpt"
    annotation (Dialog(tab="Specific Heat Capacity", group="Interpolation",
                enable=(approachSpecificHeatCapacity==
                  SorpLib.Choices.SpecificVolumeAdsorpt.Interpolation)),
                HideResult=true,
                Evaluate= true);
  parameter Real ordinate_cp_adsorpt[size(abscissa_cp_adsorpt,1)]=
    {cp_adsorpt_constant, cp_adsorpt_constant}
    "Known ordinate values for the specific heat capacity of the adsorpt"
    annotation (Dialog(tab="Specific Heat Capacity", group="Interpolation",
                enable=(approachSpecificHeatCapacity==
                  SorpLib.Choices.SpecificHeatCapacityAdsorpt.Interpolation)),
                HideResult=true,
                Evaluate= true);
  final parameter Real coefficients_cubicSplines_cp_adsorpt[size(abscissa_cp_adsorpt,1),4]=
    SorpLib.Media.Functions.Utilities.calcCubicSplineCoefficients(
    abscissa=abscissa_cp_adsorpt,
    ordinate=ordinate_cp_adsorpt)
    "Coefficient a to d for cubic polynomials for the specific heat capacity of the adsorpt"
    annotation (Dialog(tab="Specific Heat Capacity", group="Interpolation",
                enable=false),
                HideResult=true,
                Evaluate= true);

  //
  // Definition of parameters regarding adsorpt phase's specific volume
  //
  parameter Boolean neglectSpecifcVolume = false
    "= true, if specific volume of the adsorpt is neglected (i.e., v_adsorpt = 0
    -> u_adsorpt = h_adsorpt and g_adsorpt = a_adsorpt)"
    annotation (Dialog(tab = "Specific Volume", group = "General"),
                choices(checkBox=true),
                Evaluate= false);
  parameter SorpLib.Choices.SpecificVolumeAdsorpt approachSpecificVolume=
      SorpLib.Choices.SpecificVolumeAdsorpt.Constant
    "Calculation approach for the specific volume of the adsorpt"
    annotation (Dialog(tab="Specific Volume", group="General",
                enable=not neglectSpecifcVolume),
                Evaluate=false,
                choicesAllMatching=true);

  parameter Modelica.Units.SI.SpecificVolume v_adsorpt_constant= 1e-3
    "Constant specific volume of the adsorpt"
    annotation (Dialog(tab="Specific Volume", group="Constant",
                enable=(not neglectSpecifcVolume and approachSpecificVolume==
                  SorpLib.Choices.SpecificVolumeAdsorpt.Constant)),
                HideResult=true,
                Evaluate= false);

  parameter SorpLib.Choices.GeneralizedFunctionApproach v_adsorpt_function=
      SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionTemperature
    "Generalized function used to calculate the specific volume of the adsorpt"
    annotation (Dialog(tab="Specific Volume", group="Generalized Function",
                enable=(not neglectSpecifcVolume and approachSpecificVolume==
                  SorpLib.Choices.SpecificVolumeAdsorpt.GeneralizedFunction)),
                Evaluate=true);
  parameter Modelica.Units.SI.Temperature T_ref_v_adsorpt = 293.15
    "Reference temperature for the specific volume of the adsorpt"
    annotation (Dialog(tab="Specific Volume", group="Generalized Function",
                enable=(not neglectSpecifcVolume and approachSpecificVolume==
                  SorpLib.Choices.SpecificVolumeAdsorpt.GeneralizedFunction)),
                HideResult=true,
                Evaluate= true);
  parameter Modelica.Units.SI.SpecificVolume v_adsorpt_ref = 1
    "Reference fluid property data for the specific volume of the adsorpt"
    annotation (Dialog(tab="Specific Volume", group="Generalized Function",
                enable=(not neglectSpecifcVolume and approachSpecificVolume==
                  SorpLib.Choices.SpecificVolumeAdsorpt.GeneralizedFunction)),
                HideResult=true,
                Evaluate= true);
  parameter Real coefficients_v_adsorpt[:]={v_adsorpt_constant}
    "Coefficients of generalized function for the specific volume of the adsorpt"
    annotation (Dialog(tab="Specific Volume", group="Generalized Function",
                enable=(not neglectSpecifcVolume and approachSpecificVolume ==
                  SorpLib.Choices.SpecificVolumeAdsorpt.GeneralizedFunction)),
                HideResult=true,
                Evaluate= true);
  parameter Real exponents_v_adsorpt[size(coefficients_v_adsorpt,1)]={0}
    "Exponents of generalized function for the specific volume of the adsorpt"
    annotation (Dialog(tab="Specific Volume", group="Generalized Function",
                enable=(not neglectSpecifcVolume and approachSpecificVolume ==
                  SorpLib.Choices.SpecificVolumeAdsorpt.GeneralizedFunction)),
                HideResult=true,
                Evaluate= true);

  parameter SorpLib.Choices.InterpolationApproach v_adsorpt_interpolation=
      SorpLib.Choices.InterpolationApproach.Linear
    "Interpolation approach used to calculate the specific volume of the adsorpt"
    annotation (Dialog(tab="Specific Volume",group="Interpolation",
                enable=(not neglectSpecifcVolume and approachSpecificVolume==
                  SorpLib.Choices.SpecificVolumeAdsorpt.Interpolation)),
                Evaluate=false);
  parameter Real abscissa_v_adsorpt[:]={0, 1000}
    "Known abscissa values for the specific volume of the adsorpt"
    annotation (Dialog(tab="Specific Volume", group="Interpolation",
                enable=(not neglectSpecifcVolume and approachSpecificVolume ==
                  SorpLib.Choices.SpecificVolumeAdsorpt.Interpolation)),
                HideResult=true,
                Evaluate= true);
  parameter Real ordinate_v_adsorpt[size(abscissa_v_adsorpt,1)]=
    {v_adsorpt_constant, v_adsorpt_constant}
    "Known ordinate values for the specific volume of the adsorpt"
    annotation (Dialog(tab="Specific Volume", group="Interpolation",
                enable=(not neglectSpecifcVolume and approachSpecificVolume ==
                  SorpLib.Choices.SpecificVolumeAdsorpt.Interpolation)),
                HideResult=true,
                Evaluate= true);
  final parameter Real coefficients_cubicSplines_v_adsorpt[size(abscissa_v_adsorpt,1),4]=
    SorpLib.Media.Functions.Utilities.calcCubicSplineCoefficients(
    abscissa=abscissa_v_adsorpt,
    ordinate=ordinate_v_adsorpt)
    "Coefficient a to d for cubic polynomials for the specific volume of the adsorpt"
    annotation (Dialog(tab="Specific Volume", group="Interpolation",
                enable=false),
                HideResult=true,
                Evaluate= true);

  //
  // Definition of parameters regarding advanced options
  //
  parameter Boolean avoidEvents = false
    "= true, if events are avoided using the noEvent()-operator"
    annotation (Dialog(tab = "Advanced", group = "General"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);

  parameter Boolean limitLowerPressure = false
    "= true, if pressure p_adsorpt is limited to p_adsorpt_min"
    annotation (Dialog(tab = "Advanced", group = "Limiter"),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);
  parameter Boolean limitLowerPressureAdsorptive = false
    "= true, if pressure p_adsorpt is limited to p_adsorpt_min only for property
    calculation (i.e., properties of the adsorptive)"
    annotation (Dialog(tab = "Advanced", group = "Limiter",
                enable=not limitLowerPressure),
                choices(checkBox=true),
                Evaluate=true,
                HideResult=true);
  parameter Modelica.Units.SI.Pressure p_adsorpt_min = 615
    "Lower limit for the pressure p_adsorpt"
    annotation (Dialog(tab = "Advanced", group = "Limiter",
                enable=limitLowerPressure or limitLowerPressureAdsorptive),
                Evaluate=true,
                HideResult=true);

  parameter Modelica.Units.SI.Pressure p_adsorpt_lb_start = 1e3
    "Lower bound if function p_adsorpt(x_adsorpt, T_adsorpt) is calculated numerically"
    annotation (Dialog(tab="Advanced", group="Numerics - Inverses"),
                Evaluate=true,
                HideResult=true);
  parameter Modelica.Units.SI.Pressure p_adsorpt_ub_start = 1e4
    "Upper bound if function p_adsorpt(x_adsorpt, T_adsorpt) is calculated numerically"
    annotation (Dialog(tab="Advanced", group="Numerics - Inverses"),
                Evaluate=true,
                HideResult=true);
  parameter Real tolerance_p_adsorpt = 100*Modelica.Constants.eps
    "Tolerance if function p_adsorpt(x_adsorpt, T_adsorpt) is calculated numerically"
    annotation (Dialog(tab="Advanced", group="Numerics - Inverses"),
                Evaluate=true,
                HideResult=true);

  parameter Modelica.Units.SI.PressureDifference dp = 1e-3
    "Pressure difference used when calculating partial derivatives numerically"
    annotation (Dialog(tab="Advanced", group="Numerics - Derivatives"),
                Evaluate=true,
                HideResult=true);
  parameter Modelica.Units.SI.TemperatureDifference dT = 1e-3
    "Temperature difference used when calculating partial derivatives numerically"
    annotation (Dialog(tab="Advanced", group="Numerics - Derivatives"),
                Evaluate=true,
                HideResult=true);
  parameter SorpLib.Units.Uptake dx = 1e-3
    "Uptake difference used when calculating partial derivatives numerically"
    annotation (Dialog(tab="Advanced", group="Numerics - Derivatives"),
                Evaluate=true,
                HideResult=true);

  parameter Real tolerance_sorbent_int_h = 1e-2
    "Integration tolerance when calculating the specific enthalpy of the sorbent
    numerically"
    annotation (Dialog(tab="Advanced", group="Numerics - Integrals",
                enable=calcCaloricProperties),
                Evaluate=true,
                HideResult=true);
  parameter Real tolerance_sorbent_int_s = 1e-2
    "Integration tolerance when calculating the specific entropy of the sorbent
    numerically"
    annotation (Dialog(tab="Advanced", group="Numerics - Integrals",
                enable=calcEntropicProperties),
                Evaluate=true,
                HideResult=true);
  parameter Real tolerance_adsorpt_int_h = 1e-2
    "Integration tolerance when calculating the specific average enthalpy of the
    adsorpt numerically"
    annotation (Dialog(tab="Advanced", group="Numerics - Integrals",
                enable=calcAdsorptAdsorbateState and calcCaloricProperties),
                Evaluate=true,
                HideResult=true);
  parameter Real tolerance_adsorpt_int_s = 1e-2
    "Integration tolerance when calculating the specific average entropy of the
    adsorpt numerically"
    annotation (Dialog(tab="Advanced", group="Numerics - Integrals",
                enable=calcAdsorptAdsorbateState and calcEntropicProperties),
                Evaluate=true,
                HideResult=true);
  parameter Real tolerance_adsorpt_int_cp = 1e-2
    "Integration tolerance when calculating the specific heat capacity of the
    adsorpt numerically"
    annotation (Dialog(tab="Advanced", group="Numerics - Integrals",
                enable=calcCaloricProperties and (
                  approachSpecificHeatCapacity==
                  SorpLib.Choices.SpecificHeatCapacityAdsorpt.WaltonLeVan or
                  approachSpecificHeatCapacity==
                  SorpLib.Choices.SpecificHeatCapacityAdsorpt.SchwambergerSchmidt)),
                Evaluate=true,
                HideResult=true);
  parameter SorpLib.Units.Uptake x_adsorpt_lb = 0
    "Lower limit for integral when calculating the specific heat capacity of the
    adsorpt or uptake-averaged porperties of the adsorpt numerically (i.e., 
    should be zero)"
    annotation (Dialog(tab="Advanced", group="Numerics - Integrals",
                enable=calcCaloricProperties and (
                  calcAdsorptAdsorbateState or
                  approachSpecificHeatCapacity==
                  SorpLib.Choices.SpecificHeatCapacityAdsorpt.WaltonLeVan or
                  approachSpecificHeatCapacity==
                  SorpLib.Choices.SpecificHeatCapacityAdsorpt.SchwambergerSchmidt)),
                Evaluate=true,
                HideResult=true);

  parameter Modelica.Units.SI.Pressure p_sorbent_ref = 1e5
    "Reference pressure for caloric and entropic calculations (i.e., per mass 
    sorbent)"
    annotation (Dialog(tab="Advanced", group="Reference State - Sorbent",
                enable=calcCaloricProperties or calcEntropicProperties),
                Evaluate=true);
  parameter Modelica.Units.SI.Temperature T_sorbent_ref =  298.15
    "Reference temperature for caloric and entropic calculations (i.e., per mass 
    sorbent)"
    annotation (Dialog(tab="Advanced", group="Reference State - Sorbent",
                enable=calcCaloricProperties or calcEntropicProperties),
                Evaluate=true);
  parameter Modelica.Units.SI.SpecificEnthalpy h_sorbent_ref=
    WorkingPair.MediumSpecificFunctions.h_pT(
      p=p_sorbent_ref,
      T=T_sorbent_ref,
      p_lb=p_adsorpt_min,
      h_ref=0,
      p_ref=p_sorbent_ref,
      T_ref=T_sorbent_ref)
    "Specific enthalpy of adsorptive at reference state (i.e., per mass sorbent)"
    annotation (Dialog(tab="Advanced", group="Reference State - Sorbent",
                enable=calcCaloricProperties or calcEntropicProperties),
                Evaluate=true);
  parameter Modelica.Units.SI.SpecificEntropy s_sorbent_ref=
    WorkingPair.MediumSpecificFunctions.s_pT(
      p=p_sorbent_ref,
      T=T_sorbent_ref,
      p_lb=p_adsorpt_min,
      s_ref=0,
      p_ref=p_sorbent_ref,
      T_ref=T_sorbent_ref)
    "Specific entropy of adsorptive at reference state (i.e., per mass sorbent)"
    annotation (Dialog(tab="Advanced", group="Reference State - Sorbent",
                enable=calcEntropicProperties),
                Evaluate=true);

  //
  // Definition of inputs
  //
  input SorpLib.Units.Uptake x_adsorpt
    "Equilibrium uptake"
    annotation (Dialog(tab = "General", group = "Inputs",
                enable=false));
  input Modelica.Units.SI.Temperature T_adsorpt
    "Equilibrium temperature"
    annotation (Dialog(tab = "General", group = "Inputs",
                enable=false));

  //
  // Definition of outputs
  //
  output Modelica.Units.SI.Pressure p_adsorpt
    "Equilibrium pressure"
    annotation (Dialog(tab = "General", group = "Outputs", enable = false));

  output SorpLib.Media.WorkingPairs.Records.StateVariables state_sorbent
    "State properties of the sorbent (i.e., per sorbent mass)"
    annotation (Dialog(tab = "General", group = "Outputs", enable = false));
  output SorpLib.Media.WorkingPairs.Records.StateVariables state_lastAdsorbedMolecule
    "State properties of the last adsorbed molecule (i.e., per adsorptive/adsorpt 
    mass)"
    annotation (Dialog(tab = "General", group = "Outputs", enable = false));
  output SorpLib.Media.WorkingPairs.Records.StateVariables state_averageAdsorpt
    "State properties of the uptake-averaged adsorpt (i.e., per adsorptive/
    adsorpt mass)"
    annotation (Dialog(tab = "General", group = "Outputs", enable = false));
  output SorpLib.Media.WorkingPairs.Records.StateVariables state_averageAdsorbate
    "State properties of the adsorbate (i.e., per sorbent mass)"
    annotation (Dialog(tab = "General", group = "Outputs", enable = false));
  output SorpLib.Media.WorkingPairs.Records.StateVariables state_adsorptive
    "State properties of the vapor/gas (i.e., per adsorptive/adsorpt mass)"
    annotation (Dialog(tab = "General", group = "Outputs", enable = false));

  output SorpLib.Media.WorkingPairs.Records.DerivativesPureIsothermModel
    derivatives_isotherm "Partial derivatives of the isotherm model"
    annotation (Dialog(
      tab="General",
      group="Outputs",
      enable=false));
  output SorpLib.Media.WorkingPairs.Records.DerivativesPureMassEnergyBalances
    derivatives_massEnergy
    "Partial derivatives regarding mass and energy balances"
    annotation (Dialog(
      tab="General",
      group="Outputs",
      enable=false));
  output SorpLib.Media.WorkingPairs.Records.DerivativesPureEntropyBalance
    derivatives_entropy "Partial derivatives regarding entropy balances"
    annotation (Dialog(
      tab="General",
      group="Outputs",
      enable=false));

  //
  // Definition of variables
  //
  Modelica.Units.SI.SpecificEnthalpy h_ads(displayUnit="kJ/kg")
    "Specific enthalpy of adsorption";
  Modelica.Units.SI.SpecificEntropy s_ads(displayUnit="kJ/(kg.K)")
    "Specific entropy of adsorption";

  Modelica.Units.SI.SpecificHeatCapacity cp_adsorpt
    "Specific heat capacity of the adsorpt";

  //
  // Instantiation of models
  //
  WorkingPair.Sorbent medium_sorbent
    "State properties of the sorbent";

  WorkingPair.AdsorptProperties medium_adsorpt(
    final stateVariables=stateVariables,
    final calcCaloricProperties=calcCaloricProperties,
    final calcEntropicProperties=calcEntropicProperties,
    final calcAdsorptAdsorbateState=calcAdsorptAdsorbateState,
    final calcDerivativesIsotherm=calcDerivativesIsotherm,
    final calcDerivativesMassEnergyBalance=calcDerivativesMassEnergyBalance,
    final calcDerivativesEntropyBalance=calcDerivativesEntropyBalance,
    final adsorptiveAtDewPoint=adsorptiveAtDewPoint,
    final c=isothermCoefficients.c,
    final dc_dT_adsorpt=isothermCoefficients.dc_dT,
    final ddc_dT_adsorpt_dT_adsorpt=isothermCoefficients.ddc_dT_dT,
    final p_adsorpt=p_adsorpt_,
    final T_adsorpt=T_adsorpt,
    final x_adsorpt=x_adsorpt,
    final properties_adsorptive=medium_adsorptive.properties,
    final approachSorptionEnthalpy=approachSorptionEnthalpy,
    final h_ads_constant=h_ads_constant,
    final approachSpecificHeatCapacity = approachSpecificHeatCapacity,
    final cp_adsorpt_constant = cp_adsorpt_constant,
    final cp_adsorpt_function = cp_adsorpt_function,
    final T_ref_cp_adsorpt = T_ref_cp_adsorpt,
    final cp_adsorpt_ref = cp_adsorpt_ref,
    final coefficients_cp_adsorpt = coefficients_cp_adsorpt,
    final exponents_cp_adsorpt = exponents_cp_adsorpt,
    final cp_adsorpt_interpolation = cp_adsorpt_interpolation,
    final abscissa_cp_adsorpt = abscissa_cp_adsorpt,
    final ordinate_cp_adsorpt = ordinate_cp_adsorpt,
    final coefficients_cubicSplines_cp_adsorpt = coefficients_cubicSplines_cp_adsorpt,
    final neglectSpecifcVolume=neglectSpecifcVolume,
    final approachSpecificVolume=approachSpecificVolume,
    final v_adsorpt_constant=v_adsorpt_constant,
    final v_adsorpt_function=v_adsorpt_function,
    final T_ref_v_adsorpt=T_ref_v_adsorpt,
    final v_adsorpt_ref=v_adsorpt_ref,
    final coefficients_v_adsorpt=coefficients_v_adsorpt,
    final exponents_v_adsorpt=exponents_v_adsorpt,
    final v_adsorpt_interpolation=v_adsorpt_interpolation,
    final abscissa_v_adsorpt=abscissa_v_adsorpt,
    final ordinate_v_adsorpt=ordinate_v_adsorpt,
    final coefficients_cubicSplines_v_adsorpt=coefficients_cubicSplines_v_adsorpt,
    final p_adsorpt_min=p_adsorpt_min,
    final dp=dp,
    final dT=dT,
    final tolerance_adsorpt_int_h=tolerance_adsorpt_int_h,
    final tolerance_adsorpt_int_s=tolerance_adsorpt_int_s,
    final tolerance_adsorpt_int_cp=tolerance_adsorpt_int_cp,
    final x_adsorpt_lb=x_adsorpt_lb)
    "Properties of the adsorptive required for calculation: These properties are
    calculated at once to save computational costs";

  WorkingPair.MediumSpecificFunctions.AdsorptiveProperties medium_adsorptive(
    final calcCaloricProperties=calcCaloricProperties,
    final calcEntropicProperties=calcEntropicProperties,
    final calcAdsorptAdsorbateState=calcAdsorptAdsorbateState,
    final calcDerivativesIsotherm=calcDerivativesIsotherm,
    final calcDerivativesMassEnergyBalance=calcDerivativesMassEnergyBalance,
    final calcDerivativesEntropyBalance=calcDerivativesEntropyBalance,
    final adsorptiveAtDewPoint=adsorptiveAtDewPoint,
    final neglectSpecifcVolume=neglectSpecifcVolume,
    final approachSpecificVolume=approachSpecificVolume,
    final approachSpecificHeatCapacity=approachSpecificHeatCapacity,
    final approachSorptionEnthalpy=approachSorptionEnthalpy,
    final dp=dp,
    final dT=dT,
    final p=if limitLowerPressure then p_adsorpt_ elseif
      limitLowerPressureAdsorptive then max(p_adsorpt_min, p_adsorpt_) else
      p_adsorpt_,
    final T=T_adsorpt)
    "Properties of the adsorptive required for calculation: These properties are
    calculated at once to save computational costs";

  WorkingPair.IsothermCoefficients isothermCoefficients(
    final T_adsorpt=T_adsorpt,
    final dT=dT)
    "Temperature-dependent isotherm coefficients";

  //
  // Booleans indicating which variables msut be calculated
  //
protected
  parameter Boolean require_derivativesMassEnergyBalances_pT=
    (calcCaloricProperties and calcDerivativesIsotherm and
    calcDerivativesMassEnergyBalance)
    "= true, if partial derivatives of mass and energy balances w.r.t. pressure 
    and temperature are required";
  parameter Boolean require_derivativesMassEnergyBalances_xT=
    (require_derivativesMassEnergyBalances_pT and stateVariables==
    SorpLib.Choices.IndependentVariablesPureComponentWorkingPair.xT)
    "= true, if partial derivatives of mass and energy balances w.r.t. uptake 
    and temperature are required";

  parameter Boolean require_derivativesEntropyBalances_pT=
    (require_derivativesMassEnergyBalances_pT and calcDerivativesEntropyBalance)
    "= true, if partial derivatives of the entropy balance w.r.t. pressure and
    temperature are required";
  parameter Boolean require_derivativesEntropyBalances_xT=
    (require_derivativesEntropyBalances_pT and stateVariables==
    SorpLib.Choices.IndependentVariablesPureComponentWorkingPair.xT)
    "= true, if partial derivatives of the entropy balance w.r.t. uptake and
    temperature are required";

  //
  // Auxillary variables
  //
  Modelica.Units.SI.Pressure p_adsorpt_
    "Equilibrium pressure that might be limited";

equation
  //
  // Pass state properties of the sorbent (per sorbent mass)
  //
  state_sorbent = medium_sorbent.state_variables
    "State properties of the sorbent (i.e., per sorbent mass)";

  //
  // Calculate thermal state properties of the last adsorbed molecule
  // (per adsorptive/adsorpt mass)
  //
  if stateVariables ==
    SorpLib.Choices.IndependentVariablesPureComponentWorkingPair.xT then
    p_adsorpt_ = if limitLowerPressure then
      max(p_adsorpt_min, WorkingPair.IsothermModel.p_xT(
        x_adsorpt=x_adsorpt,
        T_adsorpt=T_adsorpt,
        c=isothermCoefficients.c,
        p_adsorpt_lb_start=p_adsorpt_lb_start,
        p_adsorpt_ub_start=p_adsorpt_ub_start,
        tolerance=tolerance_p_adsorpt)) else
      WorkingPair.IsothermModel.p_xT(
        x_adsorpt=x_adsorpt,
        T_adsorpt=T_adsorpt,
        c=isothermCoefficients.c,
        p_adsorpt_lb_start=p_adsorpt_lb_start,
        p_adsorpt_ub_start=p_adsorpt_ub_start,
        tolerance=tolerance_p_adsorpt)
      "Equilibrium pressure that might be limited";

    p_adsorpt = p_adsorpt_
      "Equilibrium pressure";

  else
    p_adsorpt_ = if limitLowerPressure
      then max(p_adsorpt_min, p_adsorpt) else p_adsorpt
      "Equilibrium pressure that might be limited";

    x_adsorpt = WorkingPair.IsothermModel.x_pT(
      p_adsorpt= p_adsorpt_,
      T_adsorpt=T_adsorpt,
      c=isothermCoefficients.c,
      p_adsorpt_lb_start=p_adsorpt_lb_start,
      p_adsorpt_ub_start=p_adsorpt_ub_start,
      tolerance=tolerance_p_adsorpt)
      "Equilibrium uptake";

  end if;

  state_lastAdsorbedMolecule.p = p_adsorpt_
    "Equilibrium pressure";
  state_lastAdsorbedMolecule.T = T_adsorpt
    "Equilibrium temperautre";

  state_lastAdsorbedMolecule.v = if neglectSpecifcVolume then 0 else
      medium_adsorpt.properties.v
      "Specific volume of the adsorpt";

  //
  // Calculate caloric state properties of the last adsorbed molecule
  // (per adsorptive/adsorpt mass)
  //
  state_lastAdsorbedMolecule.h =
    if not calcCaloricProperties then 0 else
    medium_adsorptive.properties.state.h - h_ads
    "Specific enthalpy of the adsorpt";
  state_lastAdsorbedMolecule.u =
    if not calcCaloricProperties then 0 else
    state_lastAdsorbedMolecule.h - state_lastAdsorbedMolecule.p *
    state_lastAdsorbedMolecule.v
    "Specific internal energy of the adsorpt";

  //
  // Calculate entropic state properties of the last adsorbed molecule
  // (per adsorptive/adsorpt mass)
  //
  state_lastAdsorbedMolecule.s =
    if not (calcCaloricProperties and calcEntropicProperties) then 0 else
    medium_adsorptive.properties.state.s - s_ads
    "Specific entropy of the adsorpt";
  state_lastAdsorbedMolecule.g =
    if not (calcCaloricProperties and calcEntropicProperties) then 0 else
    state_lastAdsorbedMolecule.h - T_adsorpt * state_lastAdsorbedMolecule.s
    "Free enthalpy (i.e., Gibbs free energy) of the adsorpt";
  state_lastAdsorbedMolecule.a =
    if not (calcCaloricProperties and calcEntropicProperties) then 0 else
    state_lastAdsorbedMolecule.u - T_adsorpt * state_lastAdsorbedMolecule.s
    "Free energy (i.e., Helmholts free energy) of the adsorpt";

  //
  // Pass state properties of the uptake-averaged adsorpt
  // (per adsorptive/adsorpt mass)
  //
  state_averageAdsorpt.p = medium_adsorpt.properties.state.p
    "Equilibrium pressure";
  state_averageAdsorpt.T = medium_adsorpt.properties.state.T
    "Equilibrium temperature";
  state_averageAdsorpt.v = medium_adsorpt.properties.state.v
    "Uptake-averaged specific volume of the adsorpt";

  state_averageAdsorpt.h = medium_adsorpt.properties.state.h
    "Uptake-averaged specific enthalpy of the adsorpt";
  state_averageAdsorpt.u = medium_adsorpt.properties.state.u
    "Uptake-averaged specific internal energy of the adsorpt";

  state_averageAdsorpt.s = medium_adsorpt.properties.state.s
    "Uptake-averaged specific entropy of the adsorpt";
  state_averageAdsorpt.g = medium_adsorpt.properties.state.g
    "Uptake-averaged specific free enthalpy (i.e., Gibbs free energy) of the 
    adsorpt";
  state_averageAdsorpt.a = medium_adsorpt.properties.state.a
    "Uptake-averaged specific free energy (i.e., Helmholts free energy) of the 
    adsorpt";

  //
  // Pass state properties of the uptake-averaged adsorbate (per sorbent mass)
  //
  state_averageAdsorbate.p = if not calcAdsorptAdsorbateState then 0 else
    state_averageAdsorpt.p
    "Equilibrium pressure";
  state_averageAdsorbate.T = if not calcAdsorptAdsorbateState then 0 else
    state_averageAdsorpt.T
    "Equilibrium temperature";
  state_averageAdsorbate.v = if not calcAdsorptAdsorbateState then 0 else
    state_sorbent.v + x_adsorpt * state_averageAdsorpt.v
    "Uptake-averaged specific volume of the adsorbate";

  state_averageAdsorbate.h = if not calcAdsorptAdsorbateState then 0 else
    state_sorbent.h + x_adsorpt * state_averageAdsorpt.h
    "Uptake-averaged specific enthalpy of the adsorbate";
  state_averageAdsorbate.u = if not calcAdsorptAdsorbateState then 0 else
    state_sorbent.u + x_adsorpt * state_averageAdsorpt.u
    "Uptake-averaged specific internal energy of the adsorbate";

  state_averageAdsorbate.s = if not calcAdsorptAdsorbateState then 0 else
    state_sorbent.s + x_adsorpt * state_averageAdsorpt.s
    "Uptake-averaged specific entropy of the adsorbate";
  state_averageAdsorbate.g = if not calcAdsorptAdsorbateState then 0 else
    state_sorbent.g + x_adsorpt * state_averageAdsorpt.g
    "Uptake-averaged specific free enthalpy (i.e., Gibbs free energy) of the 
     adsorbate";
  state_averageAdsorbate.a = if not calcAdsorptAdsorbateState then 0 else
    state_sorbent.a + x_adsorpt * state_averageAdsorpt.a
    "Uptake-averaged specific free energy (i.e., Helmholts free energy) of the 
     adsorbate";

  //
  // Pass state properties of the adsorptive (per adsorptive/adsorpt mass)
  //
  state_adsorptive = medium_adsorptive.properties.state
    "State properties of the adsorptive at vapor/gas phase";

  //
  // Pass and calculate additional properties
  //
  h_ads = medium_adsorpt.properties.h_ads
    "Specific enthalpy of adsorption";
  s_ads =
    if not (calcCaloricProperties and calcEntropicProperties) then 0 else
    h_ads / T_adsorpt
    "Specific entropy of adsorption";

  cp_adsorpt = medium_adsorpt.properties.cp
    "Specific heat capacity of the adsorpt";

  //
  // Pass partial derivatives of the isotherm model
  //
  derivatives_isotherm.dx_dp_T =
    medium_adsorpt.properties.derivatives_isotherm.dx_dp_T
    "Partial derivative of the uptake w.r.t. pressure at constant temperature";
  derivatives_isotherm.dx_dT_p =
    medium_adsorpt.properties.derivatives_isotherm.dx_dT_p
    "Partial derivative of the uptake w.r.t. temperature at constant pressure";

  derivatives_isotherm.dp_dx_T =
    medium_adsorpt.properties.derivatives_isotherm.dp_dx_T
    "Partial derivative of the uptake w.r.t. uptake at constant temperature";
  derivatives_isotherm.dp_dT_x =
    medium_adsorpt.properties.derivatives_isotherm.dp_dT_x
    "Partial derivative of the uptake w.r.t. temperature at constant uptake";

  //
  // Pass and calculate partial derivatives regarding the mass and energy balance
  //
  derivatives_massEnergy.dv_sorbent_dp_T =
    if not require_derivativesMassEnergyBalances_pT then 0 else 0
    "Partial derivative of the specific volume of the sorbent w.r.t. pressure at 
    constant temperature (i.e., ideal solid: v_solid = const.)";
  derivatives_massEnergy.dv_sorbent_dT_p =
    if not require_derivativesMassEnergyBalances_pT then 0 else 0
    "Partial derivative of the specific volume of the sorbent w.r.t. temperature 
    at constant pressure (i.e., ideal solid: v_solid = const.";
  derivatives_massEnergy.dv_sorbent_dx_T =
    if not require_derivativesMassEnergyBalances_xT then 0 else
    derivatives_massEnergy.dv_sorbent_dp_T * derivatives_isotherm.dp_dx_T
    "Partial derivative of the specific volume of the sorbent w.r.t. uptake at 
    constant temperature";
  derivatives_massEnergy.dv_sorbent_dT_x =
    if not require_derivativesMassEnergyBalances_xT then 0 else
    derivatives_massEnergy.dv_sorbent_dT_p +
    derivatives_massEnergy.dv_sorbent_dp_T * derivatives_isotherm.dp_dT_x
    "Partial derivative of the specific volume of the sorbent w.r.t. temperature 
    at constant uptake";

  derivatives_massEnergy.dh_sorbent_dp_T =
    if not require_derivativesMassEnergyBalances_pT then 0 else
    state_sorbent.v
    "Partial derivative of the specific enthalpy of the sorbent w.r.t. pressure
    at constant temperature (i.e., beta_sorbent = 0)";
  derivatives_massEnergy.dh_sorbent_dT_p =
    if not require_derivativesMassEnergyBalances_pT then 0 else
    medium_sorbent.additional_variables.c
    "Partial derivative of the specific enthalpy of the sorbent w.r.t. temperature
    at constant pressure";
  derivatives_massEnergy.dh_sorbent_dx_T =
    if not require_derivativesMassEnergyBalances_xT then 0 else
    derivatives_massEnergy.dh_sorbent_dp_T * derivatives_isotherm.dp_dx_T
    "Partial derivative of the specific enthalpy of the sorbent w.r.t. uptake
    at constant temperature";
  derivatives_massEnergy.dh_sorbent_dT_x =
    if not require_derivativesMassEnergyBalances_xT then 0 else
    derivatives_massEnergy.dh_sorbent_dT_p +
    derivatives_massEnergy.dh_sorbent_dp_T * derivatives_isotherm.dp_dT_x
    "Partial derivative of the specific enthalpy of the sorbent w.r.t. temperature
    at constant uptake";

  derivatives_massEnergy.dxv_avg_adsorpt_dp_T =
    if not require_derivativesMassEnergyBalances_pT then 0 else
    medium_adsorpt.properties.dxv_avg_dp_T
    "Partial derivative of the uptake-averaged specific volume of the adsorpt
    times the uptake w.r.t. pressure at constant temperature";
  derivatives_massEnergy.dxv_avg_adsorpt_dT_p =
    if not require_derivativesMassEnergyBalances_pT then 0 else
    medium_adsorpt.properties.dxv_avg_dT_p
    "Partial derivative of the uptake-averaged specific volume of the adsorpt
    times the uptake w.r.t. temperature at constant pressure";
  derivatives_massEnergy.dxv_avg_adsorpt_dx_T =
    if not require_derivativesMassEnergyBalances_xT then 0 else
    derivatives_massEnergy.dxv_avg_adsorpt_dp_T * derivatives_isotherm.dp_dx_T
    "Partial derivative of the uptake-averaged specific volume of the adsorpt
    times the uptake w.r.t. uptake at constant temperature";
  derivatives_massEnergy.dxv_avg_adsorpt_dT_x =
    if not require_derivativesMassEnergyBalances_xT then 0 else
    derivatives_massEnergy.dxv_avg_adsorpt_dT_p +
    derivatives_massEnergy.dxv_avg_adsorpt_dp_T * derivatives_isotherm.dp_dT_x
    "Partial derivative of the uptake-averaged specific volume of the adsorpt
    times the uptake w.r.t. temperature at constant uptake";

  derivatives_massEnergy.dxh_avg_adsorpt_dp_T =
    if not require_derivativesMassEnergyBalances_pT then 0 else
    medium_adsorpt.properties.dxh_avg_dp_T
    "Partial derivative of the uptake-averaged specific enthalpy of the adsorpt 
    times the uptake w.r.t. pressure at constant temperature";
  derivatives_massEnergy.dxh_avg_adsorpt_dT_p =
    if not require_derivativesMassEnergyBalances_pT then 0 else
    medium_adsorpt.properties.dxh_avg_dT_p
    "Partial derivative of the uptake-averaged specific enthalpy of the adsorpt
    times the uptake w.r.t. temperature at constant pressure";
  derivatives_massEnergy.dxh_avg_adsorpt_dx_T =
    if not require_derivativesMassEnergyBalances_xT then 0 else
    medium_adsorpt.properties.dxh_avg_dx_T
    "Partial derivative of the uptake-averaged specific enthalpy of the adsorpt
    times the uptake w.r.t. uptake at constant temperature";
  derivatives_massEnergy.dxh_avg_adsorpt_dT_x =
    if not require_derivativesMassEnergyBalances_xT then 0 else
    medium_adsorpt.properties.dxh_avg_dT_x
    "Partial derivative of the uptake-averaged specific enthalpy of the adsorpt
    times the uptake w.r.t. temperature at constant uptake";

  //
  // Pass and calculate partial derivatives regarding the entropy balance
  //
  derivatives_entropy.ds_sorbent_dp_T =
    if not require_derivativesEntropyBalances_pT then 0 else 0
    "Partial derivative of the specific entropy of the sorbent w.r.t. pressure
    at constant temperature (i.e., ideal solid: v_solid = const.)";
  derivatives_entropy.ds_sorbent_dT_p =
    if not require_derivativesEntropyBalances_pT then 0 else
    medium_sorbent.additional_variables.c / T_adsorpt
    "Partial derivative of the specific entropy of the sorbent w.r.t. temperature
    at constant pressure";
  derivatives_entropy.ds_sorbent_dx_T =
    if not require_derivativesEntropyBalances_xT then 0 else
    derivatives_entropy.ds_sorbent_dp_T * derivatives_isotherm.dp_dx_T
    "Partial derivative of the specific entropy of the sorbent w.r.t. uptake
    at constant temperature";
  derivatives_entropy.ds_sorbent_dT_x =
    if not require_derivativesEntropyBalances_xT then 0 else
    derivatives_entropy.ds_sorbent_dT_p +
    derivatives_entropy.ds_sorbent_dp_T * derivatives_isotherm.dp_dT_x
    "Partial derivative of the specific entropy of the sorbent w.r.t. temperature
    at constant uptake";

  derivatives_entropy.dxs_avg_adsorpt_dp_T =
    if not require_derivativesEntropyBalances_pT then 0 else
    medium_adsorpt.properties.dxs_avg_dp_T
    "Partial derivative of the uptake-averaged specific entropy of the adsorpt
    times the uptake w.r.t. pressure at constant temperature";
  derivatives_entropy.dxs_avg_adsorpt_dT_p =
    if not require_derivativesEntropyBalances_pT then 0 else
    medium_adsorpt.properties.dxs_avg_dT_p
    "Partial derivative of the uptake-averaged specific entropy of the adsorpt
    times the uptake w.r.t. temperature at constant pressure";
  derivatives_entropy.dxs_avg_adsorpt_dx_T =
    if not require_derivativesEntropyBalances_xT then 0 else
    medium_adsorpt.properties.dxs_avg_dx_T
    "Partial derivative of the uptake-averaged specific entropy of the adsorpt
    times the uptake w.r.t. uptake at constant temperature";
  derivatives_entropy.dxs_avg_adsorpt_dT_x =
    if not require_derivativesEntropyBalances_xT then 0 else
    medium_adsorpt.properties.dxs_avg_dT_x
    "Partial derivative of the uptake-averaged specific entropy of the adsorpt 
    times the uptake w.r.t. temperature at constant uptake";

  //
  // Assertations
  //
  if adsorptiveAtDewPoint then
    assert(WorkingPair.twoPhaseAdsorptive,
      "Selected adsorptive medium model does not have a two-phase regime. " +
      "Cannot calculate state properties of dew point at T_adsorpt.",
      level = AssertionLevel.error);
  end if;

  if approachSpecificVolume ==
    SorpLib.Choices.SpecificVolumeAdsorpt.BoilingCurve then
    assert(WorkingPair.twoPhaseAdsorptive,
      "Selected adsorptive medium model does not have a two-phase regime. " +
      "Cannot calculate specific volume of the adsorpt at the bubble point.",
      level = AssertionLevel.error);
  end if;

  if approachSpecificHeatCapacity ==
    SorpLib.Choices.SpecificHeatCapacityAdsorpt.BoilingCurve then
    assert(WorkingPair.twoPhaseAdsorptive,
      "Selected adsorptive medium model does not have a two-phase regime. " +
      "Cannot calculate specific heat capacity of the adsorpt at the bubble point.",
      level = AssertionLevel.error);
  end if;

  if approachSorptionEnthalpy == SorpLib.Choices.SorptionEnthalpy.Dubinin then
    assert(WorkingPair.modelOfDubinin,
      "You cannot calculate the specific enthaly of adsorption using the model " +
      "of Dubinin if the isotherm model is not the model of Dubinin.",
      level = AssertionLevel.error);

    assert(WorkingPair.twoPhaseAdsorptive,
      "Selected adsorptive medium model does not have a two-phase regime. " +
      "Cannot calculate specific enthalpy of adsorption according to Dubinin.",
      level = AssertionLevel.error);
  end if;

  if calcAdsorptAdsorbateState then
    assert(calcCaloricProperties,
      "You cannot calculate uptake-averaged adsorpt and adsorbate properties if " +
      "you do not calculate at least caloric properties.",
      level = AssertionLevel.error);
  end if;

  if calcEntropicProperties then
    assert(calcCaloricProperties,
      "You cannot calculate entropic properties if you do not calculate caloric " +
      "properties.",
      level = AssertionLevel.error);
  end if;

  if calcDerivativesEntropyBalance then
    assert(calcCaloricProperties,
      "You cannot calculate partial derivatives for the mass and energy balances " +
      "if you do not calculate caloric properties.",
      level = AssertionLevel.error);

    assert(calcDerivativesIsotherm,
      "You cannot calculate partial derivatives for the mass and energy balances " +
      "if you do not calculate partial derivatives of the isotherm models.",
      level = AssertionLevel.error);
  end if;

  if calcDerivativesEntropyBalance then
    assert(calcCaloricProperties,
      "You cannot calculate partial derivatives for the entropy balance if you " +
      "do not calculate caloric properties.",
      level = AssertionLevel.error);

    assert(calcDerivativesIsotherm,
      "You cannot calculate partial derivatives for the entropy balance if you " +
      "do not calculate partial derivatives of the isotherm models.",
      level = AssertionLevel.error);

    assert(calcDerivativesMassEnergyBalance,
      "You cannot calculate partial derivatives for the entropy balance if you " +
      "do not calculate partial derivatives of the mass and energy balances.",
      level = AssertionLevel.error);
  end if;

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This model calculates the thermodynamic properties of a working pair. 
Specifically, the properties of the sorbent, the adsorpt (i.e. only 
the adsorbed molecules), the adsorbate (i.e. sorbent and adsorpt) and 
the adsorptive (i.e. gas/vapor phase) are calculated. Depending on 
the model settings, only common state variables such as pressure, 
temperature, loading, and specific enthalpy are calculated. Note that 
adsorption is very heterogeous, so the adsorpt phase is not homogenous. 
Therefore, macroscopic averaging can be applied using the loading for 
averaging. As this averaging requires solving integrals numerically, it
should only be done if required. In addition to the phase properties, 
partial derivatives can also be calculated, which are required for 
dynamic mass, energy, and entropy balances in a working pair volume.
</p>

<h4>Main equations for the last adsorbed molecule</h4>
<p>
The adsorption equilibrium is calculated via an isotherm model, thus 
connecting the uptake, pressure, and temperature:
</p>
<pre>
    x<sub>adsorpt</sub>(p<sub>adsorpt</sub>, T<sub>adsorpt</sub>);
</pre>
<p>
For details on available isotherm models, check the package
<a href=\"Modelica://SorpLib.Media.Functions.SorptionEquilibria\">SorpLib.Media.Functions.SorptionEquilibria</a>.
</p>

<p>
The specific average enthalpy of the last adsorbed molecule
<i>h<sub>adsorpt</sub></i> is calculated as
</p>
<pre>
    h<sub>adsorpt</sub> = h<sub>adsorptive</sub> - &Delta;h<sub>ads</sub>;
</pre>
<p>
and the specific entropy of the last adsorpbed molecule 
<i>s<sub>adsorpt</sub></i> is calculated as
</p>
<pre>
    s<sub>adsorpt</sub> = s<sub>adsorptive</sub> - &Delta;h<sub>ads</sub> / T<sub>adsorpt</sub>;
</pre>
<p>
Herein, <i>h<sub>adsorptive</sub></i>/<i>s<sub>adsorptive</sub></i> is 
the specific enthalpy/entropy of the adsorptive and <i>&Delta;h<sub>ads</sub></i> 
is the specific enthalpy of adsorption. For the specific enthalpy of 
adsorption, different calculation approaches are implemented. For details, 
check the package
<a href=\"Modelica://SorpLib.Media.Functions.SorptionEnthalpies\">SorpLib.Media.Functions.SorptionEnthalpies</a>.
</p>

<p>
Knowing the specific enthalpy of the adsorpt, the specific internal 
energy ofthe last adsorbed molecule <i>u<sub>adsorpt</sub></i> is 
calculated as
</p>
<pre>
    u<sub>adsorpt</sub> = h<sub>adsorpt</sub> - p<sub>adsorpt</sub> * v<sub>adsorpt</sub>;
</pre>
<p>
Herein, <i>v<sub>adsorpt</sub></i> is the specific volume of the 
adsorpt, which can be calculated using different approaches. For 
details, check the enumeration
<a href=\"Modelica://SorpLib.Choices.SpecificVolumeAdsorpt\">SorpLib.Choices.SpecificVolumeAdsorpt</a>.
</p>

<h4>Main equations for the uptake-averaged adsorpt (i.e., macroscopic properties)</h4>
<p>
The adsorption phenomenon is very heterogeneous, so the adsorpt 
phase is typically not homogeneous. Therefore, macroscopic 
averaging is performed using the loading <i>x<sub>adsorpt</sub></i> 
to obtain average, homogeneous fluid properties
<i><SPAN STYLE=\"text-decoration:overline\">z</SPAN><sub>adsorpt</sub></i>:
</p>
<pre>
    <SPAN STYLE=\"text-decoration:overline\">z</SPAN><sub>adsorpt</sub> = 1/x<sub>adsorpt</sub> * <SPAN STYLE=\"text-decoration:underline\">z</SPAN><sub>adsorpt</sub> = 1/x<sub>adsorpt</sub> * &int;_0^x<sub>adsorpt</sub> [z(x',T<sub>adsorpt</sub>)] dx';
</pre>
<p>
Herein, loading <i>x<sub>adsorpt</sub></i> and temperature <i>T<sub>adsorpt</sub></i> 
are assumed to independent state properties. This averaging must be 
performed for all fluid quantities except for pressure <i>p<sub>adsorpt</sub></i> 
(mechanical equilibrium), temperature <i>T<sub>adsorpt</sub></i> (thermal 
equilibrium), and, consequently, loading <i>x<sub>adsorpt</sub></i>. 
Since the specific volume of the adsorpt phase <i>v<sub>adsorpt</sub></i> 
depends only on temperature (see assumptions), it already is a macroscopic
property and no averaging is necessary.
</p>

<h4>Main equations for uptake-averaged adsorbate</h4>
Sstate properties <i>z</i> of the adsorbate are calculated as
</p>
<pre>
    z<sub>adsorbate</sub> = z<sub>sor</sub> + x<sub>adsorpt</sub> * <SPAN STYLE=\"text-decoration:overline\">z</SPAN><sub>adsorpt</sub>;
</pre>
<p>
and, thus, are given per unit sorbent mass.
</p>

<h4>Main equations for partial derivatives required for mass, energy, and entropy balances</h4>
<p>
Transient governing equations are used in finite volume models under 
<a href=\"Modelica://SorpLib.Basics.Volumes.Adsorbates.\">SorpLib.Basics.Volumes.Adsorbates</a>.
This requires partial differential equations, which have to be calculated 
partially for the uptake-averaged properties. The required partial 
derivatives vary with the selected independent differential states, which 
can be either pressure <i>p<sub>adsorpt</sub></i> and temperature 
<i>T<sub>adsorpt</sub></i> or loading <i>x<sub>adsorpt</sub></i> and 
temperature <i>T<sub>adsorpt</sub></i>.
</p>

<p>
For all conservation equations, partial derivatives of the isotherm 
model are provided to switch between the independent differential states:
</p>
<ul>
  <li>
  <i>(&part;x/&part;p)<sub>T</sub></i>: 
  Partial derivative of the loading w.r.t. to pressure at 
  constant temperature.
  </li>
  <li>
  <i>(&part;x/&part;T)<sub>p</sub></i>: 
  Partial derivative of the loading w.r.t. to temperature at 
  constant pressure.
  </li>
  <li>
  <i>(&part;p/&part;x)<sub>T</sub> = 1 / (&part;x/&part;p)<sub>T</sub></i>: 
  Partial derivative of the pressure w.r.t. to loading at 
  constant temperature.
  </li>
  <li>
  <i>(&part;p/&part;T)<sub>x</sub> = -(&part;x/&part;T)<sub>p</sub> / (&part;x/&part;p)<sub>T</sub></i>: 
  Partial derivative of the pressure w.r.t. to temperature at 
  constant loading.
  </li>
</ul>

<p>
The specific volume of the sorbent <i>v<sub>sor</sub></i> is constant 
(see assumptions), so the mass of the sorbent <i>m<sub>sor</sub></i> 
does not change with the properties of the substance. The mass balance 
of the adsorpt is expressed directly in terms of the loading 
<i>x<sub>adsorpt</sub></i>, so no partial derivatives of the density 
are required either.
</p>

<p>
The energy balance of an adsorbate volume can be derived from the 
definition of the internal energy of the adsorbate volume 
<i>U<sub>adsorbate</sub></i>:
</p>
<pre>
    U<sub>adsorbate</sub> = m<sub>sor</sub> * u<sub>sor</sub> + m<sub>adsorpt</sub> * <SPAN STYLE=\"text-decoration:overline\">u</SPAN><sub>adsorpt</sub> = m<sub>sor</sub> * [u<sub>sor</sub> + x<sub>adsorpt</sub> * <SPAN STYLE=\"text-decoration:overline\">u</SPAN><sub>adsorpt</sub>];
</pre>
<p>
Thus, the total differential of the  internal energy of the adsorbate 
volume <i>dU<sub>adsorbate</sub></i> is:
</p>
<pre>
    dU<sub>adsorbate</sub> = dm<sub>sor</sub> * [u<sub>sor</sub> + x<sub>adsorpt</sub> * <SPAN STYLE=\"text-decoration:overline\">u</SPAN><sub>adsorpt</sub>] + m<sub>sor</sub> * [du<sub>sor</sub> + d(x<sub>adsorpt</sub> * <SPAN STYLE=\"text-decoration:overline\">u</SPAN><sub>adsorpt</sub>)] = dm<sub>sor</sub> * [u<sub>sor</sub> + x<sub>adsorpt</sub> * <SPAN STYLE=\"text-decoration:overline\">u</SPAN><sub>adsorpt</sub>] + m<sub>sor</sub> * [du<sub>sor</sub> + d<SPAN STYLE=\"text-decoration:underline\">u</SPAN><sub>adsorpt</sub>];
</pre>
<p>
The total differential <i>dU<sub>adsorbate</sub></i> can be further 
simplified if the definition of the specific internal energy 
<i>u = h - p * v</i> is used and then the macroscopic quantities of 
the adsorpt (pressure <i>p = p<sub>adsorpt</sub></i>, temperature 
<i>T = T<sub>adsorpt</sub></i>, and specific volume <i>v<sub>adsorpt</sub></i>) 
are taken into account:
</p>
<pre>
    dU<sub>adsorbate</sub> = dm<sub>sor</sub> * [u<sub>sor</sub> + x<sub>adsorpt</sub> * <SPAN STYLE=\"text-decoration:overline\">u</SPAN><sub>adsorpt</sub>] + m<sub>sor</sub> * [dh<sub>sor</sub> - p * dv<sub>sor</sub> - v<sub>sor</sub> * dp + d<SPAN STYLE=\"text-decoration:underline\">h</SPAN><sub>adsorpt</sub> - d(x<sub>adsorpt</sub> * v<sub>adsorpt</sub> * p)] = dm<sub>sor</sub> * [u<sub>sor</sub> + x<sub>adsorpt</sub> * <SPAN STYLE=\"text-decoration:overline\">u</SPAN><sub>adsorpt</sub>] + m<sub>sor</sub> * [dh<sub>sor</sub> - p * dv<sub>sor</sub> - v<sub>sor</sub> * dp + d<SPAN STYLE=\"text-decoration:underline\">h</SPAN><sub>adsorpt</sub> - d(x<sub>adsorpt</sub> * v<sub>adsorpt</sub>) * p - x<sub>adsorpt</sub> * v<sub>adsorpt</sub> * dp];
</pre>

<p>
Thus, the following partial derivatives are provided for the sorbent:
</p>
<ul>
  <li>
  <i>(&part;h<sub>sor</sub>/&part;p)<sub>T</sub> = v<sub>sor</sub> * (1 - T * &beta;<sub>sor</sub>)</i>: 
  Partial derivative of the specific enthaly w.r.t. to pressure at 
  constant temperature.
  </li>
  <li>
  <i>(&part;h<sub>sor</sub>/&part;T)<sub>p</sub> = c<sub>sor</sub></i>: 
  Partial derivative of the specific enthaly w.r.t. to temperature at 
  constant pressure.
  </li>
  <li>
  <i>(&part;h<sub>sor</sub>/&part;x)<sub>T</sub></i>: 
  Partial derivative of the specific enthaly w.r.t. to loading at 
  constant temperature.
  </li>
  <li>
  <i>(&part;h<sub>sor</sub>/&part;T)<sub>x</sub></i>: 
  Partial derivative of the specific enthaly w.r.t. to temperature at 
  constant loading.
  </li>
  <li>
  <i>(&part;v<sub>sor</sub>/&part;p)<sub>T</sub> = 0</i>: 
  Partial derivative of the specific volume w.r.t. to pressure at 
  constant temperature.
  </li>
  <li>
  <i>(&part;v<sub>sor</sub>/&part;T)<sub>p</sub> = 0</i>: 
  Partial derivative of the specific volume w.r.t. to temperature at 
  constant pressure.
  </li>
  <li>
  <i>(&part;v<sub>sor</sub>/&part;x)<sub>T</sub> = 0</i>: 
  Partial derivative of the specific volume w.r.t. to loading at 
  constant temperature.
  </li>
  <li>
  <i>(&part;v<sub>sor</sub>/&part;T)<sub>x</sub> = 0</i>: 
  Partial derivative of the specific volume w.r.t. to temperature at 
  constant loading.
  </li>
</ul>
<p>
Accordingly, the following partial derivatives are provided for the 
adsorpt:
</p>
<ul>
  <li>
  <i>(&part;<SPAN STYLE=\"text-decoration:underline\">h</SPAN><sub>adsorpt</sub>/&part;p)<sub>T</sub></i>: 
  Partial derivative of the specific enthaly w.r.t. to pressure at 
  constant temperature.
  </li>
  <li>
  <i>(&part;<SPAN STYLE=\"text-decoration:underline\">h</SPAN><sub>adsorpt</sub>/&part;T)<sub>p</sub></i>: 
  Partial derivative of the specific enthaly w.r.t. to temperature at 
  constant pressure.
  </li>
  <li>
  <i>(&part;<SPAN STYLE=\"text-decoration:underline\">h</SPAN><sub>adsorpt</sub>/&part;x)<sub>T</sub> = h<sub>adsorpt</sub></i>: 
  Partial derivative of the specific enthaly w.r.t. to loading at 
  constant temperature.
  </li>
  <li>
  <i>(&part;<SPAN STYLE=\"text-decoration:underline\">h</SPAN><sub>adsorpt</sub>/&part;T)<sub>x</sub> = c<sub>adsorpt</sub> * x<sub>adsorpt</sub></i>: 
  Partial derivative of the specific enthaly w.r.t. to temperature at 
  constant loading. It was assumed that the specific heat capacity is 
  either independent of the loading or directly corresponds to the 
  uptake-averaged specific heat capacity (see assumptions).
  </li>
  <li>
  <i>(&part;(x<sub>adsorpt</sub> * v<sub>adsorpt</sub>)/&part;p)<sub>T</sub> = x<sub>adsorpt</sub> * (&part;(v<sub>adsorpt</sub>)/&part;p)<sub>T</sub> + (&part;(x<sub>adsorpt</sub>)/&part;p)<sub>T</sub> * v<sub>adsorpt</sub></i>: 
  Partial derivative of the product of specific volume and uptake w.r.t. 
  to pressure at constant temperature.
  </li>
  <li>
  <i>(&part;(x<sub>adsorpt</sub> * v<sub>adsorpt</sub>)/&part;T)<sub>p</sub> = x<sub>adsorpt</sub> * (&part;(v<sub>adsorpt</sub>)/&part;T)<sub>p</sub> + (&part;(x<sub>adsorpt</sub>)/&part;T)<sub>p</sub> * v<sub>adsorpt</sub></i></i>:  
  Partial derivative of the product of specific volume and uptake w.r.t. 
  to temperature at constant pressure.
  </li>
  <li>
  <i>(&part;v(x<sub>adsorpt</sub> * v<sub>adsorpt</sub>)&part;x)<sub>T</sub></i>:  
  Partial derivative of the product of specific volume and uptake w.r.t. 
  to loading at constant temperature.
  </li>
  <li>
  <i>(&part;(x<sub>adsorpt</sub> * v<sub>adsorpt</sub>)/&part;T)<sub>x</sub></i>:  
  Partial derivative of the product of specific volume and uptake w.r.t. 
  to temperature at constant loading.
  </li>
</ul>

<p>
The partial derivatives required for the entropy balance can be 
determined in the same way as for the energy balance. The total 
entropy of the adsorbate volume <i>S<sub>adsorbate</sub></i> is:
</p>
<pre>
    S<sub>adsorbate</sub> = m<sub>sor</sub> * [s<sub>sor</sub> + x<sub>adsorpt</sub> * <SPAN STYLE=\"text-decoration:overline\">s</SPAN><sub>adsorpt</sub>];
</pre>
<p>
Thus, when applying all assumptions, the total differential of the 
entropy of the adsorbate volume <i>dS<sub>adsorbate</sub></i> is:
</p>
<pre>
    dS<sub>adsorbate</sub> = dm<sub>sor</sub> * [s<sub>sor</sub> + x<sub>adsorpt</sub> * <SPAN STYLE=\"text-decoration:overline\">s</SPAN><sub>adsorpt</sub>] + m<sub>sor</sub> * [ds<sub>sor</sub> + d<SPAN STYLE=\"text-decoration:underline\">s</SPAN><sub>adsorpt</sub>];
</pre>
<p>
Consequently, the following partial derivatives are provided for the 
sorbent and adsorpt:
</p>
<ul>
  <li>
  <i>(&part;s<sub>sor</sub>/&part;p)<sub>T</sub> = v<sub>sor</sub> * &beta;<sub>sor</sub></i>: 
  Partial derivative of the specific entropy w.r.t. to pressure at 
  constant temperature.
  </li>
  <li>
  <i>(&part;s<sub>sor</sub>/&part;T)<sub>p</sub> = c<sub>sor</sub> / T</i>: 
  Partial derivative of the specific entropy w.r.t. to temperature at 
  constant pressure.
  </li>
  <li>
  <i>(&part;s<sub>sor</sub>/&part;x)<sub>T</sub></i>: 
  Partial derivative of the specific entropy w.r.t. to loading at 
  constant temperature.
  </li>
  <li>
  <i>(&part;s<sub>sor</sub>/&part;T)<sub>x</sub></i>: 
  Partial derivative of the specific entropy w.r.t. to temperature at 
  constant loading.
  </li>
  <li>
  <i>(&part;<SPAN STYLE=\"text-decoration:underline\">s</SPAN><sub>adsorpt</sub>/&part;p)<sub>T</sub></i>: 
  Partial derivative of the specific entropy w.r.t. to pressure at 
  constant temperature.
  </li>
  <li>
  <i>(&part;<SPAN STYLE=\"text-decoration:underline\">s</SPAN><sub>adsorpt</sub>/&part;T)<sub>p</sub></i>: 
  Partial derivative of the specific entropy w.r.t. to temperature at 
  constant pressure.
  </li>
  <li>
  <i>(&part;<SPAN STYLE=\"text-decoration:underline\">s</SPAN><sub>adsorpt</sub>/&part;x)<sub>T</sub> = s<sub>adsorpt</sub></i>: 
  Partial derivative of the specific entropy w.r.t. to loading at 
  constant temperature.
  </li>
  <li>
  <i>(&part;<SPAN STYLE=\"text-decoration:underline\">s</SPAN><sub>adsorpt</sub>/&part;T)<sub>x</sub> = c<sub>adsorpt</sub> / T * x<sub>adsorpt</sub></i>: 
  Partial derivative of the specific entropy w.r.t. to temperature at 
  constant loading. It was assumed that the specific heat capacity is 
  either independent of the loading or directly corresponds to the 
  uptake-averaged specific heat capacity (see assumptions).
  </li>
  <li>
</ul>

<p>
Partial derivatives that are not fully specified can be calculated 
using the fundamental laws of analysis and the specified partial 
derivatives as well as the partial derivatives of the isotherm model.
</p>

<h4>Assumptions and limitations</h4>
<ul>
  <li>
  Adsorption equilibrium with all adsorptive beeing adsorbed and in adsorpt
  phase (i.e., chemical equilibrium).
  </li>
  <li>
  The sorbent and adsorpt have the same pressure (i.e., mechanical equilibrium).
  </li>
  <li>
  The sorbent and adsorpt have the same temperature (i.e., thermal equilibrium).
  </li>
  <li>
  Specific volume of the adsorpt may only depend on the temperature and is thus
  a macroscopic property without averaging.
  </li>
  <li>
  Specific heat capacity of the adsorpt may only depend on the temperature or is
  assumed to already be an uptake-averaged specific heat capacity.
  </li>
  <li>
  Ideal and inert sorbent with constant specific volume.
  </li>
</ul>

<h4>Typical use</h4>
<p>
This model is mainly used to calculate thermodynamic properties of an adsorbate 
volume.
</p>

<h4>Important parameters and options</h4>
<ul>
  <li>
  All parameters of the calculation setup: Define which variables are calculated.
  </li>
  <li>
  <i>adsorptiveAtDewPoint = true</i>: Assume thermodynamic state of the adsorptive
  to correspond to the dew point at given temperature.
  </li>
  <li>
  <i>neglectSpecifcVolume = true</i>: Neglect the specific volume of the adsorpt.
  This options can significantly simplify the calculation procedure and is often
  a good assumption.
  </li>
  <li>
  <i>approachSpecificVolume</i>: Choose the calculation approach for the specific
  volume of the adsorpt.
  </li>
  <li>
  <i>approachSpecificHeatCapacity</i>: Choose the calculation approach for the 
  specific heat capacity of the adsorpt.
  </li>
  <li>
  <i>approachSorptionEnthalpy</i>: Choose the calculation approach for the 
  specific enthalpy of adsorption.
  </li>
</ul>

<h4>Dynamics</h4>
<p>
Dynamic states are either the pressure and temperature or the uptake and
temperature. To change the dynamic states, use the parameter <i>stateVariables</i>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  Major adaptations and extensions (e.g. partial derivatives for mass,
  energy, and entropy balances or properties of the sorbent).
  </li>
  <li>
  December 14, 2020, by Mirko Engelpracht:<br/>
  Major reivisions (e.g., object-oriented approach, functionaliy) due 
  to restructuring of the library.
  </li>
  <li>
  November 17, 2017, by Uwe Bau:<br/> 
  Tidy up implementation and enhance documentation for publication of 
  library.
  </li>
</ul>
</html>"));
end PartialPureWorkingPairs;
