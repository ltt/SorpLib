within SorpLib.Media.WorkingPairs.BaseClasses;
partial model PartialPureCharts
  "Base model that creates an isotherm chart of a pure component working pair"
  extends Modelica.Icons.Example;

  //
  // Definition of parameters
  //
  parameter SorpLib.Choices.DiagramTypePureComponentWorkingPair diagramType=
    SorpLib.Choices.DiagramTypePureComponentWorkingPair.Isotherms
    "Diagram type: Isotherms, isosters, or isobars"
    annotation (Dialog(tab="General", group="Calculation Setup"),
                Evaluate=true,
                HideResult=true);
  parameter SorpLib.Choices.ChangingConstantVariableOfDiagram isoLineType=
    SorpLib.Choices.ChangingConstantVariableOfDiagram.Manual
    "Changing type of iso-lines: Manual, linearly distributed, or logarithmically
    distributed"
    annotation (Dialog(tab="General", group="Calculation Setup"),
                Evaluate=false,
                HideResult=false);
  parameter Integer no_isoLines = 10
    "Number of isotherm or isobars"
    annotation (Dialog(tab="General", group="Calculation Setup"),
                Evaluate=true,
                HideResult=true);

  parameter Modelica.Units.SI.Pressure p_adsorpt_min = 1000
    "Minimal pressure for diagram of isotherms or isobars"
    annotation (Dialog(tab="General", group="Setup Pressure",
                enable=
                  (diagramType==
                    SorpLib.Choices.DiagramTypePureComponentWorkingPair.Isotherms or
                  (diagramType==
                    SorpLib.Choices.DiagramTypePureComponentWorkingPair.Isobars and
                  isoLineType<>
                    SorpLib.Choices.ChangingConstantVariableOfDiagram.Manual))),
                Evaluate=false,
                HideResult=false);
  parameter Modelica.Units.SI.Pressure p_adsorpt_max = 1e5
    "Maximal pressure for diagram of isotherms or isobars"
    annotation (Dialog(tab="General", group="Setup Pressure",
                enable=
                  (diagramType==
                    SorpLib.Choices.DiagramTypePureComponentWorkingPair.Isotherms or
                  (diagramType==
                    SorpLib.Choices.DiagramTypePureComponentWorkingPair.Isobars and
                  isoLineType<>
                    SorpLib.Choices.ChangingConstantVariableOfDiagram.Manual))),
                Evaluate=false,
                HideResult=false);
  parameter Modelica.Units.SI.Pressure[no_isoLines] p_adsorpt_const=
    linspace(p_adsorpt_min, p_adsorpt_max, no_isoLines)
    "Constant pressures when using isobars"
    annotation (Dialog(tab="General", group="Setup Pressure",
                enable=
                  (diagramType==
                    SorpLib.Choices.DiagramTypePureComponentWorkingPair.Isobars and
                  isoLineType==
                    SorpLib.Choices.ChangingConstantVariableOfDiagram.Manual)),
                Evaluate=false,
                HideResult=false);

  parameter Modelica.Units.SI.Temperature T_adsorpt_min = 283.15
    "Minimal temperature for diagram of isosters or isotherms"
    annotation (Dialog(tab="General", group="Setup Temperature",
                enable=
                  (diagramType<>
                    SorpLib.Choices.DiagramTypePureComponentWorkingPair.Isotherms or
                  (diagramType==
                    SorpLib.Choices.DiagramTypePureComponentWorkingPair.Isotherms and
                  isoLineType<>
                    SorpLib.Choices.ChangingConstantVariableOfDiagram.Manual))),
                Evaluate=false,
                HideResult=false);
  parameter Modelica.Units.SI.Temperature T_adsorpt_max = 373.15
    "Maximal temperature for diagram of isosters or isotherms"
    annotation (Dialog(tab="General", group="Setup Temperature",
                enable=
                  (diagramType<>
                    SorpLib.Choices.DiagramTypePureComponentWorkingPair.Isotherms or
                  (diagramType==
                    SorpLib.Choices.DiagramTypePureComponentWorkingPair.Isotherms and
                  isoLineType<>
                    SorpLib.Choices.ChangingConstantVariableOfDiagram.Manual))),
                Evaluate=false,
                HideResult=false);
  parameter Modelica.Units.SI.Temperature[no_isoLines] T_adsorpt_const=
    linspace(T_adsorpt_min, T_adsorpt_max, no_isoLines)
    "Constant temperatures when using isotherms"
    annotation (Dialog(tab="General", group="Setup Temperature",
                enable=
                  (diagramType==
                    SorpLib.Choices.DiagramTypePureComponentWorkingPair.Isotherms and
                  isoLineType==
                    SorpLib.Choices.ChangingConstantVariableOfDiagram.Manual)),
                Evaluate=false,
                HideResult=false);

  parameter SorpLib.Units.Uptake x_adsorpt_min = 0.05
    "Minimal uptake for diagram of isosters"
    annotation (Dialog(tab="General", group="Setup Uptake",
                enable=
                  diagramType==
                    SorpLib.Choices.DiagramTypePureComponentWorkingPair.Isosters and
                  isoLineType<>
                    SorpLib.Choices.ChangingConstantVariableOfDiagram.Manual),
                Evaluate=false,
                HideResult=false);
  parameter SorpLib.Units.Uptake x_adsorpt_max = 0.2
    "Maximal uptake for diagram of isosters"
    annotation (Dialog(tab="General", group="Setup Uptake",
                enable=
                  diagramType==
                    SorpLib.Choices.DiagramTypePureComponentWorkingPair.Isosters and
                  isoLineType<>
                    SorpLib.Choices.ChangingConstantVariableOfDiagram.Manual),
                Evaluate=false,
                HideResult=false);
  parameter SorpLib.Units.Uptake[no_isoLines] x_adsorpt_const=
    linspace(x_adsorpt_min, x_adsorpt_max, no_isoLines)
    "Constant uptake when using isosters"
    annotation (Dialog(tab="General", group="Setup Uptake",
                enable=
                  (diagramType==
                    SorpLib.Choices.DiagramTypePureComponentWorkingPair.Isosters and
                  isoLineType==
                    SorpLib.Choices.ChangingConstantVariableOfDiagram.Manual)),
                Evaluate=false,
                HideResult=false);

  //
  // Definition of parameters describing packages and models
  //
  replaceable model PureWorkingPairModel =
    SorpLib.Media.WorkingPairs.PureComponents.CO2.ActivatedCarbon_Toth_DantasEtAl2011_Gas
    constrainedby
    SorpLib.Media.WorkingPairs.BaseClasses.PartialPureWorkingPairs(
      final stateVariables=stateVariables,
      final calcCaloricProperties=true,
      final calcEntropicProperties=true,
      final calcDerivativesIsotherm=true,
      final calcDerivativesMassEnergyBalance=true,
      final calcDerivativesEntropyBalance=true)
    "Pure working pair model"
    annotation (Dialog(tab="General",group="Models and Media"),
                choicesAllMatching = true,
                Evaluate=true,
                HideResult=true);

  //
  // Instanziation of models
  //
  PureWorkingPairModel[no_isoLines] pureWorkingPair(
    final p_adsorpt=p_adsorpt,
    final x_adsorpt=x_adsorpt,
    final T_adsorpt=T_adsorpt)
    "Thermodynamic properties and partial derivatives of a pure working pair";

  //
  // Definition of variables
  //
  Modelica.Units.SI.Pressure[no_isoLines] p_adsorpt
    "Pressure";
  Modelica.Units.SI.Temperature[no_isoLines] T_adsorpt
    "Temperature";
  SorpLib.Units.Uptake[no_isoLines] x_adsorpt
    "Uptake";

  //
  // Definition of final parameters
  //
protected
  final parameter Choices.IndependentVariablesPureComponentWorkingPair stateVariables=
    if diagramType==SorpLib.Choices.DiagramTypePureComponentWorkingPair.Isosters then
    SorpLib.Choices.IndependentVariablesPureComponentWorkingPair.xT else
    SorpLib.Choices.IndependentVariablesPureComponentWorkingPair.pT
    "Independent state variables of the working pair model";

  //
  // Definition of protected variables
  //
  Modelica.Units.SI.Pressure[no_isoLines] p_adsorpt_const_
    "Constant pressures";
  Modelica.Units.SI.Temperature[no_isoLines] T_adsorpt_const_
    "Constant temperatures";
  SorpLib.Units.Uptake[no_isoLines] x_adsorpt_const_
    "Constant uptakes";

equation
  //
  // Calculate constant variables
  //
  if isoLineType==SorpLib.Choices.ChangingConstantVariableOfDiagram.Manual then
    p_adsorpt_const_=p_adsorpt_const
      "Constant pressures";
    T_adsorpt_const_=T_adsorpt_const
      "Constant temperatures";
    x_adsorpt_const_=x_adsorpt_const
      "Constant uptakes";

  elseif isoLineType==SorpLib.Choices.ChangingConstantVariableOfDiagram.Linespace then
    p_adsorpt_const_=linspace(p_adsorpt_min, p_adsorpt_max, no_isoLines)
      "Constant pressures";
    T_adsorpt_const_=linspace(T_adsorpt_min, T_adsorpt_max, no_isoLines)
      "Constant temperatures";
    x_adsorpt_const_=linspace(x_adsorpt_min, x_adsorpt_max, no_isoLines)
      "Constant uptakes";

  else
    p_adsorpt_const_=
      {exp(log(p_adsorpt_min) + (log(p_adsorpt_max) - log(p_adsorpt_min)) *
      (i - 1) / (no_isoLines - 1)) for i in 1:no_isoLines}
      "Constant pressures";
    T_adsorpt_const_=
      {exp(log(T_adsorpt_min) + (log(T_adsorpt_max) - log(T_adsorpt_min)) *
      (i - 1) / (no_isoLines - 1)) for i in 1:no_isoLines}
      "Constant temperatures";
    x_adsorpt_const_=
      {exp(log(x_adsorpt_min) + (log(x_adsorpt_max) - log(x_adsorpt_min)) *
      (i - 1) / (no_isoLines - 1)) for i in 1:no_isoLines}
      "Constant uptakes";

  end if;

  //
  // Annotations
  //
  annotation (experiment(
      StopTime=20,
      Tolerance=1e-06),Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    Documentation(info="<html>
<p>
This partial model is the basic model for all models creating fluid propertey diagrams
for working pair models describing adsorption of pure components.
<br/><br/>
Models that inherit properties from this partial model have to specify the working pair
model <i>PureWorkingPairModel</i> and the test setup. Furthermore, equations must be
added to calculate the inputs <i>p_adsorpt</i>, <i>T_adsorpt</i>, and <i>x_adsorpt</i>
depending on the diagram type (i.e., just two inputs are needed in each case).
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 23, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end PartialPureCharts;
