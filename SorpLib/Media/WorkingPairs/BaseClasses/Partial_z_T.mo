within SorpLib.Media.WorkingPairs.BaseClasses;
partial function Partial_z_T
  "Base function for calculating a coefficient as function of temperature"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.Temperature T
    "Temperature"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of outputs
  //
  output Real z
    "Coefficient"
    annotation (Dialog(tab="General", group="Outputs", enable=false));

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial function is the basic function for calculating a coefficient as 
function of temperature.
<br/><br/>
This partial function defines the temperature <i>T</i> as input and the abitrary
coefficient <i>c</i> as output.
<br/><br/>
Functions that inherit properties from this partial function may have to add
furhter inputs, outputs, and the function algorithm.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Partial_z_T;
