within SorpLib.Media.WorkingPairs.BaseClasses;
partial function Partial_ddz_dT_dT
  "Base function for calculating the second-order partial derivative of coefficients w.r.t. temperature"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.Temperature T
    "Temperature"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of outputs
  //
  output Real ddz_dT_dT
    "Second-order partial derivative of coefficient w.r.t. temperature"
    annotation (Dialog(tab="General", group="Outputs", enable=false));

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial function is the basic function for calculating the second-order 
partial derivative of a coefficient with respect to temperature as function of 
temperature.
<br/><br/>
This partial function defines the temperature <i>T</i> as input and the second-
order partial derivative of the coefficient with respect to temperature <i>ddz_dT_dT</i>
as output.
<br/><br/>
Functions that inherit properties from this partial function may have to add
furhter inputs, outputs, and the function algorithm.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Partial_ddz_dT_dT;
