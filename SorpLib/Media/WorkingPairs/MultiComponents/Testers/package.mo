within SorpLib.Media.WorkingPairs.MultiComponents;
package Testers "Models to test and varify models of multi-component working pairs"
  extends Modelica.Icons.ExamplesPackage;

  annotation (Documentation(info="<html>
<p>
This package contains executable test models for all implemented working pair model. 
Each working pair model has its own test model that is saved in the correct adsorptive
package.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Testers;
