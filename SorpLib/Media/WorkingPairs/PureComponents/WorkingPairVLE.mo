within SorpLib.Media.WorkingPairs.PureComponents;
model WorkingPairVLE
  "Model of a pure working pair using the MSL for fluid property data calculation of a real fluid with a two-phase regime"

  //
  // Definition of replaceable medium
  //
  replaceable package Medium = Modelica.Media.Water.StandardWater
    constrainedby Modelica.Media.Interfaces.PartialTwoPhaseMedium
    "Medium (i.e., adsorptive)"
    annotation (Dialog(tab="General",group="Working Pair and Medium"),
                choicesAllMatching = true);

  //
  // Finalize base classe
  //
  extends SorpLib.Media.WorkingPairs.BaseClasses.PartialPureWorkingPairs(
    redeclare replaceable package WorkingPair =
      Parametrizations.PureComponents.H2O.SilicaGel123_DubininLorentzianCumulative_Schawe2000
        (
        MediumSpecificFunctions(redeclare replaceable package Medium = Medium)));

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This model calculates the thermodynamic properties of a working pair using
a real fluid with a two-phase regime as medium model for the adsorptive. Note
that isotherm models based on the model of Dubinin must have a real fluid. For
details on the working pair model, check the model
<a href=\"Modelica://SorpLib.Media.WorkingPairs.BaseClasses.PartialPureWorkingPairs\">SorpLib.Media.WorkingPairs.BaseClasses.PartialPureWorkingPairs</a>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end WorkingPairVLE;
