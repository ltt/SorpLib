within SorpLib.Media.WorkingPairs.PureComponents.H2O;
model SilicaGel_Toth_WangDouglasLeVan2009_VLE
  "H2O & Zeolith 5A via the Toth isotherm model according to Wang and Douglas LeVan (2010)"
  extends SorpLib.Media.WorkingPairs.PureComponents.WorkingPairVLE(
    redeclare replaceable package Medium =
      Modelica.Media.Water.StandardWater,
    redeclare final package WorkingPair =
      SorpLib.Media.WorkingPairs.Parametrizations.PureComponents.H2O.SilicaGel_Toth_WangDouglasLeVan2009
        (redeclare final package MediumSpecificFunctions =
          SorpLib.Media.WorkingPairs.Parametrizations.PureComponents.MediumSpecificFunctions.VLE
            (
          redeclare final package Medium = Medium)),
    v_adsorpt_constant=1/947,
    cp_adsorpt_constant=4.3e3,
    h_ads_constant=2750e3);

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This model calculates the adsorption equilibrium and thermodynamic properties of 
the working pair H<sub>2</sub>0 & silica gel using the Toth isotherm model according 
to Wang and Douglas LeVan (2010).
</p>

<h4>References</h4>
<ul>
  <li>
  Wang, Y. and Douglas LeVan, M. (2009). Adsorption Equilibrium of Carbon Dioxide and Water Vapor on Zeolites 5A and 13X and Silica Gel: Pure Components, Journal of Chemical & Engineering Data, 54:2839-2844. DOI: https://doi.org/10.1021/je800900a.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end SilicaGel_Toth_WangDouglasLeVan2009_VLE;
