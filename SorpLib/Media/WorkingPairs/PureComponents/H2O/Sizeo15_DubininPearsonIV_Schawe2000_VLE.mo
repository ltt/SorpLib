within SorpLib.Media.WorkingPairs.PureComponents.H2O;
model Sizeo15_DubininPearsonIV_Schawe2000_VLE
  "H2O & Sizeo 15 via the Dubinin isotherm model with a Pearson IV approach according to Schawe (2000)"
  extends SorpLib.Media.WorkingPairs.PureComponents.WorkingPairVLE(
    redeclare replaceable package Medium =
      Modelica.Media.Water.StandardWater,
    redeclare final package WorkingPair =
      SorpLib.Media.WorkingPairs.Parametrizations.PureComponents.H2O.Sizeo15_DubininPearsonIV_Schawe2000
        (redeclare final package MediumSpecificFunctions =
          SorpLib.Media.WorkingPairs.Parametrizations.PureComponents.MediumSpecificFunctions.VLE
            (
          redeclare final package Medium = Medium)),
    v_adsorpt_constant=1/947,
    cp_adsorpt_constant=4.3e3,
    h_ads_constant=2750e3);

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This model calculates the adsorption equilibrium and thermodynamic properties of 
the working pair H<sub>2</sub>0 & Sizeo 15 using the Dubinin isotherm model 
with a Pearson IV characteristic curve according to Schawe (2000).
</p>

<h4>References</h4>
<ul>
  <li>
  Schawe (2000). Theoretical and Experimental Investigations of an Adsorption Heat Pump with Heat Transfer between two Adsorbers, PhD thesis. DOI: http://dx.doi.org/10.18419/opus-1518.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Sizeo15_DubininPearsonIV_Schawe2000_VLE;
