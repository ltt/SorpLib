within SorpLib.Media.WorkingPairs.PureComponents.Testers;
model TestWorkingPairVLE
  "Tester for working pair models using a real fluid with a two-phase regime based on the MSL"
  extends SorpLib.Media.WorkingPairs.BaseClasses.PartialPureTest(
    redeclare replaceable model PureWorkingPairModel =
      SorpLib.Media.WorkingPairs.PureComponents.WorkingPairVLE
      constrainedby SorpLib.Media.WorkingPairs.PureComponents.WorkingPairVLE);

  //
  // Annotations
  //
  annotation (experiment(
      StopTime=20,
      Tolerance=1e-06),
Documentation(info="<html>
<p>
This model is the basic model for all testers of working pair models describing 
adsorption of pure components and using a medium model of a fluid with a two-phase 
regime based on the Modelica Standard Library.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end TestWorkingPairVLE;
