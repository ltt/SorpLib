within SorpLib.Media.WorkingPairs.PureComponents.Testers.CO2;
model Test_ActivatedCarbon_Toth_DantasEtAl2011_Gas
  "Tester for the working pair CO2 & Activated carbon via the Toth isotherm model according to Dantas et al. (2011)"
  extends
    SorpLib.Media.WorkingPairs.PureComponents.Testers.TestWorkingPairGas(
      stateVariables=SorpLib.Choices.IndependentVariablesPureComponentWorkingPair.pT,
      T_adsorpt_der=0,
      p_adsorpt_der=(3e5-1)/20,
      T_adsorpt_start=301,
      p_adsorpt_start=1,
      redeclare final model PureWorkingPairModel =
        SorpLib.Media.WorkingPairs.PureComponents.CO2.ActivatedCarbon_Toth_DantasEtAl2011_Gas
        (  approachSorptionEnthalpy=SorpLib.Choices.SorptionEnthalpy.Formal));

  //
  // Annotations
  //
  annotation (experiment(
      StopTime=20,
      Tolerance=1e-06),
Documentation(info="<html>
<p>
This tester shows the behavior of the working pair CO<sub>2</sub> & activated 
carbon using the Toth isotherm model according to Dantas et al. (2011).
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Test_ActivatedCarbon_Toth_DantasEtAl2011_Gas;
