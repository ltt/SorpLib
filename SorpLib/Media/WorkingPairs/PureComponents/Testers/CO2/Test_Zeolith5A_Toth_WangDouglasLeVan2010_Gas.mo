within SorpLib.Media.WorkingPairs.PureComponents.Testers.CO2;
model Test_Zeolith5A_Toth_WangDouglasLeVan2010_Gas
  "Tester for the working pair CO2 & Zeolith 5A via the Toth isotherm model according to Wang and Douglas LeVan (2010)"
  extends
    SorpLib.Media.WorkingPairs.PureComponents.Testers.TestWorkingPairGas(
      stateVariables=SorpLib.Choices.IndependentVariablesPureComponentWorkingPair.pT,
      T_adsorpt_der=0,
      p_adsorpt_der=(1e5-1)/20,
      T_adsorpt_start=273.15-45,
      p_adsorpt_start=1,
      redeclare final model PureWorkingPairModel =
        SorpLib.Media.WorkingPairs.PureComponents.CO2.Zeolith5A_Toth_WangDouglasLeVan2010_Gas
        (  approachSorptionEnthalpy=SorpLib.Choices.SorptionEnthalpy.Formal));

  //
  // Annotations
  //
  annotation (experiment(
      StopTime=20,
      Tolerance=1e-06),
Documentation(revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>", info="<html>
<p>
This tester shows the behavior of the working pair  CO<sub>2</sub> & Zeolith 5A 
using the Toth isotherm model according to Wang and Douglas LeVan (2009, 2010).
<br/><br/>
As an example, this tester increases the uptake and temperature with time. The 
simulation time is correctly preset (Start: 0 s, Stop = 20 s).
</p>
</html>"));
end Test_Zeolith5A_Toth_WangDouglasLeVan2010_Gas;
