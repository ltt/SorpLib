within SorpLib.Media.WorkingPairs.PureComponents.Testers.O2;
model Test_ZeolithLiLSX_Langmuir_DantasEtAl2011_Gas
  "Tester for the working pair O2 & Zeolith LiLSX via the Langmuir isotherm model according to Li et al. (2021)"
  extends SorpLib.Media.WorkingPairs.PureComponents.Testers.TestWorkingPairGas(
    stateVariables=SorpLib.Choices.IndependentVariablesPureComponentWorkingPair.pT,
    T_adsorpt_der=0,
    p_adsorpt_der=(7e5 - 1)/20,
    T_adsorpt_start=293.15,
    p_adsorpt_start=1,
    redeclare final model PureWorkingPairModel =
        SorpLib.Media.WorkingPairs.PureComponents.O2.ZeolithLiLSX_Langmuir_DantasEtAl2011_Gas
        (approachSorptionEnthalpy=SorpLib.Choices.SorptionEnthalpy.Formal));

  //
  // Annotations
  //
  annotation (experiment(
      StopTime=20,
      Tolerance=1e-06),
Documentation(info="<html>
<p>
This tester shows the behavior of the working pair N<sub>2</sub> & Zeolith LiLSX
using the Toth isotherm model according to Dantas et al. (2011).
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Test_ZeolithLiLSX_Langmuir_DantasEtAl2011_Gas;
