within SorpLib.Media.WorkingPairs.PureComponents.Testers;
package O2 "Model to test and varify working pair models that use O2 as adsorptive"
extends Modelica.Icons.ExamplesPackage;

annotation (Documentation(info="<html>
<p>
This package contains executable test models for all implemented working pair models
using O<sub>2</sub> as adsorptive. 
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end O2;
