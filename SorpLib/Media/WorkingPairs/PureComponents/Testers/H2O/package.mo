within SorpLib.Media.WorkingPairs.PureComponents.Testers;
package H2O "Model to test and varify working pair models that use H2O as adsorptive"
extends Modelica.Icons.ExamplesPackage;

annotation (Documentation(info="<html>
<p>
This package contains executable test models for all implemented working pair models
using H<sub>2</sub> as adsorptive. 
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end H2O;
