﻿within SorpLib.Media.WorkingPairs.PureComponents.CO2;
model ActivatedCarbon_Toth_DantasEtAl2011_Gas
  "CO2 & Activated carbon via the Toth isotherm model according to Dantas et al. (2011)"
  extends SorpLib.Media.WorkingPairs.PureComponents.WorkingPairGas(
    redeclare replaceable package Medium =
      SorpLib.Media.IdealGases.CO2,
    redeclare final package WorkingPair =
      SorpLib.Media.WorkingPairs.Parametrizations.PureComponents.CO2.ActivatedCarbon_Toth_DantasEtAl2011
        (redeclare final package MediumSpecificFunctions =
          SorpLib.Media.WorkingPairs.Parametrizations.PureComponents.MediumSpecificFunctions.Gas
            (
          redeclare final package Medium = Medium)),
    redeclare replaceable model Sorbent =
      Solids.Sorbents.GenericSorbent (
        v_constant=1/1138,
        c_constant=880,
        lambda_constant=0.075),
    v_adsorpt_constant=1/990,
    cp_adsorpt_constant=2.5e3,
    h_ads_constant=496e3);

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This model calculates the adsorption equilibrium and thermodynamic properties of 
the working pair CO<sub>2</sub> & activated carbon using the Toth isotherm model 
according to Dantas et al. (2011).
</p>

<h4>References</h4>
<ul>
  <li>
  Dantas, T.L.P. and Luna, F.M.T. and Silva Jr., I.J. and Azevedo, D.C.S. and Grande, C.A. and Rodrigues, A.E. and Moreira, R.F.P.M. (2011). Carbon dioxide–nitrogen separation through adsorption on activated carbon in a fixed bed, Chemical Engineering Journal, 169: 11–19. DOI: https://doi.org/10.1016/j.cej.2010.08.026.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end ActivatedCarbon_Toth_DantasEtAl2011_Gas;
