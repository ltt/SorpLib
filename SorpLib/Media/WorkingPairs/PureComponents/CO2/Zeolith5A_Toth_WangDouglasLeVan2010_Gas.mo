﻿within SorpLib.Media.WorkingPairs.PureComponents.CO2;
model Zeolith5A_Toth_WangDouglasLeVan2010_Gas
  "CO2 & Zeolith 5A via the Toth isotherm model according to Wang and Douglas LeVan (2010)"
  extends SorpLib.Media.WorkingPairs.PureComponents.WorkingPairGas(
    redeclare replaceable package Medium =
      SorpLib.Media.IdealGases.CO2,
    redeclare final package WorkingPair =
      SorpLib.Media.WorkingPairs.Parametrizations.PureComponents.CO2.Zeolith5A_Toth_WangDouglasLeVan2010
        (redeclare final package MediumSpecificFunctions =
          SorpLib.Media.WorkingPairs.Parametrizations.PureComponents.MediumSpecificFunctions.Gas
            (
          redeclare final package Medium = Medium)),
    redeclare replaceable model Sorbent =
      Solids.Sorbents.GenericSorbent (
        v_constant=1/670,
        c_constant=920,
        lambda_constant=0.085),
    v_adsorpt_constant=1/990,
    cp_adsorpt_constant=2.5e3,
    h_ads_constant=496e3);

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This model calculates the adsorption equilibrium and thermodynamic properties of 
the working pair CO<sub>2</sub> & Zeolith 5A using the Toth isotherm model 
according to Wang and Douglas LeVan (2010).
</p>

<h4>References</h4>
<ul>
  <li>
  Wang, Y. and Douglas LeVan, M. (2009). Adsorption Equilibrium of Carbon Dioxide and Water Vapor on Zeolites 5A and 13X and Silica Gel: Pure Components, Journal of Chemical & Engineering Data, 54:2839-2844. DOI: https://doi.org/10.1021/je800900a.
  </li>
  <li>
  Wang, Y. and Douglas LeVan, M. (2010). Adsorption Equilibrium of Binary Mixtures of Carbon Dioxide and Water Vapor on Zeolites 5A and 13X, Hournal of Chemical & Engineering Data, 55(9):3189–3195. DOI: https://doi.org/10.1021/je100053g.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Zeolith5A_Toth_WangDouglasLeVan2010_Gas;
