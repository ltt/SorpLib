within SorpLib.Media.WorkingPairs.PureComponents.N2;
model ZeolithLiLSX_Langmuir_DantasEtAl2011_Gas
  "N2 & Zeolith LiLSX via the Langmuir isotherm model according to Li et al. (2021)"
  extends SorpLib.Media.WorkingPairs.PureComponents.WorkingPairGas(
    redeclare replaceable package Medium =
      SorpLib.Media.IdealGases.N2,
    redeclare final package WorkingPair =
      SorpLib.Media.WorkingPairs.Parametrizations.PureComponents.N2.ZeolithLiLSX_Langmuir_LiEtAl2021
        (redeclare final package MediumSpecificFunctions =
          SorpLib.Media.WorkingPairs.Parametrizations.PureComponents.MediumSpecificFunctions.Gas
            (
          redeclare final package Medium = Medium)),
    redeclare replaceable model Sorbent = Solids.Sorbents.GenericSorbent (
      v_constant=1/1940,
      c_constant=920,
      lambda_constant=0.085),
    v_adsorpt_constant=1/730,
    cp_adsorpt_constant=2.4e3,
    h_ads_constant=582e3);

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This model calculates the adsorption equilibrium and thermodynamic properties of 
the working pair N<sub>2</sub> & Zeolith LiLSX using the Langmuir isotherm model according 
to Li et al. (2011).
</p>

<h4>References</h4>
<ul>
  <li>
  Li, L., and Yu, M., and Zi, Y., and Dang, Y., and Wu, Q., and Wang, Z., and Xu, Y., and Yan, H, and Dang, Y. (2021). A Thermodynamic Model for Pure and Binary Adsorption Equilibria of N<sub>2</sub> and O<sub>2</sub> on Lithium-Exchanged Low Silicon-to-Aluminum Ratio X Zeolite, Journal of Chemical & Engineering Data, 60:1032-1042. DOI: https://dx.doi.org/10.1021/acs.jced.0c00830.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end ZeolithLiLSX_Langmuir_DantasEtAl2011_Gas;
