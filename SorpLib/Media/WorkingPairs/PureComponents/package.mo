within SorpLib.Media.WorkingPairs;
package PureComponents "Package containing pure component working pair models"
  extends Modelica.Icons.VariantsPackage;

  annotation (Documentation(info="<html>
<p>
This package contains fully parameterized working models. For this purpose, 
the partially parametrized pure working pair models are extended by a specific 
<i>MediumSpecificFunctions</i> package. This package calculates fluid properties 
based on the open-source Modelica Standard Library (MSL).
</p>

<h4>Structure</h4>
<p>
The package 
<a href=\"Modelica://SorpLib.Media.WorkingPairs.PureComponents.MediumSpecificFunctions\">SorpLib.Media.WorkingPairs.PureComponents.MediumSpecificFunctions</a>
contains different versions of packages providing medium specific functions. For 
each package version there is also a corresponding working pair model. The finally
parametrized working pair models are alphabetically sorted by available asdsorptives 
and are stored in seperate packages. The naming convention for the parameterized working 
pair models is as follows
</p>
<pre>
    NameSorbent_NameIsothermModel_NameAuthorsYear_MediumType;
</pre>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end PureComponents;
