within SorpLib.Media.WorkingPairs.PureComponents;
model WorkingPairGas
  "Model of a pure working pair using the MSL for fluid property data calculation of a fluid with just one-regime (i.e., gas/vapor)"

  //
  // Definition of replaceable medium
  //
  replaceable package Medium = SorpLib.Media.IdealGases.CO2
    constrainedby Modelica.Media.IdealGases.Common.SingleGasNasa
    "Medium (i.e., adsorptive)"
    annotation (Dialog(tab="General",group="Working Pair and Medium"),
                choicesAllMatching = true);

  //
  // Finalize base classe
  //
  extends SorpLib.Media.WorkingPairs.BaseClasses.PartialPureWorkingPairs(
    redeclare replaceable package WorkingPair =
      SorpLib.Media.WorkingPairs.Parametrizations.PureComponents.CO2.ActivatedCarbon_Toth_DantasEtAl2011
        (
      MediumSpecificFunctions(redeclare replaceable package Medium = Medium)));

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This model calculates the thermodynamic properties of a working pair using
a fluid with just one regime (i.e., gas/vapor) as medium model for the adsorptive.
For details on the working pair model, check the model
<a href=\"Modelica://SorpLib.Media.WorkingPairs.BaseClasses.PartialPureWorkingPairs\">SorpLib.Media.WorkingPairs.BaseClasses.PartialPureWorkingPairs</a>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end WorkingPairGas;
