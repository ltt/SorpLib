within SorpLib;
package Media "Library of media property models"
  extends SorpLib.Icons.MediaPackage;

annotation (Documentation(info="<html>
<p>
This package contains functions required for calculating substance property data. 
The functions are mainly used for calculating the adsorption equilibrium or 
properties of a sorption working pair. This package contains the following 
substance models:
</p>
<ul>
  <li>
  <a href=\"Modelica://SorpLib.Media.IdealGases\">IdealGases</a>: 
  Models for calculating ideal gases.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Media.IdealGasMixtures\">IdealGasMixtures</a>: 
  Models for calculating ideal gas mixtures.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Media.IdealGasVaporMixtures\">IdealGasVaporMixtures</a>: 
  Models for calculating ideal gas-vapor mixtures, i.e., ideal gas mixtures with a condensable substance (usually water).
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Media.Solids\">Solids</a>: 
  Models for calculating solids, such as metals or sorbents.
  </li>
  <li>
  <a href=\"Modelica://SorpLib.Media.WorkingPairs\">WorkingPairs</a>: 
  Models for calculating sorption working pairs, either with one or multi components.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end Media;
