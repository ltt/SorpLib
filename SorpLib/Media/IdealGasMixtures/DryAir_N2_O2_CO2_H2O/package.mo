within SorpLib.Media.IdealGasMixtures;
package DryAir_N2_O2_CO2_H2O "SorpLib: Simple dry air consisting of N2, O2, CO2, and H2O"
  extends SorpLib.Media.IdealGasMixtures.Interfaces.PartialIdealGasMixture(
    mediumName="DryAir_N2_O2_CO2_H2O",
    data={Modelica.Media.IdealGases.Common.SingleGasesData.N2,
          Modelica.Media.IdealGases.Common.SingleGasesData.O2,
          Modelica.Media.IdealGases.Common.SingleGasesData.CO2,
          Modelica.Media.IdealGases.Common.SingleGasesData.H2O},
    fluidConstants={Modelica.Media.IdealGases.Common.FluidData.N2,
                    Modelica.Media.IdealGases.Common.FluidData.O2,
                    Modelica.Media.IdealGases.Common.FluidData.CO2,
                    Modelica.Media.IdealGases.Common.FluidData.H2O},
    substanceNames={"Nitrogen",
                    "Oxygen",
                    "Carbondioxide",
                    "Water"},
    final reference_X={0.7650,0.2344,5.90e-4,1e-5});

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This model calculates the fluid property of dry air modeled as ideal
gas mixture of N<sub>2</sub>, O<sub>2</sub>, CO<sub>2</sub>, and H<sub>2</sub>0.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 24, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end DryAir_N2_O2_CO2_H2O;
