within SorpLib.Media;
package IdealGasMixtures "Package containing medium models of ideal gas mixtures"
  extends Modelica.Icons.VariantsPackage;

  annotation (Documentation(info="<html>
<p>
This package contains models of ideal gas mixtures. These models are based
on the Modelica Standard Library but extended by new functions required in
SorpLib. Note that the reference temperature must be 0 K and the reference 
enthalpy and entropy must be 0 J/kg and 0 J/(Kg.K). Otherwise, absolute values 
caloric and entropic properties may not be correctly calculated. For details 
of the mixture models, check the documentation of the package 
<a href=\"Modelica://Modelica.Media.IdealGases.Common.MixtureGasNasa\">Modelica.Media.IdealGases.Common.MixtureGasNasa</a>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end IdealGasMixtures;
