within SorpLib.Media.IdealGasMixtures;
package DryAir_N2_O2_Ar_CO2_Ne_He_CH4_H2O "SorpLib: Simple dry air consisting of N2, O2, Ar, CO2, Ne, He, CH4, and H2O"
  extends SorpLib.Media.IdealGasMixtures.Interfaces.PartialIdealGasMixture(
    mediumName="DryAir_N2_O2_Ar_CO2_Ne_He_CH4_H2O",
    data={Modelica.Media.IdealGases.Common.SingleGasesData.N2,
          Modelica.Media.IdealGases.Common.SingleGasesData.O2,
          Modelica.Media.IdealGases.Common.SingleGasesData.Ar,
          Modelica.Media.IdealGases.Common.SingleGasesData.CO2,
          Modelica.Media.IdealGases.Common.SingleGasesData.Ne,
          Modelica.Media.IdealGases.Common.SingleGasesData.He,
          Modelica.Media.IdealGases.Common.SingleGasesData.CH4,
          Modelica.Media.IdealGases.Common.SingleGasesData.H2O},
    fluidConstants={Modelica.Media.IdealGases.Common.FluidData.N2,
                    Modelica.Media.IdealGases.Common.FluidData.O2,
                    Modelica.Media.IdealGases.Common.FluidData.Ar,
                    Modelica.Media.IdealGases.Common.FluidData.CO2,
                    Modelica.Media.IdealGases.Common.FluidData.Ne,
                    Modelica.Media.IdealGases.Common.FluidData.He,
                    Modelica.Media.IdealGases.Common.FluidData.CH4,
                    Modelica.Media.IdealGases.Common.FluidData.H2O},
    substanceNames={"Nitrogen",
                    "Oxygen",
                    "Argon",
                    "Carbondioxide",
                    "Neon",
                    "Helium",
                    "Methane",
                    "Water"},
    final reference_X={0.7552/1.00001439,0.2313/1.00001439,
                       0.0129/1.00001439,5.90e-4/1.00001439,
                       1.27e-05/1.00001439,7.20e-07/1.00001439,
                       9.70e-07/1.00001439,1e-5/1.00001439});

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This model calculates the fluid property of dry air modeled as ideal
gas mixture of N<sub>2</sub>, O<sub>2</sub>, Ar, CO<sub>2</sub>, Ne, He,
CH<sub>4</sub>, and H<sub>2</sub>0.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 24, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end DryAir_N2_O2_Ar_CO2_Ne_He_CH4_H2O;
