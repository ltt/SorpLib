within SorpLib.Media.IdealGasMixtures;
package Interfaces "Interfaces for models of ideal gas mixtures"
extends Modelica.Icons.InterfacesPackage;

annotation (Documentation(info="<html>
<p>
This package provides definitions of basic interfaces for models
of ideal gas mixtures.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Interfaces;
