within SorpLib.Media.IdealGasMixtures.Interfaces.PartialIdealGasMixture;
function partialPressures
  "Returns partial pressures"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input ThermodynamicState state
    "Thermodynamic state record"
    annotation (Dialog(tab="General",group="Inputs"));

  //
  // Definition of outputs
  //
  output Modelica.Units.SI.Pressure[nX] p_i
    "Partial pressures"
    annotation (Dialog(tab="General",group="Outputs", enable=false));

  //
  // Definition of variables
  //
protected
  Modelica.Units.SI.MoleFraction Y[nX]=
    massToMoleFractions(X=state.X, MMX=MMX)
    "Mole fractions";

algorithm
  p_i := Y .* state.p
    "Partial pressures";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
  <p>
  This function calculates the partial pressures as function of the state record.
  </p>
  </html>",
          revisions="<html>
  <ul>
    <li>
    November 24, 2023, by Mirko Engelpracht:<br/>
    First implementation.
    </li>
  </ul>
  </html>"));
end partialPressures;
