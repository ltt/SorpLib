within SorpLib.Media.IdealGasMixtures.Interfaces.PartialIdealGasMixture;
function specificEntropy_i_pTX
  "Returns specific entropy of component i"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Integer ind_component
    "Component index whose specific entropy is to be calculated"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.Pressure p
    "Pressure"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.Temperature T
    "Temperature"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.MassFraction[nX] X
    "Mass fractions of mixture (full vector)"
    annotation (Dialog(tab="General", group="Inputs"));

  input Boolean exclEnthForm=excludeEnthalpyOfFormation
    "= true, enthalpy of formation Hf is not included in specific enthalpy h"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Media.Interfaces.Choices.ReferenceEnthalpy refChoice=
    referenceChoice
    "Choice of reference enthalpy"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.SpecificEnthalpy h_off = h_offset
    "User definedreference enthalpy, if referenceChoice = UserDefined"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of outputs
  //
  output Modelica.Units.SI.SpecificEntropy s
    "Specific entropy of component ind_component"
    annotation (Dialog(tab="General", group="Outputs", enable=false));

  //
  // Definition of variables
  //
protected
    Modelica.Units.SI.MoleFraction[nX] Y = massToMoleFractions(X, data.MM)
    "Molar fractions";

algorithm
  s := Modelica.Media.IdealGases.Common.Functions.s0_T(data[ind_component],
    T) - Modelica.Constants.R/MMX[ind_component] * (if X[ind_component] <
    Modelica.Constants.eps then Y[ind_component] else
    Modelica.Math.log(Y[ind_component]*p / reference_p))
    "Specific entropy of component ind_component";

  //
  // Annotations
  //
  annotation(Inline=false,smoothOrder=2,
    Documentation(info="<html>
<p>
This function calculates the specific entropy of component i.
</p>
</html>",
        revisions="<html>
<ul>
  <li>
  November 24, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end specificEntropy_i_pTX;
