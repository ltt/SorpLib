within SorpLib.Media.IdealGasMixtures.Interfaces.PartialIdealGasMixture;
function ddsaturationPressureH2O_dT_dT
  "Returns second-order partial derivative of saturatation pressure of water w.r.t. temperature as function of temperature"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.Temperature T_sat
    "Saturation temperature"
    annotation (Dialog(tab="General",group="Inputs"));

  //
  // Definition of outputs
  //
  output SorpLib.Units.DerPressureByTemperatureTemperature ddp_sat_dT_dT
    "Second-order partial derivative of saturation pressure w.r.t. temperature"
    annotation (Dialog(tab="General",group="Outputs", enable=false));

  //
  // Definition of constants
  //
protected
  constant Modelica.Units.SI.Pressure p_trp = 611.657
    "Triple point pressure";
  constant Modelica.Units.SI.Pressure p_crit = 22.064e6
    "Critical pressure";

  constant Modelica.Units.SI.Temperature T_trp = 273.16
    "Triple point temperature";
  constant Modelica.Units.SI.Temperature T_crit = 647.096
    "Critical temperature";

  constant Real[2] coeff_s = {-13.9281690, 34.7078238}
    "Coefficients of the gas-solid boundary";
  constant Real[6] coeff_l = {-7.85951783, 1.84408259, -11.7866497,
    22.6807411, -15.9618719, 1.80122502}
    "Coefficients of the gas-liquid boundary";

  constant Real[2] exp_s = {-1.5, -1.25}
    "Exponents of the gas-solid boundary";
  constant Real[6] exp_l = {1, 1.5, 3,
    3.5, 4, 7.5}
    "Exponents of the gas-liquid boundary";

  //
  // Definition of variables
  //
  Modelica.Units.SI.Pressure p_sat_s
    "Saturation pressure of gas-solid boundary";
  Modelica.Media.Common.DerPressureByTemperature dp_sat_s_dT
    "First-order partial derivative of saturation pressure of gas-solid boundary
    w.r.t. temperature";
  SorpLib.Units.DerPressureByTemperatureTemperature ddp_sat_s_dT_dT
    "Second-order partial derivative of saturation pressure of gas-solid boundary
    w.r.t. temperature";

  Modelica.Units.SI.Pressure p_sat_l
    "Saturation pressure of gas-liquid boundary";
  Modelica.Media.Common.DerPressureByTemperature dp_sat_l_dT
    "First-order partial derivative of saturation pressure of gas-liquid boundary
    w.r.t. temperature";
  SorpLib.Units.DerPressureByTemperatureTemperature ddp_sat_l_dT_dT
    "Second-order partial derivative of saturation pressure of gas-liquid boundary
    w.r.t. temperature";

  Real T_red_s = T_sat/T_trp
    "Reduced temperature required for gas-solid boundary";
  Real T_red_fl = 1 - T_sat/T_crit
    "Reduced temperature required for gas-liquid boundary";

  Real lambda(unit="1") = SorpLib.Numerics.smoothTransition(
    x=T_sat, transitionPoint=T_trp, transitionLength=1, noDiff=3)
    "Transiation factor";
  Real dlambda_dT(unit="1/K") = SorpLib.Numerics.smoothTransition_der(
    x=T_sat, transitionPoint=T_trp, transitionLength=1, noDiff=3,
    x_der=1)
    "First-order derivative of transiation factor w.r.t. temperature";
  Real ddlambda_dT_dT(unit="1/(K2)") = SorpLib.Numerics.smoothTransition_der2(
    x=T_sat, transitionPoint=T_trp, transitionLength=1, noDiff=3,
    x_der=1, x_der2=0)
    "Second-order derivative of transiation factor w.r.t. temperature";

  Real aux_s_1
    "Auxiliary variable 1 of saturation pressure of gas-solid boundary";
  Real aux_s_2
    "Auxiliary variable 2 of saturation pressure of gas-solid boundary";

  Real aux_l_1
    "Auxiliary variable 1 of saturation pressure of gas-liquid boundary";
  Real aux_l_2
    "Auxiliary variable 2 of saturation pressure of gas-liquid boundary";

algorithm
  //
  // Calculate pressures
  //
  p_sat_s := p_trp * exp(coeff_s[1] - coeff_s[1] * T_red_s ^ exp_s[1] +
    coeff_s[2] - coeff_s[2] * T_red_s ^ exp_s[2])
    "Saturation pressure of gas-solid boundary";
  p_sat_l := p_crit * exp(T_crit/T_sat *
    (coeff_l[1] * T_red_fl ^ exp_l[1] +
    coeff_l[2] * T_red_fl ^ exp_l[2] +
    coeff_l[3] * T_red_fl ^ exp_l[3] +
    coeff_l[4] * T_red_fl ^ exp_l[4] +
    coeff_l[5] * T_red_fl ^ exp_l[5] +
    coeff_l[6] * T_red_fl ^ exp_l[6]))
    "Saturation pressure of gas-liquid boundary";

  //
  // Calculate auxiliary variables
  //
  aux_s_1 := -p_sat_s/T_sat
    "Auxiliary variable 1 of saturation pressure of gas-solid boundary";
  aux_s_2 := (coeff_s[1] * exp_s[1] * T_red_s ^ exp_s[1] +
    coeff_s[2] * exp_s[2] * T_red_s ^ exp_s[2])
    "Auxiliary variable 2 of saturation pressure of gas-solid boundary";

  aux_l_1 := -p_sat_l/T_sat
    "Auxiliary variable 1 of saturation pressure of gas-liquid boundary";
  aux_l_2 := log(p_sat_l/p_crit) + coeff_l[1] +
    coeff_l[2] * exp_l[2] * T_red_fl^(exp_l[2]-1) +
    coeff_l[3] * exp_l[3] * T_red_fl^(exp_l[3]-1) +
    coeff_l[4] * exp_l[4] * T_red_fl^(exp_l[4]-1) +
    coeff_l[5] * exp_l[5] * T_red_fl^(exp_l[5]-1) +
    coeff_l[6] * exp_l[6] * T_red_fl^(exp_l[6]-1)
    "Auxiliary variable 2 of saturation pressure of gas-liquid boundary";

  //
  // Calculate first-order partial derivatives of pressures w.r.t. temperature
  //
  dp_sat_s_dT := aux_s_1 * aux_s_2
    "First-order partial derivative of saturation pressure of gas-solid boundary
    w.r.t. temperature";
  dp_sat_l_dT := aux_l_1 * aux_l_2
    "First-order partial derivative of saturation pressure of gas-liquid boundary
    w.r.t. temperature";

  //
  // Calculate second-order partial derivatives of pressures w.r.t. temperature
  //
  ddp_sat_s_dT_dT := (p_sat_s/T_sat^2 - dp_sat_s_dT/T_sat) * aux_s_2 +
    aux_s_1/T_trp * (coeff_s[1] * exp_s[1]^2 * T_red_s ^ (exp_s[1]-1) +
    coeff_s[2] * exp_s[2]^2 * T_red_s ^ (exp_s[2]-1))
    "Second-order partial derivative of saturation pressure of gas-solid boundary
    w.r.t. temperature";
  ddp_sat_l_dT_dT := (p_sat_l/T_sat^2 - dp_sat_l_dT/T_sat) * aux_l_2 +
    aux_l_1 * (1/p_sat_l * dp_sat_l_dT + (-1/T_crit) *
    (coeff_l[2] * exp_l[2] * (exp_l[2]-1) * T_red_fl^(exp_l[2]-2) +
    coeff_l[3] * exp_l[3] * (exp_l[3]-1) * T_red_fl^(exp_l[3]-2) +
    coeff_l[4] * exp_l[4] * (exp_l[4]-1) * T_red_fl^(exp_l[4]-2) +
    coeff_l[5] * exp_l[5] * (exp_l[5]-1) * T_red_fl^(exp_l[5]-2) +
    coeff_l[6] * exp_l[6] * (exp_l[6]-1) * T_red_fl^(exp_l[6]-2)))
    "Second-order partial derivative of saturation pressure of gas-liquid boundary
    w.r.t. temperature";

  //
  // Check for boundary
  //
  ddp_sat_dT_dT :=ddlambda_dT_dT*p_sat_s +
    2*dlambda_dT*dp_sat_s_dT +
    lambda*ddp_sat_s_dT_dT +
    (-ddlambda_dT_dT)*p_sat_l +
    2*(-dlambda_dT)*dp_sat_l_dT +
    (1-lambda)*ddp_sat_l_dT_dT
    "Second-order partial derivative of saturation pressure w.r.t. temperature";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function calculates the second-order partial saturation pressure of water 
with respect to temperature depending on the temperature. This functions covers 
both, the solid-gas and the gas-liquid boundary. For details, please check the
function
<a href=\"Modelica://SorpLib.Media.IdealGasMixtures.Interfaces.PartialIdealGasMixture.saturationPressureH2O_T\">SorpLib.Media.IdealGasMixtures.Interfaces.PartialIdealGasMixture.saturationPressureH2O_T</a>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 24, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end ddsaturationPressureH2O_dT_dT;
