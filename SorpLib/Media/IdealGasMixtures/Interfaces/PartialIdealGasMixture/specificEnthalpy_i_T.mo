within SorpLib.Media.IdealGasMixtures.Interfaces.PartialIdealGasMixture;
function specificEnthalpy_i_T
  "Returns specific enthalpy of component i"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Integer ind_component
    "Component index whose specific enthalpy is to be calculated"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.Temperature T
    "Temperature"
    annotation (Dialog(tab="General", group="Inputs"));

  input Boolean exclEnthForm=excludeEnthalpyOfFormation
    "= true, enthalpy of formation Hf is not included in specific enthalpy h"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Media.Interfaces.Choices.ReferenceEnthalpy refChoice=
    referenceChoice
    "Choice of reference enthalpy"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.SpecificEnthalpy h_off = h_offset
    "User definedreference enthalpy, if referenceChoice = UserDefined"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of outputs
  //
  output Modelica.Units.SI.SpecificEnthalpy h
    "Specific enthalpy of component ind_component"
    annotation (Dialog(tab="General", group="Outputs", enable=false));

algorithm
  h := Modelica.Media.IdealGases.Common.Functions.h_T(
    data[ind_component], T, exclEnthForm, refChoice, h_off)
    "Specific enthalpy of component ind_component";

  //
  // Annotations
  //
  annotation(Inline=false,smoothOrder=2,
    Documentation(info="<html>
<p>
This function calculates the specific enthalpy of component i.
</p>
</html>",
        revisions="<html>
<ul>
  <li>
  November 24, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end specificEnthalpy_i_T;
