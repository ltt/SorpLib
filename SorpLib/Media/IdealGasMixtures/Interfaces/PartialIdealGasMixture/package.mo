within SorpLib.Media.IdealGasMixtures.Interfaces;
partial package PartialIdealGasMixture "Medium model of an ideal gas mixture based on NASA source"
  extends Modelica.Media.IdealGases.Common.MixtureGasNasa(
    final reference_T(min=0) = 0,
    final reference_p = 1e5);

  //
  // New functions
  //
  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This model extends the medium model of an ideal gas mixture provided
in the Modelica Standard Library by some additional functions required
in SorpLib. For details of the original medium model, check out the
package
<a href=\"Modelica://Modelica.Media.IdealGases.Common.MixtureGasNasa\">Modelica.Media.IdealGases.Common.MixtureGasNasa</a>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 24, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end PartialIdealGasMixture;
