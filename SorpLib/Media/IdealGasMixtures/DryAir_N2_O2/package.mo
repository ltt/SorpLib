within SorpLib.Media.IdealGasMixtures;
package DryAir_N2_O2 "SorpLib: Simple dry air consisting of N2 and O2"
  extends SorpLib.Media.IdealGasMixtures.Interfaces.PartialIdealGasMixture(
    mediumName="DryAir_N2_O2",
    data={Modelica.Media.IdealGases.Common.SingleGasesData.N2,
          Modelica.Media.IdealGases.Common.SingleGasesData.O2},
    fluidConstants={Modelica.Media.IdealGases.Common.FluidData.N2,
                    Modelica.Media.IdealGases.Common.FluidData.O2},
    substanceNames={"Nitrogen",
                    "Oxygen"},
    final reference_X={0.7655,0.2345});

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This model calculates the fluid property of dry air modeled as ideal
gas mixture of N<sub>2</sub> and O<sub>2</sub>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 24, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end DryAir_N2_O2;
