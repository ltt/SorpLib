within SorpLib.Media.IdealGasMixtures.Tester;
model Test_DryAir_N2_O2_Ar_CO2_Ne_He_CH4_H2O
  "Tester for dry air consisting of N2, O2, Ar, CO2, Ne, He, CH4, and H2O"
  extends SorpLib.Media.IdealGasMixtures.Tester.Test_DryAir_N2_O2(
    redeclare package Medium =
      SorpLib.Media.IdealGasMixtures.DryAir_N2_O2_Ar_CO2_Ne_He_CH4_H2O);

  //
  // Annotations
  //
  annotation (experiment(StopTime=20, Tolerance=1e-06),
Documentation(info="<html>
<p>
This model checks the fluid property calculation of the ideal gas mixture of
N<sub>2</sub>, O<sub>2</sub>, Ar, CO<sub>2</sub>, Ne, He, CH<sub>4</sub>, and 
H<sub>2</sub>0.
<br/><br/>
To see the model behavior, plot the variables of the models over the 
time. The simulation time is correctly preset (Start: 0 s, Stop = 20 s). 
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Test_DryAir_N2_O2_Ar_CO2_Ne_He_CH4_H2O;
