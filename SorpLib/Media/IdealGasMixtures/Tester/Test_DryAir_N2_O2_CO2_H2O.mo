within SorpLib.Media.IdealGasMixtures.Tester;
model Test_DryAir_N2_O2_CO2_H2O
  "Tester for dry air consisting of N2, O2, CO2, and H2O"
  extends SorpLib.Media.IdealGasMixtures.Tester.Test_DryAir_N2_O2(
    redeclare package Medium =
      SorpLib.Media.IdealGasMixtures.DryAir_N2_O2_CO2_H2O);

  //
  // Annotations
  //
  annotation (experiment(StopTime=20, Tolerance=1e-06),
Documentation(info="<html>
<p>
This model checks the fluid property calculation of the ideal gas mixture of
N<sub>2</sub>, O<sub>2</sub>, CO<sub>2</sub>, and H<sub>2</sub>O.
<br/><br/>
To see the model behavior, plot the variables of the models over the 
time. The simulation time is correctly preset (Start: 0 s, Stop = 20 s). 
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Test_DryAir_N2_O2_CO2_H2O;
