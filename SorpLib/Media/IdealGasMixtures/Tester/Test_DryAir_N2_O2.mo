within SorpLib.Media.IdealGasMixtures.Tester;
model Test_DryAir_N2_O2 "Tester for dry air consisting of N2 and O2"
  extends Modelica.Icons.Example;

  //
  // Definition of paramters
  //
  replaceable package Medium = SorpLib.Media.IdealGasMixtures.DryAir_N2_O2
    constrainedby Modelica.Media.IdealGases.Common.MixtureGasNasa
    "Medium"
    annotation (Dialog(tab="General",group="Models and Media"),
                choicesAllMatching = true);
  package MediumReference = Modelica.Media.Air.ReferenceAir.Air_pT
    "Reference medium: Detailed dry air"
    annotation (Dialog(tab="General",group="Models and Media"));

  //
  // Definition of variables
  //
  Modelica.Units.SI.Pressure p(start=100, fixed=true)
    "Pressure";
  Modelica.Units.SI.Temperature T(start=253.15, fixed=true)
    "Temperature";
  Modelica.Units.SI.MassFraction[Medium.nX] X = Medium.reference_X
    "Mass fractions";

  Medium.BaseProperties medium_pTX
    "Base properties calculated with pressure, temperature, and mass fractions";
  MediumReference.BaseProperties mediumReference_pTX
    "Base properties calculated with pressure, temperature, and mass fractions";

  Modelica.Units.SI.Pressure[Medium.nX] p_i
    "Partial pressures";

  Modelica.Units.SI.SpecificVolume v
    "Specific volume";
  Modelica.Units.SI.SpecificVolume vReference
    "Specific volume";

  Modelica.Units.SI.SpecificEnthalpy h
    "Specific enthalpy";
  Modelica.Units.SI.SpecificEnthalpy h_1
    "Specific enthalpy of component 1";
  Modelica.Units.SI.SpecificEnthalpy h_2
    "Specific enthalpy of component 2";
  Modelica.Units.SI.SpecificEnthalpy hReference
    "Specific enthalpy";

  Modelica.Units.SI.SpecificInternalEnergy u
    "Specific internal energy";
  Modelica.Units.SI.SpecificInternalEnergy uReference
    "Specific internal energy";

  Modelica.Units.SI.SpecificEntropy s
    "Specific entropy";
  Modelica.Units.SI.SpecificEntropy s_1
    "Specific entropy of component 1";
  Modelica.Units.SI.SpecificEntropy s_2
    "Specific entropy of component 2";
  Modelica.Units.SI.SpecificEntropy sReference
    "Specific entropy";

  Modelica.Units.SI.SpecificHeatCapacity cp
    "Specific heat capacity";
  Modelica.Units.SI.SpecificHeatCapacity cpReference
    "Specific heat capacity";

  Modelica.Units.SI.RelativePressureCoefficient beta
    "Isobaric expansion coefficient";
  Modelica.Units.SI.RelativePressureCoefficient betaReference
    "Isobaric expansion coefficient";

  Modelica.Units.SI.IsothermalCompressibility kappa
    "Isothermal compressibility";
  Modelica.Units.SI.IsothermalCompressibility kappaReference
    "Isothermal compressibility";

  Modelica.Units.SI.Pressure p_sat_water
    "Saturation pressure of water";
  Modelica.Media.Common.DerPressureByTemperature dp_sat_water_dT
    "First-order partial derivative of saturation pressure of water w.r.t. 
    temperature";
  SorpLib.Units.DerPressureByTemperatureTemperature ddp_sat_water_dT_dT
    "Second-order partial derivative of saturation pressure of water w.r.t. 
    temperature";

  //
  // Protected parameters
  //
protected
  final parameter Modelica.Units.SI.SpecificEnthalpy dh_ref=
    Medium.specificEnthalpy_pTX(p=1e5, T=273.15, X=Medium.reference_X) -
    MediumReference.specificEnthalpy_pTX(p=1e5, T=273.15, X=MediumReference.reference_X)
    "Difference of enthalpies to get offset of reference enthalpies";

  final parameter Modelica.Units.SI.SpecificInternalEnergy du_ref=
    (Medium.specificEnthalpy_pTX(p=1e5, T=273.15, X=Medium.reference_X) +
    1e5 * Medium.density_pTX(p=1e5, T=273.15, X=Medium.reference_X)) -
    (MediumReference.specificEnthalpy_pTX(p=1e5, T=273.15, X=MediumReference.reference_X) +
    1e5 * MediumReference.density_pTX(p=1e5, T=273.15, X=MediumReference.reference_X))
    "Difference of specific internal energies to get offset of reference internal
    energies";

  final parameter Modelica.Units.SI.SpecificEntropy ds_ref=
    Medium.specificEntropy_pTX(p=1e5, T=273.15, X=Medium.reference_X) -
    MediumReference.specificEntropy_pTX(p=1e5, T=273.15, X=MediumReference.reference_X)
    "Difference of entropies to get offset of reference entropies";

equation
  //
  // Change independent state variables
  //
  der(p) = (20e5 - 100) / 20
    "Predescribed slope of pressure";
  der(T) = (573.15 - 253.15) / 20
    "Predescribed slope of temperature";

  //
  // Calculate state variables
  //
  medium_pTX.p = p
    "Base properties calculated with pressure and temperature";
  medium_pTX.T = T
    "Base properties calculated with pressure and temperature";
  medium_pTX.X = X
    "Base properties calculated with pressure and temperature";

  mediumReference_pTX.p = p
    "Base properties calculated with pressure, temperature, and mass fractions";
  mediumReference_pTX.T = T
    "Base properties calculated with pressure, temperature, and mass fractions";

  //
  // Calculate further variables
  //
  p_i = Medium.partialPressures(state=medium_pTX.state)
    "Partial pressures";

  v = 1/medium_pTX.d
    "Specific volume";
  vReference = 1/mediumReference_pTX.d
    "Specific volume";

  h = medium_pTX.h
    "Specific enthalpy";
  h_1 = Medium.specificEnthalpy_i_T(ind_component=1, T=T)
    "Specific enthalpy of component 1";
  h_2 = Medium.specificEnthalpy_i_T(ind_component=2, T=T)
    "Specific enthalpy of component 2";
  hReference = mediumReference_pTX.h + dh_ref
    "Specific enthalpy";

  u = medium_pTX.u
    "Specific internal energy";
  uReference = mediumReference_pTX.u + du_ref
    "Specific internal energy";

  s = Medium.specificEntropy(state=medium_pTX.state)
    "Specific entropy";
  s_1 = Medium.specificEntropy_i_pTX(ind_component=1, p=p, T=T, X=X)
    "Specific entropy of component 1";
  s_2 = Medium.specificEntropy_i_pTX(ind_component=1, p=p, T=T, X=X)
    "Specific entropy of component 2";
  sReference = MediumReference.specificEntropy(state=mediumReference_pTX.state)
    + ds_ref
    "Specific entropy";

  cp = Medium.specificHeatCapacityCp(state=medium_pTX.state)
    "Specific heat capacity";
  cpReference = MediumReference.specificHeatCapacityCp(state=mediumReference_pTX.state)
    "Specific heat capacity";

  beta = Medium.beta(state=medium_pTX.state)
    "Isobaric expansion coefficient";
  betaReference = MediumReference.beta(state=mediumReference_pTX.state)
    "Isobaric expansion coefficient";

  kappa = Medium.kappa(state=medium_pTX.state)
    "Isothermal compressibility";
  kappaReference = MediumReference.kappa(state=mediumReference_pTX.state)
    "Isothermal compressibility";

  p_sat_water=Medium.saturationPressureH2O_T(T_sat=T)
    "Saturation pressure of water";
  dp_sat_water_dT=Medium.dsaturationPressureH2O_dT(T_sat=T)
    "First-order partial derivative of saturation pressure of water w.r.t. 
    temperature";
  ddp_sat_water_dT_dT=Medium.ddsaturationPressureH2O_dT_dT(T_sat=T)
    "Second-order partial derivative of saturation pressure of water w.r.t. 
    temperature";
  //
  // Annotations
  //
  annotation (experiment(StopTime=20, Tolerance=1e-06),
Documentation(info="<html>
<p>
This model checks the fluid property calculation of the ideal gas mixture of
N<sub>2</sub> and O<sub>2</sub>.
<br/><br/>
To see the model behavior, plot the variables of the models over the 
time. The simulation time is correctly preset (Start: 0 s, Stop = 20 s). 
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Test_DryAir_N2_O2;
