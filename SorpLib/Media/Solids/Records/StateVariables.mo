within SorpLib.Media.Solids.Records;
record StateVariables
  "This record contains thermodynamic state variables"
  extends Modelica.Icons.Record;

  //
  // Definition of variables
  //
  Modelica.Units.SI.Pressure p
    "Pressure";
  Modelica.Units.SI.Temperature T
    "Temperature";
  Modelica.Units.SI.SpecificVolume v
    "Specific volume";
  Modelica.Units.SI.SpecificEnthalpy h
    "Specific enthalpy";
  Modelica.Units.SI.SpecificInternalEnergy u
    "Specific internal energy";
  Modelica.Units.SI.SpecificEntropy s
    "Specific entropy";
  Modelica.Units.SI.SpecificGibbsFreeEnergy  g
    "Specific free enthalpy (i.e., Gibbs free energy)";
  Modelica.Units.SI.SpecificHelmholtzFreeEnergy  a
    "Specific free energy (i.e., Helmholts free energy)";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This record contains thermodynamic state variables.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 21, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end StateVariables;
