within SorpLib.Media.Solids.Records;
record AdditionalVariables
  "This record contains additional thermodynamic variables"
  extends Modelica.Icons.Record;

  //
  // Definition of paramters
  //
  Modelica.Units.SI.SpecificHeatCapacity c
    "Specific heat capacitiy";
  Modelica.Units.SI.ThermalConductivity lambda
    "Thermal conductivity";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This record contains additional thermodynamic variables.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 21, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end AdditionalVariables;
