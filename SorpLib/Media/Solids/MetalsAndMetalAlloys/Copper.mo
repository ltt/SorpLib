within SorpLib.Media.Solids.MetalsAndMetalAlloys;
model Copper "Model of copper"
  extends BaseClasses.PartialSolid(
    approach_v=SorpLib.Choices.SpecificVolumeSolid.Constant,
    final v_constant=1/8960,
    final approach_v_function=SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionTemperature,
    final T_ref_v=273.15,
    final v_ref=1,
    final coefficients_v={1.60881016976082e-22,-5.53823537743382e-19,
        7.14670930357768e-16,-4.34191656855154e-13,1.31448861732992e-10,-1.36958086403092e-08,
        0.000111423194172002},
    final exponents_v={6,5,4,3,2,1,0},
    approach_v_interpolation=SorpLib.Choices.InterpolationApproach.CubicSplines,
    final abscissa_v={100,150,200,250,300,400,600,800,1000,1200},
    final ordinate_v={0.000111,0.00011121,0.000111445,0.000111719,0.000111982,
        0.000112562,0.000113804,0.000115714,0.000116713,0.000118231},
    approach_c=SorpLib.Choices.SpecificHeatCapacitySolid.Interpolation,
    final c_constant=381,
    final approach_c_function=SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionTemperature,
    final T_ref_c=273.15,
    final c_ref=1,
    final coefficients_c={-8.07419641469257e-15,3.50215406143622e-11,-5.94904387124967e-08,
        5.02166294598675e-05,-0.0221269700391500,4.96530568724046,-65.8705951309113},
    final exponents_c={6,5,4,3,2,1,0},
    approach_c_interpolation=SorpLib.Choices.InterpolationApproach.CubicSplines,
    final abscissa_c={100,150,200,250,300,400,600,800,1000,1200},
    final ordinate_c={254,323,357,377,386,396,431,448,446,480},
    approach_lambda=SorpLib.Choices.ThermalConductivitySolid.Interpolation,
    final lambda_constant=401,
    final approach_lambda_function=SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionTemperature,
    final T_ref_lambda=273.15,
    final lambda_ref=1,
    final coefficients_lambda={1.94540420564950e-15,-8.18944543476065e-12,
        1.37969889556392e-08,-1.18885525980176e-05,0.00552526968168941,-1.38362953925209,
        545.964729052232},
    final exponents_lambda={6,5,4,3,2,1,0},
    approach_lambda_interpolation=SorpLib.Choices.InterpolationApproach.CubicSplines,
    final abscissa_lambda={100,150,200,250,300,400,600,800,1000,1200},
    final ordinate_lambda={480,429,413,406,401,393,379,366,352,339});


  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This model describes copper.
</p>

<h4>References</h4>
<ul>
  <li>
  Neubronner, M. and Bodmer, T. and H&uuml;bner, C. and Kempa, P.B. and Tsotsas, E. and Eschner, A. and Kasparek, G. and Ochs, F. and M&uuml;ller-Steinhagen, H. and Werner, H. and Spitzner M. (2010). D6 Properties of Solids and Solid Materials. In: VDI Heat Atlas. VDI-Buch. Springer, Berlin, Heidelberg. DOI: https://doi.org/10.1007/978-3-540-77877-6_26.
  </li>
  <li>
  The Engineering ToolBox (2023). Copper - Density, Specific Heat and Thermal Conductivity vs. Temperature. URL: https://www.engineeringtoolbox.com/copper-density-specific-heat-thermal-conductivity-vs-temperature-d_2223.html.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  November 21, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Copper;
