within SorpLib.Media.Solids.MetalsAndMetalAlloys.Testers;
model Test_Aluminium "Tester for the model 'Aluminium'"
  extends SorpLib.Media.Solids.BaseClasses.PartialTest(redeclare final model
      Solid = SorpLib.Media.Solids.MetalsAndMetalAlloys.Aluminium);

  //
  // Annotations
  //
  annotation (experiment(
      StopTime=20,
      Tolerance=1e-06),
Documentation(revisions="<html>
<ul>
  <li>
  November 21, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>", info="<html>
<p>
This tester shows the behavior of the 'Aluminium' model. The main 
approaches for calculating thermodynamic property data are demonstrated: 
Constant values, generic functions, and interpolation.
<br/><br/>
To see the model behavior, plot the variables of the models over the 
time. The simulation time is correctly preset (Start: 0 s, Stop = 20 s). 
</p>
</html>"));
end Test_Aluminium;
