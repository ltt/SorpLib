within SorpLib.Media.Solids.MetalsAndMetalAlloys;
package Testers "Models to test and varify sorbent models"
  extends Modelica.Icons.ExamplesPackage;

  annotation (Documentation(info="<html>
<p>
This package contains executable test models for all models of metals and metal
alloys. The test models check the implementation and demonstrate the models' 
general applicability.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Testers;
