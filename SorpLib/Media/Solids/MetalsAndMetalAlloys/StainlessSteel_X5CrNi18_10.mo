﻿within SorpLib.Media.Solids.MetalsAndMetalAlloys;
model StainlessSteel_X5CrNi18_10
  "Model of stainless steel (X5CrNi18-10, 1.4301)"
  extends BaseClasses.PartialSolid(
    approach_v=SorpLib.Choices.SpecificVolumeSolid.Constant,
    final v_constant=1/7919,
    final approach_v_function=SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionTemperature,
    final T_ref_v=293.15,
    final v_ref=1,
    final coefficients_v={1.417e-12,5.53e-09,0.0001245},
    final exponents_v={2,1,0},
    approach_v_interpolation=SorpLib.Choices.InterpolationApproach.CubicSplines,
    final abscissa_v={173.15,223.15,273.15,293.15,323.15,373.15,423.15,473.15,
        523.15,573.15,623.15,673.15,723.15,773.15,823.15,873.15,923.15,973.15,
        1023.15,1073.15,1123.15,1173.15,1223.15,1273.15},
    final ordinate_v={0.000125597,0.000125865,0.000126151,0.000126279,
        0.00012647,0.000126791,0.000127113,0.000127453,0.000127796,0.000128172,
        0.000128535,0.000128899,0.000129266,0.000129651,0.000130056,0.000130446,
        0.000130839,0.000131268,0.000131683,0.0001321,0.000132538,0.000132961,
        0.000133422,0.000133851},
    approach_c=SorpLib.Choices.SpecificHeatCapacitySolid.Interpolation,
    final c_constant=472,
    final approach_c_function=SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionTemperature,
    final T_ref_c=293.15,
    final c_ref=1,
    final coefficients_c={9.25930531486607e-23,-6.29809836854451e-19,
        1.82815391249959e-15,-2.95540224203995e-12,2.91605553475977e-09,-1.81312388707961e-06,
        0.000709566941044524,-0.169563319957511,23.0970795857625,-957.595934830084},
    final exponents_c={9,8,7,6,5,4,3,2,1,0},
    approach_c_interpolation=SorpLib.Choices.InterpolationApproach.CubicSplines,
    final abscissa_c={173.15,223.15,273.15,293.15,323.15,373.15,423.15,473.15,
        523.15,573.15,623.15,673.15,723.15,773.15,823.15,873.15,923.15,973.15,
        1023.15,1073.15,1123.15,1173.15,1223.15,1273.15},
    final ordinate_c={394,437,463,472,484,501,518,525,527,532,544,555,567,582,
        595,604,608,610,610,609,610,615,625,641},
    approach_lambda=SorpLib.Choices.ThermalConductivitySolid.Interpolation,
    final lambda_constant=14.8,
    final approach_lambda_function=SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionTemperature,
    final T_ref_lambda=293.15,
    final lambda_ref=1,
    final coefficients_lambda={-1.08698661001520e-11,3.55842498641231e-08,-4.21979972731494e-05,
        0.0355381893207769,7.17648049527021},
    final exponents_lambda={4,3,2,1,0},
    approach_lambda_interpolation=SorpLib.Choices.InterpolationApproach.CubicSplines,
    final abscissa_lambda={173.15,223.15,273.15,293.15,323.15,373.15,423.15,
        473.15,523.15,573.15,623.15,673.15,723.15,773.15,823.15,873.15,923.15,
        973.15,1023.15,1073.15,1123.15,1173.15,1223.15,1273.15},
    final ordinate_lambda={12.2,13.4,14.4,14.8,15.4,16.2,17,17.8,18.5,19.2,19.9,
        20.6,21.3,22,22.7,23.4,24.2,24.8,25.6,26.2,27,27.6,28.3,29});

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This model describes the stainless steel 'X5CrNi18-10' (i.e., 1.4301).
</p>

<h4>References</h4>
<ul>
  <li>
  Neubronner, M. and Bodmer, T. and H&uuml;bner, C. and Kempa, P.B. and Tsotsas, E. and Eschner, A. and Kasparek, G. and Ochs, F. and M&uuml;ller-Steinhagen, H. and Werner, H. and Spitzner M. (2010). D6 Properties of Solids and Solid Materials. In: VDI Heat Atlas. VDI-Buch. Springer, Berlin, Heidelberg. DOI: https://doi.org/10.1007/978-3-540-77877-6_26.
  </li>
  <li>
  Richter, F. (2011). The Physical Properties of Steels. „The 100 Steels Programme“. Part I: Tables and Figures. URL: https://www.tugraz.at/fileadmin/user_upload/Institute/IEP/Thermophysics_Group/Files/Staehle-Richter.pdf. 
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  November 21, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end StainlessSteel_X5CrNi18_10;
