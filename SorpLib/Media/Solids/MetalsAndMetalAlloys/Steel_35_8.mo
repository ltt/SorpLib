﻿within SorpLib.Media.Solids.MetalsAndMetalAlloys;
model Steel_35_8 "Model of steel (St 35.8, 1.0305)"
  extends BaseClasses.PartialSolid(
    approach_v=SorpLib.Choices.SpecificVolumeSolid.Constant,
    final v_constant=1/7849,
    final approach_v_function=SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionTemperature,
    final T_ref_v=293.15,
    final v_ref=1,
    final coefficients_v={-9.24794524189667e-16,3.38530633048595e-12,
        2.82381898383647e-09,0.000126309572458865},
    final exponents_v={3,2,1,0},
    approach_v_interpolation=SorpLib.Choices.InterpolationApproach.CubicSplines,
    final abscissa_v={173.15,223.15,273.15,293.15,323.15,373.15,423.15,473.15,
        523.15,573.15,623.15,673.15,723.15,773.15,823.15,873.15,923.15},
    final ordinate_v={0.000126904,0.000127097,0.000127307,0.000127405,
        0.000127551,0.000127779,0.000128041,0.000128304,0.000128584,0.000128866,
        0.000129149,0.000129467,0.000129769,0.00013009,0.000130412,0.000130736,
        0.000131079},
    approach_c=SorpLib.Choices.SpecificHeatCapacitySolid.Interpolation,
    final c_constant=461,
    final approach_c_function=SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionTemperature,
    final T_ref_c=293.15,
    final c_ref=1,
    final coefficients_c={1.53498822513122e-21,-7.10858683567692e-18,
        1.42358708364266e-14,-1.61700732871324e-11,1.14838006710273e-08,-5.29608834276069e-06,
        0.00159217501494625,-0.303448556797988,34.1990283010557,-1370.20964231408},
    final exponents_c={9,8,7,6,5,4,3,2,1,0},
    approach_c_interpolation=SorpLib.Choices.InterpolationApproach.CubicSplines,
    final abscissa_c={173.15,223.15,273.15,293.15,323.15,373.15,423.15,473.15,
        523.15,573.15,623.15,673.15,723.15,773.15,823.15,873.15,923.15},
    final ordinate_c={371,419,451,461,475,496,515,533,550,568,589,611,639,677,
        724,778,880},
    approach_lambda=SorpLib.Choices.ThermalConductivitySolid.Interpolation,
    final lambda_constant=47,
    final approach_lambda_function=SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionTemperature,
    final T_ref_lambda=293.15,
    final lambda_ref=1,
    final coefficients_lambda={-3.94094761029167e-13,1.07417893817399e-09,-9.96940386382202e-07,
        0.000294011493296933,0.0194428803212433,34.2898469544763},
    final exponents_lambda={5,4,3,2,1,0},
    approach_lambda_interpolation=SorpLib.Choices.InterpolationApproach.CubicSplines,
    final abscissa_lambda={173.15,223.15,273.15,293.15,323.15,373.15,423.15,
        473.15,523.15,573.15,623.15,673.15,723.15,773.15,823.15,873.15,923.15},
    final ordinate_lambda={42.2,45.1,46.6,47,47.9,48.8,49.1,48.2,47.2,45.7,44.3,
        42.6,40.8,39.3,37.7,35.9,34.4});

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This model describes the steel 'St 35.8' (i.e., 1.0305).
</p>

<h4>References</h4>
<ul>
  <li>
  Neubronner, M. and Bodmer, T. and H&uuml;bner, C. and Kempa, P.B. and Tsotsas, E. and Eschner, A. and Kasparek, G. and Ochs, F. and M&uuml;ller-Steinhagen, H. and Werner, H. and Spitzner M. (2010). D6 Properties of Solids and Solid Materials. In: VDI Heat Atlas. VDI-Buch. Springer, Berlin, Heidelberg. DOI: https://doi.org/10.1007/978-3-540-77877-6_26.
  </li>
  <li>
  Richter, F. (2011). The Physical Properties of Steels. „The 100 Steels Programme“. Part I: Tables and Figures. URL: https://www.tugraz.at/fileadmin/user_upload/Institute/IEP/Thermophysics_Group/Files/Staehle-Richter.pdf. 
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  November 21, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Steel_35_8;
