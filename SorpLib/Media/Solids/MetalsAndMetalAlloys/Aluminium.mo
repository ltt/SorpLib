within SorpLib.Media.Solids.MetalsAndMetalAlloys;
model Aluminium "Model of aluminium"
  extends BaseClasses.PartialSolid(
    approach_v=SorpLib.Choices.SpecificVolumeSolid.Constant,
    final v_constant=1/2700,
    final approach_v_function=SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionTemperature,
    final T_ref_v=273.15,
    final v_ref=1,
    final coefficients_v={2.40565103604828e-17,-3.18909783349765e-14,
        3.05844815369395e-11,9.82750318802462e-09,0.000365560992186206},
    final exponents_v={4,3,2,1,0},
    approach_v_interpolation=SorpLib.Choices.InterpolationApproach.CubicSplines,
    final abscissa_v={1.721428571,98.86428571,201.7214286,298.8642857,
        398.8642857,501.7214286,601.7214286,696.0071429,793.15,901.7214286,
        930.2928571},
    final ordinate_v={0.000365578,0.000366803,0.000368566,0.000370705,
        0.000372868,0.000375608,0.000378763,0.000382543,0.000386204,0.000391529,
        0.000393539},
    approach_c=SorpLib.Choices.SpecificHeatCapacitySolid.Interpolation,
    final c_constant=837,
    final approach_c_function=SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionTemperature,
    final T_ref_c=273.15,
    final c_ref=1,
    final coefficients_c={4.27500975902274e-12,-1.57466005351826e-08,
        2.29069115958695e-05,-0.0160371796818545,5.75972719368390,
        110.779863838808},
    final exponents_c={5,4,3,2,1,0},
    approach_c_interpolation=SorpLib.Choices.InterpolationApproach.CubicSplines,
    final abscissa_c={264.1864146,306.7634454,351.5813725,409.8446779,
        474.8306723,566.707423,667.5477591,739.2564426,799.7606443,846.8194678,
        889.3964986},
    final ordinate_c={864.2857143,901.9305019,931.8532819,958.8803089,
        984.9420849,1020.656371,1063.127413,1099.80695,1133.590734,1162.548263,
        1189.57529},
    approach_lambda=SorpLib.Choices.ThermalConductivitySolid.Interpolation,
    final lambda_constant=236,
    final approach_lambda_function=SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionTemperature,
    final T_ref_lambda=273.15,
    final lambda_ref=1,
    final coefficients_lambda={5.93000570062670e-13,-1.92890156664459e-09,
        2.59002630981166e-06,-0.00184669307956919,0.642940781600963,
        154.914872094860},
    final exponents_lambda={5,4,3,2,1,0},
    approach_lambda_interpolation=SorpLib.Choices.InterpolationApproach.CubicSplines,
    final abscissa_lambda={275.2664021,298.5468254,351.4568783,400.134127,
        501.7214286,603.3087302,700.6632275,800.134127,899.6050265,933.4674603},
    final ordinate_lambda={235.8518519,236.8888889,238.962963,240,236.3703704,
        231.1851852,224.962963,217.7037037,209.9259259,208.3703704});

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This model describes aluminium.
</p>

<h4>References</h4>
<ul>
  <li>
  Neubronner, M. and Bodmer, T. and H&uuml;bner, C. and Kempa, P.B. and Tsotsas, E. and Eschner, A. and Kasparek, G. and Ochs, F. and M&uuml;ller-Steinhagen, H. and Werner, H. and Spitzner M. (2010). D6 Properties of Solids and Solid Materials. In: VDI Heat Atlas. VDI-Buch. Springer, Berlin, Heidelberg. DOI: https://doi.org/10.1007/978-3-540-77877-6_26.
  </li>
  <li>
  Foteinopoulos, P. and Papacharalampopoulos, A. and Stavropoulos, P. (2018). On thermal modeling of Additive Manufacturing processes. CIRP Journal of Manufacturing Science and Technology, 20: 66-83. DOI: https://doi.org/10.1016/j.cirpj.2017.09.007.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  November 21, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Aluminium;
