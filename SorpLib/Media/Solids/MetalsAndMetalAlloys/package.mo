within SorpLib.Media.Solids;
package MetalsAndMetalAlloys "Parametrized models of metals and metal alloys"
  extends Modelica.Icons.VariantsPackage;

  annotation (Documentation(info="<html>
<p>
This package contains parametrized solid models describing metals and metal alloys.
The models already implemented can be found in the package content. 
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end MetalsAndMetalAlloys;
