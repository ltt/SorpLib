within SorpLib.Media;
package Solids "Models to calcualte fluid property data of solids"
  extends Modelica.Icons.VariantsPackage;

  annotation (Documentation(info="<html>
<p>
This package contains models calculating thermodynamic properties of solids, such as
sorbents or metals and metal alloys. Independent state variables are the pressure and
temperature. In the models, fundamental thermodynamic properties can be calculated:
</p>
<ol>
  <li>
  Thermal state variables: <i>p</i>, <i>T</i>, and <i>&rho;</i>.
  </li>
  <li>
  Caloric state variables: <i>h</i> and <i>u</i>.
  </li>
  <li>
  Entropic state variables: <i>s</i>, <i>g</i>, and <i>a</i>.
  </li>
</ol>
<p>
Moreover, the model allow to calculate the specific heat capacity <i>c</i> and thermal 
conductivity <i>&lambda;</i>.
</p>

<h4>How to add new solid models</h4>
<p>
To add a new solid model, duplicate an existing solid model in the correct package. Then,
parametrize the model and write the documentation, including references for the thermodynamic
properties.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end Solids;
