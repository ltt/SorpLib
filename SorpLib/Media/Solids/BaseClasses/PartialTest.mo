within SorpLib.Media.Solids.BaseClasses;
partial model PartialTest
  "Base model for testers of describing isobaric expansion coefficients of the adsorpt phase for pure component adsorption"
  extends Modelica.Icons.Example;

  //
  // Definition of parameters
  //
  parameter Modelica.Units.SI.Pressure p_start = 1000
    "Start value of pressure"
    annotation (Dialog(tab="General", group="Test setup"),
                Evaluate=false,
                HideResult=false);
  parameter Modelica.Units.SI.Temperature T_start = 273.15
    "Start value of temperature"
    annotation (Dialog(tab="General", group="Test setup"),
                Evaluate=false,
                HideResult=false);
  parameter Real p_der(unit="Pa/s") = (1e5-1000)/20
    "Prescriped sloped of pressure to test solid model"
    annotation (Dialog(tab="General", group="Test setup"),
                Evaluate=false,
                HideResult=false);
  parameter Real T_der(unit="K/s") = 500/20
    "Prescriped sloped of temperature to test solid model"
    annotation (Dialog(tab="General", group="Test setup"),
                Evaluate=false,
                HideResult=false);

  replaceable model Solid =
    SorpLib.Media.Solids.BaseClasses.PartialSolid
    constrainedby SorpLib.Media.Solids.BaseClasses.PartialSolid
    "Solid model"
    annotation (Dialog(tab="General",group="Media"),
                choicesAllMatching = true,
                Evaluate=true,
                HideResult=true);

  //
  // Definition of variables
  //
  Modelica.Units.SI.Pressure p(start=p_start, fixed=true)
    "Pressure";
  Modelica.Units.SI.Temperature T(start=T_start, fixed=true)
    "Temperature";

  //
  // Instantiation of models
  //
  Solid solid_constant(
    calcCaloricProperties=true,
    calcEntropicProperties=true,
    final approach_v=SorpLib.Choices.SpecificVolumeSolid.Constant,
    final approach_v_interpolation=SorpLib.Choices.InterpolationApproach.CubicSplines,
    final approach_c=SorpLib.Choices.SpecificHeatCapacitySolid.Constant,
    final approach_c_interpolation=SorpLib.Choices.InterpolationApproach.CubicSplines,
    final approach_lambda=SorpLib.Choices.ThermalConductivitySolid.Constant,
    final approach_lambda_interpolation=SorpLib.Choices.InterpolationApproach.CubicSplines,
    final p=p,
    final T=T) "Solid: Consant properties"
    annotation (Placement(transformation(extent={{-80,-10},{-60,10}})));

  Solid solid_generalizedFunction(
    calcCaloricProperties=true,
    calcEntropicProperties=true,
    approach_v=SorpLib.Choices.SpecificVolumeSolid.Constant,
    final approach_v_interpolation=SorpLib.Choices.InterpolationApproach.CubicSplines,
    final approach_c=SorpLib.Choices.SpecificHeatCapacitySolid.GeneralizedFunction,
    final approach_c_interpolation=SorpLib.Choices.InterpolationApproach.CubicSplines,
    final approach_lambda=SorpLib.Choices.ThermalConductivitySolid.GeneralizedFunction,
    final approach_lambda_interpolation=SorpLib.Choices.InterpolationApproach.CubicSplines,
    final p=p,
    final T=T) "Solid: Generalized function"
    annotation (Placement(transformation(extent={{-40,-10},{-20,10}})));

  Solid solid_linearInterpolation(
    calcCaloricProperties=true,
    calcEntropicProperties=true,
    approach_v=SorpLib.Choices.SpecificVolumeSolid.Constant,
    final approach_v_interpolation=SorpLib.Choices.InterpolationApproach.Linear,
    final approach_c=SorpLib.Choices.SpecificHeatCapacitySolid.Interpolation,
    final approach_c_interpolation=SorpLib.Choices.InterpolationApproach.Linear,
    final approach_lambda=SorpLib.Choices.ThermalConductivitySolid.Interpolation,
    final approach_lambda_interpolation=SorpLib.Choices.InterpolationApproach.Linear,
    final p=p,
    final T=T) "Solid: Linear interpolation"
    annotation (Placement(transformation(extent={{20,-10},{40,10}})));

  Solid solid_cubicSplineInterpolation(
    calcCaloricProperties=true,
    calcEntropicProperties=true,
    approach_v=SorpLib.Choices.SpecificVolumeSolid.Constant,
    final approach_v_interpolation=SorpLib.Choices.InterpolationApproach.CubicSplines,
    final approach_c=SorpLib.Choices.SpecificHeatCapacitySolid.Interpolation,
    final approach_c_interpolation=SorpLib.Choices.InterpolationApproach.CubicSplines,
    final approach_lambda=SorpLib.Choices.ThermalConductivitySolid.Interpolation,
    final approach_lambda_interpolation=SorpLib.Choices.InterpolationApproach.CubicSplines,
    final p=p,
    final T=T) "Solid: Cubic spline interpolation"
    annotation (Placement(transformation(extent={{60,-10},{80,10}})));

equation
  //
  // Calculation of properties
  //
  der(p) = p_der
    "Predecsriped slope of p to demonstrate the solid model";
  der(T) = T_der
    "Predecsriped slope of T to demonstrate the solid model";

  //
  // Annotations
  //
  annotation (
    Icon(coordinateSystem(preserveAspectRatio=false)),
    Diagram(coordinateSystem(preserveAspectRatio=false)),
    Documentation(info="<html>
<p>
This partial model is the basic model for all testers of solid models. This partial 
model defines all relevant parameters, variables, and models to test the solid models.
<br/><br/>
Models that inherit properties from this partial model have to redeclare the model
'Solid' and to define the test setup.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 21, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end PartialTest;
