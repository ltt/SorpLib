within SorpLib.Media.Solids;
package BaseClasses "Base classes used to build new solid models"
  extends Modelica.Icons.BasesPackage;

  annotation (Documentation(info="<html>
<p>
This package contains partial functions and models. These partial functions and models
contain fundamental definitions for calculating thermodynamic properties of solids.
The content of this package is only of interest when adding new functions to the library.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end BaseClasses;
