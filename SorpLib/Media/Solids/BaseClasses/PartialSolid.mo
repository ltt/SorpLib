within SorpLib.Media.Solids.BaseClasses;
partial model PartialSolid
  "Base model for solids"
  extends Modelica.Icons.MaterialProperty;

  //
  // Definition of parameters regarding calculation setup
  //
  parameter Boolean calcCaloricProperties = false
    "= true, if caloric properties are calculated (i.e., h and u)"
    annotation (Dialog(tab="General", group="General"),
                choices(checkBox=true),
                Evaluate= true);
  parameter Boolean calcEntropicProperties = false
    "= true, if caloric properties are calculated (i.e., s, g and a)"
    annotation (Dialog(tab="General", group="General",
                enable = calcCaloricProperties),
                choices(checkBox=true),
                Evaluate= true);

  //
  // Definition of parameters regarding the specific volume
  //
  parameter SorpLib.Choices.SpecificVolumeSolid approach_v=
    SorpLib.Choices.SpecificVolumeSolid.Constant
    "Calculation approach for the specific volume"
    annotation (Dialog(tab="Specific Volume", group="General"), Evaluate=false);

  parameter Modelica.Units.SI.SpecificVolume v_constant= 1/7919
    "Constant specific volume"
    annotation (Dialog(tab="Specific Volume", group="Constant",
                enable=(approach_v == SorpLib.Choices.SpecificVolumeSolid.Constant)),
                HideResult=true,
                Evaluate= false);

  parameter SorpLib.Choices.GeneralizedFunctionApproach approach_v_function=
      SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionTemperature
    "Generalized function used to calculate the specific volume" annotation (
      Dialog(
      tab="Specific Volume",
      group="Generalized Function",
      enable=(approach_v == SorpLib.Choices.SpecificVolumeSolid.GeneralizedFunction)),
      Evaluate=true);
  parameter Modelica.Units.SI.Temperature T_ref_v = 293.15
    "Reference temperature for the specific volume"
    annotation (Dialog(tab="Specific Volume", group="Generalized Function",
                enable=(approach_v == SorpLib.Choices.SpecificVolumeSolid.GeneralizedFunction)),
                HideResult=true,
                Evaluate= true);
  parameter Real v_ref = 1
    "Reference fluid property data for the specific volume"
    annotation (Dialog(tab="Specific Volume", group="Generalized Function",
                enable=(approach_v == SorpLib.Choices.SpecificVolumeSolid.GeneralizedFunction)),
                HideResult=true,
                Evaluate= true);
  parameter Real coefficients_v[:]={v_constant}
    "Coefficients of generalized function for the specific volume"
    annotation (Dialog(tab="Specific Volume", group="Generalized Function",
                enable=(approach_v == SorpLib.Choices.SpecificVolumeSolid.GeneralizedFunction)),
                HideResult=true,
                Evaluate= true);
  parameter Real exponents_v[size(coefficients_v,1)]={0}
    "Exponents of generalized function for the specific volume"
    annotation (Dialog(tab="Specific Volume", group="Generalized Function",
                enable=(approach_v == SorpLib.Choices.SpecificVolumeSolid.GeneralizedFunction)),
                HideResult=true,
                Evaluate= true);

  parameter SorpLib.Choices.InterpolationApproach approach_v_interpolation=
      SorpLib.Choices.InterpolationApproach.Linear
    "Interpolation approach used to calculate the specific volume" annotation (
      Dialog(
      tab="Specific Volume",
      group="Interpolation",
      enable=(approach_v == SorpLib.Choices.SpecificVolumeSolid.Interpolation)),
      Evaluate=false);
  parameter Real abscissa_v[:]={0, 1000}
    "Known abscissa values for the specific volume"
    annotation (Dialog(tab="Specific Volume", group="Interpolation",
                enable=(approach_v == SorpLib.Choices.SpecificVolumeSolid.Interpolation)),
                HideResult=true,
                Evaluate= true);
  parameter Real ordinate_v[size(abscissa_v,1)]={v_constant, v_constant}
    "Known ordinate values for the specific volume"
    annotation (Dialog(tab="Specific Volume", group="Interpolation",
                enable=(approach_v == SorpLib.Choices.SpecificVolumeSolid.Interpolation)),
                HideResult=true,
                Evaluate= true);
  final parameter Real coefficients_cubicSplines_v[size(abscissa_v,1),4]=
    SorpLib.Media.Functions.Utilities.calcCubicSplineCoefficients(
    abscissa=abscissa_v,
    ordinate=ordinate_v)
    "Coefficient a to d for cubic polynomials for the specific volume"
    annotation (Dialog(tab="Specific Volume", group="Interpolation",
                enable=false),
                HideResult=true,
                Evaluate= true);

  //
  // Definition of parameters regarding the specific heat capacity
  //
  parameter SorpLib.Choices.SpecificHeatCapacitySolid approach_c=
    SorpLib.Choices.SpecificHeatCapacitySolid.Constant
    "Calculation approach for the specific heat capacity" annotation (Dialog(
        tab="Specific Heat Capacity", group="General"), Evaluate=false);

  parameter Modelica.Units.SI.SpecificHeatCapacity c_constant= 472
    "Constant specific heat capacity"
    annotation (Dialog(tab="Specific Heat Capacity", group="Constant",
                enable=(approach_c == SorpLib.Choices.SpecificVolumeSolid.Constant)),
                HideResult=true,
                Evaluate= false);

  parameter SorpLib.Choices.GeneralizedFunctionApproach approach_c_function=
      SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionTemperature
    "Generalized function used to calculate the specific heat capacity"
    annotation (Dialog(
      tab="Specific Heat Capacity",
      group="Generalized Function",
      enable=(approach_c == SorpLib.Choices.SpecificVolumeSolid.GeneralizedFunction)),
      Evaluate=true);
  parameter Modelica.Units.SI.Temperature T_ref_c = 293.15
    "Reference temperature for the specific heat capacity"
    annotation (Dialog(tab="Specific Heat Capacity", group="Generalized Function",
                enable=(approach_c == SorpLib.Choices.SpecificVolumeSolid.GeneralizedFunction)),
                HideResult=true,
                Evaluate= true);
  parameter Real c_ref = 1
    "Reference fluid property data for the specific heat capacity"
    annotation (Dialog(tab="Specific Heat Capacity", group="Generalized Function",
                enable=(approach_c == SorpLib.Choices.SpecificVolumeSolid.GeneralizedFunction)),
                HideResult=true,
                Evaluate= true);
  parameter Real coefficients_c[:]= {c_constant}
    "Coefficients of generalized function for the specific heat capacity"
    annotation (Dialog(tab="Specific Heat Capacity", group="Generalized Function",
                enable=(approach_c == SorpLib.Choices.SpecificVolumeSolid.GeneralizedFunction)),
                HideResult=true,
                Evaluate= true);
  parameter Real exponents_c[size(coefficients_c,1)] = {0}
    "Exponents of generalized function for the specific heat capacity"
    annotation (Dialog(tab="Specific Heat Capacity", group="Generalized Function",
                enable=(approach_c == SorpLib.Choices.SpecificVolumeSolid.GeneralizedFunction)),
                HideResult=true,
                Evaluate= true);

  parameter SorpLib.Choices.InterpolationApproach approach_c_interpolation=
      SorpLib.Choices.InterpolationApproach.Linear
    "Interpolation approach used to calculate the specific heat capacity"
    annotation (Dialog(
      tab="Specific Heat Capacity",
      group="Interpolation",
      enable=(approach_c == SorpLib.Choices.SpecificVolumeSolid.Interpolation)),
      Evaluate=false);
  parameter Real abscissa_c[:]={0, 1000}
    "Known abscissa values for the specific heat capacity"
    annotation (Dialog(tab="Specific Heat Capacity", group="Interpolation",
                enable=(approach_c == SorpLib.Choices.SpecificVolumeSolid.Interpolation)),
                HideResult=true,
                Evaluate= true);
  parameter Real ordinate_c[size(abscissa_c,1)]= {c_constant, c_constant}
    "Known ordinate values for the specific heat capacity"
    annotation (Dialog(tab="Specific Heat Capacity", group="Interpolation",
                enable=(approach_c == SorpLib.Choices.SpecificVolumeSolid.Interpolation)),
                HideResult=true,
                Evaluate= true);
  final parameter Real coefficients_cubicSplines_c[size(abscissa_c,1),4]=
    SorpLib.Media.Functions.Utilities.calcCubicSplineCoefficients(
    abscissa=abscissa_c,
    ordinate=ordinate_c)
    "Coefficient a to d for cubic polynomials for the specific heat capacity"
    annotation (Dialog(tab="Specific Heat Capacity", group="Interpolation",
                enable=false),
                HideResult=true,
                Evaluate= true);

  //
  // Definition of parameters regarding the thermal conductivity
  //
  parameter SorpLib.Choices.ThermalConductivitySolid approach_lambda=
    SorpLib.Choices.ThermalConductivitySolid.Constant
    "Calculation approach for the thermal conductivity" annotation (Dialog(tab=
          "Thermal Conductivity", group="General"), Evaluate=false);

  parameter Modelica.Units.SI.ThermalConductivity lambda_constant= 14.8
    "Constant thermal conductivity"
    annotation (Dialog(tab="Thermal Conductivity", group="Constant",
                enable=(approach_lambda == SorpLib.Choices.SpecificVolumeSolid.Constant)),
                HideResult=true,
                Evaluate= false);

  parameter SorpLib.Choices.GeneralizedFunctionApproach approach_lambda_function=
    SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionTemperature
    "Generalized function used to calculate the thermal conductivity"
    annotation (Dialog(
      tab="Thermal Conductivity",
      group="Generalized Function",
      enable=(approach_lambda == SorpLib.Choices.SpecificVolumeSolid.GeneralizedFunction)),
      Evaluate=true);
  parameter Modelica.Units.SI.Temperature T_ref_lambda = 293.15
    "Reference temperature for the thermal conductivity"
    annotation (Dialog(tab="Thermal Conductivity", group="Generalized Function",
                enable=(approach_lambda == SorpLib.Choices.SpecificVolumeSolid.GeneralizedFunction)),
                HideResult=true,
                Evaluate= true);
  parameter Real lambda_ref = 1
    "Reference fluid property data for the thermal conductivity"
    annotation (Dialog(tab="Thermal Conductivity", group="Generalized Function",
                enable=(approach_lambda == SorpLib.Choices.SpecificVolumeSolid.GeneralizedFunction)),
                HideResult=true,
                Evaluate= true);
  parameter Real coefficients_lambda[:]={lambda_constant}
    "Coefficients of generalized function for the thermal conductivity"
    annotation (Dialog(tab="Thermal Conductivity", group="Generalized Function",
                enable=(approach_lambda == SorpLib.Choices.SpecificVolumeSolid.GeneralizedFunction)),
                HideResult=true,
                Evaluate= true);
  parameter Real exponents_lambda[size(coefficients_lambda,1)]={0}
    "Exponents of generalized function for the thermal conductivity"
    annotation (Dialog(tab="Thermal Conductivity", group="Generalized Function",
                enable=(approach_lambda == SorpLib.Choices.SpecificVolumeSolid.GeneralizedFunction)),
                HideResult=true,
                Evaluate= true);

  parameter SorpLib.Choices.InterpolationApproach approach_lambda_interpolation=
     SorpLib.Choices.InterpolationApproach.Linear
    "Interpolation approach used to calculate the thermal conductivity"
    annotation (Dialog(
      tab="Thermal Conductivity",
      group="Interpolation",
      enable=(approach_lambda == SorpLib.Choices.SpecificVolumeSolid.Interpolation)),
      Evaluate=false);
  parameter Real abscissa_lambda[:]={0, 1000}
    "Known abscissa values for the thermal conductivity"
    annotation (Dialog(tab="Thermal Conductivity", group="Interpolation",
                enable=(approach_lambda == SorpLib.Choices.SpecificVolumeSolid.Interpolation)),
                HideResult=true,
                Evaluate= true);
  parameter Real ordinate_lambda[size(abscissa_lambda,1)]= {lambda_constant, lambda_constant}
    "Known ordinate values for the thermal conductivity"
    annotation (Dialog(tab="Thermal Conductivity", group="Interpolation",
                enable=(approach_lambda == SorpLib.Choices.SpecificVolumeSolid.Interpolation)),
                HideResult=true,
                Evaluate= true);
  final parameter Real coefficients_cubicSplines_lambda[size(abscissa_lambda,1),4]=
    SorpLib.Media.Functions.Utilities.calcCubicSplineCoefficients(
    abscissa=abscissa_lambda,
    ordinate=ordinate_lambda)
    "Coefficient a to d for cubic polynomials for the thermal conductivity"
    annotation (Dialog(tab="Thermal Conductivity", group="Interpolation",
                enable=false),
                HideResult=true,
                Evaluate= true);

  //
  // Definition of parameters regarding advanced options
  //
  parameter Modelica.Units.SI.Pressure p_ref = 1e5
    "Reference pressure for caloric and entropic calculations"
    annotation (Dialog(tab="Advanced", group="Reference State",
                enable=calcCaloricProperties or calcEntropicProperties),
                Evaluate= true);
  parameter Modelica.Units.SI.Temperature T_ref = 298.15
    "Reference temperature for caloric and entropic calculations"
    annotation (Dialog(tab="Advanced", group="Reference State",
                enable=calcCaloricProperties or calcEntropicProperties),
                Evaluate= true);

  parameter Modelica.Units.SI.SpecificEnthalpy h_ref = 0
    "Specific enthalpy at reference state"
    annotation (Dialog(tab="Advanced", group="Reference State",
                enable=calcCaloricProperties or calcEntropicProperties),
                Evaluate= true);
  parameter Modelica.Units.SI.SpecificEntropy s_ref = 0
    "Specific entropy at reference state"
    annotation (Dialog(tab="Advanced", group="Reference State",
                enable=calcEntropicProperties),
                Evaluate= true);

  parameter Real tolerance_int_h = 1e-6
    "Integration tolerance when calculating the specific enthalpy numerically"
    annotation (Dialog(tab="Advanced", group="Numerics",
                enable=calcCaloricProperties),
                HideResult=true,
                Evaluate= true);
  parameter Real tolerance_int_s = 1e-6
    "Integration tolerance when calculating the specific entropy numerically"
    annotation (Dialog(tab="Advanced", group="Numerics",
                enable=calcEntropicProperties),
                HideResult=true,
                Evaluate= true);

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.Pressure p
    "Pressure of solid"
    annotation (Dialog(tab="General", group="Inputs",
                enable=false));
  input Modelica.Units.SI.Temperature T
    "Temperature of solid"
    annotation (Dialog(tab="General", group="Inputs",
                enable=false));

  //
  // Definition of outputs
  //
  output SorpLib.Media.Solids.Records.StateVariables state_variables
    "Thermodynamic state variables"
    annotation (Dialog(tab="General", group="Outputs",
                enable=false));
  output SorpLib.Media.Solids.Records.AdditionalVariables additional_variables
    "Additional variables"
    annotation (Dialog(tab="General", group="Outputs",
                enable=false));

  //
  // Definition of functions describing integrands
  //
protected
  function int_c_dT_generalizedFunction
    "Integrand required for calculating the specific enthalpy using the generalized
    function"
    extends Modelica.Math.Nonlinear.Interfaces.partialScalarFunction;
    input Modelica.Units.SI.Temperature T_ref
      "Reference temperature for fluid property data";
    input Real z_ref
      "Reference fluid property data";
    input Real coefficients[:]
      "Coefficients of generalized function";
    input Real exponents[size(coefficients,1)]
      "Exponents of generalized function";
    input SorpLib.Choices.GeneralizedFunctionApproach approach
      "Function approach";
  algorithm
    y := SorpLib.Media.Functions.Utilities.generalizedFunction_T(
      T=u,
      T_ref=T_ref,
      z_ref=z_ref,
      coefficients=coefficients,
      exponents=exponents,
      approach=approach)
      "Integrand required for calculating the specific enthalpy using the generalized
      function";
  end int_c_dT_generalizedFunction;

  function int_c_div_T_dT_generalizedFunction
    "Integrand required for calculating the specific entropy using the generalized
    function"
    extends Modelica.Math.Nonlinear.Interfaces.partialScalarFunction;
    input Modelica.Units.SI.Temperature T_ref
      "Reference temperature for fluid property data";
    input Real z_ref
      "Reference fluid property data";
    input Real coefficients[:]
      "Coefficients of generalized function";
    input Real exponents[size(coefficients,1)]
      "Exponents of generalized function";
    input SorpLib.Choices.GeneralizedFunctionApproach approach
      "Function approach";
  algorithm
    y := SorpLib.Media.Functions.Utilities.generalizedFunction_T(
      T=u,
      T_ref=T_ref,
      z_ref=z_ref,
      coefficients=coefficients,
      exponents=exponents,
      approach=approach) / u
      "Integrand required for calculating the specific entropy using the generalized
      function";
  end int_c_div_T_dT_generalizedFunction;

  function int_c_dT_linearInterpolation
    "Integrand required for calculating the specific enthalpy using linear 
    interpolation"
    extends Modelica.Math.Nonlinear.Interfaces.partialScalarFunction;
    input Real abscissa[:]
      "Known abscissa values";
    input Real ordinate[size(abscissa,1)]
      "Known ordinate values";
  algorithm
    y := SorpLib.Media.Functions.Utilities.linearInterpolation_T(
      T=u,
      abscissa=abscissa,
      ordinate=ordinate)
      "Integrand required for calculating the specific enthalpy using linear 
      interpolation";
  end int_c_dT_linearInterpolation;

  function int_c_div_T_dT_linearInterpolation
    "Integrand required for calculating the specific entropy using linear 
    interpolation"
    extends Modelica.Math.Nonlinear.Interfaces.partialScalarFunction;
    input Real abscissa[:]
      "Known abscissa values";
    input Real ordinate[size(abscissa,1)]
      "Known ordinate values";
  algorithm
    y := SorpLib.Media.Functions.Utilities.linearInterpolation_T(
      T=u,
      abscissa=abscissa,
      ordinate=ordinate) / u
      "Integrand required for calculating the specific entropy using linear 
      interpolation";
  end int_c_div_T_dT_linearInterpolation;

  function int_c_dT_cubicSplineInterpolation
    "Integrand required for calculating the specific enthalpy using cubic spline 
    interpolation"
    extends Modelica.Math.Nonlinear.Interfaces.partialScalarFunction;
    input Real abscissa[:]
      "Known abscissa values";
    input Real ordinate[size(abscissa,1)]
      "Known ordinate values";
    input Real coefficients[size(abscissa,1),4]
      "Coefficient a to d for cubic polynomials";
  algorithm
    y := SorpLib.Media.Functions.Utilities.cubicSplineInterpolation_T(
      T=u,
      abscissa=abscissa,
      ordinate=ordinate,
      coefficients=coefficients)
      "Integrand required for calculating the specific enthalpy using cubic spline 
      interpolation";
  end int_c_dT_cubicSplineInterpolation;

  function int_c_div_T_dT_cubicSplineInterpolation
    "Integrand required for calculating the specific entropy using cubic spline 
    interpolation"
    extends Modelica.Math.Nonlinear.Interfaces.partialScalarFunction;
    input Real abscissa[:]
      "Known abscissa values";
    input Real ordinate[size(abscissa,1)]
      "Known ordinate values";
    input Real coefficients[size(abscissa,1),4]
      "Coefficient a to d for cubic polynomials";
  algorithm
    y := SorpLib.Media.Functions.Utilities.cubicSplineInterpolation_T(
      T=u,
      abscissa=abscissa,
      ordinate=ordinate,
      coefficients=coefficients) / u
      "Integrand required for calculating the specific entropy using cubic spline 
      interpolation";
  end int_c_div_T_dT_cubicSplineInterpolation;

equation
  //
  // Pass input data
  //
  state_variables.p = p
    "Pressure";
  state_variables.T = T
    "Pressure";

  //
  // Calculate the density
  //
  if approach_v == SorpLib.Choices.SpecificVolumeSolid.Constant then
    state_variables.v = v_constant
      "Specific volume of solid";

  elseif approach_v == SorpLib.Choices.SpecificVolumeSolid.GeneralizedFunction then
    state_variables.v = SorpLib.Media.Functions.Utilities.generalizedFunction_T(
      T=T,
      T_ref=T_ref_v,
      z_ref=v_ref,
      coefficients=coefficients_v,
      exponents=exponents_v,
      approach=approach_v_function)
      "Specific volume of solid";

  else
    if approach_v_interpolation == SorpLib.Choices.InterpolationApproach.Linear then
      state_variables.v = SorpLib.Media.Functions.Utilities.linearInterpolation_T(
        T=T,
        abscissa=abscissa_v,
        ordinate=ordinate_v)
        "Specific volume of solid";

    else
      state_variables.v = SorpLib.Media.Functions.Utilities.cubicSplineInterpolation_T(
        T=T,
        abscissa=abscissa_v,
        ordinate=ordinate_v,
        coefficients=coefficients_cubicSplines_v)
        "Specific volume of solid";

    end if;
  end if;

  //
  // Calculate the specific heat capacity
  //
  if approach_c == SorpLib.Choices.SpecificHeatCapacitySolid.Constant then
    additional_variables.c = c_constant
      "Specific heat capacitiy of solid";

  elseif approach_c == SorpLib.Choices.SpecificHeatCapacitySolid.GeneralizedFunction then
    additional_variables.c = SorpLib.Media.Functions.Utilities.generalizedFunction_T(
      T=T,
      T_ref=T_ref_c,
      z_ref=c_ref,
      coefficients=coefficients_c,
      exponents=exponents_c,
      approach=approach_c_function)
      "Specific heat capacitiy of solid";

  else
    if approach_c_interpolation == SorpLib.Choices.InterpolationApproach.Linear then
      additional_variables.c = SorpLib.Media.Functions.Utilities.linearInterpolation_T(
        T=T,
        abscissa=abscissa_c,
        ordinate=ordinate_c)
        "Specific heat capacitiy of solid";

    else
      additional_variables.c = SorpLib.Media.Functions.Utilities.cubicSplineInterpolation_T(
        T=T,
        abscissa=abscissa_c,
        ordinate=ordinate_c,
        coefficients=coefficients_cubicSplines_c)
        "Specific heat capacitiy of solid";

    end if;
  end if;

  //
  // Calculate the thermal conductivity
  //
  if approach_lambda == SorpLib.Choices.ThermalConductivitySolid.Constant then
    additional_variables.lambda = lambda_constant
      "Thermal conductivity of solid";

  elseif approach_lambda == SorpLib.Choices.ThermalConductivitySolid.GeneralizedFunction then
    additional_variables.lambda = SorpLib.Media.Functions.Utilities.generalizedFunction_T(
      T=T,
      T_ref=T_ref_lambda,
      z_ref=lambda_ref,
      coefficients=coefficients_lambda,
      exponents=exponents_lambda,
      approach=approach_lambda_function)
      "Thermal conductivity of solid";

  else
    if approach_lambda_interpolation == SorpLib.Choices.InterpolationApproach.Linear then
      additional_variables.lambda = SorpLib.Media.Functions.Utilities.linearInterpolation_T(
        T=T,
        abscissa=abscissa_lambda,
        ordinate=ordinate_lambda)
        "Thermal conductivity of solid";

    else
      additional_variables.lambda = SorpLib.Media.Functions.Utilities.cubicSplineInterpolation_T(
        T=T,
        abscissa=abscissa_lambda,
        ordinate=ordinate_lambda,
        coefficients=coefficients_cubicSplines_lambda)
        "Thermal conductivity of solid";

    end if;
  end if;

  //
  // Calculate caloric and entropic properties
  //
  if calcCaloricProperties then
    //
    // Specific enthalpy: An analytical integral does not always exist or
    // numerical integration is faster
    //
    if approach_c == SorpLib.Choices.SpecificHeatCapacitySolid.Constant then
      state_variables.h = h_ref + additional_variables.c * (T - T_ref) +
        state_variables.v * (p - p_ref)
        "Specific enthalpy of solid";

    elseif approach_c == SorpLib.Choices.SpecificHeatCapacitySolid.GeneralizedFunction then
      state_variables.h = h_ref + state_variables.v * (p - p_ref) +
        Modelica.Math.Nonlinear.quadratureLobatto(
          f=function int_c_dT_generalizedFunction(
            T_ref=T_ref_c,
            z_ref=c_ref,
            coefficients=coefficients_c,
            exponents=exponents_c,
            approach=approach_c_function),
          a=T_ref,
          b=T,
          tolerance=tolerance_int_h)
        "Specific enthalpy of solid";

    else
      if approach_c_interpolation == SorpLib.Choices.InterpolationApproach.Linear then
        state_variables.h = h_ref + state_variables.v * (p - p_ref) +
          Modelica.Math.Nonlinear.quadratureLobatto(
            f=function int_c_dT_linearInterpolation(
              abscissa=abscissa_c,
              ordinate=ordinate_c),
            a=T_ref,
            b=T,
            tolerance=tolerance_int_h)
          "Specific enthalpy of solid";

      else
        state_variables.h = h_ref + state_variables.v * (p - p_ref) +
          Modelica.Math.Nonlinear.quadratureLobatto(
            f=function int_c_dT_cubicSplineInterpolation(
              abscissa=abscissa_c,
              ordinate=ordinate_c,
              coefficients=coefficients_cubicSplines_c),
            a=T_ref,
            b=T,
            tolerance=tolerance_int_h)
          "Specific enthalpy of solid";

      end if;
    end if;

    state_variables.u = state_variables.h - p * state_variables.v
      "Specific internal energy of solid";

  else
    state_variables.h = 0
      "Specific enthalpy of solid";
    state_variables.u = 0
      "Specific internal energy of solid";

  end if;

  //
  // Calculate entropic properties
  //
  if calcCaloricProperties and calcEntropicProperties then
    //
    // Specific entropy: An analytical integral does not always exist or
    // numerical integration is faster
    //
    if approach_c == SorpLib.Choices.SpecificHeatCapacitySolid.Constant then
      state_variables.s = s_ref +
        additional_variables.c * (log(abs(T)) - log(abs(T_ref)))
        "Specific entropy of solid";

    elseif approach_c == SorpLib.Choices.SpecificHeatCapacitySolid.GeneralizedFunction then
      state_variables.s = s_ref +
        Modelica.Math.Nonlinear.quadratureLobatto(
          f=function int_c_div_T_dT_generalizedFunction(
            T_ref=T_ref_c,
            z_ref=c_ref,
            coefficients=coefficients_c,
            exponents=exponents_c,
            approach=approach_c_function),
          a=T_ref,
          b=T,
          tolerance=tolerance_int_s)
        "Specific entropy of solid";

    else
      if approach_c_interpolation == SorpLib.Choices.InterpolationApproach.Linear then
        state_variables.s = s_ref +
          Modelica.Math.Nonlinear.quadratureLobatto(
            f=function int_c_div_T_dT_linearInterpolation(
              abscissa=abscissa_c,
              ordinate=ordinate_c),
            a=T_ref,
            b=T,
            tolerance=tolerance_int_s)
          "Specific entropy of solid";

      else
        state_variables.s = s_ref +
          Modelica.Math.Nonlinear.quadratureLobatto(
            f=function int_c_div_T_dT_cubicSplineInterpolation(
              abscissa=abscissa_c,
              ordinate=ordinate_c,
              coefficients=coefficients_cubicSplines_c),
            a=T_ref,
            b=T,
            tolerance=tolerance_int_s)
          "Specific entropy of solid";

      end if;
    end if;

    state_variables.g = state_variables.h - T * state_variables.s
      "Specific free enthalpy (i.e., Gibbs free energy) of solid";
    state_variables.a = state_variables.u - T * state_variables.s
      "Specific free energy (i.e., Helmholts free energy) of solid";

  else
    state_variables.s = 0
      "Specific entropy of solid";
    state_variables.g = 0
      "Specific free enthalpy (i.e., Gibbs free energy) of solid";
    state_variables.a = 0
      "Specific free energy (i.e., Helmholts free energy) of solid";

  end if;

  //
  // Assertations
  //
  if calcCaloricProperties or calcEntropicProperties then
    if not approach_v == SorpLib.Choices.SpecificVolumeSolid.Constant then
      Modelica.Utilities.Streams.print("Warning: The specific volume is not " +
        "constant. The caloric model equations assume an incompressible " +
        "solid. Thus, the thermodynamic calculation is not consistent.");
    end if;
  end if;

  if calcEntropicProperties then
    if not approach_v == SorpLib.Choices.SpecificVolumeSolid.Constant then
      Modelica.Utilities.Streams.print("Warning: The specific volume is not " +
        "constant. The caloric model equations assume an incompressible " +
        "solid. Thus, the thermodynamic calculation is not consistent.");
    end if;
  end if;

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial model is the base model for calculating thermodynamic properties of solids, 
such as sorbents or metals and metal alloys. In the model, fundamental thermodynamic 
properties can be calculated:
</p>
<ol>
  <li>
  Thermal state variables: <i>p</i>, <i>T</i>, and <i>&rho;</i>.
  </li>
  <li>
  Caloric state variables: <i>h</i> and <i>u</i>.
  </li>
  <li>
  Entropic state variables: <i>s</i>, <i>g</i>, and <i>a</i>.
  </li>
</ol>
<p>
Moreover, the model allows to calculate the specific heat capacity <i>c</i> and thermal 
conductivity <i>&lambda;</i>. Models that inherit properties from this partial models have 
to selected the correct calculation for the thermodynamic properties and to specify 
solid-specific parameters.
</p>

<h4>Implemented calculation approaches for the thermodyanmic properties <i>h</i> and <i>s</i></h4>
<p>
The calculation methods assume an ideal solid:
</p>
<pre>
    h = h<sub>ref</sub>(p<sub>ref</sub>, T<sub>ref</sub>) + &int;_T<sub>ref</sub>^T [c] dT + v * (p - p<sub>ref</sub>);
</pre>
<pre>
    s = s<sub>ref</sub>(T<sub>ref</sub>) + &int;_T<sub>ref</sub>^T [c / T] dT
</pre>
<p>
Herein, <i>h<sub>ref</sub></i> and <i>s<sub>ref</sub></i> are the specific enthalpy and
entropy at reference pressure <i>p<sub>ref</sub></i> and temperature <i>T<sub>ref</sub></i>.
</p>

<h4>Implemented calculation approaches for the thermodyanmic properties <i>&rho;</i>, <i>c</i>, and <i>&lambda;</i></h4>
<p>
Three calculation approaches can be selected for each thermodynamic property:
</p>
<ol>
  <li>
  Constant value.
  </li>
  <li>
  Generalized function.
  </li>
  <li>
  Interpolation.
  </li>
</ol>

<h4>Options regarding the generalized function</h4>
<p>
The
<a href=\"Modelica://SorpLib.Media.Functions.Utilities.generalizedFunction_T\">generalized function</a>
comprises four function types often used for fluid property data calculation:
</p>
<ol>
  <li>
  For <i>approach = PolynomialFunction_Temperature</i>:
  <br>
  <pre>z = z<sub>ref</sub> * &sum;<sub>i</sub> a<sub>i</sub> * T ^ (b<sub>i</sub>);</pre>
  <br>
  </li>
  <li>
  For <i>approach = PolynomialFunction_ReducedTemperature</i>:
  <br>
  <pre>z = z<sub>ref</sub> * &sum;<sub>i</sub> a<sub>i</sub> * (1 - T / T<sub>ref</sub>) ^ (b<sub>i</sub>);</pre>
  <br>
  </li>
  <li>
  For <i>approach = ExponentialFunction_Temperature</i>:
  <br>
  <pre>z = z<sub>ref</sub> * <strong>exp</strong>(&sum;<sub>i</sub> a<sub>i</sub> * T ^ (b<sub>i</sub>));</pre>
  <br>
  </li>
  <li>
  For <i>approach = ExponentialFunction_ReducedTemperature</i>:
  <br>
  <pre>z = z<sub>ref</sub> * <strong>exp</strong>(&sum;<sub>i</sub> a<sub>i</sub> * (1 - T / T<sub>ref</sub>) ^ (b<sub>i</sub>));</pre>
  <br>
  </li>
</ol>
<p>
Herein, <i>z<sub>ref</sub></i> is either a pre-factor for approaches that use the 
temperature or the fluid property at reference temperature <i>T<sub>ref</sub></i> 
for approaches that use the reduced temperature. The vectors <i>a</i> and <i>b</i>
contain the coefficents and exponents for each summand of the sum.
</p>

<h4>Options regarding interpolation</h4>
<p>
<a href=\"Modelica://SorpLib.Media.Functions.Utilities.linearInterpolation_T\">Linear</a>
or 
<a href=\"Modelica://SorpLib.Media.Functions.Utilities.cubicSplineInterpolation_T\">cubic spline interpolation</a>
can be selected.
</p>

<h4>Assumptions and limitations</h4>
<ul>
  <li>
  Currently, the thermodynamic properties <i>&rho;</i>, <i>c</i>, and <i>&lambda;</i> do only depend on the temperature and not on the pressrure.
  </li>
  <li>
  When calculating caloric and entropic state properties, the desnity is assumed to be constant (i.e., incompressible solid).
  </li>
</ul>
</html>",
        revisions="<html>
<ul>
  <li>
  November 21, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
  <li>
  January 25, 2021, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end PartialSolid;
