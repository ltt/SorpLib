﻿within SorpLib.Media.Solids.Sorbents;
model RDSilicaGel
  "Model of a regular density silica gel (e.g., silica gel 123)"
  extends BaseClasses.PartialSolid(
    v_constant=1/725,
    coefficients_v={v_constant},
    exponents_v={0},
    abscissa_v={0,1000},
    ordinate_v={v_constant,v_constant},
    approach_c=SorpLib.Choices.SpecificHeatCapacitySolid.GeneralizedFunction,
    final approach_c_function=SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionTemperature,
    final c_constant=934,
    final T_ref_c=293.15,
    final c_ref=1,
    final coefficients_c={655.814203422836,1.21401945870847,-7816005.43610635},
    final exponents_c={0,1,-2},
    approach_c_interpolation=SorpLib.Choices.InterpolationApproach.CubicSplines,
    final abscissa_c={303.15,308.15,313.15,318.15,323.15,328.15,333.15,338.15,
        343.15,348.15,353.15,358.15,363.15,368.15,373.15},
    final ordinate_c={934,951,958,966,974,982,990,997,1006,1013,1020,1029,1037,
        1045,1055},
    lambda_constant=0.12,
    coefficients_lambda={lambda_constant},
    exponents_lambda={0},
    abscissa_lambda={0,1000},
    ordinate_lambda={lambda_constant,lambda_constant});

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This model describes regular density silica gel, such as silica gel 123. The properties
density and thermal conductivity are fixed to values given by Schawe (1999). The specific
heat capacity is temperature dependt according to measurements by Islam et al. (2020). Note
that the density describes the true particle density.
</p>

<h4>References</h4>
<ul>
  <li>
  Schawe, D. (1999). Theoretical and Experimental Investigations of an Adsorption Heat Pump with Heat Transfer between two Adsorbers, PhD Thesis, Stuttgart.
  </li>
  <li>
  Islam, M.A. and Pal, A. and Saha, B.B. (2020). Experimental study on thermophysical and porous properties of silica gels, International Journal of Refrigeration, 110:277–28. DOI https://doi.org/10.1016/j.ijrefrig.2019.10.027.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  November 21, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end RDSilicaGel;
