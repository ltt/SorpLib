within SorpLib.Media.Solids.Sorbents;
model GenericSorbent "Model of a generic solid sorbent"
  extends BaseClasses.PartialSolid(
    v_constant=1/2200,
    coefficients_v = {v_constant},
    exponents_v = {0},
    abscissa_v = {0, 1000},
    ordinate_v = {v_constant, v_constant},
    c_constant=1000,
    coefficients_c = {c_constant},
    exponents_c = {0},
    abscissa_c = {0, 1000},
    ordinate_c = {c_constant, c_constant},
    lambda_constant=0.12,
    coefficients_lambda = {lambda_constant},
    exponents_lambda = {0},
    abscissa_lambda = {0, 1000},
    ordinate_lambda = {lambda_constant, lambda_constant});

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This model can be used a generic sorbent model. This model is useful if the exact 
thermodynamic properties as a function of temperature are not known or if individual 
parameters are determined during parameter estimation. Note that the density 
describes the true particle density.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 21, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end GenericSorbent;
