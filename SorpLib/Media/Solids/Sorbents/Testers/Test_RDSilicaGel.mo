within SorpLib.Media.Solids.Sorbents.Testers;
model Test_RDSilicaGel "Tester for the model 'RDSilicaGel'"
  extends SorpLib.Media.Solids.BaseClasses.PartialTest(
    redeclare final model Solid = SorpLib.Media.Solids.Sorbents.RDSilicaGel);

  //
  // Annotations
  //
  annotation (experiment(
      StopTime=20,
      Tolerance=1e-06),
Documentation(revisions="<html>
<ul>
  <li>
  November 21, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>", info="<html>
<p>
This tester shows the behavior of the 'RDSilicaGel' model. The main 
approaches for calculating thermodynamic property data are demonstrated: 
Constant values, generic functions, and interpolation.
<br/><br/>
To see the model behavior, plot the variables of the models over the 
time. The simulation time is correctly preset (Start: 0 s, Stop = 20 s). 
</p>
</html>"));
end Test_RDSilicaGel;
