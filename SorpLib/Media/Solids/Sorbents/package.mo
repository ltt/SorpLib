within SorpLib.Media.Solids;
package Sorbents "Parametrized sorbent models"
  extends Modelica.Icons.VariantsPackage;

  annotation (Documentation(info="<html>
<p>
This package contains parametrized solid models describing sorbents. The sorbent models 
already implemented can be found in the package content. 
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end Sorbents;
