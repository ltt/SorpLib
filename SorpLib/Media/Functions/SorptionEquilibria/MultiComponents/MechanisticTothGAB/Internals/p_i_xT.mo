within SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.MechanisticTothGAB.Internals;
function p_i_xT
  "Mechanistic Toth-GAB isotherm model developed by Young et al. (2021): Partial pressures as function of uptakes and temperature"
  extends SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialMulti;

  //
  // Definition of inputs
  //
  input SorpLib.Units.Uptake[size(c,2)] x_adsorpt
    "Equilibrium uptakes of the adsorpt phase"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of outputs
  //
  output Modelica.Units.SI.Pressure[size(c,2)] p_i
    "Equilibrium pressures of the adsorpt phase"
    annotation (Dialog(tab="General", group="Outputs", enable=false));

  //
  // Definition of variables
  //
protected
  Real Phi_available(unit="1")
    "Fraction of sites available for adsorption";
  Real A_CO2(unit="1/Pa")
    "Enhancement factor of saturation uptake of CO2";

  Real DH_avg(unit="J/mol")
    "Average isosteric heat of adsorption";
  Real B_CO2(unit="1/Pa")
    "Toth coefficient";

algorithm
  //
  // First, calculate the equilibrium pressure of component 1 (i.e., CO2)
  //
  Phi_available := c[3,1] - c[5,1] *
    (1 - Modelica.Math.exp(-(c[7,1] * x_adsorpt[2]) ^ c[8,1]))
    "Fraction of sites available for adsorption";
  A_CO2 := (c[4,1] + (Phi_available - c[4,1]) *
    Modelica.Math.exp(-c[6,1] / x_adsorpt[2])) / c[4,1]
    "Enhancement factor of saturation uptake of CO2";

  DH_avg := (1 - Modelica.Math.exp(-c[6,1] / x_adsorpt[2])) * c[10,1] +
    Modelica.Math.exp(-c[6,1] / x_adsorpt[2]) * c[11,1]
    "Average isosteric heat of adsorption";
  B_CO2 := c[9,1] * Modelica.Math.exp(-DH_avg / (Modelica.Constants.R * T_adsorpt))
    "Toth coefficient";

  p_i[1] := SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Toth.p_xT(
    x_adsorpt=x_adsorpt[1],
    T_adsorpt=T_adsorpt,
    c={A_CO2 * c[1,1],
       B_CO2,
       c[2,1]},
    p_adsorpt_lb_start=1,
    p_adsorpt_ub_start=10,
    tolerance=100*Modelica.Constants.eps)
    "Calculate equilibrium pressure of CO2";

  p_i[1] := max(p_i[1], p_threshold_min)
    "Limit equilibrium pressure if necessary";

  //
  // Second, calculte the equilibrium pressure of component 2 (i.e., H2O)
  //
  //
  p_i[2] := SorpLib.Media.Functions.SorptionEquilibria.PureComponents.GAB.p_xT(
    x_adsorpt=x_adsorpt[2],
    T_adsorpt=T_adsorpt,
    c={max(c[1,2], p_threshold_min),
       c[2,2],
       c[3,2],
       c[4,2]},
    p_adsorpt_lb_start=1,
    p_adsorpt_ub_start=10,
    tolerance=100*Modelica.Constants.eps)
    "Calculate equilibrium pressure of H2O";

  p_i[2] := max(p_i[2], p_threshold_min)
    "Limit equilibrium pressure if necessary";

  //
  // Assertions
  //
  assert(size(c,2)==2,
        "This function is only valid for the two components CO2 and H2O with this order!",
        level=AssertionLevel.error);

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function calculates the equilibrium pressures <i>p_i</i> (i.e., partial
pressures) as function of the equilibrium uptakes <i>x_adsorpt</i> and the 
equilibrium temperature <i>T_adsorpt</i>. Thus, this function is a inverse of the 
function 'x_pyT.' For full details of the original function 'x_pyT,' check the 
documentation of the function 
<a href=\"Modelica://SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.MechanisticTothGAB.x_pyT\">SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.MechanisticTothGAB.x_pyT</a>.
</p>
</p>
</html>", revisions="<html>
<ul>
  <li>
  August 1, 2024, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end p_i_xT;
