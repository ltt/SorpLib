within SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.Langmuir.Internals;
function p_i_xT
  "Extended Langmuir isotherm model: Partial pressures as function of uptakes and temperature"
   extends SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialMulti;

  //
  // Definition of inputs
  //
  input SorpLib.Units.Uptake[size(c,2)] x_adsorpt
    "Equilibrium uptakes of the adsorpt phase"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of outputs
  //
  output Modelica.Units.SI.Pressure[size(c,2)] p_i
    "Equilibrium pressures of the adsorpt phase"
    annotation (Dialog(tab="General", group="Outputs", enable=false));

  //
  // Definition of variables
  //
protected
  Real[size(c,2), size(c,2)] A
    "Matrix A of A*z = -x_adsorpt";
  Real[size(c,2)] z
    "Vector z of A*z = -x_adsorpt (i.e., z = first substitution)";

algorithm
  //
  // Set up matrix A and solve system of linear equations:
  //
  // To calculate the invesere function of the extended Langmuir isotherm regarding
  // the equilibrium pressure and mole fractions of the independent components of
  // the gas or vapor phase, the term ' c[2,ind] * p_i[ind]' is substituted by
  // 'z.' Thus, a system of linear equations is created that can be solved.
  // Re-substitution allows to calculate partial pressures p_i and, thus, the
  // equilibrium pressure and mole fractions.
  //
  for ind in 1:size(c,2) loop
    A[ind,:] := fill(x_adsorpt[ind], size(c,2))
      "First, fill row with correct uptake";
    A[ind,ind] := x_adsorpt[ind] - c[1,ind]
      "Second, correct diagonale";
  end for;

  z := Modelica.Math.Matrices.solve(A, -1 .* x_adsorpt)
    "Third, solve the system of linear equations";

  //
  // Calculate partial pressures
  //
  for ind in 1:size(c,2) loop
    p_i[ind] := max(z[ind] / c[2,ind], p_threshold_min)
      "Re-substituate values and consider threshold if necessary";
  end for;

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function calculates the equilibrium pressures <i>p_i</i> (i.e., partial
pressures) as function of the equilibrium uptakes <i>x_adsorpt</i> and the 
equilibrium temperature <i>T_adsorpt</i>. Thus, this function is a inverse of the 
function 'x_pyT.' For full details of the original function 'x_pyT,' check the 
documentation of the package 
<a href=\"Modelica://SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.Langmuir\">SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.Langmuir</a>.
</p>
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 7, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end p_i_xT;
