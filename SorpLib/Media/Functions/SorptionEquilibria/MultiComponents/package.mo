within SorpLib.Media.Functions.SorptionEquilibria;
package MultiComponents "Functions required to calculate sorption equlibria for multicomponents"
extends Modelica.Icons.VariantsPackage;

annotation (Documentation(info="<html>
<p>
This package contains isotherm models for multi-component adsorption. Each isotherm 
model is stored as a separate package. There are also test models for each isotherm 
model, which test all functions of the isotherm models regarding their implementation. 
The isotherm models already implemented can be found in the package content. 
</p>

<h4>Implemented functions for each isotherm model</h4>
<p>
The following functions are provided for each isotherm model:
</p>
<ul>
  <li>
  Equilibrium uptakes as function of equilibrium pressure, mole fractions of independent 
  gas phase components, and equilibrium temperature.
  </li>
  <li>
  Equilibrium pressure as function of equilibrium uptakes, mole fractions of independent 
  gas phase components, and eqiulibrium temperature.
  </li>
  <li>
  Mole fractions of independent gas phase components as function of equilibrium pressure,
  equilibrium uptakes, and eqiulibrium temperature.
  </li>
  <li>
  Equilibrium pressure and mole fractions of independent gas phase components as function 
  of equilibrium uptakes and eqiulibrium temperature.
  </li>
  <li>
  Partial derivatives of equilibrium uptakes w.r.t. equilibrium pressure at constant mole 
  fractions and temperature.
  </li>
  <li>
  Partial derivatives of equilibrium uptakes w.r.t. mole fractions of independent gas phase 
  components at constant pressure and temperature.
  </li>
  <li>
  Partial derivatives of equilibrium uptakes w.r.t. equilibrium temperature at constant
  pressure and mole fractions.
  </li>
</ul>

<h4>How to add new isotherm models</h4>
<p>
To add a new isotherm model, duplicate the package of a similar isotherm model that is 
already implemented. Then, customise all functions of the isotherm model. If new 
functions are implemented (e.g., equilibrium temperature as function of equilibrium 
pressure, independent mole fractions, and uptake), add these for all existing isotherm 
models as well. Then, adapt the test models and check the functions of the new isotherm 
model regarding their implementation. Finally, write the documentation for each function, 
model, and package.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end MultiComponents;
