within SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.TothGAB_Schellevis.Internals;
function p_i_xT
  "Toth-GAB isotherm model developed by Schellevis (2023): Partial pressures as function of uptakes and temperature"
  extends SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialMulti;

  //
  // Definition of inputs
  //
  input SorpLib.Units.Uptake[size(c,2)] x_adsorpt
    "Equilibrium uptakes of the adsorpt phase"
    annotation (Dialog(tab="General", group="Inputs"));

  input Modelica.Units.SI.Pressure p_adsorpt_lb_start = 1
    "Lower bound of equilibrium pressure (i.e., root finding))"
    annotation (Dialog(tab="General", group="Numerical inputs"));
  input Modelica.Units.SI.Pressure p_adsorpt_ub_start = 10
    "Upper bound of equilibrium pressure (i.e., root finding))"
    annotation (Dialog(tab="General", group="Numerical inputs"));
  input Real tolerance = 100*Modelica.Constants.eps
    "Tolerance for numerical calculation (i.e., root finding))"
    annotation (Dialog(tab="General", group="Numerical inputs"));

  //
  // Definition of outputs
  //
  output Modelica.Units.SI.Pressure[size(c,2)] p_i
    "Equilibrium pressures of the adsorpt phase"
    annotation (Dialog(tab="General", group="Outputs", enable=false));

  //
  // Definition of functions
  //
protected
  function func_p_CO2_num
    "Function used to find root (i.e., p_adsorpt_CO2) numerically"
    extends Modelica.Math.Nonlinear.Interfaces.partialScalarFunction;
    input Real c[:,:]
      "Coefficients of isotherm model";
    input Modelica.Units.SI.Temperature T_adsorpt
      "Equilibrium temperature";
    input Modelica.Units.SI.Pressure p_adsorpt_H2O
      "Equilibrium pressure of component 2 (i.e., H2O)";
    input SorpLib.Units.Uptake x_adsorpt_CO2
      "Equilibrium uptake of component 1 (i.e., CO2)";

    input Modelica.Units.SI.Pressure p_threshold_min
      "Threshold for partial pressure of all components: If a partial pressure is
      below the threshold, its value is set to the threshold";
  algorithm
    y :=
    SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.TothGAB_Schellevis.Internals.x_CO2_pT(
    p_i=cat(1, {u}, {p_adsorpt_H2O}),
    T_adsorpt=T_adsorpt,
    c=c,
    p_threshold_min=p_threshold_min) - x_adsorpt_CO2
    "Function '0 = x(p_adsorpt_CO2, p_adsorpt_H2O, T_adsorpt, c) - x_adsorpt_CO2' 
     used to find root (i.e., p_adsorpt) numerically";
  end func_p_CO2_num;

  //
  // Definition of variables
  //
  Boolean bound_ok = false
    "= true, if bounds are found such that func_p_CO2_num(lb) < 0 and 
    func_p_CO2_num(ub) > 0";

  Modelica.Units.SI.Pressure p_adsorpt_CO2_lb = p_adsorpt_lb_start
    "Current best lower bound of equilibrium pressure";
  Modelica.Units.SI.Pressure p_adsorpt_CO2_ub = p_adsorpt_ub_start
    "Current best upper bound of equilibrium pressure";

  SorpLib.Units.Uptake x_adsorpt_CO2_lb
    "Equilibrium uptake at current best lower bound of equilibrium pressure";
  SorpLib.Units.Uptake x_adsorpt_CO2_ub
    "Equilibrium uptake at current best upper bound of equilibrium pressure";
    Integer no=0;
algorithm
  //
  // Assertions
  //
  assert(size(c,2)==2,
        "This function is only valid for the two components CO2 and H2O with this order!",
        level=AssertionLevel.error);

  //
  // First, calculte the equilibrium pressure of component 2 (i.e., H2O)
  //
  //
  p_i[2] := SorpLib.Media.Functions.SorptionEquilibria.PureComponents.GAB.p_xT(
    x_adsorpt=x_adsorpt[2],
    T_adsorpt=T_adsorpt,
    c={max(c[1,2], p_threshold_min),
       c[2,2],
       c[3,2],
       c[4,2]},
    p_adsorpt_lb_start=1,
    p_adsorpt_ub_start=10,
    tolerance=100*Modelica.Constants.eps)
    "Calculate equilibrium pressure of H2O";

  p_i[2] := max(p_i[2], p_threshold_min)
    "Limit equilibrium pressure if necessary";

  //
  // Second, find start values such that func_p_CO2_num(lb) < 0 and func_p_CO2_num(ub) > 0
  //
  // Reducing p_adsorpt_CO2 reduces x_adsorpt_CO2 -> func_p_CO2_num(p_adsorpt_CO2)
  // can become < 0
  // Increasing p_adsorpt_CO2 increases x_adsorpt_CO2 -> func_p_CO2_num(p_adsorpt_CO2)
  // can become > 0
  //
  x_adsorpt_CO2_lb:=
    SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.TothGAB_Schellevis.Internals.x_CO2_pT(
    p_i=cat(
      1,
      {p_adsorpt_CO2_lb},
      {p_i[2]}),
    T_adsorpt=T_adsorpt,
    c=c,
    p_threshold_min=p_threshold_min)
    "Equilibrium uptake at current best lower bound of equilibrium pressure";
  x_adsorpt_CO2_ub:=
    SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.TothGAB_Schellevis.Internals.x_CO2_pT(
    p_i=cat(
      1,
      {p_adsorpt_CO2_ub},
      {p_i[2]}),
    T_adsorpt=T_adsorpt,
    c=c,
    p_threshold_min=p_threshold_min)
    "Equilibrium uptake at current best upper bound of equilibrium pressure";

  while not bound_ok loop
    if Modelica.Math.isEqual(s1=x_adsorpt_CO2_lb, s2=x_adsorpt[1], eps=tolerance) then
      p_i[1] := max(p_adsorpt_CO2_lb, p_threshold_min);
      bound_ok := true;
      return;

    elseif Modelica.Math.isEqual(s1=x_adsorpt_CO2_ub, s2=x_adsorpt[1], eps=tolerance) then
      p_i[1] := max(p_adsorpt_CO2_ub, p_threshold_min);
      bound_ok := true;
      return;

    elseif x_adsorpt_CO2_lb-x_adsorpt[1] < 0 and x_adsorpt_CO2_ub-x_adsorpt[1] < 0 then
      p_adsorpt_CO2_lb := p_adsorpt_CO2_ub;
      x_adsorpt_CO2_lb := x_adsorpt_CO2_ub;

      p_adsorpt_CO2_ub := p_adsorpt_CO2_ub*10;
      x_adsorpt_CO2_ub :=
        SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.TothGAB_Schellevis.Internals.x_CO2_pT(
        p_i=cat(1, {p_adsorpt_CO2_ub}, {p_i[2]}),
        T_adsorpt=T_adsorpt,
        c=c,
        p_threshold_min=p_threshold_min);

    elseif x_adsorpt_CO2_lb-x_adsorpt[1] > 0 and x_adsorpt_CO2_ub-x_adsorpt[1] > 0 then
      p_adsorpt_CO2_ub := p_adsorpt_CO2_lb;
      x_adsorpt_CO2_ub := x_adsorpt_CO2_lb;

      p_adsorpt_CO2_lb := if p_adsorpt_CO2_lb > Modelica.Constants.small then
        p_adsorpt_CO2_lb*0.1 else 0;
      x_adsorpt_CO2_lb :=
        SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.TothGAB_Schellevis.Internals.x_CO2_pT(
        p_i=cat(1, {p_adsorpt_CO2_lb}, {p_i[2]}),
        T_adsorpt=T_adsorpt,
        c=c,
        p_threshold_min=p_threshold_min);

    else
      bound_ok := true;

    end if;
  end while;

  //
  // Third, find root in the interval lb <= root <= up
  //
  p_i[1] := Modelica.Math.Nonlinear.solveOneNonlinearEquation(
    function func_p_CO2_num(
      c=c,
      T_adsorpt=T_adsorpt,
      p_adsorpt_H2O=p_i[2],
      x_adsorpt_CO2=x_adsorpt[1],
      p_threshold_min=p_threshold_min),
    p_adsorpt_CO2_lb,
    p_adsorpt_CO2_ub,
    tolerance)
    "Calculation of the equilibrium pressure of the adsorpt phase";

  p_i[1] := max(p_i[1], p_threshold_min)
    "Limit equilibrium pressure if necessary";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function calculates the equilibrium pressures <i>p_i</i> (i.e., partial
pressures) as function of the equilibrium uptakes <i>x_adsorpt</i> and the 
equilibrium temperature <i>T_adsorpt</i>. Thus, this function is a inverse of the 
function 'x_pyT.' For full details of the original function 'x_pyT,' check the 
documentation of the function 
<a href=\"Modelica://SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.TothGAB_Schellevis.x_pyT\">SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.TothGAB_Schellevis.x_pyT</a>.
</p>
</p>
</html>", revisions="<html>
<ul>
  <li>
  August 5, 2024, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end p_i_xT;
