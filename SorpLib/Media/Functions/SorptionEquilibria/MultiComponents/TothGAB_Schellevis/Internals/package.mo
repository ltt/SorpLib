within SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.TothGAB_Schellevis;
package Internals "Internal functions used to avoid redundancy"
  extends Modelica.Icons.InternalPackage;

  annotation (Documentation(info="<html>
<p>
This package contains functions used for various functions of the Toth-GAB isotherm
model. Thus, redundancy of code shall be avoided.
</p>
</html>", revisions="<html>
<ul>
  <li>
  August 5, 2024, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Internals;
