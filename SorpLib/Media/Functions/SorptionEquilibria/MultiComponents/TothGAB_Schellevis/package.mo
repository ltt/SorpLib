within SorpLib.Media.Functions.SorptionEquilibria.MultiComponents;
package TothGAB_Schellevis "Package containing all functions regarding the Toth-GAB isotherm developed by Schellevis (2023) for adsorption of CO2 & H2O"
  extends
  SorpLib.Media.Functions.SorptionEquilibria.Interfaces.PartialMultiComponents;

  //
  // Internal package
  //
  redeclare final function extends x_pyT
    "Toth-GAB isotherm model developed by Schellevis (2023): Uptakes as function of pressure, mole fractions of independent gas phase components, and temperature"
  algorithm
    //
    // First, calculate the equilibrium uptake of component 1 (i.e., CO2)
    //
    x_adsorpt[1] :=
      SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.TothGAB_Schellevis.Internals.x_CO2_pT(
        p_i=p_i,
        T_adsorpt=T_adsorpt,
        c=c,
        p_threshold_min=p_threshold_min)
      "Calculate equilibrium uptake of CO2";

    //
    // Second, calculte the equilibrium uptake of component 2 (i.e., H2O)
    //
    x_adsorpt[2] :=
      SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.TothGAB_Schellevis.Internals.x_H20_pT(
        p_H2O=p_i[2],
        T_adsorpt=T_adsorpt,
        c=c,
        p_threshold_min=p_threshold_min)
      "Calculate equilibrium uptake of H2O limited to its maximal equilibrium uptake";

    //
    // Annotations
    //
    annotation (Inline=false,
  InlineAfterIndexReduction=false,
  LateInline=true,
  inverse(p_adsorpt = SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.TothGAB_Schellevis.p_xyT(x_adsorpt=x_adsorpt, y_i=y_i, T_adsorpt=T_adsorpt, c=c, p_threshold_min=p_threshold_min, p_adsorpt_lb_start=p_adsorpt_lb_start, p_adsorpt_ub_start=p_adsorpt_ub_start, tolerance=tolerance),
          y_i = SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.TothGAB_Schellevis.y_pxT(p_adsorpt=p_adsorpt, x_adsorpt=x_adsorpt, T_adsorpt=T_adsorpt, c=c, p_threshold_min=p_threshold_min, p_adsorpt_lb_start=p_adsorpt_lb_start, p_adsorpt_ub_start=p_adsorpt_ub_start, tolerance=tolerance)));
  end x_pyT;

  redeclare final function extends p_xyT
    "Toth-GAB isotherm model developed by Schellevis (2023): Pressure as function of uptakes, mole fractions of independent gas phase components, and temperature"
  algorithm
    //
    // First, calculte the equilibrium pressure of component 2 (i.e., H2O)
    //
    p_i[2] := SorpLib.Media.Functions.SorptionEquilibria.PureComponents.GAB.p_xT(
      x_adsorpt=x_adsorpt[2],
      T_adsorpt=T_adsorpt,
      c={max(c[1,2], p_threshold_min),
         c[2,2],
         c[3,2],
         c[4,2]},
      p_adsorpt_lb_start=1,
      p_adsorpt_ub_start=10,
      tolerance=100*Modelica.Constants.eps)
      "Calculate equilibrium pressure of H2O";

    p_i[2] := max(p_i[2], p_threshold_min)
      "Limit equilibrium pressure if necessary";

    //
    // Second, calculte the equilibrium pressure of component 1 (i.e., CO2)
    //
    p_i[1] := y_i[1] * (p_i[2] / (1-y_i[1]))
      "Calculate equilibrium pressure of CO2";

    p_i[1] := max(p_i[1], p_threshold_min)
      "Limit equilibrium pressure if necessary";

    //
    // Third, calculate the equilibrium pressure
    //
    p_adsorpt := sum(p_i)
      "Equilibrium pressure";

    //
    // Assertions
    //
    assert(size(c,2)==2,
          "This function is only valid for the two components CO2 and H2O with this order!",
          level=AssertionLevel.error);

    //
    // Annotations
    //
    annotation (Inline=false,
  InlineAfterIndexReduction=false,
  LateInline=true,
  inverse(x_adsorpt = SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.TothGAB_Schellevis.x_pyT(p_adsorpt=p_adsorpt, y_i=y_i, T_adsorpt=T_adsorpt, c=c, p_threshold_min=p_threshold_min, p_adsorpt_lb_start=p_adsorpt_lb_start, p_adsorpt_ub_start=p_adsorpt_ub_start, tolerance=tolerance),
          y_i = SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.TothGAB_Schellevis.y_pxT(p_adsorpt=p_adsorpt, x_adsorpt=x_adsorpt, T_adsorpt=T_adsorpt, c=c, p_threshold_min=p_threshold_min, p_adsorpt_lb_start=p_adsorpt_lb_start, p_adsorpt_ub_start=p_adsorpt_ub_start, tolerance=tolerance)));
  end p_xyT;

  redeclare final function extends y_pxT
    "Toth-GAB isotherm model developed by Schellevis (2023): Mole fractions of independent gas phase components as function of uptakes, pressure, and temperature"
  algorithm
    //
    // First, calculte the equilibrium pressure of component 2 (i.e., H2O)
    //
    p_i[2] := SorpLib.Media.Functions.SorptionEquilibria.PureComponents.GAB.p_xT(
      x_adsorpt=x_adsorpt[2],
      T_adsorpt=T_adsorpt,
      c={max(c[1,2], p_threshold_min),
         c[2,2],
         c[3,2],
         c[4,2]},
      p_adsorpt_lb_start=1,
      p_adsorpt_ub_start=10,
      tolerance=100*Modelica.Constants.eps)
      "Calculate equilibrium pressure of H2O";

    p_i[2] := max(p_i[2], p_threshold_min)
      "Limit equilibrium pressure if necessary";

    //
    // Second, calculte the equilibrium pressure of component 1 (i.e., CO2)
    //
    p_i[1] := (1 - p_i[2] / max(p_adsorpt, p_threshold_min)) * max(p_adsorpt, p_threshold_min)
      "Calculate equilibrium pressure of CO2";

    p_i[1] := max(p_i[1], p_threshold_min)
      "Limit equilibrium pressure if necessary";

    //
    // Third, calculate the independent mole fractions (i.e., mole fraction of
    // component 1)
    //
    y_i[1] := p_i[1] / max(p_adsorpt, p_threshold_min)
      "Mole fractions of independent component in the gas or vapor phase";

    //
    // Assertions
    //
    assert(size(c,2)==2,
          "This function is only valid for the two components CO2 and H2O with this order!",
          level=AssertionLevel.error);

    //
    // Annotations
    //
    annotation (Inline=false,
  InlineAfterIndexReduction=false,
  LateInline=true,
  inverse(x_adsorpt = SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.TothGAB_Schellevis.x_pyT(p_adsorpt=p_adsorpt, y_i=y_i, T_adsorpt=T_adsorpt, c=c, p_threshold_min=p_threshold_min, p_adsorpt_lb_start=p_adsorpt_lb_start, p_adsorpt_ub_start=p_adsorpt_ub_start, tolerance=tolerance),
          p_adsorpt = SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.TothGAB_Schellevis.p_xyT(x_adsorpt=x_adsorpt, y_i=y_i, T_adsorpt=T_adsorpt, c=c, p_threshold_min=p_threshold_min, p_adsorpt_lb_start=p_adsorpt_lb_start, p_adsorpt_ub_start=p_adsorpt_ub_start, tolerance=tolerance)));
  end y_pxT;

  redeclare final function extends py_xT
    "Toth-GAB isotherm model developed by Schellevis (2023): Pressure and mole fractions of independent gas phase components as function of uptakes and temperature"
  algorithm
    p_i :=
      SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.TothGAB_Schellevis.Internals.p_i_xT(
      x_adsorpt=x_adsorpt,
      T_adsorpt=T_adsorpt,
      c=c,
      p_threshold_min=p_threshold_min,
      p_adsorpt_lb_start=p_adsorpt_lb_start,
      p_adsorpt_ub_start=p_adsorpt_ub_start,
      tolerance=tolerance)
      "Partial pressures";

    p_adsorpt := max(sum(p_i), p_threshold_min)
      "Equilibrium pressure";

    for ind in 1:size(c,2)-1 loop
      y_i[ind] := p_i[ind] / p_adsorpt
        "Mole fractions of independent components in the gas or vapor phase";
    end for;

    //
    // Annotations
    //
    annotation (Inline=false,
  InlineAfterIndexReduction=false,
  LateInline=true);
  end py_xT;

  redeclare final function extends dx_dp
    "Toth-GAB isotherm model developed by Schellevis (2023): Partial derivative of uptakes w.r.t. pressure at constant mole fractions and temperature"

    //
    // Definition of variables
    //
protected
    SorpLib.Units.Uptake x_adsorpt_CO2_dry
      "Equilibrium uptake of component 1 (i.e., CO2) under dry conditions";
    SorpLib.Units.DerUptakeByPressure dx_adsorpt_CO2_dry_dp_adsorpt
      "Partial derivative of the equilibrium uptake of component 1 (i.e., CO2)
    w.r.t. the equilibrium pressure";

    Real EF_CO2(unit="1")
      "Enhancement factor of the CO2 uptake";
    Real dEF_CO2_dp_adsorpt_H2O(unit="1/Pa")
      "Partial derivative of the enhancement factor of the CO2 uptake w.r.t. the
    equilibrium pressure of component 2 (i.e., H2O)";
    Real dEF_CO2_dx_adsorpt_CO2_dry(unit="kg/kg")
      "Partial derivative of the enhancement factor of the CO2 uptake w.r.t. the
    equilibrium uptake of component 1 (i.e., CO2)";
    Real dEF_CO2_dp_adsorpt(unit="1/Pa")
      "Partial derivative of the enhancement factor of the CO2 uptake w.r.t. the
    equilibrium pressure";

  algorithm
    //
    // The existing derivative function of the Toth isotherm model corresponds to
    // the partial derivative w.r.t. the partial pressure. Hence, the result must
    // be multiplied by the partial derivative of the partial pressure w.r.t. the
    // equilibrium pressure (i.e., y_i_[1]).
    //
    x_adsorpt_CO2_dry :=
      SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Toth.x_pT(
      p_adsorpt=max(p_i[1], p_threshold_min),
      T_adsorpt=T_adsorpt,
      c={c[2,1],
         c[3,1],
         c[4,1]},
      p_adsorpt_lb_start=1,
      p_adsorpt_ub_start=10,
      tolerance=100*Modelica.Constants.eps)
      "Calculate equilibrium uptake of CO2 under dry conditions";
    dx_adsorpt_CO2_dry_dp_adsorpt := y_i_[1]*
      SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Toth.dx_dp(
      p_adsorpt=max(p_i[1], p_threshold_min),
      T_adsorpt=T_adsorpt,
      c={c[2,1],
         c[3,1],
         c[4,1]})
      "Partial derivative of first component's equilibrium uptake w.r.t. the 
    equilibrium pressure";

    EF_CO2 := 1 + c[5,1] * Modelica.Math.exp(c[6,1] * x_adsorpt_CO2_dry) *
      min(max(p_i[2], p_threshold_min) / max(c[1,1], p_threshold_min), 1)
      "Enhancement factor of the CO2 uptake";
    dEF_CO2_dp_adsorpt_H2O :=c[5, 1] * Modelica.Math.exp(c[6, 1] * x_adsorpt_CO2_dry) /
      max(c[1,1], p_threshold_min)
      "Partial derivative of the enhancement factor of the CO2 uptake w.r.t. the
    equilibrium pressure of component 2 (i.e., H2O)";
    dEF_CO2_dx_adsorpt_CO2_dry :=  c[6,1] * c[5,1] *
      Modelica.Math.exp(c[6,1] * x_adsorpt_CO2_dry) *
      min(max(p_i[2], p_threshold_min) / max(c[1,1], p_threshold_min), 1)
      "Partial derivative of the enhancement factor of the CO2 uptake w.r.t. the
    equilibrium uptake of component 1 (i.e., CO2)";
    dEF_CO2_dp_adsorpt :=
      dEF_CO2_dp_adsorpt_H2O * y_i_[2] +
      dEF_CO2_dx_adsorpt_CO2_dry * dx_adsorpt_CO2_dry_dp_adsorpt
      "Partial derivative of the enhancement factor of the CO2 uptake w.r.t. the
    equilibrium pressure";

    dx_adsorpt_dp_adsorpt[1] :=
      dEF_CO2_dp_adsorpt * x_adsorpt_CO2_dry +
      EF_CO2 * dx_adsorpt_CO2_dry_dp_adsorpt
      "Partial derivative of first component's equilibrium uptake w.r.t. the 
    equilibrium pressure at constant mole fractions and temperature";

    //
    // The existing derivative function of the GAB isotherm model corresponds to
    // the partial derivative w.r.t. the partial pressure. Hence, the result must
    // be multiplied by the partial derivative of the partial pressure w.r.t. the
    // equilibrium pressure (i.e., y_i_[2]).
    //
    dx_adsorpt_dp_adsorpt[2] := y_i_[2] *
      SorpLib.Media.Functions.SorptionEquilibria.PureComponents.GAB.dx_dp(
      p_adsorpt=max(p_i[2], p_threshold_min),
      T_adsorpt=T_adsorpt,
      c={max(c[1,2], p_threshold_min),
         c[2,2],
         c[3,2],
         c[4,2]})
      "Partial derivative of second component's equilibrium uptake w.r.t. the 
    equilibrium pressure at constant mole fractions and temperature";
  end dx_dp;

  redeclare final function extends dx_dy
    "Toth-GAB isotherm model developed by Schellevis (2023): Partial derivative of uptakes w.r.t. mole fractions of independent gas phase components at constant pressure and temperature"

    //
    // Definition of variables
    //
protected
    SorpLib.Units.Uptake x_adsorpt_CO2_dry
      "Equilibrium uptake of component 1 (i.e., CO2) under dry conditions";
    SorpLib.Units.DerUptakeByMolarFraction dx_adsorpt_CO2_dry_dy_i
      "Partial derivative of the equilibrium uptake of component 1 (i.e., CO2)
    w.r.t. the independent mole fraction";

    Real EF_CO2(unit="1")
      "Enhancement factor of the CO2 uptake";
    Real dEF_CO2_dp_adsorpt_H2O(unit="1/Pa")
      "Partial derivative of the enhancement factor of the CO2 uptake w.r.t. the
    equilibrium pressure of component 2 (i.e., H2O)";
    Real dEF_CO2_dx_adsorpt_CO2_dry(unit="kg/kg")
      "Partial derivative of the enhancement factor of the CO2 uptake w.r.t. the
    equilibrium uptake of component 1 (i.e., CO2)";
    Real dEF_CO2_dy_i(unit="mol/mol")
      "Partial derivative of the enhancement factor of the CO2 uptake w.r.t. the
    independent mole fraction";

  algorithm
    //
    // The existing derivative function of the Toth isotherm model corresponds to
    // the partial derivative w.r.t. the partial pressure. Hence, the result must
    // be multiplied by the partial derivative of the partial pressure w.r.t. the
    // independent mole fraction (i.e., p_adsorpt).
    //
    x_adsorpt_CO2_dry :=
      SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Toth.x_pT(
      p_adsorpt=max(p_i[1], p_threshold_min),
      T_adsorpt=T_adsorpt,
      c={c[2,1],
         c[3,1],
         c[4,1]},
      p_adsorpt_lb_start=1,
      p_adsorpt_ub_start=10,
      tolerance=100*Modelica.Constants.eps)
      "Calculate equilibrium uptake of CO2 under dry conditions";
    dx_adsorpt_CO2_dry_dy_i := p_adsorpt*
      SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Toth.dx_dp(
      p_adsorpt=max(p_i[1], p_threshold_min),
      T_adsorpt=T_adsorpt,
      c={c[2,1],
         c[3,1],
         c[4,1]})
      "Partial derivative of first component's equilibrium uptake w.r.t. the 
    independent mole fraction";

    EF_CO2 := 1 + c[5,1] * Modelica.Math.exp(c[6,1] * x_adsorpt_CO2_dry) *
      min(max(p_i[2], p_threshold_min) / max(c[1,1], p_threshold_min), 1)
      "Enhancement factor of the CO2 uptake";
    dEF_CO2_dp_adsorpt_H2O := c[5, 1] * Modelica.Math.exp(c[6, 1] * x_adsorpt_CO2_dry) /
      max(c[1,1], p_threshold_min)
      "Partial derivative of the enhancement factor of the CO2 uptake w.r.t. the
    equilibrium pressure of component 2 (i.e., H2O)";
    dEF_CO2_dx_adsorpt_CO2_dry :=  c[6,1] * c[5,1] *
      Modelica.Math.exp(c[6,1] * x_adsorpt_CO2_dry) *
      min(max(p_i[2], p_threshold_min) / max(c[1,1], p_threshold_min), 1)
      "Partial derivative of the enhancement factor of the CO2 uptake w.r.t. the
    equilibrium uptake of component 1 (i.e., CO2)";
    dEF_CO2_dy_i :=
      dEF_CO2_dp_adsorpt_H2O * (-p_adsorpt) +
      dEF_CO2_dx_adsorpt_CO2_dry * dx_adsorpt_CO2_dry_dy_i
      "Partial derivative of the enhancement factor of the CO2 uptake w.r.t. the
    independent mole fraction";

    dx_adsorpt_dy_i[1,1] :=
      dEF_CO2_dy_i * x_adsorpt_CO2_dry +
      EF_CO2 * dx_adsorpt_CO2_dry_dy_i
      "Partial derivative of first component's equilibrium uptake w.r.t. the 
    independent mole fraction at constant pressure and temperature";

    //
    // The existing derivative function of the GAB isotherm model corresponds to
    // the partial derivative w.r.t. the partial pressure. Hence, the result must
    // be multiplied by the partial derivative of the partial pressure w.r.t. the
    // independent mole fractions (i.e., -p_adsorpt)
    //
    dx_adsorpt_dy_i[2,1] := -p_adsorpt *
      SorpLib.Media.Functions.SorptionEquilibria.PureComponents.GAB.dx_dp(
      p_adsorpt=max(p_i[2], p_threshold_min),
      T_adsorpt=T_adsorpt,
      c={max(c[1,2], p_threshold_min),
         c[2,2],
         c[3,2],
         c[4,2]})
      "Partial derivative of second component's equilibrium uptake w.r.t. the 
    independent mole fractions at constant pressure and temperature";
  end dx_dy;

  redeclare final function extends dx_dT
    "Toth-GAB isotherm model developed by Schellevis (2023): Partial derivative of uptakes w.r.t. temperature at constant pressure and mole fractions"

    //
    // Definition of variables
    //
protected
    SorpLib.Units.Uptake x_adsorpt_CO2_dry
      "Equilibrium uptake of component 1 (i.e., CO2) under dry conditions";
    SorpLib.Units.DerUptakeByTemperature dx_adsorpt_CO2_dry_dT_adsorpt
      "Partial derivative of the equilibrium uptake of component 1 (i.e., CO2)
    w.r.t. the equilibrium temperature";

    Real EF_CO2(unit="1")
      "Enhancement factor of the CO2 uptake";
    Real dEF_CO2_dc1(unit="1/Pa")
      "Partial derivative of the enhancement factor of the CO2 uptake w.r.t. the
    first coefficient of the Toth isotherm model";
    Real dEF_CO2_dc5(unit="1")
      "Partial derivative of the enhancement factor of the CO2 uptake w.r.t. the
    fivth coefficient of the Toth isotherm model";
    Real dEF_CO2_dc6(unit="kg/kg")
      "Partial derivative of the enhancement factor of the CO2 uptake w.r.t. the
    sixth coefficient of the Toth isotherm model";
    Real dEF_CO2_dx_adsorpt_CO2_dry(unit="kg/kg")
      "Partial derivative of the enhancement factor of the CO2 uptake w.r.t. the
    equilibrium uptake of component 1 (i.e., CO2)";
    Real dEF_CO2_dT_adsorpt(unit="1/K")
      "Partial derivative of the enhancement factor of the CO2 uptake w.r.t. the
    equilibrium temperature";

  algorithm
    //
    // The existing derivative functions of the Toth and GAB isotherm models
    // correspond to partial derivatives w.r.t. the equilibrium temperature. Note
    // that for component 1, the uptake is enhanced by an enhancement factor. Thus,
    // the correct partial derivative w.r.t. the equilibrium temperature must be
    // calculated.
    //
    x_adsorpt_CO2_dry :=
      SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Toth.x_pT(
      p_adsorpt=max(p_i[1], p_threshold_min),
      T_adsorpt=T_adsorpt,
      c={c[2,1],
         c[3,1],
         c[4,1]},
      p_adsorpt_lb_start=1,
      p_adsorpt_ub_start=10,
      tolerance=100*Modelica.Constants.eps)
      "Calculate equilibrium uptake of CO2 under dry conditions";
    dx_adsorpt_CO2_dry_dT_adsorpt :=
      SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Toth.dx_dT(
      p_adsorpt=max(p_i[1], p_threshold_min),
      T_adsorpt=T_adsorpt,
      c={c[2,1],
         c[3,1],
         c[4,1]},
      dc_dT_adsorpt={dc_dT_adsorpt[2,1],
                     dc_dT_adsorpt[3,1],
                     dc_dT_adsorpt[4,1]})
      "Partial derivative of first component's equilibrium uptake w.r.t. the 
    equilibrium temperature";

    EF_CO2 := 1 + c[5,1] * Modelica.Math.exp(c[6,1] * x_adsorpt_CO2_dry) *
      min(max(p_i[2], p_threshold_min) / max(c[1,1], p_threshold_min), 1)
      "Enhancement factor of the CO2 uptake";
    dEF_CO2_dc1 := -c[5,1] * Modelica.Math.exp(c[6,1] * x_adsorpt_CO2_dry) *
      max(p_i[2], p_threshold_min) / max(c[1,1], p_threshold_min)^2
      "Partial derivative of the enhancement factor of the CO2 uptake w.r.t. the
    first coefficient of the Toth isotherm model";
    dEF_CO2_dc5 := Modelica.Math.exp(c[6,1] * x_adsorpt_CO2_dry) *
      min(max(p_i[2], p_threshold_min) / max(c[1,1], p_threshold_min), 1)
      "Partial derivative of the enhancement factor of the CO2 uptake w.r.t. the
    fivth coefficient of the Toth isotherm model";
    dEF_CO2_dc6 := c[5,1] * Modelica.Math.exp(c[6,1] * x_adsorpt_CO2_dry) *
      min(max(p_i[2], p_threshold_min) / max(c[1,1], p_threshold_min), 1) *
      x_adsorpt_CO2_dry
      "Partial derivative of the enhancement factor of the CO2 uptake w.r.t. the
    sixth coefficient of the Toth isotherm model";
    dEF_CO2_dx_adsorpt_CO2_dry :=  c[6,1] * c[5,1] *
      Modelica.Math.exp(c[6,1] * x_adsorpt_CO2_dry) *
      min(max(p_i[2], p_threshold_min) / max(c[1,1], p_threshold_min), 1)
      "Partial derivative of the enhancement factor of the CO2 uptake w.r.t. the
    equilibrium uptake of component 1 (i.e., CO2)";
    dEF_CO2_dT_adsorpt :=
      dEF_CO2_dc1 * dc_dT_adsorpt[1,1] +
      dEF_CO2_dc5 * dc_dT_adsorpt[5,1] +
      dEF_CO2_dc6 * dc_dT_adsorpt[6,1] +
      dEF_CO2_dx_adsorpt_CO2_dry * dx_adsorpt_CO2_dry_dT_adsorpt
      "Partial derivative of the enhancement factor of the CO2 uptake w.r.t. the
    equilibrium temperature";
    //Modelica.Utilities.Streams.print(String(dEF_CO2_dx_adsorpt_CO2_dry));

    dx_adsorpt_dT_adsorpt[1] :=
      dEF_CO2_dT_adsorpt * x_adsorpt_CO2_dry +
      EF_CO2 * dx_adsorpt_CO2_dry_dT_adsorpt
      "Partial derivative of first component's equilibrium uptake w.r.t. the 
    equilibrium pressure at constant pressure and mole fractions";

    dx_adsorpt_dT_adsorpt[2] := SorpLib.Media.Functions.SorptionEquilibria.PureComponents.GAB.dx_dT(
      p_adsorpt=max(p_i[2], p_threshold_min),
      T_adsorpt=T_adsorpt,
      c={max(c[1,2], p_threshold_min),
         c[2,2],
         c[3,2],
         c[4,2]},
      dc_dT_adsorpt={dc_dT_adsorpt[1,2],
                     dc_dT_adsorpt[2,2],
                     dc_dT_adsorpt[3,2],
                     dc_dT_adsorpt[4,2]})
      "Partial derivative of second component's equilibrium uptake w.r.t. the 
    equilibrium pressure at constant pressure and mole fractions";
  end dx_dT;
  //
  // Annotations
  //
annotation (Documentation(revisions="<html>
<ul>
  <li>
  August 5, 2024, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>", info="<html>
<p>
The Toth-GAB isotherm model calculates equilibrium uptakes <i>x_adsorpt</i> of
CO<sub>2</sub> and H<sub>2</sub>O on amine-functionalized sorbents as a function 
of the equilibrium pressure <i>p_adsorpt</i>, mole fractions of independent 
components in the gas or vapor phase <i>y_i</i>, and the equilibrium temperature 
<i>T_adsorpt</i>. The model was developed by Schellevis (2023) for modeling of 
direct air capture systems. A modified Toth isotherm model describes the uptake 
of CO<sub>2</sub>, while a GAB isotherm model describes the uptake of H<sub>2</sub>O.
</p>

<h4>Main equations</h4>
<p>
The modified Toth isotherm model has the following form:
</p>
<pre>
    x<sub>adsorpt,CO<sub>2</sub></sub> = (1 + &beta;<sub>H<sub>2</sub>O</sub> * <strong>exp</strong>(&gamma;<sub>CO<sub>2</sub></sub> * x<sub>adsorpt,CO<sub>2</sub>,dry</sub>) * &phi;<sub>H<sub>2</sub>O</sub>(T<sub>adsorpt</sub>)) * x<sub>adsorpt,CO<sub>2</sub>,dry</sub>;
</pre>
<p>
with
</p>
<pre>
    x<sub>adsorpt,CO<sub>2</sub>,dry</sub> = x<sub>sat,CO<sub>2</sub></sub>(T<sub>adsorpt</sub>) * b<sub>CO<sub>2</sub></sub>(T<sub>adsorpt</sub>) * p<sub>adsorpt,CO<sub>2</sub></sub> / ((1 + (b<sub>CO<sub>2</sub></sub>(T<sub>adsorpt</sub>) * p<sub>adsorpt,CO<sub>2</sub></sub>) ^ t<sub>CO<sub>2</sub></sub>(T<sub>adsorpt</sub>)) ^ (1/t<sub>CO<sub>2</sub></sub>(T<sub>adsorpt</sub>)));
</pre>
<pre>
    &phi;<sub>H<sub>2</sub>O</sub>(T<sub>adsorpt</sub>) = p<sub>adsorpt</sub>/p<sub>sat,H<sub>2</sub>O</sub>(T<sub>adsorpt</sub>);
</pre>
<p>
Herein, <i>x<sub>sat,CO<sub>2</sub></sub>(T<sub>adsorpt</sub>)</i> is the saturation 
uptake, <i>b<sub>CO<sub>2</sub></sub>(T<sub>adsorpt</sub>)</i> is the Toth coefficient, 
and <i>t<sub>CO<sub>2</sub></sub>(T<sub>adsorpt</sub>)</i> is the Toth exponent. The
coefficients <i>&beta;<sub>CO<sub>2</sub></sub></i> and <i>&gamma;<sub>H<sub>2</sub>O</sub></i>
describe the enhancement of the CO<sub>2</sub> uptake depending on the relative humidty
<i>&phi;<sub>H<sub>2</sub>O</sub>(T<sub>adsorpt</sub>)</i>. Typical temperature 
dependencies may have the following forms:
</p>
<pre>
    x<sub>sat,CO<sub>2</sub></sub>(T<sub>adsorpt</sub>) =  x<sub>ref,CO<sub>2</sub></sub> * <strong>exp</strong>(&Chi;<sub>CO<sub>2</sub></sub> * (1 - T<sub>adsorpt</sub>/T<sub>ref,CO<sub>2</sub></sub>));
</pre>
<pre>
    b<sub>CO<sub>2</sub></sub>(T<sub>adsorpt</sub>) = b<sub>ref,CO<sub>2</sub></sub> * <strong>exp</strong>(Q<sub>CO<sub>2</sub></sub>/(R * T<sub>ref,CO<sub>2</sub></sub>) * (T<sub>ref,CO<sub>2</sub></sub>/T<sub>adsorpt</sub> - 1));
</pre>
<pre>
    t<sub>CO<sub>2</sub></sub>(T<sub>adsorpt</sub>) = t<sub>ref,CO<sub>2</sub></sub> + &alpha;<sub>CO<sub>2</sub></sub> * (1 - T<sub>ref,CO<sub>2</sub></sub>/T<sub>adsorpt</sub>);
</pre>
<p>
where <i>x<sub>ref,CO<sub>2</sub></sub></i> is the saturation uptake at reference 
temperature <i>T<sub>ref,CO<sub>2</sub></sub></i>, <i>b<sub>ref,CO<sub>2</sub></sub></i> 
is the Toth coefficient at reference temperature, and <i>t<sub>ref,CO<sub>2</sub></sub></i> 
is the Toth exponent at reference temperature. The parameter <i>Q<sub>CO<sub>2</sub></sub></i> 
is a measure for the isosteric adsorption enthalpy at a fractional loading of 
<i>x<sub>adsorpt,CO<sub>2</sub></sub>/x<sub>sat,CO<sub>2</sub></sub>(T<sub>adsorpt</sub>) = 0.0</i>, the 
parameter <i>&Chi;<sub>CO<sub>2</sub></sub></i> describes the change of the saturation 
uptake with temperature, and the parameter <i>&alpha;<sub>CO<sub>2</sub></sub></i> 
describes the change of the Toth exponent with temperature. All seven parameters 
and the two enhancement factors <i>&gamma;<sub>CO<sub>2</sub></sub></i> and
<i>&beta;<sub>H<sub>2</sub>O</sub></i> can be used as fitting parameters.
<br/>
<p>
The GAB isotherm model has the following form:
</p>
</p>
<pre>
    x<sub>adsorpt,H<sub>2</sub>O</sub> = x<sub>mon,H<sub>2</sub>O</sub>(T<sub>adsorpt</sub>) * c<sub>H<sub>2</sub>O</sub>(T<sub>adsorpt</sub>) * k<sub>H<sub>2</sub>O</sub>(T<sub>adsorpt</sub>) * &phi;<sub>H<sub>2</sub>O</sub>(T<sub>adsorpt</sub>) / ((1 - k<sub>H<sub>2</sub>O</sub>(T<sub>adsorpt</sub>) * &phi;<sub>H<sub>2</sub>O</sub>(T<sub>adsorpt</sub>)) * (1 + (c<sub>H<sub>2</sub>O</sub>(T<sub>adsorpt</sub>) - 1) * k<sub>H<sub>2</sub>O</sub>(T<sub>adsorpt</sub>) * &phi;<sub>H<sub>2</sub>O</sub>(T<sub>adsorpt</sub>)));
</pre>
<p>
with
</p>
<pre>
    &phi;<sub>H<sub>2</sub>O</sub>(T<sub>adsorpt</sub>) = p<sub>adsorpt</sub>/p<sub>sat,H<sub>2</sub>O</sub>(T<sub>adsorpt</sub>);
</pre>
<p>
Herein, <i>x<sub>mon,H<sub>2</sub>O</sub>(T<sub>adsorpt</sub>)</i> is the monolayer 
uptake and <i>c<sub>H<sub>2</sub>O</sub>(T<sub>adsorpt</sub>)</i> and 
<i>k<sub>H<sub>2</sub>O</sub>(T<sub>adsorpt</sub>)</i> are affinity coefficients of 
the GAB isotherm model. These three parameters can be modeled independent of temperature. 
When assuming these three parameters to be dependent on temperature, typical temperature 
dependencies may have the following forms:
</p>
<pre>
    x<sub>mon,H<sub>2</sub>O</sub>(T<sub>adsorpt</sub>) =  x<sub>mon,ref,H<sub>2</sub>O</sub> * <strong>exp</strong>(&Chi;<sub>H<sub>2</sub>O</sub> * (1 - T<sub>adsorpt</sub>/T<sub>ref,H<sub>2</sub>O</sub>));
</pre>
<pre>
    c<sub>H<sub>2</sub>O</sub>(T<sub>adsorpt</sub>) = <strong>exp</strong>((E<sub>1,H<sub>2</sub>O</sub> - E<sub>10+,H<sub>2</sub>O</sub>) / (R * T<sub>adsorpt</sub>));
</pre>
<pre>
    k<sub>H<sub>2</sub>O</sub>(T<sub>adsorpt</sub>) = <strong>exp</strong>((E<sub>2-9,H<sub>2</sub>O</sub> - E<sub>10+,H<sub>2</sub>O</sub>) / (R * T<sub>adsorpt</sub>));
</pre>
<p>
with
</p>
<pre>
    E<sub>1,H<sub>2</sub>O</sub> = C<sub>H<sub>2</sub>O</sub> - <strong>exp</strong>(D<sub>H<sub>2</sub>O</sub> * T<sub>adsorpt</sub>);
</pre>
<pre>
    E<sub>2-9,H<sub>2</sub>O</sub> = F<sub>H<sub>2</sub>O</sub> + G<sub>H<sub>2</sub>O</sub> * T<sub>adsorpt</sub>;
</pre>
<pre>
    E<sub>10+,H<sub>2</sub>O</sub> = &Delta;h<sub>vap,H<sub>2</sub>O</sub>(T<sub>adsorpt</sub>);
</pre>
<p>
where <i>x<sub>mon,ref,H<sub>2</sub>O</sub></i> is the monolayer uptake at reference 
temperature <i>T<sub>ref,H<sub>2</sub>O</sub></i> and  <i>&Chi;<sub>H<sub>2</sub>O</sub></i> 
describes the change of the monolayer uptake with temperature. The coefficient 
<i>E<sub>1,H<sub>2</sub>O</sub></i> is the enthalpy of adsorption of the first layer 
and <i>E<sub>2-9,H<sub>2</sub>O</sub></i> is the enthalpy of adsorption of layers 
2-9: These enthalpies of adsorption can be modeled temperature-dependent as shown 
in the example above, with the four fitting parameters <i>C<sub>H<sub>2</sub>O</sub></i>, 
<i>D<sub>H<sub>2</sub>O</sub></i>, <i>E<sub>H<sub>2</sub>O</sub></i>, and <i>F<sub>H<sub>2</sub>O</sub></i>. 
The coefficient <i>E<sub>10+,H<sub>2</sub>O</sub></i> is the enthalpy of adsorption 
for layer 10 or higher layers and is assumed to correspond to the temperature-dependent 
enthalpy of vaporization <i>&Delta;h<sub>vap,H<sub>2</sub>O</sub>(T<sub>adsorpt</sub>)</i>.
</p>

<h4>Required parameter order in function input c[:,no_components]:</h4>
<p>
For component 1 (i.e., CO<sub>2</sub>), the required parameter order in the function 
input <i>c</i> is as follows:
</p>
<ul>
  <li>
  c[1,1] = p<sub>sat,H<sub>2</sub>O</sub>(T<sub>adsorpt</sub>) in Pa
  </li>
  <li>
  c[2,1] = x<sub>sat,CO<sub>2</sub></sub>(T<sub>adsorpt</sub>) in kg/kg
  </li>
  <li>
  c[3,1] = b<sub>CO<sub>2</sub></sub>(T<sub>adsorpt</sub>) in 1/Pa
  </li>
  <li>
  c[4,1] = t<sub>CO<sub>2</sub></sub>(T<sub>adsorpt</sub>) in -
  </li>
  <li>
  c[5,1] = &beta;<sub>CO<sub>2</sub></sub> in -
  </li>
  <li>
  c[6,1] = &gamma;<sub>H<sub>2</sub>O</sub> in kg/kg
  </li>
</ul>
<p>
For component 2 (i.e., H<sub>2</sub>0), the required parameter order in the function 
input <i>c</i> is as follows:
</p>
<ul>
  <li>
  c[1,2] = p<sub>sat,H<sub>2</sub>O</sub>(T<sub>adsorpt</sub>) in Pa
  </li>
  <li>
  c[2,2] = x<sub>mon,H<sub>2</sub>O</sub>(T<sub>adsorpt</sub>) in kg/kg
  </li>
  <li>
  c[3,2] = c<sub>H<sub>2</sub>O</sub>(T<sub>adsorpt</sub>) in -
  </li>
  <li>
  c[4,2] = k<sub>H<sub>2</sub>O</sub>(T<sub>adsorpt</sub>) in -
  </li>
  <li>
  c[5,2] = 0 (i.e., not required)
  </li>
  <li>
  c[6,2] = 0 (i.e., not required)
  </li>
</ul>

<h4>Example</h4>
<p>
The following figure shows the Toth-GAB isotherm model for one parameter set. In 
the upper sub-figure, the equilibrium pressure changes with time. In the centre 
sub-figure, the independent mole fractions change with time. In the lower sub-figure, 
the equilibrium temperature changes with time. The left side shows the uptake of 
component 1 (i.e., CO<sub>2</sub>), and the right side shows the uptake of component 
2 (i.e., H<sub>2</sub>0). 
<br/><br/>
</p>
<img src=\"Modelica://SorpLib/Resources/doc/media_functions_equilibria_multi_toth_gab_s.png\" alt=\"media_functions_equilibria_multi_toth_gab_s.png\">

<h4>References</h4>
<ul>
  <li>
  <li>
  Schellevis, H. M. (2023). CO<sub>2</sub> from air: A process engineering approach, PhD thesis. DOI: http://doi.org/10.3990/1.9789036555852.
  </li>
</ul>
</html>"));
end TothGAB_Schellevis;
