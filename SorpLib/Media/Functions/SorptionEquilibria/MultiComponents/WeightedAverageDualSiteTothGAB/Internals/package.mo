within SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.WeightedAverageDualSiteTothGAB;
package Internals "Internal functions used to avoid redundancy"
  extends Modelica.Icons.InternalPackage;

  annotation (Documentation(info="<html>
<p>
This package contains functions used for various functions of the weighted-
average dual site Toth-GAB isotherm model. Thus, redundancy of code shall 
be avoided.
</p>
</html>", revisions="<html>
<ul>
  <li>
  August 2, 2024, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Internals;
