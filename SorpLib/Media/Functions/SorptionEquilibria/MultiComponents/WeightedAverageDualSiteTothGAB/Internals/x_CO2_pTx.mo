within SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.WeightedAverageDualSiteTothGAB.Internals;
function x_CO2_pTx
  "Weighted-average dual site Toth-GAB isotherm model developed by Young et al. (2021): Equilibrium uptake of component 1 (i.e., CO2) as function of partial pessures and temperature"
  extends SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialMulti;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.Pressure p_CO2
    "Equilibrium pressures of the adsorpt phase"
    annotation (Dialog(tab="General", group="Inputs"));
  input SorpLib.Units.Uptake x_adsorpt_H2O
    "Equilibrium uptake of component 2 (i.e., H2O)"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of outputs
  //
  output SorpLib.Units.Uptake x_adsorpt_CO2
    "Equilibrium uptake of component 1 (i.e., CO2)"
    annotation (Dialog(tab="General", group="Outputs", enable=false));

algorithm
  //
  // Calculate equilibrium uptake
  //
  x_adsorpt_CO2 := (1 - Modelica.Math.exp(-c[4,1] / x_adsorpt_H2O)) *
    SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Toth.x_pT(
    p_adsorpt=max(p_CO2, p_threshold_min),
    T_adsorpt=T_adsorpt,
    c={c[1,1],
       c[2,1],
       c[3,1]},
    p_adsorpt_lb_start=1,
    p_adsorpt_ub_start=10,
    tolerance=100*Modelica.Constants.eps) + Modelica.Math.exp(-c[4,1] / x_adsorpt_H2O) *
    SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Toth.x_pT(
    p_adsorpt=max(p_CO2, p_threshold_min),
    T_adsorpt=T_adsorpt,
    c={c[5,1],
       c[6,1],
       c[7,1]},
    p_adsorpt_lb_start=1,
    p_adsorpt_ub_start=10,
    tolerance=100*Modelica.Constants.eps)
    "Calculate equilibrium uptake of CO2";

  //
  // Assertions
  //
  assert(size(c,2)==2,
        "This function is only valid for the two components CO2 and H2O with this order!",
        level=AssertionLevel.error);

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function calculates the equilibrium uptake of component 1 (i.e., CO2) 
<i>x_adsorpt_CO2</i> as function of the partial equilibrium pressure of CO2 <i>p_CO2</i>,
the equilibrium uptake of component 2 (i.e., H2O) <i>x_adsorpt_H2O</i>, and the 
equilibrium temperature <i>T_adsorpt</i>. For full details of the isotherm model, 
check the documentation of the function 
<a href=\"Modelica://SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.WeightedAverageDualSiteTothGAB.x_pyT\">SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.WeightedAverageDualSiteTothGAB.x_pyT</a>.
</p>
</p>
</html>", revisions="<html>
<ul>
  <li>
  August 2, 2024, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end x_CO2_pTx;
