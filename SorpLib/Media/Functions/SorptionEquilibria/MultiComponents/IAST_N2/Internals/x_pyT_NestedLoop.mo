﻿within SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.IAST_N2.Internals;
function x_pyT_NestedLoop
  "IAST for two components: Uptakes as function of pressure, mole fractions of independent gas phase components, and temperature using the 'Nested Loop' algorithm"
  extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialMultiIAST;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.Pressure p_adsorpt
    "Equilibrium temperature of the adsorpt phase"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.MoleFraction[:] y_i
    "Mole fractions of the components in the gas or vapor phase"
    annotation (Dialog(tab="General", group="Inputs"));

  input Boolean flag_startValues = false
    " = true, if start values are given; otherwise, estimate start values"
    annotation (Dialog(tab="General", group="Inputs - Start values"));
  input Modelica.Units.SI.Pressure[size(M_i,1)] p_i_pure_0=
    fill(1, size(M_i,1))
    "Hypothetical pure component pressures"
    annotation (Dialog(tab="General", group="Inputs - Start values",
                enable=flag_startValues));
  input SorpLib.Units.ReducedSpreadingPressure pi_0 = 1
    "Reduced spreading pressure of all components"
    annotation (Dialog(tab="General", group="Inputs - Start values",
                enable=flag_startValues));

  //
  // Definition of outputs
  //
  output SorpLib.Units.Uptake[size(M_i,1)] x_adsorpt
    "Equilibrium uptakes of the adsorpt phase"
    annotation (Dialog(tab="General", group="Outputs", enable=false));
  output Modelica.Units.SI.Pressure[size(M_i,1)] p_i_pure
    "Hypothetical pure component pressures"
    annotation (Dialog(tab="General", group="Outputs", enable=false));
  output SorpLib.Units.ReducedSpreadingPressure pi
    "Reduced spreading pressure of all components"
    annotation (Dialog(tab="General", group="Outputs", enable=false));

  //
  // Definition of variables
  //
protected
  Real[size(M_i,1)] K_i(each unit="mol/(kg.Pa)")
    "Henry constants of the components";
  Real K_avg(unit="mol/(kg.Pa)")
    "Average Henry constant";

  SorpLib.Units.ReducedSpreadingPressure[size(M_i,1)] pi_i
    "Reduced spreading pressures";

  Integer no_inner = 1
    "Counter of inner loop";
  Integer no_outer = 1
    "Counter of outer loop";

  SorpLib.Units.ReducedSpreadingPressure error_inner = 1
    "Error of inner loop";
  Modelica.Units.SI.MoleFraction error_outer = 1
    "Error of outer loop";

  SorpLib.Units.ReducedSpreadingPressure[size(M_i,1)] delta_pi_i
    "Difference between reduced spreading pressures and reduced spreading pressure
    of all components";
  Real[size(M_i,1)] ddelta_pi_i_dp_i_pure(each unit="mol/(kg.Pa)")
    "Partial derivative of difference between reduced spreading pressures and 
    reduced spreading pressure of all components w.r.t. the hypothetical pure
    component pressures";

  Modelica.Units.SI.MoleFraction[size(M_i,1)] z_i
    "Mole fractions of the adsorpt phase";

  SorpLib.Units.MolarUptake[size(M_i,1)] q_adsorpt_i
    "Molar equilibrium uptakes at hypothetical pure component pressures";
  SorpLib.Units.MolarUptake q_adsorpt
    "Total molar equilibrium uptake";

algorithm
  if flag_startValues then
    //
    // Use start values
    //
    p_i_pure := p_i_pure_0
      "Hypothetical pure component pressures";
    pi := pi_0
      "Reduced spreading pressure of all components";

  else
    //
    // Caclulate initial guesses ensuring convergence: Henry constants
    //
    K_i[1] := 1/M_i[1] * func_dx_dp_1(p_adsorpt=num.p_K_0,
      T_adsorpt=T_adsorpt,
      c=c_1)
      "Henry constant of the first component";
    K_i[2] := 1/M_i[2] * func_dx_dp_2(p_adsorpt=num.p_K_0,
      T_adsorpt=T_adsorpt,
      c=c_2)
      "Henry constant of the second component";

    K_avg := sum(y_i .* K_i)
      "Average Henry constant";

    //
    // Caclulate initial guesses ensuring convergence: Reduced spreading pressures
    // and hypothetical pure component pressures
    //
    p_i_pure := p_adsorpt .* K_avg ./ K_i
      "Hypothetical pure component pressures";

    pi_i[1] := func_pi_pT_1(M_adsorptive=M_i[1],
      p_adsorpt=p_i_pure[1],
      T_adsorpt=T_adsorpt,
      c=c_1,
      integral_pi_lb=num_comp_1.integral_pi_lb,
      tolerance=num_comp_1.tolerance_pi)
      "Reduced spreading pressure of the first component";
    pi_i[2] := func_pi_pT_2(M_adsorptive=M_i[2],
      p_adsorpt=p_i_pure[2],
      T_adsorpt=T_adsorpt,
      c=c_2,
      integral_pi_lb=num_comp_2.integral_pi_lb,
      tolerance=num_comp_2.tolerance_pi)
      "Reduced spreading pressure of the second component";

    pi := min(pi_i)
      "Reduced spreading pressure of all components";
    p_i_pure := pi ./ K_i
      "Hypothetical pure component pressures";

  end if;

  //
  // Outer loop: Ensure mass conservation
  //
  while error_outer >= num.tolerance_outer and no_outer <= num.no_max_outer loop
    //
    // Inner loop: Ensure identical spreading pressures
    //
    no_inner :=1
      "Counter of inner loop";
    error_inner :=1
      "Error of inner loop";

    while error_inner >= num.tolerance_inner and no_inner <= num.no_max_inner loop
      //
      // Calculate reduced spreading pressures
      //
      pi_i[1] := func_pi_pT_1(M_adsorptive=M_i[1],
        p_adsorpt=p_i_pure[1],
        T_adsorpt=T_adsorpt,
        c=c_1,
        integral_pi_lb=num_comp_1.integral_pi_lb,
        tolerance=num_comp_1.tolerance_pi)
        "Reduced spreading pressure of the first component";
      pi_i[2] := func_pi_pT_2(M_adsorptive=M_i[2],
        p_adsorpt=p_i_pure[2],
        T_adsorpt=T_adsorpt,
        c=c_2,
        integral_pi_lb=num_comp_2.integral_pi_lb,
        tolerance=num_comp_2.tolerance_pi)
        "Reduced spreading pressure of the second component";

      //
      // Apply Newton-Raphson method
      //
      delta_pi_i := pi_i .- pi
        "Difference betwenn reduced spreading pressures and reduced spreading pressure
        of all components";

      ddelta_pi_i_dp_i_pure[1] := 1/M_i[1] * func_x_pT_1(p_adsorpt=p_i_pure[1],
        T_adsorpt=T_adsorpt,
        c=c_1,
        p_adsorpt_lb_start=num_comp_1.p_adsorpt_lb_start,
        p_adsorpt_ub_start=num_comp_1.p_adsorpt_ub_start,
        tolerance=num_comp_1.tolerance_p_adsorpt) / p_i_pure[1]
        "Partial derivative of difference between reduced spreading pressure of the
        first component and reduced spreading pressure of all components w.r.t. the 
        hypothetical pure component pressure of the first component";
      ddelta_pi_i_dp_i_pure[2] := 1/M_i[2] * func_x_pT_2(p_adsorpt=p_i_pure[2],
        T_adsorpt=T_adsorpt,
        c=c_2,
        p_adsorpt_lb_start=num_comp_2.p_adsorpt_lb_start,
        p_adsorpt_ub_start=num_comp_2.p_adsorpt_ub_start,
        tolerance=num_comp_2.tolerance_p_adsorpt) / p_i_pure[2]
        "Partial derivative of difference between reduced spreading pressure of the
        second component and reduced spreading pressure of all components w.r.t. the 
        hypothetical pure component pressure of the second component";

      p_i_pure := p_i_pure .- delta_pi_i ./ ddelta_pi_i_dp_i_pure
        "Hypothetical pure component pressures";

      //
      // Check for convergence
      //
      no_inner := no_inner + 1
        "Counter of inner loop";
      error_inner := sum(abs(delta_pi_i))
        "Error of inner loop";

    end while;

    //
    // Calculate mole fractions of adsorpt phase
    //
    z_i := y_i .* p_adsorpt ./ p_i_pure
      "Mole fractions of the adsorpt phase";

    //
    // Calculate equilibrium uptakes
    //
    q_adsorpt_i[1] := 1/M_i[1] * func_x_pT_1(p_adsorpt=p_i_pure[1],
      T_adsorpt=T_adsorpt,
      c=c_1,
      p_adsorpt_lb_start=num_comp_1.p_adsorpt_lb_start,
      p_adsorpt_ub_start=num_comp_1.p_adsorpt_ub_start,
      tolerance=num_comp_1.tolerance_p_adsorpt)
      "Molar equilibrium uptake at hypothetical pure compoment pressure of the 
      first component";
    q_adsorpt_i[2] := 1/M_i[2] * func_x_pT_2(p_adsorpt=p_i_pure[2],
      T_adsorpt=T_adsorpt,
      c=c_2,
      p_adsorpt_lb_start=num_comp_2.p_adsorpt_lb_start,
      p_adsorpt_ub_start=num_comp_2.p_adsorpt_ub_start,
      tolerance=num_comp_2.tolerance_p_adsorpt)
      "Molar equilibrium uptake at hypothetical pure compoment pressure of the 
      second component";

    q_adsorpt := 1 / sum(z_i ./ q_adsorpt_i)
      "Total molar equilibrium uptake";

    //
    // Apply Newton-Raphson method
    //
    pi := pi + (-(1 - sum(z_i)) / (1/q_adsorpt))
      "Reduced spreading pressure of all components";

    //
    // Check for convergence
    //
    no_outer := no_outer + 1
      "Counter of outer loop";
    error_outer := abs(1 - sum(z_i))
      "Error of outer loop";

  end while;

  //
  // Calculate final ouputs
  //
  x_adsorpt := q_adsorpt .* z_i .* M_i
    "Equilibrium uptakes of the adsorpt phase";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function provides an algorithm for solving the IAST for two components. The 
uptakes <i>x_adsorpt</i> are calculated as a function of the pressure <i>p_adsorpt</i>, 
the molar composition of the gas/vapour phase <i>y_i</i>, and the temperature 
<i>T_adsorpt</i>. The algorithm uses two loops, with the inner loop nested in the
outer loop. Hence, the algorithm is called 'Nested Loop.' The inner loop iterates
over the reduced spreading pressure <i>&pi;</i>, while the outer loop iterates over
the molar composition of the adsorpt phase <i>z<sub><I>i</i></sub></i>.
</p>

<h4>Main equations</h4>
<p>
The algorithm consists of 17 steps:
</p>
<ol>
  <li>
  Calculate the Henry's constant of each component <i>i</i>:
  <pre>K<sub><i>i</i></sub> = 1 / M<sub><i>i</i></sub> * dx<sub><i>i</i></sub>/dp<sub><i>i</i></sub>,</pre>
  where <i>M<sub><i>i</i></sub></i> is the molar mass and <i>dx<sub><i>i</i></sub>/dp<sub><i>i</i></sub></i> 
  is the partial derivative of the uptake with respect to the pressure.
  <br>
  </li>
  <li>
  Calculate the average Henry's constant:
  <pre>K<sub>avg</sub> = &sum;<sub><i>i</i></sub> y<sub><i>i</i></sub> * K<sub><i>i</i></sub>.</pre>
  <br>
  </li>
  <li>
  Calculate the hypothetical pure component pressures of each component <i>i</i>:
  <pre>p<sup>*</sup><sub><i>i</i></sub> = p<sub>adsorpt</sub> * K<sub>avg</sub> / K<sub><i>i</i></sub>.</pre>
  <br>
  </li>
  <li>
  Calculate the reduced spreading pressures of each component <i>i</i>:
  <pre>&pi;<sub><i>i</i></sub> = f(M<sub><i>i</i></sub>, p<sup>*</sup><sub><i>i</i></sub>, T<sub>adsorpt</sub>).</pre>
  <br>
  </li>
  <li>
  Calculate the initial guess of the reduced spreading pressure:
  <pre>&pi; = <strong>min</strong>(&pi;<sub><i>i</i></sub>).</pre>
  <br>
  </li>
  <li>
  Calculate the initial guesses of the hypothetical pure component pressures of each 
  component <i>i</i>:
  <pre>p<sup>*</sup><sub><i>i</i></sub> = &pi; / K<sub><i>i</i></sub>.</pre>
  <br>
  </li>
  <li>
  Calculate the reduced spreading pressures of each component <i>i</i>:
  <pre>&pi;<sub><i>i</i></sub> = f(M<sub><i>i</i></sub>, p<sup>*</sup><sub><i>i</i></sub>, T<sub>adsorpt</sub>).</pre>
  <br>
  </li>
  <li>
  Calculate the differences between the reduced spreading pressure of each 
  component <i>i</i> and the reduced spreading pressure of the last component:
  <pre>&Delta;&pi;<sub><i>i</i></sub> = &pi;<sub><i>i</i></sub> - &pi;<sub>last</sub>.</pre>
  <br>
  </li>
  <li>
  Calculate the partial derivatives of the differences between the reduced 
  spreading pressure of each component <i>i</i> and the reduced spreading 
  pressure of the last component with respect to the hypothetical pure
  component pressures:
  <pre>d&Delta;&pi;<sub><i>i</i></sub>/dp<sup>*</sup><sub><i>i</i></sub> = 1 / M<sub><i>i</i></sub> * x<sub>adsorpt,<i>i</i></sub>(p<sup>*</sup><sub><i>i</i></sub>, T<sub>adsorpt</sub>) / p<sup>*</sup><sub><i>i</i></sub>.</pre>
  <br>
  </li>
  <li>
  Calculate the hypothetical pure component pressures of each component 
  <i>i</i> of the next iteration step:
  <pre>p<sup>*</sup><sub>next,<i>i</i></sub> = p<sup>*</sup><sub><i>i</i></sub> - &Delta;&pi;<sub><i>i</i></sub> / d&Delta;&pi;<sub><i>i</i></sub>/dp<sup>*</sup><sub><i>i</i></sub>.</pre>
  <br>
  </li>
  <li>
  Check for convergence of the inner loop:
  <pre>&sum;<sub><i>i</i></sub> |&Delta;&pi;<sub><i>i</i></sub>| &le; tolerance.</pre>
  If the convergence criterion is not fulfilled, got to step 7. It the convergence
  criterion is fulfilled, got to step 12.
  <br>
  </li>
  <li>
  Calculate the molar composition of each component <i>i</i> in the adsorpt
  phase:
  <pre>z<sub><i>i</i></sub> = y<sub><i>i</i></sub> * p<sub>adsorpt</sub> / p<sup>*</sup><sub><i>i</i></sub>.</pre>
  <br>
  </li>
  <li>
  Calculate the molar uptake of each component <i>i</i>:
  <pre>q<sub>adsorpt,<i>i</i></sub> = 1 / M<sub><i>i</i></sub> * x<sub>adsorpt,<i>i</i></sub>(p<sup>*</sup><sub><i>i</i></sub>, T<sub>adsorpt</sub>).</pre>
  <br>
  </li>
  <li>
  Calculate the total molar uptake:
  <pre>q<sub>adsorpt,total</sub> = 1 / &sum;<sub><i>i</i></sub> [z<sub><i>i</i></sub> / q<sub>adsorpt,<i>i</i></sub>].</pre>
  <br>
  </li>
  <li>
  Calculate the reduced spreading pressure of the next iteration step:
  <pre>&pi;<sub>next</sub> = &pi; - (1 - &sum; z<sub><i>i</i></sub>) / (1 / q<sub>adsorpt,total</sub>).</pre>
  <br>
  </li>
  <li>
  Check for convergence of the outer loop:
  <pre>|1 - &sum;<sub><i>i</i></sub> z<sub><i>i</i></sub>| &le; tolerance.</pre>
  If the convergence criterion is not fulfilled, got to step 7. It the convergence
  criterion is fulfilled, got to step 17.
  <br>
  </li>
  <li>
  Calculate the uptakes of each component <i>i</i>:
  <pre>x<sub>adsorpt,<i>i</i></sub> = M<sub><i>i</i></sub> * z<sub><i>i</i></sub> * q<sub>adsorpt,total</sub>.</pre>
  </li>
</ol>

<h4>References</h4>
<ul>
  <li>
  Mangano E., Friedrich, D., and Brandani, S. (2015). Robust algorithms for the solution of the ideal adsorbed solution theory equations. AIChE Journal, 61(3): 981–991. DOI: 10.1002/aic.14684.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  November 10, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end x_pyT_NestedLoop;
