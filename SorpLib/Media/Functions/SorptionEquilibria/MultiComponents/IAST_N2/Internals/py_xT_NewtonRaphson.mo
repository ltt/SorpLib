within SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.IAST_N2.Internals;
function py_xT_NewtonRaphson
  "IAST for two components: Uptakes as function of pressure, mole fractions of independent gas phase components, and temperature using the standard 'Newton-Raphson' algorithm"
  extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialMultiIAST;

  //
  // Definition of inputs
  //
  input SorpLib.Units.Uptake[size(M_i,1)] x_adsorpt
    "Equilibrium uptakes of the adsorpt phase"
    annotation (Dialog(tab="General", group="Inputs"));

  input Boolean flag_startValues = false
    " = true, if start values are given; otherwise, estimate start values"
    annotation (Dialog(tab="General", group="Inputs - Start values"));
  input SorpLib.Units.ReducedSpreadingPressure pi_0 = 1
    "Reduced spreading pressure of all components"
    annotation (Dialog(tab="General", group="Inputs - Start values",
                enable=flag_startValues));

  //
  // Definition of outputs
  //
  output Modelica.Units.SI.Pressure p_adsorpt
    "Equilibrium pressure of the adsorpt phase"
    annotation (Dialog(tab="General", group="Outputs", enable=false));
  output Modelica.Units.SI.MoleFraction[size(M_i,1)] y_i
    "Mole fractions of components in the vapor or gas phase"
    annotation (Dialog(tab="General", group="Outputs", enable=false));
  output Modelica.Units.SI.Pressure[size(M_i,1)] p_i_pure
    "Hypothetical pure component pressures"
    annotation (Dialog(tab="General", group="Outputs", enable=false));
  output SorpLib.Units.ReducedSpreadingPressure pi
    "Reduced spreading pressure of all components"
    annotation (Dialog(tab="General", group="Outputs", enable=false));

  //
  // Definition of variables
  //
protected
  SorpLib.Units.ReducedSpreadingPressure[size(M_i,1)] pi_i
    "Reduced spreading pressures";

  Integer no_iteration = 1
    "Counter of loop";
  Real error = 1
    "Error of loop";

  SorpLib.Units.MolarUptake[size(M_i,1)] q_adsorpt_i = x_adsorpt ./ M_i
    "Molar equilibrium uptakes at hypothetical pure component pressures";
  Real[size(M_i,1)] dq_adsorpt_i_dp_i_pure(each unit="mol/(kg.Pa)")
    "Partical derivatives of molar equilibrium uptakes at hypothetical pure 
    component pressures w.r.t. hypothetical pure component pressures";

  Real delta_sum_q_adsorpt(unit="kg/mol")
    "Satisfaction of total molar uptake";
  Real ddelta_sum_q_adsorpt_dpi(unit="kg.kg/(mol.mol)")
    "Partial derivative of satisfaction of total molar uptake w.r.t. to reduced 
    spreading pressure of all components";

  SorpLib.Units.MolarUptake q_adsorpt = sum(q_adsorpt_i)
    "Total molar equilibrium uptake";

  Modelica.Units.SI.MoleFraction[size(M_i,1)] z_i = q_adsorpt_i ./ q_adsorpt
    "Mole fractions of the adsorpt phase";

algorithm
  if flag_startValues then
    //
    // Use start values
    //
    pi := pi_0
      "Reduced spreading pressure of all components";

  else
    //
    // Caclulate initial guesses ensuring convergence: Equilibrium pressures
    //
    p_i_pure[1] := func_p_xT_1(x_adsorpt=x_adsorpt[1],
      T_adsorpt=T_adsorpt,
      c=c_1,
      p_adsorpt_lb_start=num_comp_1.p_adsorpt_lb_start,
      p_adsorpt_ub_start=num_comp_1.p_adsorpt_ub_start,
      tolerance=num_comp_1.tolerance_p_adsorpt)
      "Pure component pressure of the first component (not at hypothetical pressure)";
    p_i_pure[2] := func_p_xT_2(x_adsorpt=x_adsorpt[2],
      T_adsorpt=T_adsorpt,
      c=c_2,
      p_adsorpt_lb_start=num_comp_2.p_adsorpt_lb_start,
      p_adsorpt_ub_start=num_comp_2.p_adsorpt_ub_start,
      tolerance=num_comp_2.tolerance_p_adsorpt)
      "Pure component pressure of the second component (not at hypothetical pressure)";

    //
    // Caclulate initial guesses ensuring convergence: Reduced spreading pressures
    //
    pi_i[1] := func_pi_pT_1(M_adsorptive=M_i[1],
      p_adsorpt=p_i_pure[1],
      T_adsorpt=T_adsorpt,
      c=c_1,
      integral_pi_lb=num_comp_1.integral_pi_lb,
      tolerance=num_comp_1.tolerance_pi)
      "Reduced spreading pressure of the first component";
    pi_i[2] := func_pi_pT_2(M_adsorptive=M_i[2],
      p_adsorpt=p_i_pure[2],
      T_adsorpt=T_adsorpt,
      c=c_2,
      integral_pi_lb=num_comp_2.integral_pi_lb,
      tolerance=num_comp_2.tolerance_pi)
      "Reduced spreading pressure of the second component";

    pi := sum(z_i .* pi_i)
      "Reduced spreading pressure of all components";

  end if;

  //
  // Loop to solve the inverse of the IAST
  //
  while error >= num.tolerance_inv and no_iteration <= num.no_max_inv loop
    //
    // Calculate hypothetical pure component pressures
    //
    p_i_pure[1] := func_p_piT_1(
      M_adsorptive=M_i[1],
      pi=pi,
      T_adsorpt=T_adsorpt,
      c=c_1,
      p_adsorpt_lb_start=num_comp_1.p_adsorpt_lb_start,
      p_adsorpt_ub_start=num_comp_1.p_adsorpt_ub_start,
      integral_pi_lb=num_comp_1.integral_pi_lb,
      tolerance_p_adsorpt=num_comp_1.tolerance_p_adsorpt,
      tolerance_pi=num_comp_1.tolerance_pi)
      "Hypothetical pure component pressure of the first component";
    p_i_pure[2] := func_p_piT_2(
      M_adsorptive=M_i[2],
      pi=pi,
      T_adsorpt=T_adsorpt,
      c=c_2,
      p_adsorpt_lb_start=num_comp_2.p_adsorpt_lb_start,
      p_adsorpt_ub_start=num_comp_2.p_adsorpt_ub_start,
      integral_pi_lb=num_comp_2.integral_pi_lb,
      tolerance_p_adsorpt=num_comp_2.tolerance_p_adsorpt,
      tolerance_pi=num_comp_2.tolerance_pi)
      "Hypothetical pure component pressure of the first component";

    //
    // Calculate molar uptakes
    //
    q_adsorpt_i[1] := 1/M_i[1] * func_x_pT_1(p_adsorpt=p_i_pure[1],
      T_adsorpt=T_adsorpt,
      c=c_1,
      p_adsorpt_lb_start=num_comp_1.p_adsorpt_lb_start,
      p_adsorpt_ub_start=num_comp_1.p_adsorpt_ub_start,
      tolerance=num_comp_1.tolerance_p_adsorpt)
      "Molar equilibrium uptake at hypothetical pure component pressure of the 
      first component";
    q_adsorpt_i[2] := 1/M_i[2] * func_x_pT_2(p_adsorpt=p_i_pure[2],
      T_adsorpt=T_adsorpt,
      c=c_2,
      p_adsorpt_lb_start=num_comp_2.p_adsorpt_lb_start,
      p_adsorpt_ub_start=num_comp_2.p_adsorpt_ub_start,
      tolerance=num_comp_2.tolerance_p_adsorpt)
      "Molar equilibrium uptake at hypothetical pure component pressure of the 
      second component";

    //
    // Calculate partial derivatives of molar uptakes w.r.t. hypothetical pure
    // component pressures
    //
    dq_adsorpt_i_dp_i_pure[1] := 1/M_i[1] * func_dx_dp_1(p_adsorpt=p_i_pure[1],
      T_adsorpt=T_adsorpt,
      c=c_1)
      "Molar equilibrium uptake at hypothetical pure component pressure of the 
      first component";
    dq_adsorpt_i_dp_i_pure[2] := 1/M_i[2] * func_dx_dp_2(p_adsorpt=p_i_pure[2],
      T_adsorpt=T_adsorpt,
      c=c_2)
      "Molar equilibrium uptake at hypothetical pure component pressure of the 
      second component";

    //
    // Apply Newton-Raphson method
    //
    delta_sum_q_adsorpt := sum(z_i ./ q_adsorpt_i) - 1 / q_adsorpt
      "Satisfaction of total molar uptake";
    ddelta_sum_q_adsorpt_dpi := -sum(z_i .* p_i_pure ./ q_adsorpt_i .^ 3 .*
      dq_adsorpt_i_dp_i_pure)
      "Partial derivative of satisfaction of total molar uptake w.r.t. to reduced 
      spreading pressure of all components";

    pi := if pi - delta_sum_q_adsorpt / ddelta_sum_q_adsorpt_dpi > 0 then
      pi - delta_sum_q_adsorpt / ddelta_sum_q_adsorpt_dpi else pi / 2
      "Update reduced spreading pressure of all components";

    //
    // Check for convergence
    //
    no_iteration := no_iteration + 1
      "Counter of loop";
    error := abs(delta_sum_q_adsorpt)
      "Error of loop";

  end while;

  //
  // Calculate final ouputs
  //
  p_adsorpt := sum(z_i .* p_i_pure)
    "Equilibrium pressure of the adsorpt phase";

  y_i := z_i .* p_i_pure ./ p_adsorpt
    "Mole fractions of components in the vapor or gas phase";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function provides an algorithm for solving the inverse of the IAST for two 
components. The pressure <i>p_adsorpt</i> and molar composition of the gas/vapour 
phase <i>y_i</i> are calculated as a function of the uptakes <i>x_adsorpt</i> 
and the temperature <i>T_adsorpt</i>. The algorithm uses a Newton-Raphson method 
to solve the system of equations of the inverse of the IAST. 
</p>

<h4>Main equations</h4>
<p>
The algorithm consists of 13 steps:
</p>
<ol>
  <li>
  Calculate the pure component pressures of each component <i>i</i>:
  <pre>p<sub>adsorpt,<i>i</i></sub> = f(x<sub>adsorpt,<i>i</i></sub>, T<sub>adsorpt</sub>).</pre>
  <br>
  </li>
  <li>
  Calculate the reduced spreading pressures of each component <i>i</i>:
  <pre>&pi;<sub><i>i</i></sub> = f(M<sub><i>i</i></sub>, p<sub>adsorpt,<i>i</i></sub>, T<sub>adsorpt</sub>),</pre>
  where <i>M<sub><i>i</i></sub></i> is the molar mass.
  <br>
  </li>
  </li>
  <li>
  Calculate the initial guess of the reduced spreading pressure:
  <pre>&pi; = &sum;<sub><i>i</i></sub> z<sub><i>i</i></sub> * &pi;<sub><i>i</i></sub>,</pre>
  where <i>z<sub><i>i</i></sub></i> is the molar molar composition of each component 
  <i>i</i> in the adsorpt phase
  <br>
  </li>
  <li>
  Calculate the hypothetical pure component pressures of each component <i>i</i>:
  <pre>p<sup>*</sup><sub><i>i</i></sub> = f(M<sub><i>i</i></sub>, &pi;, T<sub>adsorpt</sub>).</pre>
  This formula is an inverse of the reduced spreading pressure, which often
  has to be calculated numerically.
  <br>
  </li>
  <li>
  Calculate the molar uptake of each component <i>i</i>:
  <pre>q<sub>adsorpt,<i>i</i></sub> = 1 / M<sub><i>i</i></sub> * x<sub>adsorpt,<i>i</i></sub>(p<sup>*</sup><sub><i>i</i></sub>, T<sub>adsorpt</sub>).</pre>
  <br>
  </li>
  <li>
  Calculate partial derivatives of the molar uptake of each component <i>i</i>
  with respect to the hypothetical pure component pressures of each component 
  <i>i</i>:
  <pre>dq<sub>adsorpt,<i>i</i></sub>/dp<sup>*</sup><sub><i>i</i></sub> = 1 / M<sub><i>i</i></sub> * dx<sub>adsorpt,<i>i</i></sub>/dp<sub>adsorpt</sub>(p<sup>*</sup><sub><i>i</i></sub>, T<sub>adsorpt</sub>),</pre>
  where <i>dx<sub>adsorpt,<i>i</i></sub>/dp<sub>adsorpt</sub></i> is the partial
  derivative of the uptake with respect to the pressure.
  <br>
  </li>
  <li>
  Calculate the satisfaction of the total molar uptake:
  <pre>F = &sum;<sub><i>i</i></sub> [z<sub><i>i</i></sub> / q<sub>adsorpt,<i>i</i></sub>] - 1 / &sum;<sub><i>i</i></sub> [x<sub>adsorpt,<i>i</i></sub> / M<sub><i>i</i></sub>].</pre>
  <br>
  </li>
  <li>
  Calculate the partial derivative of the total molar uptake with respect to the 
  reduced spreading pressure:
  <pre>dF/d&pi; = -&sum;<sub><i>i</i></sub> [z<sub><i>i</i></sub> * p<sup>*</sup><sub><i>i</i></sub> / (q<sup>3</sup><sub>adsorpt,<i>i</i></sub>) * dq<sub>adsorpt,<i>i</i></sub>/dp<sup>*</sup><sub><i>i</i></sub>].</pre>
  <br>
  </li>
  <li>
  Calculate the reduced spreading pressure of the next iteration step:
  <pre>&pi;<sub>next</sub> = <strong>if</strong> &pi; - F / dF/d&pi; > 0 <strong>then</strong> &pi; - F / dF/d&pi; > 0 <strong>else</strong> &pi;/2.</pre>
  <br>
  </li>
  <li>
  Check for convergence:
  <pre>|F| &le; tolerance.</pre>
  If the convergence criterion is not fulfilled, got to step 4. It the convergence
  criterion is fulfilled, got to step 11.
  <br>
  </li>
  <li>
  Calculate the partial pressure of each component <i>i</i>:
  <pre>p<sub>adsorpt,<i>i</i></sub> = z<sub><i>i</i></sub> * p<sup>*</sup><sub><i>i</i></sub>.</pre>
  <br>
  </li>
  <li>
  Calculate the pressure:
  <pre>p<sub>adsorpt</sub> = &sum;<sub><i>i</i></sub> p<sub>adsorpt,<i>i</i></sub>.</pre>
  <br>
  </li>
  <li>
  Calculate the molar fractions each component <i>i</i> in the gas/vapor
  phase:
  <pre>y<sub><i>i</i></sub> = p<sub>adsorpt,<i>i</i></sub> / p<sub>adsorpt</sub>.</pre>
  </li>
</ol>

<h4>References</h4>
<ul>
  <li>
  Do, D. D. (1998). Adsorption Analysis: Equilibria and Kinetics, 1st Edition, ISBN 978-1-86094-130-6, Imperial College Press.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  November 10, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end py_xT_NewtonRaphson;
