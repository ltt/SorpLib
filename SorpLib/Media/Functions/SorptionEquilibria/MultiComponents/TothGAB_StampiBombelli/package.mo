﻿within SorpLib.Media.Functions.SorptionEquilibria.MultiComponents;
package TothGAB_StampiBombelli "Package containing all functions regarding the Toth-GAB isotherm developed by Stampi-Bombelli et al. (2020) for adsorption of CO2 & H2O"
  extends
  SorpLib.Media.Functions.SorptionEquilibria.Interfaces.PartialMultiComponents;

  //
  // Internal package
  //
  redeclare final function extends x_pyT
    "Toth-GAB isotherm model developed by Stampi-Bombelli et al. (2020): Uptakes as function of pressure, mole fractions of independent gas phase components, and temperature"
  algorithm
    //
    // First, calculte the equilibrium uptake of component 2 (i.e., H2O) because it
    // is required to calculate the equilibrium uptake of component 1 (i.e., CO2)
    //
    x_adsorpt[2] :=
      SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.TothGAB_StampiBombelli.Internals.x_H20_pT(
      p_i=p_i,
      T_adsorpt=T_adsorpt,
      c=c,
      p_threshold_min=p_threshold_min)
      "Calculate equilibrium uptake of H2O limited to its maximal equilibrium uptake";

    //
    // Second, calculate the equilibrium uptake of component 1 (i.e., CO2)
    //
    x_adsorpt[1] := SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Toth.x_pT(
      p_adsorpt=max(p_i[1], p_threshold_min),
      T_adsorpt=T_adsorpt,
      c={c[1,1] * (1 / (1 - c[4,1]*x_adsorpt[2])),
         c[2,1] * (1 + c[5,1]*x_adsorpt[2]),
         c[3,1]},
      p_adsorpt_lb_start=1,
      p_adsorpt_ub_start=10,
      tolerance=100*Modelica.Constants.eps)
      "Calculate equilibrium uptake of CO2";

    //
    // Annotations
    //
    annotation (Inline=false,
  InlineAfterIndexReduction=false,
  LateInline=true,
  inverse(p_adsorpt = SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.TothGAB_StampiBombelli.p_xyT(x_adsorpt=x_adsorpt, y_i=y_i, T_adsorpt=T_adsorpt, c=c, p_threshold_min=p_threshold_min, p_adsorpt_lb_start=p_adsorpt_lb_start, p_adsorpt_ub_start=p_adsorpt_ub_start, tolerance=tolerance),
          y_i = SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.TothGAB_StampiBombelli.y_pxT(p_adsorpt=p_adsorpt, x_adsorpt=x_adsorpt, T_adsorpt=T_adsorpt, c=c, p_threshold_min=p_threshold_min, p_adsorpt_lb_start=p_adsorpt_lb_start, p_adsorpt_ub_start=p_adsorpt_ub_start, tolerance=tolerance)));
  end x_pyT;

  redeclare final function extends p_xyT
    "Toth-GAB isotherm model developed by Stampi-Bombelli et al. (2020): Pressure as function of uptakes, mole fractions of independent gas phase components, and temperature"
  algorithm
    p_i :=
      SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.TothGAB_StampiBombelli.Internals.p_i_xT(
      x_adsorpt=x_adsorpt,
      T_adsorpt=T_adsorpt,
      c=c,
      p_threshold_min=p_threshold_min) "Partial pressures";

    p_adsorpt := sum(p_i)
      "Equilibrium pressure";

    //
    // Annotations
    //
    annotation (Inline=false,
  InlineAfterIndexReduction=false,
  LateInline=true,
  inverse(x_adsorpt = SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.TothGAB_StampiBombelli.x_pyT(p_adsorpt=p_adsorpt, y_i=y_i, T_adsorpt=T_adsorpt, c=c, p_threshold_min=p_threshold_min, p_adsorpt_lb_start=p_adsorpt_lb_start, p_adsorpt_ub_start=p_adsorpt_ub_start, tolerance=tolerance),
          y_i = SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.TothGAB_StampiBombelli.y_pxT(p_adsorpt=p_adsorpt, x_adsorpt=x_adsorpt, T_adsorpt=T_adsorpt, c=c, p_threshold_min=p_threshold_min, p_adsorpt_lb_start=p_adsorpt_lb_start, p_adsorpt_ub_start=p_adsorpt_ub_start, tolerance=tolerance)));
  end p_xyT;

  redeclare final function extends y_pxT
    "Toth-GAB isotherm model developed by Stampi-Bombelli et al. (2020): Mole fractions of independent gas phase components as function of uptakes, pressure, and temperature"
  algorithm
    p_i :=
      SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.TothGAB_StampiBombelli.Internals.p_i_xT(
      x_adsorpt=x_adsorpt,
      T_adsorpt=T_adsorpt,
      c=c,
      p_threshold_min=p_threshold_min) "Partial pressures";

    for ind in 1:size(c,2)-1 loop
      y_i[ind] := p_i[ind] / max(p_adsorpt, p_threshold_min)
        "Mole fractions of independent components in the gas or vapor phase";
    end for;

    //
    // Annotations
    //
    annotation (Inline=false,
  InlineAfterIndexReduction=false,
  LateInline=true,
  inverse(x_adsorpt = SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.TothGAB_StampiBombelli.x_pyT(p_adsorpt=p_adsorpt, y_i=y_i, T_adsorpt=T_adsorpt, c=c, p_threshold_min=p_threshold_min, p_adsorpt_lb_start=p_adsorpt_lb_start, p_adsorpt_ub_start=p_adsorpt_ub_start, tolerance=tolerance),
          p_adsorpt = SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.TothGAB_StampiBombelli.p_xyT(x_adsorpt=x_adsorpt, y_i=y_i, T_adsorpt=T_adsorpt, c=c, p_threshold_min=p_threshold_min, p_adsorpt_lb_start=p_adsorpt_lb_start, p_adsorpt_ub_start=p_adsorpt_ub_start, tolerance=tolerance)));
  end y_pxT;

  redeclare final function extends py_xT
    "Toth-GAB isotherm model developed by Stampi-Bombelli et al. (2020): Pressure and mole fractions of independent gas phase components as function of uptakes and temperature"
  algorithm
    p_i :=
      SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.TothGAB_StampiBombelli.Internals.p_i_xT(
      x_adsorpt=x_adsorpt,
      T_adsorpt=T_adsorpt,
      c=c,
      p_threshold_min=p_threshold_min) "Partial pressures";

    p_adsorpt := max(sum(p_i), p_threshold_min)
      "Equilibrium pressure";

    for ind in 1:size(c,2)-1 loop
      y_i[ind] := p_i[ind] / p_adsorpt
        "Mole fractions of independent components in the gas or vapor phase";
    end for;

    //
    // Annotations
    //
    annotation (Inline=false,
  InlineAfterIndexReduction=false,
  LateInline=true);
  end py_xT;

  redeclare final function extends dx_dp
    "Toth-GAB isotherm model developed by Stampi-Bombelli et al. (2020): Partial derivative of uptakes w.r.t. pressure at constant mole fractions and temperature"

    //
    // Definition of variables
    //
protected
    SorpLib.Units.Uptake x_adsorpt_H2O
      "Equilibrium uptakes of component 2 (i.e., H2O)";

    Real dc_1_CO2_dp_adsorpt
      "Partial derivative of first coefficient of the Toth isotherm w.r.t. the
    equilibrium pressure";
    Real dc_2_CO2_dp_adsorpt
      "Partial derivative of second coefficient of the Toth isotherm w.r.t. the
    equilibrium pressure";
    Real dc_3_CO2_dp_adsorpt
      "Partial derivative of third coefficient of the Toth isotherm w.r.t. the
    equilibrium pressure";

  algorithm
    //
    // First, calculte the equilibrium uptake of component 2 (i.e., H2O) because it
    // is required to calculate the partial derivative of the equilibrium uptake
    // w.r.t. the equilibrium pressure of component 1 (i.e., CO2)
    //
    x_adsorpt_H2O :=
      SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.TothGAB_StampiBombelli.Internals.x_H20_pT(
      p_i=p_i,
      T_adsorpt=T_adsorpt,
      c=c,
      p_threshold_min=p_threshold_min)
      "Calculate equilibrium uptake of H2O limited to its maximal equilibrium uptake";

    //
    // Second, calculate partial derivatives:
    //
    // The partial derivative of component 2 (i.e., H2O) must be calculated first
    // because it is required to calculate the partial derivative of component 1
    // (i.e., CO2).
    //
    // The existing derivative function of the GAB isotherm model corresponds to
    // the partial derivative w.r.t. the partial pressure. Hence, the result must
    // be multiplied by the partial derivative of the partial pressure w.r.t. the
    // equilibrium pressure (i.e., y_i_[2]).
    //
    dx_adsorpt_dp_adsorpt[2] := y_i_[2] *
      SorpLib.Media.Functions.SorptionEquilibria.PureComponents.GAB.dx_dp(
      p_adsorpt=max(p_i[2], p_threshold_min),
      T_adsorpt=T_adsorpt,
      c={max(c[1,2], p_threshold_min),
         c[2,2],
         c[3,2],
         c[4,2]})
      "Partial derivative of second component's equilibrium uptake w.r.t. the 
    equilibrium pressure at constant mole fractions and temperature";

    //
    // Note that for component 1, the coefficients of the isotherm model are
    // enhanced by the uptake of component 2. Hence, the parameters c depend also
    // on the partial pressure of component 2 and, thus, on the equilibrium
    // pressure. Note that all coefficients are devided by 'y_i_[1]' to compensate
    // the multiplication by 'y_i_[1],' which is the partial derivative of the
    // partial pressure of component 1 w.r.t. the equilibrium pressure.
    //
    dc_1_CO2_dp_adsorpt := c[1,1] * c[4,1] / (c[4,1] * x_adsorpt_H2O - 1) ^ 2 *
      dx_adsorpt_dp_adsorpt[2] / y_i_[1]
      "Partial derivative of first coefficient of the Toth isotherm w.r.t. the
    equilibrium pressure";
    dc_2_CO2_dp_adsorpt := c[2,1] * c[5,1] * dx_adsorpt_dp_adsorpt[2] / y_i_[1]
      "Partial derivative of second coefficient of the Toth isotherm w.r.t. the
    equilibrium pressure";
    dc_3_CO2_dp_adsorpt := 0 / y_i_[1]
      "Partial derivative of third coefficient of the Toth isotherm w.r.t. the
    equilibrium pressure";

    dx_adsorpt_dp_adsorpt[1] := y_i_[1]*
      SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.TothGAB_StampiBombelli.Internals.dx_CO2_dp(
      p_adsorpt=max(p_i[1], p_threshold_min),
      T_adsorpt=T_adsorpt,
      c={c[1, 1]*(1/(1 - c[4, 1]*x_adsorpt_H2O)),c[2, 1]*(1 + c[5, 1]*
        x_adsorpt_H2O),c[3, 1]},
      dc_dp_adsorpt={dc_1_CO2_dp_adsorpt,dc_2_CO2_dp_adsorpt,dc_3_CO2_dp_adsorpt})
      "Partial derivative of first component's equilibrium uptake w.r.t. the 
    equilibrium pressure at constant mole fractions and temperature";
  end dx_dp;

  redeclare final function extends dx_dy
    "Toth-GAB isotherm model developed by Stampi-Bombelli et al. (2020): Partial derivative of uptakes w.r.t. mole fractions of independent gas phase components at constant pressure and temperature"

    //
    // Definition of variables
    //
protected
    SorpLib.Units.Uptake x_adsorpt_H2O
      "Equilibrium uptakes of component 2 (i.e., H2O)";

    Real dc_1_CO2_dp_adsorpt
      "Partial derivative of first coefficient of the Toth isotherm w.r.t. the
    equilibrium pressure";
    Real dc_2_CO2_dp_adsorpt
      "Partial derivative of second coefficient of the Toth isotherm w.r.t. the
    equilibrium pressure";
    Real dc_3_CO2_dp_adsorpt
      "Partial derivative of third coefficient of the Toth isotherm w.r.t. the
    equilibrium pressure";

  algorithm
    //
    // First, calculte the equilibrium uptake of component 2 (i.e., H2O) because it
    // is required to calculate the partial derivative of the equilibrium uptake
    // w.r.t. the independent mole fractions of component 1 (i.e., CO2)
    //
    x_adsorpt_H2O :=
      SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.TothGAB_StampiBombelli.Internals.x_H20_pT(
      p_i=p_i,
      T_adsorpt=T_adsorpt,
      c=c,
      p_threshold_min=p_threshold_min)
      "Calculate equilibrium uptake of H2O limited to its maximal equilibrium uptake";

    //
    // Second, calculate partial derivatives:
    //
    // The partial derivative of component 2 (i.e., H2O) must be calculated first
    // because it is required to calculate the partial derivative of component 1
    // (i.e., CO2).
    //
    // The existing derivative function of the GAB isotherm model corresponds to
    // the partial derivative w.r.t. the partial pressure. Hence, the result must
    // be multiplied by the partial derivative of the partial pressure w.r.t. the
    // independent mole fractions (i.e., -p_adsorpt)
    //
    dx_adsorpt_dy_i[2,1] := -p_adsorpt *
      SorpLib.Media.Functions.SorptionEquilibria.PureComponents.GAB.dx_dp(
      p_adsorpt=max(p_i[2], p_threshold_min),
      T_adsorpt=T_adsorpt,
      c={max(c[1,2], p_threshold_min),
         c[2,2],
         c[3,2],
         c[4,2]})
      "Partial derivative of second component's equilibrium uptake w.r.t. the 
    independent mole fractions at constant pressure and temperature";

    //
    // Note that for component 1, the coefficients of the isotherm model are
    // enhanced by the uptake of component 2. Hence, the parameters c depend also
    // on the partial pressure of component 2 and, thus, on the equilibrium
    // pressure. Note that all coefficients are devided by 'p_adsorpt' to compensate
    // the multiplication by 'p_adsorpt,' which is the partial derivative of the
    // partial pressure of component 1 w.r.t. the equilibrium pressure.
    //
    dc_1_CO2_dp_adsorpt := c[1,1] * c[4,1] / (c[4,1] * x_adsorpt_H2O - 1) ^ 2 *
      dx_adsorpt_dy_i[2,1] / p_adsorpt
      "Partial derivative of first coefficient of the Toth isotherm w.r.t. the
    equilibrium pressure";
    dc_2_CO2_dp_adsorpt := c[2,1] * c[5,1] * dx_adsorpt_dy_i[2,1] / p_adsorpt
      "Partial derivative of second coefficient of the Toth isotherm w.r.t. the
    equilibrium pressure";
    dc_3_CO2_dp_adsorpt := 0 / p_adsorpt
      "Partial derivative of third coefficient of the Toth isotherm w.r.t. the
    equilibrium pressure";

    dx_adsorpt_dy_i[1, 1] := p_adsorpt*
      SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.TothGAB_StampiBombelli.Internals.dx_CO2_dp(
      p_adsorpt=max(p_i[1], p_threshold_min),
      T_adsorpt=T_adsorpt,
      c={c[1, 1]*(1/(1 - c[4, 1]*x_adsorpt_H2O)),c[2, 1]*(1 + c[5, 1]*
        x_adsorpt_H2O),c[3, 1]},
      dc_dp_adsorpt={dc_1_CO2_dp_adsorpt,dc_2_CO2_dp_adsorpt,dc_3_CO2_dp_adsorpt})
      "Partial derivative of first component's equilibrium uptake w.r.t. the 
    independent mole fractions at constant pressure and temperature";
  end dx_dy;

  redeclare final function extends dx_dT
    "Toth-GAB isotherm model developed by Stampi-Bombelli et al. (2020): Partial derivative of uptakes w.r.t. temperature at constant pressure and mole fractions"

    //
    // Definition of variables
    //
protected
    SorpLib.Units.Uptake x_adsorpt_H2O
      "Equilibrium uptakes of component 2 (i.e., H2O)";

    Real dc_1_CO2_dT_adsorpt
      "Partial derivative of first coefficient of the Toth isotherm w.r.t. the
    equilibrium temperature";
    Real dc_2_CO2_dT_adsorpt
      "Partial derivative of second coefficient of the Toth isotherm w.r.t. the
    equilibrium temperature";
    Real dc_3_CO2_dT_adsorpt
      "Partial derivative of third coefficient of the Toth isotherm w.r.t. the
    equilibrium temperature";

  algorithm
    //
    // First, calculte the equilibrium uptake of component 2 (i.e., H2O) because it
    // is required to calculate the partial derivative of the equilibrium uptake
    // w.r.t. the equilibrium temperature of component 1 (i.e., CO2)
    //
    x_adsorpt_H2O :=
      SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.TothGAB_StampiBombelli.Internals.x_H20_pT(
      p_i=p_i,
      T_adsorpt=T_adsorpt,
      c=c,
      p_threshold_min=p_threshold_min)
      "Calculate equilibrium uptake of H2O limited to its maximal equilibrium uptake";

    //
    // Second, calculate partial derivatives:
    //
    // The existing derivative functions of the Toth and GAB isotherm models
    // correspond to partial derivatives w.r.t. the equilibrium temperature. Again,
    // the partial derivative of component 2 (i.e., H2O) must be calculated first
    // because it is required to calculate the partial derivative of component 1
    // (i.e., CO2).
    //
    dx_adsorpt_dT_adsorpt[2] := SorpLib.Media.Functions.SorptionEquilibria.PureComponents.GAB.dx_dT(
      p_adsorpt=max(p_i[2], p_threshold_min),
      T_adsorpt=T_adsorpt,
      c={max(c[1,2], p_threshold_min),
         c[2,2],
         c[3,2],
         c[4,2]},
      dc_dT_adsorpt={dc_dT_adsorpt[1,2],
                     dc_dT_adsorpt[2,2],
                     dc_dT_adsorpt[3,2],
                     dc_dT_adsorpt[4,2]})
      "Partial derivative of second component's equilibrium uptake w.r.t. the 
    equilibrium pressure at constant pressure and mole fractions";

    //
    // Note that for component 1, the coefficients of the isotherm model are
    // enhanced by the uptake of component 2. Hence, the parameter dc_dT cannot
    // directly be passed to the existing derivative function of the Toth isotherm
    // model. Instead, the the correct derivative must be calculated first.
    //
    dc_1_CO2_dT_adsorpt := dc_dT_adsorpt[1,1] * (1 / (1 - c[4,1] * x_adsorpt_H2O)) +
      c[1,1] * (x_adsorpt_H2O / (c[4,1] * x_adsorpt_H2O - 1) ^ 2 * dc_dT_adsorpt[4,1] +
      c[4,1] / (c[4,1] * x_adsorpt_H2O - 1) ^ 2 * dx_adsorpt_dT_adsorpt[2])
      "Partial derivative of first coefficient of the Toth isotherm w.r.t. the
    equilibrium temperature";
    dc_2_CO2_dT_adsorpt := dc_dT_adsorpt[2,1] * (1 + c[5,1] * x_adsorpt_H2O) +
      c[2,1] * (x_adsorpt_H2O * dc_dT_adsorpt[5,1] + c[5,1] * dx_adsorpt_dT_adsorpt[2])
      "Partial derivative of second coefficient of the Toth isotherm w.r.t. the
    equilibrium temperature";
    dc_3_CO2_dT_adsorpt := dc_dT_adsorpt[3,1]
      "Partial derivative of third coefficient of the Toth isotherm w.r.t. the
    equilibrium temperature";

    dx_adsorpt_dT_adsorpt[1] := SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Toth.dx_dT(
      p_adsorpt=max(p_i[1], p_threshold_min),
      T_adsorpt=T_adsorpt,
      c={c[1,1] * (1 / (1 - c[4,1] * x_adsorpt_H2O)),
         c[2,1] * (1 + c[5,1] * x_adsorpt_H2O),
         c[3,1]},
      dc_dT_adsorpt={dc_1_CO2_dT_adsorpt,
                     dc_2_CO2_dT_adsorpt,
                     dc_3_CO2_dT_adsorpt})
      "Partial derivative of first component's equilibrium uptake w.r.t. the 
    equilibrium pressure at constant pressure and mole fractions";
  end dx_dT;
  //
  // Annotations
  //
annotation (Documentation(revisions="<html>
<ul>
  <li>
  July 31, 2024, by Mirko Engelpracht:<br/>
  Adaptations (e.g., object-orientied approach) due to restructuring the library
  and documentation.
  </li>
  <li>
  September 1, 2021, by Patrik Postweiler:<br/>
  First implementation.
  </li>
</ul>
</html>", info="<html>
<p>
The Toth-GAB isotherm model calculates equilibrium uptakes <i>x_adsorpt</i> of
CO<sub>2</sub> and H<sub>2</sub>O on amine-functionalized sorbents as a function 
of the equilibrium pressure <i>p_adsorpt</i>, mole fractions of independent 
components in the gas or vapor phase <i>y_i</i>, and the equilibrium temperature 
<i>T_adsorpt</i>. The model was developed by Stampi-Bombelli et al. (2020) for
modeling of direct air capture systems. A modified Toth isotherm model describes 
the uptake of CO<sub>2</sub>, while a GAB isotherm model describes the uptake of 
H<sub>2</sub>O.
</p>

<h4>Main equations</h4>
<p>
The modified Toth isotherm model has the following form:
</p>
<pre>
    x<sub>adsorpt,CO<sub>2</sub></sub> = x<sub>sat,CO<sub>2</sub></sub>(T<sub>adsorpt</sub>) * A<sub>CO<sub>2</sub></sub> * b<sub>CO<sub>2</sub></sub>(T<sub>adsorpt</sub>) * B<sub>CO<sub>2</sub></sub> * p<sub>adsorpt,CO<sub>2</sub></sub> / ((1 + (b<sub>CO<sub>2</sub></sub>(T<sub>adsorpt</sub>) * B<sub>CO<sub>2</sub></sub> * p<sub>adsorpt,CO<sub>2</sub></sub>) ^ t<sub>CO<sub>2</sub></sub>(T<sub>adsorpt</sub>)) ^ (1/t<sub>CO<sub>2</sub></sub>(T<sub>adsorpt</sub>)));
</pre>
<p>
with
</p>
<pre>
    A<sub>CO<sub>2</sub></sub> = (1 / (1 - &gamma;<sub>CO<sub>2</sub></sub> * x<sub>adsorpt,H<sub>2</sub>O</sub>));
</pre>
<pre>
    B<sub>CO<sub>2</sub></sub> = (1 + &beta;<sub>CO<sub>2</sub></sub>x<sub>adsorpt,H<sub>2</sub>O</sub>);
</pre>
<p>
Herein, <i>x<sub>sat,CO<sub>2</sub></sub>(T<sub>adsorpt</sub>)</i> is the saturation 
uptake, <i>b<sub>CO<sub>2</sub></sub>(T<sub>adsorpt</sub>)</i> is the Toth coefficient, 
and <i>t<sub>CO<sub>2</sub></sub>(T<sub>adsorpt</sub>)</i> is the Toth exponent. The
coefficients <i>&gamma;<sub>CO<sub>2</sub></sub></i> and <i>&beta;<sub>H<sub>2</sub>O</sub></i>
are enhancement factors of the saturation uptake and the Toth coeffient, respectively, both
multiplied by the uptake of water <i>x<sub>adsorpt,H<sub>2</sub>O</sub></i>. Typical 
temperature dependencies may have the following forms:
</p>
<pre>
    x<sub>sat,CO<sub>2</sub></sub>(T<sub>adsorpt</sub>) =  x<sub>ref,CO<sub>2</sub></sub> * <strong>exp</strong>(&Chi;<sub>CO<sub>2</sub></sub> * (1 - T<sub>adsorpt</sub>/T<sub>ref,CO<sub>2</sub></sub>));
</pre>
<pre>
    b<sub>CO<sub>2</sub></sub>(T<sub>adsorpt</sub>) = b<sub>ref,CO<sub>2</sub></sub> * <strong>exp</strong>(Q<sub>CO<sub>2</sub></sub>/(R * T<sub>ref,CO<sub>2</sub></sub>) * (T<sub>ref,CO<sub>2</sub></sub>/T<sub>adsorpt</sub> - 1));
</pre>
<pre>
    t<sub>CO<sub>2</sub></sub>(T<sub>adsorpt</sub>) = t<sub>ref,CO<sub>2</sub></sub> + &alpha;<sub>CO<sub>2</sub></sub> * (1 - T<sub>ref,CO<sub>2</sub></sub>/T<sub>adsorpt</sub>);
</pre>
<p>
where <i>x<sub>ref,CO<sub>2</sub></sub></i> is the saturation uptake at reference 
temperature <i>T<sub>ref,CO<sub>2</sub></sub></i>, <i>b<sub>ref,CO<sub>2</sub></sub></i> 
is the Toth coefficient at reference temperature, and <i>t<sub>ref,CO<sub>2</sub></sub></i> 
is the Toth exponent at reference temperature. The parameter <i>Q<sub>CO<sub>2</sub></sub></i> 
is a measure for the isosteric adsorption enthalpy at a fractional loading of 
<i>x<sub>adsorpt,CO<sub>2</sub></sub>/x<sub>sat,CO<sub>2</sub></sub>(T<sub>adsorpt</sub>) = 0.0</i>, the 
parameter <i>&Chi;<sub>CO<sub>2</sub></sub></i> describes the change of the saturation 
uptake with temperature, and the parameter <i>&alpha;<sub>CO<sub>2</sub></sub></i> 
describes the change of the Toth exponent with temperature. All seven parameters 
and the two enhancement factors <i>&gamma;<sub>CO<sub>2</sub></sub></i> and
<i>&beta;<sub>H<sub>2</sub>O</sub></i> can be used as fitting parameters.
<br/>
<p>
The GAB isotherm model has the following form:
</p>
</p>
<pre>
    x<sub>adsorpt,H<sub>2</sub>O</sub> = x<sub>mon,H<sub>2</sub>O</sub>(T<sub>adsorpt</sub>) * c<sub>H<sub>2</sub>O</sub>(T<sub>adsorpt</sub>) * k<sub>H<sub>2</sub>O</sub>(T<sub>adsorpt</sub>) * &phi;<sub>H<sub>2</sub>O</sub>(T<sub>adsorpt</sub>) / ((1 - k<sub>H<sub>2</sub>O</sub>(T<sub>adsorpt</sub>) * &phi;<sub>H<sub>2</sub>O</sub>(T<sub>adsorpt</sub>)) * (1 + (c<sub>H<sub>2</sub>O</sub>(T<sub>adsorpt</sub>) - 1) * k<sub>H<sub>2</sub>O</sub>(T<sub>adsorpt</sub>) * &phi;<sub>H<sub>2</sub>O</sub>(T<sub>adsorpt</sub>)));
</pre>
<p>
with
</p>
<pre>
    &phi;<sub>H<sub>2</sub>O</sub>(T<sub>adsorpt</sub>) = p<sub>adsorpt</sub>/p<sub>sat,H<sub>2</sub>O</sub>(T<sub>adsorpt</sub>);
</pre>
<p>
Herein, <i>x<sub>mon,H<sub>2</sub>O</sub>(T<sub>adsorpt</sub>)</i> is the monolayer 
uptake and <i>c<sub>H<sub>2</sub>O</sub>(T<sub>adsorpt</sub>)</i> and 
<i>k<sub>H<sub>2</sub>O</sub>(T<sub>adsorpt</sub>)</i> are affinity coefficients of 
the GAB isotherm model. These three parameters can be modeled independent of temperature. 
When assuming these three parameters to be dependent on temperature, typical temperature 
dependencies may have the following forms:
</p>
<pre>
    x<sub>mon,H<sub>2</sub>O</sub>(T<sub>adsorpt</sub>) =  x<sub>mon,ref,H<sub>2</sub>O</sub> * <strong>exp</strong>(&Chi;<sub>H<sub>2</sub>O</sub> * (1 - T<sub>adsorpt</sub>/T<sub>ref,H<sub>2</sub>O</sub>));
</pre>
<pre>
    c<sub>H<sub>2</sub>O</sub>(T<sub>adsorpt</sub>) = <strong>exp</strong>((E<sub>1,H<sub>2</sub>O</sub> - E<sub>10+,H<sub>2</sub>O</sub>) / (R * T<sub>adsorpt</sub>));
</pre>
<pre>
    k<sub>H<sub>2</sub>O</sub>(T<sub>adsorpt</sub>) = <strong>exp</strong>((E<sub>2-9,H<sub>2</sub>O</sub> - E<sub>10+,H<sub>2</sub>O</sub>) / (R * T<sub>adsorpt</sub>));
</pre>
<p>
with
</p>
<pre>
    E<sub>1,H<sub>2</sub>O</sub> = C<sub>H<sub>2</sub>O</sub> - <strong>exp</strong>(D<sub>H<sub>2</sub>O</sub> * T<sub>adsorpt</sub>);
</pre>
<pre>
    E<sub>2-9,H<sub>2</sub>O</sub> = F<sub>H<sub>2</sub>O</sub> + G<sub>H<sub>2</sub>O</sub> * T<sub>adsorpt</sub>;
</pre>
<pre>
    E<sub>10+,H<sub>2</sub>O</sub> = &Delta;h<sub>vap,H<sub>2</sub>O</sub>(T<sub>adsorpt</sub>);
</pre>
<p>
where <i>x<sub>mon,ref,H<sub>2</sub>O</sub></i> is the monolayer uptake at reference 
temperature <i>T<sub>ref,H<sub>2</sub>O</sub></i> and  <i>&Chi;<sub>H<sub>2</sub>O</sub></i> 
describes the change of the monolayer uptake with temperature. The coefficient 
<i>E<sub>1,H<sub>2</sub>O</sub></i> is the enthalpy of adsorption of the first layer 
and <i>E<sub>2-9,H<sub>2</sub>O</sub></i> is the enthalpy of adsorption of layers 
2-9: These enthalpies of adsorption can be modeled temperature-dependent as shown 
in the example above, with the four fitting parameters <i>C<sub>H<sub>2</sub>O</sub></i>, 
<i>D<sub>H<sub>2</sub>O</sub></i>, <i>E<sub>H<sub>2</sub>O</sub></i>, and <i>F<sub>H<sub>2</sub>O</sub></i>. 
The coefficient <i>E<sub>10+,H<sub>2</sub>O</sub></i> is the enthalpy of adsorption 
for layer 10 or higher layers and is assumed to correspond to the temperature-dependent 
enthalpy of vaporization <i>&Delta;h<sub>vap,H<sub>2</sub>O</sub>(T<sub>adsorpt</sub>)</i>.
</p>

<h4>Required parameter order in function input c[:,no_components]:</h4>
<p>
For component 1 (i.e., CO<sub>2</sub>), the required parameter order in the function 
input <i>c</i> is as follows:
</p>
<ul>
  <li>
  c[1,1] = x<sub>sat,CO<sub>2</sub></sub>(T<sub>adsorpt</sub>) in kg/kg
  </li>
  <li>
  c[2,1] = b<sub>CO<sub>2</sub></sub>(T<sub>adsorpt</sub>) in 1/Pa
  </li>
  <li>
  c[3,1] = t<sub>CO<sub>2</sub></sub>(T<sub>adsorpt</sub>) in -
  </li>
  <li>
  c[4,1] = &gamma;<sub>CO<sub>2</sub></sub> in kg/kg
  </li>
  <li>
  c[5,1] = &beta;<sub>H<sub>2</sub>O</sub> in kg/kg
  </li>
</ul>
<p>
For component 2 (i.e., H<sub>2</sub>0), the required parameter order in the function 
input <i>c</i> is as follows:
</p>
<ul>
  <li>
  c[1,2] = p<sub>sat,H<sub>2</sub>O</sub>(T<sub>adsorpt</sub>) in Pa
  </li>
  <li>
  c[2,2] = x<sub>mon,H<sub>2</sub>O</sub>(T<sub>adsorpt</sub>) in kg/kg
  </li>
  <li>
  c[3,2] = c<sub>H<sub>2</sub>O</sub>(T<sub>adsorpt</sub>) in -
  </li>
  <li>
  c[4,2] = k<sub>H<sub>2</sub>O</sub>(T<sub>adsorpt</sub>) in -
  </li>
  <li>
  c[5,2] = 0 (i.e., not required)
  </li>
</ul>

<h4>Example</h4>
<p>
The following figure shows the Toth-GAB isotherm model for one parameter set. In 
the upper sub-figure, the equilibrium pressure changes with time. In the centre 
sub-figure, the independent mole fractions change with time. In the lower sub-figure, 
the equilibrium temperature changes with time. The left side shows the uptake of 
component 1 (i.e., CO<sub>2</sub>), and the right side shows the uptake of component 
2 (i.e., H<sub>2</sub>0). 
<br/><br/>
</p>
<img src=\"Modelica://SorpLib/Resources/doc/media_functions_equilibria_multi_toth_gab_sb.png\" alt=\"media_functions_equilibria_multi_toth_gab_sb.png\">

<h4>References</h4>
<ul>
  <li>
  <li>
  Stampi-Bombelli, V. and van der Spek, M. and Mazzotti, M. (2020). Analysis of direct capture of CO2 from ambient air via steamassisted temperature-vacuum swing adsorption, Adsorption, 26(7):1183–1197. DOI: http://doi.org/10.1007/s10450-020-00249-w.
  </li>
</ul>
</html>"));
end TothGAB_StampiBombelli;
