within SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.TothGAB_StampiBombelli.Internals;
function x_H20_pT
  "Toth-GAB isotherm model developed by Stampi-Bombelli et al. (2020): Equilibrium uptake of component 2 (i.e., H2O) as function of partial pessures and temperature"
  extends SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialMulti;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.Pressure[size(c,2)] p_i
    "Equilibrium pressure of the adsorpt phase"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of outputs
  //
  output SorpLib.Units.Uptake x_adsorpt_H2O
    "Equilibrium uptakes of component 2 (i.e., H2O)"
    annotation (Dialog(tab="General", group="Outputs", enable=false));

  //
  // Definition of variables
  //
protected
  SorpLib.Units.Uptake x_adsorpt_H2O_max=
    SorpLib.Media.Functions.SorptionEquilibria.PureComponents.GAB.x_pT(
    p_adsorpt=max(c[1,2], p_threshold_min),
    T_adsorpt=T_adsorpt,
    c={max(c[1,2], p_threshold_min),
       c[2,2],
       c[3,2],
       c[4,2]},
    p_adsorpt_lb_start=1,
    p_adsorpt_ub_start=10,
    tolerance=100*Modelica.Constants.eps)
    "Maximal equilibrium uptake of H2O achieved at relative humidity of 1 (i.e.,
    p_adsorpt_H2O = p_sat_H2O";

algorithm
  //
  // First, calculte the equilibrium uptake of component 2 (i.e., H2O) because it
  // is required to calculate the equilibrium uptake of component 1 (i.e., CO2)
  //
  //
  x_adsorpt_H2O := SorpLib.Media.Functions.SorptionEquilibria.PureComponents.GAB.x_pT(
    p_adsorpt=max(p_i[2], p_threshold_min),
    T_adsorpt=T_adsorpt,
    c={max(c[1,2], p_threshold_min),
       c[2,2],
       c[3,2],
       c[4,2]},
    p_adsorpt_lb_start=1,
    p_adsorpt_ub_start=10,
    tolerance=100*Modelica.Constants.eps)
    "Calculate equilibrium uptake of H2O";

  x_adsorpt_H2O := min(x_adsorpt_H2O, x_adsorpt_H2O_max)
    "Limit equilibrium uptake of H2O to its maximal equilibrium uptake";

  //
  // Assertions
  //
  assert(size(c,2)==2,
        "This function is only valid for the two components CO2 and H2O with this order!",
        level=AssertionLevel.error);

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function calculates the equilibrium uptake of component 2 (i.e., H2O) 
<i>x_adsorpt_H2O</i> as function of the partial equilibrium pressures <i>p_i</i>
and the equilibrium temperature <i>T_adsorpt</i>. For full details of the isotherm 
model, check the documentation of the function 
<a href=\"Modelica://SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.TothGAB_StampiBombelli.x_pyT\">SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.TothGAB_StampiBombelli.x_pyT</a>.
</p>
</p>
</html>", revisions="<html>
<ul>
  <li>
  July 31, 2024, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end x_H20_pT;
