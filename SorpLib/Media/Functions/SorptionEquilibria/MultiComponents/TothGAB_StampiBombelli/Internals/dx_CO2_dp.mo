within SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.TothGAB_StampiBombelli.Internals;
function dx_CO2_dp
  "Toth-GAB isotherm model developed by Stampi-Bombelli et al. (2020): Partial derivative of uptake w.r.t. pressure as function of pressure and temperature"
  extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_dx_dp;

  //
  // Definition of inputs
  //
  input Real[:] dc_dp_adsorpt
    "Partial derivatives of coefficients of isotherm model w.r.t. temperature"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of variables
  //
protected
  Real dx_adsorpt_dc1 = c[2] * p_adsorpt /
    ((1 + (c[2]*p_adsorpt)^c[3])^(1/c[3]))
    "Derivative of uptake w.r.t. to first coefficient of Toth isotherm";
  Real dx_adsorpt_dc2 = c[1] * p_adsorpt *
    ((c[2]*p_adsorpt)^c[3] + 1) ^ (-1/c[3] - 1)
    "Derivative of uptake w.r.t. to second coefficient of Toth isotherm";
  Real dx_adsorpt_dc3 = c[1] * c[2] * p_adsorpt *
    (log(1 + (c[2]*p_adsorpt)^c[3]) / c[3]^2 -
    (c[2]*p_adsorpt)^c[3] * log(c[2]*p_adsorpt) /
    (c[3] * (1 + (c[2]*p_adsorpt)^c[3]))) /
    ((1 + (c[2]*p_adsorpt)^c[3])^(1/c[3]))
    "Derivative of uptake w.r.t. to third coefficient of Toth isotherm";

algorithm
  dx_adsorpt_dp_adsorpt :=
    c[1]*c[2] * ((c[2]*p_adsorpt)^c[3] + 1) ^ (-1/c[3] - 1) +
    dx_adsorpt_dc1*dc_dp_adsorpt[1] +
    dx_adsorpt_dc2*dc_dp_adsorpt[2] +
    dx_adsorpt_dc3*dc_dp_adsorpt[3]
    "Calculation of the partial derivative of the equilibrium uptake w.r.t. the
     equilibrium pressure";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function is the partial derivative of the function 'x_pT' of the Toth isotherm
model with respect to the equilibrium pressure. For full details of the original function 'x_pT,' 
check the documentation of the function 
<a href=\"Modelica://SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Toth.x_pT\">SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Toth.x_pT</a>.
<br>
In contrast to the existing function 'dx_dp' of the Toth isotherm model, this function 
recognises that the coefficients <i>c</i> can also depend on the equilibrium pressure. 
Therefore, this function has the partial derivatives of the coefficients w.r.t. the
equilibrium pressure <i>dc_dp_adsorpt</i> as an additional input.
</p>
</html>", revisions="<html>
<ul>
  <li>
  July 31, 2024, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end dx_CO2_dp;
