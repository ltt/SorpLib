within SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.Testers.IAST_N3;
model Test_changing_everything
  "Tester for all functions of the IAST for three components: Changing pressure, independent mole fractions, and temperature"

  //
  // Definition of parameters of component 1
  //
  parameter SorpLib.Units.Uptake[2] a0_1 = {1.468 * 0.04401, 7.891 * 0.04401}
    "First fitting parameter"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real[2] a1_1(each unit="kg.K/kg") = {-1e-2, -1e-2}
    "Second fitting parameter"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real[2] b0_1(each unit="1/Pa") = {0.024e-6, 0.001645e-6}
    "Third fitting parameter"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real[2] Delta_H_ads_1(each unit="J/mol") =  {-2e4, -2e4}
    "Fourth fitting parameter"
    annotation (Dialog(tab="General", group="Parameters"));

  //
  // Definition of parameters of component 2
  //
  parameter SorpLib.Units.Uptake[2] a0_2 = {2.847 * 0.044097, 2.223 * 0.044097}
    "First fitting parameter"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real[2] a1_2(each unit="kg.K/kg") = {-1e-2, -1e-2}
    "Second fitting parameter"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real[2] b0_2(each unit="1/Pa") = {0.028e-6, 1.228e-6}
    "Third fitting parameter"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real[2] Delta_H_ads_2(each unit="J/mol") =  {-2e4, -2e4}
    "Fourth fitting parameter"
    annotation (Dialog(tab="General", group="Parameters"));

  //
  // Definition of parameters of component 3
  //
  parameter SorpLib.Units.Uptake[2] a0_3 = {2.581 * 0.042081, 2.901 * 0.042081}
    "First fitting parameter"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real[2] a1_3(each unit="kg.K/kg") = {-1e-2, -1e-2}
    "Second fitting parameter"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real[2] b0_3(each unit="1/Pa") = {0.84e-6, 0.021e-6}
    "Third fitting parameter"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real[2] Delta_H_ads_3(each unit="J/mol") =  {-2e4, -2e4}
    "Fourth fitting parameter"
    annotation (Dialog(tab="General", group="Parameters"));

  extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialTestMultiIAST_N3(
    final p_adsorpt_der=10000,
    final y_i_der={-0.9/100,0.9/100/2},
    final T_adsorpt_der=80/100,
    final p_adsorpt_start=100,
    final y_i_start={0.95,0.025},
    final T_adsorpt_start=293.15,
    final no_components=3,
    final M_i={0.04401,0.044097,0.042081},
    final no_coefficients_1=4,
    final no_coefficients_2=4,
    final no_coefficients_3=4,
    redeclare final package IsothermModelComponent1 =
      SorpLib.Media.Functions.SorptionEquilibria.PureComponents.BiLangmuir,
    redeclare final package IsothermModelComponent2 =
      SorpLib.Media.Functions.SorptionEquilibria.PureComponents.BiLangmuir,
    redeclare final package IsothermModelComponent3 =
      SorpLib.Media.Functions.SorptionEquilibria.PureComponents.BiLangmuir,
    num(IASTAlgorithm=SorpLib.Choices.IASTAlgorithm.NewtonRaphson),
    num_comp_1(tolerance_p_adsorpt=1e-6, tolerance_pi=1e-2),
    num_comp_2(tolerance_p_adsorpt=1e-6, tolerance_pi=1e-2),
    num_comp_3(tolerance_p_adsorpt=1e-6, tolerance_pi=1e-2));

equation
  //
  // Coefficients of the isotherm model of component 1
  //
  c_1[1] = a0_1[1] + a1_1[1] / T_adsorpt
    "Coefficients of isotherm model";
  c_1[2] = b0_1[1]*exp(-Delta_H_ads_1[1] / (Modelica.Constants.R * T_adsorpt))
    "Coefficients of isotherm model";
  c_1[3] = a0_1[2] + a1_1[2] / T_adsorpt
    "Coefficients of isotherm model";
  c_1[4] = b0_1[2]*exp(-Delta_H_ads_1[2] / (Modelica.Constants.R * T_adsorpt))
    "Coefficients of isotherm model";

  c_pdT_1[1] = a0_1[1] + a1_1[1] / (T_adsorpt+dT)
    "Coefficients of isotherm model";
  c_pdT_1[2] = b0_1[1]*exp(-Delta_H_ads_1[1] / (Modelica.Constants.R * (T_adsorpt+dT)))
    "Coefficients of isotherm model";
  c_pdT_1[3] = a0_1[2] + a1_1[2] / (T_adsorpt+dT)
    "Coefficients of isotherm model";
  c_pdT_1[4] = b0_1[2]*exp(-Delta_H_ads_1[2] / (Modelica.Constants.R * (T_adsorpt+dT)))
    "Coefficients of isotherm model";

  c_mdT_1[1] = a0_1[1] + a1_1[1] / (T_adsorpt-dT)
    "Coefficients of isotherm model";
  c_mdT_1[2] = b0_1[1]*exp(-Delta_H_ads_1[1] / (Modelica.Constants.R * (T_adsorpt-dT)))
    "Coefficients of isotherm model";
  c_mdT_1[3] = a0_1[2] + a1_1[2] / (T_adsorpt-dT)
    "Coefficients of isotherm model";
  c_mdT_1[4] = b0_1[2]*exp(-Delta_H_ads_1[2] / (Modelica.Constants.R * (T_adsorpt-dT)))
    "Coefficients of isotherm model";

  //
  // Coefficients of the isotherm model of component 2
  //
  c_2[1] = a0_2[1] + a1_2[1] / T_adsorpt
    "Coefficients of isotherm model";
  c_2[2] = b0_2[1]*exp(-Delta_H_ads_2[1] / (Modelica.Constants.R * T_adsorpt))
    "Coefficients of isotherm model";
  c_2[3] = a0_2[2] + a1_2[2] / T_adsorpt
    "Coefficients of isotherm model";
  c_2[4] = b0_2[2]*exp(-Delta_H_ads_2[2] / (Modelica.Constants.R * T_adsorpt))
    "Coefficients of isotherm model";

  c_pdT_2[1] = a0_2[1] + a1_2[1] / (T_adsorpt+dT)
    "Coefficients of isotherm model";
  c_pdT_2[2] = b0_2[1]*exp(-Delta_H_ads_2[1] / (Modelica.Constants.R * (T_adsorpt+dT)))
    "Coefficients of isotherm model";
  c_pdT_2[3] = a0_2[2] + a1_2[2] / (T_adsorpt+dT)
    "Coefficients of isotherm model";
  c_pdT_2[4] = b0_2[2]*exp(-Delta_H_ads_2[2] / (Modelica.Constants.R * (T_adsorpt+dT)))
    "Coefficients of isotherm model";

  c_mdT_2[1] = a0_2[1] + a1_2[1] / (T_adsorpt-dT)
    "Coefficients of isotherm model";
  c_mdT_2[2] = b0_2[1]*exp(-Delta_H_ads_2[1] / (Modelica.Constants.R * (T_adsorpt-dT)))
    "Coefficients of isotherm model";
  c_mdT_2[3] = a0_2[2] + a1_2[2] / (T_adsorpt-dT)
    "Coefficients of isotherm model";
  c_mdT_2[4] = b0_2[2]*exp(-Delta_H_ads_2[2] / (Modelica.Constants.R * (T_adsorpt-dT)))
    "Coefficients of isotherm model";

  //
  // Coefficients of the isotherm model of component 3
  //
  c_3[1] = a0_3[1] + a1_3[1] / T_adsorpt
    "Coefficients of isotherm model";
  c_3[2] = b0_3[1]*exp(-Delta_H_ads_3[1] / (Modelica.Constants.R * T_adsorpt))
    "Coefficients of isotherm model";
  c_3[3] = a0_3[2] + a1_3[2] / T_adsorpt
    "Coefficients of isotherm model";
  c_3[4] = b0_3[2]*exp(-Delta_H_ads_3[2] / (Modelica.Constants.R * T_adsorpt))
    "Coefficients of isotherm model";

  c_pdT_3[1] = a0_3[1] + a1_3[1] / (T_adsorpt+dT)
    "Coefficients of isotherm model";
  c_pdT_3[2] = b0_3[1]*exp(-Delta_H_ads_3[1] / (Modelica.Constants.R * (T_adsorpt+dT)))
    "Coefficients of isotherm model";
  c_pdT_3[3] = a0_3[2] + a1_3[2] / (T_adsorpt+dT)
    "Coefficients of isotherm model";
  c_pdT_3[4] = b0_3[2]*exp(-Delta_H_ads_3[2] / (Modelica.Constants.R * (T_adsorpt+dT)))
    "Coefficients of isotherm model";

  c_mdT_3[1] = a0_3[1] + a1_3[1] / (T_adsorpt-dT)
    "Coefficients of isotherm model";
  c_mdT_3[2] = b0_3[1]*exp(-Delta_H_ads_3[1] / (Modelica.Constants.R * (T_adsorpt-dT)))
    "Coefficients of isotherm model";
  c_mdT_3[3] = a0_3[2] + a1_3[2] / (T_adsorpt-dT)
    "Coefficients of isotherm model";
  c_mdT_3[4] = b0_3[2]*exp(-Delta_H_ads_3[2] / (Modelica.Constants.R * (T_adsorpt-dT)))
    "Coefficients of isotherm model";

  //
  // Annotations
  //
  annotation (experiment(StartTime=0, StopTime=100, Tolerance=1e-6),
Documentation(revisions="<html>
<ul>
  <li>
  November 14, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>", info="<html>
<p>
This tester shows the behavior of all functions of the IAST for three components.
<br/><br/>
As an example, this tester increases the pressure, independent mole fractions, and
temperature with time. To  see the behavior of all functions, plot the variables 
<i>x_adsorpt</i>, <i>dx_adsorpt_dp_adsorpt</i>, <i>dx_adsorpt_dy_i</i>, and 
<i>dx_adsorpt_dT_adsorpt</i> over the variable <i>p_adsorpt</i>. The simulation 
time is correctly preset (Start: 0 s, Stop = 100 s).  
</p>
</html>"));
end Test_changing_everything;
