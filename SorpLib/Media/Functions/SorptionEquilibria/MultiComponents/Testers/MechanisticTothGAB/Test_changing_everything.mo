within SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.Testers.MechanisticTothGAB;
model Test_changing_everything
  "Tester for all functions of the mechanistic Toth-GAB isotherm model developed by Young et al. (2021): Changing pressure, independent mole fractions, and temperature"

  //
  // Definition of parameters of component 1 (i.e., CO2)
  //
  parameter SorpLib.Units.Uptake x_ref_CO2 = 4.86 * 0.0440098
    "Saturation uptake at reference temperature"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real chi_CO2(unit="1") = 0
    "Parameter describing the change of the saturation uptake with temperature"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real t_ref_CO2(unit="1") = 0.209
    "Toth exponent at reference temperature"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real alpha_CO2(unit="1") = 0.5823
    "Parameter describing the change of the Toth exponent with temperature"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real b_ref_CO2(unit="1/Pa") = 2.85e-21
    "Toth coefficient at reference temperature"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real DH_dry_CO2(unit="J/mol") = -117798
    "Parameter describing the isosteric enthalpy of adsorption under dry conditions"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real DH_wet_CO2(unit="J/mol") = -130155
    "Parameter describing the isosteric enthalpy of adsorption under wet conditions"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real Phi_max(unit="1") = 1
    "Maximal possible amine efficiency"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real Phi_dry(unit="1") = 1
    "Amine efficiency under dry conditions"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real f_blocked_max(unit="1") = 0.433
    "Maximal fraction of blocked adsorption sited"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real C_CO2(unit="kg/kg") = 1.535 * 0.018015267
    "Critical water uptake"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real k_CO2(unit="kg/kg") = 0.795 / 0.018015267
    "First parameter describing change of fraction of blocked adsorption sites"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real n_CO2(unit="1") = 1.425
    "Second parameter describing change of fraction of blocked adsorption sites"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Modelica.Units.SI.Temperature T_ref_CO2 = 298.15
    "Reference temperature"
    annotation (Dialog(tab="General", group="Parameters"));

  //
  // Definition of parameters of component 2 (i.e., H20)
  //
  parameter SorpLib.Units.Uptake x_mon_H2O = 3.63 * 0.018015267
    "Monolayer uptake at reference temperature"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real C_H2O(unit="J/mol") = 47110
    "First parameter of the adsorption enthalpy of the first layer"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real D_H2O(unit="1/K") = 0.023744
    "Second parameter of the adsorption enthalpy of the first layer"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real F_H2O(unit="J/mol") = 57706
    "First parameter of the adsorption enthalpy of the layers 2 to 9"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real G_H2O(unit="J/(mol.K)") = -47.814
    "Second parameter of the adsorption enthalpy of the ayers 2 to 9"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real I_H2O(unit="J/mol") = 57220
    "First parameter of the enthalpy of vaporization"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real J_H2O(unit="J/(mol.K)") = -44.38
    "Second parameter of the enthalpy of vaporization"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Modelica.Units.SI.Temperature T_ref_H2O = 298.15
    "Reference temperature"
    annotation (Dialog(tab="General", group="Parameters"));

  extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialTestMulti(
    final p_adsorpt_der = (1e5-12349)/100,
    final y_i_der = {0},
    final T_adsorpt_der = 75/100,
    final p_adsorpt_start = 1,
    final y_i_start = {1-0.5*12349/1e5},
    final T_adsorpt_start = 273.15+25,
    final no_components = 2,
    final no_coefficients = 11,
    redeclare final package IsothermModel =
      SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.MechanisticTothGAB,
    p_threshold_min = 0);

equation
  //
  // Coefficients of the isotherm model
  //
  c[1,1] = x_ref_CO2 * Modelica.Math.exp(chi_CO2 * (1 - (T_adsorpt / T_ref_CO2)))
    "First coefficient of the Toth isotherm model";
  c[2,1] = t_ref_CO2 + alpha_CO2 * (1 - T_ref_CO2 / T_adsorpt)
    "Second coefficient of the Toth isotherm model";
  c[3,1] = Phi_max + 1e-6 * (T_adsorpt)
    "Third coefficient of the Toth isotherm model";
  c[4,1] = Phi_dry + 1e-6 * (T_adsorpt)
    "Fourth coefficient of the Toth isotherm model";
  c[5,1] = f_blocked_max + 1e-6 * (T_adsorpt)
    "Fivth coefficient of the Toth isotherm model";
  c[6,1] = C_CO2 + 1e-6 * (T_adsorpt)
    "Sixth coefficient of the Toth isotherm model";
  c[7,1] = k_CO2 + 1e-6 * (T_adsorpt)
    "Seventh coefficient of the Toth isotherm model";
  c[8,1] = n_CO2 + 1e-6 * (T_adsorpt)
    "Eigth coefficient of the Toth isotherm model";
  c[9,1] = b_ref_CO2 + 1e-6 * (T_adsorpt)
    "Ninth coefficient of the Toth isotherm model";
  c[10,1] = DH_dry_CO2 + 1e-6 * (T_adsorpt)
    "Tenth coefficient of the Toth isotherm model";
  c[11,1] = DH_wet_CO2 + 1e-6 * (T_adsorpt)
    "Eleventh coefficient of the Toth isotherm model";

  c[1,2] = Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.psat(T=T_adsorpt)
    "First coefficient of the GAB isotherm model";
  c[2,2] = x_mon_H2O
    "Second coefficient of the GAB isotherm model";
  c[3,2] = Modelica.Math.exp(((C_H2O - Modelica.Math.exp(D_H2O * T_adsorpt)) -
    (J_H2O * T_adsorpt + I_H2O)) / (Modelica.Constants.R * T_adsorpt))
    "Third coefficient of the GAB isotherm model";
  c[4,2] = Modelica.Math.exp(((F_H2O + G_H2O * T_adsorpt) -
    (J_H2O * T_adsorpt + I_H2O)) / (Modelica.Constants.R * T_adsorpt))
    "Fourth coefficient of the GAB isotherm model";
  c[5,2] = 0
    "Fivth coefficient of the GAB isotherm model";
  c[6,2] = 0
    "Sixth coefficient of the GAB isotherm model";
  c[7,2] = 0
    "Seventh coefficient of the GAB isotherm model";
  c[8,2] = 0
    "Eigth coefficient of the GAB isotherm model";
  c[9,2] = 0
    "Ninth coefficient of the GAB isotherm model";
  c[10,2] = 0
    "Tenth coefficient of the GAB isotherm model";
  c[11,2] = 0
    "Eleventh coefficient of the GAB isotherm model";

  //
  // Partial derivative of coefficients of isotherm model w.r.t. temperature
  //
  dc_dT[1,1] = -chi_CO2 / T_ref_CO2 * c[1,1]
    "First coefficient of the Toth isotherm model";
  dc_dT[2,1] = alpha_CO2 * T_ref_CO2 / T_adsorpt^2
    "Second coefficient of the Toth isotherm model";
  dc_dT[3,1] = 1e-6
    "Third coefficient of the Toth isotherm model";
  dc_dT[4,1] = 1e-6
    "Fourth coefficient of the Toth isotherm model";
  dc_dT[5,1] = 1e-6
    "Fivth coefficient of the Toth isotherm model";
  dc_dT[6,1] = 1e-6
    "Sixth coefficient of the Toth isotherm model";
  dc_dT[7,1] = 1e-6
    "Seventh coefficient of the Toth isotherm model";
  dc_dT[8,1] = 1e-6
    "Eigth coefficient of the Toth isotherm model";
  dc_dT[9,1] = 1e-6
    "Ninth coefficient of the Toth isotherm model";
  dc_dT[10,1] = 1e-6
    "Tenth coefficient of the Toth isotherm model";
  dc_dT[11,1] = 1e-6
    "Eleventh coefficient of the Toth isotherm model";

  dc_dT[1,2] = Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.dptofT(T=T_adsorpt)
    "First coefficient of the GAB isotherm model";
  dc_dT[2,2] = 0
    "Second coefficient of the GAB isotherm model";
  dc_dT[3,2] = -((D_H2O*T_adsorpt - 1) * exp(D_H2O*T_adsorpt) - I_H2O + C_H2O) /
    (Modelica.Constants.R*T_adsorpt^2) * c[3,2]
    "Third coefficient of the GAB isotherm model";
  dc_dT[4,2] = (I_H2O - F_H2O) / (Modelica.Constants.R*T_adsorpt^2) * c[4,2]
    "Fourth coefficient of the GAB isotherm model";
  dc_dT[5,2] = 0
    "Fivth coefficient of the GAB isotherm model";
  dc_dT[6,2] = 0
    "Sixth coefficient of the GAB isotherm model";
  dc_dT[7,2] = 0
    "Seventh coefficient of the GAB isotherm model";
  dc_dT[8,2] = 0
    "Eigth coefficient of the GAB isotherm model";
  dc_dT[9,2] = 0
    "Ninth coefficient of the GAB isotherm model";
  dc_dT[10,2] = 0
    "Tenth coefficient of the GAB isotherm model";
  dc_dT[11,2] = 0
    "Eleventh coefficient of the GAB isotherm model";

  //
  // Coefficients of the isotherm model: T + dT K
  //
  c_pdT[1,1] = x_ref_CO2 * Modelica.Math.exp(chi_CO2 * (1 - ((T_adsorpt+dT) / T_ref_CO2)))
    "First coefficient of the Toth isotherm model";
  c_pdT[2,1] = t_ref_CO2 + alpha_CO2 * (1 - T_ref_CO2 / (T_adsorpt+dT))
    "Second coefficient of the Toth isotherm model";
  c_pdT[3,1] = Phi_max + 1e-6 * (T_adsorpt+dT)
    "Third coefficient of the Toth isotherm model";
  c_pdT[4,1] = Phi_dry + 1e-6 * (T_adsorpt+dT)
    "Fourth coefficient of the Toth isotherm model";
  c_pdT[5,1] = f_blocked_max + 1e-6 * (T_adsorpt+dT)
    "Fivth coefficient of the Toth isotherm model";
  c_pdT[6,1] = C_CO2 + 1e-6 * (T_adsorpt+dT)
    "Sixth coefficient of the Toth isotherm model";
  c_pdT[7,1] = k_CO2 + 1e-6 * (T_adsorpt+dT)
    "Seventh coefficient of the Toth isotherm model";
  c_pdT[8,1] = n_CO2 + 1e-6 * (T_adsorpt+dT)
    "Eigth coefficient of the Toth isotherm model";
  c_pdT[9,1] = b_ref_CO2 + 1e-6 * (T_adsorpt+dT)
    "Ninth coefficient of the Toth isotherm model";
  c_pdT[10,1] = DH_dry_CO2 + 1e-6 * (T_adsorpt+dT)
    "Tenth coefficient of the Toth isotherm model";
  c_pdT[11,1] = DH_wet_CO2 + 1e-6 * (T_adsorpt+dT)
    "Eleventh coefficient of the Toth isotherm model";

  c_pdT[1,2] = Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.psat(T=T_adsorpt+dT)
    "First coefficient of the GAB isotherm model";
  c_pdT[2,2] = x_mon_H2O
    "Second coefficient of the GAB isotherm model";
  c_pdT[3,2] = Modelica.Math.exp(((C_H2O - Modelica.Math.exp(D_H2O * (T_adsorpt+dT))) -
    (J_H2O * (T_adsorpt+dT) + I_H2O)) / (Modelica.Constants.R * (T_adsorpt+dT)))
    "Third coefficient of the GAB isotherm model";
  c_pdT[4,2] = Modelica.Math.exp(((F_H2O + G_H2O * (T_adsorpt+dT)) -
    (J_H2O * (T_adsorpt+dT) + I_H2O)) / (Modelica.Constants.R * (T_adsorpt+dT)))
    "Fourth coefficient of the GAB isotherm model";
  c_pdT[5,2] = 0
    "Fivth coefficient of the GAB isotherm model";
  c_pdT[6,2] = 0
    "Sixth coefficient of the GAB isotherm model";
  c_pdT[7,2] = 0
    "Seventh coefficient of the GAB isotherm model";
  c_pdT[8,2] = 0
    "Eigth coefficient of the GAB isotherm model";
  c_pdT[9,2] = 0
    "Ninth coefficient of the GAB isotherm model";
  c_pdT[10,2] = 0
    "Tenth coefficient of the GAB isotherm model";
  c_pdT[11,2] = 0
    "Eleventh coefficient of the GAB isotherm model";

  //
  // Coefficients of the isotherm model: T - dT K
  //
  c_mdT[1,1] = x_ref_CO2 * Modelica.Math.exp(chi_CO2 * (1 - ((T_adsorpt-dT) / T_ref_CO2)))
    "First coefficient of the Toth isotherm model";
  c_mdT[2,1] = t_ref_CO2 + alpha_CO2 * (1 - T_ref_CO2 / (T_adsorpt-dT))
    "Second coefficient of the Toth isotherm model";
  c_mdT[3,1] = Phi_max + 1e-6 * (T_adsorpt-dT)
    "Third coefficient of the Toth isotherm model";
  c_mdT[4,1] = Phi_dry + 1e-6 * (T_adsorpt-dT)
    "Fourth coefficient of the Toth isotherm model";
  c_mdT[5,1] = f_blocked_max + 1e-6 * (T_adsorpt-dT)
    "Fivth coefficient of the Toth isotherm model";
  c_mdT[6,1] = C_CO2 + 1e-6 * (T_adsorpt-dT)
    "Sixth coefficient of the Toth isotherm model";
  c_mdT[7,1] = k_CO2 + 1e-6 * (T_adsorpt-dT)
    "Seventh coefficient of the Toth isotherm model";
  c_mdT[8,1] = n_CO2 + 1e-6 * (T_adsorpt-dT)
    "Eigth coefficient of the Toth isotherm model";
  c_mdT[9,1] = b_ref_CO2 + 1e-6 * (T_adsorpt-dT)
    "Ninth coefficient of the Toth isotherm model";
  c_mdT[10,1] = DH_dry_CO2 + 1e-6 * (T_adsorpt-dT)
    "Tenth coefficient of the Toth isotherm model";
  c_mdT[11,1] = DH_wet_CO2 + 1e-6 * (T_adsorpt-dT)
    "Eleventh coefficient of the Toth isotherm model";

  c_mdT[1,2] = Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.psat(T=T_adsorpt-dT)
    "First coefficient of the GAB isotherm model";
  c_mdT[2,2] = x_mon_H2O
    "Second coefficient of the GAB isotherm model";
  c_mdT[3,2] = Modelica.Math.exp(((C_H2O - Modelica.Math.exp(D_H2O * (T_adsorpt-dT))) -
    (J_H2O * (T_adsorpt-dT) + I_H2O)) / (Modelica.Constants.R * (T_adsorpt-dT)))
    "Third coefficient of the GAB isotherm model";
  c_mdT[4,2] = Modelica.Math.exp(((F_H2O + G_H2O * (T_adsorpt-dT)) -
    (J_H2O * (T_adsorpt-dT) + I_H2O)) / (Modelica.Constants.R * (T_adsorpt-dT)))
    "Fourth coefficient of the GAB isotherm model";
  c_mdT[5,2] = 0
    "Fivth coefficient of the GAB isotherm model";
  c_mdT[6,2] = 0
    "Sixth coefficient of the GAB isotherm model";
  c_mdT[7,2] = 0
    "Seventh coefficient of the GAB isotherm model";
  c_mdT[8,2] = 0
    "Eigth coefficient of the GAB isotherm model";
  c_mdT[9,2] = 0
    "Ninth coefficient of the GAB isotherm model";
  c_mdT[10,2] = 0
    "Tenth coefficient of the GAB isotherm model";
  c_mdT[11,2] = 0
    "Eleventh coefficient of the GAB isotherm model";

  //
  // Annotations
  //
  annotation (experiment(StartTime=0, StopTime=100, Tolerance=1e-6),
Documentation(revisions="<html>
<ul>
  <li>
  August 1, 2024, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>", info="<html>
<p>
This tester shows the behavior of all functions of the mechanistic Toth-GAB isotherm 
model. Additionally, the implemented functions for the partial derivarives 'dx_dp,'
'dx_dy,' and 'dx_dT' are checked via numerical calculations.
<br/><br/>
As an example, this tester increases the pressure, independent mole fractions, and
temperature with time. To  see the behavior of all functions, plot the variables 
<i>x_adsorpt</i>, <i>dx_adsorpt_dp_adsorpt</i>, <i>dx_adsorpt_dy_i</i>, and 
<i>dx_adsorpt_dT_adsorpt</i> over the variable <i>p_adsorpt</i>. The simulation 
time is correctly preset (Start: 0 s, Stop = 100 s).  
</p>
</html>"));
end Test_changing_everything;
