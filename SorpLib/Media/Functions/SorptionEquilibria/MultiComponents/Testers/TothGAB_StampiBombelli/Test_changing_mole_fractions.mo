within SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.Testers.TothGAB_StampiBombelli;
model Test_changing_mole_fractions
  "Tester for all functions of the Toth-GAB isotherm model developed by Stampi-Bombelli et al. (2020): Changing independent mole fractions"

  //
  // Definition of parameters of component 1 (i.e., CO2)
  //
  parameter SorpLib.Units.Uptake x_ref_CO2 = 2.38 * 0.0440098
    "Saturation uptake at reference temperature"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real chi_CO2(unit="1") = 0
    "Parameter describing the change of the saturation uptake with temperature"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real b_ref_CO2(unit="1/Pa") = 0.07074
    "Toth coefficient at reference temperature"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real Q_CO2(unit="J/mol") = -57047
    "Parameter describing the change of the Toth coefficient with temperature"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real t_ref_CO2(unit="1") = 0.4148
    "Toth exponent at reference temperature"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real alpha_CO2(unit="1") = -1.6060
    "Parameter describing the change of the Toth exponent with temperature"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter SorpLib.Units.Uptake gamma_CO2 = 0.0061 / 0.0440098
    "Enhancement factor for saturation uptake"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter SorpLib.Units.Uptake beta_CO2 = 28.907 / 0.0440098
    "Enhancement factor for Toth coefficient"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Modelica.Units.SI.Temperature T_ref_CO2 = 296
    "Reference temperature"
    annotation (Dialog(tab="General", group="Parameters"));

  //
  // Definition of parameters of component 2 (i.e., H20)
  //
  parameter SorpLib.Units.Uptake x_mon_H2O = 36.48 * 0.018015267
    "Monolayer uptake at reference temperature"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real c_H2O(unit="1") = 0.1489
    "First affinity coefficient"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real k_H2O(unit="1") = 0.5751
    "Second affinity coefficient"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Modelica.Units.SI.Temperature T_ref_H2O = 296
    "Reference temperature"
    annotation (Dialog(tab="General", group="Parameters"));

  extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialTestMulti(
    final p_adsorpt_der = 0,
    final y_i_der = {0.98/100},
    final T_adsorpt_der = 0,
    final p_adsorpt_start = 10000,
    final y_i_start = {1e-2},
    final T_adsorpt_start = 323.15,
    final no_components = 2,
    final no_coefficients = 5,
    redeclare final package IsothermModel =
      SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.TothGAB_StampiBombelli,
    p_threshold_min = 0);

equation
  //
  // Coefficients of the isotherm model
  //
  c[1,1] = x_ref_CO2 * Modelica.Math.exp(chi_CO2 * (1 - (T_adsorpt / T_ref_CO2)))
    "First coefficient of the Toth isotherm model";
  c[2,1] = b_ref_CO2 * Modelica.Math.exp(Q_CO2 /
    (Modelica.Constants.R * T_ref_CO2) *(T_ref_CO2 / T_adsorpt - 1))
    "Second coefficient of the Toth isotherm model";
  c[3,1] = t_ref_CO2 + alpha_CO2 * (1 - T_ref_CO2 / T_adsorpt)
    "Third coefficient of the Toth isotherm model";
  c[4,1] = gamma_CO2
    "Fourth coefficient of the Toth isotherm model";
  c[5,1] = beta_CO2
    "Fivth coefficient of the Toth isotherm model";

  c[1,2] = Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.psat(T=T_adsorpt)
    "First coefficient of the GAB isotherm model";
  c[2,2] = x_mon_H2O
    "Second coefficient of the GAB isotherm model";
  c[3,2] = c_H2O
    "Third coefficient of the GAB isotherm model";
  c[4,2] = k_H2O
    "Fourth coefficient of the GAB isotherm model";
  c[5,2] = 0
    "Fivth coefficient of the GAB isotherm model";

  //
  // Partial derivative of coefficients of isotherm model w.r.t. temperature
  //
  dc_dT[1,1] = -chi_CO2 / T_ref_CO2 * c[1,1]
    "First coefficient of the Toth isotherm model";
  dc_dT[2,1] = -Q_CO2 / (Modelica.Constants.R * T_adsorpt^2) * c[2,1]
    "Second coefficient of the Toth isotherm model";
  dc_dT[3,1] = alpha_CO2 * T_ref_CO2 / T_adsorpt^2
    "Third coefficient of the Toth isotherm model";
  dc_dT[4,1] = 0
    "Fourth coefficient of the Toth isotherm model";
  dc_dT[5,1] = 0
    "Fivth coefficient of the Toth isotherm model";

  dc_dT[1,2] = Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.dptofT(T=T_adsorpt)
    "First coefficient of the GAB isotherm model";
  dc_dT[2,2] = 0
    "Second coefficient of the GAB isotherm model";
  dc_dT[3,2] = 0
    "Third coefficient of the GAB isotherm model";
  dc_dT[4,2] = 0
    "Fourth coefficient of the GAB isotherm model";
  dc_dT[5,2] = 0
    "Fivth coefficient of the GAB isotherm model";

  //
  // Coefficients of the isotherm model: T + dT K
  //
  c_pdT[1,1] = x_ref_CO2 * Modelica.Math.exp(chi_CO2 * (1 - ((T_adsorpt+dT) / T_ref_CO2)))
    "First coefficient of the Toth isotherm model";
  c_pdT[2,1] = b_ref_CO2 * Modelica.Math.exp(Q_CO2 /
    (Modelica.Constants.R * T_ref_CO2) *(T_ref_CO2 / (T_adsorpt+dT) - 1))
    "Second coefficient of the Toth isotherm model";
  c_pdT[3,1] = t_ref_CO2 + alpha_CO2 * (1 - T_ref_CO2 / (T_adsorpt+dT))
    "Third coefficient of the Toth isotherm model";
  c_pdT[4,1] = gamma_CO2
    "Fourth coefficient of the Toth isotherm model";
  c_pdT[5,1] = beta_CO2
    "Fivth coefficient of the Toth isotherm model";

  c_pdT[1,2] = Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.psat(T=T_adsorpt+dT)
    "First coefficient of the GAB isotherm model";
  c_pdT[2,2] = x_mon_H2O
    "Second coefficient of the GAB isotherm model";
  c_pdT[3,2] = c_H2O
    "Third coefficient of the GAB isotherm model";
  c_pdT[4,2] = k_H2O
    "Fourth coefficient of the GAB isotherm model";
  c_pdT[5,2] = 0
    "Fivth coefficient of the GAB isotherm model";

  //
  // Coefficients of the isotherm model: T - dT K
  //
  c_mdT[1,1] = x_ref_CO2 * Modelica.Math.exp(chi_CO2 * (1 - ((T_adsorpt-dT) / T_ref_CO2)))
    "First coefficient of the Toth isotherm model";
  c_mdT[2,1] = b_ref_CO2 * Modelica.Math.exp(Q_CO2 /
    (Modelica.Constants.R * T_ref_CO2) *(T_ref_CO2 / (T_adsorpt-dT) - 1))
    "Second coefficient of the Toth isotherm model";
  c_mdT[3,1] = t_ref_CO2 + alpha_CO2 * (1 - T_ref_CO2 / (T_adsorpt-dT))
    "Third coefficient of the Toth isotherm model";
  c_mdT[4,1] = gamma_CO2
    "Fourth coefficient of the Toth isotherm model";
  c_mdT[5,1] = beta_CO2
    "Fivth coefficient of the Toth isotherm model";

  c_mdT[1,2] = Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.psat(T=T_adsorpt-dT)
    "First coefficient of the GAB isotherm model";
  c_mdT[2,2] = x_mon_H2O
    "Second coefficient of the GAB isotherm model";
  c_mdT[3,2] = c_H2O
    "Third coefficient of the GAB isotherm model";
  c_mdT[4,2] = k_H2O
    "Fourth coefficient of the GAB isotherm model";
  c_mdT[5,2] = 0
    "Fivth coefficient of the GAB isotherm model";

  //
  // Annotations
  //
  annotation (experiment(StartTime=0, StopTime=100, Tolerance=1e-6),
Documentation(revisions="<html>
<ul>
  <li>
  July 31, 2024, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>", info="<html>
<p>
This tester shows the behavior of all functions of the Toth-GAB isotherm 
model. Additionally, the implemented functions for the partial derivarives 'dx_dp,'
'dx_dy,' and 'dx_dT' are checked via numerical calculations.
<br/><br/>
As an example, this tester increases the independent mole fractions with time. To 
see the behavior of all functions, plot the variables <i>x_adsorpt</i>,
<i>dx_adsorpt_dp_adsorpt</i>, <i>dx_adsorpt_dy_i</i>, and <i>dx_adsorpt_dT_adsorpt</i> 
over the variable <i>p_adsorpt</i>. The simulation time is correctly preset 
(Start: 0 s, Stop = 100 s).  
</p>
</html>"));
end Test_changing_mole_fractions;
