within SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.Testers.Langmuir;
model Test_changing_mole_fractions
  "Tester for all functions of the extended Langmuir isotherm model: Changing independent mole fractions"

  //
  // Definition of parameters
  //
  parameter SorpLib.Units.Uptake[no_components] a0 = {0.35, 0.25}
    "First fitting parameter"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real[no_components] a1(each unit="kg.K/kg") = {1e-4, 1e-4}
    "Second fitting parameter"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real[no_components] b0(each unit="1/Pa") = {5e-8, 5e-8}
    "Third fitting parameter"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real[no_components] Delta_H_ads(each unit="J/mol") =  {-2e4, -3e4}
    "Fourth fitting parameter"
    annotation (Dialog(tab="General", group="Parameters"));

  extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialTestMulti(
    final p_adsorpt_der = 0,
    final y_i_der = {0.98/100},
    final T_adsorpt_der = 0,
    final p_adsorpt_start = 10000,
    final y_i_start = {1e-2},
    final T_adsorpt_start = 298.15,
    final no_components = 2,
    final no_coefficients = 2,
    redeclare final package IsothermModel =
      SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.Langmuir,
    p_threshold_min = 0);

equation

  for ind_comp in 1:no_components loop
    //
    // Coefficients of the isotherm model
    //
    c[1,ind_comp] = a0[ind_comp] + a1[ind_comp]/T_adsorpt
      "Coefficients of isotherm model";
    c[2,ind_comp] = b0[ind_comp]*exp(-Delta_H_ads[ind_comp]/
      (Modelica.Constants.R*T_adsorpt))
      "Coefficients of isotherm model";

    //
    // Partial derivative of coefficients of isotherm model w.r.t. temperature
    //
    dc_dT[1,ind_comp] = -a1[ind_comp]/T_adsorpt^2
      "Partial derivative of coefficients of isotherm model w.r.t. temperature";
    dc_dT[2,ind_comp] = Delta_H_ads[ind_comp]/(Modelica.Constants.R*T_adsorpt^2) *
      c[2,ind_comp]
      "Partial derivative of coefficients of isotherm model w.r.t. temperature";

    //
    // Coefficients of the isotherm model: T + dT K
    //
    c_pdT[1,ind_comp] = a0[ind_comp] + a1[ind_comp]/(T_adsorpt+dT)
      "Coefficients of isotherm model: T + dT K";
    c_pdT[2,ind_comp] = b0[ind_comp]*exp(-Delta_H_ads[ind_comp]/
      (Modelica.Constants.R*(T_adsorpt+dT)))
      "Coefficients of isotherm model: T + dT K";

    //
    // Coefficients of the isotherm model: T - dT K
    //
    c_mdT[1,ind_comp] = a0[ind_comp] + a1[ind_comp]/(T_adsorpt-dT)
      "Coefficients of isotherm model: T - dT K";
    c_mdT[2,ind_comp] = b0[ind_comp]*exp(-Delta_H_ads[ind_comp]/
      (Modelica.Constants.R*(T_adsorpt-dT)))
      "Coefficients of isotherm model: T - dT K";
  end for;

  //
  // Annotations
  //
  annotation (experiment(StartTime=0, StopTime=100, Tolerance=1e-6),
Documentation(revisions="<html>
<ul>
  <li>
  November 7, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>", info="<html>
<p>
This tester shows the behavior of all functions of the extended Langmuir isotherm 
model. Additionally, the implemented functions for the partial derivarives 'dx_dp,'
'dx_dy,' and 'dx_dT' are checked via numerical calculations.
<br/><br/>
As an example, this tester increases the independent mole fractions with time. To 
see the behavior of all functions, plot the variables <i>x_adsorpt</i>,
<i>dx_adsorpt_dp_adsorpt</i>, <i>dx_adsorpt_dy_i</i>, and <i>dx_adsorpt_dT_adsorpt</i> 
over the variable <i>p_adsorpt</i>. The simulation time is correctly preset 
(Start: 0 s, Stop = 100 s).  
</p>
</html>"));
end Test_changing_mole_fractions;
