within SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.Testers.WeightedAverageDualSiteTothGAB;
model Test_changing_temperature
  "Weighted-average dual site Toth-GAB isotherm model developed by Young et al. (2021): Changing temperature"

  //
  // Definition of parameters of component 1 (i.e., CO2)
  //
  parameter SorpLib.Units.Uptake x_ref_CO2_dry = 4.86 * 0.0440098
    "Saturation uptake at reference temperature under dry conditions"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real chi_CO2_dry(unit="1") = 0
    "Parameter describing the change of the saturation uptake with temperature
     under dry conditions"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real b_ref_CO2_dry(unit="1/Pa") = 2.85e-21
    "Toth coefficient at reference temperature under dry conditions"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real DH_CO2_dry(unit="J/mol") = -117798
    "Parameter describing the isosteric enthalpy of adsorption under dry conditions"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real t_ref_CO2_dry(unit="1") = 0.209
    "Toth exponent at reference temperature under dry conditions"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real alpha_CO2_dry(unit="1") = 0.5823
    "Parameter describing the change of the Toth exponent with temperature under 
    dry conditions"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Modelica.Units.SI.Temperature T_ref_CO2_dry = 298.15
    "Reference temperature under dry conditions"
    annotation (Dialog(tab="General", group="Parameters"));

  parameter Real C_CO2(unit="kg/kg") = 1.532 * 0.018015267
    "Critical water uptake"
    annotation (Dialog(tab="General", group="Parameters"));

  parameter SorpLib.Units.Uptake x_ref_CO2_wet = 9.035 * 0.0440098
    "Saturation uptake at reference temperature under wet conditions"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real chi_CO2_wet(unit="1") = 0
    "Parameter describing the change of the saturation uptake with temperature
     under wet conditions"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real b_ref_CO2_wet(unit="1/Pa") = 1.230e-18
    "Toth coefficient at reference temperature under wet conditions"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real DH_CO2_wet(unit="J/mol") = -203687
    "Parameter describing the isosteric enthalpy of adsorption under wet conditions"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real t_ref_CO2_wet(unit="1") = 0.053
    "Toth exponent at reference temperature under wet conditions"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real alpha_CO2_wet(unit="1") = 0.053
    "Parameter describing the change of the Toth exponent with temperature under 
    wet conditions"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Modelica.Units.SI.Temperature T_ref_CO2_wet = 298.15
    "Reference temperature under wet conditions"
    annotation (Dialog(tab="General", group="Parameters"));

  //
  // Definition of parameters of component 2 (i.e., H20)
  //
  parameter SorpLib.Units.Uptake x_mon_H2O = 3.63 * 0.018015267
    "Monolayer uptake at reference temperature"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real C_H2O(unit="J/mol") = 47110
    "First parameter of the adsorption enthalpy of the first layer"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real D_H2O(unit="1/K") = 0.023744
    "Second parameter of the adsorption enthalpy of the first layer"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real F_H2O(unit="J/mol") = 57706
    "First parameter of the adsorption enthalpy of the layers 2 to 9"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real G_H2O(unit="J/(mol.K)") = -47.814
    "Second parameter of the adsorption enthalpy of the ayers 2 to 9"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real I_H2O(unit="J/mol") = 57220
    "First parameter of the enthalpy of vaporization"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real J_H2O(unit="J/(mol.K)") = -44.38
    "Second parameter of the enthalpy of vaporization"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Modelica.Units.SI.Temperature T_ref_H2O = 298.15
    "Reference temperature"
    annotation (Dialog(tab="General", group="Parameters"));

  extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialTestMulti(
    final p_adsorpt_der = 0,
    final y_i_der = {0},
    final T_adsorpt_der = 50/100,
    final p_adsorpt_start = 0.6e5,
    final y_i_start = {1-0.5*12349/0.6e5},
    final T_adsorpt_start = 273.15+50,
    final no_components = 2,
    final no_coefficients = 7,
    redeclare final package IsothermModel =
      SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.WeightedAverageDualSiteTothGAB,
    p_threshold_min = 0);

equation
  //
  // Coefficients of the isotherm model
  //
  c[1,1] = x_ref_CO2_dry *
    Modelica.Math.exp(chi_CO2_dry * (1 - (T_adsorpt / T_ref_CO2_dry)))
    "First coefficient of the Toth isotherm model";
  c[2,1] = b_ref_CO2_dry *
    Modelica.Math.exp(-DH_CO2_dry / (Modelica.Constants.R * T_adsorpt))
    "Second coefficient of the Toth isotherm model";
  c[3,1] = t_ref_CO2_dry + alpha_CO2_dry * (1 - T_ref_CO2_dry / T_adsorpt)
    "Third coefficient of the Toth isotherm model";
  c[4,1] = C_CO2
    "Fourth coefficient of the Toth isotherm model";
  c[5,1] = x_ref_CO2_wet *
    Modelica.Math.exp(chi_CO2_wet * (1 - (T_adsorpt / T_ref_CO2_wet)))
    "Fivth coefficient of the Toth isotherm model";
  c[6,1] = b_ref_CO2_wet *
    Modelica.Math.exp(-DH_CO2_wet / (Modelica.Constants.R * T_adsorpt))
    "Sixth coefficient of the Toth isotherm model";
  c[7,1] = t_ref_CO2_wet + alpha_CO2_wet * (1 - T_ref_CO2_wet / T_adsorpt)
    "Seventh coefficient of the Toth isotherm model";

  c[1,2] = Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.psat(T=T_adsorpt)
    "First coefficient of the GAB isotherm model";
  c[2,2] = x_mon_H2O
    "Second coefficient of the GAB isotherm model";
  c[3,2] = Modelica.Math.exp(((C_H2O - Modelica.Math.exp(D_H2O * T_adsorpt)) -
    (J_H2O * T_adsorpt + I_H2O)) / (Modelica.Constants.R * T_adsorpt))
    "Third coefficient of the GAB isotherm model";
  c[4,2] = Modelica.Math.exp(((F_H2O + G_H2O * T_adsorpt) -
    (J_H2O * T_adsorpt + I_H2O)) / (Modelica.Constants.R * T_adsorpt))
    "Fourth coefficient of the GAB isotherm model";
  c[5,2] = 0
    "Fivth coefficient of the GAB isotherm model";
  c[6,2] = 0
    "Sixth coefficient of the GAB isotherm model";
  c[7,2] = 0
    "Seventh coefficient of the GAB isotherm model";

  //
  // Partial derivative of coefficients of isotherm model w.r.t. temperature
  //
  dc_dT[1,1] = -chi_CO2_dry / T_ref_CO2_dry * c[1,1]
    "First coefficient of the Toth isotherm model";
  dc_dT[2,1] = DH_CO2_dry * c[2,1] / (Modelica.Constants.R * T_adsorpt^2)
    "Second coefficient of the Toth isotherm model";
  dc_dT[3,1] = alpha_CO2_dry * T_ref_CO2_dry / T_adsorpt^2
    "Third coefficient of the Toth isotherm model";
  dc_dT[4,1] = 0
    "Fourth coefficient of the Toth isotherm model";
  dc_dT[5,1] = -chi_CO2_wet / T_ref_CO2_wet * c[5,1]
    "Fivth coefficient of the Toth isotherm model";
  dc_dT[6,1] = DH_CO2_wet * c[6,1] / (Modelica.Constants.R * T_adsorpt^2)
    "Sixth coefficient of the Toth isotherm model";
  dc_dT[7,1] = alpha_CO2_wet * T_ref_CO2_wet / T_adsorpt^2
    "Seventh coefficient of the Toth isotherm model";

  dc_dT[1,2] = Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.dptofT(T=T_adsorpt)
    "First coefficient of the GAB isotherm model";
  dc_dT[2,2] = 0
    "Second coefficient of the GAB isotherm model";
  dc_dT[3,2] = -((D_H2O*T_adsorpt - 1) * exp(D_H2O*T_adsorpt) - I_H2O + C_H2O) /
    (Modelica.Constants.R*T_adsorpt^2) * c[3,2]
    "Third coefficient of the GAB isotherm model";
  dc_dT[4,2] = (I_H2O - F_H2O) / (Modelica.Constants.R*T_adsorpt^2) * c[4,2]
    "Fourth coefficient of the GAB isotherm model";
  dc_dT[5,2] = 0
    "Fivth coefficient of the GAB isotherm model";
  dc_dT[6,2] = 0
    "Sixth coefficient of the GAB isotherm model";
  dc_dT[7,2] = 0
    "Seventh coefficient of the GAB isotherm model";

  //
  // Coefficients of the isotherm model: T + dT K
  //
  c_pdT[1,1] = x_ref_CO2_dry *
    Modelica.Math.exp(chi_CO2_dry * (1 - ((T_adsorpt+dT) / T_ref_CO2_dry)))
    "First coefficient of the Toth isotherm model";
  c_pdT[2,1] = b_ref_CO2_dry *
    Modelica.Math.exp(-DH_CO2_dry / (Modelica.Constants.R * (T_adsorpt+dT)))
    "Second coefficient of the Toth isotherm model";
  c_pdT[3,1] = t_ref_CO2_dry + alpha_CO2_dry * (1 - T_ref_CO2_dry / (T_adsorpt+dT))
    "Third coefficient of the Toth isotherm model";
  c_pdT[4,1] = C_CO2
    "Fourth coefficient of the Toth isotherm model";
  c_pdT[5,1] = x_ref_CO2_wet *
    Modelica.Math.exp(chi_CO2_wet * (1 - ((T_adsorpt+dT) / T_ref_CO2_wet)))
    "Fivth coefficient of the Toth isotherm model";
  c_pdT[6,1] = b_ref_CO2_wet *
    Modelica.Math.exp(-DH_CO2_wet / (Modelica.Constants.R * (T_adsorpt+dT)))
    "Sixth coefficient of the Toth isotherm model";
  c_pdT[7,1] = t_ref_CO2_wet + alpha_CO2_wet * (1 - T_ref_CO2_wet / (T_adsorpt+dT))
    "Seventh coefficient of the Toth isotherm model";

  c_pdT[1,2] = Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.psat(T=T_adsorpt+dT)
    "First coefficient of the GAB isotherm model";
  c_pdT[2,2] = x_mon_H2O
    "Second coefficient of the GAB isotherm model";
  c_pdT[3,2] = Modelica.Math.exp(((C_H2O - Modelica.Math.exp(D_H2O * (T_adsorpt+dT))) -
    (J_H2O * (T_adsorpt+dT) + I_H2O)) / (Modelica.Constants.R * (T_adsorpt+dT)))
    "Third coefficient of the GAB isotherm model";
  c_pdT[4,2] = Modelica.Math.exp(((F_H2O + G_H2O * (T_adsorpt+dT)) -
    (J_H2O * (T_adsorpt+dT) + I_H2O)) / (Modelica.Constants.R * (T_adsorpt+dT)))
    "Fourth coefficient of the GAB isotherm model";
  c_pdT[5,2] = 0
    "Fivth coefficient of the GAB isotherm model";
  c_pdT[6,2] = 0
    "Sixth coefficient of the GAB isotherm model";
  c_pdT[7,2] = 0
    "Seventh coefficient of the GAB isotherm model";

  //
  // Coefficients of the isotherm model: T - dT K
  //
  c_mdT[1,1] = x_ref_CO2_dry *
    Modelica.Math.exp(chi_CO2_dry * (1 - ((T_adsorpt-dT) / T_ref_CO2_dry)))
    "First coefficient of the Toth isotherm model";
  c_mdT[2,1] = b_ref_CO2_dry *
    Modelica.Math.exp(-DH_CO2_dry / (Modelica.Constants.R * (T_adsorpt-dT)))
    "Second coefficient of the Toth isotherm model";
  c_mdT[3,1] = t_ref_CO2_dry + alpha_CO2_dry * (1 - T_ref_CO2_dry / (T_adsorpt-dT))
    "Third coefficient of the Toth isotherm model";
  c_mdT[4,1] = C_CO2
    "Fourth coefficient of the Toth isotherm model";
  c_mdT[5,1] = x_ref_CO2_wet *
    Modelica.Math.exp(chi_CO2_wet * (1 - ((T_adsorpt-dT) / T_ref_CO2_wet)))
    "Fivth coefficient of the Toth isotherm model";
  c_mdT[6,1] = b_ref_CO2_wet *
    Modelica.Math.exp(-DH_CO2_wet / (Modelica.Constants.R * (T_adsorpt-dT)))
    "Sixth coefficient of the Toth isotherm model";
  c_mdT[7,1] = t_ref_CO2_wet + alpha_CO2_wet * (1 - T_ref_CO2_wet / (T_adsorpt-dT))
    "Seventh coefficient of the Toth isotherm model";

  c_mdT[1,2] = Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.psat(T=T_adsorpt-dT)
    "First coefficient of the GAB isotherm model";
  c_mdT[2,2] = x_mon_H2O
    "Second coefficient of the GAB isotherm model";
  c_mdT[3,2] = Modelica.Math.exp(((C_H2O - Modelica.Math.exp(D_H2O * (T_adsorpt-dT))) -
    (J_H2O * (T_adsorpt-dT) + I_H2O)) / (Modelica.Constants.R * (T_adsorpt-dT)))
    "Third coefficient of the GAB isotherm model";
  c_mdT[4,2] = Modelica.Math.exp(((F_H2O + G_H2O * (T_adsorpt-dT)) -
    (J_H2O * (T_adsorpt-dT) + I_H2O)) / (Modelica.Constants.R * (T_adsorpt-dT)))
    "Fourth coefficient of the GAB isotherm model";
  c_mdT[5,2] = 0
    "Fivth coefficient of the GAB isotherm model";
  c_mdT[6,2] = 0
    "Sixth coefficient of the GAB isotherm model";
  c_mdT[7,2] = 0
    "Seventh coefficient of the GAB isotherm model";

  //
  // Annotations
  //
  annotation (experiment(StartTime=0, StopTime=100, Tolerance=1e-6),
Documentation(revisions="<html>
<ul>
  <li>
  August 2, 2024, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>", info="<html>
<p>
This tester shows the behavior of all functions of the weighted-average dual site
Toth-GAB isotherm model. Additionally, the implemented functions for the partial 
derivarives 'dx_dp,' 'dx_dy,' and 'dx_dT' are checked via numerical calculations.
<br/><br/>
As an example, this tester increases the temperature with time. To see the behavior of
all functions, plot the variables <i>x_adsorpt</i>, <i>dx_adsorpt_dp_adsorpt</i>, 
<i>dx_adsorpt_dy_i</i>, and <i>dx_adsorpt_dT_adsorpt</i> over the variable 
<i>p_adsorpt</i>. The simulation time is correctly preset (Start: 0 s, Stop = 100 s).  
</p>
</html>"));
end Test_changing_temperature;
