within SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.Testers.TothGAB_Schellevis;
model Test_changing_everything
  "Tester for all functions of the Toth-GAB isotherm model developed by Schellevis (2023): Changing pressure, independent mole fractions, and temperature"

  //
  // Definition of parameters of component 1 (i.e., CO2)
  //
  parameter SorpLib.Units.Uptake x_ref_CO2 = 3.40 * 0.0440098
    "Saturation uptake at reference temperature"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real chi_CO2(unit="1") = 0
    "Parameter describing the change of the saturation uptake with temperature"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real b_ref_CO2(unit="1/Pa") = 93e-5
    "Toth coefficient at reference temperature"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real Q_CO2(unit="J/mol") = 95.3e3
    "Parameter describing the change of the Toth coefficient with temperature"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real t_ref_CO2(unit="1") = 0.37
    "Toth exponent at reference temperature"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real alpha_CO2(unit="1") = 0.33
    "Parameter describing the change of the Toth exponent with temperature"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real beta_CO2(unit="1") = 2.05
    "First coefficient of the enhancement factor"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real gamma_CO2(unit="kg/kg") = -0.85 / 0.018015267
    "Second coefficient of the enhancement factor"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Modelica.Units.SI.Temperature T_ref_CO2 = 353.15
    "Reference temperature"
    annotation (Dialog(tab="General", group="Parameters"));

  //
  // Definition of parameters of component 2 (i.e., H20)
  //
  parameter SorpLib.Units.Uptake x_mon_H2O = 5.55 * 0.018015267
    "Monolayer uptake at reference temperature"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real c_ref_H2O(unit="1") = 100
    "First affinity coefficient at reference temperature"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real Q_c_CO2(unit="J/mol") = -8.69e3
    "Parameter describing the change of the first affinity coefficient with 
    temperature"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real k_ref_H2O(unit="1") = 0.92
    "Second affinity coefficient at reference temperature"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real Q_k_CO2(unit="J/mol") = -0.82e3
    "Parameter describing the change of the second affinity coefficient with 
    temperature"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Modelica.Units.SI.Temperature T_ref_H2O = 353.15
    "Reference temperature"
    annotation (Dialog(tab="General", group="Parameters"));

  extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialTestMulti(
    final p_adsorpt_der = 100,
    final y_i_der = {-0.90/100},
    final T_adsorpt_der = 75/100,
    final p_adsorpt_start = 1,
    final y_i_start = {0.95},
    final T_adsorpt_start = 298.15,
    final no_components = 2,
    final no_coefficients = 6,
    redeclare final package IsothermModel =
      SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.TothGAB_Schellevis,
    p_threshold_min = 0);

equation
  //
  // Coefficients of the isotherm model
  //
  c[1,1] = Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.psat(T=T_adsorpt)
    "First coefficient of the Toth isotherm model";
  c[2,1] = x_ref_CO2 * Modelica.Math.exp(chi_CO2 * (1 - (T_adsorpt / T_ref_CO2)))
    "Second coefficient of the Toth isotherm model";
  c[3,1] = b_ref_CO2 * Modelica.Math.exp(Q_CO2 /
    (Modelica.Constants.R * T_ref_CO2) *(T_ref_CO2 / T_adsorpt - 1))
    "Third coefficient of the Toth isotherm model";
  c[4,1] = t_ref_CO2 + alpha_CO2 * (1 - T_ref_CO2 / T_adsorpt)
    "Fourth coefficient of the Toth isotherm model";
  c[5,1] = beta_CO2
    "Fivth coefficient of the Toth isotherm model";
  c[6,1] = gamma_CO2
    "Sixth coefficient of the Toth isotherm model";

  c[1,2] = Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.psat(T=T_adsorpt)
    "First coefficient of the GAB isotherm model";
  c[2,2] = x_mon_H2O
    "Second coefficient of the GAB isotherm model";
  c[3,2] = c_ref_H2O * Modelica.Math.exp(Q_c_CO2/
    (Modelica.Constants.R * T_adsorpt))
    "Third coefficient of the GAB isotherm model";
  c[4,2] = k_ref_H2O * Modelica.Math.exp(Q_k_CO2/
    (Modelica.Constants.R * T_adsorpt))
    "Fourth coefficient of the GAB isotherm model";
  c[5,2] = 0
    "Fivth coefficient of the GAB isotherm model";
  c[6,2] = 0
    "Sixth coefficient of the GAB isotherm model";

  //
  // Partial derivative of coefficients of isotherm model w.r.t. temperature
  //
  dc_dT[1,1] = Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.dptofT(T=T_adsorpt)
    "First coefficient of the Toth isotherm model";
  dc_dT[2,1] = -chi_CO2 / T_ref_CO2 * c[2,1]
    "Second coefficient of the Toth isotherm model";
  dc_dT[3,1] = -Q_CO2 / (Modelica.Constants.R * T_adsorpt^2) * c[3,1]
    "Third coefficient of the Toth isotherm model";
  dc_dT[4,1] = alpha_CO2 * T_ref_CO2 / T_adsorpt^2
    "Fourth coefficient of the Toth isotherm model";
  dc_dT[5,1] = 0
    "Fivth coefficient of the Toth isotherm model";
  dc_dT[6,1] = 0
    "Sixth coefficient of the Toth isotherm model";

  dc_dT[1,2] = Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.dptofT(T=T_adsorpt)
    "First coefficient of the GAB isotherm model";
  dc_dT[2,2] = 0
    "Second coefficient of the GAB isotherm model";
  dc_dT[3,2] = -Q_c_CO2 / (Modelica.Constants.R * T_adsorpt^2) * c[3,2]
    "Third coefficient of the GAB isotherm model";
  dc_dT[4,2] = -Q_k_CO2 / (Modelica.Constants.R * T_adsorpt^2) * c[4,2]
    "Fourth coefficient of the GAB isotherm model";
  dc_dT[5,2] = 0
    "Fivth coefficient of the GAB isotherm model";
  dc_dT[6,2] = 0
    "Sixth coefficient of the GAB isotherm model";

  //
  // Coefficients of the isotherm model: T + dT K
  //
  c_pdT[1,1] = Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.psat(T=T_adsorpt+dT)
    "First coefficient of the Toth isotherm model";
  c_pdT[2,1] = x_ref_CO2 * Modelica.Math.exp(chi_CO2 * (1 - ((T_adsorpt+dT) / T_ref_CO2)))
    "Second coefficient of the Toth isotherm model";
  c_pdT[3,1] = b_ref_CO2 * Modelica.Math.exp(Q_CO2 /
    (Modelica.Constants.R * T_ref_CO2) *(T_ref_CO2 / (T_adsorpt+dT) - 1))
    "Third coefficient of the Toth isotherm model";
  c_pdT[4,1] = t_ref_CO2 + alpha_CO2 * (1 - T_ref_CO2 / (T_adsorpt+dT))
    "Fourth coefficient of the Toth isotherm model";
  c_pdT[5,1] = beta_CO2
    "Fivth coefficient of the Toth isotherm model";
  c_pdT[6,1] = gamma_CO2
    "Sixth coefficient of the Toth isotherm model";

  c_pdT[1,2] = Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.psat(T=T_adsorpt+dT)
    "First coefficient of the GAB isotherm model";
  c_pdT[2,2] = x_mon_H2O
    "Second coefficient of the GAB isotherm model";
  c_pdT[3,2] = c_ref_H2O * Modelica.Math.exp(Q_c_CO2/
    (Modelica.Constants.R * (T_adsorpt+dT)))
    "Third coefficient of the GAB isotherm model";
  c_pdT[4,2] = k_ref_H2O * Modelica.Math.exp(Q_k_CO2/
    (Modelica.Constants.R * (T_adsorpt+dT)))
    "Fourth coefficient of the GAB isotherm model";
  c_pdT[5,2] = 0
    "Fivth coefficient of the GAB isotherm model";
  c_pdT[6,2] = 0
    "Sixth coefficient of the GAB isotherm model";

  //
  // Coefficients of the isotherm model: T - dT K
  //
  c_mdT[1,1] = Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.psat(T=T_adsorpt-dT)
    "First coefficient of the Toth isotherm model";
  c_mdT[2,1] = x_ref_CO2 * Modelica.Math.exp(chi_CO2 * (1 - ((T_adsorpt-dT) / T_ref_CO2)))
    "Second coefficient of the Toth isotherm model";
  c_mdT[3,1] = b_ref_CO2 * Modelica.Math.exp(Q_CO2 /
    (Modelica.Constants.R * T_ref_CO2) *(T_ref_CO2 / (T_adsorpt-dT) - 1))
    "Third coefficient of the Toth isotherm model";
  c_mdT[4,1] = t_ref_CO2 + alpha_CO2 * (1 - T_ref_CO2 / (T_adsorpt-dT))
    "Fourth coefficient of the Toth isotherm model";
  c_mdT[5,1] = beta_CO2
    "Fivth coefficient of the Toth isotherm model";
  c_mdT[6,1] = gamma_CO2
    "Sixth coefficient of the Toth isotherm model";

  c_mdT[1,2] = Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.psat(T=T_adsorpt-dT)
    "First coefficient of the GAB isotherm model";
  c_mdT[2,2] = x_mon_H2O
    "Second coefficient of the GAB isotherm model";
  c_mdT[3,2] = c_ref_H2O * Modelica.Math.exp(Q_c_CO2/
    (Modelica.Constants.R * (T_adsorpt-dT)))
    "Third coefficient of the GAB isotherm model";
  c_mdT[4,2] = k_ref_H2O * Modelica.Math.exp(Q_k_CO2/
    (Modelica.Constants.R * (T_adsorpt-dT)))
    "Fourth coefficient of the GAB isotherm model";
  c_mdT[5,2] = 0
    "Fivth coefficient of the GAB isotherm model";
  c_mdT[6,2] = 0
    "Sixth coefficient of the GAB isotherm model";

  //
  // Annotations
  //
  annotation (experiment(StartTime=0, StopTime=100, Tolerance=1e-6),
Documentation(revisions="<html>
<ul>
  <li>
  August 5, 2024, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>", info="<html>
<p>
This tester shows the behavior of all functions of the Toth-GAB isotherm 
model. Additionally, the implemented functions for the partial derivarives 'dx_dp,'
'dx_dy,' and 'dx_dT' are checked via numerical calculations.
<br/><br/>
As an example, this tester increases the pressure, independent mole fractions, and
temperature with time. To  see the behavior of all functions, plot the variables 
<i>x_adsorpt</i>, <i>dx_adsorpt_dp_adsorpt</i>, <i>dx_adsorpt_dy_i</i>, and 
<i>dx_adsorpt_dT_adsorpt</i> over the variable <i>p_adsorpt</i>. The simulation 
time is correctly preset (Start: 0 s, Stop = 100 s).  
</p>
</html>"));
end Test_changing_everything;
