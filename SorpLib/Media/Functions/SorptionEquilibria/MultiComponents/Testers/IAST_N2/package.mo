within SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.Testers;
package IAST_N2 "Models ot test and varify functions of the IAST for two components"
  extends Modelica.Icons.ExamplesPackage;

  annotation (Documentation(info="<html>
<p>
This package contains executable test models for all functions of the IAST for two 
components. The test models check the implementation of the functions and enable 
verification of the function behavior. Four test models are implemented, in which the 
pressure, mole fractions of independent components in the gas or vapor phase, and 
temperature change over time. In addition, the test models demonstrate the functions' 
general applicability.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end IAST_N2;
