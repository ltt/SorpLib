within SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.Testers.Toth;
model Test_changing_pressure
  "Tester for all functions of the extended Toth isotherm model: Changing pressure"

  //
  // Definition of parameters
  //
  parameter SorpLib.Units.Uptake[no_components] x_ref = {0.38, 0.28}
    "Saturation uptake at reference temperature"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real[no_components] chi(each unit="1") = {0, 0.1}
    "Parameter describing the change of the saturation uptake with temperature"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real[no_components] b_ref(each unit="1/Pa") = {10.54e-3, 10.54e-3}
    "Toth coefficient at reference temperature"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real[no_components] Q(each unit="J/mol") = {46.093e3, 36.093e3}
    "Parameter describing the change of the Toth coefficient with temperature"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real[no_components] t_ref(each unit="1") = {0.2842, 0.2842}
    "Toth exponent at reference temperature"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real[no_components] alpha(each unit="1") = {0.284, 0.284}
    "Parameter describing the change of the Toth exponent with temperature"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Modelica.Units.SI.Temperature[no_components] T_ref = {283, 283}
    "Reference temperature"
    annotation (Dialog(tab="General", group="Parameters"));

  extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialTestMulti(
    final p_adsorpt_der = 1400,
    final y_i_der = {0},
    final T_adsorpt_der = 0,
    final p_adsorpt_start = 1,
    final y_i_start = {0.5},
    final T_adsorpt_start = 298.15,
    final no_components = 2,
    final no_coefficients = 3,
    redeclare final package IsothermModel =
      SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.Toth,
    p_threshold_min = 0);

equation
  for ind_comp in 1:no_components loop
    //
    // Coefficients of the isotherm model
    //
    c[1,ind_comp] = x_ref[ind_comp] * exp(chi[ind_comp] *
      (1 - T_adsorpt/T_ref[ind_comp]))
      "Coefficients of isotherm model";
    c[2,ind_comp] = b_ref[ind_comp] * exp(Q[ind_comp]/Modelica.Constants.R/T_ref[ind_comp] *
      (T_ref[ind_comp]/T_adsorpt - 1))
      "Coefficients of isotherm model";
    c[3,ind_comp] = t_ref[ind_comp] + alpha[ind_comp] * (1 - T_ref[ind_comp]/T_adsorpt)
      "Coefficients of isotherm model";

    //
    // Partial derivative of coefficients of isotherm model w.r.t. temperature
    //
    dc_dT[1,ind_comp] = -chi[ind_comp]/T_ref[ind_comp] * c[1,ind_comp]
      "Partial derivative of coefficients of isotherm model w.r.t. temperature";
    dc_dT[2,ind_comp] = -Q[ind_comp]/Modelica.Constants.R/T_adsorpt^2 * c[2,ind_comp]
      "Partial derivative of coefficients of isotherm model w.r.t. temperature";
    dc_dT[3,ind_comp] = T_ref[ind_comp]*alpha[ind_comp]/T_adsorpt^2
      "Partial derivative of coefficients of isotherm model w.r.t. temperature";

    //
    // Coefficients of the isotherm model: T + dT K
    //
    c_pdT[1,ind_comp] = x_ref[ind_comp] * exp(chi[ind_comp] *
      (1 - (T_adsorpt+dT)/T_ref[ind_comp]))
      "Coefficients of isotherm model: T + dT K";
    c_pdT[2,ind_comp] = b_ref[ind_comp] * exp(Q[ind_comp]/Modelica.Constants.R/T_ref[ind_comp] *
      (T_ref[ind_comp]/(T_adsorpt+dT) - 1))
      "Coefficients of isotherm model: T + dT K";
    c_pdT[3,ind_comp] = t_ref[ind_comp] + alpha[ind_comp] *
      (1 - T_ref[ind_comp]/(T_adsorpt+dT))
      "Coefficients of isotherm model: T + dT K";

    //
    // Coefficients of the isotherm model: T - dT K
    //
    c_mdT[1,ind_comp] = x_ref[ind_comp] * exp(chi[ind_comp] *
      (1 - (T_adsorpt-dT)/T_ref[ind_comp]))
      "Coefficients of isotherm model: T - dT K";
    c_mdT[2,ind_comp] = b_ref[ind_comp] * exp(Q[ind_comp]/Modelica.Constants.R/T_ref[ind_comp] *
      (T_ref[ind_comp]/(T_adsorpt-dT) - 1))
      "Coefficients of isotherm model: T - dT K";
    c_mdT[3,ind_comp] = t_ref[ind_comp] + alpha[ind_comp] *
      (1 - T_ref[ind_comp]/(T_adsorpt-dT))
      "Coefficients of isotherm model: T - dT K";
  end for;

  //
  // Annotations
  //
  annotation (experiment(StartTime=0, StopTime=100, Tolerance=1e-6),
Documentation(revisions="<html>
<ul>
  <li>
  November 8, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>", info="<html>
<p>
This tester shows the behavior of all functions of the extended Toth isotherm 
model. Additionally, the implemented functions for the partial derivarives 'dx_dp,'
'dx_dy,' and 'dx_dT' are checked via numerical calculations.
<br/><br/>
As an example, this tester increases the pressure with time. To see the behavior of
all functions, plot the variables <i>x_adsorpt</i>, <i>dx_adsorpt_dp_adsorpt</i>, 
<i>dx_adsorpt_dy_i</i>, and <i>dx_adsorpt_dT_adsorpt</i> over the variable 
<i>p_adsorpt</i>. The simulation time is correctly preset (Start: 0 s, Stop = 100 s).  
</p>
</html>"));
end Test_changing_pressure;
