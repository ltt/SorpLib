within SorpLib.Media.Functions.SorptionEquilibria.MultiComponents;
package Testers "This package contains test models to check and varify multi-component isotherm models"
extends Modelica.Icons.ExamplesPackage;

annotation (Documentation(info="<html>
<p>
This package contains executable test models for all implemented isotherm models. Each
isotherm model has its test models saved in a separate package.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end Testers;
