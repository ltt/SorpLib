within SorpLib.Media.Functions.SorptionEquilibria.MultiComponents;
package Sips "Package containing all functions regarding the extended Sips isotherm"
  extends
  SorpLib.Media.Functions.SorptionEquilibria.Interfaces.PartialMultiComponents;

  //
  // Internal package
  //
  redeclare final function extends x_pyT
    "Extended Sips isotherm model: Uptakes as function of pressure, mole fractions of independent gas phase components, and temperature"

    //
    // Definition of variables
    //
protected
    Real[size(c,2)] num_i
      "Individual numerator for each component of the isotherm model";
    Real den
      "Identical denominator for each component of the isotherm model";

  algorithm
    (p_i, num_i, den) :=
      SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.Sips.Internals.calc_p_i_num_i_den(
      p_i=p_i, c=c, p_threshold_min=p_threshold_min)
      "Regulated partial pressures and isotherm models' numerators and denominator";

    x_adsorpt := num_i ./ den
      "Calculate equilibrium uptakes of the adsorpt phase";

    //
    // Annotations
    //
    annotation (Inline=false,
  InlineAfterIndexReduction=false,
  LateInline=true,
  inverse(p_adsorpt = SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.Sips.p_xyT(x_adsorpt=x_adsorpt, y_i=y_i, T_adsorpt=T_adsorpt, c=c, p_threshold_min=p_threshold_min, p_adsorpt_lb_start=p_adsorpt_lb_start, p_adsorpt_ub_start=p_adsorpt_ub_start, tolerance=tolerance),
          y_i = SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.Sips.y_pxT(p_adsorpt=p_adsorpt, x_adsorpt=x_adsorpt, T_adsorpt=T_adsorpt, c=c, p_threshold_min=p_threshold_min, p_adsorpt_lb_start=p_adsorpt_lb_start, p_adsorpt_ub_start=p_adsorpt_ub_start, tolerance=tolerance)));
  end x_pyT;

  redeclare final function extends p_xyT
    "Extended Sips isotherm model: Pressure as function of uptakes, mole fractions of independent gas phase components, and temperature"
  algorithm
    p_i :=
      SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.Sips.Internals.p_i_xT(
        x_adsorpt=x_adsorpt, T_adsorpt=T_adsorpt, c=c, p_threshold_min=p_threshold_min)
        "Partial pressures";

    p_adsorpt := sum(p_i)
      "Equilibrium pressure";

    //
    // Annotations
    //
    annotation (Inline=false,
  InlineAfterIndexReduction=false,
  LateInline=true,
  inverse(x_adsorpt = SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.Sips.x_pyT(p_adsorpt=p_adsorpt, y_i=y_i, T_adsorpt=T_adsorpt, c=c, p_threshold_min=p_threshold_min, p_adsorpt_lb_start=p_adsorpt_lb_start, p_adsorpt_ub_start=p_adsorpt_ub_start, tolerance=tolerance),
          y_i = SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.Sips.y_pxT(p_adsorpt=p_adsorpt, x_adsorpt=x_adsorpt, T_adsorpt=T_adsorpt, c=c, p_threshold_min=p_threshold_min, p_adsorpt_lb_start=p_adsorpt_lb_start, p_adsorpt_ub_start=p_adsorpt_ub_start, tolerance=tolerance)));
  end p_xyT;

  redeclare final function extends y_pxT
    "Extended Sips isotherm model: Mole fractions of independent gas phase components as function of uptakes, pressure, and temperature"
  algorithm
    p_i :=
      SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.Sips.Internals.p_i_xT(
       x_adsorpt=x_adsorpt, T_adsorpt=T_adsorpt, c=c, p_threshold_min=p_threshold_min)
       "Partial pressures";

    for ind in 1:size(c,2)-1 loop
      y_i[ind] := p_i[ind] / max(p_adsorpt, p_threshold_min)
        "Mole fractions of independent components in the gas or vapor phase";
    end for;

    //
    // Annotations
    //
    annotation (Inline=false,
  InlineAfterIndexReduction=false,
  LateInline=true,
  inverse(x_adsorpt = SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.Sips.x_pyT(p_adsorpt=p_adsorpt, y_i=y_i, T_adsorpt=T_adsorpt, c=c, p_threshold_min=p_threshold_min, p_adsorpt_lb_start=p_adsorpt_lb_start, p_adsorpt_ub_start=p_adsorpt_ub_start, tolerance=tolerance),
          p_adsorpt = SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.Sips.p_xyT(x_adsorpt=x_adsorpt, y_i=y_i, T_adsorpt=T_adsorpt, c=c, p_threshold_min=p_threshold_min, p_adsorpt_lb_start=p_adsorpt_lb_start, p_adsorpt_ub_start=p_adsorpt_ub_start, tolerance=tolerance)));
  end y_pxT;

  redeclare final function extends py_xT
    "Extended Sips isotherm model: Pressure and mole fractions of independent gas phase components as function of uptakes and temperature"
  algorithm
    p_i :=
      SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.Sips.Internals.p_i_xT(
       x_adsorpt=x_adsorpt, T_adsorpt=T_adsorpt, c=c, p_threshold_min=p_threshold_min)
       "Partial pressures";

    p_adsorpt := max(sum(p_i), p_threshold_min)
      "Equilibrium pressure";

    for ind in 1:size(c,2)-1 loop
      y_i[ind] := p_i[ind] / p_adsorpt
        "Mole fractions of independent components in the gas or vapor phase";
    end for;

    //
    // Annotations
    //
    annotation (Inline=false,
  InlineAfterIndexReduction=false,
  LateInline=true);
  end py_xT;

  redeclare final function extends dx_dp
    "Extended Sips isotherm model: Partial derivative of uptakes w.r.t. pressure at constant mole fractions and temperature"

    //
    // Definition of variables
    //
protected
    Real[size(c,2)] num_i
      "Individual numerator for each component of the isotherm model";
    Real den
      "Identical denominator for each component of the isotherm model";

    Real[size(c,2)] dnum_i_dp_adsorpt
      "Partial derivatives of the numerators w.r.t. the equilibrium pressure";
    Real dden_dp_adsorpt
      "Partial derivative of the denominator w.r.t. the equilbrium pressure";

  algorithm
    (p_i, num_i, den) :=
      SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.Sips.Internals.calc_p_i_num_i_den(
      p_i=p_i, c=c, p_threshold_min=p_threshold_min)
      "Regulated partial pressures and isotherm models' numerators and denominator";

    //
    // Calculate partial derivatives
    //
    dden_dp_adsorpt := 0
      "Start value of the derivative of the denominator w.r.t. equilbrium pressure";

    for ind_comp in 1:size(c,2) loop
      dnum_i_dp_adsorpt[ind_comp] := num_i[ind_comp] /
        (c[3,ind_comp] * p_i[ind_comp]) * y_i_[ind_comp]
        "Partial derivatives of numerators w.r.t. equilibrium pressure";

      dden_dp_adsorpt := dden_dp_adsorpt + (c[2,ind_comp] * p_i[ind_comp]) ^
        (1/c[3,ind_comp]) / (c[3,ind_comp] * p_i[ind_comp]) * y_i_[ind_comp]
        "Partial derivative of denominator w.r.t. equilibrium pressure";
    end for;

    //
    // Calculate derivatives of loadings w.r.t. equilibrium pressure
    //
    dx_adsorpt_dp_adsorpt :=
      (dnum_i_dp_adsorpt.* den .- num_i.* dden_dp_adsorpt) ./ den^2
      "Calculation of the partial derivatives of the equilibrium uptakes w.r.t. the
     equilibrium pressure at constant mole fractions and temperature";
  end dx_dp;

  redeclare final function extends dx_dy
    "Extended Sips isotherm model: Partial derivative of uptakes w.r.t. mole fractions of independent gas phase components at constant pressure and temperature"

    //
    // Definition of variables
    //
protected
    Real[size(c,2)] num_i
      "Individual numerator for each component of the isotherm model";
    Real den
      "Identical denominator for each component of the isotherm model";

    Real[size(c,2),size(c,2)-1] dnum_i_dy_i
      "Partial derivatives of the numerators w.r.t. the mole fractions of independent 
    gas phase components";
    Real[size(c,2)-1] dden_dy_i
      "Partial derivative of the denominator w.r.t. the mole fractions of independent 
    gas phase components";

  algorithm
    (p_i, num_i, den) :=
      SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.Sips.Internals.calc_p_i_num_i_den(
      p_i=p_i, c=c, p_threshold_min=p_threshold_min)
      "Regulated partial pressures and isotherm models' numerators and denominator";

    //
    // Calculate partial derivatives of numerators, which varies with components and
    // mole fractions of independent components
    //
    dnum_i_dy_i:=zeros(size(c,2),size(c,2)-1)
      "Partial derivatives of the numerators w.r.t. the mole fractions of independent 
    gas phase components";

    for ind_comp in 1:size(c,2) loop
      if ind_comp < size(c,2) then
        // Partial derivatives of independent components
        //
        dnum_i_dy_i[ind_comp,ind_comp] := num_i[ind_comp] /
          (c[3,ind_comp] * p_i[ind_comp]) * p_adsorpt
          "Partial derivatives of numerators w.r.t. the mole fractions of independent 
        gas phase components";

      else
        // Partial derivatives of dependent component
        //
        for ind_y_i in 1:size(c,2)-1 loop
          dnum_i_dy_i[ind_comp,ind_y_i] :=  num_i[ind_comp] /
          (c[3,ind_comp] * p_i[ind_comp]) * (-p_adsorpt)
          "Partial derivatives of numerators w.r.t. the mole fractions of independent 
        gas phase components";

        end for;
      end if;
    end for;

    //
    // Calculate partial derivatives of the denominator, which varies with components and
    // mole fractions of independent components
    //
    dden_dy_i:=zeros(size(c, 2) - 1)
      "Partial derivative of the denominator w.r.t. the mole fractions of independent 
    gas phase components";

    for ind_y_i in 1:size(c,2)-1 loop
      dden_dy_i[ind_y_i] := dden_dy_i[ind_y_i] +
        (c[2,ind_y_i] * p_i[ind_y_i]) ^ (1/c[3,ind_y_i]) /
        (c[3,ind_y_i] * p_i[ind_y_i]) * p_adsorpt +
        (c[2,size(c,2)] * p_i[size(c,2)]) ^ (1/c[3,size(c,2)]) /
        (c[3,size(c,2)] * p_i[size(c,2)]) * (-p_adsorpt)
        "Partial derivative of the denominator w.r.t. the mole fractions of independent 
      gas phase components";
    end for;

    //
    // Calculate partial derivatives of uptakes w.r.t. mole fractions of independent
    // gas phase components
    //
    for ind_comp in 1:size(c,2) loop
      for ind_y_i in 1:size(c,2)-1 loop
        dx_adsorpt_dy_i[ind_comp,ind_y_i] :=
          (dnum_i_dy_i[ind_comp,ind_y_i] * den -
          num_i[ind_comp] * dden_dy_i[ind_y_i]) ./ den^2
          "Partial derivatives of the uptakes w.r.t. the mole fractions of independent 
        gas phase components at constant pressure and temperature";
      end for;
    end for;
  end dx_dy;

  redeclare final function extends dx_dT
    "Extended Sips isotherm model: Partial derivative of uptakes w.r.t. temperature at constant pressure and mole fractions"

    //
    // Definition of variables
    //
protected
    Real[size(c,2)] num_i
      "Individual numerator for each component of the isotherm model";
    Real den
      "Identical denominator for each component of the isotherm model";

    Real[size(c,2)] dnum_i_dT_adsorpt
      "Derivatives of the numerators w.r.t. the equilibrium temperature";
    Real dden_dT_adsorpt
      "Derivative of the denominator w.r.t. the equilbrium temperature";

  algorithm
    (p_i, num_i, den) :=
      SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.Sips.Internals.calc_p_i_num_i_den(
      p_i=p_i, c=c, p_threshold_min=p_threshold_min)
      "Regulated partial pressures and isotherm models' numerators and denominator";

    //
    // Calculate partial derivatives
    //
    dden_dT_adsorpt := 0
      "Start value of the derivative of the denominator w.r.t. equilbrium pressure";

    for ind_comp in 1:size(c,2) loop
      dnum_i_dT_adsorpt[ind_comp] :=
        (num_i[ind_comp] / c[1,ind_comp]) * dc_dT_adsorpt[1,ind_comp] +
        (num_i[ind_comp] / (c[2,ind_comp] * c[3,ind_comp])) *
        dc_dT_adsorpt[2,ind_comp] +
        (-num_i[ind_comp] * log(c[2,ind_comp] *p_i[ind_comp]) / c[3,ind_comp]^2) *
        dc_dT_adsorpt[3,ind_comp]
        "Derivatives of numerators w.r.t. equilibrium temperature";

      dden_dT_adsorpt := dden_dT_adsorpt +
        ((c[2,ind_comp] * p_i[ind_comp]) ^ (1/c[3,ind_comp]) /
        (c[2,ind_comp] * c[3,ind_comp])) * dc_dT_adsorpt[2,ind_comp] +
        (-(c[2,ind_comp] * p_i[ind_comp]) ^ (1/c[3,ind_comp]) *
        log(c[2,ind_comp] * p_i[ind_comp]) / c[3,ind_comp]^2) *
        dc_dT_adsorpt[3,ind_comp]
        "Derivative of denominator w.r.t. equilibrium temperature";
    end for;

    //
    // Calculate derivatives of uptakes wrt. total pressure
    //
    dx_adsorpt_dT_adsorpt :=
      (dnum_i_dT_adsorpt.* den .- num_i.* dden_dT_adsorpt) ./ den^2
      "Calculation of the partial derivatives of the equilibrium uptakes w.r.t. the
     equilibrium pressure at constant pressure and mole fractions";
  end dx_dT;
  //
  // Annotations
  //
annotation (Documentation(revisions="<html>
<ul>
  <li>
  November 9, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>", info="<html>
<p>
The extended Sips isotherm model calculates equilibrium uptakes <i>x_adsorpt</i>
as a function of the equilibrium pressure <i>p_adsorpt</i>, mole fractions of
independent components in the gas or vapor phase <i>y_i</i>, and the equilibrium
temperature <i>T_adsorpt</i>. Each component of the extended Sips isotherm model
has three parameters.
</p>

<h4>Main equations</h4>
<p>
The extended Sips isotherm model has the following form:
</p>
<pre>
    x<sub>adsorpt,<i>i</i></sub> = x<sub>sat,<i>i</i></sub>(T<sub>adsorpt</sub>) * (b<sub><i>i</i></sub>(T<sub>adsorpt</sub>) * p<sub>adsorpt,<i>i</i></sub>) ^ (1/n<sub><i>i</i></sub>(T<sub>adsorpt</sub>)) / (1 + &sum;[(b<sub><i>i</i></sub>(T<sub>adsorpt</sub>) * p<sub>adsorpt,<i>i</i></sub>) ^ (1/n<sub><i>i</i></sub>(T<sub>adsorpt</sub>))]);
</pre>
<p>
where <i>x<sub>sat,<i>i</i></sub>(T<sub>adsorpt</sub>)</i> is the saturation uptake
of component <i>i</i>, <i>b<sub><i>i</i></sub>(T<sub>adsorpt</sub>)</i> is the Sips 
coefficient of component <i>i</i>, and <i>n<sub><i>i</i></sub>(T<sub>adsorpt</sub>)</i>
is the Sips exponent of component <i>i</i>. Typical temperature dependencies may 
have the following forms:
</p>
<pre>
    x<sub>sat,<i>i</i></sub>(T<sub>adsorpt</sub>) =  x<sub>ref,<i>i</i></sub> * <strong>exp</strong>(&Chi;<sub><i>i</i></sub> * (1 - T<sub>adsorpt</sub>/T<sub>ref,<i>i</i></sub>));
</pre>
<pre>
    b<sub><i>i</i></sub>(T<sub>adsorpt</sub>) = b<sub>ref,<i>i</i></sub> * <strong>exp</strong>(Q<sub><i>i</i></sub>/(R * T<sub>ref,<i>i</i></sub>) * (T<sub>ref,<i>i</i></sub>/T<sub>adsorpt</sub> - 1));
</pre>
<pre>
    n<sub><i>i</i></sub>(T<sub>adsorpt</sub>) = (1/n<sub>ref,<i>i</i></sub> + &alpha;<sub><i>i</i></sub> * (1 - T<sub>ref,<i>i</i></sub>/T<sub>adsorpt</sub>)) ^ (-1);
</pre>
<p>
Herein, for each component <i>i</i>, <i>x<sub>ref,<i>i</i></sub></i> is the saturation 
uptake at reference temperature <i>T<sub>ref,<i>i</i></sub></i>, <i>b<sub>ref,<i>i</i></sub></i> 
is the Sips coefficient at reference temperature, and <i>n<sub>ref,<i>i</i></sub></i> 
is the Sips exponent at reference temperature. The parameter <i>Q<sub><i>i</i></sub></i> 
is a measure for the isosteric adsorption enthalpy at a fractional loading of 
<i>x<sub>adsorpt,<i>i</i></sub>/x<sub>sat,<i>i</i></sub>(T<sub>adsorpt</sub>) = 0.5</i>, 
the parameter <i>&Chi;<sub><i>i</i></sub></i> describes the change of the saturation 
uptake with temperature, and the parameter <i>&alpha;<sub><i>i</i></sub></i> describes 
the change of the Sips exponent with temperature. All seven parameters can be used as 
fitting parameters.
<br/><br/>
Note that the Sips exponent <i>n,<i>i</i>(T<sub>adsorpt</sub>)</i> is typically greater 
than unity. For <i>n,<i>i</i>(T<sub>adsorpt</sub>) = 1</i>, the Sips isotherm becomes 
the Langmuir isotherm. Hence, the Sips exponent <i>n,<i>i</i>(T<sub>adsorpt</sub>)</i> 
can be interpreted as a parameter describing the heterogeneity of the adsorption system. 
</p>

<h4>Required parameter order in function input c[:,no_components]:</h4>
<p>
For each component <i>i</i>, the required parameter order in the function input <i>c</i>
is as follows:
</p>
<ul>
  <li>
  c[1,<i>i</i>] = x<sub>sat,<i>i</i></sub>(T<sub>adsorpt</sub>) in kg/kg
  </li>
  <li>
  c[2,<i>i</i>] = b<sub><i>i</i></sub>(T<sub>adsorpt</sub>) in 1/Pa
  </li>
  <li>
  c[3,<i>i</i>] = n<sub><i>i</i></sub>(T<sub>adsorpt</sub>) in -
  </li>
</ul>

<h4>Assumptions and limitations</h4>
<ul>
  <li>
  No proper Henry law behavior.
  </li>
</ul>

<h4>Example</h4>
<p>
The following figure shows the extended Sips isotherm model for one parameter set
and two components. In the upper sub-figure, the equilibrium pressure changes with 
time. In the centre sub-figure, the independent mole fractions change with time.
In the lower sub-figure, the equilibrium temperature changes with time. The left
side shows the uptake of component 1, and the right side shows the uptake of
component 2. 
<br/><br/>
</p>
<img src=\"Modelica://SorpLib/Resources/doc/media_functions_equilibria_multi_sips.png\" alt=\"media_functions_equilibria_multi_sips.png\">

<h4>References</h4>
<ul>
  <li>
  Do, D. D. (1998). Adsorption Analysis: Equilibria and Kinetics, 1st Edition, ISBN 978-1-86094-130-6, Imperial College Press.
  </li>
  <li>
  Amrutha and Jeppu, G. and Girish, C.R. and Prabhu, B., and Mayer, K. (2023). Multi-component Adsorption Isotherms: Review and Modeling Studies, Environmental Processes, 10:38. DOI: https://doi.org/10.1007/s40710-023-00631-0.
  </li>
</ul>
</html>"));
end Sips;
