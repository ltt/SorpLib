﻿within SorpLib.Media.Functions.SorptionEquilibria.MultiComponents;
package IAST_N3 "Package containing all functions regarding the IAST for three components"
  extends
  SorpLib.Media.Functions.SorptionEquilibria.Interfaces.PartialMultiComponentsIAST;

  //
  // Internal package
  //
  redeclare final function extends x_pyT
    "IAST for three components: Uptakes as function of pressure, mole fractions of independent gas phase components, and temperature"

    //
    // Definition of inputs
    //
    input Real[:] c_3
      "Coefficients of the isotherm model of the third component"
      annotation (Dialog(tab="General", group="Inputs - Components"));

    input
      SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_x_pT func_x_pT_3
      "Uptake of the third component as function of pressure and temperature"
      annotation (Dialog(tab="General", group="Inputs - Functions"));
    input
      SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_p_xT func_p_xT_3
      "Pressure of the third component as function of uptake and temperature"
      annotation (Dialog(tab="General", group="Inputs - Functions"));
    input
      SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_dx_dp func_dx_dp_3
      "Partial derivative of the uptake of the third component w.r.t. the equilibrium 
    pressure"
      annotation (Dialog(tab="General", group="Inputs - Functions"));
    input
      SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_pi_pT func_pi_pT_3
      "Reduced spreading pressure of the third component as function of pressure 
    and temperature"
      annotation (Dialog(tab="General", group="Inputs - Functions"));
    input
      SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_p_piT func_p_piT_3
      "Pressure of the third component as function of reduced spreading pressure
    and temperature"
      annotation (Dialog(tab="General", group="Inputs - Functions"));

    input SorpLib.Media.Functions.SorptionEquilibria.Records.NumericsIAST_PureComponents num_comp_3
      "Record definining numerics of the third component's isotherm model"
      annotation (Dialog(tab="General", group="Inputs - Numerics"),
                  choicesAllMatching=true);

    //
    // Definition of variables
    //
protected
    Modelica.Units.SI.MoleFraction[size(M_i,1)] y_i_ = cat(1, y_i, {1-sum(y_i)})
      "Mole fractions of all components in the vapor or gas phase";

  algorithm
    //
    // Select IAST algorithm
    //
    if num.IASTAlgorithm == SorpLib.Choices.IASTAlgorithm.NewtonRaphson then
      (x_adsorpt,,) :=
        SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.IAST_N3.Internals.x_pyT_NewtonRaphson(
        p_adsorpt=p_adsorpt,
        y_i=y_i_,
        T_adsorpt=T_adsorpt,
        M_i=M_i,
        c_1=c_1,
        c_2=c_2,
        c_3=c_3,
        func_x_pT_1=func_x_pT_1,
        func_p_xT_1=func_p_xT_1,
        func_dx_dp_1=func_dx_dp_1,
        func_pi_pT_1=func_pi_pT_1,
        func_p_piT_1=func_p_piT_1,
        func_x_pT_2=func_x_pT_2,
        func_p_xT_2=func_p_xT_2,
        func_dx_dp_2=func_dx_dp_2,
        func_pi_pT_2=func_pi_pT_2,
        func_p_piT_2=func_p_piT_2,
        func_x_pT_3=func_x_pT_3,
        func_p_xT_3=func_p_xT_3,
        func_dx_dp_3=func_dx_dp_3,
        func_pi_pT_3=func_pi_pT_3,
        func_p_piT_3=func_p_piT_3,
        num=num,
        num_comp_1=num_comp_1,
        num_comp_2=num_comp_2,
        num_comp_3=num_comp_3)
        "Standard 'Newton-Raphson' algorithm";

    elseif num.IASTAlgorithm == SorpLib.Choices.IASTAlgorithm.NestedLoop then
      (x_adsorpt,,) :=
        SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.IAST_N3.Internals.x_pyT_NestedLoop(
        p_adsorpt=p_adsorpt,
        y_i=y_i_,
        T_adsorpt=T_adsorpt,
        M_i=M_i,
        c_1=c_1,
        c_2=c_2,
        c_3=c_3,
        func_x_pT_1=func_x_pT_1,
        func_p_xT_1=func_p_xT_1,
        func_dx_dp_1=func_dx_dp_1,
        func_pi_pT_1=func_pi_pT_1,
        func_p_piT_1=func_p_piT_1,
        func_x_pT_2=func_x_pT_2,
        func_p_xT_2=func_p_xT_2,
        func_dx_dp_2=func_dx_dp_2,
        func_pi_pT_2=func_pi_pT_2,
        func_p_piT_2=func_p_piT_2,
        func_x_pT_3=func_x_pT_3,
        func_p_xT_3=func_p_xT_3,
        func_dx_dp_3=func_dx_dp_3,
        func_pi_pT_3=func_pi_pT_3,
        func_p_piT_3=func_p_piT_3,
        num=num,
        num_comp_1=num_comp_1,
        num_comp_2=num_comp_2,
        num_comp_3=num_comp_3)
        "'Nested Loop' algorithm";

    elseif num.IASTAlgorithm == SorpLib.Choices.IASTAlgorithm.FastIAST then
      (x_adsorpt,,,,) :=
        SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.IAST_N3.Internals.x_pyT_FastIAST(
        p_adsorpt=p_adsorpt,
        y_i=y_i_,
        T_adsorpt=T_adsorpt,
        M_i=M_i,
        c_1=c_1,
        c_2=c_2,
        c_3=c_3,
        func_x_pT_1=func_x_pT_1,
        func_p_xT_1=func_p_xT_1,
        func_dx_dp_1=func_dx_dp_1,
        func_pi_pT_1=func_pi_pT_1,
        func_p_piT_1=func_p_piT_1,
        func_x_pT_2=func_x_pT_2,
        func_p_xT_2=func_p_xT_2,
        func_dx_dp_2=func_dx_dp_2,
        func_pi_pT_2=func_pi_pT_2,
        func_p_piT_2=func_p_piT_2,
        func_x_pT_3=func_x_pT_3,
        func_p_xT_3=func_p_xT_3,
        func_dx_dp_3=func_dx_dp_3,
        func_pi_pT_3=func_pi_pT_3,
        func_p_piT_3=func_p_piT_3,
        num=num,
        num_comp_1=num_comp_1,
        num_comp_2=num_comp_2,
        num_comp_3=num_comp_3)
        "'FASTIast' algorithm";

    end if;

    //
    // Annotations
    //
    annotation (Inline=false,
  InlineAfterIndexReduction=false,
  LateInline=true,
  inverse(p_adsorpt = SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.IAST_N3.p_xyT(x_adsorpt=x_adsorpt, y_i=y_i, T_adsorpt=T_adsorpt, M_i=M_i, c_1=c_1, c_2=c_2, c_3=c_3, func_x_pT_1=func_x_pT_1, func_p_xT_1=func_p_xT_1, func_dx_dp_1=func_dx_dp_1, func_pi_pT_1=func_pi_pT_1, func_p_piT_1=func_p_piT_1, func_x_pT_2=func_x_pT_2, func_p_xT_2=func_p_xT_2, func_dx_dp_2=func_dx_dp_2, func_pi_pT_2=func_pi_pT_2, func_p_piT_2=func_p_piT_2, func_x_pT_3=func_x_pT_3, func_p_xT_3=func_p_xT_3, func_dx_dp_3=func_dx_dp_3, func_pi_pT_3=func_pi_pT_3, func_p_piT_3=func_p_piT_3, num=num, num_comp_1=num_comp_1, num_comp_2=num_comp_2, num_comp_3=num_comp_3),
          y_i = SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.IAST_N3.y_pxT(p_adsorpt=p_adsorpt, x_adsorpt=x_adsorpt, T_adsorpt=T_adsorpt, M_i=M_i, c_1=c_1, c_2=c_2, c_3=c_3, func_x_pT_1=func_x_pT_1, func_p_xT_1=func_p_xT_1, func_dx_dp_1=func_dx_dp_1, func_pi_pT_1=func_pi_pT_1, func_p_piT_1=func_p_piT_1, func_x_pT_2=func_x_pT_2, func_p_xT_2=func_p_xT_2, func_dx_dp_2=func_dx_dp_2, func_pi_pT_2=func_pi_pT_2, func_p_piT_2=func_p_piT_2, func_x_pT_3=func_x_pT_3, func_p_xT_3=func_p_xT_3, func_dx_dp_3=func_dx_dp_3, func_pi_pT_3=func_pi_pT_3, func_p_piT_3=func_p_piT_3, num=num, num_comp_1=num_comp_1, num_comp_2=num_comp_2, num_comp_3=num_comp_3)));
  end x_pyT;

  redeclare final function extends p_xyT
    "IAST for three components: Pressure as function of uptakes, mole fractions of independent gas phase components, and temperature"

    //
    // Definition of inputs
    //
    input Real[:] c_3
      "Coefficients of the isotherm model of the third component"
      annotation (Dialog(tab="General", group="Inputs - Components"));

    input
      SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_x_pT func_x_pT_3
      "Uptake of the third component as function of pressure and temperature"
      annotation (Dialog(tab="General", group="Inputs - Functions"));
    input
      SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_p_xT func_p_xT_3
      "Pressure of the third component as function of uptake and temperature"
      annotation (Dialog(tab="General", group="Inputs - Functions"));
    input
      SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_dx_dp func_dx_dp_3
      "Partial derivative of the uptake of the third component w.r.t. the equilibrium 
    pressure"
      annotation (Dialog(tab="General", group="Inputs - Functions"));
    input
      SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_pi_pT func_pi_pT_3
      "Reduced spreading pressure of the third component as function of pressure 
    and temperature"
      annotation (Dialog(tab="General", group="Inputs - Functions"));
    input
      SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_p_piT func_p_piT_3
      "Pressure of the third component as function of reduced spreading pressure
    and temperature"
      annotation (Dialog(tab="General", group="Inputs - Functions"));

    input SorpLib.Media.Functions.SorptionEquilibria.Records.NumericsIAST_PureComponents num_comp_3
      "Record definining numerics of the third component's isotherm model"
      annotation (Dialog(tab="General", group="Inputs - Numerics"),
                  choicesAllMatching=true);

    //
    // Definition of variables
    //
protected
    Modelica.Units.SI.MoleFraction[size(M_i,1)] y_i_ = cat(1, y_i, {1-sum(y_i)})
      "Mole fractions of all components in the vapor or gas phase";

  algorithm
    (p_adsorpt,,) :=
      SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.IAST_N3.Internals.py_xT_NewtonRaphson(
      x_adsorpt=x_adsorpt,
      T_adsorpt=T_adsorpt,
      M_i=M_i,
      c_1=c_1,
      c_2=c_2,
      c_3=c_3,
      func_x_pT_1=func_x_pT_1,
      func_p_xT_1=func_p_xT_1,
      func_dx_dp_1=func_dx_dp_1,
      func_pi_pT_1=func_pi_pT_1,
      func_p_piT_1=func_p_piT_1,
      func_x_pT_2=func_x_pT_2,
      func_p_xT_2=func_p_xT_2,
      func_dx_dp_2=func_dx_dp_2,
      func_pi_pT_2=func_pi_pT_2,
      func_p_piT_2=func_p_piT_2,
      func_x_pT_3=func_x_pT_3,
      func_p_xT_3=func_p_xT_3,
      func_dx_dp_3=func_dx_dp_3,
      func_pi_pT_3=func_pi_pT_3,
      func_p_piT_3=func_p_piT_3,
      num=num,
      num_comp_1=num_comp_1,
      num_comp_2=num_comp_2,
      num_comp_3=num_comp_3)
      "Standard 'Newton-Raphson' algorithm";

    //
    // Annotations
    //
    annotation (Inline=false,
  InlineAfterIndexReduction=false,
  LateInline=true,
  inverse(x_adsorpt = SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.IAST_N3.x_pyT(p_adsorpt=p_adsorpt, y_i=y_i, T_adsorpt=T_adsorpt, M_i=M_i, c_1=c_1, c_2=c_2, c_3=c_3, func_x_pT_1=func_x_pT_1, func_p_xT_1=func_p_xT_1, func_dx_dp_1=func_dx_dp_1, func_pi_pT_1=func_pi_pT_1, func_p_piT_1=func_p_piT_1, func_x_pT_2=func_x_pT_2, func_p_xT_2=func_p_xT_2, func_dx_dp_2=func_dx_dp_2, func_pi_pT_2=func_pi_pT_2, func_p_piT_2=func_p_piT_2, func_x_pT_3=func_x_pT_3, func_p_xT_3=func_p_xT_3, func_dx_dp_3=func_dx_dp_3, func_pi_pT_3=func_pi_pT_3, func_p_piT_3=func_p_piT_3, num=num, num_comp_1=num_comp_1, num_comp_2=num_comp_2, num_comp_3=num_comp_3),
          y_i = SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.IAST_N3.y_pxT(p_adsorpt=p_adsorpt, x_adsorpt=x_adsorpt, T_adsorpt=T_adsorpt, M_i=M_i, c_1=c_1, c_2=c_2, c_3=c_3, func_x_pT_1=func_x_pT_1, func_p_xT_1=func_p_xT_1, func_dx_dp_1=func_dx_dp_1, func_pi_pT_1=func_pi_pT_1, func_p_piT_1=func_p_piT_1, func_x_pT_2=func_x_pT_2, func_p_xT_2=func_p_xT_2, func_dx_dp_2=func_dx_dp_2, func_pi_pT_2=func_pi_pT_2, func_p_piT_2=func_p_piT_2, func_x_pT_3=func_x_pT_3, func_p_xT_3=func_p_xT_3, func_dx_dp_3=func_dx_dp_3, func_pi_pT_3=func_pi_pT_3, func_p_piT_3=func_p_piT_3, num=num, num_comp_1=num_comp_1, num_comp_2=num_comp_2, num_comp_3=num_comp_3)));
  end p_xyT;

  redeclare final function extends y_pxT
    "IAST for three components: Mole fractions of independent gas phase components as function of uptakes, pressure, and temperature"

    //
    // Definition of inputs
    //
    input Real[:] c_3
      "Coefficients of the isotherm model of the third component"
      annotation (Dialog(tab="General", group="Inputs - Components"));

    input
      SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_x_pT func_x_pT_3
      "Uptake of the third component as function of pressure and temperature"
      annotation (Dialog(tab="General", group="Inputs - Functions"));
    input
      SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_p_xT func_p_xT_3
      "Pressure of the third component as function of uptake and temperature"
      annotation (Dialog(tab="General", group="Inputs - Functions"));
    input
      SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_dx_dp func_dx_dp_3
      "Partial derivative of the uptake of the third component w.r.t. the equilibrium 
    pressure"
      annotation (Dialog(tab="General", group="Inputs - Functions"));
    input
      SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_pi_pT func_pi_pT_3
      "Reduced spreading pressure of the third component as function of pressure 
    and temperature"
      annotation (Dialog(tab="General", group="Inputs - Functions"));
    input
      SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_p_piT func_p_piT_3
      "Pressure of the third component as function of reduced spreading pressure
    and temperature"
      annotation (Dialog(tab="General", group="Inputs - Functions"));

    input SorpLib.Media.Functions.SorptionEquilibria.Records.NumericsIAST_PureComponents num_comp_3
      "Record definining numerics of the third component's isotherm model"
      annotation (Dialog(tab="General", group="Inputs - Numerics"),
                  choicesAllMatching=true);

    //
    // Definition of variables
    //
protected
    Modelica.Units.SI.MoleFraction[size(M_i,1)] y_i_
      "Mole fractions of all components in the vapor or gas phase";

  algorithm
    //
    // Solve inverse of IAST
    //
    (,y_i_,) :=
      SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.IAST_N3.Internals.py_xT_NewtonRaphson(
      x_adsorpt=x_adsorpt,
      T_adsorpt=T_adsorpt,
      M_i=M_i,
      c_1=c_1,
      c_2=c_2,
      c_3=c_3,
      func_x_pT_1=func_x_pT_1,
      func_p_xT_1=func_p_xT_1,
      func_dx_dp_1=func_dx_dp_1,
      func_pi_pT_1=func_pi_pT_1,
      func_p_piT_1=func_p_piT_1,
      func_x_pT_2=func_x_pT_2,
      func_p_xT_2=func_p_xT_2,
      func_dx_dp_2=func_dx_dp_2,
      func_pi_pT_2=func_pi_pT_2,
      func_p_piT_2=func_p_piT_2,
      func_x_pT_3=func_x_pT_3,
      func_p_xT_3=func_p_xT_3,
      func_dx_dp_3=func_dx_dp_3,
      func_pi_pT_3=func_pi_pT_3,
      func_p_piT_3=func_p_piT_3,
      num=num,
      num_comp_1=num_comp_1,
      num_comp_2=num_comp_2,
      num_comp_3=num_comp_3)
      "Standard 'Newton-Raphson' algorithm";

    //
    // Assign correct output values
    //
    y_i := y_i_[1:end-1]
      "Mole fractions of independent components in the gas or vapor phase";

    //
    // Annotations
    //
    annotation (Inline=false,
  InlineAfterIndexReduction=false,
  LateInline=true);
  end y_pxT;

  redeclare final function extends py_xT
    "IAST for three components: Pressure and mole fractions of independent gas phase components as function of uptakes and temperature"

    //
    // Definition of inputs
    //
    input Real[:] c_3
      "Coefficients of the isotherm model of the third component"
      annotation (Dialog(tab="General", group="Inputs - Components"));

    input
      SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_x_pT func_x_pT_3
      "Uptake of the third component as function of pressure and temperature"
      annotation (Dialog(tab="General", group="Inputs - Functions"));
    input
      SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_p_xT func_p_xT_3
      "Pressure of the third component as function of uptake and temperature"
      annotation (Dialog(tab="General", group="Inputs - Functions"));
    input
      SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_dx_dp func_dx_dp_3
      "Partial derivative of the uptake of the third component w.r.t. the equilibrium 
    pressure"
      annotation (Dialog(tab="General", group="Inputs - Functions"));
    input
      SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_pi_pT func_pi_pT_3
      "Reduced spreading pressure of the third component as function of pressure 
    and temperature"
      annotation (Dialog(tab="General", group="Inputs - Functions"));
    input
      SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_p_piT func_p_piT_3
      "Pressure of the third component as function of reduced spreading pressure
    and temperature"
      annotation (Dialog(tab="General", group="Inputs - Functions"));

    input SorpLib.Media.Functions.SorptionEquilibria.Records.NumericsIAST_PureComponents num_comp_3
      "Record definining numerics of the third component's isotherm model"
      annotation (Dialog(tab="General", group="Inputs - Numerics"),
                  choicesAllMatching=true);

    //
    // Definition of variables
    //
protected
    Modelica.Units.SI.MoleFraction[size(M_i,1)] y_i_
      "Mole fractions of all components in the vapor or gas phase";

  algorithm
    //
    // Solve inverse of IAST
    //
    (p_adsorpt,y_i_,) :=
      SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.IAST_N3.Internals.py_xT_NewtonRaphson(
      x_adsorpt=x_adsorpt,
      T_adsorpt=T_adsorpt,
      M_i=M_i,
      c_1=c_1,
      c_2=c_2,
      c_3=c_3,
      func_x_pT_1=func_x_pT_1,
      func_p_xT_1=func_p_xT_1,
      func_dx_dp_1=func_dx_dp_1,
      func_pi_pT_1=func_pi_pT_1,
      func_p_piT_1=func_p_piT_1,
      func_x_pT_2=func_x_pT_2,
      func_p_xT_2=func_p_xT_2,
      func_dx_dp_2=func_dx_dp_2,
      func_pi_pT_2=func_pi_pT_2,
      func_p_piT_2=func_p_piT_2,
      func_x_pT_3=func_x_pT_3,
      func_p_xT_3=func_p_xT_3,
      func_dx_dp_3=func_dx_dp_3,
      func_pi_pT_3=func_pi_pT_3,
      func_p_piT_3=func_p_piT_3,
      num=num,
      num_comp_1=num_comp_1,
      num_comp_2=num_comp_2,
      num_comp_3=num_comp_3)
      "Standard 'Newton-Raphson' algorithm";

    //
    // Assign correct output values
    //
    y_i := y_i_[1:end-1]
      "Mole fractions of independent components in the gas or vapor phase";

    //
    // Annotations
    //
    annotation (Inline=false,
  InlineAfterIndexReduction=false,
  LateInline=true);
  end py_xT;

  redeclare final function extends dx_dp
    "IAST for three components: Partial derivative of uptakes w.r.t. pressure at constant mole fractions and temperature (numerical solution)"

    //
    // Definition of inputs
    //
    input Real[:] c_3
      "Coefficients of the isotherm model of the third component"
      annotation (Dialog(tab="General", group="Inputs - Components"));

    input
      SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_x_pT func_x_pT_3
      "Uptake of the third component as function of pressure and temperature"
      annotation (Dialog(tab="General", group="Inputs - Functions"));
    input
      SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_p_xT func_p_xT_3
      "Pressure of the third component as function of uptake and temperature"
      annotation (Dialog(tab="General", group="Inputs - Functions"));
    input
      SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_dx_dp func_dx_dp_3
      "Partial derivative of the uptake of the third component w.r.t. the equilibrium 
    pressure"
      annotation (Dialog(tab="General", group="Inputs - Functions"));
    input
      SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_pi_pT func_pi_pT_3
      "Reduced spreading pressure of the third component as function of pressure 
    and temperature"
      annotation (Dialog(tab="General", group="Inputs - Functions"));
    input
      SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_p_piT func_p_piT_3
      "Pressure of the third component as function of reduced spreading pressure
    and temperature"
      annotation (Dialog(tab="General", group="Inputs - Functions"));

    input SorpLib.Media.Functions.SorptionEquilibria.Records.NumericsIAST_PureComponents num_comp_3
      "Record definining numerics of the third component's isotherm model"
      annotation (Dialog(tab="General", group="Inputs - Numerics"),
                  choicesAllMatching=true);

    //
    // Definition of variables
    //
protected
    SorpLib.Units.Uptake[size(M_i,1)] x_adsorpt_pdp
      "Equilibrium uptakes of the adsorpt phase: p + dp";
    SorpLib.Units.Uptake[size(M_i,1)] x_adsorpt_mdp
      "Equilibrium uptakes of the adsorpt phase: p - dp";

  algorithm
    //
    // Select IAST algorithm
    //
    if num.IASTAlgorithm == SorpLib.Choices.IASTAlgorithm.NewtonRaphson then
      (x_adsorpt_pdp,,) :=
        SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.IAST_N3.Internals.x_pyT_NewtonRaphson(
        p_adsorpt=p_adsorpt + dp,
        y_i=y_i_,
        T_adsorpt=T_adsorpt,
        M_i=M_i,
        c_1=c_1,
        c_2=c_2,
        c_3=c_3,
        func_x_pT_1=func_x_pT_1,
        func_p_xT_1=func_p_xT_1,
        func_dx_dp_1=func_dx_dp_1,
        func_pi_pT_1=func_pi_pT_1,
        func_p_piT_1=func_p_piT_1,
        func_x_pT_2=func_x_pT_2,
        func_p_xT_2=func_p_xT_2,
        func_dx_dp_2=func_dx_dp_2,
        func_pi_pT_2=func_pi_pT_2,
        func_p_piT_2=func_p_piT_2,
        func_x_pT_3=func_x_pT_3,
        func_p_xT_3=func_p_xT_3,
        func_dx_dp_3=func_dx_dp_3,
        func_pi_pT_3=func_pi_pT_3,
        func_p_piT_3=func_p_piT_3,
        num=num,
        num_comp_1=num_comp_1,
        num_comp_2=num_comp_2,
        num_comp_3=num_comp_3)
        "Standard 'Newton-Raphson' algorithm";

      (x_adsorpt_mdp,,) :=
        SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.IAST_N3.Internals.x_pyT_NewtonRaphson(
        p_adsorpt=p_adsorpt - dp,
        y_i=y_i_,
        T_adsorpt=T_adsorpt,
        M_i=M_i,
        c_1=c_1,
        c_2=c_2,
        c_3=c_3,
        func_x_pT_1=func_x_pT_1,
        func_p_xT_1=func_p_xT_1,
        func_dx_dp_1=func_dx_dp_1,
        func_pi_pT_1=func_pi_pT_1,
        func_p_piT_1=func_p_piT_1,
        func_x_pT_2=func_x_pT_2,
        func_p_xT_2=func_p_xT_2,
        func_dx_dp_2=func_dx_dp_2,
        func_pi_pT_2=func_pi_pT_2,
        func_p_piT_2=func_p_piT_2,
        func_x_pT_3=func_x_pT_3,
        func_p_xT_3=func_p_xT_3,
        func_dx_dp_3=func_dx_dp_3,
        func_pi_pT_3=func_pi_pT_3,
        func_p_piT_3=func_p_piT_3,
        num=num,
        num_comp_1=num_comp_1,
        num_comp_2=num_comp_2,
        num_comp_3=num_comp_3)
        "Standard 'Newton-Raphson' algorithm";

    elseif num.IASTAlgorithm == SorpLib.Choices.IASTAlgorithm.NestedLoop then
      (x_adsorpt_pdp,,) :=
        SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.IAST_N3.Internals.x_pyT_NestedLoop(
        p_adsorpt=p_adsorpt + dp,
        y_i=y_i_,
        T_adsorpt=T_adsorpt,
        M_i=M_i,
        c_1=c_1,
        c_2=c_2,
        c_3=c_3,
        func_x_pT_1=func_x_pT_1,
        func_p_xT_1=func_p_xT_1,
        func_dx_dp_1=func_dx_dp_1,
        func_pi_pT_1=func_pi_pT_1,
        func_p_piT_1=func_p_piT_1,
        func_x_pT_2=func_x_pT_2,
        func_p_xT_2=func_p_xT_2,
        func_dx_dp_2=func_dx_dp_2,
        func_pi_pT_2=func_pi_pT_2,
        func_p_piT_2=func_p_piT_2,
        func_x_pT_3=func_x_pT_3,
        func_p_xT_3=func_p_xT_3,
        func_dx_dp_3=func_dx_dp_3,
        func_pi_pT_3=func_pi_pT_3,
        func_p_piT_3=func_p_piT_3,
        num=num,
        num_comp_1=num_comp_1,
        num_comp_2=num_comp_2,
        num_comp_3=num_comp_3)
        "'Nested Loop' algorithm";

      (x_adsorpt_mdp,,) :=
        SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.IAST_N3.Internals.x_pyT_NestedLoop(
        p_adsorpt=p_adsorpt - dp,
        y_i=y_i_,
        T_adsorpt=T_adsorpt,
        M_i=M_i,
        c_1=c_1,
        c_2=c_2,
        c_3=c_3,
        func_x_pT_1=func_x_pT_1,
        func_p_xT_1=func_p_xT_1,
        func_dx_dp_1=func_dx_dp_1,
        func_pi_pT_1=func_pi_pT_1,
        func_p_piT_1=func_p_piT_1,
        func_x_pT_2=func_x_pT_2,
        func_p_xT_2=func_p_xT_2,
        func_dx_dp_2=func_dx_dp_2,
        func_pi_pT_2=func_pi_pT_2,
        func_p_piT_2=func_p_piT_2,
        func_x_pT_3=func_x_pT_3,
        func_p_xT_3=func_p_xT_3,
        func_dx_dp_3=func_dx_dp_3,
        func_pi_pT_3=func_pi_pT_3,
        func_p_piT_3=func_p_piT_3,
        num=num,
        num_comp_1=num_comp_1,
        num_comp_2=num_comp_2,
        num_comp_3=num_comp_3)
        "'Nested Loop' algorithm";

    elseif num.IASTAlgorithm == SorpLib.Choices.IASTAlgorithm.FastIAST then
      (x_adsorpt_pdp,,,,) :=
        SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.IAST_N3.Internals.x_pyT_FastIAST(
        p_adsorpt=p_adsorpt + dp,
        y_i=y_i_,
        T_adsorpt=T_adsorpt,
        M_i=M_i,
        c_1=c_1,
        c_2=c_2,
        c_3=c_3,
        func_x_pT_1=func_x_pT_1,
        func_p_xT_1=func_p_xT_1,
        func_dx_dp_1=func_dx_dp_1,
        func_pi_pT_1=func_pi_pT_1,
        func_p_piT_1=func_p_piT_1,
        func_x_pT_2=func_x_pT_2,
        func_p_xT_2=func_p_xT_2,
        func_dx_dp_2=func_dx_dp_2,
        func_pi_pT_2=func_pi_pT_2,
        func_p_piT_2=func_p_piT_2,
        func_x_pT_3=func_x_pT_3,
        func_p_xT_3=func_p_xT_3,
        func_dx_dp_3=func_dx_dp_3,
        func_pi_pT_3=func_pi_pT_3,
        func_p_piT_3=func_p_piT_3,
        num=num,
        num_comp_1=num_comp_1,
        num_comp_2=num_comp_2,
        num_comp_3=num_comp_3)
        "'FASTIast' algorithm";

      (x_adsorpt_mdp,,,,) :=
        SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.IAST_N3.Internals.x_pyT_FastIAST(
        p_adsorpt=p_adsorpt - dp,
        y_i=y_i_,
        T_adsorpt=T_adsorpt,
        M_i=M_i,
        c_1=c_1,
        c_2=c_2,
        c_3=c_3,
        func_x_pT_1=func_x_pT_1,
        func_p_xT_1=func_p_xT_1,
        func_dx_dp_1=func_dx_dp_1,
        func_pi_pT_1=func_pi_pT_1,
        func_p_piT_1=func_p_piT_1,
        func_x_pT_2=func_x_pT_2,
        func_p_xT_2=func_p_xT_2,
        func_dx_dp_2=func_dx_dp_2,
        func_pi_pT_2=func_pi_pT_2,
        func_p_piT_2=func_p_piT_2,
        func_x_pT_3=func_x_pT_3,
        func_p_xT_3=func_p_xT_3,
        func_dx_dp_3=func_dx_dp_3,
        func_pi_pT_3=func_pi_pT_3,
        func_p_piT_3=func_p_piT_3,
        num=num,
        num_comp_1=num_comp_1,
        num_comp_2=num_comp_2,
        num_comp_3=num_comp_3)
        "'FASTIast' algorithm";

    end if;

    //
    // Calculate derivatives of loadings w.r.t. equilibrium pressure
    //
    dx_adsorpt_dp_adsorpt := (x_adsorpt_pdp .- x_adsorpt_mdp) ./ (2*dp)
      "Calculation of the partial derivatives of the equilibrium uptakes w.r.t. the
     equilibrium pressure at constant mole fractions and temperature";
  end dx_dp;

  redeclare final function extends dx_dy
    "IAST for three components: Partial derivative of uptakes w.r.t. mole fractions of independent gas phase components at constant pressure and temperature (numerical solution)"

    //
    // Definition of inputs
    //
    input Real[:] c_3
      "Coefficients of the isotherm model of the third component"
      annotation (Dialog(tab="General", group="Inputs - Components"));

    input
      SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_x_pT func_x_pT_3
      "Uptake of the third component as function of pressure and temperature"
      annotation (Dialog(tab="General", group="Inputs - Functions"));
    input
      SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_p_xT func_p_xT_3
      "Pressure of the third component as function of uptake and temperature"
      annotation (Dialog(tab="General", group="Inputs - Functions"));
    input
      SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_dx_dp func_dx_dp_3
      "Partial derivative of the uptake of the third component w.r.t. the equilibrium 
    pressure"
      annotation (Dialog(tab="General", group="Inputs - Functions"));
    input
      SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_pi_pT func_pi_pT_3
      "Reduced spreading pressure of the third component as function of pressure 
    and temperature"
      annotation (Dialog(tab="General", group="Inputs - Functions"));
    input
      SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_p_piT func_p_piT_3
      "Pressure of the third component as function of reduced spreading pressure
    and temperature"
      annotation (Dialog(tab="General", group="Inputs - Functions"));

    input SorpLib.Media.Functions.SorptionEquilibria.Records.NumericsIAST_PureComponents num_comp_3
      "Record definining numerics of the third component's isotherm model"
      annotation (Dialog(tab="General", group="Inputs - Numerics"),
                  choicesAllMatching=true);

    //
    // Definition of variables
    //
protected
    SorpLib.Units.Uptake[size(M_i,1)] x_adsorpt_pdy
      "Equilibrium uptakes of the adsorpt phase: y + dy";
    SorpLib.Units.Uptake[size(M_i,1)] x_adsorpt_mdy
      "Equilibrium uptakes of the adsorpt phase: y - dy";

  algorithm
    for ind_y_i in 1:size(M_i,1)-1 loop
      //
      // Select IAST algorithm
      //
      if num.IASTAlgorithm == SorpLib.Choices.IASTAlgorithm.NewtonRaphson then
        (x_adsorpt_pdy,,) :=
          SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.IAST_N3.Internals.x_pyT_NewtonRaphson(
          p_adsorpt=p_adsorpt,
          y_i=cat(
            1,
            y_i_[1:ind_y_i - 1],
            {y_i_[ind_y_i] + dy},
            y_i_[ind_y_i + 1:size(M_i, 1)]),
          T_adsorpt=T_adsorpt,
          M_i=M_i,
          c_1=c_1,
          c_2=c_2,
          c_3=c_3,
          func_x_pT_1=func_x_pT_1,
          func_p_xT_1=func_p_xT_1,
          func_dx_dp_1=func_dx_dp_1,
          func_pi_pT_1=func_pi_pT_1,
          func_p_piT_1=func_p_piT_1,
          func_x_pT_2=func_x_pT_2,
          func_p_xT_2=func_p_xT_2,
          func_dx_dp_2=func_dx_dp_2,
          func_pi_pT_2=func_pi_pT_2,
          func_p_piT_2=func_p_piT_2,
          func_x_pT_3=func_x_pT_3,
          func_p_xT_3=func_p_xT_3,
          func_dx_dp_3=func_dx_dp_3,
          func_pi_pT_3=func_pi_pT_3,
          func_p_piT_3=func_p_piT_3,
          num=num,
          num_comp_1=num_comp_1,
          num_comp_2=num_comp_2,
          num_comp_3=num_comp_3)
          "Standard 'Newton-Raphson' algorithm";

        (x_adsorpt_mdy,,) :=
          SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.IAST_N3.Internals.x_pyT_NewtonRaphson(
          p_adsorpt=p_adsorpt,
          y_i=cat(
            1,
            y_i_[1:ind_y_i - 1],
            {y_i_[ind_y_i] - dy},
            y_i_[ind_y_i + 1:size(M_i, 1)]),
          T_adsorpt=T_adsorpt,
          M_i=M_i,
          c_1=c_1,
          c_2=c_2,
          c_3=c_3,
          func_x_pT_1=func_x_pT_1,
          func_p_xT_1=func_p_xT_1,
          func_dx_dp_1=func_dx_dp_1,
          func_pi_pT_1=func_pi_pT_1,
          func_p_piT_1=func_p_piT_1,
          func_x_pT_2=func_x_pT_2,
          func_p_xT_2=func_p_xT_2,
          func_dx_dp_2=func_dx_dp_2,
          func_pi_pT_2=func_pi_pT_2,
          func_p_piT_2=func_p_piT_2,
          func_x_pT_3=func_x_pT_3,
          func_p_xT_3=func_p_xT_3,
          func_dx_dp_3=func_dx_dp_3,
          func_pi_pT_3=func_pi_pT_3,
          func_p_piT_3=func_p_piT_3,
          num=num,
          num_comp_1=num_comp_1,
          num_comp_2=num_comp_2,
          num_comp_3=num_comp_3)
          "Standard 'Newton-Raphson' algorithm";

      elseif num.IASTAlgorithm == SorpLib.Choices.IASTAlgorithm.NestedLoop then
        (x_adsorpt_pdy,,) :=
          SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.IAST_N3.Internals.x_pyT_NestedLoop(
          p_adsorpt=p_adsorpt,
          y_i=cat(
            1,
            y_i_[1:ind_y_i - 1],
            {y_i_[ind_y_i] + dy},
            y_i_[ind_y_i + 1:size(M_i, 1)]),
          T_adsorpt=T_adsorpt,
          M_i=M_i,
          c_1=c_1,
          c_2=c_2,
          c_3=c_3,
          func_x_pT_1=func_x_pT_1,
          func_p_xT_1=func_p_xT_1,
          func_dx_dp_1=func_dx_dp_1,
          func_pi_pT_1=func_pi_pT_1,
          func_p_piT_1=func_p_piT_1,
          func_x_pT_2=func_x_pT_2,
          func_p_xT_2=func_p_xT_2,
          func_dx_dp_2=func_dx_dp_2,
          func_pi_pT_2=func_pi_pT_2,
          func_p_piT_2=func_p_piT_2,
          func_x_pT_3=func_x_pT_3,
          func_p_xT_3=func_p_xT_3,
          func_dx_dp_3=func_dx_dp_3,
          func_pi_pT_3=func_pi_pT_3,
          func_p_piT_3=func_p_piT_3,
          num=num,
          num_comp_1=num_comp_1,
          num_comp_2=num_comp_2,
          num_comp_3=num_comp_3)
          "'Nested Loop' algorithm";

        (x_adsorpt_mdy,,) :=
          SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.IAST_N3.Internals.x_pyT_NestedLoop(
          p_adsorpt=p_adsorpt,
          y_i=cat(
            1,
            y_i_[1:ind_y_i - 1],
            {y_i_[ind_y_i] - dy},
            y_i_[ind_y_i + 1:size(M_i, 1)]),
          T_adsorpt=T_adsorpt,
          M_i=M_i,
          c_1=c_1,
          c_2=c_2,
          c_3=c_3,
          func_x_pT_1=func_x_pT_1,
          func_p_xT_1=func_p_xT_1,
          func_dx_dp_1=func_dx_dp_1,
          func_pi_pT_1=func_pi_pT_1,
          func_p_piT_1=func_p_piT_1,
          func_x_pT_2=func_x_pT_2,
          func_p_xT_2=func_p_xT_2,
          func_dx_dp_2=func_dx_dp_2,
          func_pi_pT_2=func_pi_pT_2,
          func_p_piT_2=func_p_piT_2,
          func_x_pT_3=func_x_pT_3,
          func_p_xT_3=func_p_xT_3,
          func_dx_dp_3=func_dx_dp_3,
          func_pi_pT_3=func_pi_pT_3,
          func_p_piT_3=func_p_piT_3,
          num=num,
          num_comp_1=num_comp_1,
          num_comp_2=num_comp_2,
          num_comp_3=num_comp_3)
          "'Nested Loop' algorithm";

      elseif num.IASTAlgorithm == SorpLib.Choices.IASTAlgorithm.FastIAST then
        (x_adsorpt_pdy,,,,) :=
          SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.IAST_N3.Internals.x_pyT_FastIAST(
          p_adsorpt=p_adsorpt,
          y_i=cat(
            1,
            y_i_[1:ind_y_i - 1],
            {y_i_[ind_y_i] + dy},
            y_i_[ind_y_i + 1:size(M_i, 1)]),
          T_adsorpt=T_adsorpt,
          M_i=M_i,
          c_1=c_1,
          c_2=c_2,
          c_3=c_3,
          func_x_pT_1=func_x_pT_1,
          func_p_xT_1=func_p_xT_1,
          func_dx_dp_1=func_dx_dp_1,
          func_pi_pT_1=func_pi_pT_1,
          func_p_piT_1=func_p_piT_1,
          func_x_pT_2=func_x_pT_2,
          func_p_xT_2=func_p_xT_2,
          func_dx_dp_2=func_dx_dp_2,
          func_pi_pT_2=func_pi_pT_2,
          func_p_piT_2=func_p_piT_2,
          func_x_pT_3=func_x_pT_3,
          func_p_xT_3=func_p_xT_3,
          func_dx_dp_3=func_dx_dp_3,
          func_pi_pT_3=func_pi_pT_3,
          func_p_piT_3=func_p_piT_3,
          num=num,
          num_comp_1=num_comp_1,
          num_comp_2=num_comp_2,
          num_comp_3=num_comp_3)
          "'FASTIast' algorithm";

        (x_adsorpt_mdy,,,,) :=
          SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.IAST_N3.Internals.x_pyT_FastIAST(
          p_adsorpt=p_adsorpt,
          y_i=cat(
            1,
            y_i_[1:ind_y_i - 1],
            {y_i_[ind_y_i] - dy},
            y_i_[ind_y_i + 1:size(M_i, 1)]),
          T_adsorpt=T_adsorpt,
          M_i=M_i,
          c_1=c_1,
          c_2=c_2,
          c_3=c_3,
          func_x_pT_1=func_x_pT_1,
          func_p_xT_1=func_p_xT_1,
          func_dx_dp_1=func_dx_dp_1,
          func_pi_pT_1=func_pi_pT_1,
          func_p_piT_1=func_p_piT_1,
          func_x_pT_2=func_x_pT_2,
          func_p_xT_2=func_p_xT_2,
          func_dx_dp_2=func_dx_dp_2,
          func_pi_pT_2=func_pi_pT_2,
          func_p_piT_2=func_p_piT_2,
          func_x_pT_3=func_x_pT_3,
          func_p_xT_3=func_p_xT_3,
          func_dx_dp_3=func_dx_dp_3,
          func_pi_pT_3=func_pi_pT_3,
          func_p_piT_3=func_p_piT_3,
          num=num,
          num_comp_1=num_comp_1,
          num_comp_2=num_comp_2,
          num_comp_3=num_comp_3)
          "'FASTIast' algorithm";

      end if;

      //
      // Calculate partial darivatives for each independent mole fraction
      //
      dx_adsorpt_dy_i[:,ind_y_i] := (x_adsorpt_pdy .- x_adsorpt_mdy) ./ (2*dy)
        "Partial derivatives of the uptakes w.r.t. the mole fractions of independent 
      gas phase components at constant pressure and temperature";
    end for;
  end dx_dy;

  redeclare final function extends dx_dT
    "IAST for three components: Partial derivative of uptakes w.r.t. temperature at constant pressure and mole fractions (numerical solution)"

    //
    // Definition of inputs
    //
    input Real[:] c_3
      "Coefficients of the isotherm model of the third component"
      annotation (Dialog(tab="General", group="Inputs - Components"));
    input Real[size(c_2,1)] c_pdT_3
      "Coefficients of the isotherm model of the third component: T + dT"
      annotation (Dialog(tab="General", group="Inputs - Components"));
    input Real[size(c_2,1)] c_mdT_3
      "Coefficients of the isotherm model of the third component: T - dT"
      annotation (Dialog(tab="General", group="Inputs - Components"));

    input
      SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_x_pT func_x_pT_3
      "Uptake of the third component as function of pressure and temperature"
      annotation (Dialog(tab="General", group="Inputs - Functions"));
    input
      SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_p_xT func_p_xT_3
      "Pressure of the third component as function of uptake and temperature"
      annotation (Dialog(tab="General", group="Inputs - Functions"));
    input
      SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_dx_dp func_dx_dp_3
      "Partial derivative of the uptake of the third component w.r.t. the equilibrium 
    pressure"
      annotation (Dialog(tab="General", group="Inputs - Functions"));
    input
      SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_pi_pT func_pi_pT_3
      "Reduced spreading pressure of the third component as function of pressure 
    and temperature"
      annotation (Dialog(tab="General", group="Inputs - Functions"));
    input
      SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_p_piT func_p_piT_3
      "Pressure of the third component as function of reduced spreading pressure
    and temperature"
      annotation (Dialog(tab="General", group="Inputs - Functions"));

    input SorpLib.Media.Functions.SorptionEquilibria.Records.NumericsIAST_PureComponents num_comp_3
      "Record definining numerics of the third component's isotherm model"
      annotation (Dialog(tab="General", group="Inputs - Numerics"),
                  choicesAllMatching=true);

    //
    // Definition of variables
    //
protected
    SorpLib.Units.Uptake[size(M_i,1)] x_adsorpt_pdT
      "Equilibrium uptakes of the adsorpt phase: T + dT";
    SorpLib.Units.Uptake[size(M_i,1)] x_adsorpt_mdT
      "Equilibrium uptakes of the adsorpt phase: T - dT";

  algorithm
    //
    // Select IAST algorithm
    //
    if num.IASTAlgorithm == SorpLib.Choices.IASTAlgorithm.NewtonRaphson then
      (x_adsorpt_pdT,,) :=
        SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.IAST_N3.Internals.x_pyT_NewtonRaphson(
        p_adsorpt=p_adsorpt,
        y_i=y_i_,
        T_adsorpt=T_adsorpt + dT,
        M_i=M_i,
        c_1=c_pdT_1,
        c_2=c_pdT_2,
        c_3=c_pdT_3,
        func_x_pT_1=func_x_pT_1,
        func_p_xT_1=func_p_xT_1,
        func_dx_dp_1=func_dx_dp_1,
        func_pi_pT_1=func_pi_pT_1,
        func_p_piT_1=func_p_piT_1,
        func_x_pT_2=func_x_pT_2,
        func_p_xT_2=func_p_xT_2,
        func_dx_dp_2=func_dx_dp_2,
        func_pi_pT_2=func_pi_pT_2,
        func_p_piT_2=func_p_piT_2,
        func_x_pT_3=func_x_pT_3,
        func_p_xT_3=func_p_xT_3,
        func_dx_dp_3=func_dx_dp_3,
        func_pi_pT_3=func_pi_pT_3,
        func_p_piT_3=func_p_piT_3,
        num=num,
        num_comp_1=num_comp_1,
        num_comp_2=num_comp_2,
        num_comp_3=num_comp_3)
        "Standard 'Newton-Raphson' algorithm";

      (x_adsorpt_mdT,,) :=
        SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.IAST_N3.Internals.x_pyT_NewtonRaphson(
        p_adsorpt=p_adsorpt,
        y_i=y_i_,
        T_adsorpt=T_adsorpt - dT,
        M_i=M_i,
        c_1=c_mdT_1,
        c_2=c_mdT_2,
        c_3=c_mdT_3,
        func_x_pT_1=func_x_pT_1,
        func_p_xT_1=func_p_xT_1,
        func_dx_dp_1=func_dx_dp_1,
        func_pi_pT_1=func_pi_pT_1,
        func_p_piT_1=func_p_piT_1,
        func_x_pT_2=func_x_pT_2,
        func_p_xT_2=func_p_xT_2,
        func_dx_dp_2=func_dx_dp_2,
        func_pi_pT_2=func_pi_pT_2,
        func_p_piT_2=func_p_piT_2,
        func_x_pT_3=func_x_pT_3,
        func_p_xT_3=func_p_xT_3,
        func_dx_dp_3=func_dx_dp_3,
        func_pi_pT_3=func_pi_pT_3,
        func_p_piT_3=func_p_piT_3,
        num=num,
        num_comp_1=num_comp_1,
        num_comp_2=num_comp_2,
        num_comp_3=num_comp_3)
        "Standard 'Newton-Raphson' algorithm";

    elseif num.IASTAlgorithm == SorpLib.Choices.IASTAlgorithm.NestedLoop then
      (x_adsorpt_pdT,,) :=
        SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.IAST_N3.Internals.x_pyT_NestedLoop(
        p_adsorpt=p_adsorpt,
        y_i=y_i_,
        T_adsorpt=T_adsorpt + dT,
        M_i=M_i,
        c_1=c_pdT_1,
        c_2=c_pdT_2,
        c_3=c_pdT_3,
        func_x_pT_1=func_x_pT_1,
        func_p_xT_1=func_p_xT_1,
        func_dx_dp_1=func_dx_dp_1,
        func_pi_pT_1=func_pi_pT_1,
        func_p_piT_1=func_p_piT_1,
        func_x_pT_2=func_x_pT_2,
        func_p_xT_2=func_p_xT_2,
        func_dx_dp_2=func_dx_dp_2,
        func_pi_pT_2=func_pi_pT_2,
        func_p_piT_2=func_p_piT_2,
        func_x_pT_3=func_x_pT_3,
        func_p_xT_3=func_p_xT_3,
        func_dx_dp_3=func_dx_dp_3,
        func_pi_pT_3=func_pi_pT_3,
        func_p_piT_3=func_p_piT_3,
        num=num,
        num_comp_1=num_comp_1,
        num_comp_2=num_comp_2,
        num_comp_3=num_comp_3)
        "'Nested Loop' algorithm";

      (x_adsorpt_mdT,,) :=
        SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.IAST_N3.Internals.x_pyT_NestedLoop(
        p_adsorpt=p_adsorpt,
        y_i=y_i_,
        T_adsorpt=T_adsorpt - dT,
        M_i=M_i,
        c_1=c_mdT_1,
        c_2=c_mdT_2,
        c_3=c_mdT_3,
        func_x_pT_1=func_x_pT_1,
        func_p_xT_1=func_p_xT_1,
        func_dx_dp_1=func_dx_dp_1,
        func_pi_pT_1=func_pi_pT_1,
        func_p_piT_1=func_p_piT_1,
        func_x_pT_2=func_x_pT_2,
        func_p_xT_2=func_p_xT_2,
        func_dx_dp_2=func_dx_dp_2,
        func_pi_pT_2=func_pi_pT_2,
        func_p_piT_2=func_p_piT_2,
        func_x_pT_3=func_x_pT_3,
        func_p_xT_3=func_p_xT_3,
        func_dx_dp_3=func_dx_dp_3,
        func_pi_pT_3=func_pi_pT_3,
        func_p_piT_3=func_p_piT_3,
        num=num,
        num_comp_1=num_comp_1,
        num_comp_2=num_comp_2,
        num_comp_3=num_comp_3)
        "'Nested Loop' algorithm";

    elseif num.IASTAlgorithm == SorpLib.Choices.IASTAlgorithm.FastIAST then
      (x_adsorpt_pdT,,,,) :=
        SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.IAST_N3.Internals.x_pyT_FastIAST(
        p_adsorpt=p_adsorpt,
        y_i=y_i_,
        T_adsorpt=T_adsorpt + dT,
        M_i=M_i,
        c_1=c_pdT_1,
        c_2=c_pdT_2,
        c_3=c_pdT_3,
        func_x_pT_1=func_x_pT_1,
        func_p_xT_1=func_p_xT_1,
        func_dx_dp_1=func_dx_dp_1,
        func_pi_pT_1=func_pi_pT_1,
        func_p_piT_1=func_p_piT_1,
        func_x_pT_2=func_x_pT_2,
        func_p_xT_2=func_p_xT_2,
        func_dx_dp_2=func_dx_dp_2,
        func_pi_pT_2=func_pi_pT_2,
        func_p_piT_2=func_p_piT_2,
        func_x_pT_3=func_x_pT_3,
        func_p_xT_3=func_p_xT_3,
        func_dx_dp_3=func_dx_dp_3,
        func_pi_pT_3=func_pi_pT_3,
        func_p_piT_3=func_p_piT_3,
        num=num,
        num_comp_1=num_comp_1,
        num_comp_2=num_comp_2,
        num_comp_3=num_comp_3)
        "'FASTIast' algorithm";

      (x_adsorpt_mdT,,,,) :=
        SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.IAST_N3.Internals.x_pyT_FastIAST(
        p_adsorpt=p_adsorpt,
        y_i=y_i_,
        T_adsorpt=T_adsorpt - dT,
        M_i=M_i,
        c_1=c_mdT_1,
        c_2=c_mdT_2,
        c_3=c_mdT_3,
        func_x_pT_1=func_x_pT_1,
        func_p_xT_1=func_p_xT_1,
        func_dx_dp_1=func_dx_dp_1,
        func_pi_pT_1=func_pi_pT_1,
        func_p_piT_1=func_p_piT_1,
        func_x_pT_2=func_x_pT_2,
        func_p_xT_2=func_p_xT_2,
        func_dx_dp_2=func_dx_dp_2,
        func_pi_pT_2=func_pi_pT_2,
        func_p_piT_2=func_p_piT_2,
        func_x_pT_3=func_x_pT_3,
        func_p_xT_3=func_p_xT_3,
        func_dx_dp_3=func_dx_dp_3,
        func_pi_pT_3=func_pi_pT_3,
        func_p_piT_3=func_p_piT_3,
        num=num,
        num_comp_1=num_comp_1,
        num_comp_2=num_comp_2,
        num_comp_3=num_comp_3)
        "'FASTIast' algorithm";

    end if;

    //
    // Calculate derivatives of loadings w.r.t. equilibrium pressure
    //
    dx_adsorpt_dT_adsorpt := (x_adsorpt_pdT .- x_adsorpt_mdT) ./ (2*dT)
      "Calculation of the partial derivatives of the equilibrium uptakes w.r.t. the
     equilibrium pressure at constant pressure and mole fractions";
  end dx_dT;
  //
  // Annotations
  //
annotation (Documentation(revisions="<html>
<ul>
  <li>
  November 14, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>", info="<html>
<p>
The IAST for three components calculates equilibrium uptakes <i>x_adsorpt</i> as a
function of the equilibrium pressure <i>p_adsorpt</i>, mole fractions of independent 
components in the gas or vapor phase <i>y_i</i>, and the equilibrium temperature 
<i>T_adsorpt</i>. The IAST calculates the multi-component adsorption equilibrium based 
on the pure component isotherm models. For this purpose, a system of equations is 
solved using numerical methods.
</p>

<h4>Main equations</h4>
<p>
The system of equations consists of 2 * <i>n_components</i> + 1 equations (i.e., 5), 
which can be divided into 4 main equation types:
</p>
<ol>
  <li>
  Reduced spreading pressure <i>&pi;</i> that is the same for all components:
  <pre>&pi; = &pi;<sub><i>i</i></sub> = &int;<sub>0</sub><sup>p<sup>*</sup><sub><i>i</i></sub></sup> [x<sub>adsorpt,<i>i</i></sub> / p<sub>adsorpt,<i>i</i></sub> * dp<sub>adsorpt,<i>i</i></sub>].</pre>
  <br>
  </li>
  <li>
  Closing condition of the molar composition of the adsorpt phase <i>z<sub><i>i</i></sub></i>:
  <pre>1 = &sum;<sub><i>i</i></sub> z<sub><i>i</i></sub> = &sum;<sub><i>i</i></sub> [y<sub><i>i</i></sub> * p<sub>adsorpt</sub> / p<sup>*</sup><sub><i>i</i></sub>].</pre>
  <br>
  </li>
  <li>
  Molar uptake of the adsorpt phase of each component <i>i</i>:
  <pre>q<sub>adsorpt,<i>i</i></sub> = z<sub><i>i</i></sub> * q<sub>total</sub>.</pre>
  <br>
  </li>
  <li>
  Total molar uptake of the adsorpt phase:
  <pre>q<sub>total</sub> = (&sum;<sub><i>i</i></sub> [z<sub><i>i</i></sub> / q<sub>adsorpt,<i>i</i></sub>]) ^ (-1).</pre>
  <br>
  </li>
</ol>
<p>
Herein, for each component <i>i</i<>, <i>p<sup>*</sup><sub><i>i</i></sub></i> is the
hypothetical pure component pressure at which the reduced spreading pressure of each
component <i>&pi;<sub><i>i</i></sub></i> is identical (i.e., equilibrium condition).
</p>

<h4>Example</h4>
<p>
The following figure shows the IAST for three components for one parameter set. In the 
upper sub-figure, the equilibrium pressure changes with time. In the centre sub-figure, 
the independent mole fractions change with time. In the lower sub-figure, the 
equilibrium temperature changes with time. The left side shows the uptake of component 
1, the middle shows the uptake of component 2, and the right side shows the uptake of 
component 3. 
<br/><br/>
</p>
<img src=\"Modelica://SorpLib/Resources/doc/media_functions_equilibria_multi_iast_n3.png\" alt=\"media_functions_equilibria_multi_iast_n3.png\">

<h4>References</h4>
<p>
<ul>
  <li>
  Do, D. D. (1998). Adsorption Analysis: Equilibria and Kinetics, 1st Edition, ISBN 978-1-86094-130-6, Imperial College Press.
  </li>
  <li>
  Bathen, D. and Breitbach, M. (2001). Adsorptionstechnik (in German), 1st Edition, ISBN 3-540-41908-X, Springer-Verlag Berlin Heidelberg New York.
  </li>
  <li>
  Mangano E., Friedrich, D., and Brandani, S. (2015). Robust algorithms for the solution of the ideal adsorbed solution theory equations. AIChE Journal, 61(3): 981–991. DOI: 10.1002/aic.14684.
  </li>
</ul>
</p>
</html>"));
end IAST_N3;
