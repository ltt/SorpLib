within SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.IAST_N3.Internals;
function x_pyT_NewtonRaphson
  "IAST for two components: Uptakes as function of pressure, mole fractions of independent gas phase components, and temperature using the standard 'Newton-Raphson' algorithm"
  extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialMultiIAST;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.Pressure p_adsorpt
    "Equilibrium temperature of the adsorpt phase"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.MoleFraction[:] y_i
    "Mole fractions of the components in the gas or vapor phase"
    annotation (Dialog(tab="General", group="Inputs"));

  input Real[:] c_3
    "Coefficients of the isotherm model of the third component"
    annotation (Dialog(tab="General", group="Inputs - Components"));

  input
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_x_pT func_x_pT_3
    "Uptake of the third component as function of pressure and temperature"
    annotation (Dialog(tab="General", group="Inputs - Functions"));
  input
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_p_xT func_p_xT_3
    "Pressure of the third component as function of uptake and temperature"
    annotation (Dialog(tab="General", group="Inputs - Functions"));
  input
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_dx_dp func_dx_dp_3
    "Partial derivative of the uptake of the third component w.r.t. the equilibrium 
    pressure"
    annotation (Dialog(tab="General", group="Inputs - Functions"));
  input
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_pi_pT func_pi_pT_3
    "Reduced spreading pressure of the third component as function of pressure 
    and temperature"
    annotation (Dialog(tab="General", group="Inputs - Functions"));
  input
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_p_piT func_p_piT_3
    "Pressure of the third component as function of reduced spreading pressure
    and temperature"
    annotation (Dialog(tab="General", group="Inputs - Functions"));

  input SorpLib.Media.Functions.SorptionEquilibria.Records.NumericsIAST_PureComponents num_comp_3
    "Record definining numerics of the third component's isotherm model"
    annotation (Dialog(tab="General", group="Inputs - Numerics"),
                choicesAllMatching=true);

  input Boolean flag_startValues = false
    " = true, if start values are given; otherwise, estimate start values"
    annotation (Dialog(tab="General", group="Inputs - Start values"));
  input SorpLib.Units.ReducedSpreadingPressure pi_0 = 1
    "Reduced spreading pressure of all components"
    annotation (Dialog(tab="General", group="Inputs - Start values",
                enable=flag_startValues));

  //
  // Definition of outputs
  //
  output SorpLib.Units.Uptake[size(M_i,1)] x_adsorpt
    "Equilibrium uptakes of the adsorpt phase"
    annotation (Dialog(tab="General", group="Outputs", enable=false));
  output Modelica.Units.SI.Pressure[size(M_i,1)] p_i_pure
    "Hypothetical pure component pressures"
    annotation (Dialog(tab="General", group="Outputs", enable=false));
  output SorpLib.Units.ReducedSpreadingPressure pi
    "Reduced spreading pressure of all components"
    annotation (Dialog(tab="General", group="Outputs", enable=false));

  //
  // Definition of variables
  //
protected
  Real[size(M_i,1)] K_i(each unit="mol/(kg.Pa)")
    "Henry constants of the components";
  Real K_avg(unit="mol/(kg.Pa)")
    "Average Henry constant";

  SorpLib.Units.ReducedSpreadingPressure[size(M_i,1)] pi_i
    "Reduced spreading pressures";

  Integer no_iteration = 1
    "Counter of loop";
  Real error = 1
    "Error of loop";

  SorpLib.Units.MolarUptake[size(M_i,1)] q_adsorpt_i
    "Molar equilibrium uptakes at hypothetical pure component pressures";

  Modelica.Units.SI.MoleFraction delta_sum_z_i
    "Satisfaction of molar composition of adsorpt phase";
  Real ddelta_sum_z_i_dpi(unit="kg/mol")
    "Partial derivative of satisfaction of molar composition of adsorpt phase
    w.r.t. to reduced spreading pressure of all components";

  Modelica.Units.SI.MoleFraction[size(M_i,1)] z_i
    "Mole fractions of the adsorpt phase";

  SorpLib.Units.MolarUptake q_adsorpt
    "Total molar equilibrium uptake";

algorithm
  if flag_startValues then
    //
    // Use start values
    //
    pi := pi_0
      "Reduced spreading pressure of all components";

  else
    //
    // Caclulate initial guesses ensuring convergence: Henry constants
    //
    K_i[1] := 1/M_i[1] * func_dx_dp_1(p_adsorpt=num.p_K_0,
      T_adsorpt=T_adsorpt,
      c=c_1)
      "Henry constant of the first component";
    K_i[2] := 1/M_i[2] * func_dx_dp_2(p_adsorpt=num.p_K_0,
      T_adsorpt=T_adsorpt,
      c=c_2)
      "Henry constant of the second component";
    K_i[3] := 1/M_i[3] * func_dx_dp_3(p_adsorpt=num.p_K_0,
      T_adsorpt=T_adsorpt,
      c=c_3)
      "Henry constant of the third component";

    K_avg := sum(y_i .* K_i)
      "Average Henry constant";

    //
    // Caclulate initial guesses ensuring convergence: Reduced spreading pressures
    // and hypothetical pure component pressures
    //
    p_i_pure := p_adsorpt .* K_avg ./ K_i
      "Hypothetical pure component pressures";

    pi_i[1] := func_pi_pT_1(M_adsorptive=M_i[1],
      p_adsorpt=p_i_pure[1],
      T_adsorpt=T_adsorpt,
      c=c_1,
      integral_pi_lb=num_comp_1.integral_pi_lb,
      tolerance=num_comp_1.tolerance_pi)
      "Reduced spreading pressure of the first component";
    pi_i[2] := func_pi_pT_2(M_adsorptive=M_i[2],
      p_adsorpt=p_i_pure[2],
      T_adsorpt=T_adsorpt,
      c=c_2,
      integral_pi_lb=num_comp_2.integral_pi_lb,
      tolerance=num_comp_2.tolerance_pi)
      "Reduced spreading pressure of the second component";
    pi_i[3] := func_pi_pT_3(M_adsorptive=M_i[3],
      p_adsorpt=p_i_pure[3],
      T_adsorpt=T_adsorpt,
      c=c_3,
      integral_pi_lb=num_comp_3.integral_pi_lb,
      tolerance=num_comp_3.tolerance_pi)
      "Reduced spreading pressure of the third component";

    pi := min(pi_i)
      "Reduced spreading pressure of all components";

  end if;

  //
  // Loop to solve the IAST
  //
  while error >= num.tolerance and no_iteration <= num.no_max loop
    //
    // Calculate hypothetical pure component pressures
    //
    p_i_pure[1] := func_p_piT_1(
      M_adsorptive=M_i[1],
      pi=pi,
      T_adsorpt=T_adsorpt,
      c=c_1,
      p_adsorpt_lb_start=num_comp_1.p_adsorpt_lb_start,
      p_adsorpt_ub_start=num_comp_1.p_adsorpt_ub_start,
      integral_pi_lb=num_comp_1.integral_pi_lb,
      tolerance_p_adsorpt=num_comp_1.tolerance_p_adsorpt,
      tolerance_pi=num_comp_1.tolerance_pi)
      "Hypothetical pure component pressure of the first component";
    p_i_pure[2] := func_p_piT_2(
      M_adsorptive=M_i[2],
      pi=pi,
      T_adsorpt=T_adsorpt,
      c=c_2,
      p_adsorpt_lb_start=num_comp_2.p_adsorpt_lb_start,
      p_adsorpt_ub_start=num_comp_2.p_adsorpt_ub_start,
      integral_pi_lb=num_comp_2.integral_pi_lb,
      tolerance_p_adsorpt=num_comp_2.tolerance_p_adsorpt,
      tolerance_pi=num_comp_2.tolerance_pi)
      "Hypothetical pure component pressure of the first component";
    p_i_pure[3] := func_p_piT_3(
      M_adsorptive=M_i[3],
      pi=pi,
      T_adsorpt=T_adsorpt,
      c=c_3,
      p_adsorpt_lb_start=num_comp_3.p_adsorpt_lb_start,
      p_adsorpt_ub_start=num_comp_3.p_adsorpt_ub_start,
      integral_pi_lb=num_comp_3.integral_pi_lb,
      tolerance_p_adsorpt=num_comp_3.tolerance_p_adsorpt,
      tolerance_pi=num_comp_3.tolerance_pi)
      "Hypothetical pure component pressure of the third component";

    //
    // Calculate molar uptakes
    //
    q_adsorpt_i[1] := 1/M_i[1] * func_x_pT_1(p_adsorpt=p_i_pure[1],
      T_adsorpt=T_adsorpt,
      c=c_1,
      p_adsorpt_lb_start=num_comp_1.p_adsorpt_lb_start,
      p_adsorpt_ub_start=num_comp_1.p_adsorpt_ub_start,
      tolerance=num_comp_1.tolerance_p_adsorpt)
      "Molar equilibrium uptake at hypothetical pure component pressure of the 
      first component";
    q_adsorpt_i[2] := 1/M_i[2] * func_x_pT_2(p_adsorpt=p_i_pure[2],
      T_adsorpt=T_adsorpt,
      c=c_2,
      p_adsorpt_lb_start=num_comp_2.p_adsorpt_lb_start,
      p_adsorpt_ub_start=num_comp_2.p_adsorpt_ub_start,
      tolerance=num_comp_2.tolerance_p_adsorpt)
      "Molar equilibrium uptake at hypothetical pure component pressure of the 
      second component";
    q_adsorpt_i[3] := 1/M_i[3] * func_x_pT_3(p_adsorpt=p_i_pure[3],
      T_adsorpt=T_adsorpt,
      c=c_3,
      p_adsorpt_lb_start=num_comp_3.p_adsorpt_lb_start,
      p_adsorpt_ub_start=num_comp_3.p_adsorpt_ub_start,
      tolerance=num_comp_3.tolerance_p_adsorpt)
      "Molar equilibrium uptake at hypothetical pure component pressure of the 
      third component";

    //
    // Apply Newton-Raphson method
    //
    delta_sum_z_i := sum((p_adsorpt .* y_i) ./ p_i_pure) - 1
      "Satisfaction of molar composition of adsorpt phase";
    ddelta_sum_z_i_dpi := -sum((p_adsorpt .* y_i) ./ (p_i_pure .* q_adsorpt_i))
      "Partial derivative of satisfaction of molar composition of adsorpt phase
      w.r.t. to reduced spreading pressure of all components";

    pi := if pi - delta_sum_z_i / ddelta_sum_z_i_dpi > 0 then
      pi - delta_sum_z_i / ddelta_sum_z_i_dpi else pi / 2
      "Update reduced spreading pressure of all components";

    //
    // Check for convergence
    //
    no_iteration := no_iteration + 1
      "Counter of loop";
    error := abs(delta_sum_z_i)
      "Error of loop";

  end while;

  //
  // Calculate mole fractions of adsorpt phase
  //
  z_i := y_i .* p_adsorpt ./ p_i_pure
    "Mole fractions of the adsorpt phase";

  //
  // Calculate equilibrium uptake
  //
  q_adsorpt := 1 / sum(z_i ./ q_adsorpt_i)
  "Total molar equilibrium uptake";

  //
  // Calculate final ouputs
  //
  x_adsorpt := q_adsorpt .* z_i .* M_i
    "Equilibrium uptakes of the adsorpt phase";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function provides an algorithm for solving the IAST for three components. The 
uptakes <i>x_adsorpt</i> are calculated as a function of the pressure <i>p_adsorpt</i>, 
the molar composition of the gas/vapour phase <i>y_i</i>, and the temperature 
<i>T_adsorpt</i>. The algorithm uses a Newton-Raphson method to solve the system 
of equations of the IAST. 
</p>

<h4>Main equations</h4>
<p>
The algorithm consists of 14 steps:
</p>
<ol>
  <li>
  Calculate the Henry's constant of each component <i>i</i>:
  <pre>K<sub><i>i</i></sub> = 1 / M<sub><i>i</i></sub> * dx<sub><i>i</i></sub>/dp<sub><i>i</i></sub>,</pre>
  where <i>M<sub><i>i</i></sub></i> is the molar mass and <i>dx<sub><i>i</i></sub>/dp<sub><i>i</i></sub></i> 
  is the partial derivative of the uptake with respect to the pressure.
  <br>
  </li>
  <li>
  Calculate the average Henry's constant:
  <pre>K<sub>avg</sub> = &sum;<sub><i>i</i></sub> y<sub><i>i</i></sub> * K<sub><i>i</i></sub>.</pre>
  <br>
  </li>
  <li>
  Calculate the hypothetical pure component pressures of each component <i>i</i>:
  <pre>p<sup>*</sup><sub><i>i</i></sub> = p<sub>adsorpt</sub> * K<sub>avg</sub> / K<sub><i>i</i></sub>.</pre>
  <br>
  </li>
  <li>
  Calculate the reduced spreading pressures of each component <i>i</i>:
  <pre>&pi;<sub><i>i</i></sub> = f(M<sub><i>i</i></sub>, p<sup>*</sup><sub><i>i</i></sub>, T<sub>adsorpt</sub>).</pre>
  <br>
  </li>
  <li>
  Calculate the initial guess of the reduced spreading pressure:
  <pre>&pi; = <strong>min</strong>(&pi;<sub><i>i</i></sub>).</pre>
  <br>
  </li>
  <li>
  Calculate the hypothetical pure component pressures of each component <i>i</i>:
  <pre>p<sup>*</sup><sub><i>i</i></sub> = f(M<sub><i>i</i></sub>, &pi;, T<sub>adsorpt</sub>).</pre>
  This formula is an inverse of the reduced spreading pressure, which often
  has to be calculated numerically.
  <br>
  </li>
  <li>
  Calculate the molar uptake of each component <i>i</i>:
  <pre>q<sub>adsorpt,<i>i</i></sub> = 1 / M<sub><i>i</i></sub> * x<sub>adsorpt,<i>i</i></sub>(p<sup>*</sup><sub><i>i</i></sub>, T<sub>adsorpt</sub>).</pre>
  <br>
  </li>
  <li>
  Calculate the satisfaction of the molar composition of the adsorpt phase:
  <pre>F = &sum;<sub><i>i</i></sub> [y<sub><i>i</i></sub> * x<sub>adsorpt</sub> / p<sup>*</sup><sub><i>i</i></sub>] - 1.</pre>
  <br>
  </li>
  <li>
  Calculate the partial derivative of the satisfaction of the adsorpt phase
  with respect to the reduced spreading pressure:
  <pre>dF/d&pi; = -&sum;<sub><i>i</i></sub> [y<sub><i>i</i></sub> * p<sub>adsorpt</sub> / (p<sup>*</sup><sub><i>i</i></sub> * q<sub>adsorpt,<i>i</i></sub>)].</pre>
  <br>
  </li>
  <li>
  Calculate the reduced spreading pressure of the next iteration step:
  <pre>&pi;<sub>next</sub> = <strong>if</strong> &pi; - F / dF/d&pi; > 0 <strong>then</strong> &pi; - F / dF/d&pi; > 0 <strong>else</strong> &pi;/2.</pre>
  <br>
  </li>
  <li>
  Check for convergence:
  <pre>|F| &le; tolerance.</pre>
  If the convergence criterion is not fulfilled, got to step 6. It the convergence
  criterion is fulfilled, got to step 12.
  <br>
  </li>
  <li>
  Calculate the molar composition of each component <i>i</i> in the adsorpt
  phase:
  <pre>z<sub><i>i</i></sub> = y<sub><i>i</i></sub> * p<sub>adsorpt</sub> / p<sup>*</sup><sub><i>i</i></sub>.</pre>
  <br>
  </li>
  <li>
  Calculate the total molar uptake:
  <pre>q<sub>adsorpt,total</sub> = 1 / &sum;<sub><i>i</i></sub> [z<sub><i>i</i></sub> / q<sub>adsorpt,<i>i</i></sub>].</pre>
  <br>
  </li>
  <li>
  Calculate the uptakes of each component <i>i</i>:
  <pre>x<sub>adsorpt,<i>i</i></sub> = M<sub><i>i</i></sub> * z<sub><i>i</i></sub> * q<sub>adsorpt,total</sub>.</pre>
  </li>
</ol>

<h4>References</h4>
<ul>
  <li>
  Do, D. D. (1998). Adsorption Analysis: Equilibria and Kinetics, 1st Edition, ISBN 978-1-86094-130-6, Imperial College Press.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  November 14, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end x_pyT_NewtonRaphson;
