﻿within SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.IAST_N3.Internals;
function x_pyT_FastIAST
  "IAST for three components: Uptakes as function of pressure, mole fractions of independent gas phase components, and temperature using the 'FastIAST' algorithm"
  extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialMultiIAST;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.Pressure p_adsorpt
    "Equilibrium temperature of the adsorpt phase"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.MoleFraction[:] y_i
    "Mole fractions of the components in the gas or vapor phase"
    annotation (Dialog(tab="General", group="Inputs"));

  input Real[:] c_3
    "Coefficients of the isotherm model of the third component"
    annotation (Dialog(tab="General", group="Inputs - Components"));

  input
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_x_pT func_x_pT_3
    "Uptake of the third component as function of pressure and temperature"
    annotation (Dialog(tab="General", group="Inputs - Functions"));
  input
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_p_xT func_p_xT_3
    "Pressure of the third component as function of uptake and temperature"
    annotation (Dialog(tab="General", group="Inputs - Functions"));
  input
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_dx_dp func_dx_dp_3
    "Partial derivative of the uptake of the third component w.r.t. the equilibrium 
    pressure"
    annotation (Dialog(tab="General", group="Inputs - Functions"));
  input
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_pi_pT func_pi_pT_3
    "Reduced spreading pressure of the third component as function of pressure 
    and temperature"
    annotation (Dialog(tab="General", group="Inputs - Functions"));
  input
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_p_piT func_p_piT_3
    "Pressure of the third component as function of reduced spreading pressure
    and temperature"
    annotation (Dialog(tab="General", group="Inputs - Functions"));

  input SorpLib.Media.Functions.SorptionEquilibria.Records.NumericsIAST_PureComponents num_comp_3
    "Record definining numerics of the third component's isotherm model"
    annotation (Dialog(tab="General", group="Inputs - Numerics"),
                choicesAllMatching=true);

  input Boolean flag_startValues = false
    " = true, if start values are given; otherwise, estimate start values"
    annotation (Dialog(tab="General", group="Inputs - Start values"));
  input Real[size(M_i,1)] Kp_i_0(each unit="1/Pa") = fill(1, size(M_i,1))
    "Auxiliary variable for reduced pressues of the components"
    annotation (Dialog(tab="General", group="Inputs - Start values",
                enable=flag_startValues));
  input Real[size(M_i,1)] eta_i_0(each unit="1") = fill(1, size(M_i,1))
    "Reduced pressures of the components"
    annotation (Dialog(tab="General", group="Inputs - Start values",
                enable=flag_startValues));

  //
  // Definition of outputs
  //
  output SorpLib.Units.Uptake[size(M_i,1)] x_adsorpt
    "Equilibrium uptakes of the adsorpt phase"
    annotation (Dialog(tab="General", group="Outputs", enable=false));
  output Modelica.Units.SI.Pressure[size(M_i,1)] p_i_pure
    "Hypothetical pure component pressures"
    annotation (Dialog(tab="General", group="Outputs", enable=false));
  output SorpLib.Units.ReducedSpreadingPressure pi
    "Reduced spreading pressure of all components"
    annotation (Dialog(tab="General", group="Outputs", enable=false));
  output Real[size(M_i,1)] Kp_i(each unit="1/Pa")
    "Auxiliary variable for reduced pressues of the components"
    annotation (Dialog(tab="General", group="Outputs", enable=false));
  output Real[size(M_i,1)] eta_i(each unit="1")
    "Reduced pressures of the components"
    annotation (Dialog(tab="General", group="Outputs", enable=false));

  //
  // Definition of variables
  //
protected
  Real[size(M_i,1)] K_i(each unit="mol/(kg.Pa)")
    "Henry constants of the components";
  Real K_avg(unit="mol/(kg.Pa)")
    "Average Henry constant";

  SorpLib.Units.MolarUptake[size(M_i,1)] q_adsorpt_sat
    "Saturation uptakes of the components";

  Integer no_iteration = 1
    "Counter of loop";
  Real error = 1
    "Error of loop";

  Real[size(M_i,1)] Jac_last_row
    "Jacobian matrix: Elements of the last row";
  Real[size(M_i,1)] Jac_diagonal
    "Jacobian matrix: Elements of the diagonal";

  SorpLib.Units.ReducedSpreadingPressure[size(M_i,1)] pi_i
    "Reduced spreading pressures";
  SorpLib.Units.ReducedSpreadingPressure[size(M_i,1)] delta_pi_i
    "Difference between reduced spreading pressures and reduced spreading pressure
    of last component";

  Real[size(M_i,1)] delta_eta_i(each unit="1")
    "Newton steps for reduced pressures of the components";

  Modelica.Units.SI.MoleFraction[size(M_i,1)] z_i
    "Mole fractions of the adsorpt phase";

  SorpLib.Units.MolarUptake[size(M_i,1)] q_adsorpt_i
    "Molar equilibrium uptakes at hypothetical pure component pressures";
  SorpLib.Units.MolarUptake q_adsorpt
    "Total molar equilibrium uptake";

algorithm
  if flag_startValues then
    //
    // Use start values
    //
    Kp_i := Kp_i_0
      "Auxiliary variable for reduced pressues of the components";

    eta_i := eta_i_0
      "Reduced pressures of the components";

  else
    //
    // Caclulate initial guesses ensuring convergence: Henry constants
    //
    K_i[1] := 1/M_i[1] * func_dx_dp_1(p_adsorpt=num.p_K_0,
      T_adsorpt=T_adsorpt,
      c=c_1)
      "Henry constant of the first component";
    K_i[2] := 1/M_i[2] * func_dx_dp_2(p_adsorpt=num.p_K_0,
      T_adsorpt=T_adsorpt,
      c=c_2)
      "Henry constant of the second component";
    K_i[3] := 1/M_i[3] * func_dx_dp_3(p_adsorpt=num.p_K_0,
      T_adsorpt=T_adsorpt,
      c=c_3)
      "Henry constant of the third component";

    K_avg := sum(y_i .* K_i)
      "Average Henry constant";

    //
    // Calculate initial guesses ensuring convergence: Saturation uptakes
    //
    q_adsorpt_sat[1] := 1/M_i[1] * func_x_pT_1(
      p_adsorpt=num.p_sat_0,
      T_adsorpt=T_adsorpt,
      c=c_1,
      p_adsorpt_lb_start=num_comp_1.p_adsorpt_lb_start,
      p_adsorpt_ub_start=num_comp_1.p_adsorpt_ub_start,
      tolerance=num_comp_1.tolerance_p_adsorpt)
      "Saturation uptake of the first component";
    q_adsorpt_sat[2] := 1/M_i[2] * func_x_pT_2(
      p_adsorpt=num.p_sat_0,
      T_adsorpt=T_adsorpt,
      c=c_2,
      p_adsorpt_lb_start=num_comp_2.p_adsorpt_lb_start,
      p_adsorpt_ub_start=num_comp_2.p_adsorpt_ub_start,
      tolerance=num_comp_2.tolerance_p_adsorpt)
      "Saturation uptake of the second component";
    q_adsorpt_sat[3] := 1/M_i[3] * func_x_pT_3(
      p_adsorpt=num.p_sat_0,
      T_adsorpt=T_adsorpt,
      c=c_3,
      p_adsorpt_lb_start=num_comp_3.p_adsorpt_lb_start,
      p_adsorpt_ub_start=num_comp_3.p_adsorpt_ub_start,
      tolerance=num_comp_3.tolerance_p_adsorpt)
      "Saturation uptake of the third component";

    //
    // Calculate initial guesses ensuring convergence: Reduced pressures
    //
    Kp_i := K_i ./ q_adsorpt_sat
      "Auxiliary variable for reduced pressues of the components";

    eta_i := {min(Kp_i[i] / K_i[i] * p_adsorpt * K_avg, p_adsorpt * Kp_i[i])
      for i in 1:size(M_i,1)}
      "Reduced pressures of the components";

  end if;

  //
  // Loop to solve the IAST
  //
  while error >= num.tolerance and no_iteration <= num.no_max loop
    //
    // Calculate the Jacobian matrix: Last row
    //
    Jac_last_row := Kp_i .* p_adsorpt .* y_i ./ eta_i.^2
      "Jacobian matrix: Elements of the last row";

    //
    // Calculate hypothetical pure component pressures
    //
    p_i_pure := eta_i ./ Kp_i
      "Hypothetical pure component pressures";

    //
    // Calculate the Jacobian matrix: Diagonal
    //
    Jac_diagonal[1] := 1 / (M_i[1] * eta_i[1]) * func_x_pT_1(
      p_adsorpt=p_i_pure[1],
      T_adsorpt=T_adsorpt,
      c=c_1,
      p_adsorpt_lb_start=num_comp_1.p_adsorpt_lb_start,
      p_adsorpt_ub_start=num_comp_1.p_adsorpt_ub_start,
      tolerance=num_comp_1.tolerance_p_adsorpt)
      "Diagonale of the Jacobian matrix: Component 1";
    Jac_diagonal[2] := 1 / (M_i[2] * eta_i[2]) * func_x_pT_2(
      p_adsorpt=p_i_pure[2],
      T_adsorpt=T_adsorpt,
      c=c_2,
      p_adsorpt_lb_start=num_comp_2.p_adsorpt_lb_start,
      p_adsorpt_ub_start=num_comp_2.p_adsorpt_ub_start,
      tolerance=num_comp_2.tolerance_p_adsorpt)
      "Diagonale of the Jacobian matrix: Component 2";
    Jac_diagonal[3] := 1 / (M_i[3] * eta_i[3]) * func_x_pT_3(
      p_adsorpt=p_i_pure[3],
      T_adsorpt=T_adsorpt,
      c=c_3,
      p_adsorpt_lb_start=num_comp_3.p_adsorpt_lb_start,
      p_adsorpt_ub_start=num_comp_3.p_adsorpt_ub_start,
      tolerance=num_comp_3.tolerance_p_adsorpt)
      "Diagonale of the Jacobian matrix: Component 3";

    //
    // Calculate the Jacobian matrix: Correct last element of the diagonal
    //
    Jac_diagonal[end] := Jac_last_row[end] + Jac_diagonal[end] *
      sum(Jac_last_row[1:end-1] ./ Jac_diagonal[1:end-1])
      "Diagonale of the Jacobian matrix: Last element of the diagonal";

    //
    // Calculate recuded spreading pressures
    //
    pi_i[1] := func_pi_pT_1(M_adsorptive=M_i[1],
      p_adsorpt=p_i_pure[1],
      T_adsorpt=T_adsorpt,
      c=c_1,
      integral_pi_lb=num_comp_1.integral_pi_lb,
      tolerance=num_comp_1.tolerance_pi)
      "Reduced spreading pressure of the first component";
    pi_i[2] := func_pi_pT_2(M_adsorptive=M_i[2],
      p_adsorpt=p_i_pure[2],
      T_adsorpt=T_adsorpt,
      c=c_2,
      integral_pi_lb=num_comp_2.integral_pi_lb,
      tolerance=num_comp_2.tolerance_pi)
      "Reduced spreading pressure of the second component";
    pi_i[3] := func_pi_pT_3(M_adsorptive=M_i[3],
      p_adsorpt=p_i_pure[3],
      T_adsorpt=T_adsorpt,
      c=c_3,
      integral_pi_lb=num_comp_3.integral_pi_lb,
      tolerance=num_comp_3.tolerance_pi)
      "Reduced spreading pressure of the third component";

    //
    // Calculate residuals
    //
    delta_pi_i := pi_i .- pi_i[end]
      "Difference between reduced spreading pressures and reduced spreading pressure
      of last component";
    delta_pi_i[end] := 1 - sum(Kp_i .* p_adsorpt .* y_i ./ eta_i) -
      sum(Jac_last_row[1:end-1] ./ Jac_diagonal[1:end-1] .* delta_pi_i[1:end-1])
      "Difference between reduced spreading pressures and reduced spreading pressure
      of last component: Last element";

    //
    // Apply Newton-Raphson method
    //
    delta_eta_i[end] := -delta_pi_i[end] / Jac_diagonal[end]
      "Newton steps for reduced pressures of the components: Last component";
    delta_eta_i[1:end-1] := (-delta_pi_i[1:end-1] .+ Jac_diagonal[end] *
      delta_eta_i[end]) ./ Jac_diagonal[1:end-1]
      "Newton steps for reduced pressures of the components: Remaining components";

    for i in 1:size(M_i,1) loop
      if eta_i[i] + delta_eta_i[i] < 0 then
        eta_i[i] := eta_i[i] / 2
          "Update reduced pressure";

      else
        eta_i[i] := eta_i[i] + delta_eta_i[i]
          "Update reduced pressure";

      end if;
    end for;

    //
    // Check for convergence
    //
    no_iteration := no_iteration + 1
      "Counter of loop";
    error := sum(abs(delta_eta_i))
      "Error of loop";

  end while;

  //
  // Return final results
  //
  if error >= num.tolerance or no_iteration > num.no_max or max(p_i_pure) >= 1e30 then
    //
    // Calculation failed: Use 'Nested Loop' algorithm as fallback solution
    //
    (x_adsorpt,p_i_pure,pi) :=
      SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.IAST_N3.Internals.x_pyT_NestedLoop(
      p_adsorpt=p_adsorpt,
      y_i=y_i,
      T_adsorpt=T_adsorpt,
      M_i=M_i,
      c_1=c_1,
      c_2=c_2,
      c_3=c_3,
      func_x_pT_1=func_x_pT_1,
      func_p_xT_1=func_p_xT_1,
      func_dx_dp_1=func_dx_dp_1,
      func_pi_pT_1=func_pi_pT_1,
      func_p_piT_1=func_p_piT_1,
      func_x_pT_2=func_x_pT_2,
      func_p_xT_2=func_p_xT_2,
      func_dx_dp_2=func_dx_dp_2,
      func_pi_pT_2=func_pi_pT_2,
      func_p_piT_2=func_p_piT_2,
      func_x_pT_3=func_x_pT_3,
      func_p_xT_3=func_p_xT_3,
      func_dx_dp_3=func_dx_dp_3,
      func_pi_pT_3=func_pi_pT_3,
      func_p_piT_3=func_p_piT_3,
      num=num,
      num_comp_1=num_comp_1,
      num_comp_2=num_comp_2,
      num_comp_3=num_comp_3)
      "'Nested Loop' algorithm to solve IAST";

    else
    //
    // Calculate mole fractions of adsorpt phase
    //
    z_i := y_i .* p_adsorpt ./ p_i_pure
      "Mole fractions of the adsorpt phase";

    //
    // Calculate equilibrium uptakes
    //
    q_adsorpt_i[1] := 1/M_i[1] * func_x_pT_1(p_adsorpt=p_i_pure[1],
      T_adsorpt=T_adsorpt,
      c=c_1,
      p_adsorpt_lb_start=num_comp_1.p_adsorpt_lb_start,
      p_adsorpt_ub_start=num_comp_1.p_adsorpt_ub_start,
      tolerance=num_comp_1.tolerance_p_adsorpt)
      "Molar equilibrium uptake at hypothetical pure component pressure of the 
      first component";
    q_adsorpt_i[2] := 1/M_i[2] * func_x_pT_2(p_adsorpt=p_i_pure[2],
      T_adsorpt=T_adsorpt,
      c=c_2,
      p_adsorpt_lb_start=num_comp_2.p_adsorpt_lb_start,
      p_adsorpt_ub_start=num_comp_2.p_adsorpt_ub_start,
      tolerance=num_comp_2.tolerance_p_adsorpt)
    "Molar equilibrium uptake at hypothetical pure component pressure of the 
    second component";
    q_adsorpt_i[3] := 1/M_i[3] * func_x_pT_3(p_adsorpt=p_i_pure[3],
      T_adsorpt=T_adsorpt,
      c=c_3,
      p_adsorpt_lb_start=num_comp_3.p_adsorpt_lb_start,
      p_adsorpt_ub_start=num_comp_3.p_adsorpt_ub_start,
      tolerance=num_comp_3.tolerance_p_adsorpt)
    "Molar equilibrium uptake at hypothetical pure component pressure of the 
    second component";

    q_adsorpt := 1 / sum(z_i ./ q_adsorpt_i)
    "Total molar equilibrium uptake";

    //
    // Calculate final ouputs
    //
    x_adsorpt := q_adsorpt .* z_i .* M_i
      "Equilibrium uptakes of the adsorpt phase";

    pi := pi_i[end]
      "Reduced spreading pressure of all components";

  end if;

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function provides an algorithm for solving the IAST for three components. The 
uptakes <i>x_adsorpt</i> are calculated as a function of the pressure <i>p_adsorpt</i>, 
the molar composition of the gas/vapour phase <i>y_i</i>, and the temperature 
<i>T_adsorpt</i>. The algorithm uses one loops to iterate simultaneously over the 
reduced spreading pressure <i>&pi;</i> and the molar composition of the adsorpt phase
<i>z<sub><I>i</i></sub></i>. The algorithm exploits that all relevant information can 
be written into a Jacobian matrix in which all elements are zero except the last row, 
the last column, and the diagonal. For the calculation, only the information of the 
last row and diagonal is required, which reduces the computational effort.
</p>

<h4>Main equations</h4>
<p>
The algorithm consists of 20 steps:
</p>
<ol>
  <li>
  Calculate the Henry's constant of each component <i>i</i>:
  <pre>K<sub><i>i</i></sub> = 1 / M<sub><i>i</i></sub> * dx<sub><i>i</i></sub>/dp<sub><i>i</i></sub>,</pre>
  where <i>M<sub><i>i</i></sub></i> is the molar mass and <i>dx<sub><i>i</i></sub>/dp<sub><i>i</i></sub></i> 
  is the partial derivative of the uptake with respect to the pressure.
  <br>
  </li>
  <li>
  Calculate the average Henry's constant:
  <pre>K<sub>avg</sub> = &sum;<sub><i>i</i></sub> y<sub><i>i</i></sub> * K<sub><i>i</i></sub>.</pre>
  <br>
  </li>
  <li>
  Calculate the molar saturation uptake of each component <i>i</i>:
  <pre>q<sub>adsorpt,sat,<i>i</i></sub> = 1 / M<sub><i>i</i></sub> * x<sub>adsorpt,<i>i</i></sub>(p &rarr; &infin;, T<sub>adsorpt</sub>).</pre>
  <br>
  </li>
  <li>
  Calculate auxiliary variable of each component <i>i</i>:
  <pre>Kp<sub><i>i</i></sub> = K<sub><i>i</i></sub> / q<sub>adsorpt,sat,<i>i</i></sub>.</pre>
  <br>
  </li>
  <li>
  Calculate the initial guess of the reduced pressure of each component <i>i</i>:
  <pre>&eta;<sub><i>i</i></sub> = <strong>min</strong>(Kp<sub><i>i</i></sub> / K<sub><i>i</i></sub> * p<sub>adsorpt</sub> * K<sub>avg</sub>, Kp<sub><i>i</i></sub> * p<sub>adsorpt</sub>).</pre>
  <br>
  </li>
  <li>
  Calculate the last row of the Jacobian matrix for each column <i>i</i>:
  <pre>Jac<sub>last row,<i>i</i></sub> = Kp<sub><i>i</i></sub> * p<sub>adsorpt</sub> * y<sub><i>i</i></sub> / &eta;<sup>2</sup><sub><i>i</i></sub>.</pre>
  <br>
  </li>
  <li>
  Calculate the hypothetical pure component pressures of each component <i>i</i>:
  <pre>p<sup>*</sup><sub><i>i</i></sub> = &eta;<sub><i>i</i></sub> / Kp<sub><i>i</i></sub>.</pre>
  <br>
  </li>
  <li>
  Calculate the diagonal of the Jacobian matrix for each component <i>i</i>:
  <pre>Jac<sub>diagonal,<i>i</i></sub> = 1 / (M<sub><i>i</i></sub> * &eta;<sub><i>i</i></sub>) * x<sub>adsorpt,<i>i</i></sub>(p<sup>*</sup><sub><i>i</i></sub>, T<sub>adsorpt</sub>).</pre>
  <br>
  </li>
  <li>
  Correct the last element of the diagonal of the Jacobian matrix:
  <pre>Jac<sub>diagonal,last</sub> = Jac<sub>last row,last</sub> + Jac<sub>diagonal,last</sub> * &sum;<sub><i>i</i></sub><sup>N-1</sup> [Jac<sub>last row,<i>i</i></sub> / Jac<sub>diagonal,<i>i</i></sub>].</pre>
  <br>
  </li>
  <li>
  Calculate the reduced spreading pressures of each component <i>i</i>:
  <pre>&pi;<sub><i>i</i></sub> = f(M<sub><i>i</i></sub>, p<sup>*</sup><sub><i>i</i></sub>, T<sub>adsorpt</sub>).</pre>
  <br>
  </li>
  <li>
  Calculate the differences between the reduced spreading pressure of each 
  component <i>i</i> and the reduced spreading pressure of the last component:
  <pre>&Delta;&pi;<sub><i>i</i></sub> = &pi;<sub><i>i</i></sub> - &pi;<sub>last</sub>.</pre>
  <br>
  </li>
  <li>
  Correct the last element of the differences between the reduced spreading pressure 
  of each component <i>i</i> and the reduced spreading pressure of the last component:
  <pre>&Delta;&pi;<sub>last</sub> =1 - &sum;<sub><i>i</i></sub> [Kp<sub><i>i</i></sub> * p<sub>adsorpt</sub> * y<sub><i>i</i></sub> / &eta;<sub><i>i</i></sub>] - &sum;<sub><i>i</i></sub><sup>N-1</sup> [Jac<sub>last row,<i>i</i></sub> / Jac<sub>diagonal,<i>i</i></sub> * &Delta;&pi;<sub><i>i</i></sub>].</pre>
  <br>
  </li>
  <li>
  Calculate the change of the reduced pressure of the last component:
  <pre>&Delta;&eta;<sub>last</sub> = &Delta;&pi;<sub>last</sub> / Jac<sub>diagonal,last</sub>.</pre>
  <br>
  </li>
  <li>
  Calculate the change of the reduced pressure of the remaining components <i>i</i>:
  <pre>&Delta;&eta;<sub><i>i</i></sub> = (-&Delta;&pi;<sub><i>i</i></sub>  + Jac<sub>diagonal,last</sub> * &Delta;&eta;<sub>last</sub>) / Jac<sub>diagonal,<i>i</i></sub>.</pre>
  <br>
  </li>
  <li>
  Calculate the reduced pressure of the next iteration step of each component <i>i</i>:
  <pre>&eta;<sub>next,<i>i</i></sub> = <strong>if</strong> &eta;<sub><i>i</i></sub> + &Delta;&eta;<sub><i>i</i></sub> > 0 <strong>then</strong> &eta;<sub><i>i</i></sub> + &Delta;&eta;<sub><i>i</i></sub> <strong>else</strong> &eta;<sub><i>i</i></sub>/2.</pre>
  <br>
  </li>
  <li>
  Check for convergence:
  <pre>&sum; |&Delta;&eta;<sub><i>i</i></sub>| &le; tolerance.</pre>
  If the convergence criterion is not fulfilled, got to step 6. It the convergence
  criterion is fulfilled, got to step 17.
  <br>
  </li>
  <li>
  Calculate the molar composition of each component <i>i</i> in the adsorpt
  phase:
  <pre>z<sub><i>i</i></sub> = y<sub><i>i</i></sub> * p<sub>adsorpt</sub> / p<sup>*</sup><sub><i>i</i></sub>.</pre>
  <br>
  </li>
  <li>
  Calculate the molar uptake of each component <i>i</i>:
  <pre>q<sub>adsorpt,<i>i</i></sub> = 1 / M<sub><i>i</i></sub> * x<sub>adsorpt,<i>i</i></sub>(p<sup>*</sup><sub><i>i</i></sub>, T<sub>adsorpt</sub>).</pre>
  <br>
  </li>
  <li>
  Calculate the total molar uptake:
  <pre>q<sub>adsorpt,total</sub> = 1 / &sum;<sub><i>i</i></sub> [z<sub><i>i</i></sub> / q<sub>adsorpt,<i>i</i></sub>].</pre>
  <br>
  </li>
  <li>
  Calculate the uptakes of each component <i>i</i>:
  <pre>x<sub>adsorpt,<i>i</i></sub> = M<sub><i>i</i></sub> * z<sub><i>i</i></sub> * q<sub>adsorpt,total</sub>.</pre>
  </li>
</ol>

<h4>References</h4>
<ul>
  <li>
  Mangano E., Friedrich, D., and Brandani, S. (2015). Robust algorithms for the solution of the ideal adsorbed solution theory equations. AIChE Journal, 61(3): 981–991. DOI: 10.1002/aic.14684.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  November 14, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end x_pyT_FastIAST;
