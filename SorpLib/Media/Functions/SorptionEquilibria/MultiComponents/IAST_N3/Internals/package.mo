within SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.IAST_N3;
package Internals "Internal functions used to avoid redundancy"
  extends Modelica.Icons.InternalPackage;

  annotation (Documentation(info="<html>
<p>
This package contains functions used for various functions of the IAST for
three components. Thus, redundancy of code shall be avoided.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 14, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Internals;
