within SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.Toth.Internals;
function p_i_xT
  "Extended Toth isotherm model: Partial pressures as function of uptakes and temperature"
  extends SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialMulti;

  //
  // Definition of inputs
  //
  input SorpLib.Units.Uptake[size(c,2)] x_adsorpt
    "Equilibrium uptakes of the adsorpt phase"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of outputs
  //
  output Modelica.Units.SI.Pressure[size(c,2)] p_i
    "Equilibrium pressures of the adsorpt phase"
    annotation (Dialog(tab="General", group="Outputs", enable=false));

  //
  // Definition of variables
  //
protected
  Real[size(c,2), size(c,2)] A
    "Matrix A of A*zz = -x_adsorpt";
  Real[size(c,2)] zz
    "Vector zz of A*zz = -x_adsorpt (i.e., zz = second substitution)";
  Real[size(c,2)] z
    "Vector z (i.e., z = first substitution)";

  SorpLib.Units.Uptake[size(c,2)] x_adsorpt_pow_c3
    "Equilibrium uptakes of the adsorpt phase to the power of coefficient c3";

algorithm
  //
  // Set up matrix A and solve system of linear equations:
  //
  // To calculate the invesere function of the extended Toth isotherm regarding
  // the equilibrium pressure and mole fractions of the independent components of
  // the gas or vapor phase, the term ' c[2,ind] * p_i[ind]' is substituted by 'z,'
  // and the term 'z ^ c[3,ind] is substituted by 'zz.' Thus, a system of linear
  // equations is created that canbe solved. Re-substitution allows to calculate
  // partial pressures p_i and,thus, the equilibrium pressure and mole fractions.
  //
  for ind_comp in 1:size(c,2) loop
    x_adsorpt_pow_c3[ind_comp] := x_adsorpt[ind_comp] ^ c[3, ind_comp]
      "First, calculate equilibrium uptakes of the adsorpt phase to the power of 
      coefficient c3";
    A[ind_comp,:] := fill(x_adsorpt_pow_c3[ind_comp], size(c,2))
      "Second, fill row with correct uptake";
    A[ind_comp,ind_comp] :=  x_adsorpt_pow_c3[ind_comp] - c[1,ind_comp] ^ c[3,ind_comp]
      "Third, correct diagonale";
  end for;

  zz := Modelica.Math.Matrices.solve(A, -1 .* x_adsorpt_pow_c3)
    "Fourth, solve the system of linear equations";

  for ind_comp in 1:size(c,2) loop
    z[ind_comp] := zz[ind_comp] ^ (1/c[3, ind_comp])
      "Re-substituate values and consider threshold if necessary";
    p_i[ind_comp] := max(z[ind_comp] / c[2,ind_comp], p_threshold_min)
      "Re-substituate values and consider threshold if necessary";
  end for;

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function calculates the equilibrium pressures <i>p_i</i> (i.e., partial
pressures) as function of the equilibrium uptakes <i>x_adsorpt</i> and the 
equilibrium temperature <i>T_adsorpt</i>. Thus, this function is a inverse of the 
function 'x_pyT.' For full details of the original function 'x_pyT,' check the 
documentation of the function 
<a href=\"Modelica://SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.Toth.x_pyT\">SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.Toth.x_pyT</a>.
</p>
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 8, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end p_i_xT;
