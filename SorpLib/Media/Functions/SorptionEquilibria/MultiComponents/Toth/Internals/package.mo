within SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.Toth;
package Internals "Internal functions used to avoid redundancy"
  extends Modelica.Icons.InternalPackage;

  annotation (Documentation(info="<html>
<p>
This package contains functions used for various functions of the extended Sips isotherm
model. Thus, redundancy of code shall be avoided.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 9, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end Internals;
