within SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.Toth.Internals;
function calc_p_i_num_i_den_i
  "Extended Toth isotherm model: Corrected partial pressures, numerators, denominators of the isotherm model"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Real[:,:] c
    "Coefficients of the isotherm model"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.Pressure[size(c,2)] p_i
    "Equilibrium pressures of the adsorpt phase"
    annotation (Dialog(tab="General", group="Inputs"));

  input Modelica.Units.SI.Pressure p_threshold_min = 0
    "Threshold for partial pressures of all components: If a partial pressure is
    below the threshold, its value is set to the threshold"
    annotation (Dialog(tab="General", group="Numerical inputs"));

  //
  // Definition of outputs
  //
  output Modelica.Units.SI.Pressure[size(c,2)] p_i_regulated
    "Regulated equilibrium pressures of the adsorpt phase"
    annotation (Dialog(tab="General", group="Outputs", enable=false));
  output Real[size(c,2)] num_i
    "Individual numerator for each component of the isotherm model"
    annotation (Dialog(tab="General", group="Outputs", enable=false));
  output Real[size(c,2)] den_i
    "Individual denominator for each component of the isotherm model"
    annotation (Dialog(tab="General", group="Outputs", enable=false));

  //
  // Definition of variables
  //
protected
  Real den_tmp
    "Identical temporary denominator for all components of the isotherm model";

algorithm
  den_tmp := 1
    "Start value of the temporary denominator";

  for ind_comp in 1:size(c,2) loop
    p_i_regulated[ind_comp] := max(p_i[ind_comp], p_threshold_min)
      "Limiter of partial pressures to increase numerical stability";

    num_i[ind_comp] := c[1,ind_comp] * c[2,ind_comp] * p_i_regulated[ind_comp]
      "Individual numerators";

    den_tmp := den_tmp + (c[2,ind_comp] * p_i_regulated[ind_comp]) ^ c[3,ind_comp]
      "Identical temporary denominator";
  end for;

  for ind_comp in 1:size(c,2) loop
    den_i[ind_comp] := den_tmp ^ (1/c[3,ind_comp])
      "Individual denominators";
  end for;

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function regulates the equilibrium pressures <i>p_i</i> (i.e., partial
pressures) so that they are equal or greater than <i>p_threshold_min</i>. Further,
this function calculates the numerators <i>num_i</i> and denominators <i>den_i</i>
of the extended Toth isotherm model. For full details of the extended Toth
isotherm mode, check the documentation of the function 
<a href=\"Modelica://SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.Toth.x_pyT\">SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.Toth.x_pyT</a>.
</p>
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 8, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end calc_p_i_num_i_den_i;
