within SorpLib.Media.Functions.SorptionEquilibria.Interfaces;
partial package PartialMultiComponentsIAST "Base package for all isotherm models of multi components based on the IAST"
  extends Modelica.Icons.FunctionsPackage;

  //
  // Basic isotherm equations
  //
  replaceable partial function x_pyT
    "Uptakes as function of pressure, mole fractions of independent gas phase components, and temperature"
    extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialMultiIAST_x_pyT;

    //
    // Annotations
    //
    annotation (Inline=false,
                InlineAfterIndexReduction=false,
                LateInline=true);
  end x_pyT;

  replaceable partial function p_xyT
    "Pressure as function of uptakes, mole fractions of independent gas phase components, and temperature"
    extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialMultiIAST_p_xyT;

    //
    // Annotations
    //
    annotation (Inline=false,
                InlineAfterIndexReduction=false,
                LateInline=true);
  end p_xyT;

  replaceable partial function y_pxT
    "Mole fractions of independent gas phase components as function of pressure, uptakes, and temperature"
    extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialMultiIAST_y_pxT;

    //
    // Annotations
    //
    annotation (Inline=false,
                InlineAfterIndexReduction=false,
                LateInline=true);
  end y_pxT;

  replaceable partial function py_xT
    "Pressure and mole fractions of independent gas phase components as function of uptakes and temperature"
    extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialMultiIAST_py_xT;

    //
    // Annotations
    //
    annotation (Inline=false,
                InlineAfterIndexReduction=false,
                LateInline=true);
  end py_xT;

  replaceable partial function dx_dp
    "Partial derivative of uptakes w.r.t. pressure at constant mole fractions and temperature"
    extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialMultiIAST_dx_dp;
  end dx_dp;

  replaceable partial function dx_dy
    "Partial derivative of uptakes w.r.t. molar fractions of independent gas phase components at constant temperature and pressure"
    extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialMultiIAST_dx_dy;
  end dx_dy;

  replaceable partial function dx_dT
    "Partial derivative of uptakes w.r.t. temperature at contsant pressure and mole fractions"
    extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialMultiIAST_dx_dT;
  end dx_dT;
  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial package is the basic package for all isotherm models describing 
the adsorption of multi components based on the ideal adsorption solution
theory.
<br/><br/>
This partial package contains all declarations for a multi-component isotherm
model. This means that functions are defined that every multi-component isotherm
model must support. A multi-component isotherm model inherits from this partial
package and must provide the isotherm equations by redeclaring all partial
functions.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 7, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end PartialMultiComponentsIAST;
