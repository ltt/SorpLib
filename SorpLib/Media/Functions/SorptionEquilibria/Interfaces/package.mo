within SorpLib.Media.Functions.SorptionEquilibria;
package Interfaces "Interfaces for sorption equilibria models"
  extends Modelica.Icons.InterfacesPackage;

  annotation (Documentation(info="<html>
<p>
This package provides definitions of basic interfaces for sorption equilibria models. 
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Interfaces;
