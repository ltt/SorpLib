within SorpLib.Media.Functions.SorptionEquilibria.Interfaces;
partial package PartialPureComponentsDubinin "Base package for all isotherm models of pure components based on the model of Dubinin"
  extends
  SorpLib.Media.Functions.SorptionEquilibria.Interfaces.PartialPureComponents;

  //
  // Dubinin-specific isotherm equations
  //
  replaceable partial function W_A
    "Filled pore volume as function of molar adsorption potential"
    extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_W_A;

    //
    // Annotations
    //
    annotation (Inline=false,
                InlineAfterIndexReduction=false,
                LateInline=true);
  end W_A;

  replaceable partial function A_W
    "Molar adsorption potential as function of filled pore volume"
    extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_A_W;

    //
    // Annotations
    //
    annotation (Inline=false,
                InlineAfterIndexReduction=false,
                LateInline=true);
  end A_W;

  replaceable partial function dW_dA
    "Partial derivative of filled pore volume w.r.t. molar adsorption potential at constant pressure and temperature"
    extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_dW_dA;
  end dW_dA;

  replaceable partial function ddW_dA_dA
    "Second-order partial derivative of filled pore volume w.r.t. molar adsorption potential at constant pressure and temperature"
    extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_ddW_dA_dA;
  end ddW_dA_dA;

  replaceable partial function ddW_dA_dT
    "Second-order partial derivative of filled pore volume w.r.t. molar adsorption potential and temperature at constant pressure"
    extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_ddW_dA_dT;
  end ddW_dA_dT;
  //
  // Redeclare basic isotherm equations
  //
  redeclare replaceable function extends x_pT
    "Uptake as function of pressure and temperature"

    //
    // Definition of variables
    //
protected
    SorpLib.Units.MolarAdsorptionPotential A=
      SorpLib.Media.Functions.SorptionEquilibria.Utilities.A_ppsT(
      p_adsorpt=p_adsorpt,
      p_sat=c[1],
      T_adsorpt=T_adsorpt)
      "Adsorption potential";
    SorpLib.Units.FilledPoreVolume W=W_A(
      A=A,
      c=c)
      "Filled pore volume";

  algorithm
    x_adsorpt := c[2] * W
      "Calculation of the equilibrium uptake of the adsorpt phase";

    //
    // Annotations
    //
    annotation (Inline=false,
                InlineAfterIndexReduction=false,
                LateInline=true);
  end x_pT;

  redeclare replaceable function extends p_xT
    "Pressure as function of uptake and temperature"

    //
    // Definition of variables
    //
protected
    SorpLib.Units.MolarAdsorptionPotential A=A_W(
       W=x_adsorpt/c[2],
       c=c)
       "Adsorption potential";

  algorithm
    p_adsorpt :=SorpLib.Media.Functions.SorptionEquilibria.Utilities.p_ApsT(
      A=A,
      p_sat=c[1],
      T_adsorpt=T_adsorpt)
      "Calculation of the equilibrium pressure of the adsorpt phase";

    //
    // Annotations
    //
    annotation (Inline=false,
                InlineAfterIndexReduction=false,
                LateInline=true);
  end p_xT;

  redeclare final function extends dx_dp
    "Partial derivative of uptake w.r.t. pressure at constant temperature"

    //
    // Definition of variables
    //
protected
    SorpLib.Units.MolarAdsorptionPotential A=
      SorpLib.Media.Functions.SorptionEquilibria.Utilities.A_ppsT(
      p_adsorpt=p_adsorpt,
      p_sat=c[1],
      T_adsorpt=T_adsorpt)
      "Adsorption potential";
    SorpLib.Units.DerMolarAdsorptionPotentialByPressure dA_dp_adsorpt=
      SorpLib.Media.Functions.SorptionEquilibria.Utilities.dA_dp(
      p_adsorpt=p_adsorpt,
      p_sat=c[1],
      T_adsorpt=T_adsorpt)
      "Partial derivative of adsorption potential w.r.t. to pressure at
      constant temperature";

    SorpLib.Units.DerFilledPoreVolumeByAdsorptionPotential dW_dA_=dW_dA(
      A=A,
      c=c)
      "Partial derivative of filled pore volume w.r.t. adsorption potential at
      constant pressure and temperature";

  algorithm
    dx_adsorpt_dp_adsorpt := c[2] * dW_dA_*dA_dp_adsorpt
      "Calculation of the partial derivative of the equilibrium uptake w.r.t. the
       equilibrium pressure at constant temperature";
  end dx_dp;

  redeclare final function extends ddx_dp_dp
    "Second-order partial derivative of uptake w.r.t. pressure at constant temperature"

    //
    // Definition of variables
    //
protected
    SorpLib.Units.MolarAdsorptionPotential A=
      SorpLib.Media.Functions.SorptionEquilibria.Utilities.A_ppsT(
      p_adsorpt=p_adsorpt,
      p_sat=c[1],
      T_adsorpt=T_adsorpt)
      "Adsorption potential";
    SorpLib.Units.DerMolarAdsorptionPotentialByPressure dA_dp_adsorpt=
      SorpLib.Media.Functions.SorptionEquilibria.Utilities.dA_dp(
      p_adsorpt=p_adsorpt,
      p_sat=c[1],
      T_adsorpt=T_adsorpt)
      "Partial derivative of adsorption potential w.r.t. to pressure at constant
      temperature";
    SorpLib.Units.DerMolarAdsorptionPotentialByPressurePressure ddA_dp_adsorpt_dp_adsorpt=
      SorpLib.Media.Functions.SorptionEquilibria.Utilities.ddA_dp_dp(
      p_adsorpt=p_adsorpt,
      p_sat=c[1],
      T_adsorpt=T_adsorpt)
      "Second-order partial derivative of adsorption potential w.r.t. to pressure
      at constant temperature";

    SorpLib.Units.DerFilledPoreVolumeByAdsorptionPotential dW_dA_=dW_dA(
      A=A,
      c=c)
      "Partial derivative of filled pore volume w.r.t. adsorption potential at
      constant pressure and temeprature";
    SorpLib.Units.DerFilledPoreVolumeByAdsorptionPotentialAdsorptionPotential ddW_dA_dA_=ddW_dA_dA(
      A=A,
      c=c)
      "Second-order partial derivative of filled pore volume w.r.t. adsorption 
      potential at constant pressure and temeprature";

  algorithm
    ddx_adsorpt_dp_adsorpt_dp_adsorpt :=
      (c[2] * dA_dp_adsorpt) * ddW_dA_dA_*dA_dp_adsorpt +
      (c[2] * dW_dA_) * ddA_dp_adsorpt_dp_adsorpt
      "Calculation of the second-order partial derivative of the equilibrium uptake 
      w.r.t. the equilibrium pressure at constant temperature";
  end ddx_dp_dp;
  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial package is the basic package for all isotherm models describing 
the adsorption of pure components based on the model of Dubinin. Such models
describe the adsorption based on filled pores. 
<br/><br/>
This partial package contains all declarations for a pure component isotherm
model. This means that functions are defined that every pure component isotherm
model must support. A pure component isotherm model inherits from this partial
package and must provide the isotherm equations by redeclaring all partial
functions.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 1, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end PartialPureComponentsDubinin;
