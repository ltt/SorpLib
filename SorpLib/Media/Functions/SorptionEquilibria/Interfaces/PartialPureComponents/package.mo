within SorpLib.Media.Functions.SorptionEquilibria.Interfaces;
partial package PartialPureComponents "Base package for all isotherm models of pure components"
  extends Modelica.Icons.FunctionsPackage;

  //
  // Basic isotherm equations
  //
  replaceable partial function x_pT
    "Uptake as function of pressure and temperature"
    extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_x_pT;

    //
    // Annotations
    //
    annotation (Inline=false,
                InlineAfterIndexReduction=false,
                LateInline=true);
  end x_pT;

  replaceable partial function p_xT
    "Pressure as function of uptake and temperature"
    extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_p_xT;

    //
    // Annotations
    //
    annotation (Inline=false,
                InlineAfterIndexReduction=false,
                LateInline=true);
  end p_xT;

  replaceable partial function dx_dp
    "Partial derivative of uptake w.r.t. pressure at constant temperature"
    extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_dx_dp;
  end dx_dp;

  replaceable partial function dx_dT
    "Partial derivative of uptake w.r.t. temperature at constant pressure"
    extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_dx_dT;
  end dx_dT;

  replaceable partial function ddx_dp_dp
    "Second-order partial derivative of uptake w.r.t. pressure at constant temperature"
    extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_ddx_dp_dp;
  end ddx_dp_dp;

  replaceable partial function ddx_dT_dT
    "Second-order partial derivative of uptake w.r.t. temperature at constant pressure"
    extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_ddx_dT_dT;
  end ddx_dT_dT;

  replaceable partial function ddx_dp_dT
    "Second-order partial derivative of uptake w.r.t. pressure and temperature"
    extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_ddx_dp_dT;
  end ddx_dp_dT;
  //
  // Equations regarding reduced spreading pressure
  //
  replaceable partial function pi_pT
    "Reduced spreading pressure as function of pressure and temperature"
    extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_pi_pT;
  end pi_pT;

  replaceable partial function p_piT
    "Pressure as function of reduced spreading pressure and temperature"
    extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_p_piT;
  end p_piT;
  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial package is the basic package for all isotherm models describing 
the adsorption of pure components. Such models can be classical isotherm models 
based on a (plain) boundary surface or isotherm models based on filled pores. 
<br/><br/>
This partial package contains all declarations for a pure component isotherm
model. This means that functions are defined that every pure component isotherm
model must support. A pure component isotherm model inherits from this partial
package and must provide the isotherm equations by redeclaring all partial
functions.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 1, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end PartialPureComponents;
