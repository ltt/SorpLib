within SorpLib.Media.Functions.SorptionEquilibria.Records;
record NumericsIAST
  "This record contains entries defining the numerics of IAST algorithms"
  extends Modelica.Icons.Record;

  //
  // Definition of paramters
  //
  parameter SorpLib.Choices.IASTAlgorithm IASTAlgorithm=SorpLib.Choices.IASTAlgorithm.NewtonRaphson
    "Algorithm used to solve the IAST"
    annotation (Dialog(tab="General", group="Basic setup"));

  parameter Integer no_max = 250
    "Maximal number of iterations"
    annotation (Dialog(tab="General", group="Newton-Raphson and Fast IAST",
                enable=IASTAlgorithm == SorpLib.Choices.IASTAlgorithm.FastIAST
           or IASTAlgorithm == SorpLib.Choices.IASTAlgorithm.NewtonRaphson));
  parameter Real tolerance = 1e-10
    "Tolereance for loop"
    annotation (Dialog(tab="General", group="Newton-Raphson and Fast IAST",
                enable=IASTAlgorithm == SorpLib.Choices.IASTAlgorithm.FastIAST
           or IASTAlgorithm == SorpLib.Choices.IASTAlgorithm.NewtonRaphson));

  parameter Integer no_max_inner = 25
    "Maximal number of iterations for inner loop"
    annotation (Dialog(tab="General", group="Nested Loop - Inner loop",
                enable=IASTAlgorithm == SorpLib.Choices.IASTAlgorithm.NestedLoop));
  parameter Real tolerance_inner = 1e-10
    "Tolerance for inner loop"
    annotation (Dialog(tab="General", group="Nested Loop - Inner loop",
                enable=IASTAlgorithm == SorpLib.Choices.IASTAlgorithm.NestedLoop));

  parameter Integer no_max_outer = 25
    "Maximal number of iterations for outer loop"
    annotation (Dialog(tab="General", group="Nested Loop - Outer loop",
                enable=IASTAlgorithm == SorpLib.Choices.IASTAlgorithm.NestedLoop));
  parameter Real tolerance_outer = 1e-10
    "Tolerance for outer loop"
    annotation (Dialog(tab="General", group="Nested Loop - Outer loop",
                enable=IASTAlgorithm == SorpLib.Choices.IASTAlgorithm.NestedLoop));

  parameter Integer no_max_inv = 250
    "Maximal number of iterations"
    annotation (Dialog(tab="General", group="Inverses of IAST"));
  parameter Real tolerance_inv = 1e-10
    "Tolereance for loop"
    annotation (Dialog(tab="General", group="Inverses of IAST"));

  parameter Modelica.Units.SI.Pressure p_K_0 = 1e-6
    "Pressure used to estimated Henry constants"
    annotation (Dialog(tab="General", group="Initial values"));
  parameter Modelica.Units.SI.Pressure p_sat_0 = 1e15
    "Pressure used to estimated saturation uptake"
    annotation (Dialog(tab="General", group="Initial values",
                enable=IASTAlgorithm == SorpLib.Choices.IASTAlgorithm.FastIAST));

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This record contains entries defining the numerics of IAST algorithms.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 10, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end NumericsIAST;
