within SorpLib.Media.Functions.SorptionEquilibria;
package Records "Package containing records used as function inputs"
  extends Modelica.Icons.RecordsPackage;

  annotation (Documentation(info="<html>
<p>
This package contains definitions of records that are used as function inputs. 
Using records reduces the number of function inputs. In addition, the functions 
are easier to expand if the function prototype needs to remain unchanged, as only 
more entries need to be added to the data set if necessary.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Records;
