within SorpLib.Media.Functions.SorptionEquilibria.Records;
record NumericsIAST_PureComponents
  "This record contains entries defining the numerics of pure component isotherm models used within IAST algorithms"
  extends Modelica.Icons.Record;

  //
  // Definition of paramters
  //
  parameter Modelica.Units.SI.Pressure p_adsorpt_lb_start = 1
    "Lower bound of equilibrium pressure (required if pressure is calculated 
    numerically (i.e., root finding))"
    annotation (Dialog(tab="General", group="Equilibrium pressure"));
  parameter Modelica.Units.SI.Pressure p_adsorpt_ub_start = 10
    "Upper bound of equilibrium pressure (required if pressure is calculated 
    numerically (i.e., root finding))"
    annotation (Dialog(tab="General", group="Equilibrium pressure"));
  parameter Real tolerance_p_adsorpt = 100*Modelica.Constants.eps
    "Tolerance for numerical calculation (required if pressure is calculated
    numerically (i.e., root finding))"
    annotation (Dialog(tab="General", group="Equilibrium pressure"));

  parameter Modelica.Units.SI.Pressure integral_pi_lb = 0
    "Lower limit of integral when calculating the reduced spreading pressure
    numerically (should be 0)"
    annotation (Dialog(tab="General", group="Reduced spreading pressure"));
  parameter Real tolerance_pi = 100*Modelica.Constants.eps
    "Tolerance for numerical calculation (required if reduced spreading pressure
    is calculated numerically (i.e., integral))"
    annotation (Dialog(tab="General", group="Reduced spreading pressure"));

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This record contains entries defining the numerics of pure component isotherm 
models used within IAST algorithms.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 10, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end NumericsIAST_PureComponents;
