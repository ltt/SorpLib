within SorpLib.Media.Functions.SorptionEquilibria.BaseClasses;
partial model PartialTestMultiIAST_N2
  "Base model for testers of isotherm models describing two-component adsorption based on the ideal adsorbed solution theory"
  extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialTestMultiIAST(
      redeclare final package IsothermModel =
        SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.IAST_N2);

equation
  if check_func_p_xyT then
    p_adsorpt_inv_xyT = IsothermModel.p_xyT(
      x_adsorpt=x_adsorpt,
      y_i=y_i,
      T_adsorpt=T_adsorpt,
      M_i=M_i,
      c_1=c_1,
      c_2=c_2,
      func_x_pT_1=IsothermModelComponent1.x_pT,
      func_p_xT_1=IsothermModelComponent1.p_xT,
      func_dx_dp_1=IsothermModelComponent1.dx_dp,
      func_pi_pT_1=IsothermModelComponent1.pi_pT,
      func_p_piT_1=IsothermModelComponent1.p_piT,
      func_x_pT_2=IsothermModelComponent2.x_pT,
      func_p_xT_2=IsothermModelComponent2.p_xT,
      func_dx_dp_2=IsothermModelComponent2.dx_dp,
      func_pi_pT_2=IsothermModelComponent2.pi_pT,
      func_p_piT_2=IsothermModelComponent2.p_piT,
      num=num,
      num_comp_1=num_comp_1,
      num_comp_2=num_comp_2)
      "Equilibrium pressure calculated via the inverse function 'p_xyT'";
  else
    p_adsorpt_inv_xyT = -1
      "Equilibrium pressure calculated via the inverse function 'p_xyT'";
  end if;

  if check_func_y_pxT then
    y_i_inv_pxT = IsothermModel.y_pxT(
      p_adsorpt=p_adsorpt,
      x_adsorpt=x_adsorpt,
      T_adsorpt=T_adsorpt,
      M_i=M_i,
      c_1=c_1,
      c_2=c_2,
      func_x_pT_1=IsothermModelComponent1.x_pT,
      func_p_xT_1=IsothermModelComponent1.p_xT,
      func_dx_dp_1=IsothermModelComponent1.dx_dp,
      func_pi_pT_1=IsothermModelComponent1.pi_pT,
      func_p_piT_1=IsothermModelComponent1.p_piT,
      func_x_pT_2=IsothermModelComponent2.x_pT,
      func_p_xT_2=IsothermModelComponent2.p_xT,
      func_dx_dp_2=IsothermModelComponent2.dx_dp,
      func_pi_pT_2=IsothermModelComponent2.pi_pT,
      func_p_piT_2=IsothermModelComponent2.p_piT,
      num=num,
      num_comp_1=num_comp_1,
      num_comp_2=num_comp_2)
      "Independent mole fractions calculated via the inverse function 'y_pxT'";
  else
    y_i_inv_pxT = fill(-1, no_components-1)
      "Independent mole fractions calculated via the inverse function 'y_pxT'";
  end if;

  if check_func_py_xT then
    (p_adsorpt_inv_xT, y_i_inv_xT) = IsothermModel.py_xT(
      x_adsorpt=x_adsorpt,
      T_adsorpt=T_adsorpt,
      M_i=M_i,
      c_1=c_1,
      c_2=c_2,
      func_x_pT_1=IsothermModelComponent1.x_pT,
      func_p_xT_1=IsothermModelComponent1.p_xT,
      func_dx_dp_1=IsothermModelComponent1.dx_dp,
      func_pi_pT_1=IsothermModelComponent1.pi_pT,
      func_p_piT_1=IsothermModelComponent1.p_piT,
      func_x_pT_2=IsothermModelComponent2.x_pT,
      func_p_xT_2=IsothermModelComponent2.p_xT,
      func_dx_dp_2=IsothermModelComponent2.dx_dp,
      func_pi_pT_2=IsothermModelComponent2.pi_pT,
      func_p_piT_2=IsothermModelComponent2.p_piT,
      num=num,
      num_comp_1=num_comp_1,
      num_comp_2=num_comp_2)
      "Independent mole fractions calculated via the inverse function 'y_xT'";
  else
    p_adsorpt_inv_xT = -1
      "Equilibrium pressure calculated via the inverse function 'p_xT'";
    y_i_inv_xT = fill(-1, no_components-1)
      "Independent mole fractions calculated via the inverse function 'y_xT'";
  end if;

  x_adsorpt = IsothermModel.x_pyT(
    p_adsorpt=p_adsorpt,
    y_i=y_i,
    T_adsorpt=T_adsorpt,
    M_i=M_i,
    c_1=c_1,
    c_2=c_2,
    func_x_pT_1=IsothermModelComponent1.x_pT,
    func_p_xT_1=IsothermModelComponent1.p_xT,
    func_dx_dp_1=IsothermModelComponent1.dx_dp,
    func_pi_pT_1=IsothermModelComponent1.pi_pT,
    func_p_piT_1=IsothermModelComponent1.p_piT,
    func_x_pT_2=IsothermModelComponent2.x_pT,
    func_p_xT_2=IsothermModelComponent2.p_xT,
    func_dx_dp_2=IsothermModelComponent2.dx_dp,
    func_pi_pT_2=IsothermModelComponent2.pi_pT,
    func_p_piT_2=IsothermModelComponent2.p_piT,
    num=num,
    num_comp_1=num_comp_1,
    num_comp_2=num_comp_2)
    "Equilibrium uptakes";

  if check_func_dx_dp then
    dx_adsorpt_dp_adsorpt = IsothermModel.dx_dp(
      p_adsorpt=p_adsorpt,
      y_i=y_i,
      T_adsorpt=T_adsorpt,
      M_i=M_i,
      c_1=c_1,
      c_2=c_2,
      func_x_pT_1=IsothermModelComponent1.x_pT,
      func_p_xT_1=IsothermModelComponent1.p_xT,
      func_dx_dp_1=IsothermModelComponent1.dx_dp,
      func_pi_pT_1=IsothermModelComponent1.pi_pT,
      func_p_piT_1=IsothermModelComponent1.p_piT,
      func_x_pT_2=IsothermModelComponent2.x_pT,
      func_p_xT_2=IsothermModelComponent2.p_xT,
      func_dx_dp_2=IsothermModelComponent2.dx_dp,
      func_pi_pT_2=IsothermModelComponent2.pi_pT,
      func_p_piT_2=IsothermModelComponent2.p_piT,
      num=num,
      num_comp_1=num_comp_1,
      num_comp_2=num_comp_2,
      dp=dp)
      "Partial derivative of the uptake w.r.t. pressure at constant mole fractions
      and temperature";
  else
    dx_adsorpt_dp_adsorpt = fill(-1, no_components)
      "Partial derivative of the uptake w.r.t. pressure at constant mole fractions
      and temperature";
  end if;

  if check_func_dx_dy then
    dx_adsorpt_dy_i = IsothermModel.dx_dy(
      p_adsorpt=p_adsorpt,
      y_i=y_i,
      T_adsorpt=T_adsorpt,
      M_i=M_i,
      c_1=c_1,
      c_2=c_2,
      func_x_pT_1=IsothermModelComponent1.x_pT,
      func_p_xT_1=IsothermModelComponent1.p_xT,
      func_dx_dp_1=IsothermModelComponent1.dx_dp,
      func_pi_pT_1=IsothermModelComponent1.pi_pT,
      func_p_piT_1=IsothermModelComponent1.p_piT,
      func_x_pT_2=IsothermModelComponent2.x_pT,
      func_p_xT_2=IsothermModelComponent2.p_xT,
      func_dx_dp_2=IsothermModelComponent2.dx_dp,
      func_pi_pT_2=IsothermModelComponent2.pi_pT,
      func_p_piT_2=IsothermModelComponent2.p_piT,
      num=num,
      num_comp_1=num_comp_1,
      num_comp_2=num_comp_2,
      dy=dy)
      "Partial derivative of the uptake w.r.t. independent molar fractions at
      constant pressure and temperature";
  else
    dx_adsorpt_dy_i = fill(-1, no_components, no_components-1)
      "Partial derivative of the uptake w.r.t. independent molar fractions at
      constant pressure and temperature";
  end if;

  if check_func_dx_dT then
    dx_adsorpt_dT_adsorpt = IsothermModel.dx_dT(
      p_adsorpt=p_adsorpt,
      y_i=y_i,
      T_adsorpt=T_adsorpt,
      M_i=M_i,
      c_1=c_1,
      c_2=c_2,
      c_pdT_1=c_pdT_1,
      c_mdT_1=c_mdT_1,
      c_pdT_2=c_pdT_2,
      c_mdT_2=c_mdT_2,
      func_x_pT_1=IsothermModelComponent1.x_pT,
      func_p_xT_1=IsothermModelComponent1.p_xT,
      func_dx_dp_1=IsothermModelComponent1.dx_dp,
      func_pi_pT_1=IsothermModelComponent1.pi_pT,
      func_p_piT_1=IsothermModelComponent1.p_piT,
      func_x_pT_2=IsothermModelComponent2.x_pT,
      func_p_xT_2=IsothermModelComponent2.p_xT,
      func_dx_dp_2=IsothermModelComponent2.dx_dp,
      func_pi_pT_2=IsothermModelComponent2.pi_pT,
      func_p_piT_2=IsothermModelComponent2.p_piT,
      num=num,
      num_comp_1=num_comp_1,
      num_comp_2=num_comp_2,
      dT=dT)
      "Partial derivative of the uptake w.r.t. temperature at constant pressure
      and mole fractions";
  else
    dx_adsorpt_dT_adsorpt = fill(-1, no_components)
      "Partial derivative of the uptake w.r.t. temperature at constant pressure
      and mole fractions";
  end if;

  //
  // Annotations
  //
  annotation (Documentation(revisions="<html>
<ul>
  <li>
  November 10, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>", info="<html>
<p>
This partial model is the basic model for all testers of isotherm models 
based on the ideal adsorbed solution theory (IAST) describing the adsorption 
of two components.
<br/><br/>
This partial model defines all relevant parameters, variables, and equations to
check (1) the classical form of isotherm models, (2) the inverse forms of isotherm
models, (3) the partial derivatives of the uptakes w.r.t. to pressure, (4) the 
partial derivatives of the uptakes w.r.t. mole fractions of independent components
in the gas or vapor phase, and (5) the partial derivatives of the uptake w.r.t. 
temperature.
<br/><br/>
Models that inherit properties from this partial model have to specify the number 
of components <i>no_components</i> and number of coefficients for each pure 
component isotherm model <i>no_coefficients_i</i>, respectively, and have to redeclare 
all functions of the multi and pure component isotherm models <i>func_i</i>. 
Besides, the coefficients of each isotherm model (i.e., <i>c_i</i>, <i>c_pdT_i</i>, 
and <i>c_mdT_i</i>) have to be implemented. Additionally, it has to be specified 
whether the equilibrium pressure (<i>p_adsorpt_der</i>), the mole fractions of
independent components in the gas or vapor phase (<i>y_i_der</i>), or the 
equilibrium temperature (<i>T_adsorpt_der</i>) changes with time.
</p>
</html>"));
end PartialTestMultiIAST_N2;
