within SorpLib.Media.Functions.SorptionEquilibria.BaseClasses;
partial function PartialPure
  "Base function for isotherm models of pure components"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.Temperature T_adsorpt
    "Equilibrium temperature of the adsorpt phase"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real[:] c
    "Coefficients of the isotherm model"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial function is the basic function for all isotherm models describing 
the adsorption of pure components. Such models can be classical isotherm models 
based on a (plain) boundary surface or isotherm models based on filled pores. 
<br/><br/>
This partial function defines the equilibrium temperature <i>T_adsorpt</i> and
the coefficients of the isotherm model <i>c</i> as inputs. The coefficients of 
the isotherm model <i>c</i> may depend on the equilibrium temperature 
<i>T_adsorpt</i>.
<br/><br/>
Functions that inherit properties from this partial function may have to 
implement further inputs, the output, and the function algorithm.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 1, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end PartialPure;
