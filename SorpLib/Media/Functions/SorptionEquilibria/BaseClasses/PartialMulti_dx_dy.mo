within SorpLib.Media.Functions.SorptionEquilibria.BaseClasses;
partial function PartialMulti_dx_dy
  "Base function for isotherm models of multi components: Partial derivative of uptakes w.r.t. molar fractions of independent gas phase components at constant temperature and pressure"
  extends SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialMulti;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.Pressure p_adsorpt
    "Equilibrium pressure of the adsorpt phase"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.MoleFraction[size(c,2)-1] y_i
    "Mole fractions of independent components in the vapor or gas phase"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of outputs
  //
  output
    SorpLib.Units.DerUptakeByMolarFraction[size(c,2),size(c,2)-1] dx_adsorpt_dy_i
    "Partial derivatives of the uptakes w.r.t. the molar fractions of independent 
    gas phase components at constant temperature and pressure"
    annotation (Dialog(tab="General", group="Outputs", enable=false));

  //
  // Definition of variables
  //
protected
  Modelica.Units.SI.MoleFraction[size(c,2)] y_i_ = cat(1, y_i, {1-sum(y_i)})
    "Mole fractions of all components in the vapor or gas phase";
  Modelica.Units.SI.Pressure[size(c,2)] p_i = p_adsorpt .* y_i_
    "Partial pressures of all components";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial function is the basic function for all isotherm models describing 
the adsorption of multi components. Such models can be extended pure component 
isotherm models or specifically developed multi component isotherm models.  
<br/><br/>
This partial function is the basic function for calculating the partial derivative
in of the equilibrium uptakes w.r.t. the molar fractions of the independent components
the gas or vapor phase at constant temperature and pressure <i>dx_adsorpt_dy_i</i> 
as a function of the equilibrium pressure <i>p_adsorpt</i>, the mole fractions of 
the independent components in the gas or vapor phase <i>y_i</i>, the equilibrium 
temperature <i>T_adsorpt</i>. Further inputs are the coefficients of the isotherm 
model <i>c</i>. The coefficients of the isotherm model <i>c</i> may depend on the 
equilibrium temperature <i>T_adsorpt</i>. The coefficients of the isotherm model 
<i>c</i> are a matrix: The columns contain the coefficients of the different components. 
The rows contain the actual coefficients, i.e, the row size depends on the selected 
isotherm model. Different components may require different numbers of coefficients, 
which is why the row size depends on the component with the most coefficients. 
Optional input regarding numerics is the threshold <i>p_threshold_min</i> to 
regulate the minimal values of all partial pressures.
<br/><br/>
The partial derivatives of the equilibrium uptakes w.r.t. the molar fractions of 
the independent components the gas or vapor phase at constant temperature and pressure
<i>dx_adsorpt_dy_i</i> are defined as the outputs.
<br/><br/>
Functions that inherit properties from this partial function may have to implement
further inputs and the function algorithm.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 7, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end PartialMulti_dx_dy;
