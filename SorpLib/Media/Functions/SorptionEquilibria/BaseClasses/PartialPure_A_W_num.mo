within SorpLib.Media.Functions.SorptionEquilibria.BaseClasses;
partial function PartialPure_A_W_num
  "Base function for isotherm models of pure components: Molar adsorption potential as function of filled pore volume (numerical solution)"
   extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_A_W;

  //
  // Definition of protected replacable functions
  //
protected
  replaceable function func_W_A =
    SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininAstakhov.W_A
    constrainedby
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_W_A
    "Filled pore volume as function of molar adsorption potential"
    annotation (Dialog(tab="General", group="Functions"));

  //
  // Definition of functions
  //
protected
  function func_A_num
    "Function used to find root (i.e., A) numerically"
    extends Modelica.Math.Nonlinear.Interfaces.partialScalarFunction;
    input SorpLib.Units.FilledPoreVolume W
      "Filled pore volume";
    input Real c[:]
      "Coefficients of isotherm model";
  algorithm
    y := func_W_A(A=u, c=c) - W
     "Function '0 = W(A, c) - W' used to find root (i.e., A) numerically";
  end func_A_num;

  //
  // Definition of variables
  //
  Boolean bound_ok = false
    "= true, if bounds are found such that func_A_num(lb) > 0 and func_A_num(ub) < 0";

  SorpLib.Units.MolarAdsorptionPotential A_lb=A_lb_start
    "Current best lower bound of molar adsorption potential";
  SorpLib.Units.MolarAdsorptionPotential A_ub=A_ub_start
    "Current best upper bound of molar adsorption potential";

  SorpLib.Units.FilledPoreVolume W_lb = func_W_A(A=A_lb, c=c)
    "Filled pore volume at current best lower bound of molar adsorption potential";
  SorpLib.Units.FilledPoreVolume W_ub = func_W_A(A=A_ub, c=c)
    "Filled pore volume at current best upper bound of molar adsorption potential";

algorithm
  //
  // Find start values such that func_A_num(lb) > 0 and func_A_num(ub) < 0
  //
  // Reducing A increases W -> func_A_num(W) can become > 0
  // Increasing A reduces W -> func_A_num(W) can become < 0
  //
  while not bound_ok loop
    if Modelica.Math.isEqual(s1=W_lb, s2=W, eps=tolerance) then
      A := A_lb;
      bound_ok := true;
      return;

    elseif Modelica.Math.isEqual(s1=W_ub, s2=W, eps=tolerance) then
      A := A_ub;
      bound_ok := true;
      return;

    elseif W_lb-W < 0 and W_ub-W < 0 then
      A_ub := A_lb;
      W_ub := W_lb;

      A_lb := if A_lb > Modelica.Constants.small then A_lb*0.1 else 0;
      W_lb := func_W_A(A=A_lb, c=c);

    elseif W_lb-W > 0 and W_ub-W > 0 then
      A_lb := A_ub;
      W_lb := W_ub;

      A_ub := A_ub*10;
      W_ub := func_W_A(A=A_ub, c=c);

    else
      bound_ok := true;

    end if;

    //
    // Check if func_W_A is not strictly monotonic with A (i.e., unfavourable
    // parameterisation or function approach selected)
    //
    if Modelica.Math.isEqual(s1=A_lb, s2=A_ub, eps=tolerance) or A_lb > 1e15 then
      A := if A_lb > 1e15 then 1e15 else 0
        "Set A to bound";
      Modelica.Utilities.Streams.print("Characteristic curbe W(A) is not strictly monotonic with A: Inverse A(W) cannot be found!");
      return;
    end if;
  end while;

  //
  // Find root in the interval lb <= root <= up
  //
  A := Modelica.Math.Nonlinear.solveOneNonlinearEquation(
    f = function func_A_num(W=W, c=c),
    u_min = A_lb,
    u_max = A_ub,
    tolerance = tolerance)
    "Calculation of the molar adsorption potential";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial function is the basic function for all isotherm models describing 
the adsorption of pure components. Such models must be based on filled pores
(i.e., Dubinin theory).
<br/><br/>
This partial function is the basic function for calculating the molar adsorption 
potential <i>A</i> as a function of the filled pore volume <i>W</i>. Defined 
inputs are the filled pore volume <i>W</i> and the coefficients of the isotherm 
model <i>c</i>. The coefficients of the isotherm model <i>c</i> may depend on the 
equilibrium temperature <i>T_adsorpt</i>. Inputs regarding numerics are the lower 
bound (<i>A_lb_start</i>) and upper bound (<i>A_ub_start</i>) of the molar 
adsorption potential and the tolerance (<i>tolerance</i>). The defined output is 
molar the adsorption potential <i>A</i>.
<br/><br/>
With this function, the eadsorption potential <i>A</i> is determined numerically 
by solving a zero problem. The zero problem is as follows:
</p>
<pre>
    f(A) = 0 => 0 = func_W_A(A=A, c=c) - W;
</pre>
<p>
The zero problem is solved using the numerically very efficient function 
<a href=\"Modelica://Modelica.Math.Nonlinear.solveOneNonlinearEquation\">Modelica.Math.Nonlinear.solveOneNonlinearEquation</a>.
For the application of this function, it must be ensured that <i>f(A)</i> 
has a different sign for the upper and lower limits of <i>A</i>. To ensure 
the different signs, the upper and lower limits are determined in an upstream loop.
<br/><br/>
Functions that inherit properties from this partial function have to redeclare
the function <i>func_W_A</i>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 2, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end PartialPure_A_W_num;
