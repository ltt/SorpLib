within SorpLib.Media.Functions.SorptionEquilibria.BaseClasses;
partial model PartialTestMultiIAST
  "Base model for testers of isotherm models describing multi component adsorption based on the ideal adsorbed solution theory"
  extends Modelica.Icons.Example;

  //
  // Definition of parameters
  //
  parameter Real p_adsorpt_der(unit="Pa/s") = 1
    "Prescriped sloped of equilibrium pressure to test isotherm model"
    annotation (Dialog(tab="General", group="Test setup"),
                Evaluate=false,
                HideResult=false);
  parameter Real T_adsorpt_der(unit="K/s") = 0
    "Prescriped sloped of equilibrium temperature to test isotherm model"
    annotation (Dialog(tab="General", group="Test setup"),
                Evaluate=false,
                HideResult=false);
  parameter Real[no_components-1] y_i_der(each unit="mol/(mol.s)")=
    fill(0, no_components-1)
    "Prescriped sloped of mole fractions of independent components in the gas or
    vapor phase to test isotherm model"
    annotation (Dialog(tab="General", group="Test setup"),
                Evaluate=false,
                HideResult=false);
  parameter Modelica.Units.SI.Pressure p_adsorpt_start = 0
    "Start value of equilibrium pressure"
    annotation (Dialog(tab="General", group="Test setup"),
                Evaluate=false,
                HideResult=false);
  parameter Modelica.Units.SI.MoleFraction[no_components-1] y_i_start=
    fill(1/no_components, no_components-1)
    "Start value of mole fractions of independent components in the gas or vapor
    phase (sum must <= 1)"
    annotation (Dialog(tab="General", group="Test setup"),
                Evaluate=false,
                HideResult=false);
  parameter Modelica.Units.SI.Temperature T_adsorpt_start = 298.15
    "Start value of equilibrium temperature"
    annotation (Dialog(tab="General", group="Test setup"),
                Evaluate=false,
                HideResult=false);

  parameter Boolean print_asserts = true
    "= true, if assertations shall be printed; otherwise, no assertations are printed"
    annotation (Dialog(tab="General", group="Checks"),
                Evaluate=true,
                HideResult=true);
  parameter Boolean check_func_p_xyT = true
    "= true, if function 'func_p_xyT' is checked; otherwise, function is not checked"
    annotation (Dialog(tab="General", group="Checks"),
                Evaluate=true,
                HideResult=true);
  parameter Boolean check_func_y_pxT = true
    "= true, if function 'func_y_pxT' is checked; otherwise, function is not checked"
    annotation (Dialog(tab="General", group="Checks"),
                Evaluate=true,
                HideResult=true);
  parameter Boolean check_func_py_xT = true
    "= true, if function 'func_py_xT' is checked; otherwise, function is not checked"
    annotation (Dialog(tab="General", group="Checks"),
                Evaluate=true,
                HideResult=true);
  parameter Boolean check_func_dx_dp = true
    "= true, if function 'func_dx_dp' is checked; otherwise, function is not checked"
    annotation (Dialog(tab="General", group="Checks"),
                Evaluate=true,
                HideResult=true);
  parameter Boolean check_func_dx_dy = true
    "= true, if function 'func_dx_dy' is checked; otherwise, function is not checked"
    annotation (Dialog(tab="General", group="Checks"),
                Evaluate=true,
                HideResult=true);
  parameter Boolean check_func_dx_dT = true
    "= true, if function 'func_dx_dT' is checked; otherwise, function is not checked"
    annotation (Dialog(tab="General", group="Checks"),
                Evaluate=true,
                HideResult=true);

  replaceable package IsothermModel =
    SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.IAST_N2
    constrainedby
    SorpLib.Media.Functions.SorptionEquilibria.Interfaces.PartialMultiComponentsIAST
    "Package providing all functions of the IAST isotherm model"
    annotation (Dialog(tab="General", group="Isotherm Model"),
                choicesAllMatching=true);
  parameter Integer no_components = 1
    "Number of components of the isotherm model"
    annotation (Dialog(tab="General", group="Isotherm Model"),
                Evaluate=true,
                HideResult=true);
  parameter Modelica.Units.SI.MolarMass[no_components] M_i = fill(1, no_components)
    "Molar masses of the components"
    annotation (Dialog(tab="General", group="Isotherm Model"),
                Evaluate=true,
                HideResult=true);

  replaceable package IsothermModelComponent1 =
    SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Henry
    constrainedby
    SorpLib.Media.Functions.SorptionEquilibria.Interfaces.PartialPureComponents
    "Package providing all functions of the isotherm model of component 1"
    annotation (Dialog(tab="General", group="Isotherm Model"),
                choicesAllMatching=true);
  parameter Integer no_coefficients_1 = 1
    "Number of coefficients of the first isotherm model"
    annotation (Dialog(tab="General", group="Isotherm Model"),
                Evaluate=true,
                HideResult=true);

  replaceable package IsothermModelComponent2 =
    SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Henry
    constrainedby
    SorpLib.Media.Functions.SorptionEquilibria.Interfaces.PartialPureComponents
    "Package providing all functions of the isotherm model of component 2"
    annotation (Dialog(tab="General", group="Isotherm Model"),
                choicesAllMatching=true);
  parameter Integer no_coefficients_2 = 1
    "Number of coefficients of the second isotherm model"
    annotation (Dialog(tab="General", group="Isotherm Model"),
                Evaluate=true,
                HideResult=true);

  parameter SorpLib.Media.Functions.SorptionEquilibria.Records.NumericsIAST num
    "Record definining numerics of the IAST algorithms"
    annotation (Dialog(tab="Numerics", group="IAST"),
                choicesAllMatching=true);
  parameter Modelica.Units.SI.PressureDifference dp = 1e-4
    "Pressure difference used to calculate partial derivatives w.r.t. pressure"
    annotation (Dialog(tab="Numerics", group="IAST"));
  parameter Real dy = 1e-4
    "Mole fraction difference used to calculate partial derivatives w.r.t. independent
    mole fractions"
    annotation (Dialog(tab="Numerics", group="IAST"));
  parameter Modelica.Units.SI.TemperatureDifference dT = 1e-4
    "Temperature difference used to calculate partial derivatives w.r.t. temperature"
    annotation (Dialog(tab="Numerics", group="IAST"));
  parameter SorpLib.Media.Functions.SorptionEquilibria.Records.NumericsIAST_PureComponents num_comp_1
    "Record definining numerics of the first component's isotherm model"
    annotation (Dialog(tab="Numerics", group="Components"),
                choicesAllMatching=true);
  parameter SorpLib.Media.Functions.SorptionEquilibria.Records.NumericsIAST_PureComponents num_comp_2
    "Record definining numerics of the second component's isotherm model"
    annotation (Dialog(tab="Numerics", group="Components"),
                choicesAllMatching=true);

  //
  // Definition of variables
  //
  Modelica.Units.SI.Pressure p_adsorpt(start=p_adsorpt_start, fixed=true)
    "Equilibrium pressure";
  Modelica.Units.SI.MoleFraction[no_components-1] y_i(start=y_i_start, each fixed=true)
    "Independent mole fractions";
  Modelica.Units.SI.Temperature T_adsorpt(start=T_adsorpt_start, fixed=true)
    "Equilibrium temperature";

  Modelica.Units.SI.Pressure p_adsorpt_inv_xyT
    "Equilibrium pressure calculated via inverse function 'p_xyT'";
  Modelica.Units.SI.Pressure p_adsorpt_inv_xT
    "Equilibrium pressure calculated via inverse function 'p_xT'";

  Modelica.Units.SI.MoleFraction[no_components-1] y_i_inv_pxT(each min=-1)
    "Independent mole fractions calculated via inverse function 'p_pxT'";
  Modelica.Units.SI.MoleFraction[no_components-1] y_i_inv_xT(each min=-1)
    "Independent mole fractions calculated via inverse function 'p_xT'";

  SorpLib.Units.Uptake[no_components] x_adsorpt
    "Equilibrium uptakes";

  SorpLib.Units.DerUptakeByPressure[no_components] dx_adsorpt_dp_adsorpt
    "Partial derivative of the uptakes w.r.t. pressure";
  SorpLib.Units.DerUptakeByMolarFraction[no_components,no_components-1] dx_adsorpt_dy_i
    "Partial derivatives of the uptakes w.r.t. the molar fractions of independent 
    gas phase components";
  SorpLib.Units.DerUptakeByTemperature[no_components] dx_adsorpt_dT_adsorpt
    "Partial derivatives of the uptakes w.r.t. temperature";

  Real[no_coefficients_1] c_1
    "Coefficients of the first isotherm model";
  Real[no_coefficients_2] c_2
    "Coefficients of the second isotherm model";

protected
  Real[no_coefficients_1] c_pdT_1
    "Coefficients of the first isotherm model: T + 1e-6 K";
  Real[no_coefficients_1] c_mdT_1
    "Coefficients of the first isotherm model: T - 1e-6 K";

  Real[no_coefficients_2] c_pdT_2
    "Coefficients of the second isotherm model: T + 1e-6 K";
  Real[no_coefficients_2] c_mdT_2
    "Coefficients of the second isotherm model: T - 1e-6 K";

equation
  der(p_adsorpt) = p_adsorpt_der
    "Predecsriped slope of p_adsorpt to demonstrate the isotherm model";
  der(y_i) = y_i_der
    "Predecsriped slope of y_i to demonstrate the isotherm model";
  der(T_adsorpt) = T_adsorpt_der
    "Predecsriped slope of T_adsorpt to demonstrate the isotherm model";

  //
  // Definition of assertions: Check thermodynamics and numerical implementation
  // of isotherm model
  //
  assert(1-sum(y_i) >= 0 and 1-sum(y_i) <= 1,
    "Sum of mole fractions must be 1 mol/mol!",
    level = AssertionLevel.error);

  if print_asserts then
    if check_func_p_xyT then
      assert(abs(p_adsorpt-p_adsorpt_inv_xyT) < 1e-6,
        "Inverse function of isotherm model is not valid: Deviation (|" +
        String(abs(p_adsorpt-p_adsorpt_inv_xyT)) +
        "|) is greater than 1e-6 Pa!",
        level = AssertionLevel.warning);
    end if;

    if check_func_y_pxT then
      for ind_comp in 1:no_components-1 loop
        assert(abs(y_i[ind_comp]-y_i_inv_pxT[ind_comp]) < 1e-6,
          "Inverse function of isotherm model is not valid: For component " +
          String(ind_comp) +
          ", deviation (|" +
          String(abs(y_i[ind_comp]-y_i_inv_pxT[ind_comp])) +
          "|) is greater than 1e-6 mol/mol!",
          level = AssertionLevel.warning);
      end for;
    end if;

    if check_func_py_xT then
      assert(abs(p_adsorpt-p_adsorpt_inv_xT) < 1e-6,
        "Inverse function of isotherm model is not valid: Deviation (|" +
        String(abs(p_adsorpt-p_adsorpt_inv_xT)) +
        "|) is greater than 1e-6 Pa!",
        level = AssertionLevel.warning);

      for ind_comp in 1:no_components-1 loop
        assert(abs(y_i[ind_comp]-y_i_inv_xT[ind_comp]) < 1e-6,
          "Inverse function of isotherm model is not valid: For component " +
          String(ind_comp) +
          ", deviation (|" +
          String(abs(y_i[ind_comp]-y_i_inv_xT[ind_comp])) +
          "|) is greater than 1e-6 mol/mol!",
          level = AssertionLevel.warning);
      end for;
    end if;
  end if;

  //
  // Annotations
  //
  annotation (Documentation(revisions="<html>
<ul>
  <li>
  November 10, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>", info="<html>
<p>
This partial model is the basic model for all testers of isotherm models 
based on the ideal adsorbed solution theory (IAST) describing the adsorption 
of multi components.
<br/><br/>
This partial model defines all relevant parameters, variables, and equations to
check (1) the classical form of isotherm models, (2) the inverse forms of isotherm
models, (3) the partial derivatives of the uptakes w.r.t. to pressure, (4) the 
partial derivatives of the uptakes w.r.t. mole fractions of independent components
in the gas or vapor phase, and (5) the partial derivatives of the uptake w.r.t. 
temperature.
<br/><br/>
Models that inherit properties from this partial model have to specify the number 
of components <i>no_components</i> and number of coefficients for each pure 
component isotherm model <i>no_coefficients_i</i>, respectively, and have to redeclare 
all functions of the multi and pure component isotherm models <i>func_i</i>. 
Besides, the coefficients of each isotherm model (i.e., <i>c_i</i>, <i>c_pdT_i</i>, 
and <i>c_mdT_i</i>) have to be implemented. Additionally, it has to be specified 
whether the equilibrium pressure (<i>p_adsorpt_der</i>), the mole fractions of
independent components in the gas or vapor phase (<i>y_i_der</i>), or the 
equilibrium temperature (<i>T_adsorpt_der</i>) changes with time. Finally, the
algorithm has to be complemented such that the functions of the multi component
isotherm model <i>func_i</i> are used.
<br/><br/>
Note that the parameters of this model only allow to check the IAST for two
components. To check the IAST for more than two components, further paramters
have to be added.
</p>
</html>"));
end PartialTestMultiIAST;
