within SorpLib.Media.Functions.SorptionEquilibria.BaseClasses;
partial function PartialMulti_x_pyT
  "Base function for isotherm models of multi components: Uptakes as function of pressure, mole fractions of independent gas phase components, and temperature"
  extends SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialMulti;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.Pressure p_adsorpt
    "Equilibrium pressure of the adsorpt phase"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.MoleFraction[size(c,2)-1] y_i
    "Mole fractions of independent components in the vapor or gas phase"
    annotation (Dialog(tab="General", group="Inputs"));

  input Modelica.Units.SI.Pressure p_adsorpt_lb_start = 1
    "Lower bound of equilibrium pressure (required if pressure is calculated 
    numerically (i.e., root finding))"
    annotation (Dialog(tab="General", group="Numerical inputs"));
  input Modelica.Units.SI.Pressure p_adsorpt_ub_start = 10
    "Upper bound of equilibrium pressure (required if pressure is calculated 
    numerically (i.e., root finding))"
    annotation (Dialog(tab="General", group="Numerical inputs"));
  input Real tolerance = 100*Modelica.Constants.eps
    "Tolerance for numerical calculation (required if pressure is calculated
    numerically (i.e., root finding))"
    annotation (Dialog(tab="General", group="Numerical inputs"));

  //
  // Definition of outputs
  //
  output SorpLib.Units.Uptake[size(c,2)] x_adsorpt
    "Equilibrium uptakes of the adsorpt phase"
    annotation (Dialog(tab="General", group="Outputs", enable=false));

  //
  // Definition of variables
  //
protected
  Modelica.Units.SI.MoleFraction[size(c,2)] y_i_ = cat(1, y_i, {1-sum(y_i)})
    "Mole fractions of all components in the vapor or gas phase";
  Modelica.Units.SI.Pressure[size(c,2)] p_i = p_adsorpt .* y_i_
    "Partial pressures of all components";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial function is the basic function for all isotherm models describing 
the adsorption of multi components. Such models can be extended pure component 
isotherm models or specifically developed multi component isotherm models. 
<br/><br/>
This partial function is the basic function for calculating the equilibrium uptakes 
<i>x_adsorpt</i> as a function of the equilibrium pressure <i>p_adsorpt</i>, the
mole fractions of the independent components in the gas or vapor phase <i>y_i</i>, 
the equilibrium temperature <i>T_adsorpt</i>. Further inputs are the coefficients of 
the isotherm model <i>c</i>. The coefficients of the isotherm model <i>c</i> may 
depend on the equilibrium temperature <i>T_adsorpt</i>. The coefficients of the 
isotherm model <i>c</i> are a matrix: The columns contain the coefficients of the 
different components. The rows contain the actual coefficients, i.e, the row size 
depends on the selected isotherm model. Different components may require different 
numbers of coefficients, which is why the row size depends on the component with 
the most coefficients. Optional input regarding numerics is the threshold 
<i>p_threshold_min</i> to regulate the minimal values of all partial pressures. 
Further optional inputs regarding numerics are the lower bound (<i>p_adsorpt_lb_start</i>) 
and upper bound (<i>p_adsorpt_ub_start</i>) of the equilibrium pressure and the 
tolerance (<i>tolerance</i>), only required if the inverse of this function cannot 
be solved analytically.
<br/><br/>
The equilibrium uptakes <i>x_adsorpt</i> are defined as the output.
<br/><br/>
Functions that inherit properties from this partial function may have to implement
further inputs and the function algorithm.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 7, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end PartialMulti_x_pyT;
