within SorpLib.Media.Functions.SorptionEquilibria.BaseClasses;
partial function PartialMulti
  "Base function for isotherm models of multi components"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Real[:,:] c
    "Coefficients of the isotherm model"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.Temperature T_adsorpt
    "Equilibrium temperature of the adsorpt phase"
    annotation (Dialog(tab="General", group="Inputs"));

  input Modelica.Units.SI.Pressure p_threshold_min = 0
    "Threshold for partial pressures of all components: If a partial pressure is
    below the threshold, its value is set to the threshold"
    annotation (Dialog(tab="General", group="Numerical inputs"));

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial function is the basic function for all isotherm models describing 
the adsorption of multi components. Such models can be extended pure component 
isotherm models or specifically developed multi component isotherm models. 
<br/><br/>
This partial function defines the equilibrium temperature <i>T_adsorpt</i> and the 
coefficients of the isotherm model <i>c</i> as inputs. The coefficients of the 
isotherm model <i>c</i> are a matrix: The columns contain the coefficients of the 
different components. The rows contain the actual coefficients, i.e, the row size 
depends on the selected isotherm model. Different components may require different 
numbers of coefficients, which is why the row size depends on the component with 
the most coefficients. The coefficients of the isotherm model <i>c</i> may depend 
on the equilibrium temperature <i>T_adsorpt</i>. Optional input regarding numerics 
is the threshold <i>p_threshold_min</i> to regulate the minimal values of all 
partial pressures.
<br/><br/>
Functions that inherit properties from this partial function may have to 
implement further inputs, the output, and the function algorithm.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 7, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end PartialMulti;
