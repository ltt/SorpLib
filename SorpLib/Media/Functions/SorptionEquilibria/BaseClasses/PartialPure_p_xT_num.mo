within SorpLib.Media.Functions.SorptionEquilibria.BaseClasses;
partial function PartialPure_p_xT_num
  "Base function for isotherm models of pure components: Pressure as function of uptake and temperature (numerical solution)"
  extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_p_xT;

  //
  // Definition of protected replacable functions
  //
protected
  replaceable function func_x_pT =
    SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininAstakhov.x_pT
    constrainedby
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_x_pT
    "Calculates uptake as function of pressure and temperature"
    annotation (Dialog(tab="General", group="Functions"));

  //
  // Definition of functions
  //
protected
  function func_p_num
    "Function used to find root (i.e., p_adsorpt) numerically"
    extends Modelica.Math.Nonlinear.Interfaces.partialScalarFunction;
    input SorpLib.Units.Uptake x_adsorpt
      "Equilibrium uptake";
    input Modelica.Units.SI.Temperature T_adsorpt
      "Equilibrium temperature";
    input Real c[:]
      "Coefficients of isotherm model";
  algorithm
    y := func_x_pT(p_adsorpt=u, T_adsorpt=T_adsorpt, c=c,
      p_adsorpt_lb_start=1, p_adsorpt_ub_start=10,
      tolerance=100*Modelica.Constants.eps) - x_adsorpt
     "Function '0 = x(p_adsorpt, T_adsorpt, c) - x_adsorpt' used to find root 
      (i.e., p_adsorpt) numerically";
  end func_p_num;

  //
  // Definition of variables
  //
  Boolean bound_ok = false
    "= true, if bounds are found such that func_p_num(lb) < 0 and func_p_num(ub) > 0";

  Modelica.Units.SI.Pressure p_adsorpt_lb = p_adsorpt_lb_start
    "Current best lower bound of equilibrium pressure";
  Modelica.Units.SI.Pressure p_adsorpt_ub = p_adsorpt_ub_start
    "Current best upper bound of equilibrium pressure";

  SorpLib.Units.Uptake x_adsorpt_lb = func_x_pT(
    p_adsorpt=p_adsorpt_lb, T_adsorpt=T_adsorpt, c=c,
    p_adsorpt_lb_start=1, p_adsorpt_ub_start=10,
    tolerance=100*Modelica.Constants.eps)
    "Equilibrium uptake at current best lower bound of equilibrium pressure";
  SorpLib.Units.Uptake x_adsorpt_ub = func_x_pT(
    p_adsorpt=p_adsorpt_ub, T_adsorpt=T_adsorpt, c=c,
    p_adsorpt_lb_start=1, p_adsorpt_ub_start=10,
    tolerance=100*Modelica.Constants.eps)
    "Equilibrium uptake at current best upper bound of equilibrium pressure";

algorithm
  //
  // Find start values such that func_p_num(lb) < 0 and func_p_num(ub) > 0
  //
  // Reducing p_adsorpt reduces x_adsorpt -> func_p_num(p_adsorpt) can become < 0
  // Increasing p_adsorpt increases x_adsorpt -> func_p_num(p_adsorpt) can become > 0
  //
  while not bound_ok loop
    if Modelica.Math.isEqual(s1=x_adsorpt_lb, s2=x_adsorpt, eps=tolerance) then
      p_adsorpt := p_adsorpt_lb;
      bound_ok := true;
      return;

    elseif Modelica.Math.isEqual(s1=x_adsorpt_ub, s2=x_adsorpt, eps=tolerance) then
      p_adsorpt := p_adsorpt_ub;
      bound_ok := true;
      return;

    elseif x_adsorpt_lb-x_adsorpt < 0 and x_adsorpt_ub-x_adsorpt < 0 then
      p_adsorpt_lb := p_adsorpt_ub;
      x_adsorpt_lb := x_adsorpt_ub;

      p_adsorpt_ub := p_adsorpt_ub*10;
      x_adsorpt_ub := func_x_pT(p_adsorpt=p_adsorpt_ub, T_adsorpt=T_adsorpt, c=c,
        p_adsorpt_lb_start=1, p_adsorpt_ub_start=10,
        tolerance=100*Modelica.Constants.eps);

    elseif x_adsorpt_lb-x_adsorpt > 0 and x_adsorpt_ub-x_adsorpt > 0 then
      p_adsorpt_ub := p_adsorpt_lb;
      x_adsorpt_ub := x_adsorpt_lb;

      p_adsorpt_lb := if p_adsorpt_lb > Modelica.Constants.small then
        p_adsorpt_lb*0.1 else 0;
      x_adsorpt_lb := func_x_pT(p_adsorpt=p_adsorpt_lb, T_adsorpt=T_adsorpt, c=c,
        p_adsorpt_lb_start=1, p_adsorpt_ub_start=10,
        tolerance=100*Modelica.Constants.eps);

    else
      bound_ok := true;

    end if;
  end while;

  //
  // Find root in the interval lb <= root <= up
  //
  p_adsorpt := Modelica.Math.Nonlinear.solveOneNonlinearEquation(
    f = function func_p_num(
      x_adsorpt=x_adsorpt,
      T_adsorpt=T_adsorpt,
      c=c),
    u_min=p_adsorpt_lb,
    u_max=p_adsorpt_ub,
    tolerance=tolerance)
    "Calculation of the equilibrium pressure of the adsorpt phase";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial function is the basic function for all isotherm models describing 
the adsorption of pure components. Such models can be classical isotherm models 
based on a (plain) boundary surface or isotherm models based on filled pores.
<br/><br/>
This partial function is the basic function for calculating the equilibrium 
pressure <i>p_adsorpt</i> as a function of the equilibrium uptake <i>x_adsorpt</i> 
and equilibrium temperature <i>T_adsorpt</i>. Defined inputs are the equilibrium 
uptake <i>x_adsorpt</i>, the equilibrium temperature <i>T_adsorpt</i>, and the 
coefficients of the isotherm model <i>c</i> as inputs. The coefficients of the
isotherm model <i>c</i> may depend on the equilibrium temperature <i>T_adsorpt</i>. 
Optional inputs regarding numerics are the lower bound (<i>p_adsorpt_lb_start</i>) 
and upper bound (<i>p_adsorpt_ub_start</i>) of the equilibrium pressure and the 
tolerance (<i>tolerance</i>). Besides, the equilibrium pressure <i>p_adsorpt</i> 
is defined as the output.
<br/><br/>
With this function, the equilibrium pressure <i>p_adsorpt</i> is determined 
numerically by solving a zero problem. The zero problem is as follows:
</p>
<pre>
    f(p_adsorpt) = 0 => 0 = func_x_pT(p_adsorpt=p_adsorpt, T_adsorpt=T_adsorpt, c=c) - x_adsorpt;
</pre>
<p>
The zero problem is solved using the numerically very efficient function 
<a href=\"Modelica://Modelica.Math.Nonlinear.solveOneNonlinearEquation\">Modelica.Math.Nonlinear.solveOneNonlinearEquation</a>.
For the application of this function, it must be ensured that <i>f(p_adsorpt)</i> 
has a different sign for the upper and lower limits of <i>p_adsorpt</i>. To ensure 
the different signs, the upper and lower limits are determined in an upstream loop.
<br/><br/>
Functions that inherit properties from this partial function have to redeclare
the function <i>func_x_pT</i>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 1, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end PartialPure_p_xT_num;
