within SorpLib.Media.Functions.SorptionEquilibria.BaseClasses;
partial model PartialTestMulti
  "Base model for testers of isotherm models describing multi component adsorption"
  extends Modelica.Icons.Example;

  //
  // Definition of parameters
  //
  parameter Real p_adsorpt_der(unit="Pa/s") = 1
    "Prescriped sloped of equilibrium pressure to test isotherm model"
    annotation (Dialog(tab="General", group="Test setup"),
                Evaluate=false,
                HideResult=false);
  parameter Real T_adsorpt_der(unit="K/s") = 0
    "Prescriped sloped of equilibrium temperature to test isotherm model"
    annotation (Dialog(tab="General", group="Test setup"),
                Evaluate=false,
                HideResult=false);
  parameter Real[no_components-1] y_i_der(each unit="mol/(mol.s)")=
    fill(0, no_components-1)
    "Prescriped sloped of mole fractions of independent components in the gas or
    vapor phase to test isotherm model"
    annotation (Dialog(tab="General", group="Test setup"),
                Evaluate=false,
                HideResult=false);
  parameter Modelica.Units.SI.Pressure p_adsorpt_start = 0
    "Start value of equilibrium pressure"
    annotation (Dialog(tab="General", group="Test setup"),
                Evaluate=false,
                HideResult=false);
  parameter Modelica.Units.SI.MoleFraction[no_components-1] y_i_start=
    fill(1/no_components, no_components-1)
    "Start value of mole fractions of independent components in the gas or vapor
    phase (sum must <= 1)"
    annotation (Dialog(tab="General", group="Test setup"),
                Evaluate=false,
                HideResult=false);
  parameter Modelica.Units.SI.Temperature T_adsorpt_start = 298.15
    "Start value of equilibrium temperature"
    annotation (Dialog(tab="General", group="Test setup"),
                Evaluate=false,
                HideResult=false);

  parameter Boolean print_asserts = true
    "= true, if assertations shall be printed; otherwise, no assertations are printed"
    annotation (Dialog(tab="General", group="Checks"),
                Evaluate=true,
                HideResult=true);
  parameter Boolean check_func_p_xyT = true
    "= true, if function 'func_p_xyT' is checked; otherwise, function is not checked"
    annotation (Dialog(tab="General", group="Checks"),
                Evaluate=true,
                HideResult=true);
  parameter Boolean check_func_y_pxT = true
    "= true, if function 'func_y_pxT' is checked; otherwise, function is not checked"
    annotation (Dialog(tab="General", group="Checks"),
                Evaluate=true,
                HideResult=true);
  parameter Boolean check_func_py_xT = true
    "= true, if function 'func_py_xT' is checked; otherwise, function is not checked"
    annotation (Dialog(tab="General", group="Checks"),
                Evaluate=true,
                HideResult=true);
  parameter Boolean check_func_dx_dp = true
    "= true, if function 'func_dx_dp' is checked; otherwise, function is not checked"
    annotation (Dialog(tab="General", group="Checks"),
                Evaluate=true,
                HideResult=true);
  parameter Boolean check_func_dx_dy = true
    "= true, if function 'func_dx_dy' is checked; otherwise, function is not checked"
    annotation (Dialog(tab="General", group="Checks"),
                Evaluate=true,
                HideResult=true);
  parameter Boolean check_func_dx_dT = true
    "= true, if function 'func_dx_dT' is checked; otherwise, function is not checked"
    annotation (Dialog(tab="General", group="Checks"),
                Evaluate=true,
                HideResult=true);

  replaceable package IsothermModel =
    SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.Langmuir
    constrainedby
    SorpLib.Media.Functions.SorptionEquilibria.Interfaces.PartialMultiComponents
    "Package providing all functions of the isotherm model"
    annotation (Dialog(tab="General", group="Isotherm Model"),
                choicesAllMatching=true);
  parameter Integer no_components = 1
    "Number of components of the isotherm model"
    annotation (Dialog(tab="General", group="Isotherm Model"),
                Evaluate=true,
                HideResult=true);
  parameter Integer no_coefficients = 1
    "Number of coefficients of the isotherm model (i.e., highest number among 
    different components)"
    annotation (Dialog(tab="General", group="Isotherm Model"),
                Evaluate=true,
                HideResult=true);

  parameter Modelica.Units.SI.Pressure p_threshold_min = 0
    "Threshold for partial pressures of all components: If a partial pressure is
    below the threshold, its value is set to the threshold"
    annotation (Dialog(tab="Numerics", group="Limiter"),
                Evaluate=true,
                HideResult=true);

  parameter Modelica.Units.SI.PressureDifference dp = 1e-6
    "Pressure difference used to calculate partial derivatives numerically"
    annotation (Dialog(tab="Numerics", group="Derivatives"),
                Evaluate=true,
                HideResult=true);
  parameter Real dy = 1e-6
    "Mole fraction difference used to calculate partial derivatives numerically"
    annotation (Dialog(tab="Numerics", group="Derivatives"),
                Evaluate=true,
                HideResult=true);
  parameter Modelica.Units.SI.TemperatureDifference dT = 1e-6
    "Temperature difference used to calculate partial derivatives numerically"
    annotation (Dialog(tab="Numerics", group="Derivatives"),
                Evaluate=true,
                HideResult=true);

  //
  // Definition of variables
  //
  Modelica.Units.SI.Pressure p_adsorpt(start=p_adsorpt_start, fixed=true)
    "Equilibrium pressure";
  Modelica.Units.SI.MoleFraction[no_components-1] y_i(start=y_i_start, each fixed=true)
    "Independent mole fractions";
  Modelica.Units.SI.Temperature T_adsorpt(start=T_adsorpt_start, fixed=true)
    "Equilibrium temperature";

  Modelica.Units.SI.Pressure p_adsorpt_inv_xyT
    "Equilibrium pressure calculated via inverse function 'p_xyT'";
  Modelica.Units.SI.Pressure p_adsorpt_inv_xT
    "Equilibrium pressure calculated via inverse function 'p_xT'";

  Modelica.Units.SI.MoleFraction[no_components-1] y_i_inv_pxT(each min=-1)
    "Independent mole fractions calculated via inverse function 'p_pxT'";
  Modelica.Units.SI.MoleFraction[no_components-1] y_i_inv_xT(each min=-1)
    "Independent mole fractions calculated via inverse function 'p_xT'";

  SorpLib.Units.Uptake[no_components] x_adsorpt
    "Equilibrium uptakes";

  SorpLib.Units.DerUptakeByPressure[no_components] dx_adsorpt_dp_adsorpt
    "Partial derivative of the uptakes w.r.t. pressure at constant mole fractions
    and temperature";
  SorpLib.Units.DerUptakeByPressure[no_components] dx_adsorpt_dp_adsorpt_num
    "Partial derivative of the uptakes w.r.t. pressure at constant mole fractions
    and temperature calculated numerically";

  SorpLib.Units.DerUptakeByMolarFraction[no_components,no_components-1] dx_adsorpt_dy_i
    "Partial derivatives of the uptakes w.r.t. the molar fractions of independent 
    gas phase components at constant pressure and temperature";
  SorpLib.Units.DerUptakeByMolarFraction[no_components,no_components-1] dx_adsorpt_dy_i_num
    "Partial derivatives of the uptakes w.r.t. the molar fractions of independent 
    gas phase components at constant pressure and temperature calculated numerically";

  SorpLib.Units.DerUptakeByTemperature[no_components] dx_adsorpt_dT_adsorpt
    "Partial derivatives of the uptakes w.r.t. temperature at constant pressure 
    and mole fractions";
  SorpLib.Units.DerUptakeByTemperature[no_components] dx_adsorpt_dT_adsorpt_num
    "Partial derivatives of the uptakes w.r.t. temperature at constant pressure 
    and mole fractions calculated numerically";

  Real[no_coefficients,no_components] c
    "Coefficients of isotherm model";
  Real[no_coefficients,no_components] dc_dT
    "Partial derivative of coefficients of isotherm model w.r.t. temperature";

protected
  Real[no_coefficients,no_components] c_pdT
    "Coefficients of isotherm model: T + 1e-6 K";
  Real[no_coefficients,no_components] c_mdT
    "Coefficients of isotherm model: T - 1e-6 K";

  function dx_dy_num
    "Calculates partial derivatives of equilibrium uptakes w.r.t. independent mole
    fractions numericalls at constant pressure and temperature"

    //
    // Note that this function is a workaround for OpenModelica because OpenModelica
    // apparently cannot handle the assignment of a vector to the column of a
    // matrix.
    //

    //
    // Definition of inputs
    //
    input Real[:,:] c
      "Coefficients of isotherm model";
    input Real p_adsorpt
      "Equilibrium pressure";
    input Real[size(c,2)-1] y_i
      "Independent mole fractions";
    input Real T_adsorpt
      "Equilibrium temperature";
    input Real p_threshold_min
      "Threshold for partial pressures of all components: If a partial pressure is
      below the threshold, its value is set to the threshold";
    input Real dy
      "Mole fraction difference used to calculate partial derivatives numerically";

    //
    // Definition of outputs
    //
    output SorpLib.Units.DerUptakeByMolarFraction[size(c,2),size(c,2)-1] dx_adsorpt_dy_i_num
      "Partial derivatives of the uptakes w.r.t. the molar fractions of independent 
      gas phase components calculated numerically";

    //
    // Definition of variables
    //
  protected
    SorpLib.Units.DerUptakeByMolarFraction[size(c,2)] dx_adsorpt_dy_i_num_tmp
      "Temporary partial derivatives of the uptakes w.r.t. the molar fraction 
       of independent gas phase component at constant pressure and temperature
       calculated numerically";

  algorithm
    for ind_y_i in 1:size(c,2)-1 loop
      //
      // Calculate partial darivatives for each independent mole fraction
      //
      dx_adsorpt_dy_i_num_tmp :=(IsothermModel.x_pyT(
        p_adsorpt=p_adsorpt,
        y_i=cat(1, y_i[1:ind_y_i - 1], {y_i[ind_y_i] + dy}, y_i[ind_y_i + 1:size(c,2) - 1]),
        T_adsorpt=T_adsorpt,
        c=c,
        p_threshold_min=p_threshold_min) .-
        IsothermModel.x_pyT(
        p_adsorpt=p_adsorpt,
        y_i=cat(1, y_i[1:ind_y_i - 1], {y_i[ind_y_i] - dy}, y_i[ind_y_i + 1:size(c,2) - 1]),
        T_adsorpt=T_adsorpt,
        c=c,
        p_threshold_min=p_threshold_min)) ./ (2*dy)
        "Temporary partial derivative of the uptake w.r.t. independent molar  
        fraction at constant pressure and temeprature calculated numerically";

        //
        // Assign temporary results to output matrix
        //
        for ind_com in 1:size(c,2) loop
          dx_adsorpt_dy_i_num[ind_com,ind_y_i] := dx_adsorpt_dy_i_num_tmp[ind_com]
            "Partial derivatives of the uptakes w.r.t. the molar fractions of  
            independent gas phase components at contant pressure and temperature
            calculated numerically";
        end for;
    end for;
  end dx_dy_num;

equation
  der(p_adsorpt) = p_adsorpt_der
    "Predecsriped slope of p_adsorpt to demonstrate the isotherm model";
  der(y_i) = y_i_der
    "Predecsriped slope of y_i to demonstrate the isotherm model";
  der(T_adsorpt) = T_adsorpt_der
    "Predecsriped slope of T_adsorpt to demonstrate the isotherm model";

  if check_func_p_xyT then
    p_adsorpt_inv_xyT = IsothermModel.p_xyT(x_adsorpt=x_adsorpt, y_i=y_i, T_adsorpt=T_adsorpt,
      c=c, p_threshold_min=p_threshold_min)
      "Equilibrium pressure calculated via the inverse function 'p_xyT'";
  else
    p_adsorpt_inv_xyT = -1
      "Equilibrium pressure calculated via the inverse function 'p_xyT'";
  end if;

  if check_func_y_pxT then
    y_i_inv_pxT = IsothermModel.y_pxT(p_adsorpt=p_adsorpt, x_adsorpt=x_adsorpt, T_adsorpt=T_adsorpt,
      c=c, p_threshold_min=p_threshold_min)
      "Independent mole fractions calculated via the inverse function 'y_pxT'";
  else
    y_i_inv_pxT = fill(-1, no_components-1)
      "Independent mole fractions calculated via the inverse function 'y_pxT'";
  end if;

  if check_func_py_xT then
    (p_adsorpt_inv_xT, y_i_inv_xT) =
      IsothermModel.py_xT(x_adsorpt=x_adsorpt, T_adsorpt=T_adsorpt,
      c=c, p_threshold_min=p_threshold_min)
      "Independent mole fractions calculated via the inverse function 'y_xT'";
  else
    p_adsorpt_inv_xT = -1
      "Equilibrium pressure calculated via the inverse function 'p_xT'";
    y_i_inv_xT = fill(-1, no_components-1)
      "Independent mole fractions calculated via the inverse function 'y_xT'";
  end if;

  x_adsorpt = IsothermModel.x_pyT(p_adsorpt=p_adsorpt, y_i=y_i, T_adsorpt=T_adsorpt,
    c=c, p_threshold_min=p_threshold_min)
    "Equilibrium uptakes";

  if check_func_dx_dp then
    dx_adsorpt_dp_adsorpt = IsothermModel.dx_dp(
      p_adsorpt=p_adsorpt, y_i=y_i, T_adsorpt=T_adsorpt,
      c=c, p_threshold_min=p_threshold_min)
      "Partial derivative of the uptake w.r.t. pressure at constant mole fractions 
      and temeprature";
    dx_adsorpt_dp_adsorpt_num = (
      IsothermModel.x_pyT(p_adsorpt=p_adsorpt+dp, y_i=y_i, T_adsorpt=T_adsorpt,
      c=c, p_threshold_min=p_threshold_min) .-
      IsothermModel.x_pyT(p_adsorpt=p_adsorpt-dp, y_i=y_i, T_adsorpt=T_adsorpt,
      c=c, p_threshold_min=p_threshold_min)) ./
      (2*dp)
      "Partial derivative of the uptake w.r.t. pressure at constant mole fractions 
      and temeprature calculated numerically";
  else
    dx_adsorpt_dp_adsorpt = fill(-1, no_components)
      "Partial derivative of the uptake w.r.t. pressure at constant mole fractions 
      and temeprature";
    dx_adsorpt_dp_adsorpt_num = fill(-1, no_components)
      "Partial derivative of the uptake w.r.t. pressure at constant mole fractions 
      and temeprature calculated numerically";
  end if;

  if check_func_dx_dy then
    dx_adsorpt_dy_i = IsothermModel.dx_dy(
      p_adsorpt=p_adsorpt, y_i=y_i, T_adsorpt=T_adsorpt,
      c=c, p_threshold_min=p_threshold_min)
      "Partial derivative of the uptake w.r.t. independent molar fractions at 
      constant pressure and temperature";

    dx_adsorpt_dy_i_num = dx_dy_num(
      p_adsorpt=p_adsorpt, y_i=y_i, T_adsorpt=T_adsorpt, c=c,
      p_threshold_min=p_threshold_min,dy=dy)
      "Partial derivative of the uptake w.r.t. temperature at constant pressure 
      and temperature calculated numerically";
  else
    dx_adsorpt_dy_i = fill(-1, no_components, no_components-1)
      "Partial derivative of the uptake w.r.t. independent molar fractions at 
      constant pressure and temperature";
    dx_adsorpt_dy_i_num = fill(-1, no_components, no_components-1)
      "Partial derivative of the uptake w.r.t. independent molar fractions at 
      constant pressure and temperature calculated numerically";
  end if;

  if check_func_dx_dT then
    dx_adsorpt_dT_adsorpt = IsothermModel.dx_dT(
      p_adsorpt=p_adsorpt, y_i=y_i, T_adsorpt=T_adsorpt,
      c=c, dc_dT_adsorpt=dc_dT,
      p_threshold_min=p_threshold_min)
      "Partial derivative of the uptake w.r.t. temperature at constant pressure 
      and mole fractions";
    dx_adsorpt_dT_adsorpt_num = (
      IsothermModel.x_pyT(p_adsorpt=p_adsorpt, y_i=y_i, T_adsorpt=T_adsorpt+dT,
      c=c_pdT, p_threshold_min=p_threshold_min) .-
      IsothermModel.x_pyT(p_adsorpt=p_adsorpt, y_i=y_i, T_adsorpt=T_adsorpt-dT,
      c=c_mdT, p_threshold_min=p_threshold_min)) ./
      (2*dT)
      "Partial derivative of the uptake w.r.t. temperature at constant pressure 
      and mole fractions calculated numerically";
  else
    dx_adsorpt_dT_adsorpt = fill(-1, no_components)
      "Partial derivative of the uptake w.r.t. temperature at constant pressure 
      and mole fractions";
    dx_adsorpt_dT_adsorpt_num = fill(-1, no_components)
      "Partial derivative of the uptake w.r.t. temperature at constant pressure 
      and mole fractions calculated numerically";
  end if;

  //
  // Definition of assertions: Check thermodynamics and numerical implementation
  // of isotherm model
  //
  assert(1-sum(y_i) >= 0 and 1-sum(y_i) <= 1,
    "Sum of mole fractions must be 1 mol/mol!",
    level = AssertionLevel.error);

  if print_asserts then
    if check_func_p_xyT then
      assert(abs(p_adsorpt-p_adsorpt_inv_xyT) < 1e-6,
        "Inverse function of isotherm model is not valid: Deviation (|" +
        String(abs(p_adsorpt-p_adsorpt_inv_xyT)) +
        "|) is greater than 1e-6 Pa!",
        level = AssertionLevel.warning);
    end if;

    if check_func_y_pxT then
      for ind_comp in 1:no_components-1 loop
        assert(abs(y_i[ind_comp]-y_i_inv_pxT[ind_comp]) < 1e-6,
          "Inverse function of isotherm model is not valid: For component " +
          String(ind_comp) +
          ", deviation (|" +
          String(abs(y_i[ind_comp]-y_i_inv_pxT[ind_comp])) +
          "|) is greater than 1e-6 mol/mol!",
          level = AssertionLevel.warning);
      end for;
    end if;

    if check_func_py_xT then
      assert(abs(p_adsorpt-p_adsorpt_inv_xT) < 1e-6,
        "Inverse function of isotherm model is not valid: Deviation (|" +
        String(abs(p_adsorpt-p_adsorpt_inv_xT)) +
        "|) is greater than 1e-6 Pa!",
        level = AssertionLevel.warning);

      for ind_comp in 1:no_components-1 loop
        assert(abs(y_i[ind_comp]-y_i_inv_xT[ind_comp]) < 1e-6,
          "Inverse function of isotherm model is not valid: For component " +
          String(ind_comp) +
          ", deviation (|" +
          String(abs(y_i[ind_comp]-y_i_inv_xT[ind_comp])) +
          "|) is greater than 1e-6 mol/mol!",
          level = AssertionLevel.warning);
      end for;
    end if;

    if check_func_dx_dp then
      for ind_comp in 1:no_components loop
        assert(abs(dx_adsorpt_dp_adsorpt[ind_comp]-dx_adsorpt_dp_adsorpt_num[ind_comp]) < 1e-6,
          "Partial derivative of isotherm model w.r.t. pressure at constant " +
          "mole fractions and temperature is not valid: For component " +
          String(ind_comp) +
          ", deviation (|"
          + String(abs(dx_adsorpt_dp_adsorpt[ind_comp]-dx_adsorpt_dp_adsorpt_num[ind_comp])) +
          "|) is greater than 1e-6 kg/(kg.Pa)!",
          level = AssertionLevel.warning);
      end for;
    end if;

    if check_func_dx_dy then
      for ind_comp in 1:no_components loop
        for ind_y_i in 1:no_components-1 loop
          assert(abs(dx_adsorpt_dy_i[ind_comp,ind_y_i]-dx_adsorpt_dy_i_num[ind_comp,ind_y_i]) < 1e-6,
            "Partial derivative of isotherm model w.r.t. pressure at constant " +
            "pressure and temperature is not valid: For component " +
            String(ind_comp) +
            " and independent mole fraction " +
            String(ind_y_i) +
            ", deviation (|" +
            String(abs(dx_adsorpt_dy_i[ind_comp,ind_y_i]-dx_adsorpt_dy_i_num[ind_comp,ind_y_i])) +
            "|) is greater than 1e-6 kg.mol/(kg.mol)!",
            level = AssertionLevel.warning);
        end for;
      end for;
    end if;

    if check_func_dx_dT then
      for ind_comp in 1:no_components loop
        assert(abs(dx_adsorpt_dT_adsorpt[ind_comp]-dx_adsorpt_dT_adsorpt_num[ind_comp]) < 1e-6,
          "Partial derivative of isotherm model w.r.t. temperature at " +
          "constant pressure and mole fractions is not valid: For component " +
          String(ind_comp) +
          ", deviation (|" +
          String(abs(dx_adsorpt_dT_adsorpt[ind_comp]-dx_adsorpt_dT_adsorpt_num[ind_comp])) +
          "|) is greater than 1e-6 kg/(kg.K)!",
          level = AssertionLevel.warning);
      end for;
    end if;
  end if;

  //
  // Annotations
  //
  annotation (Documentation(revisions="<html>
<ul>
  <li>
  November 7, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>", info="<html>
<p>
This partial model is the basic model for all testers of isotherm models describing
adsorption of multi components.
<br/><br/>
This partial model defines all relevant parameters, variables, and equations to
check (1) the classical form of isotherm models, (2) the inverse forms of isotherm
models, (3) the partial derivatives of the uptakes w.r.t. to pressure, (4) the 
partial derivatives of the uptakes w.r.t. mole fractions of independent components
in the gas or vapor phase, and (5) the partial derivatives of the uptake w.r.t. 
temperature. All partial derivatives are also implemented as central finite 
differences to check for the analytical implementation of the partial derivatives.
<br/><br/>
Models that inherit properties from this partial model have to specify the number 
of components and coefficients of the isotherm model <i>no_components</i> and
<i>no_coefficients</i>, respectively, and have to redeclare the seven functions 
of the isotherm model <i>func_x_pyT</i>, <i>func_p_xyT</i>, <i>func_y_pxT</i>, 
<i>func_py_xT</i>, <i>func_dx_dp</i>, <i>func_dx_dy</i>, and <i>func_dx_dT</i>. 
Besides, the coefficients of the isotherm model (i.e., <i>c</i>, <i>c_pdT</i>, 
and <i>c_mdT</i>) and their partial derivatives with respect to temperature 
(i.e., <i>dc_dT</i>) have to be implemented. Additionally, it has to be specified 
whether the equilibrium pressure (<i>p_adsorpt_der</i>), the mole fractions of
independent components in the gas or vapor phase (<i>y_i_der</i>), or the 
equilibrium temperature (<i>T_adsorpt_der</i>) changes with time.
</p>
</html>"));
end PartialTestMulti;
