within SorpLib.Media.Functions.SorptionEquilibria.BaseClasses;
partial function PartialPure_p_piT
  "Base function for isotherm models of pure components: Pressure as function of reduced spreading pressure and temperature"

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.MolarMass M_adsorptive
    "Molar mass of adsorptive"
    annotation (Dialog(tab="General", group="Inputs"));
  input SorpLib.Units.ReducedSpreadingPressure pi "Reduced spreading pressure"
    annotation (Dialog(tab="General", group="Inputs"));
  extends SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure;

  input Modelica.Units.SI.Pressure p_adsorpt_lb_start
    "Lower bound of equilibrium pressure (required if pressure is calculated  
    numerically (i.e., root finding))"
    annotation (Dialog(tab="General", group="Numerical inputs"));
  input Modelica.Units.SI.Pressure p_adsorpt_ub_start
    "Upper bound of equilibrium pressure (required if pressure is calculated 
    numerically (i.e., root finding))"
    annotation (Dialog(tab="General", group="Numerical inputs"));

  input Modelica.Units.SI.Pressure integral_pi_lb
    "Lower limit of integral when calculating the reduced spreading pressure
    numerically (should be 0)"
    annotation (Dialog(tab="General", group="Numerical inputs"));

  input Real tolerance_p_adsorpt
    "Tolerance for numerical calculation of equilibrium pressure (required if 
    pressure is calculated numerically (i.e., root finding))"
    annotation (Dialog(tab="General", group="Numerical inputs"));
  input Real tolerance_pi
    "Tolerance for numerical calculation of reduced spreading pressure (required 
    if reduced spreading pressure is calculated numerically (i.e., integral))"
    annotation (Dialog(tab="General", group="Numerical inputs"));

  /* Note that optional numerical inputs do not have default values as this 
  function is used as functional input argument within the "Ideal Adsorbed
  Solution Theory," a multi-component adsorption isotherm model. Using default
  values resulted in translation errors. */

  //
  // Definition of outputs
  //
  output Modelica.Units.SI.Pressure p_adsorpt
    "Equilibrium pressure of adsorpt phase"
    annotation (Dialog(tab="General", group="Outputs", enable=false));

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial function is the basic function for all isotherm models describing 
the adsorption of pure components. Such models can be classical isotherm models 
based on a (plain) boundary surface or isotherm models based on filled pores.
<br/><br/>
This partial function is the basic function for calculating the equilibrium pressure 
<i>p_adsorpt</i> as a function of the reduced spreading pressure <i>pi</i> and 
equilibrium temperature <i>T_adsorpt</i>. Defined inputs are the molar mass of 
the adsorptive <i>M_adsorptive</i>, the reduced spreading pressure <i>pi</i>, 
the equilibrium temperature <i>T_adsorpt</i>, and the coefficients of the isotherm 
model <i>c</i>. The coefficients of the isotherm model <i>c</i> may depend on the 
equilibrium temperature <i>T_adsorpt</i>. Optional inputs regarding numerics are 
the lower bound (<i>p_adsorpt_lb_start</i>) and upper bound (<i>p_adsorpt_ub_start</i>) 
of the equilibrium pressure and the tolerance (<i>tolerance_p_adsorpt</i>), only 
required if this function cannot be solved analytically. Further optional inputs 
regarding numerics are the lower integral bound (<i>integral_pi_lb</i>) and the 
tolerance (<i>integral_pi_lb</i>), only required if the reduced spreading pressure 
cannot be solved analytically. Besides, this partial function defines the equilibrium 
pressure <i>p_adsorpt</i> as the output.
<br/><br/>
Functions that inherit properties from this partial function may have to implement 
further inputs and the function algorithm.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 1, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end PartialPure_p_piT;
