within SorpLib.Media.Functions.SorptionEquilibria.BaseClasses;
partial function PartialPure_p_piT_num
  "Base function for isotherm models of pure components: Pressure as function of reduced spreading pressure and temperature (numerical solution)"
  extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_p_piT;

  //
  // Definition of protected replacable functions
  //
protected
  replaceable function func_pi_pT =
    SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininAstakhov.pi_pT
    constrainedby
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_pi_pT
    "Reduced spreading pressure as function of pressure and temperature"
    annotation (Dialog(tab="General", group="Functions"));

  //
  // Definition of functions
  //
protected
  function func_p_num
    "Function used to find root (i.e., p_adsorpt) numerically"
    extends Modelica.Math.Nonlinear.Interfaces.partialScalarFunction;
    input SorpLib.Units.ReducedSpreadingPressure pi
      "Reduced spreading pressure";
    input Modelica.Units.SI.MolarMass M_adsorptive
      "Molar mass of adsorptive";
    input Modelica.Units.SI.Temperature T_adsorpt
      "Equilibrium temperature";
    input Real c[:]
      "Coefficients of isotherm model";
    input Modelica.Units.SI.Pressure integral_pi_lb
      "Lower limit of integral when calculating the reduced spreading pressure
      numerically (should be 0)";
    input Real tolerance
      "Tolerance for numerical calculation (required if reduced spreading pressure
      is calculated numerically";
  algorithm
    y := func_pi_pT(M_adsorptive=M_adsorptive, p_adsorpt=u, T_adsorpt=T_adsorpt,
      c=c, integral_pi_lb=integral_pi_lb, tolerance=tolerance) - pi
     "Function '0 = pi(M_adsorptive, p_adsorpt, T_adsorpt, c) - pi' used to find  
      root (i.e., p_adsorpt) numerically";
  end func_p_num;

  //
  // Definition of variables
  //
  Boolean bound_ok = false
    "= true, if bounds are found such that func_p_num(lb) < 0 and func_p_num(ub) > 0";

  Modelica.Units.SI.Pressure p_adsorpt_lb = p_adsorpt_lb_start
    "Current best lower bound of equilibrium pressure";
  Modelica.Units.SI.Pressure p_adsorpt_ub = p_adsorpt_ub_start
    "Current best upper bound of equilibrium pressure";

  SorpLib.Units.ReducedSpreadingPressure pi_lb=func_pi_pT(
      M_adsorptive=M_adsorptive,
      p_adsorpt=p_adsorpt_lb,
      T_adsorpt=T_adsorpt,
      c=c,
      integral_pi_lb=integral_pi_lb,
      tolerance=tolerance_pi)
    "Reduced spreading pressure at current best lower bound of equilibrium pressure";
  SorpLib.Units.ReducedSpreadingPressure pi_ub=func_pi_pT(
      M_adsorptive=M_adsorptive,
      p_adsorpt=p_adsorpt_ub,
      T_adsorpt=T_adsorpt,
      c=c,
      integral_pi_lb=integral_pi_lb,
      tolerance=tolerance_pi)
    "Reduced spreading pressure at current best upper bound of equilibrium pressure";

algorithm
  //
  // Find start values such that func_p_num(lb) < 0 and func_p_num(ub) > 0
  //
  // Reducing p_adsorpt reduces pi -> func_p_num(p_adsorpt) can become < 0
  // Increasing p_adsorpt increases pi -> func_p_num(p_adsorpt) can become > 0
  //
  while not bound_ok loop
    if Modelica.Math.isEqual(s1=pi_lb, s2=pi, eps=tolerance_p_adsorpt) then
      p_adsorpt := p_adsorpt_lb;
      bound_ok := true;
      return;

    elseif Modelica.Math.isEqual(s1=pi_ub, s2=pi, eps=tolerance_p_adsorpt) then
      p_adsorpt := p_adsorpt_ub;
      bound_ok := true;
      return;

    elseif pi_lb-pi < 0 and pi_ub-pi < 0 then
      p_adsorpt_lb := p_adsorpt_ub;
      pi_lb := pi_ub;

      p_adsorpt_ub := p_adsorpt_ub*10;
      pi_ub := func_pi_pT(M_adsorptive=M_adsorptive,
        p_adsorpt=p_adsorpt_ub, T_adsorpt=T_adsorpt, c=c,
        integral_pi_lb=integral_pi_lb, tolerance=tolerance_pi);

    elseif pi_lb-pi > 0 and pi_ub-pi > 0 then
      p_adsorpt_ub := p_adsorpt_lb;
      pi_ub := pi_lb;

      p_adsorpt_lb := if p_adsorpt_lb > Modelica.Constants.small then
        p_adsorpt_lb*0.1 else 0;
      pi_lb := func_pi_pT(M_adsorptive=M_adsorptive,
        p_adsorpt=p_adsorpt_lb, T_adsorpt=T_adsorpt, c=c,
        integral_pi_lb=integral_pi_lb, tolerance=tolerance_pi);

    else
      bound_ok := true;

    end if;
  end while;

  //
  // Find root in the interval lb <= root <= up
  //
  p_adsorpt := Modelica.Math.Nonlinear.solveOneNonlinearEquation(
    f = function func_p_num(
      pi=pi,
      M_adsorptive=M_adsorptive,
      T_adsorpt=T_adsorpt,
      c=c,
      integral_pi_lb=integral_pi_lb,
      tolerance=tolerance_pi),
    u_min=p_adsorpt_lb,
    u_max=p_adsorpt_ub,
    tolerance=tolerance_p_adsorpt)
    "Calculation of the equilibrium pressure of the adsorpt phase";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial function is the basic function for all isotherm models describing 
the adsorption of pure components. Such models can be classical isotherm models 
based on a (plain) boundary surface or isotherm models based on filled pores.
<br/><br/>
This partial function is the basic function for calculating the equilibrium pressure 
<i>p_adsorpt</i> as a function of the reduced spreading pressure <i>pi</i> and 
equilibrium temperature <i>T_adsorpt</i>. Defined inputs are the molar mass of 
the adsorptive <i>M_adsorptive</i>, the reduced spreading pressure <i>pi</i>, 
the equilibrium temperature <i>T_adsorpt</i>, and the coefficients of the isotherm 
model <i>c</i>. The coefficients of the isotherm model <i>c</i> may depend on the 
equilibrium temperature <i>T_adsorpt</i>. Optional inputs regarding numerics are 
the lower bound (<i>p_adsorpt_lb_start</i>) and upper bound (<i>p_adsorpt_ub_start</i>) 
of the equilibrium pressure and the tolerance (<i>tolerance_p_adsorpt</i>), only 
required if this function cannot be solved analytically. Further optional inputs 
regarding numerics are the lower integral bound (<i>integral_pi_lb</i>) and the 
tolerance (<i>integral_pi_lb</i>), only required if the reduced spreading pressure 
cannot be solved analytically. Besides, this partial function defines the equilibrium 
pressure <i>p_adsorpt</i> as the output.
<br/><br/>
With this function, the equilibrium pressure <i>p_adsorpt</i> is determined 
numerically by solving a zero problem. The zero problem is as follows:
</p>
<pre>
    f(p_adsorpt) = 0 => 0 = func_pi_pT(p_adsorpt=p_adsorpt, T_adsorpt=T_adsorpt, c=c) - pi;
</pre>
<p>
The zero problem is solved using the numerically very efficient function 
<a href=\"Modelica://Modelica.Math.Nonlinear.solveOneNonlinearEquation\">Modelica.Math.Nonlinear.solveOneNonlinearEquation</a>.
For the application of this function, it must be ensured that <i>f(p_adsorpt)</i> 
has a different sign for the upper and lower limits of <i>p_adsorpt</i>. To ensure 
the different signs, the upper and lower limits are determined in an upstream loop.
<br/><br/>
Functions that inherit properties from this partial function have to redeclare
the function <i>func_pi_pT</i>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 1, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end PartialPure_p_piT_num;
