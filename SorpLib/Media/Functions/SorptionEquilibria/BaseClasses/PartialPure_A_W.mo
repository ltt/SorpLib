within SorpLib.Media.Functions.SorptionEquilibria.BaseClasses;
partial function PartialPure_A_W
  "Base function for isotherm models of pure components: Molar adsorption potential as function of filled pore volume"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input SorpLib.Units.FilledPoreVolume W
    "Filled pore volume"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real[:] c
    "Coefficients of the isotherm model"
    annotation (Dialog(tab="General", group="Inputs"));

  input SorpLib.Units.MolarAdsorptionPotential A_lb_start=10
  "Lower bound of molar adsorption potential (required if molar adsorption potential
  is calculated numerically (i.e., root finding))"
    annotation (Dialog(tab="General", group="Numerical inputs"));
  input SorpLib.Units.MolarAdsorptionPotential A_ub_start=100
  "Upper bound of molar adsorption potential ((required if molar adsorption potential
  is calculated numerically (i.e., root finding))"
    annotation (Dialog(tab="General", group="Numerical inputs"));
  input Real tolerance = 100*Modelica.Constants.eps
    "Tolerance for numerical calculation (required if molar adsorption potential
  is calculated numerically (i.e., root finding))"
    annotation (Dialog(tab="General", group="Numerical inputs"));

  //
  // Definition of outputs
  //
  output SorpLib.Units.MolarAdsorptionPotential A "Molar adsorption potential"
    annotation (Dialog(
      tab="General",
      group="Outputs",
      enable=false));

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial function is the basic function for all isotherm models describing 
the adsorption of pure components. Such models must be based on filled pores
(i.e., Dubinin theory).
<br/><br/>
This partial function is the basic function for calculating the molar adsorption 
potential <i>A</i> as a function of the filled pore volume <i>W</i>. Defined inputs 
are the filled pore volume <i>W</i> and the coefficients of the isotherm model <i>c</i>. 
The coefficients of the isotherm model <i>c</i> may depend on the equilibrium
temperature <i>T_adsorpt</i>. Optional inputs regarding numerics are the lower 
bound (<i>A_lb_start</i>) and upper bound (<i>A_ub_start</i>) of the molar 
adsorption potential and the tolerance (<i>tolerance</i>), only required  if this 
function cannot be solved analytically. The defined output is the molar adsorption 
potential <i>A</i>.
<br/><br/>
Functions that inherit properties from this partial function may have to implement
further inputs and the function algorithm.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 2, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end PartialPure_A_W;
