within SorpLib.Media.Functions.SorptionEquilibria.BaseClasses;
partial function PartialPure_pi_pT_num
  "Base function for isotherm models of pure components: Reduced spreading pressure as function of pressure and temperature (numerical solution)"
  extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_pi_pT;

  //
  // Definition of protected replacable functions
  //
protected
  replaceable function func_x_pT =
    SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininAstakhov.x_pT
    constrainedby
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_x_pT
    "Calculates uptake as function of pressure and temperature"
    annotation (Dialog(tab="General", group="Functions"));

  //
  // Definition of protected functions
  //
protected
  function func_pi_num
    "Integrand required for calculating the reduced spreading pressure numerically"
    extends Modelica.Math.Nonlinear.Interfaces.partialScalarFunction;
    input Modelica.Units.SI.Temperature T_adsorpt
      "Equilibrium temperature";
    input Real c[:]
      "Coefficients of isotherm model";
  algorithm
    y := func_x_pT(p_adsorpt=u, T_adsorpt=T_adsorpt, c=c,
      p_adsorpt_lb_start=1, p_adsorpt_ub_start=10,
      tolerance=100*Modelica.Constants.eps) /
      max(u, Modelica.Constants.small)
      "Integrand 'x(p,T,c) / p' required for calculating the reduced spreading
      pressure";
  end func_pi_num;

algorithm
  pi := 1/M_adsorptive *
    Modelica.Math.Nonlinear.quadratureLobatto(
      f=function func_pi_num(T_adsorpt=T_adsorpt, c=c),
      a=integral_pi_lb,
      b=p_adsorpt,
      tolerance=tolerance)
    "Calculation of the reduced spreading pressure";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial function is the basic function for all isotherm models describing 
the adsorption of pure components. Such models can be classical isotherm models 
based on a (plain) boundary surface or isotherm models based on filled pores.
<br/><br/>
This partial function is the basic function for calculating the reduced spreading 
pressure <i>pi</i> as a function of the equilibrium pressure <i>p_adsorpt</i> and 
equilibrium temperature <i>T_adsorpt</i>. Defined inputs are the molar mass of 
the adsorptive <i>M_adsorptive</i>, the equilibrium pressure <i>p_adsorpt</i>, 
the equilibrium temperature <i>T_adsorpt</i>, and the coefficients of the isotherm 
model <i>c</i>. The coefficients of the isotherm model <i>c</i> may depend on the 
equilibrium temperature <i>T_adsorpt</i>. Optional inputs regarding numerics are 
the lower integral bound (<i>integral_pi_lb</i>) and the tolerance (<i>tolerance</i>). 
Besides, this partial function defines the reduced spreading pressure <i>pi</i> as 
the output.
<br/><br/>
With this function, the reduced spreading pressure <i>pi</i> is determined 
numerically by using the numerically very efficient function 
<a href=\"Modelica://Modelica.Math.Nonlinear.quadratureLobatto\">Modelica.Math.Nonlinear.quadratureLobatto</a>.
<br/><br/>
Functions that inherit properties from this partial function have to redeclare
the function <i>func_x_pT</i>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 1, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end PartialPure_pi_pT_num;
