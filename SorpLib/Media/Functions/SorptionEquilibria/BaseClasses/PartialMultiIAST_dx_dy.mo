within SorpLib.Media.Functions.SorptionEquilibria.BaseClasses;
partial function PartialMultiIAST_dx_dy
  "Base function for isotherm models based on the ideal adsorbed solution theory: Partial derivative of uptakes w.r.t. molar fractions of independent gas phase components at constant temperature and pressure"
  extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialMultiIAST;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.Pressure p_adsorpt
    "Equilibrium pressure of the adsorpt phase"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.MoleFraction[size(M_i,1)-1] y_i
    "Mole fractions of independent components in the vapor or gas phase"
    annotation (Dialog(tab="General", group="Inputs"));

  input Real dy = 1e-4
    "Mole fraction difference used to calculate partial derivatives w.r.t. independent
    mole fractions"
    annotation (Dialog(tab="General", group="Inputs - Numerics"));

  //
  // Definition of outputs
  //
  output
    SorpLib.Units.DerUptakeByMolarFraction[size(M_i,1),size(M_i,1)-1] dx_adsorpt_dy_i
    "Partial derivatives of the uptakes w.r.t. the molar fractions of independent 
    gas phase components at constant temperature and pressure"
    annotation (Dialog(tab="General", group="Outputs", enable=false));

  //
  // Definition of variables
  //
protected
  Modelica.Units.SI.MoleFraction[size(M_i,1)] y_i_ = cat(1, y_i, {1-sum(y_i)})
    "Mole fractions of all components in the vapor or gas phase";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial function is the basic function for all isotherm models based on
the ideal adsorbed solution theory (IAST) describing the adsorption of multi 
components.
<br/><br/>
This partial function is the basic function for calculating the partial derivative
in of the equilibrium uptakes w.r.t. the molar fractions of the independent components
the gas or vapor phase at constant temperature and pressure <i>dx_adsorpt_dy_i</i> 
as a function of the equilibrium pressure <i>p_adsorpt</i>, the mole fractions of 
the independent components in the gas or vapor phase <i>y_i</i>, the equilibrium 
temperature <i>T_adsorpt</i>. Further inputs are the coefficients of the isotherm 
model of the first component <i>c_1</i> and second component <i>c_2</i>. The 
coefficients of the isotherm model <i>c_i</i> may depend on the equilibrium temperature 
<i>T_adsorpt</i>. Further inputs are functions describing the pure component isotherm 
models. These functional arguments are <i>func_x_pT_1</i>, <i>func_p_xT_1</i>, 
<i>func_dx_dp_1</i>, <i>func_pi_pT_1</i>, and <i>func_p_piT_1</i> for component 1 
(i.e., <i>i = 1</i>) and 2 (i.e., <i>i = 2</i>). Optional input regarding numerics 
define the setup of the IAST algorithm <i>num</i>, the pure component isotherm models 
<i>num_comp_i</i>, and the infinitesimal difference <i>dy</i> to calculate the
partial derivatives.
<br/><br/>
The partial derivatives of the equilibrium uptakes w.r.t. the molar fractions of 
the independent components the gas or vapor phase at constant temperature and pressure
<i>dx_adsorpt_dy_i</i> are defined as the outputs.
<br/><br/>
Functions that inherit properties from this partial function may have to implement
further inputs and the function algorithm. Further inputs are particulary required 
when developing IAST functions for more than two components.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 10, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end PartialMultiIAST_dx_dy;
