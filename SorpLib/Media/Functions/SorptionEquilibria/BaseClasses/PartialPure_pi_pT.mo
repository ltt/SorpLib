within SorpLib.Media.Functions.SorptionEquilibria.BaseClasses;
partial function PartialPure_pi_pT
  "Base function for isotherm models of pure components: Reduced spreading pressure as function of pressure and temperature"

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.MolarMass M_adsorptive
    "Molar mass of adsorptive"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.Pressure p_adsorpt
    "Equilibrium pressure of adsorpt phase"
    annotation (Dialog(tab="General", group="Inputs"));
  extends SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure;

  input Modelica.Units.SI.Pressure integral_pi_lb
    "Lower limit of integral when calculating the reduced spreading pressure
    numerically (should be 0)"
    annotation (Dialog(tab="General", group="Numerical inputs"));
  input Real tolerance
    "Tolerance for numerical calculation (required if reduced spreading pressure
    is calculated numerically (i.e., integral))"
    annotation (Dialog(tab="General", group="Numerical inputs"));

  /* Note that optional numerical inputs do not have default values as this 
  function is used as functional input argument within the "Ideal Adsorbed
  Solution Theory," a multi-component adsorption isotherm model. Using default
  values resulted in translation errors. */

  //
  // Definition of outputs
  //
  output SorpLib.Units.ReducedSpreadingPressure pi
    "Reduced spreading pressure"
    annotation (Dialog(tab="General", group="Outputs", enable=false));

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial function is the basic function for all isotherm models describing 
the adsorption of pure components. Such models can be classical isotherm models 
based on a (plain) boundary surface or isotherm models based on filled pores.
<br/><br/>
This partial function is the basic function for calculating the reduced spreading 
pressure <i>pi</i> as a function of the equilibrium pressure <i>p_adsorpt</i> and 
equilibrium temperature <i>T_adsorpt</i>. Defined inputs are the molar mass of 
the adsorptive <i>M_adsorptive</i>, the equilibrium pressure <i>p_adsorpt</i>, 
the equilibrium temperature <i>T_adsorpt</i>, and the coefficients of the isotherm 
model <i>c</i>. The coefficients of the isotherm model <i>c</i> may depend on the 
equilibrium temperature <i>T_adsorpt</i>. Optional inputs regarding numerics are 
the lower integral bound (<i>integral_pi_lb</i>) and the tolerance (<i>tolerance</i>), 
only required if this function cannot be solved analytically. Besides, this partial 
function defines the reduced spreading pressure <i>pi</i> as the output.
<br/><br/>
Functions that inherit properties from this partial function may have to implement 
further inputs and the function algorithm.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 1, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end PartialPure_pi_pT;
