within SorpLib.Media.Functions.SorptionEquilibria.BaseClasses;
partial model PartialTestPure
  "Base model for testers of isotherm models describing pure component adsorption"
  extends Modelica.Icons.Example;

  //
  // Definition of parameters
  //
  parameter Real p_adsorpt_der(unit="Pa/s") = 1
    "Prescriped sloped of equilibrium pressure to test isotherm model"
    annotation (Dialog(tab="General", group="Test setup"),
                Evaluate=false,
                HideResult=false);
  parameter Real T_adsorpt_der(unit="K/s") = 0
    "Prescriped sloped of equilibrium temperature to test isotherm model"
    annotation (Dialog(tab="General", group="Test setup"),
                Evaluate=false,
                HideResult=false);
  parameter Modelica.Units.SI.Pressure p_adsorpt_start = 0
    "Start value of equilibrium pressure"
    annotation (Dialog(tab="General", group="Test setup"),
                Evaluate=false,
                HideResult=false);
  parameter Modelica.Units.SI.Temperature T_adsorpt_start = 298.15
    "Start value of equilibrium temperature"
    annotation (Dialog(tab="General", group="Test setup"),
                Evaluate=false,
                HideResult=false);
  parameter Modelica.Units.SI.MolarMass M_adsorptive = 0.018
    "Molar mass of adsorptive"
    annotation (Dialog(tab="General", group="Test setup"),
                Evaluate=true,
                HideResult=true);

  parameter Boolean print_asserts = true
    "= true, if assertations shall be printed; otherwise, no assertations are printed"
    annotation (Dialog(tab="General", group="Checks"),
                Evaluate=true,
                HideResult=true);
  parameter Boolean check_func_p_xT = true
    "= true, if function 'func_p_xT' is checked; otherwise, function is not checked"
    annotation (Dialog(tab="General", group="Checks"),
                Evaluate=true,
                HideResult=true);
  parameter Boolean check_func_dx_dp = true
    "= true, if function 'func_dx_dp' is checked; otherwise, function is not checked"
    annotation (Dialog(tab="General", group="Checks"),
                Evaluate=true,
                HideResult=true);
  parameter Boolean check_func_dx_dT = true
    "= true, if function 'func_dx_dT' is checked; otherwise, function is not checked"
    annotation (Dialog(tab="General", group="Checks"),
                Evaluate=true,
                HideResult=true);
  parameter Boolean check_func_ddx_dp_dp = true
    "= true, if function 'func_ddx_dp_dp' is checked; otherwise, function is not checked"
    annotation (Dialog(tab="General", group="Checks"),
                Evaluate=true,
                HideResult=true);
  parameter Boolean check_func_ddx_dT_dT = true
    "= true, if function 'func_ddx_dT_dT' is checked; otherwise, function is not checked"
    annotation (Dialog(tab="General", group="Checks"),
                Evaluate=true,
                HideResult=true);
  parameter Boolean check_func_ddx_dp_dT = true
    "= true, if function 'ddx_dp_dT' is checked; otherwise, function is not checked"
    annotation (Dialog(tab="General", group="Checks"),
                Evaluate=true,
                HideResult=true);
  parameter Boolean check_func_pi_pT = true
    "= true, if function 'func_pi_pT' is checked; otherwise, function is not checked"
    annotation (Dialog(tab="General", group="Checks"),
                Evaluate=true,
                HideResult=true);
  parameter Boolean check_func_p_piT = true
    "= true, if function 'func_p_piT' is checked; otherwise, function is not checked"
    annotation (Dialog(tab="General", group="Checks", enable=check_func_pi_pT),
                Evaluate=true,
                HideResult=true);

  replaceable package IsothermModel =
    SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Henry
    constrainedby
    SorpLib.Media.Functions.SorptionEquilibria.Interfaces.PartialPureComponents
    "Package providing all functions of the isotherm model"
    annotation (Dialog(tab="General", group="Isotherm Model"),
                choicesAllMatching=true);
  parameter Integer no_coefficients = 1
    "Number of coefficients of the isotherm model"
    annotation (Dialog(tab="General", group="Isotherm Model"),
                Evaluate=true,
                HideResult=true);

  parameter Real tolerance_p_inv = 100*Modelica.Constants.eps
    "Tolerance for numerical calculation of equilibrium pressure via function 'p_xT'
    (only used if required)"
    annotation (Dialog(tab="Numerics", group="Tolerances"),
                Evaluate=true,
                HideResult=true);
  parameter Real tolerance_p_pi = 100*Modelica.Constants.eps
    "Tolerance for numerical calculation of equilibrium pressure via function 'p_piT'
    (only used if required)"
    annotation (Dialog(tab="Numerics", group="Tolerances"),
                Evaluate=true,
                HideResult=true);
  parameter Real tolerance_pi = 100*Modelica.Constants.eps
    "Tolerance for numerical calculation of reduced spreading pressure (only used 
    if required)"
    annotation (Dialog(tab="Numerics", group="Tolerances"),
                Evaluate=true,
                HideResult=true);

  parameter Modelica.Units.SI.Pressure p_lb_pi= 0
    "Lower bound to calculate reduced spreading pressure (should be 0)"
    annotation (Dialog(tab="Numerics", group="Reduced spreading pressure"),
                Evaluate=true,
                HideResult=true);

  parameter Modelica.Units.SI.PressureDifference dp = 1e-6
    "Pressure difference used to calculate partial derivatives numerically"
    annotation (Dialog(tab="Numerics", group="Derivatives"),
                Evaluate=true,
                HideResult=true);
  parameter Modelica.Units.SI.TemperatureDifference dT = 1e-6
    "Temperature difference used to calculate partial derivatives numerically"
    annotation (Dialog(tab="Numerics", group="Derivatives"),
                Evaluate=true,
                HideResult=true);

  //
  // Definition of variables
  //
  Modelica.Units.SI.Pressure p_adsorpt(start=p_adsorpt_start, fixed=true)
    "Equilibrium pressure";
  Modelica.Units.SI.Temperature T_adsorpt(start=T_adsorpt_start, fixed=true)
    "Equilibrium temperature";
  SorpLib.Units.Uptake x_adsorpt
    "Equilibrium uptake";

  Modelica.Units.SI.Pressure p_adsorpt_inv
    "Equilibrium pressure calculated via inverse function of the isotherm model";
  Modelica.Units.SI.Pressure p_adsorpt_pi
    "Equilibrium pressure calculated via reduced spreading pressure";

  SorpLib.Units.DerUptakeByPressure dx_adsorpt_dp_adsorpt
    "Partial derivative of the uptake w.r.t. pressure at constant temperature";
  SorpLib.Units.DerUptakeByPressure dx_adsorpt_dp_adsorpt_num
    "Partial derivative of the uptake w.r.t. pressure at constant temperature
    calculated numerically";

  SorpLib.Units.DerUptakeByTemperature dx_adsorpt_dT_adsorpt
    "Partial derivative of the uptake w.r.t. temperature at constant pressure";
  SorpLib.Units.DerUptakeByTemperature dx_adsorpt_dT_adsorpt_num
    "Partial derivative of the uptake w.r.t. temperature at constant pressure
    calculated numerically";

  SorpLib.Units.DerUptakeByPressurePressure ddx_adsorpt_dp_adsorpt_dp_adsorpt
    "Second-order partial derivative of the uptake w.r.t. pressure at constant 
    temperature";
  SorpLib.Units.DerUptakeByPressurePressure ddx_adsorpt_dp_adsorpt_dp_adsorpt_num
    "Second-order partial derivative of the uptake w.r.t. pressure at constant 
    temperature calculated numerically";

  SorpLib.Units.DerUptakeByTemperatureTemperature ddx_adsorpt_dT_adsorpt_dT_adsorpt
    "Second-order partial derivative of the uptake w.r.t. temperature at constant 
    pressure";
  SorpLib.Units.DerUptakeByTemperatureTemperature ddx_adsorpt_dT_adsorpt_dT_adsorpt_num
    "Second-order partial derivative of the uptake w.r.t. temperature at constant 
    pressure calculated numerically";

  SorpLib.Units.DerUptakeByPressureTemperature ddx_adsorpt_dp_adsorpt_dT_adsorpt
    "Second-order partial derivative of the uptake w.r.t. pressure and temperature";
  SorpLib.Units.DerUptakeByPressureTemperature ddx_adsorpt_dp_adsorpt_dT_adsorpt_num
    "Second-order partial derivative of the uptake w.r.t. pressure and temperature
     calculated numerically";

  SorpLib.Units.ReducedSpreadingPressure red_spreading_pressure
    "Reduced spreading pressure";
  SorpLib.Units.ReducedSpreadingPressure red_spreading_pressure_num
    "Reduced spreading pressure calculated numerically";

  Real[no_coefficients] c
    "Coefficients of isotherm model";

  Real[no_coefficients] dc_dT
    "Partial derivative of coefficients of isotherm model w.r.t. temperature";
  Real[no_coefficients] dc_dT_num = (c_pdT .- c_mdT) ./ (2 * dT)
    "Partial derivative of coefficients of isotherm model w.r.t. temperature
    calculated numerically";

  Real[no_coefficients] ddc_dT_dT
    "Second-order partial derivative of coefficients of isotherm model w.r.t. 
    temperature";
  Real[no_coefficients] ddc_dT_dT_num = (c_pdT .- 2 .* c .+ c_mdT) ./ (dT ^ 2)
    "Second-order partial derivative of coefficients of isotherm model w.r.t. 
    temperature calculated numerically";

protected
  Real[no_coefficients] c_pdT
    "Coefficients of isotherm model: T + 1e-6 K";
  Real[no_coefficients] c_mdT
    "Coefficients of isotherm model: T - 1e-6 K";

  function func_pi_num
    "Integrand required for calculating the reduced spreading pressure numerically"
    extends Modelica.Math.Nonlinear.Interfaces.partialScalarFunction;
    input Real T_adsorpt
      "Equilibrium temperature";
    input Real c[:]
      "Coefficients of isotherm model";
  algorithm
    y :=  IsothermModel.x_pT(p_adsorpt=u, T_adsorpt=T_adsorpt, c=c,
      p_adsorpt_lb_start=1, p_adsorpt_ub_start=10,
      tolerance=100*Modelica.Constants.eps) /
      max(u, Modelica.Constants.small)
      "Integrand 'x(p,T,c) / p' required for calculating the reduced spreading
      pressure";
  end func_pi_num;

equation
  der(p_adsorpt) = p_adsorpt_der
    "Predecsriped slope of p_adsorpt to demonstrate the isotherm model";
  der(T_adsorpt) = T_adsorpt_der
    "Predecsriped slope of T_adsorpt to demonstrate the isotherm model";

  if check_func_p_xT then
    p_adsorpt_inv = IsothermModel.p_xT(x_adsorpt=x_adsorpt, T_adsorpt=T_adsorpt, c=c,
      p_adsorpt_lb_start=1, p_adsorpt_ub_start=10, tolerance=tolerance_p_inv)
      "Equilibrium pressure calculated via the inverse function of the isotherm model";
  else
    p_adsorpt_inv = -1
      "Equilibrium pressure calculated via the inverse function of the isotherm model";
  end if;

  if check_func_pi_pT and check_func_p_piT then
    p_adsorpt_pi = IsothermModel.p_piT(M_adsorptive=M_adsorptive, pi=red_spreading_pressure,
      T_adsorpt=T_adsorpt, c=c, p_adsorpt_lb_start=1, p_adsorpt_ub_start=10,
      integral_pi_lb=p_lb_pi, tolerance_p_adsorpt=tolerance_p_pi,
      tolerance_pi=tolerance_pi)
      "Equilibrium pressure calculated via reduced spreading pressure";
  else
    p_adsorpt_pi = -1
      "Equilibrium pressure calculated via reduced spreading pressure";
  end if;

  x_adsorpt = IsothermModel.x_pT(p_adsorpt=p_adsorpt, T_adsorpt=T_adsorpt, c=c,
    p_adsorpt_lb_start=1, p_adsorpt_ub_start=10, tolerance=tolerance_p_inv)
    "Equilibrium uptake";

  if check_func_dx_dp then
    dx_adsorpt_dp_adsorpt = IsothermModel.dx_dp(p_adsorpt=p_adsorpt, T_adsorpt=T_adsorpt,
      c=c)
      "Partial derivative of the uptake w.r.t. pressure at constant temperature";
    dx_adsorpt_dp_adsorpt_num = (
      IsothermModel.x_pT(p_adsorpt=p_adsorpt+dp, T_adsorpt=T_adsorpt, c=c,
      p_adsorpt_lb_start=1, p_adsorpt_ub_start=10, tolerance=tolerance_p_inv) -
      IsothermModel.x_pT(p_adsorpt=p_adsorpt-dp, T_adsorpt=T_adsorpt, c=c,
      p_adsorpt_lb_start=1, p_adsorpt_ub_start=10, tolerance=tolerance_p_inv)) /
      (2*dp)
      "Partial derivative of the uptake w.r.t. pressure at constant temperature
      calculated numerically";
  else
    dx_adsorpt_dp_adsorpt = -1
      "Partial derivative of the uptake w.r.t. pressure at constant temperature";
    dx_adsorpt_dp_adsorpt_num = -1
      "Partial derivative of the uptake w.r.t. pressure at constant temperature
      calculated numerically";
  end if;

  if check_func_dx_dT then
    dx_adsorpt_dT_adsorpt = IsothermModel.dx_dT(p_adsorpt=p_adsorpt, T_adsorpt=T_adsorpt,
      c=c, dc_dT_adsorpt=dc_dT)
      "Partial derivative of the uptake w.r.t. temperature at constant pressure";
    dx_adsorpt_dT_adsorpt_num = (
      IsothermModel.x_pT(p_adsorpt=p_adsorpt, T_adsorpt=T_adsorpt+dT, c=c_pdT,
      p_adsorpt_lb_start=1, p_adsorpt_ub_start=10, tolerance=tolerance_p_inv) -
      IsothermModel.x_pT(p_adsorpt=p_adsorpt, T_adsorpt=T_adsorpt-dT, c=c_mdT,
      p_adsorpt_lb_start=1, p_adsorpt_ub_start=10, tolerance=tolerance_p_inv)) /
      (2*dT)
      "Partial derivative of the uptake w.r.t. temperature at constant pressure
      calculated numerically";
  else
    dx_adsorpt_dT_adsorpt = -1
      "Partial derivative of the uptake w.r.t. temperature at constant pressure";
    dx_adsorpt_dT_adsorpt_num = -1
      "Partial derivative of the uptake w.r.t. temperature at constant pressure
      calculated numerically";
  end if;

  if check_func_ddx_dp_dp then
    ddx_adsorpt_dp_adsorpt_dp_adsorpt = IsothermModel.ddx_dp_dp(p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt, c=c)
      "Second-order partial derivative of the uptake w.r.t. pressure at constant
       temperature";
    ddx_adsorpt_dp_adsorpt_dp_adsorpt_num = (
      IsothermModel.x_pT(p_adsorpt=p_adsorpt+dp, T_adsorpt=T_adsorpt, c=c,
      p_adsorpt_lb_start=1, p_adsorpt_ub_start=10, tolerance=tolerance_p_inv) -
      2 * IsothermModel.x_pT(p_adsorpt=p_adsorpt, T_adsorpt=T_adsorpt, c=c,
      p_adsorpt_lb_start=1, p_adsorpt_ub_start=10, tolerance=tolerance_p_inv) +
      IsothermModel.x_pT(p_adsorpt=p_adsorpt-dp, T_adsorpt=T_adsorpt, c=c,
      p_adsorpt_lb_start=1, p_adsorpt_ub_start=10, tolerance=tolerance_p_inv)) /
      (dp^2)
      "Second-order partial derivative of the uptake w.r.t. pressure at constant 
      temperature numerically";
  else
    ddx_adsorpt_dp_adsorpt_dp_adsorpt = -1
      "Second-order partial derivative of the uptake w.r.t. temperature at constant 
      temperature";
    ddx_adsorpt_dp_adsorpt_dp_adsorpt_num = -1
      "Second-order partial derivative of the uptake w.r.t. temperature at constant 
      temperature numerically";
  end if;

  if check_func_ddx_dT_dT then
    ddx_adsorpt_dT_adsorpt_dT_adsorpt = IsothermModel.ddx_dT_dT(p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt, c=c, dc_dT_adsorpt=dc_dT,
      ddc_dT_adsorpt_dT_adsorpt=ddc_dT_dT)
      "Second-order partial derivative of the uptake w.r.t. temperature at constant
      pressure";
    ddx_adsorpt_dT_adsorpt_dT_adsorpt_num = (
      IsothermModel.x_pT(p_adsorpt=p_adsorpt, T_adsorpt=T_adsorpt+dT, c=c_pdT,
      p_adsorpt_lb_start=1, p_adsorpt_ub_start=10, tolerance=tolerance_p_inv) -
      2 * IsothermModel.x_pT(p_adsorpt=p_adsorpt, T_adsorpt=T_adsorpt, c=c,
      p_adsorpt_lb_start=1, p_adsorpt_ub_start=10, tolerance=tolerance_p_inv) +
      IsothermModel.x_pT(p_adsorpt=p_adsorpt, T_adsorpt=T_adsorpt-dT, c=c_mdT,
      p_adsorpt_lb_start=1, p_adsorpt_ub_start=10, tolerance=tolerance_p_inv)) /
      (dT^2)
      "Second-order partial derivative of the uptake w.r.t. temperature at constant 
      pressure numerically";
  else
    ddx_adsorpt_dT_adsorpt_dT_adsorpt = -1
      "Second-order partial derivative of the uptake w.r.t. temperature at constant 
      pressure";
    ddx_adsorpt_dT_adsorpt_dT_adsorpt_num = -1
      "Second-order partial derivative of the uptake w.r.t. temperature at constant 
      pressure numerically";
  end if;

  if check_func_ddx_dp_dT then
    ddx_adsorpt_dp_adsorpt_dT_adsorpt = IsothermModel.ddx_dp_dT(p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt, c=c, dc_dT_adsorpt=dc_dT)
      "Second-order partial derivative of the uptake w.r.t. pressure and temperature";
    ddx_adsorpt_dp_adsorpt_dT_adsorpt_num = (
      IsothermModel.x_pT(p_adsorpt=p_adsorpt+dp, T_adsorpt=T_adsorpt+dT, c=c_pdT,
      p_adsorpt_lb_start=1, p_adsorpt_ub_start=10, tolerance=tolerance_p_inv) -
      IsothermModel.x_pT(p_adsorpt=p_adsorpt+dp, T_adsorpt=T_adsorpt-dT, c=c_mdT,
      p_adsorpt_lb_start=1, p_adsorpt_ub_start=10, tolerance=tolerance_p_inv) -
      IsothermModel.x_pT(p_adsorpt=p_adsorpt-dp, T_adsorpt=T_adsorpt+dT, c=c_pdT,
      p_adsorpt_lb_start=1, p_adsorpt_ub_start=10, tolerance=tolerance_p_inv) +
      IsothermModel.x_pT(p_adsorpt=p_adsorpt-dp, T_adsorpt=T_adsorpt-dT, c=c_mdT,
      p_adsorpt_lb_start=1, p_adsorpt_ub_start=10, tolerance=tolerance_p_inv)) /
      (2*dp*2*dT)
      "Second-order partial derivative of the uptake w.r.t. pressure and temperature
       calculated numerically";
  else
    ddx_adsorpt_dp_adsorpt_dT_adsorpt = -1
      "Second-order partial derivative of the uptake w.r.t. pressure and temperature";
    ddx_adsorpt_dp_adsorpt_dT_adsorpt_num = -1
      "Second-order partial derivative of the uptake w.r.t. pressure and temperature
       calculated numerically";
  end if;

  if check_func_pi_pT then
    red_spreading_pressure = IsothermModel.pi_pT(M_adsorptive=M_adsorptive, p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt, c=c, integral_pi_lb=p_lb_pi, tolerance=tolerance_pi)
      "Reduced spreading pressure";
    red_spreading_pressure_num = Modelica.Math.Nonlinear.quadratureLobatto(
      f=function func_pi_num(T_adsorpt=T_adsorpt, c=c),
      a=p_lb_pi,
      b=p_adsorpt,
      tolerance=tolerance_pi) / M_adsorptive
      "Reduced spreading pressure calculated numerically";
  else
    red_spreading_pressure = -1
      "Reduced spreading pressure";
    red_spreading_pressure_num = -1
      "Reduced spreading pressure calculated numerically";

  end if;

  //
  // Definition of assertions: Check numerical implementation of isotherm model
  //
  if print_asserts then
    if check_func_p_xT then
      assert(abs(p_adsorpt-p_adsorpt_inv) < 1e-6,
        "Inverse function of isotherm model is not valid: Deviation (|" +
        String(abs(p_adsorpt-p_adsorpt_inv)) +
        "|) is greater than 1e-6 Pa!",
        level = AssertionLevel.warning);
    end if;

    if check_func_pi_pT and check_func_p_piT then
      assert(abs(p_adsorpt-p_adsorpt_pi) < 1e-6,
        "Inverse function of reduced spreading pressure is not valid: Deviation (|" +
        String(abs(p_adsorpt-p_adsorpt_pi)) +"|) is greater " +
        "than 1e-6 Pa!",
        level = AssertionLevel.warning);
    end if;

    if check_func_pi_pT then
      assert(abs(red_spreading_pressure-red_spreading_pressure_num) < 1e-6,
        "Reduced spreading pressure is not valid: Deviation (|" +
        String(abs(red_spreading_pressure-red_spreading_pressure_num)) +
        "|) is greater than 1e-6 mol/kg!",
        level = AssertionLevel.warning);
    end if;

    if check_func_dx_dp then
      assert(abs(dx_adsorpt_dp_adsorpt-dx_adsorpt_dp_adsorpt_num) < 1e-6,
        "Partial derivative of isotherm model w.r.t. pressure at constant " +
        "temperature is not valid: Deviation (|" +
        String(abs(dx_adsorpt_dp_adsorpt-dx_adsorpt_dp_adsorpt_num)) +
        "|) is greater than 1e-6 kg/(kg.Pa)!",
        level = AssertionLevel.warning);
    end if;

    if check_func_dx_dT then
      assert(abs(dx_adsorpt_dT_adsorpt-dx_adsorpt_dT_adsorpt_num) < 1e-6,
        "Partial derivative of isotherm model w.r.t. temperature at contant " +
        "pressure is not valid: Deviation (|" +
        String(abs(dx_adsorpt_dT_adsorpt-dx_adsorpt_dT_adsorpt_num)) +
        "|) is greater than 1e-6 kg/(kg.K)!",
        level = AssertionLevel.warning);
    end if;

    if check_func_ddx_dp_dp then
      assert(abs(ddx_adsorpt_dp_adsorpt_dp_adsorpt-ddx_adsorpt_dp_adsorpt_dp_adsorpt_num) < 1e-6,
        "Second-order partial derivative of isotherm model w.r.t. pressure " +
        "at constant temperature is not valid: Deviation (|" +
        String(abs(ddx_adsorpt_dp_adsorpt_dp_adsorpt-ddx_adsorpt_dp_adsorpt_dp_adsorpt_num)) +
        "|) is greater than 1e-6 kg/(kg.Pa2)!",
        level = AssertionLevel.warning);
    end if;

    if check_func_ddx_dT_dT then
      assert(abs(ddx_adsorpt_dT_adsorpt_dT_adsorpt-ddx_adsorpt_dT_adsorpt_dT_adsorpt_num) < 1e-6,
        "Second-order partial derivative of isotherm model w.r.t. temperature " +
        "at constant pressure is not valid: Deviation (|" +
        String(abs(ddx_adsorpt_dT_adsorpt_dT_adsorpt-ddx_adsorpt_dT_adsorpt_dT_adsorpt_num)) +
        "|) is greater than 1e-6 kg/(kg.K2)!",
        level = AssertionLevel.warning);
    end if;

    if check_func_ddx_dp_dT then
      assert(abs(ddx_adsorpt_dp_adsorpt_dT_adsorpt-ddx_adsorpt_dp_adsorpt_dT_adsorpt_num) < 1e-6,
        "Second-order partial derivative of isotherm model w.r.t. pressure " +
        "and temperature is not valid: Deviation (|" +
        String(abs(ddx_adsorpt_dp_adsorpt_dT_adsorpt-ddx_adsorpt_dp_adsorpt_dT_adsorpt_num)) +
        "|) is greater than 1e-6 kg/(kg.Pa.K)!",
        level = AssertionLevel.warning);
    end if;
  end if;

  //
  // Annotations
  //
  annotation (Documentation(revisions="<html>
<ul>
  <li>
  November 1, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>", info="<html>
<p>
This partial model is the basic model for all testers of isotherm models describing
adsorption of pure components. Such isotherm models must be classical isotherm models,
which are based on the assumption of a (plain) boundary surface.
<br/><br/>
This partial model defines all relevant parameters, variables, and equations to
check (1) the classical form of isotherm models, (2) the inverse form of isotherm
models, (3) the partial derivative of the uptake w.r.t. to pressure, (4) the
partial derivative of the uptake w.r.t. pressure, (5) the partial derivative of the 
uptake w.r.t. temperature, (6) the second-order partial derivative of the uptake w.r.t. 
to pressure and temperature, (7) the second-order partial derivative of the uptake w.r.t. 
to temperature, and (8) the reduced spreading pressure. All partial derivatives are also 
implemented as central finite differences to check for the analytical implementation 
of the partial derivatives. Additionally, the reduced spreading pressure is also calculated 
numerically to check the numerical implementation of the reduced spreading pressure.
<br/><br/>
Models that inherit properties from this partial model have to specify the number 
of coefficients of the isotherm model <i>no_coefficients</i> and have to redeclare 
the <i>IsotherModel</i> package. Besides, the coefficients of the isotherm model (i.e.,  
<i>c</i>, <i>c_pdT</i>, and <i>c_mdT</i>) and their partial derivatives with respect to  
temperature (i.e., <i>dc_dT</i> and <i>ddc_dT_dT</i>) have to be implemented. Additionally, 
it has to be specified whether the equilibrium pressure (<i>p_adsorpt_der</i>) or the  
equilibrium temperature (<i>T_adsorpt_der</i>) changes with time.
</p>
</html>"));
end PartialTestPure;
