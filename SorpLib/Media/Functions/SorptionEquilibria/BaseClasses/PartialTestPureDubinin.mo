within SorpLib.Media.Functions.SorptionEquilibria.BaseClasses;
partial model PartialTestPureDubinin
  "Base model for testers of isotherm models describing pure component adsorption based on the Dubinin model"
  extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialTestPure(
      redeclare replaceable package IsothermModel =
        SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininAstakhov
        constrainedby
      SorpLib.Media.Functions.SorptionEquilibria.Interfaces.PartialPureComponentsDubinin);

  //
  // Definition of parameters
  //
  parameter Boolean check_func_A_W = true
    "= true, if function 'func_A_W' is checked; otherwise, function is not 
    checked"
    annotation (Dialog(tab="General", group="Checks"),
                Evaluate=true,
                HideResult=true);
  parameter Boolean check_func_dW_dA = true
    "= true, if function 'func_dW_dA' is checked; otherwise, function is not 
    checked"
    annotation (Dialog(tab="General", group="Checks"),
                Evaluate=true,
                HideResult=true);
  parameter Boolean check_func_ddW_dA_dA = true
    "= true, if function 'func_ddW_dA_dA' is checked; otherwise, function is not 
    checked"
    annotation (Dialog(tab="General", group="Checks"),
                Evaluate=true,
                HideResult=true);
  parameter Boolean check_func_ddW_dA_dT = true
    "= true, if function 'func_ddW_dA_dT' is checked; otherwise, function is not 
    checked"
    annotation (Dialog(tab="General", group="Checks"),
                Evaluate=true,
                HideResult=true);

  parameter SorpLib.Units.MolarAdsorptionPotential dA=1e-6
    "Molar adsorption potential difference used to calculate partial derivatives 
    numerically" annotation (
    Dialog(tab="Numerics", group="Derivatives"),
    Evaluate=true,
    HideResult=true);

  //
  // Definition of variables
  //
  SorpLib.Units.MolarAdsorptionPotential A "Molar adsorption potential";
  SorpLib.Units.MolarAdsorptionPotential A_inv(min=-1)
    "Molar adsorption potential calculated via inverse form of characteristic curve";
  SorpLib.Units.AdsorptionPotential A_mass "Adsorption potential";

  SorpLib.Units.FilledPoreVolume W
    "Filled pore volume";

  SorpLib.Units.DerFilledPoreVolumeByAdsorptionPotential dW_dA
    "Partial derivative of filled pore volume w.r.t. molar adsorption potential 
    at constant pressure and temperature";
  SorpLib.Units.DerFilledPoreVolumeByAdsorptionPotential dW_dA_num
    "Partial derivative of filled pore volume w.r.t. molar adsorption potential 
    calculated at constant pressure and temperature numerically";

  SorpLib.Units.DerFilledPoreVolumeByAdsorptionPotentialAdsorptionPotential ddW_dA_dA
    "Second-order partial derivative of filled pore volume w.r.t. molar adsorption 
    potential at constant pressure and temperature";
  SorpLib.Units.DerFilledPoreVolumeByAdsorptionPotentialAdsorptionPotential ddW_dA_dA_num
    "Second-order partial derivative of filled pore volume w.r.t. molar adsorption 
    potential at constant pressure and temperature calculated numerically";

  SorpLib.Units.DerFilledPoreVolumeByAdsorptionPotentialTemperature ddW_dA_dT
    "Second-order partial derivative of filled pore volume w.r.t. molar adsorption 
    potential and temperature at constant pressure";
  SorpLib.Units.DerFilledPoreVolumeByAdsorptionPotentialTemperature ddW_dA_dT_num
    "Second-order partial derivative of filled pore volume w.r.t. molar adsorption 
    potential and temperature at constant pressure calculated numerically";

protected
  SorpLib.Units.MolarAdsorptionPotential A_pdT=
    SorpLib.Media.Functions.SorptionEquilibria.Utilities.A_ppsT(
      p_adsorpt=p_adsorpt, p_sat=c_pdT[1], T_adsorpt=T_adsorpt+dT)
    "Molar adsorption potential_ T + dT";
  SorpLib.Units.MolarAdsorptionPotential A_mdT=
    SorpLib.Media.Functions.SorptionEquilibria.Utilities.A_ppsT(
      p_adsorpt=p_adsorpt, p_sat=c_mdT[1], T_adsorpt=T_adsorpt-dT)
    "Molar adsorption potential: T - dT";

equation
  A =SorpLib.Media.Functions.SorptionEquilibria.Utilities.A_ppsT(
    p_adsorpt=p_adsorpt,
    p_sat=c[1],
    T_adsorpt=T_adsorpt)
    "Molar adsorption potential";

  if check_func_A_W then
    A_inv = IsothermModel.A_W(W=W, c=c)
      "Molar adsorption potential calculated via inverse form of characteristic curve";
  else
    A_inv = -1
      "Molar adsorption potential calculated via inverse form of characteristic curve";
  end if;

  A_mass = A / M_adsorptive
    "Adsorption potential";

  W = IsothermModel.W_A(A=A, c=c)
    "Filled pore volume";

  if check_func_dW_dA then
    dW_dA = IsothermModel.dW_dA(A=A, c=c)
      "Partial derivative of filled pore volume w.r.t. molar adsorption potential 
      at constant pressure and temperature";
    dW_dA_num = (IsothermModel.W_A(A=A+dA, c=c) - IsothermModel.W_A(A=A-dA, c=c)) / (2*dA)
      "Partial derivative of filled pore volume w.r.t. molar adsorption potential 
      at constant pressure and temperature calculated numerically";
  else
    dW_dA = -1
      "Partial derivative of filled pore volume w.r.t. molar adsorption potential 
      at constant pressure and temperature";
    dW_dA_num = -1
      "Partial derivative of filled pore volume w.r.t. molar adsorption potential 
      at constant pressure and temperature calculated numerically";
  end if;

  if check_func_ddW_dA_dA then
    ddW_dA_dA = IsothermModel.ddW_dA_dA(A=A, c=c)
      "Second-order partial derivative of filled pore volume w.r.t. molar adsorption 
      potential at constant pressure and temperature";
    ddW_dA_dA_num = (IsothermModel.W_A(A=A+dA, c=c) -
      2 * IsothermModel.W_A(A=A, c=c) + IsothermModel.W_A(A=A-dA, c=c)) / (dA^2)
      "Second-order partial derivative of filled pore volume w.r.t. molar adsorption 
      potential at constant pressure and temperature calculated numerically";
  else
    ddW_dA_dA = -1
      "Second-order partial derivative of filled pore volume w.r.t. molar adsorption 
      potential at constant pressure and temperature";
    ddW_dA_dA_num = -1
      "Second-order partial derivative of filled pore volume w.r.t. molar adsorption 
      potential at constant pressure and temperature calculated numerically";
  end if;

  if check_func_ddW_dA_dT then
    ddW_dA_dT = IsothermModel.ddW_dA_dT(p_adsorpt=p_adsorpt, T_adsorpt=T_adsorpt, A=A,
      c=c, dc_dT_adsorpt=dc_dT)
      "Second-order partial derivative of filled pore volume w.r.t. molar adsorption 
      potential and temperature at constant pressure";
    ddW_dA_dT_num = (IsothermModel.dW_dA(A=A_pdT, c=c_pdT) - IsothermModel.dW_dA(A=A_mdT, c=c_mdT)) /
      (2*dT)
      "Second-order partial derivative of filled pore volume w.r.t. molar adsorption 
      potential and temperature at constant pressure calculated numerically";
  else
    ddW_dA_dT = -1
      "Second-order partial derivative of filled pore volume w.r.t. molar adsorption 
      potential and temperature at constant pressure";
    ddW_dA_dT_num = -1
      "Second-order partial derivative of filled pore volume w.r.t. molar adsorption 
      potential and temperature at constant pressure calculated numerically";
  end if;

  //
  // Definition of assertions: Check numerical implementation of isotherm model
  //
  if print_asserts then
    if check_func_A_W then
      assert(abs(A-A_inv) < 1e-6,
        "Inverse function of characteristic curve is not valid: Deviation (|"
        + String(abs(A-A_inv)) +
        "|) is greater than 1e-6 J/mol!",
        level = AssertionLevel.warning);
    end if;

    if check_func_dW_dA then
      assert(abs(dW_dA-dW_dA_num) < 1e-6,
        "Partial derivative of filled pore volume wrt. adsorption potential " +
        "at constant pressure and temperature is not valid: Deviation (|" +
        String(abs(dW_dA-dW_dA_num)) +
        "|) is greater than 1e-6 m3.mol/(kg.J)!",
        level = AssertionLevel.warning);
    end if;

    if check_func_ddW_dA_dA then
      assert(abs(ddW_dA_dA-ddW_dA_dA_num) < 1e-6,
        "Second-order partial derivative of filled pore volume wrt. adsorption " +
        "potential at constant pressure and temperature is not valid: Deviation (|" +
        String(abs(ddW_dA_dA-ddW_dA_dA_num)) +
        "|) is greater than 1e-6 m3.mol2/(kg.J2)!",
        level = AssertionLevel.warning);
    end if;

    if check_func_ddW_dA_dT then
      assert(abs(ddW_dA_dT-ddW_dA_dT_num) < 1e-6,
        "Second-order partial derivative of filled pore volume wrt. adsorption " +
        "potential and temperature at constant pressure is not valid: Deviation (|" +
        String(abs(ddW_dA_dT-ddW_dA_dT_num)) +
        "|) is greater than 1e-6 m3.mol/(kg.J.K)!",
        level = AssertionLevel.warning);
    end if;
  end if;

  //
  // Annotations
  //
  annotation (Documentation(revisions="<html>
<ul>
  <li>
  November 1, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>", info="<html>
<p>
This partial model is the basic model for all testers of isotherm models describing
adsorption of pure components. Such isotherm models must be isotherm models, which 
are based on the assumpation of filled pores.
<br/><br/>
This partial model defines all relevant parameters, variables, and equations to
check (1) the classical form of isotherm models, (2) the inverse form of isotherm
models, (3) the partial derivative of the uptake w.r.t. to pressure, (4) the
partial derivative of the uptake w.r.t. pressure, (5) the partial derivative of the 
uptake w.r.t. temperature, (6) the second-order partial derivative of the uptake w.r.t. 
to pressure and temperature, (7) the second-order partial derivative of the uptake w.r.t. 
to temperature, (8) the reduced spreading pressure, (9) the characteristic curve, (10) 
the inverse form of the characteristic curve, and (11) partial derivative of the 
characteristic curve w.r.t. molar adsorption potential. All partial derivatives are 
also implemented as central finite  differences to check for the analytical implementation 
of the partial derivatives. Additionally, the reduced spreading pressure is also 
calculated numerically to check the numerical implementation of the reduced spreading 
pressure.
<br/><br/>
Models that inherit properties from this partial model have to specify the number 
of coefficients of the isotherm model <i>no_coefficients</i> and have to redeclare 
the <i>IsotherModel</i> package. Besides, the coefficients of the isotherm 
model (i.e., <i>c</i>, <i>c_pdT</i>, and <i>c_mdT</i>) and their partial derivatives 
with respect to temperature (i.e., <i>dc_dT</i>) have to be implemented. Additionally, 
it has to specify whether the equilibrium pressure (<i>p_adsorpt_der</i>) or the 
equilibrium temperature (<i>T_adsorpt_der</i>) changes with time.
</p>
</html>"));
end PartialTestPureDubinin;
