within SorpLib.Media.Functions.SorptionEquilibria;
package BaseClasses "Base classes used to build new functions calculating sorption equlibria"
  extends Modelica.Icons.BasesPackage;

  annotation (Documentation(info="<html>
<p>
This package contains partial basic functions and models. These partial functions
and models contain fundamental definitions of all isotherm models and corresponding
test models, covering pure and multi-component adsorption. The content of this package 
is only of interest when adding new isotherm models to the library.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end BaseClasses;
