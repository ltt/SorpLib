within SorpLib.Media.Functions.SorptionEquilibria.BaseClasses;
partial function PartialPure_p_xT
  "Base function for isotherm models of pure components: Pressure as function of uptake and temperature"

  //
  // Definition of inputs
  //
  input SorpLib.Units.Uptake x_adsorpt
    "Equilibrium uptake of adsorpt phase"
    annotation (Dialog(tab="General", group="Inputs"));
  extends SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure;

  input Modelica.Units.SI.Pressure p_adsorpt_lb_start
    "Lower bound of equilibrium pressure (required if pressure is calculated 
    numerically (i.e., root finding))"
    annotation (Dialog(tab="General", group="Numerical inputs"));
  input Modelica.Units.SI.Pressure p_adsorpt_ub_start
    "Upper bound of equilibrium pressure (required if pressure is calculated 
    numerically (i.e., root finding))"
    annotation (Dialog(tab="General", group="Numerical inputs"));
  input Real tolerance
    "Tolerance for numerical calculation (required if pressure is calculated
    numerically (i.e., root finding))"
    annotation (Dialog(tab="General", group="Numerical inputs"));

  /* Note that optional numerical inputs do not have default values as this 
  function is used as functional input argument within the "Ideal Adsorbed
  Solution Theory," a multi-component adsorption isotherm model. Using default
  values resulted in translation errors. */

  //
  // Definition of outputs
  //
  output Modelica.Units.SI.Pressure p_adsorpt
    "Equilibrium pressure of adsorpt phase"
    annotation (Dialog(tab="General", group="Outputs", enable=false));

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial function is the basic function for all isotherm models describing 
the adsorption of pure components. Such models can be classical isotherm models 
based on a (plain) boundary surface or isotherm models based on filled pores.
<br/><br/>
This partial function is the basic function for calculating the equilibrium 
pressure <i>p_adsorpt</i> as a function of the equilibrium uptake <i>x_adsorpt</i> 
and equilibrium temperature <i>T_adsorpt</i>. Defined inputs are the equilibrium 
uptake <i>x_adsorpt</i>, the equilibrium temperature <i>T_adsorpt</i>, and the 
coefficients of the isotherm model <i>c</i> as inputs. The coefficients of the
isotherm model <i>c</i> may depend on the equilibrium temperature <i>T_adsorpt</i>.
Optional inputs regarding numerics are the lower bound (<i>p_adsorpt_lb_start</i>) 
and upper bound (<i>p_adsorpt_ub_start</i>) of the equilibrium pressure and the 
tolerance (<i>tolerance</i>), only required if this function cannot be solved 
analytically. Besides, the equilibrium pressure <i>p_adsorpt</i> is defined as 
the output.
<br/><br/>
Functions that inherit properties from this partial function may have to implement
further inputs and the function algorithm.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 1, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end PartialPure_p_xT;
