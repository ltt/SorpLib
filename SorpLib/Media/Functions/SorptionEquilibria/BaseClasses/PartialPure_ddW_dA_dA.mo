within SorpLib.Media.Functions.SorptionEquilibria.BaseClasses;
partial function PartialPure_ddW_dA_dA
  "Base function for isotherm models of pure components: Second-order partial derivative of filled pore volume w.r.t. molar adsorption potential at constant pressure and temperature"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input SorpLib.Units.MolarAdsorptionPotential A
    "Molar adsorption potential"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real[:] c
    "Coefficients of the isotherm model"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of outputs
  //
  output
    SorpLib.Units.DerFilledPoreVolumeByAdsorptionPotentialAdsorptionPotential ddW_dA_dA
    "Second-order partial derivative of filled pore volume w.r.t. molar adsorption 
    potential at constant pressure and temperature"
    annotation (Dialog(
      tab="General",
      group="Outputs",
      enable=false));

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial function is the basic function for all isotherm models describing 
the adsorption of pure components. Such models must be based on filled pores
(i.e., Dubinin theory).
<br/><br/>
This partial function is the basic function for calculating the second-order partial 
derivative of the filled pore volume with respect to the molar adsorption potential 
at constant pressure and temperature <i>ddW_dA_dA</i> as a function of the molar 
adsorption potential <i>A</i>. Defined inputs are the molar  adsorption potential 
<i>A</i> and the coefficients of the isotherm model <i>c</i>. The coefficients of 
the isotherm model <i>c</i> may depend on the equilibrium temperature <i>T_adsorpt</i>. 
The defined output is the second-order partial derivative of the filled pore volume 
with respect to the molar adsorption potential at constant pressure and temperature
<i>ddW_dA_dA</i>.
<br/><br/>
Functions that inherit properties from this partial function may have to implement
further inputs and the function algorithm.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 2, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end PartialPure_ddW_dA_dA;
