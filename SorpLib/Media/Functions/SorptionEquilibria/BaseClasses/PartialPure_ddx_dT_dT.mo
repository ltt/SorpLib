within SorpLib.Media.Functions.SorptionEquilibria.BaseClasses;
partial function PartialPure_ddx_dT_dT
  "Base function for isotherm models of pure components: Second-order partial derivative of uptake w.r.t. temperature at constant pressure"

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.Pressure p_adsorpt
    "Equilibrium pressure of adsorpt phase"
    annotation (Dialog(tab="General", group="Inputs"));
  extends SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure;
  input Real[:] dc_dT_adsorpt
    "Partial derivatives of coefficients of isotherm model w.r.t. temperature"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real[:] ddc_dT_adsorpt_dT_adsorpt
    "Second-order partial derivatives of coefficients of isotherm model w.r.t. 
    temperature"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of outputs
  //
  output SorpLib.Units.DerUptakeByTemperatureTemperature
    ddx_adsorpt_dT_adsorpt_dT_adsorpt
    "Second-order partial derivative of uptake w.r.t. temperature at constant pressure"
    annotation (Dialog(tab="General", group="Outputs", enable=false));

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial function is the basic function for all isotherm models describing 
the adsorption of pure components. Such models can be classical isotherm models 
based on a (plain) boundary surface or isotherm models based on filled pores.
<br/><br/>
This partial function is the basic function for calculating the second-order 
partial derivative of the equilibrium uptake with respect to the equilibrium 
temperature at constant pressure <i>ddx_adsorpt_dT_adsorpt_dT_adsorpt</i> as a 
function of the equilibrium pressure <i>p_adsorpt</i> and equilibrium temperature 
<i>T_adsorpt</i>. Defined inputs are the equilibrium pressure <i>p_adsorpt</i>, the 
equilibrium temperature <i>T_adsorpt</i>, the coefficients of the isotherm model 
<i>c</i>, the partial derivatives ot the coefficients with respect to the equilibrium 
temperatre <i>dc_dT_adsorpt</i>, and the second-order partial derivatives ot the 
coefficients with respect to the equilibrium temperatre <i>ddc_dT_adsorpt_dT_adsorpt</i>. 
The coefficients of the isotherm model <i>c</i> may depend on the equilibrium 
temperature <i>T_adsorpt</i>. Besides, this partial function defines the second-
order partial derivative of the equilibrium uptake with respect to the equilibrium 
temperature at constant pressure <i>ddx_adsorpt_dT_adsorpt_dT_adsorpt</i> as the output.
<br/><br/>
Functions that inherit properties from this partial function may have to implement 
further inputs and the function algorithm.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 1, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end PartialPure_ddx_dT_dT;
