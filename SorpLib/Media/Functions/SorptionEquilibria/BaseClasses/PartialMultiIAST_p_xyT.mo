within SorpLib.Media.Functions.SorptionEquilibria.BaseClasses;
partial function PartialMultiIAST_p_xyT
  "Base function for isotherm models based on the ideal adsorbed solution theory: Pressure as function of uptakes, mole fractions of independent gas phase components, and temperature"
  extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialMultiIAST;

  //
  // Definition of inputs
  //
  input SorpLib.Units.Uptake[size(M_i,1)] x_adsorpt
    "Equilibrium uptakes of the adsorpt phase"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.MoleFraction[size(M_i,1)-1] y_i
    "Mole fractions of independent components in the vapor or gas phase"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of outputs
  //
  output Modelica.Units.SI.Pressure p_adsorpt
    "Equilibrium pressure of the adsorpt phase"
    annotation (Dialog(tab="General", group="Outputs", enable=false));

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial function is the basic function for all isotherm models based on
the ideal adsorbed solution theory (IAST) describing the adsorption of multi 
components.
<br/><br/>
This partial function is the basic function for calculating the equilibrium uptakes 
<i>x_adsorpt</i> as a function of the equilibrium pressure <i>p_adsorpt</i>, the
mole fractions of the independent components in the gas or vapor phase <i>y_i</i>, 
the equilibrium temperature <i>T_adsorpt</i>. Further inputs are the coefficients 
of the isotherm model of the first component <i>c_1</i> and second component 
<i>c_2</i>. The coefficients of the isotherm model <i>c_i</i> may depend on the 
equilibrium temperature <i>T_adsorpt</i>. Further inputs are functions describing 
the pure component isotherm models. These functional arguments are <i>func_x_pT_1</i>, 
<i>func_p_xT_1</i>, <i>func_dx_dp_1</i>, <i>func_pi_pT_1</i>, and <i>func_p_piT_1</i> 
for component 1 (i.e., <i>i = 1</i>) and 2 (i.e., <i>i = 2</i>). Optional input 
regarding numerics define the setup of the IAST algorithm <i>num</i> and the pure 
component isotherm models <i>num_comp_i</i>.
<br/><br/>
The equilibrium pressure <i>p_adsorpt</i> is defined as the output.
<br/><br/>
Functions that inherit properties from this partial function may have to implement
further inputs and the function algorithm. Further inputs are particulary required 
when developing IAST functions for more than two components.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 10, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end PartialMultiIAST_p_xyT;
