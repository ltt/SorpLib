within SorpLib.Media.Functions.SorptionEquilibria.BaseClasses;
partial function PartialPure_dx_dT
  "Base function for isotherm models of pure components: Partial derivative of uptake w.r.t. temperature at constant pressure"

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.Pressure p_adsorpt
    "Equilibrium pressure of adsorpt phase"
    annotation (Dialog(tab="General", group="Inputs"));
  extends SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure;
  input Real[:] dc_dT_adsorpt
    "Partial derivatives of coefficients of isotherm model w.r.t. temperature"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of outputs
  //
  output SorpLib.Units.DerUptakeByTemperature dx_adsorpt_dT_adsorpt
    "Partial derivative of the uptake w.r.t. temperature at constant pressure"
    annotation (Dialog(tab="General", group="Outputs", enable=false));

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial function is the basic function for all isotherm models describing 
the adsorption of pure components. Such models can be classical isotherm models 
based on a (plain) boundary surface or isotherm models based on filled pores.
<br/><br/>
This partial function is the basic function for calculating the partial derivative
of the equilibrium uptake with respect to the equilibrium temperature at constant 
pressure <i>dx_adsorpt_dT_adsorpt</i> as a function of the equilibrium pressure 
<i>p_adsorpt</i> and equilibrium temperature <i>T_adsorpt</i>. Defined inputs are 
the equilibrium pressure <i>p_adsorpt</i>, the equilibrium temperature <i>T_adsorpt</i>, 
the coefficients of the isotherm model <i>c</i>, and the partial derivatives ot the 
coefficients with respect to the equilibrium temperatre <i>dc_dT_adsorpt</i>. The 
coefficients of the isotherm model <i>c</i> may depend on the equilibrium temperature 
<i>T_adsorpt</i>. Besides, this partial function defines the partial derivative of 
the equilibrium uptake with respect to the equilibrium temperature at constant pressure
<i>dx_adsorpt_dT_adsorpt</i> as the output.
<br/><br/>
Functions that inherit properties from this partial function may have to implement 
further inputs and the function algorithm.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 1, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end PartialPure_dx_dT;
