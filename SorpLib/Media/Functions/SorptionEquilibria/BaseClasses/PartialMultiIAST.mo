within SorpLib.Media.Functions.SorptionEquilibria.BaseClasses;
partial function PartialMultiIAST
  "Base function for isotherm models based on the ideal adsorbed solution theory"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.Temperature T_adsorpt
    "Equilibrium temperature of the adsorpt phase"
    annotation (Dialog(tab="General", group="Inputs"));

  input Modelica.Units.SI.MolarMass[:] M_i
    "Molar masses of the components"
    annotation (Dialog(tab="General", group="Inputs - Components"));
  input Real[:] c_1
    "Coefficients of the isotherm model of the first component"
    annotation (Dialog(tab="General", group="Inputs - Components"));
  input Real[:] c_2
    "Coefficients of the isotherm model of the second component"
    annotation (Dialog(tab="General", group="Inputs - Components"));

  input
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_x_pT func_x_pT_1
    "Uptake of the first component as function of pressure and temperature"
    annotation (Dialog(tab="General", group="Inputs - Functions"));
  input
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_p_xT func_p_xT_1
    "Pressure of the first component as function of uptake and temperature"
    annotation (Dialog(tab="General", group="Inputs - Functions"));
  input
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_dx_dp func_dx_dp_1
    "Partial derivative of the uptake of the first component w.r.t. the equilibrium 
    pressure"
    annotation (Dialog(tab="General", group="Inputs - Functions"));
  input
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_pi_pT func_pi_pT_1
    "Reduced spreading pressure of the first component as function of pressure 
    and temperature"
    annotation (Dialog(tab="General", group="Inputs - Functions"));
  input
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_p_piT func_p_piT_1
    "Pressure of the first component as function of reduced spreading pressure
    and temperature"
    annotation (Dialog(tab="General", group="Inputs - Functions"));

  input
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_x_pT func_x_pT_2
    "Uptake of the second component as function of pressure and temperature"
    annotation (Dialog(tab="General", group="Inputs - Functions"));
  input
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_p_xT func_p_xT_2
    "Pressure of the second component as function of uptake and temperature"
    annotation (Dialog(tab="General", group="Inputs - Functions"));
  input
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_dx_dp func_dx_dp_2
    "Partial derivative of the uptake of the second component w.r.t. the equilibrium 
    pressure"
    annotation (Dialog(tab="General", group="Inputs - Functions"));
  input
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_pi_pT func_pi_pT_2
    "Reduced spreading pressure of the second component as function of pressure 
    and temperature"
    annotation (Dialog(tab="General", group="Inputs - Functions"));
  input
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_p_piT func_p_piT_2
    "Pressure of the second component as function of reduced spreading pressure
    and temperature"
    annotation (Dialog(tab="General", group="Inputs - Functions"));

  input SorpLib.Media.Functions.SorptionEquilibria.Records.NumericsIAST num
    "Record definining numerics of the IAST algorithms"
    annotation (Dialog(tab="General", group="Inputs - Numerics"),
                choicesAllMatching=true);
  input SorpLib.Media.Functions.SorptionEquilibria.Records.NumericsIAST_PureComponents num_comp_1
    "Record definining numerics of the first component's isotherm model"
    annotation (Dialog(tab="General", group="Inputs - Numerics"),
                choicesAllMatching=true);
  input SorpLib.Media.Functions.SorptionEquilibria.Records.NumericsIAST_PureComponents num_comp_2
    "Record definining numerics of the second component's isotherm model"
    annotation (Dialog(tab="General", group="Inputs - Numerics"),
                choicesAllMatching=true);

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial function is the basic function for all isotherm models based on
the ideal adsorbed solution theory (IAST) describing the adsorption of multi 
components.
<br/><br/>
This partial function defines the equilibrium temperature <i>T_adsorpt</i>, the
molar masses <i>M_i</i>, and the coefficients of the isotherm model of the first
component <i>c_1</i> and second component <i>c_2</i> as inputs. The coefficients 
of the isotherm model <i>c_i</i> may depend on the equilibrium temperature <i>T_adsorpt</i>. 
Further inputs are functions describing the pure component isotherm models. These
functional arguments are <i>func_x_pT_1</i>, <i>func_p_xT_1</i>, <i>func_dx_dp_1</i>, 
<i>func_pi_pT_1</i>, and <i>func_p_piT_1</i> for component 1 (i.e., <i>i = 1</i>) and 
2 (i.e., <i>i = 2</i>). Optional input regarding numerics define the setup of the
IAST algorithm <i>num</i> and the pure component isotherm models <i>num_comp_i</i>.
<br/><br/>
Functions that inherit properties from this partial function may have to implement 
further inputs, the output, and the function algorithm. Further inputs are 
particulary required when developing IAST functions for more than two components.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 10, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end PartialMultiIAST;
