within SorpLib.Media.Functions.SorptionEquilibria.PureComponents;
package Henry "Package containing all functions regarding the Henry isotherm"
extends
  SorpLib.Media.Functions.SorptionEquilibria.Interfaces.PartialPureComponents;

  redeclare final function extends x_pT
    "Henry isotherm model: Uptake as function of pressure and temperature"
  algorithm
    x_adsorpt := c[1]*p_adsorpt
      "Calculation of the equilibrium uptake of the adsorpt phase";

    //
    // Annotations
    //
    annotation (Inline=false,
                InlineAfterIndexReduction=false,
                LateInline=true,
                inverse(p_adsorpt=SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Henry.p_xT(x_adsorpt, T_adsorpt, c, p_adsorpt_lb_start, p_adsorpt_ub_start, tolerance)));
  end x_pT;

  redeclare final function extends p_xT
    "Henry isotherm model: Pressure as function of uptake and temperature"
  algorithm
    p_adsorpt := x_adsorpt/c[1]
      "Calculation of the equilibrium pressure of the adsorpt phase";

    //
    // Annotations
    //
    annotation (Inline=false,
                InlineAfterIndexReduction=false,
                LateInline=true,
                inverse(x_adsorpt=SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Henry.x_pT(p_adsorpt, T_adsorpt, c, p_adsorpt_lb_start, p_adsorpt_ub_start, tolerance)));
  end p_xT;

  redeclare final function extends dx_dp
    "Henry isotherm model: Partial derivative of uptake w.r.t. pressure at constant temperature"
  algorithm
    dx_adsorpt_dp_adsorpt := c[1]
      "Calculation of the partial derivative of the equilibrium uptake w.r.t. the
     equilibrium pressure at constant temperature";
  end dx_dp;

  redeclare final function extends dx_dT
    "Henry isotherm model: Partial derivative of uptake w.r.t. temperature at constant pressure"
  algorithm
    dx_adsorpt_dT_adsorpt := p_adsorpt*dc_dT_adsorpt[1]
      "Calculation of the partial derivative of the equilibrium uptake w.r.t. the
     equilibrium temperature at constant pressure";
  end dx_dT;

  redeclare final function extends ddx_dp_dp
    "Henry isotherm model: Second-order partial derivative of uptake w.r.t. pressure at constant temperature"
  algorithm
    ddx_adsorpt_dp_adsorpt_dp_adsorpt := 0
      "Calculation of the second-order partial derivative of the equilibrium uptake 
    w.r.t. the equilibrium pressure at constant temperature";
  end ddx_dp_dp;

  redeclare final function extends ddx_dT_dT
    "Henry isotherm model: Second-order partial derivative of uptake w.r.t. temperature at constant pressure"
  algorithm
    ddx_adsorpt_dT_adsorpt_dT_adsorpt := p_adsorpt*ddc_dT_adsorpt_dT_adsorpt[1]
      "Calculation of the second-order partial derivative of the equilibrium uptake 
    w.r.t. the equilibrium temperature at constant pressure";
  end ddx_dT_dT;

  redeclare final function extends ddx_dp_dT
    "Henry isotherm model: Second-order partial derivative of uptake w.r.t. pressure and temperature"
  algorithm
    ddx_adsorpt_dp_adsorpt_dT_adsorpt := dc_dT_adsorpt[1]
      "Calculation of the second-order partial derivative of the equilibrium uptake 
     w.r.t. the equilibrium pressure and temperature";
  end ddx_dp_dT;

  redeclare final function extends pi_pT
    "Henry isotherm model: Reduced spreading pressure as function of pressure and temperature"
  algorithm
    pi := 1/M_adsorptive * (c[1]*p_adsorpt)
      "Calculation of the reduced spreading pressure";
  end pi_pT;

  redeclare final function extends p_piT
    "Henry isotherm model: Pressure as function of reduced spreading pressure and temperature"
  algorithm
    p_adsorpt := M_adsorptive * pi / c[1]
      "Calculation of the equilibrium pressure";
  end p_piT;
  //
  // Annotations
  //
annotation (Documentation(revisions="<html>
<ul>
  <li>
  November 1, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>", info="<html>
<p>
The Henry isotherm model calculates the equilibrium uptake <i>x_adsorpt</i> in 
linear relation to the equilibrium pressure <i>p_adsorpt</i>. The proportionality 
coefficient is the so-called Henry coefficient <i>H</i>, which may depend on the 
equilibrium temperature <i>T_adsorpt</i>.
</p>

<h4>Main equations</h4>
<p>
The Henry isotherm model has the following form:
</p>
<pre>
    x<sub>adsorpt</sub> = H(T<sub>adsorpt</sub>) * p<sub>adsorpt</sub>;
</pre>
<p>
where <i>H</i> is the temperature-dependent Henry coefficient. A typical temperature
dependency may have the following form:
</p>
<pre>
    H(T<sub>adsorpt</sub>) = H<sub>ref</sub> * <strong>exp</strong>(C<sub>exp</sub> * (1/T<sub>adsorpt</sub> - 1/T<sub>ref</sub>));
</pre>
<p>
where <i>H<sub>ref</sub></i> is the Henry coefficient at reference temperature 
<i>T<sub>ref</sub></i> and <i>C<sub>exp</sub></i> describes the change of the Henry
coefficient with the temperature. All three parameters can be used as fitting
parameters.
</p>

<h4>Required parameter order in function input c[:]:</h4>
<ul>
  <li>
  c[1] = H(T<sub>adsorpt</sub>) in 1/Pa
  </li>
</ul>

<h4>Assumptions and limitations</h4>
<ul>
  <li>
  All adsorption sites are energetically equivalent.
  </li>
  <li>
  All adsorption sites can be occupied.
  </li>
  <li>
  No interactions occur between the adsorbent molecules.
  </li>
</ul>

<h4>Typical use</h4>
<p>
The isotherm model is used to calculate the equilibrium uptake <i>x_adsorpt</i> 
at very low equilibrium pressures <i>p_adsorpt</i>.
</p>

<h4>Example</h4>
<p>
The following figure shows the Henry isotherm model for different reference 
values of the Henry coefficient <i>H_ref</i>. In the upper sub-figure, the 
equilibrium pressure changes with time, while the equilibrium temperature
remains constant; in the lower sub-figure, the equilibrium temperature 
changes with time, while the equilibrium pressure remains constant. 
<br/><br/>
</p>
<img src=\"Modelica://SorpLib/Resources/doc/media_functions_equilibria_pure_henry.png\" alt=\"media_functions_equilibria_pure_henry.png\">

<h4>References</h4>
<ul>
  <li>
  Bathen, D. and Breitbach, M. (2001). Adsorptionstechnik (in German), 1st Edition, ISBN 3-540-41908-X, Springer-Verlag Berlin Heidelberg New York.
  </li>
</ul>
</html>"));
end Henry;
