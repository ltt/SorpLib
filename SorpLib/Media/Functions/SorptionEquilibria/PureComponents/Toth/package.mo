within SorpLib.Media.Functions.SorptionEquilibria.PureComponents;
package Toth "Package containing all functions regarding the Toth isotherm"
extends
  SorpLib.Media.Functions.SorptionEquilibria.Interfaces.PartialPureComponents;

  redeclare final function extends x_pT
    "Toth isotherm model: Uptake as function of pressure and temperature"
  algorithm
    x_adsorpt := c[1] * (c[2]*p_adsorpt) / ((1 + (c[2]*p_adsorpt)^c[3])^(1/c[3]))
      "Calculation of the equilibrium uptake of the adsorpt phase";

    //
    // Annotations
    //
    annotation (Inline=false,
  InlineAfterIndexReduction=false,
  LateInline=true,
  inverse(p_adsorpt=SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Toth.p_xT(x_adsorpt, T_adsorpt, c, p_adsorpt_lb_start, p_adsorpt_ub_start, tolerance)));
  end x_pT;

  redeclare final function extends p_xT
    "Toth isotherm model: Pressure as function of uptake and temperature"
  algorithm
    p_adsorpt := x_adsorpt/c[2] * 1 / (c[1]^c[3] - x_adsorpt^c[3]) ^ (1/c[3])
      "Calculation of the equilibrium pressure of the adsorpt phase";

    //
    // Annotations
    //
    annotation (Inline=false,
  InlineAfterIndexReduction=false,
  LateInline=true,
  inverse(x_adsorpt=SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Toth.x_pT(p_adsorpt, T_adsorpt, c, p_adsorpt_lb_start, p_adsorpt_ub_start, tolerance)));
  end p_xT;

  redeclare final function extends dx_dp
    "Toth isotherm model: Partial derivative of uptake w.r.t. pressure at constant temperature"
  algorithm
    dx_adsorpt_dp_adsorpt := c[1]*c[2] * ((c[2]*p_adsorpt)^c[3] + 1) ^
      (-1/c[3] - 1)
      "Calculation of the partial derivative of the equilibrium uptake w.r.t. the
     equilibrium pressure at constant temperature";
  end dx_dp;

  redeclare final function extends dx_dT
    "Toth isotherm model: Partial derivative of uptake w.r.t. temperature at constant pressure"
    //
    // Definition of variables
    //
protected
    Real dx_adsorpt_dc1 = c[2] * p_adsorpt /
      ((1 + (c[2]*p_adsorpt)^c[3])^(1/c[3]))
      "Derivative of uptake w.r.t. to first coefficient of Toth isotherm";
    Real dx_adsorpt_dc2 = c[1] * p_adsorpt *
      ((c[2]*p_adsorpt)^c[3] + 1) ^ (-1/c[3] - 1)
      "Derivative of uptake w.r.t. to second coefficient of Toth isotherm";
    Real dx_adsorpt_dc3 = c[1] * c[2] * p_adsorpt *
      (log(1 + (c[2]*p_adsorpt)^c[3]) / c[3]^2 -
      (c[2]*p_adsorpt)^c[3] * log(c[2]*p_adsorpt) /
      (c[3] * (1 + (c[2]*p_adsorpt)^c[3]))) /
      ((1 + (c[2]*p_adsorpt)^c[3])^(1/c[3]))
      "Derivative of uptake w.r.t. to third coefficient of Toth isotherm";

  algorithm
    dx_adsorpt_dT_adsorpt :=
      dx_adsorpt_dc1*dc_dT_adsorpt[1] +
      dx_adsorpt_dc2*dc_dT_adsorpt[2] +
      dx_adsorpt_dc3*dc_dT_adsorpt[3]
      "Calculation of the partial derivative of the equilibrium uptake w.r.t. the
     equilibrium temperature at constant pressure";
  end dx_dT;

  redeclare final function extends ddx_dp_dp
    "Toth isotherm model: Second-order partial derivative of uptake w.r.t. pressure at constant temperature"
  algorithm
    ddx_adsorpt_dp_adsorpt_dp_adsorpt := -(c[1] * c[2] * (c[3] + 1) *
      (c[2] * p_adsorpt)^c[3]) /
      (p_adsorpt * ((c[2] * p_adsorpt)^c[3] + 1)^((2 * c[3] + 1) / c[3]))
      "Calculation of the second-order partial derivative of the equilibrium uptake 
    w.r.t. the equilibrium pressure at constant temperature";
  end ddx_dp_dp;

  redeclare final function extends ddx_dT_dT
    "Toth isotherm model: Second-order partial derivative of uptake w.r.t. temperature at constant pressure"

    //
    // Definition of variables
    //
protected
    Real dx_adsorpt_dc1 = c[2] * p_adsorpt /
      ((1 + (c[2]*p_adsorpt)^c[3])^(1/c[3]))
      "Derivative of uptake w.r.t. to first coefficient of Toth isotherm";
    Real dx_adsorpt_dc2 = c[1] * p_adsorpt *
      ((c[2]*p_adsorpt)^c[3] + 1) ^ (-1/c[3] - 1)
      "Derivative of uptake w.r.t. to second coefficient of Toth isotherm";
    Real dx_adsorpt_dc3 = c[1] * c[2] * p_adsorpt *
      (log(1 + (c[2]*p_adsorpt)^c[3]) / c[3]^2 -
      (c[2]*p_adsorpt)^c[3] * log(c[2]*p_adsorpt) /
      (c[3] * (1 + (c[2]*p_adsorpt)^c[3]))) /
      ((1 + (c[2]*p_adsorpt)^c[3])^(1/c[3]))
      "Derivative of uptake w.r.t. to third coefficient of Toth isotherm";

    Real ddx_adsorpt_dc1_dc2 = p_adsorpt * ((p_adsorpt * c[2])^c[3] + 1) ^
      (-1 / c[3] - 1)
      "Second-order partial derivative of uptake w.r.t. to first and second 
    coefficient of Toth isotherm";
    Real ddx_adsorpt_dc1_dc3 = (c[2] * p_adsorpt * (log((c[2] * p_adsorpt)^c[3] + 1) /
      c[3]^2 - ((c[2] * p_adsorpt)^c[3] * log(c[2] * p_adsorpt)) /
      (((c[2] * p_adsorpt)^c[3] + 1) * c[3]))) /
      ((c[2] * p_adsorpt)^c[3] + 1)^(1 / c[3])
      "Second-order partial derivative of uptake w.r.t. to first and third 
    coefficient of Toth isotherm";

    Real ddx_adsorpt_dc2_dc2 = -(c[1] * (c[3] + 1) * p_adsorpt *
      (p_adsorpt * c[2])^c[3] * ((p_adsorpt * c[2])^c[3] + 1)^(-1 / c[3] - 2)) / c[2]
      "Second-order partial derivative of uptake w.r.t. to second coefficient of 
    Toth isotherm";
    Real ddx_adsorpt_dc2_dc3 = c[1] * p_adsorpt * ((c[2] * p_adsorpt)^c[3] + 1) ^
      (-1 / c[3] - 1) * (((c[2] * p_adsorpt)^c[3] * log(c[2] * p_adsorpt) *
      (-1 / c[3] - 1)) / ((c[2] * p_adsorpt)^c[3] + 1) +
      log((c[2] * p_adsorpt)^c[3] + 1) / c[3]^2)
      "Second-order partial derivative of uptake w.r.t. to second and third 
    coefficient of Toth isotherm";

    Real ddx_adsorpt_dc3_dc3 = (c[1] * c[2] * p_adsorpt * (-((c[2] * p_adsorpt)^c[3] *
      log(c[2] * p_adsorpt)^2) / (((c[2] * p_adsorpt)^c[3] + 1) * c[3]) +
      ((c[2] * p_adsorpt)^(2 * c[3]) * log(c[2] * p_adsorpt)^2) /
      (((c[2] * p_adsorpt)^c[3] + 1)^2 * c[3]) + (2 * (c[2] * p_adsorpt)^c[3] *
      log(c[2] * p_adsorpt)) / (((c[2] * p_adsorpt)^c[3] + 1) * c[3]^2) -
      (2 * log((c[2] * p_adsorpt)^c[3] + 1)) / c[3]^3)) /
      ((c[2] * p_adsorpt)^c[3] + 1)^(1 / c[3]) + (c[1] * c[2] * p_adsorpt *
      (log((c[2] * p_adsorpt)^c[3] + 1) / c[3]^2 - ((c[2] * p_adsorpt)^c[3] *
      log(c[2] * p_adsorpt)) / (((c[2] * p_adsorpt)^c[3] + 1) * c[3]))^2) /
      ((c[2] * p_adsorpt)^c[3] + 1)^(1 / c[3])
      "Second-order partial derivative of uptake w.r.t. to third coefficient of 
    Toth isotherm";

    Real ddx_adsorpt_dc1_dT_adsorpt = ddx_adsorpt_dc1_dc2*dc_dT_adsorpt[2] +
      ddx_adsorpt_dc1_dc3*dc_dT_adsorpt[3]
      "Second-order partial derivative of uptake w.r.t. to first coefficient of 
    Toth isotherm and temperature";
    Real ddx_adsorpt_dc2_dT_adsorpt = ddx_adsorpt_dc1_dc2*dc_dT_adsorpt[1] +
      ddx_adsorpt_dc2_dc2*dc_dT_adsorpt[2] +
      ddx_adsorpt_dc2_dc3*dc_dT_adsorpt[3]
      "Second-order partial derivative of uptake w.r.t. to second coefficient of 
    Toth isotherm and temperature";
    Real ddx_adsorpt_dc3_dT_adsorpt = ddx_adsorpt_dc1_dc3*dc_dT_adsorpt[1] +
      ddx_adsorpt_dc2_dc3*dc_dT_adsorpt[2] +
      ddx_adsorpt_dc3_dc3*dc_dT_adsorpt[3]
      "Second-order partial derivative of uptake w.r.t. to third coefficient of 
    Toth isotherm and temperature";

  algorithm
    ddx_adsorpt_dT_adsorpt_dT_adsorpt :=
      (ddx_adsorpt_dc1_dT_adsorpt*dc_dT_adsorpt[1] +
       dx_adsorpt_dc1*ddc_dT_adsorpt_dT_adsorpt[1]) +
      (ddx_adsorpt_dc2_dT_adsorpt*dc_dT_adsorpt[2] +
       dx_adsorpt_dc2*ddc_dT_adsorpt_dT_adsorpt[2]) +
      (ddx_adsorpt_dc3_dT_adsorpt*dc_dT_adsorpt[3] +
       dx_adsorpt_dc3*ddc_dT_adsorpt_dT_adsorpt[3])
      "Calculation of the second-order partial derivative of the equilibrium uptake 
    w.r.t. the equilibrium temperature at constant pressure";
  end ddx_dT_dT;

  redeclare final function extends ddx_dp_dT
    "Toth isotherm model: Second-order partial derivative of uptake w.r.t. pressure and temperature"

    //
    // Definition of variables
    //
protected
    Real ddx_adsorpt_dp_adsorpt_dc1 = c[2] * ((c[2] * p_adsorpt)^c[3] + 1) ^
      (-1 / c[3] - 1)
      "Second-order partial derivative of uptake w.r.t. to equilibrium pressure and
    first coefficient of Toth isotherm";
    Real ddx_adsorpt_dp_adsorpt_dc2 = -c[1] * ((c[2] * p_adsorpt)^c[3] + 1) ^
      (-1 / c[3] - 2) * (c[3] * (c[2] * p_adsorpt)^c[3] - 1)
      "Second-order partial derivative of uptake w.r.t. to equilibrium pressure and
    second coefficient of Toth isotherm";
    Real ddx_adsorpt_dp_adsorpt_dc3 = (c[1] * c[2] * ((c[2] * p_adsorpt)^c[3] + 1) ^
      (-1 / c[3] - 2) * (((c[2] * p_adsorpt)^c[3] + 1) *
      log((c[2] * p_adsorpt)^c[3] + 1) + (-c[3]^2 - c[3]) * (c[2] * p_adsorpt)^c[3] *
      log(c[2] * p_adsorpt))) / c[3]^2
      "Second-order partial derivative of uptake w.r.t. to equilibrium pressure and
    third coefficient of Toth isotherm";

  algorithm
    ddx_adsorpt_dp_adsorpt_dT_adsorpt :=
      ddx_adsorpt_dp_adsorpt_dc1*dc_dT_adsorpt[1] +
      ddx_adsorpt_dp_adsorpt_dc2*dc_dT_adsorpt[2] +
      ddx_adsorpt_dp_adsorpt_dc3*dc_dT_adsorpt[3]
      "Calculation of the second-oder partial derivative of the equilibrium uptake 
     w.r.t. the equilibrium pressure and temperature";
  end ddx_dp_dT;

  redeclare function pi_pT
    "Toth isotherm model: Reduced spreading pressure as function of pressure and temperature (numerical solution)"
    extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_pi_pT_num(
        redeclare final function func_x_pT =
        SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Toth.x_pT);
  end pi_pT;

  redeclare function p_piT
    "Toth isotherm model: Pressure as function of reduced spreading pressure and temperature (numerical solution)"
    extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_p_piT_num(
        redeclare final function func_pi_pT =
        SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Toth.pi_pT);
  end p_piT;
    //
    // Annotations
    //
annotation (Documentation(revisions="<html>
<ul>
  <li>
  November 1, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>", info="<html>
<p>
The Toth isotherm model is a three-parameter model for calculating the  equilibrium
uptake <i>x_adsorpt</i> as a function of the equilibrium pressure <i>p_adsorpt</i>.
The Toth isotherm model is suitable for type I-III isotherms according to the 
IUPAC definition.
</p>

<h4>Main equations</h4>
<p>
The Toth isotherm model has the following form:
</p>
<pre>
    x<sub>adsorpt</sub> = x<sub>sat</sub>(T<sub>adsorpt</sub>) * b(T<sub>adsorpt</sub>) * p<sub>adsorpt</sub> / ((1 + (b(T<sub>adsorpt</sub>) * p<sub>adsorpt</sub>) ^ t(T<sub>adsorpt</sub>)) ^ (1/t(T<sub>adsorpt</sub>)));
</pre>
<p>
where <i>x<sub>sat</sub>(T<sub>adsorpt</sub>)</i> is the saturation uptake, 
<i>b(T<sub>adsorpt</sub>)</i> is the Toth coefficient, and <i>t(T<sub>adsorpt</sub>)</i>
is the Toth exponent. Typical temperature dependencies may have the following forms:
</p>
<pre>
    x<sub>sat</sub>(T<sub>adsorpt</sub>) =  x<sub>ref</sub> * <strong>exp</strong>(&Chi; * (1 - T<sub>adsorpt</sub>/T<sub>ref</sub>));
</pre>
<pre>
    b(T<sub>adsorpt</sub>) = b<sub>ref</sub> * <strong>exp</strong>(Q/(R * T<sub>ref</sub>) * (T<sub>ref</sub>/T<sub>adsorpt</sub> - 1));
</pre>
<pre>
    t(T<sub>adsorpt</sub>) = t<sub>ref</sub> + &alpha; * (1 - T<sub>ref</sub>/T<sub>adsorpt</sub>);
</pre>
<p>
where <i>x<sub>ref</sub></i> is the saturation uptake at reference temperature 
<i>T<sub>ref</sub></i>, <i>b<sub>ref</sub></i> is the Toth coefficient at reference 
temperature, and <i>t<sub>ref</sub></i> is the Toth exponent at reference temperature. 
The parameter <i>Q</i> is a measure for the isosteric adsorption enthalpy at a fractional
loading of <i>x<sub>adsorpt</sub>/x<sub>sat</sub>(T<sub>adsorpt</sub>) = 0.0</i>, the 
parameter <i>&Chi;</i> describes the change of the saturation uptake with temperature, 
and the parameter <i>&alpha;</i> describes the change of the Toth exponent with 
temperature. All seven parameters can be used as fitting parameters.
<br/><br/>
Note that the Toth exponent <i>t(T<sub>adsorpt</sub>)</i> is typically lower than
unity. For <i>t(T<sub>adsorpt</sub>) = 1</i>, the Toth isotherm becomes the
Langmuir isotherm. Hence, the Toth exponent <i>t(T<sub>adsorpt</sub>)</i> can be
interpreted as a parameter describing the heterogeneity of the adsorption system.
</p>

<h4>Required parameter order in function input c[:]:</h4>
<ul>
  <li>
  c[1] = x<sub>sat</sub>(T<sub>adsorpt</sub>) in kg/kg
  </li>
  <li>
  c[2] = b(T<sub>adsorpt</sub>) in 1/Pa
  </li>
  <li>
  c[3] = t(T<sub>adsorpt</sub>) in -
  </li>
</ul>

<h4>Typical use</h4>
<p>
The isotherm model is used for type I-III isotherms according to the IUPAC definition.
</p>

<h4>Example</h4>
<p>
The following figure shows the Toth isotherm model for different parameter sets.
In the upper sub-figure, the equilibrium pressure changes with time, while the 
equilibrium temperature remains constant; in the lower sub-figure, the equilibrium 
temperature changes with time, while the equilibrium pressure remains constant. 
<br/><br/>
</p>
<img src=\"Modelica://SorpLib/Resources/doc/media_functions_equilibria_pure_toth.png\" alt=\"media_functions_equilibria_pure_toth.png\">

<h4>References</h4>
<ul>
  <li>
  Bathen, D. and Breitbach, M. (2001). Adsorptionstechnik (in German), 1st Edition, ISBN 3-540-41908-X, Springer-Verlag Berlin Heidelberg New York.
  </li>
  <li>
  Do, D. D. (1998). Adsorption Analysis: Equilibria and Kinetics, 1st Edition, ISBN 978-1-86094-130-6, Imperial College Press.
  </li>
</ul>
</html>"));
end Toth;
