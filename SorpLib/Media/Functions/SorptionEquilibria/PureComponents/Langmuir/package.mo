within SorpLib.Media.Functions.SorptionEquilibria.PureComponents;
package Langmuir "Package containing all functions regarding the Langmuir isotherm"
extends
  SorpLib.Media.Functions.SorptionEquilibria.Interfaces.PartialPureComponents;

  redeclare final function extends x_pT
    "Langmuir isotherm model: Uptake as function of pressure and temperature"
  algorithm
    x_adsorpt := c[1]*c[2]*p_adsorpt / (1 + c[2]*p_adsorpt)
      "Calculation of the equilibrium uptake of the adsorpt phase";

    //
    // Annotations
    //
    annotation (Inline=false,
  InlineAfterIndexReduction=false,
  LateInline=true,
  inverse(p_adsorpt=SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Langmuir.p_xT(x_adsorpt, T_adsorpt, c, p_adsorpt_lb_start, p_adsorpt_ub_start, tolerance)));
  end x_pT;

  redeclare final function extends p_xT
    "Langmuir isotherm model: Pressure as function of uptake and temperature"
  algorithm
    p_adsorpt := x_adsorpt / (c[2] * (c[1]-x_adsorpt))
      "Calculation of the equilibrium pressure of the adsorpt phase";

    //
    // Annotations
    //
    annotation (Inline=false,
  InlineAfterIndexReduction=false,
  LateInline=true,
  inverse(x_adsorpt=SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Langmuir.x_pT(p_adsorpt, T_adsorpt, c, p_adsorpt_lb_start, p_adsorpt_ub_start, tolerance)));
  end p_xT;

  redeclare final function extends dx_dp
    "Langmuir isotherm model: Partial derivative of uptake w.r.t. pressure at constant temperature"
  algorithm
    dx_adsorpt_dp_adsorpt := c[1]*c[2] / (1 + c[2]*p_adsorpt) -
      c[1]*c[2]^2*p_adsorpt / (1 + c[2]*p_adsorpt)^2
      "Calculation of the partial derivative of the equilibrium uptake w.r.t. the
     equilibrium pressure at constant temperature";
  end dx_dp;

  redeclare final function extends dx_dT
    "Langmuir isotherm model: Partial derivative of uptake w.r.t. temperature at constant pressure"

    //
    // Definition of variables
    //
protected
    Real dx_adsorpt_dc1 = c[2]*p_adsorpt / (1 + c[2]*p_adsorpt)
      "Derivative of uptake w.r.t. to first coefficient of Langmuir isotherm";
    Real dx_adsorpt_dc2 = c[1]*p_adsorpt / (1 + c[2]*p_adsorpt)^2
      "Derivative of uptake w.r.t. to second coefficient of Langmuir isotherm";

  algorithm
    dx_adsorpt_dT_adsorpt :=
      dx_adsorpt_dc1*dc_dT_adsorpt[1] +
      dx_adsorpt_dc2*dc_dT_adsorpt[2]
      "Calculation of the partial derivative of the equilibrium uptake w.r.t. the
     equilibrium temperature at constant pressure";
  end dx_dT;

  redeclare final function extends ddx_dp_dp
    "Langmuir isotherm model: Second-order partial derivative of uptake w.r.t. pressure at constant temperature"
  algorithm
    ddx_adsorpt_dp_adsorpt_dp_adsorpt :=
      -(2 * c[1] * c[2]^2) / (c[2] * p_adsorpt + 1)^3
      "Calculation of the second-order partial derivative of the equilibrium uptake 
    w.r.t. the equilibrium pressure at constant temperature";
  end ddx_dp_dp;

  redeclare final function extends ddx_dT_dT
    "Langmuir isotherm model: Second-order partial derivative of uptake w.r.t. temperature at constant pressure"

    //
    // Definition of variables
    //
protected
    Real dx_adsorpt_dc1 = c[2]*p_adsorpt / (1 + c[2]*p_adsorpt)
      "Partial derivative of uptake w.r.t. to first coefficient of Langmuir isotherm";
    Real dx_adsorpt_dc2 = c[1]*p_adsorpt / (1 + c[2]*p_adsorpt)^2
      "Partial derivative of uptake w.r.t. to second coefficient of Langmuir isotherm";

    Real ddx_adsorpt_dc1_dc2 = p_adsorpt / (1 + c[2]*p_adsorpt)^2
      "Second-order partial derivative of uptake w.r.t. to first and second 
    coefficient of Langmuir isotherm";

    Real ddx_adsorpt_dc2_dc2 = -2 * c[1] * p_adsorpt^2 / (1 + c[2]*p_adsorpt)^3
      "Second-order partial derivative of uptake w.r.t. to second coefficient of 
    Langmuir isotherm";

    Real ddx_adsorpt_dc1_dT_adsorpt = ddx_adsorpt_dc1_dc2*dc_dT_adsorpt[2]
      "Second-order partial derivative of uptake w.r.t. to first coefficient of 
    Langmuir isotherm and temperature";
    Real ddx_adsorpt_dc2_dT_adsorpt = ddx_adsorpt_dc1_dc2*dc_dT_adsorpt[1] +
      ddx_adsorpt_dc2_dc2*dc_dT_adsorpt[2]
      "Second-order partial derivative of uptake w.r.t. to second coefficient of 
    Langmuir isotherm and temperature";

  algorithm
    ddx_adsorpt_dT_adsorpt_dT_adsorpt :=
      (ddx_adsorpt_dc1_dT_adsorpt*dc_dT_adsorpt[1] +
       dx_adsorpt_dc1*ddc_dT_adsorpt_dT_adsorpt[1]) +
      (ddx_adsorpt_dc2_dT_adsorpt*dc_dT_adsorpt[2] +
       dx_adsorpt_dc2*ddc_dT_adsorpt_dT_adsorpt[2])
      "Calculation of the second-order partial derivative of the equilibrium uptake 
    w.r.t. the equilibrium temperature at constant pressure";
  end ddx_dT_dT;

  redeclare final function extends ddx_dp_dT
    "Langmuir isotherm model: Second-order partial derivative of uptake w.r.t. pressure and temperature"

    //
    // Definition of variables
    //
protected
    Real ddx_adsorpt_dp_adsorpt_dc1 = c[2] / (1 + c[2]*p_adsorpt)^2
      "Second-order partial derivative of uptake w.r.t. to equilibrium pressure and
    first coefficient of Langmuir isotherm";
    Real ddx_adsorpt_dp_adsorpt_dc2 = -c[1] * (c[2]*p_adsorpt - 1) /
      (1 + c[2]*p_adsorpt)^3
      "Second-order partial derivative of uptake w.r.t. to equilibrium pressure and
    second coefficient of Langmuir isotherm";

  algorithm
    ddx_adsorpt_dp_adsorpt_dT_adsorpt :=
      ddx_adsorpt_dp_adsorpt_dc1*dc_dT_adsorpt[1] +
      ddx_adsorpt_dp_adsorpt_dc2*dc_dT_adsorpt[2]
      "Calculation of the second-oder partial derivative of the equilibrium uptake 
     w.r.t. the equilibrium pressure and temperature";
  end ddx_dp_dT;

  redeclare final function extends pi_pT
    "Langmuir isotherm model: Pressure as function of reduced spreading pressure and temperature"
  algorithm
    pi := 1/M_adsorptive * (c[1]*log(1 + c[2]*p_adsorpt))
      "Calculation of the reduced spreading pressure";
  end pi_pT;

  redeclare final function extends p_piT
    "Langmuir isotherm model: Pressure as function of reduced spreading pressure and temperature"
  algorithm
    p_adsorpt := (exp(M_adsorptive * pi / c[1]) - 1) / c[2]
      "Calculation of the equilibrium pressure";
  end p_piT;
  //
  // Annotations
  //
annotation (Documentation(revisions="<html>
<ul>
  <li>
  November 1, 2023, by Mirko Engelpracht:<br/>
  </li>
</ul>
</html>", info="<html>
<p>
The Langmuir isotherm model is a two-parameter model for calculating the  equilibrium
uptake <i>x_adsorpt</i> as a function of the equilibrium pressure <i>p_adsorpt</i>.
The Langmuir isotherm model is suitable for type I isotherms according to the 
IUPAC definition.
</p>

<h4>Main equations</h4>
<p>
The Langmuir isotherm model has the following form:
</p>
<pre>
    x<sub>adsorpt</sub> = x<sub>sat</sub>(T<sub>adsorpt</sub>) * (b(T<sub>adsorpt</sub>) * p<sub>adsorpt</sub>) / (1 + b(T<sub>adsorpt</sub>) * p<sub>adsorpt</sub>);
</pre>
<p>
where <i>x<sub>sat</sub>(T<sub>adsorpt</sub>)</i> is the saturation uptake and 
<i>b(T<sub>adsorpt</sub>)</i> is the Langmuir coefficient. Typical temperature
dependencies may have the following forms:
</p>
<pre>
    x<sub>sat</sub>(T<sub>adsorpt</sub>) = a<sub>0</sub> + a<sub>1</sub>/T<sub>adsorpt</sub>;
</pre>
<pre>
    b(T<sub>adsorpt</sub>) = b<sub>0</sub> * <strong>exp</strong>(-&Delta;H<sub>ads</sub>/(R * T<sub>adsorpt</sub>));
</pre>
<p>
where <i>a<sub>0</sub></i>, <i>a<sub>1</sub></i>, <i>b<sub>0</sub></i>, and 
<i>&Delta;H<sub>ads</sub></i> are fitting parameters. The parameter
<i>&Delta;H<sub>ads</sub></i> is the isosteric adsorption heat, which is
invariant with the surface uptake. Note that the Langmuir isotherm model only 
consists of thermodynamic consistency if the saturation uptake <i>x<sub>sat</sub></i> 
is constant.
</p>

<h4>Required parameter order in function input c[:]:</h4>
<ul>
  <li>
  c[1] = x<sub>sat</sub>(T<sub>adsorpt</sub>) in kg/kg
  </li>
  <li>
  c[2] = b(T<sub>adsorpt</sub>) in 1/Pa
  </li>
</ul>

<h4>Assumptions and limitations</h4>
<ul>
  <li>
  All adsorption sites are energetically equivalent.
  </li>
  <li>
  All adsorption sites can be occupied.
  </li>
  <li>
  No interactions occur between the adsorbent molecules.
  </li>
  <li>
  The adsorbent surface is covered monomolecularly.
  </li>
</ul>

<h4>Typical use</h4>
<p>
The isotherm model is used for type I isotherms according to the IUPAC definition.
</p>

<h4>Example</h4>
<p>
The following figure shows the Langmuir isotherm model for different parameter sets.
In the upper sub-figure, the equilibrium pressure changes with time, while the 
equilibrium temperature remains constant; in the lower sub-figure, the equilibrium 
temperature changes with time, while the equilibrium pressure remains constant. 
<br/><br/>
</p>
<img src=\"Modelica://SorpLib/Resources/doc/media_functions_equilibria_pure_langmuir.png\" alt=\"media_functions_equilibria_pure_langmuir.png\">

<h4>References</h4>
<ul>
  <li>
  Bathen, D. and Breitbach, M. (2001). Adsorptionstechnik (in German), 1st Edition, ISBN 3-540-41908-X, Springer-Verlag Berlin Heidelberg New York.
  </li>
  <li>
  Do, D. D. (1998). Adsorption Analysis: Equilibria and Kinetics, 1st Edition, ISBN 978-1-86094-130-6, Imperial College Press.
  </li>
</ul>
</html>"));
end Langmuir;
