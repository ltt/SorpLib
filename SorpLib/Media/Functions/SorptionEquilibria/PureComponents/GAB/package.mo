﻿within SorpLib.Media.Functions.SorptionEquilibria.PureComponents;
package GAB "Package containing all functions regarding the GAB isotherm"
extends
  SorpLib.Media.Functions.SorptionEquilibria.Interfaces.PartialPureComponents;

  redeclare final function extends x_pT
    "GAB isotherm model: Uptake as function of pressure and temperature"

    //
    // Definition of variables
    //
protected
    Real phi = p_adsorpt/c[1]
      "Relative pressure";

  algorithm
    x_adsorpt := c[2]*c[3]*c[4] * phi /
      ((1 - c[4] * phi) * (1 + (c[3] - 1) * c[4] * phi))
      "Calculation of the equilibrium uptake of the adsorpt phase";

    //
    // Annotations
    //
    annotation (Inline=false,
  InlineAfterIndexReduction=false,
  LateInline=true,
  inverse(p_adsorpt=SorpLib.Media.Functions.SorptionEquilibria.PureComponents.GAB.p_xT(x_adsorpt, T_adsorpt, c, p_adsorpt_lb_start, p_adsorpt_ub_start, tolerance)));
  end x_pT;

  redeclare final function extends p_xT
    "GAB isotherm model: Pressure as function of uptake and temperature"

    //
    // Definition of variables
    //
protected
    Real phi = p_adsorpt/c[1]
      "Relative pressure";

  algorithm
    p_adsorpt := (c[3]*x_adsorpt - c[3]*c[2] - 2*x_adsorpt + sqrt(c[3] *
      (c[3]*x_adsorpt^2 - 2*c[3]*c[2]*x_adsorpt + c[3]*c[2]^2 + 4*c[2]*x_adsorpt))) /
      (2*c[4]*x_adsorpt * (c[3] - 1)) * c[1]
      "Calculation of the equilibrium pressure of the adsorpt phase: Solve 
    quadratic equation and discard negative solution";

    //
    // Annotations
    //
    annotation (Inline=false,
  InlineAfterIndexReduction=false,
  LateInline=true,
  inverse(x_adsorpt=SorpLib.Media.Functions.SorptionEquilibria.PureComponents.GAB.x_pT(p_adsorpt, T_adsorpt, c, p_adsorpt_lb_start, p_adsorpt_ub_start, tolerance)));
  end p_xT;

  redeclare final function extends dx_dp
    "GAB isotherm model: Partial derivative of uptake w.r.t. pressure at constant temperature"

    //
    // Definition of variables
    //
protected
    Real phi = p_adsorpt/c[1]
      "Relative pressure";
    Real dphi_dp_adsorpt =  1/c[1]
      "Partial derivative of relative pressure w.r.t. to pressure";

  algorithm
    dx_adsorpt_dp_adsorpt := c[2]*c[3]*c[4] * ((c[3] - 1) * c[4]^2 * phi^2 + 1) /
      ((c[4] * phi - 1)^2 * (1 + (c[3] - 1) * c[4] * phi)^2) * dphi_dp_adsorpt
      "Calculation of the partial derivative of the equilibrium uptake w.r.t. the
     equilibrium pressure at constant temperature";
  end dx_dp;

  redeclare final function extends dx_dT
    "GAB isotherm model: Partial derivative of uptake w.r.t. temperature at constant pressure"

    //
    // Definition of variables
    //
protected
    Real phi = p_adsorpt/c[1]
      "Relative pressure";
    Real dphi_dc1 = -p_adsorpt/c[1]^2
      "Partial derivative of relative pressure w.r.t. to saturation pressure";

    Real dx_adsorpt_dphi =  c[2]*c[3]*c[4] * ((c[3] - 1) * c[4]^2 * phi^2 + 1) /
      ((c[4] * phi - 1)^2 * (1 + (c[3] - 1) * c[4] * phi)^2)
      "Partial derivative of uptake w.r.t. to relative pressure";

    Real dx_adsorpt_dc1 = dx_adsorpt_dphi*dphi_dc1
      "Derivative of uptake w.r.t. to first coefficient of GAB isotherm";
    Real dx_adsorpt_dc2 = c[3]*c[4] * phi /
      ((1 - c[4] * phi) * (1 + (c[3] - 1) * c[4] * phi))
      "Derivative of uptake w.r.t. to second coefficient of GAB isotherm";
    Real dx_adsorpt_dc3 = c[2]*c[4] * phi / (c[3]*c[4] * phi - c[4] * phi + 1)^2
      "Derivative of uptake w.r.t. to third coefficient of GAB isotherm";
    Real dx_adsorpt_dc4 = c[2]*c[3] * phi * ((c[3] - 1) * c[4]^2 * phi^2 + 1) /
      ((c[4] * phi - 1)^2 * (1 + (c[3] - 1) * c[4] * phi)^2)
      "Derivative of uptake w.r.t. to fourtg coefficient of GAB isotherm";

  algorithm
    dx_adsorpt_dT_adsorpt :=
      dx_adsorpt_dc1*dc_dT_adsorpt[1] +
      dx_adsorpt_dc2*dc_dT_adsorpt[2] +
      dx_adsorpt_dc3*dc_dT_adsorpt[3] +
      dx_adsorpt_dc4*dc_dT_adsorpt[4]
      "Calculation of the partial derivative of the equilibrium uptake w.r.t. the
     equilibrium temperature at constant pressure";
  end dx_dT;

  redeclare final function extends ddx_dp_dp
    "GAB isotherm model: Second-order partial derivative of uptake w.r.t. pressure at constant temperature"

    //
    // Definition of variables
    //
protected
    Real phi = p_adsorpt/c[1]
      "Relative pressure";
    Real dphi_dp_adsorpt =  1/c[1]
      "Partial derivative of relative pressure w.r.t. to pressure";

  algorithm
    ddx_adsorpt_dp_adsorpt_dp_adsorpt :=
      (-(2 * c[2] * c[3] * c[4]^2 * ((c[3] - 1) * c[4] * phi *
      ((c[3] - 1) * c[4]^2 * phi^2 + 3) - c[3] + 2)) /
      ((c[4] * phi - 1)^3 * ((c[3] - 1) * c[4] * phi + 1)^3)) * dphi_dp_adsorpt^2
      "Calculation of the second-order partial derivative of the equilibrium uptake 
    w.r.t. the equilibrium pressure at constant temperature";
  end ddx_dp_dp;

  redeclare final function extends ddx_dT_dT
    "GAB isotherm model: Second-order partial derivative of uptake w.r.t. temperature at constant pressure"

    //
    // Definition of variables
    //
protected
    Real dx_adsorpt_dc1 = -(c[2] * c[3] * c[4] * p_adsorpt * (c[1]^2 + (c[3] - 1) *
      c[4]^2 * p_adsorpt^2)) / ((c[1] - c[4] * p_adsorpt)^2 * (c[1] + (c[3] - 1) *
      c[4] * p_adsorpt)^2)
      "Derivative of uptake w.r.t. to first coefficient of GAB isotherm";
    Real dx_adsorpt_dc2 = -(c[1] * c[3] * c[4] * p_adsorpt) / ((c[4] * p_adsorpt -
      c[1]) * ((c[3] - 1) * c[4] * p_adsorpt + c[1]))
      "Derivative of uptake w.r.t. to second coefficient of GAB isotherm";
    Real dx_adsorpt_dc3 = (c[1] * c[2] * c[4] * p_adsorpt) / (c[4] * p_adsorpt *
      c[3] - c[4] * p_adsorpt + c[1])^2
      "Derivative of uptake w.r.t. to third coefficient of GAB isotherm";
    Real dx_adsorpt_dc4 = (c[1] * c[2] * c[3] * p_adsorpt * ((c[3] - 1) * p_adsorpt^2 *
      c[4]^2 + c[1]^2)) / ((p_adsorpt * c[4] - c[1])^2 * ((c[3] - 1) * p_adsorpt *
      c[4] + c[1])^2)
      "Derivative of uptake w.r.t. to fourtg coefficient of GAB isotherm";

    Real ddx_adsorpt_dc1_dc1 = (2 * c[2] * c[3] * c[4] * p_adsorpt * (c[1]^3 + (3 *
      c[3] - 3) * c[4]^2 * p_adsorpt^2 * c[1] + (c[3]^2 - 3 * c[3] + 2) * c[4]^3 *
      p_adsorpt^3)) / ((c[1] - c[4] * p_adsorpt)^3 * (c[1] + (c[3] - 1) * c[4] *
      p_adsorpt)^3)
      "Second-order partial derivative of uptake w.r.t. to first coefficient of 
    GAB isotherm";
    Real ddx_adsorpt_dc3_dc3 = -(2 * c[1] * c[2] * c[4]^2 * p_adsorpt^2) / (c[4] *
      p_adsorpt * c[3] - c[4] * p_adsorpt + c[1])^3
      "Second-order partial derivative of uptake w.r.t. to third coefficient of 
    GAB isotherm";
    Real ddx_adsorpt_dc4_dc4 = -(2 * c[1] * c[2] * c[3] * p_adsorpt^2 * ((c[3]^2 -
      2 * c[3] + 1) * p_adsorpt^3 * c[4]^3 + (3 * c[1]^2 * c[3] - 3 * c[1]^2) *
      p_adsorpt * c[4] - c[1]^3 * c[3] + 2 * c[1]^3)) / ((p_adsorpt * c[4] - c[1])^3 *
      ((c[3] - 1) * p_adsorpt * c[4] + c[1])^3)
      "Second-order partial derivative of uptake w.r.t. to fourth coefficient of 
    GAB isotherm";

    Real ddx_adsorpt_dc1_dc2= -(c[3] * c[4] * p_adsorpt * ((c[3] - 1) * c[4]^2 *
      p_adsorpt^2 + c[1]^2)) / ((c[1] - c[4] * p_adsorpt)^2 * ((c[3] - 1) * c[4] *
      p_adsorpt + c[1])^2)
      "Second-order partial derivative of uptake w.r.t. to first and second 
    coefficient of GAB isotherm";
    Real ddx_adsorpt_dc1_dc3 = (c[2] * c[4] * p_adsorpt * (c[4] * p_adsorpt * c[3] -
      c[4] * p_adsorpt - c[1])) / (c[4] * p_adsorpt * c[3] - c[4] * p_adsorpt +
      c[1])^3
      "Second-order partial derivative of uptake w.r.t. to first and third 
    coefficient of GAB isotherm";
    Real ddx_adsorpt_dc1_dc4 = (c[2] * c[3] * p_adsorpt * ((c[3]^2 - 2 * c[3] + 1) *
      p_adsorpt^4 * c[4]^4 + (c[1] * c[3]^2 - 3 * c[1] * c[3] + 2 * c[1]) * p_adsorpt^3 *
      c[4]^3 + (6 * c[1]^2 * c[3] - 6 * c[1]^2) * p_adsorpt^2 * c[4]^2 + (2 * c[1]^3 -
      c[1]^3 * c[3]) * p_adsorpt * c[4] + c[1]^4)) / ((p_adsorpt * c[4] - c[1])^3 *
      ((c[3] - 1) * p_adsorpt * c[4] + c[1])^3)
      "Second-order partial derivative of uptake w.r.t. to first and fourth 
    coefficient of GAB isotherm";

    Real ddx_adsorpt_dc2_dc3 = (c[1] * c[4] * p_adsorpt) / (c[4] * p_adsorpt * c[3] -
      c[4] * p_adsorpt + c[1])^2
      "Second-order partial derivative of uptake w.r.t. to second and third 
    coefficient of GAB isotherm";
    Real ddx_adsorpt_dc2_dc4 = (c[1] * c[3] * p_adsorpt * ((c[3] - 1) * p_adsorpt^2 *
      c[4]^2 + c[1]^2)) / ((p_adsorpt * c[4] - c[1])^2 * ((c[3] - 1) * p_adsorpt *
      c[4] + c[1])^2)
      "Second-order partial derivative of uptake w.r.t. to second and fourht 
    coefficient of GAB isotherm";

    Real ddx_adsorpt_dc3_dc4 = -(c[1] * c[2] * p_adsorpt * ((c[3] - 1) * p_adsorpt *
      c[4] - c[1])) / ((c[3] - 1) * p_adsorpt * c[4] + c[1])^3
      "Second-order partial derivative of uptake w.r.t. to third and fourht 
    coefficient of GAB isotherm";

    Real ddx_adsorpt_dc1_dT_dT_adsorpt = ddx_adsorpt_dc1_dc1*dc_dT_adsorpt[1] +
      ddx_adsorpt_dc1_dc2*dc_dT_adsorpt[2] +
      ddx_adsorpt_dc1_dc3*dc_dT_adsorpt[3] +
      ddx_adsorpt_dc1_dc4*dc_dT_adsorpt[4]
      "Second-order partial derivative of uptake w.r.t. to first coefficient of 
    GAB isotherm and temperature";
    Real ddx_adsorpt_dc2_dT_dT_adsorpt = ddx_adsorpt_dc1_dc2*dc_dT_adsorpt[1] +
      ddx_adsorpt_dc2_dc3*dc_dT_adsorpt[3] +
      ddx_adsorpt_dc2_dc4*dc_dT_adsorpt[4]
      "Second-order partial derivative of uptake w.r.t. to second coefficient of 
    GAB isotherm and temperature";
    Real ddx_adsorpt_dc3_dT_dT_adsorpt = ddx_adsorpt_dc1_dc3*dc_dT_adsorpt[1] +
      ddx_adsorpt_dc2_dc3*dc_dT_adsorpt[2] +
      ddx_adsorpt_dc3_dc3*dc_dT_adsorpt[3] +
      ddx_adsorpt_dc3_dc4*dc_dT_adsorpt[4]
      "Second-order partial derivative of uptake w.r.t. to third coefficient of 
    GAB isotherm and temperature";
    Real ddx_adsorpt_dc4_dT_dT_adsorpt = ddx_adsorpt_dc1_dc4*dc_dT_adsorpt[1] +
      ddx_adsorpt_dc2_dc4*dc_dT_adsorpt[2] +
      ddx_adsorpt_dc3_dc4*dc_dT_adsorpt[3] +
      ddx_adsorpt_dc4_dc4*dc_dT_adsorpt[4]
      "Second-order partial derivative of uptake w.r.t. to fourth coefficient of 
    GAB isotherm and temperature";

  algorithm
    ddx_adsorpt_dT_adsorpt_dT_adsorpt :=
      (ddx_adsorpt_dc1_dT_dT_adsorpt*dc_dT_adsorpt[1] +
       dx_adsorpt_dc1*ddc_dT_adsorpt_dT_adsorpt[1]) +
      (ddx_adsorpt_dc2_dT_dT_adsorpt*dc_dT_adsorpt[2] +
       dx_adsorpt_dc2*ddc_dT_adsorpt_dT_adsorpt[2]) +
      (ddx_adsorpt_dc3_dT_dT_adsorpt*dc_dT_adsorpt[3] +
       dx_adsorpt_dc3*ddc_dT_adsorpt_dT_adsorpt[3]) +
      (ddx_adsorpt_dc4_dT_dT_adsorpt*dc_dT_adsorpt[4] +
       dx_adsorpt_dc4*ddc_dT_adsorpt_dT_adsorpt[4])
      "Calculation of the second-order partial derivative of the equilibrium uptake 
    w.r.t. the equilibrium temperature at constant pressure";
  end ddx_dT_dT;

  redeclare final function extends ddx_dp_dT
    "GAB isotherm model: Second-order partial derivative of uptake w.r.t. pressure and temperature"

    //
    // Definition of variables
    //
protected
    Real ddx_adsorpt_dp_adsorpt_dc1 = (c[2] * c[3] * c[4] * ((c[3]^2 - 2 * c[3] + 1) *
      c[4]^4 * p_adsorpt^4 + (c[1] * c[3]^2 - 3 * c[1] * c[3] + 2 * c[1]) * c[4]^3 *
      p_adsorpt^3 + (6 * c[1]^2 * c[3] - 6 * c[1]^2) * c[4]^2 * p_adsorpt^2 + (2 *
      c[1]^3 - c[1]^3 * c[3]) * c[4] * p_adsorpt + c[1]^4)) / ((c[4] * p_adsorpt -
      c[1])^3 * ((c[3] - 1) * c[4] * p_adsorpt + c[1])^3)
      "Second-order partial derivative of uptake w.r.t. to equilibrium pressure and
    first coefficient of GAB isotherm";
    Real ddx_adsorpt_dp_adsorpt_dc2 = (c[1] * c[3] * c[4] * ((c[3] - 1) * c[4]^2 *
      p_adsorpt^2 + c[1]^2)) / ((c[4] * p_adsorpt - c[1])^2 * ((c[3] - 1) * c[4] *
      p_adsorpt + c[1])^2)
      "Second-order partial derivative of uptake w.r.t. to equilibrium pressure and
    second coefficient of GAB isotherm";
    Real ddx_adsorpt_dp_adsorpt_dc3 = -(c[1] * c[2] * c[4] * ((c[3] - 1) * c[4] *
      p_adsorpt - c[1])) / ((c[3] - 1) * c[4] * p_adsorpt + c[1])^3
      "Second-order partial derivative of uptake w.r.t. to equilibrium pressure and
    third coefficient of GAB isotherm";
    Real ddx_adsorpt_dp_adsorpt_dc4 = -(c[1] * c[2] * c[3] * ((c[3]^2 - 2 * c[3] + 1) *
      c[4]^4 * p_adsorpt^4 + (c[1] * c[3]^2 - 3 * c[1] * c[3] + 2 * c[1]) * c[4]^3 *
      p_adsorpt^3 + (6 * c[1]^2 * c[3] - 6 * c[1]^2) * c[4]^2 * p_adsorpt^2 + (2 *
      c[1]^3 - c[1]^3 * c[3]) * c[4] * p_adsorpt + c[1]^4)) / ((c[4] * p_adsorpt -
      c[1])^3 * ((c[3] - 1) * c[4] * p_adsorpt + c[1])^3)
      "Second-order partial derivative of uptake w.r.t. to equilibrium pressure and
    fourth coefficient of GAB isotherm";

  algorithm
    ddx_adsorpt_dp_adsorpt_dT_adsorpt :=
      ddx_adsorpt_dp_adsorpt_dc1*dc_dT_adsorpt[1] +
      ddx_adsorpt_dp_adsorpt_dc2*dc_dT_adsorpt[2] +
      ddx_adsorpt_dp_adsorpt_dc3*dc_dT_adsorpt[3] +
      ddx_adsorpt_dp_adsorpt_dc4*dc_dT_adsorpt[4]
      "Calculation of the second-oder partial derivative of the equilibrium uptake 
     w.r.t. the equilibrium pressure and temperature";
  end ddx_dp_dT;

  redeclare final function extends pi_pT
    "GAB isotherm model: Reduced spreading pressure as function of pressure and temperature"

    //
    // Definition of variables
    //
protected
    Real phi = p_adsorpt/c[1]
      "Relative pressure";

  algorithm
    pi := 1/M_adsorptive *
      (c[2] * log((1 - c[4] * phi + c[3]*c[4] * phi) / (1 - c[4] * phi)))
      "Calculation of the reduced spreading pressure";
  end pi_pT;

  redeclare final function extends p_piT
    "GAB isotherm model: Pressure as function of reduced spreading pressure and temperature"
  algorithm
    p_adsorpt := c[1] * (1 - exp(pi * M_adsorptive / c[2])) /
      (-exp(pi * M_adsorptive / c[2]) * c[4] + c[4] - c[3] * c[4])
      "Calculation of the equilibrium pressure";
  end p_piT;
  //
  // Annotations
  //
  annotation (Documentation(revisions="<html>
<ul>
  <li>
  November 1, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>", info="<html>
<p>
The GAB isotherm model is a four-parameter model for calculating the  equilibrium
uptake <i>x_adsorpt</i> as a function of the equilibrium pressure <i>p_adsorpt</i>.
The GAB isotherm model is suitable for type III isotherms according to the IUPAC 
definition.
</p>

<h4>Main equations</h4>
<p>
The GAB isotherm model has the following form:
</p>
<pre>
    x<sub>adsorpt</sub> = x<sub>mon</sub>(T<sub>adsorpt</sub>) * c(T<sub>adsorpt</sub>) * k(T<sub>adsorpt</sub>) * &phi;(T<sub>adsorpt</sub>) / ((1 - k(T<sub>adsorpt</sub>) * &phi;(T<sub>adsorpt</sub>)) * (1 + (c(T<sub>adsorpt</sub>) - 1) * k(T<sub>adsorpt</sub>) * &phi;(T<sub>adsorpt</sub>)));
</pre>
<p>
with
</p>
<pre>
    &phi;(T<sub>adsorpt</sub>) = p<sub>adsorpt</sub>/p<sub>sat</sub>(T<sub>adsorpt</sub>);
</pre>
<p>
Herein, <i>x<sub>mon</sub>(T<sub>adsorpt</sub>)</i> is the monolayer uptake and 
<i>c(T<sub>adsorpt</sub>)</i> and <i>k(T<sub>adsorpt</sub>)</i> are affinity
coefficients of the GAB isotherm model. These three parameters can be modeled
independent of temperature. When assuming these three parameters to be dependent
on temperature, typical temperature dependencies may have the following forms:
</p>
<pre>
    x<sub>mon</sub>(T<sub>adsorpt</sub>) =  x<sub>mon,ref</sub> * <strong>exp</strong>(&Chi; * (1 - T<sub>adsorpt</sub>/T<sub>ref</sub>));
</pre>
<pre>
    c(T<sub>adsorpt</sub>) = <strong>exp</strong>((E<sub>1</sub> - E<sub>10+</sub>) / (R * T<sub>adsorpt</sub>));
</pre>
<pre>
    k(T<sub>adsorpt</sub>) = <strong>exp</strong>((E<sub>2-9</sub> - E<sub>10+</sub>) / (R * T<sub>adsorpt</sub>));
</pre>
<p>
with
</p>
<pre>
    E<sub>1</sub> = C - <strong>exp</strong>(D * T<sub>adsorpt</sub>);
</pre>
<pre>
    E<sub>2-9</sub> = F + G * T<sub>adsorpt</sub>;
</pre>
<pre>
    E<sub>10+</sub> = &Delta;h<sub>vap</sub>(T<sub>adsorpt</sub>);
</pre>
<p>
where <i>x<sub>mon,ref</sub></i> is the monolayer uptake at reference temperature 
<i>T<sub>ref</sub></i> and  <i>&Chi;</i> describes the change of the monolayer uptake 
with temperature. The coefficient <i>E<sub>1</sub></i> is the enthalpy of adsorption 
of the first layer and <i>E<sub>2-9</sub></i> is the enthalpy of adsorption of layers 
2-9: These enthalpies of adsorption can be modeled temperature-dependent as shown 
in the example above, with the four fitting parameters <i>C</i>, <i>D</i>, <i>E</i>, 
and <i>F</i>. The coefficient <i>E<sub>10+</sub></i> is the enthalpy of adsorptions 
for layer 10 or higher layers and is assumed to correspond to the temperature-dependent 
enthalpy of vaporization <i>&Delta;h<sub>vap</sub>(T<sub>adsorpt</sub>)</i>.
</p>

<h4>Required parameter order in function input c[:]:</h4>
<ul>
  <li>
  c[1] = p<sub>sat</sub>(T<sub>adsorpt</sub>) in Pa
  </li>
  <li>
  c[2] = x<sub>mon</sub>(T<sub>adsorpt</sub>) in kg/kg
  </li>
  <li>
  c[3] = c(T<sub>adsorpt</sub>) in -
  </li>
  <li>
  c[4] = k(T<sub>adsorpt</sub>) in -
  </li>
</ul>

<h4>Typical use</h4>
<p>
The isotherm model is used for type III isotherms according to the IUPAC definition.
</p>

<h4>Example</h4>
<p>
The following figure shows the GAB isotherm model for different parameter sets.
In the upper sub-figure, the equilibrium pressure changes with time, while the 
equilibrium temperature remains constant; in the lower sub-figure, the equilibrium 
temperature changes with time, while the equilibrium pressure remains constant. 
<br/><br/>
</p>
<img src=\"Modelica://SorpLib/Resources/doc/media_functions_equilibria_pure_gab.png\" alt=\"media_functions_equilibria_pure_gab.png\">

<h4>References</h4>
<ul>
  <li>
  Young et al. (2021). The impact of binary water – CO<sub>2</sub> isotherm models on the optimal performance of sorbent-based direct air capture processes, Energy Environ. Sci. 14: 5377. DOI: 10.1039/d1ee01272j.
  </li>
</ul>
</html>"));
end GAB;
