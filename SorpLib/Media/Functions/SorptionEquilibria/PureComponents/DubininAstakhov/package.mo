within SorpLib.Media.Functions.SorptionEquilibria.PureComponents;
package DubininAstakhov "Package containing all functions regarding the Dubinin-Astakhov isotherm"
extends
  SorpLib.Media.Functions.SorptionEquilibria.Interfaces.PartialPureComponentsDubinin;

  redeclare final function extends x_pT
    "Dubinin-Astakhov isotherm model: Uptake as function of pressure and temperature"
    //
    // Annotations
    //
    annotation (Inline=false,
  InlineAfterIndexReduction=false,
  LateInline=true,
  inverse(p_adsorpt=SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininAstakhov.p_xT(x_adsorpt, T_adsorpt, c, p_adsorpt_lb_start, p_adsorpt_ub_start, tolerance)));
  end x_pT;

  redeclare final function extends p_xT
    "Dubinin-Astakhov isotherm model: Pressure as function of uptake and temperature"
    //
    // Annotations
    //
    annotation (Inline=false,
  InlineAfterIndexReduction=false,
  LateInline=true,
  inverse(x_adsorpt=SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininAstakhov.x_pT(p_adsorpt, T_adsorpt, c, p_adsorpt_lb_start, p_adsorpt_ub_start, tolerance)));
  end p_xT;

  redeclare final function extends dx_dT
    "Dubinin-Astakhov isotherm model: Partial derivative of uptake w.r.t. temperature at constant pressure"

    //
    // Definition of variables
    //
protected
    SorpLib.Units.MolarAdsorptionPotential A=
      SorpLib.Media.Functions.SorptionEquilibria.Utilities.A_ppsT(
      p_adsorpt=p_adsorpt,
      p_sat=c[1],
      T_adsorpt=T_adsorpt)
      "Adsorption potential";
    SorpLib.Units.DerMolarAdsorptionPotentialByTemperature dA_dT_adsorpt=
      SorpLib.Media.Functions.SorptionEquilibria.Utilities.dA_dT(
      p_adsorpt=p_adsorpt,
      p_sat=c[1],
      T_adsorpt=T_adsorpt,
      dp_sat_dT_adsorpt=dc_dT_adsorpt[1])
      "Partial derivative of adsorption potential w.r.t. equilibrium temperature
      at constant pressure";

    SorpLib.Units.FilledPoreVolume W=
      SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininAstakhov.W_A(
      A=A,
      c=c)
      "Filled pore volume";
    SorpLib.Units.DerFilledPoreVolumeByAdsorptionPotential dW_dA=
      SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininAstakhov.dW_dA(
      A=A, c=c)
      "Partial derivative of filled pore volume w.r.t. adsorption potential at
      constant pressure and temperature";

    Real dx_adsorpt_dA = c[2] * dW_dA
      "Derivative of uptake w.r.t. to adsorption potential";
    Real dx_adsorpt_dc2 = W
      "Derivative of uptake w.r.t. to second coefficient of Dubinin-Astakhov isotherm";
    Real dx_adsorpt_dc3 = c[2] * exp(-(A / c[4]) ^ c[5])
      "Derivative of uptake w.r.t. to third coefficient of Dubinin-Astakhov isotherm";
    Real dx_adsorpt_dc4 = c[2] * c[5] / c[4] * (A / c[4]) ^ c[5] * W
      "Derivative of uptake w.r.t. to fourth coefficient of Dubinin-Astakhov isotherm";
    Real dx_adsorpt_dc5 = -c[2] * log(A / c[4]) * (A / c[4]) ^ c[5] *  W
      "Derivative of uptake w.r.t. to fivth coefficient of Dubinin-Astakhov isotherm";

  algorithm
    dx_adsorpt_dT_adsorpt :=
      dx_adsorpt_dA*dA_dT_adsorpt +
      dx_adsorpt_dc2*dc_dT_adsorpt[2] +
      dx_adsorpt_dc3*dc_dT_adsorpt[3] +
      dx_adsorpt_dc4*dc_dT_adsorpt[4] +
      dx_adsorpt_dc5*dc_dT_adsorpt[5]
      "Calculation of the partial derivative of the equilibrium uptake w.r.t. the
     equilibrium temperature at constant pressure";
  end dx_dT;

  redeclare final function extends ddx_dT_dT
    "Dubinin-Astakhov isotherm model: Second-order partial derivative of uptake w.r.t. temperature at constant pressure"

    //
    // Definition of variables
    //
protected
    SorpLib.Units.MolarAdsorptionPotential A=
      SorpLib.Media.Functions.SorptionEquilibria.Utilities.A_ppsT(
      p_adsorpt=p_adsorpt,
      p_sat=c[1],
      T_adsorpt=T_adsorpt)
      "Adsorption potential";
    SorpLib.Units.DerMolarAdsorptionPotentialByTemperature dA_dT_adsorpt=
      SorpLib.Media.Functions.SorptionEquilibria.Utilities.dA_dT(
      p_adsorpt=p_adsorpt,
      p_sat=c[1],
      T_adsorpt=T_adsorpt,
      dp_sat_dT_adsorpt=dc_dT_adsorpt[1])
      "Partial derivative of adsorption potential w.r.t. equilibrium temperature
      at constant pressure";
    SorpLib.Units.DerMolarAdsorptionPotentialByTemperatureTemperature ddA_dT_adsorpt_dT_adsorpt=
      SorpLib.Media.Functions.SorptionEquilibria.Utilities.ddA_dT_dT(
      p_adsorpt=p_adsorpt,
      p_sat=c[1],
      T_adsorpt=T_adsorpt,
      dp_sat_dT_adsorpt=dc_dT_adsorpt[1],
      ddp_sat_dT_adsorpt_dT_adsorpt=ddc_dT_adsorpt_dT_adsorpt[1])
      "Second-order partial derivative of adsorption potential w.r.t. equilibrium 
      temperature at constant pressure";

    SorpLib.Units.DerFilledPoreVolumeByAdsorptionPotential dW_dA=
      SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininAstakhov.dW_dA(
      A=A,
      c=c)
      "Partial derivative of filled pore volume w.r.t. adsorption potential at
      constant pressure and temperature";
    SorpLib.Units.DerFilledPoreVolumeByAdsorptionPotentialTemperature ddW_dA_dT_adsorpt=
      SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininAstakhov.ddW_dA_dT(
      p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt,
      A=A,
      c=c,
      dc_dT_adsorpt=dc_dT_adsorpt)
      "Second-order partial derivative of characteristic curve w.r.t. adsorption 
      potential and temperature at constant pressure";

    Real dx_adsorpt_dA = c[2] * dW_dA
      "Derivative of uptake w.r.t. to adsorption potential";
    Real dx_adsorpt_dc2 = c[3] * exp(-(A / c[4]) ^ c[5])
      "Derivative of uptake w.r.t. to second coefficient of Dubinin-Astakhov 
    isotherm";
    Real dx_adsorpt_dc3 = c[2] * exp(-(A / c[4]) ^ c[5])
      "Derivative of uptake w.r.t. to third coefficient of Dubinin-Astakhov 
    isotherm";
    Real dx_adsorpt_dc4 = c[2] * c[5] / c[4] * (A / c[4]) ^ c[5] *
      c[3] * exp(-(A / c[4]) ^ c[5])
      "Derivative of uptake w.r.t. to fourth coefficient of Dubinin-Astakhov 
    isotherm";
    Real dx_adsorpt_dc5 = -c[2] * log(A / c[4]) * (A / c[4]) ^ c[5] *
      c[3] * exp(-(A / c[4]) ^ c[5])
      "Derivative of uptake w.r.t. to fivth coefficient of Dubinin-Astakhov 
    isotherm";

    Real ddx_adsorpt_dc4_dc4 = (c[2] * c[3] * c[5] * (c[5] * (A / c[4])^c[5] -
      c[5] - 1) * (A / c[4])^c[5] * exp(-(A / c[4])^c[5])) / c[4]^2
      "Second-order partial derivative of uptake w.r.t. to fourth coefficient of 
    Dubinin-Astakhov isotherm";
    Real ddx_adsorpt_dc5_dc5 = c[2] * c[3] * log(A / c[4])^2 * ((A / c[4])^c[5] -
      1) * (A / c[4])^c[5] * exp(-(A / c[4])^c[5])
      "Second-order partial derivative of uptake w.r.t. to fivth coefficient of 
    Dubinin-Astakhov isotherm";

    Real ddx_adsorpt_dc2_dA= -(c[3] * c[5] * (A / c[4])^c[5] *
      exp(-(A / c[4])^c[5])) / A
      "Second-order partial derivative of uptake w.r.t. to second coefficient of 
    Dubinin-Astakhov isotherm and adsorption potential";
    Real ddx_adsorpt_dc2_dc3 = exp(-(A / c[4])^c[5])
      "Second-order partial derivative of uptake w.r.t. to second and third 
    coefficient of Dubinin-Astakhov isotherm";
    Real ddx_adsorpt_dc2_dc4 = (c[3] * c[5] * (A / c[4])^c[5] *
      exp(-(A / c[4])^c[5])) / c[4]
      "Second-order partial derivative of uptake w.r.t. to second and fourht 
    coefficient of Dubinin-Astakhov isotherm";
    Real ddx_adsorpt_dc2_dc5 = -c[3] * log(A / c[4]) * (A / c[4])^c[5] *
      exp(-(A / c[4])^c[5])
      "Second-order partial derivative of uptake w.r.t. to second and fivth 
    coefficient of Dubinin-Astakhov isotherm";

    Real ddx_adsorpt_dc3_dA = -(c[2] * c[5] * (A / c[4])^c[5] *
      exp(-(A / c[4])^c[5])) / A
      "Second-order partial derivative of uptake w.r.t. to third coefficient of 
    Dubinin-Astakhov isotherm and adsorption potential";
    Real ddx_adsorpt_dc3_dc4 = (c[2] * c[5] * (A / c[4])^c[5] *
      exp(-(A / c[4])^c[5])) / c[4]
      "Second-order partial derivative of uptake w.r.t. to third and fourht 
    coefficient of Dubinin-Astakhov isotherm";
    Real ddx_adsorpt_dc3_dc5 = -c[2] * log(A / c[4]) * (A / c[4])^c[5] *
      exp(-(A / c[4])^c[5])
      "Second-order partial derivative of uptake w.r.t. to third and fivth 
    coefficient of Dubinin-Astakhov isotherm";

    Real ddx_adsorpt_dc4_dA = -(c[2] * c[3] * c[5]^2 * (A / c[4])^c[5] *
      ((A / c[4])^c[5] - 1) * exp(-(A / c[4])^c[5])) / (c[4] * A)
      "Second-order partial derivative of uptake w.r.t. to fourth  coefficient of 
    Dubinin-Astakhov isotherm and adsorption potential";
    Real ddx_adsorpt_dc4_dc5 = -(c[2] * c[3] * (A / c[4])^c[5] * ((log(A / c[4]) *
      (A / c[4])^c[5] - log(A / c[4])) * c[5] - 1) * exp(-(A / c[4])^c[5])) / c[4]
      "Second-order partial derivative of uptake w.r.t. to fourth and fivth
    coefficient of Dubinin-Astakhov isotherm";

    Real ddx_adsorpt_dc5_dA = (c[2] * c[3] * (A / c[4])^c[5] *
      exp(-(A / c[4])^c[5]) * ((c[5] * (A / c[4])^c[5] - c[5]) *
      log(A / c[4]) - 1)) / A
      "Second-order partial derivative of uptake w.r.t. to fivth coefficient of 
    Dubinin-Astakhov isotherm and adsorption potential";

    Real ddx_adsorpt_dA_dT_dT_adsorpt = dc_dT_adsorpt[2] * dW_dA +
      c[2] * ddW_dA_dT_adsorpt
      "Second-order partial derivative of uptake w.r.t. to adsorption potential
    and temperature";
    Real ddx_adsorpt_dc2_dT_dT_adsorpt = ddx_adsorpt_dc2_dA*dA_dT_adsorpt +
      ddx_adsorpt_dc2_dc3*dc_dT_adsorpt[3] +
      ddx_adsorpt_dc2_dc4*dc_dT_adsorpt[4] +
      ddx_adsorpt_dc2_dc5*dc_dT_adsorpt[5]
      "Second-order partial derivative of uptake w.r.t. to second coefficient of 
    Dubinin-Astakhov isotherm and temperature";
    Real ddx_adsorpt_dc3_dT_dT_adsorpt = ddx_adsorpt_dc3_dA*dA_dT_adsorpt +
      ddx_adsorpt_dc2_dc3*dc_dT_adsorpt[2] +
      ddx_adsorpt_dc3_dc4*dc_dT_adsorpt[4] +
      ddx_adsorpt_dc3_dc5*dc_dT_adsorpt[5]
      "Second-order partial derivative of uptake w.r.t. to third coefficient of 
    Dubinin-Astakhov isotherm and temperature";
    Real ddx_adsorpt_dc4_dT_dT_adsorpt = ddx_adsorpt_dc4_dA*dA_dT_adsorpt +
      ddx_adsorpt_dc2_dc4*dc_dT_adsorpt[2] +
      ddx_adsorpt_dc3_dc4*dc_dT_adsorpt[3] +
      ddx_adsorpt_dc4_dc4*dc_dT_adsorpt[4] +
      ddx_adsorpt_dc4_dc5*dc_dT_adsorpt[5]
      "Second-order partial derivative of uptake w.r.t. to fourth coefficient of 
    Dubinin-Astakhov isotherm and temperature";
    Real ddx_adsorpt_dc5_dT_dT_adsorpt = ddx_adsorpt_dc5_dA*dA_dT_adsorpt +
      ddx_adsorpt_dc2_dc5*dc_dT_adsorpt[2] +
      ddx_adsorpt_dc3_dc5*dc_dT_adsorpt[3] +
      ddx_adsorpt_dc4_dc5*dc_dT_adsorpt[4] +
      ddx_adsorpt_dc5_dc5*dc_dT_adsorpt[5]
      "Second-order partial derivative of uptake w.r.t. to fivth coefficient of 
    Dubinin-Astakhov isotherm and temperature";

  algorithm
    ddx_adsorpt_dT_adsorpt_dT_adsorpt :=
      (ddx_adsorpt_dA_dT_dT_adsorpt*dA_dT_adsorpt +
       dx_adsorpt_dA*ddA_dT_adsorpt_dT_adsorpt) +
      (ddx_adsorpt_dc2_dT_dT_adsorpt*dc_dT_adsorpt[2] +
       dx_adsorpt_dc2*ddc_dT_adsorpt_dT_adsorpt[2]) +
      (ddx_adsorpt_dc3_dT_dT_adsorpt*dc_dT_adsorpt[3] +
       dx_adsorpt_dc3*ddc_dT_adsorpt_dT_adsorpt[3]) +
      (ddx_adsorpt_dc4_dT_dT_adsorpt*dc_dT_adsorpt[4] +
       dx_adsorpt_dc4*ddc_dT_adsorpt_dT_adsorpt[4]) +
      (ddx_adsorpt_dc5_dT_dT_adsorpt*dc_dT_adsorpt[5] +
       dx_adsorpt_dc5*ddc_dT_adsorpt_dT_adsorpt[5])
      "Calculation of the second-order partial derivative of the equilibrium uptake 
    w.r.t. the equilibrium temperature at constant pressure";
  end ddx_dT_dT;

  redeclare final function extends ddx_dp_dT
    "Dubinin-Astakhov isotherm model: Second-order partial derivative of uptake w.r.t. pressure and temperature"

    //
    // Definition of variables
    //
protected
    SorpLib.Units.MolarAdsorptionPotential A=
        SorpLib.Media.Functions.SorptionEquilibria.Utilities.A_ppsT(
        p_adsorpt=p_adsorpt,
        p_sat=c[1],
        T_adsorpt=T_adsorpt) "Adsorption potential";
    SorpLib.Units.DerMolarAdsorptionPotentialByPressure dA_dp_adsorpt=
        SorpLib.Media.Functions.SorptionEquilibria.Utilities.dA_dp(
        p_adsorpt=p_adsorpt,
        p_sat=c[1],
        T_adsorpt=T_adsorpt)
      "Partial derivative of adsorption potential w.r.t. to pressure at constant
      temperature";
    SorpLib.Units.DerMolarAdsorptionPotentialByPressureTemperature ddA_dp_adsorpt_dT_adsorpt=
        SorpLib.Media.Functions.SorptionEquilibria.Utilities.ddA_dp_dT(
        p_adsorpt=p_adsorpt,
        p_sat=c[1],
        T_adsorpt=T_adsorpt)
      "Second-order partial derivative of adsorption potential w.r.t. to pressure
      and temperature";

    SorpLib.Units.DerFilledPoreVolumeByAdsorptionPotential dW_dA=
        SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininAstakhov.dW_dA(
        A=A, c=c)
      "Partial derivative of filled pore volume w.r.t. adsorption potential at
      constant pressure and temperature";
    SorpLib.Units.DerFilledPoreVolumeByAdsorptionPotentialTemperature ddW_dA_dT_adsorpt=
        SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininAstakhov.ddW_dA_dT(
        p_adsorpt=p_adsorpt,
        T_adsorpt=T_adsorpt,
        A=A,
        c=c,
        dc_dT_adsorpt=dc_dT_adsorpt)
      "Second-order partial derivative of characteristic curve w.r.t. adsorption 
      potential and temperature at constant pressure";

  algorithm
    ddx_adsorpt_dp_adsorpt_dT_adsorpt :=
      dc_dT_adsorpt[2] * dW_dA * dA_dp_adsorpt +
      c[2] * ddW_dA_dT_adsorpt * dA_dp_adsorpt +
      c[2] * dW_dA * ddA_dp_adsorpt_dT_adsorpt
      "Calculation of the second-oder partial derivative of the equilibrium uptake 
     w.r.t. the equilibrium pressure and temperature";
  end ddx_dp_dT;

  redeclare function pi_pT
    "Dubinin-Astakhov isotherm model: Reduced spreading pressure as function of pressure and temperature (numerical solution)"
    extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_pi_pT_num(
        integral_pi_lb = Modelica.Constants.small,
        tolerance = 100*Modelica.Constants.eps,
        redeclare final function func_x_pT =
        SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininAstakhov.x_pT);
  end pi_pT;

  redeclare function p_piT
    "Dubinin-Astakhov isotherm model: Pressure as function of reduced spreading pressure and temperature (numerical solution)"
    extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_p_piT_num(
        p_adsorpt_lb_start = 1,
        integral_pi_lb = Modelica.Constants.small,
        tolerance_p_adsorpt = 1e-6,
        tolerance_pi = 100*Modelica.Constants.eps,
        redeclare final function func_pi_pT =
        SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininAstakhov.pi_pT);
  end p_piT;

  redeclare final function extends W_A
    "Dubinin-Astakhov isotherm model: Filled pore volume as function of adsorption potential"
  algorithm
    W := c[3] * exp(-(max(A, 0) / c[4]) ^ c[5])
      "Filled pore volume";

    //
    // Annotations
    //
    annotation (Inline=false,
  InlineAfterIndexReduction=false,
  LateInline=true,
  inverse(A=SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininAstakhov.A_W(W, c, A_lb_start, A_ub_start, tolerance)));
  end W_A;

  redeclare final function extends A_W
    "Dubinin-Astakhov isotherm model: Adsorption potential as function of the filled pore volume"
  algorithm
    A := c[4] * (-log(W/c[3])) ^ (1/c[5])
      "Adsorption potential";

    //
    // Annotations
    //
    annotation (Inline=false,
  InlineAfterIndexReduction=false,
  LateInline=true,
  inverse(W=SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininAstakhov.W_A(A, c, A_lb_start, A_ub_start, tolerance)));
  end A_W;

  redeclare final function extends dW_dA
    "Dubinin-Astakhov isotherm model: Partial derivative of filled pore volume w.r.t. adsorption potential at constant pressure and temperature"
  algorithm
    dW_dA := -c[5] / c[4] * (A / c[4]) ^ (c[5] - 1) * c[3] * exp(-(A / c[4]) ^ c[5])
      "Partial derivative of filled pore volume w.r.t. adsorption potential at 
    constant pressure and temperature";
  end dW_dA;

  redeclare final function extends ddW_dA_dA
    "Dubinin-Astakhov isotherm model: Second-order partial derivative of filled pore volume w.r.t. adsorption potential at constant pressure and temperature"
  algorithm
    ddW_dA_dA := (c[3] * c[5] * (A / c[4])^c[5] * (c[5] * ((A / c[4])^c[5] - 1) + 1) *
      exp(-(A / c[4])^c[5])) / A^2
      "Second-order partial derivative of filled pore volume w.r.t. adsorption 
    potential at constant pressure and temperature";
  end ddW_dA_dA;

  redeclare final function extends ddW_dA_dT
    "Dubinin-Astakhov isotherm model: Second-order partial derivative of filled pore volume w.r.t. adsorption potential and temperature at constant pressure"

    //
    // Definition of variables
    //
protected
    SorpLib.Units.DerMolarAdsorptionPotentialByTemperature dA_dT_adsorpt=
        SorpLib.Media.Functions.SorptionEquilibria.Utilities.dA_dT(
        p_adsorpt=p_adsorpt,
        p_sat=c[1],
        T_adsorpt=T_adsorpt,
        dp_sat_dT_adsorpt=dc_dT_adsorpt[1])
      "Partial derivative of adsorption potential w.r.t. equilibrium temperature
      at constant pressure";

    Real ddW_dA_dA = (c[3] * c[5] * (A / c[4])^c[5] * (c[5] * (A / c[4])^c[5] -
      c[5] + 1) * exp(-(A / c[4])^c[5])) / A^2
      "Second-order partial derivative of filled pore volume w.r.t. adsorption
    potential";
    Real ddW_dA_dc3 = -((A / c[4])^c[5] * c[5] * exp(-(A / c[4])^c[5])) / A
      "Second-order partial derivative of filled pore volume w.r.t. adsorption
    potential and third coefficient of the Dubinin-Astakhov isotherm model";
    Real ddW_dA_dc4 = -(c[3] * c[5]^2 * ((A / c[4])^c[5] - 1) * (A / c[4])^c[5] *
      exp(-(A / c[4])^c[5])) / (A * c[4])
      "Second-order partial derivative of filled pore volume w.r.t. adsorption
    potential and fourth coefficient of the Dubinin-Astakhov isotherm model";
    Real ddW_dA_dc5 = (c[3] * (A / c[4])^c[5] * ((log(A / c[4]) * (A / c[4])^c[5] -
      log(A / c[4])) * c[5] - 1) * exp(-(A / c[4])^c[5])) / A
      "Second-order partial derivative of filled pore volume w.r.t. adsorption
    potential and fivth coefficient of the Dubinin-Astakhov isotherm model";

  algorithm
    ddW_dA_dT := ddW_dA_dA * dA_dT_adsorpt +
      ddW_dA_dc3 * dc_dT_adsorpt[3] +
      ddW_dA_dc4 * dc_dT_adsorpt[4] +
      ddW_dA_dc5 * dc_dT_adsorpt[5]
      "Second-order partial derivative of filled pore volume w.r.t. adsorption 
    potential and temperature at constant pressure";
  end ddW_dA_dT;
  //
  // Annotations
  //
annotation (Documentation(revisions="<html>
<ul>
  <li>
  November 2, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>", info="<html>
<p>
The Dubinin-Astakhov isotherm model is a five-parameter model for calculating the
equilibrium uptake <i>x_adsorpt</i> as a function of the equilibrium pressure <i>p_adsorpt</i>.
The Dubinin-Astakhov isotherm model is suitable for type I, IV and V isotherms 
according to the IUPAC definition.
</p>

<h4>Main equations</h4>
<p>
The Dubinin-Astakhov isotherm model has the following form:
</p>
<pre>
    x<sub>adsorpt</sub> = &rho;<sub>adsorpt</sub>(T<sub>adsorpt</sub>) * W(A(T<sub>adsorpt</sub>));
</pre>
<p>
with
</p>
<pre>
    W(A(T<sub>adsorpt</sub>)) = W<sub>0</sub> * <strong>exp</strong>(-(A(T<sub>adsorpt</sub>) / E) ^ n);
</pre>
<pre>
    A(T<sub>adsorpt</sub>) = R * T<sub>adsorpt</sub> * <strong>ln</strong>(p<sub>sat</sub>(T<sub>adsorpt</sub>) / p<sub>adsorpt</sub>);
</pre>
<p>
Herein, <i>W(A(T<sub>adsorpt</sub>))</i> is the so-called characteristic curve and
<i>A(T<sub>adsorpt</sub>)</i> is the adsorption potential. Within the characteristic 
curve, the parameter <i>W<sub>0</sub></i> describes the maximum filled pore volume,
<i>E</i> is the characteristic energy, and <i>n</i> describes the heterogeneity of the
system. These three parameters can be used as fitting parameters.
<br/><br/>
Note that the exponent <i>n</i> is typically greater than unity. Furthermore, the 
density of the adsorpt <i>&rho;<sub>adsorpt</sub>(T<sub>adsorpt</sub>)</i> is assumed 
to be the saturated liquid density &rho;<sub>sat,liq</sub>(T<sub>adsorpt</sub>) without 
any further information about the system under consideration. For super-critical
adsorptives (i.e., <i>T<sub>adsorpt</sub> &ge; T<sub>crit</sub></i>), the density of
the adsorpt <i>&rho;<sub>adsorpt</sub>(T<sub>adsorpt</sub>)</i> can be estimated by
</p>
<pre>
    &rho;<sub>adsorpt</sub>(T<sub>adsorpt</sub>) = &rho;<sub>sat,liq</sub>(T<sub>boiling,0</sub>) * <strong>exp</strong>(-0.0025 * (T<sub>adsorpt</sub> - T<sub>boiling,0</sub>));
</pre>
<p>
and a pseudo-vapour pressure <i>p<sub>sat</sub>(T<sub>adsorpt</sub>)</i> can be 
calculated by
</p>
<pre>
    p<sub>sat</sub>(T<sub>adsorpt</sub>) = p<sub>crit</sub>(T<sub>adsorpt</sub>) * (T<sub>adsorpt</sub> / T<sub>crit</sub>) ^ k;
</pre>
<p>
where <i>T<sub>boiling,0</sub></i> is the normal boiling point at 1 atm and <i>k</i> is
a fitting parameter specific to the system under consideration.
</p>

<h4>Required parameter order in function input c[:]:</h4>
<ul>
  <li>
  c[1] = p<sub>sat</sub>(T<sub>adsorpt</sub>) in Pa
  </li>
  <li>
  c[2] = &rho;<sub>adsorpt</sub>(T<sub>adsorpt</sub>) in kg/m<sup>3</sup>
  </li>
  <li>
  c[3] = W<sub>0</sub> in m<sup>3</sup>/kg
  </li>
  <li>
  c[4] = E in J/mol
  </li>
  <li>
  c[5] = n in -
  </li>
</ul>

<h4>Assumptions and limitations</h4>
<ul>
  <li>
  The characteristic curve <i>W(A)</i> must decrease strictly monotonically with 
  increasing adsorption potential <i>A</i>. Otherwise, the inverses <i>A(W)</i>
  and <i>p(x,T</i>) may not be solveable.
  </li>
  <li>
  The reduced spreading pressure <i>&pi;</i> may not be calculable. Accordingly, the 
  inverse <i>p(&pi;,T)</i> cannot be calculated either.
  </li>
</ul>

<h4>Typical use</h4>
<p>
The isotherm model is used for type I, IV, and V isotherms according to the IUPAC
definition.
</p>

<h4>Example</h4>
<p>
The following figure shows the Dubinin-Astakhov isotherm model for different parameter 
sets. In the upper sub-figure, the equilibrium pressure changes with time, while the 
equilibrium temperature is constant. In the centered sub-figure, the equilibrium 
temperature changes with time, while the equilibrium pressure is constant. In the 
lower sub-figure, the characteristic curve is shown. 
<br/><br/>
</p>
<img src=\"Modelica://SorpLib/Resources/doc/media_functions_equilibria_pure_dubinin_astakhov.png\" alt=\"media_functions_equilibria_pure_dubinin_astakhov.png\">

<h4>References</h4>
<ul>
  <li>
  Do, D. D. (1998). Adsorption Analysis: Equilibria and Kinetics, 1st Edition, ISBN 978-1-86094-130-6, Imperial College Press.
  </li>
</ul>
</html>"));
end DubininAstakhov;
