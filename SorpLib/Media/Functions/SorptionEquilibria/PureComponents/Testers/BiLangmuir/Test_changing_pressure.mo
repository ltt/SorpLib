within SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Testers.BiLangmuir;
model Test_changing_pressure
  "Tester for all functions of the Bi-Langmuir isotherm model: Changing pressure"

  //
  // Definition of parameters
  //
  parameter SorpLib.Units.Uptake a0_1 = 0.35
    "First fitting parameter"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter SorpLib.Units.Uptake a0_2 = 0.45
    "First fitting parameter"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real a1_1(unit="kg.K/kg") = 1e-4
    "Second fitting parameter"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real a1_2(unit="kg.K/kg") = 2e-4
    "Second fitting parameter"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real b0_1(unit="1/Pa") = 5e-8
    "Third fitting parameter"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real b0_2(unit="1/Pa") = 6e-8
    "Third fitting parameter"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real Delta_H_ads_1(unit="J/mol") = -2e4
    "Fourth fitting parameter"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real Delta_H_ads_2(unit="J/mol") = -2.5e4
    "Fourth fitting parameter"
    annotation (Dialog(tab="General", group="Parameters"));

  extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialTestPure(
    final p_adsorpt_der = 1000,
    final T_adsorpt_der = 0,
    final p_adsorpt_start = 0,
    final T_adsorpt_start = 298.15,
    final M_adsorptive = 0.044,
    final no_coefficients = 4,
    redeclare final package IsothermModel =
      SorpLib.Media.Functions.SorptionEquilibria.PureComponents.BiLangmuir,
    final dp=1e-3,
    final dT=1e-3);

equation
  //
  // Coefficients of the isotherm model
  //
  c[1] = a0_1 + a1_1/T_adsorpt;
  c[2] = b0_1*exp(-Delta_H_ads_1/(Modelica.Constants.R*T_adsorpt));
  c[3] = a0_2 + a1_2/T_adsorpt;
  c[4] = b0_2*exp(-Delta_H_ads_2/(Modelica.Constants.R*T_adsorpt));

  //
  // Partial derivative of coefficients of isotherm model w.r.t. temperature
  //
  dc_dT[1] = -a1_1/T_adsorpt^2;
  dc_dT[2] = Delta_H_ads_1/(Modelica.Constants.R*T_adsorpt^2) * c[2];
  dc_dT[3] = -a1_2/T_adsorpt^2;
  dc_dT[4] = Delta_H_ads_2/(Modelica.Constants.R*T_adsorpt^2) * c[4];

  //
  // Second-oder partial derivative of coefficients of isotherm model w.r.t. temperature
  //
  ddc_dT_dT[1] = -2*dc_dT[1]/T_adsorpt;
  ddc_dT_dT[2] = -(2*Modelica.Constants.R*T_adsorpt - Delta_H_ads_1) /
    (Modelica.Constants.R*T_adsorpt^2) * dc_dT[2];
  ddc_dT_dT[3] = -2*dc_dT[3]/T_adsorpt;
  ddc_dT_dT[4] = -(2*Modelica.Constants.R*T_adsorpt - Delta_H_ads_2) /
    (Modelica.Constants.R*T_adsorpt^2) * dc_dT[4];

  //
  // Coefficients of isotherm model: T + dT K
  //
  c_pdT[1] = a0_1 + a1_1/(T_adsorpt+dT);
  c_pdT[2] = b0_1*exp(-Delta_H_ads_1/(Modelica.Constants.R*(T_adsorpt+dT)));
  c_pdT[3] = a0_2 + a1_2/(T_adsorpt+dT);
  c_pdT[4] = b0_2*exp(-Delta_H_ads_2/(Modelica.Constants.R*(T_adsorpt+dT)));

  //
  // Coefficients of isotherm model: T - dT K
  //
  c_mdT[1] = a0_1 + a1_1/(T_adsorpt-dT);
  c_mdT[2] = b0_1*exp(-Delta_H_ads_1/(Modelica.Constants.R*(T_adsorpt-dT)));
  c_mdT[3] = a0_2 + a1_2/(T_adsorpt-dT);
  c_mdT[4] = b0_2*exp(-Delta_H_ads_2/(Modelica.Constants.R*(T_adsorpt-dT)));

  //
  // Annotations
  //
  annotation (experiment(StartTime=0, StopTime=100, Tolerance=1e-6),
Documentation(revisions="<html>
<ul>
  <li>
  November 1, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>", info="<html>
<p>
This tester shows the behavior of all functions of the Bi-Langmuir isotherm model.
<br/><br/>
As an example, this tester increases the pressure with time. To see the behavior of
all functions, plot the variables <i>x_adsorpt</i>, <i>dx_adsorpt_dp_adsorpt</i>, 
<i>dx_adsorpt_dT_adsorpt</i>, <i>dx_adsorpt_dp_adsorpt_dT_adsorpt</i>, 
<i>dx_adsorpt_dp_adsorpt_dp_adsorpt</i>, <i>dx_adsorpt_dT_adsorpt_dT_adsorpt</i>, 
and <i>red_spreading_pressure</i> over the variable <i>p_adsorpt</i>. The simulation 
time is correctly preset (Start: 0 s, Stop = 100 s).
</p>
</html>"));
end Test_changing_pressure;
