within SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Testers.DubininChebyshevSeriesRaionalOrder33;
model Test_changing_temperature
  "Tester for all functions of the Dubinin-Chebyshev-Series-3/3 isotherm model: Changing temperature"

  //
  // Definition of parameters
  //
  parameter SorpLib.Units.FilledPoreVolume char_curve_a = 3.472616e-2 / 1000
    "First fitting parameter"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real char_curve_b = 1.322831
    "Second fitting parameter"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter SorpLib.Units.FilledPoreVolume char_curve_c = 9.401171e-3 / 1000
    "Third fitting parameter"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real char_curve_d = 5.760414e-1
    "Fourth fitting parameter"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter SorpLib.Units.FilledPoreVolume char_curve_e = 1.350676e-3 / 1000
    "Fivth fitting parameter"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real char_curve_f = 1.313913e-1
    "Sixth fitting parameter"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter SorpLib.Units.FilledPoreVolume char_curve_g(min=-1) = -1.926730e-4 / 1000
    "Seventh fitting parameter"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter SorpLib.Units.MolarAdsorptionPotential char_curve_h = 1061.930 * 1000 * 0.018
    "Ninth fitting parameter"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter SorpLib.Units.MolarAdsorptionPotential char_curve_i = 1065.595 * 1000 * 0.018
    "Tenth fitting parameter"
    annotation (Dialog(tab="General", group="Parameters"));

  extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialTestPureDubinin(
    final p_adsorpt_der = 0,
    final T_adsorpt_der = 40/100,
    final p_adsorpt_start = 10000,
    final T_adsorpt_start = 323.15,
    final M_adsorptive = 0.018,
    final no_coefficients = 11,
    redeclare final package IsothermModel =
      SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininChebyshevSeriesRaionalOrder33,
    final p_lb_pi=1e-2,
    final tolerance_p_pi=1e-6,
    final tolerance_pi=1e-6,
    final dp=1e-3,
    final dT=1e-3,
    final dA=1e-3);

equation
  //
  // Coefficients of the isotherm model
  //
  c[1] = Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.psat(T=T_adsorpt);
  c[2] = Modelica.Media.Water.IF97_Utilities.BaseIF97.Regions.rhol_T(T=T_adsorpt);
  c[3] = char_curve_a-1e-10*T_adsorpt^2;
  c[4] = char_curve_b-1e-10*T_adsorpt^2;
  c[5] = char_curve_c-1e-10*T_adsorpt^2;
  c[6] = char_curve_d-1e-10*T_adsorpt^2;
  c[7] = char_curve_e-1e-10*T_adsorpt^2;
  c[8] = char_curve_f-1e-10*T_adsorpt^2;
  c[9] = char_curve_g-1e-10*T_adsorpt^2;
  c[10] = char_curve_h-1e-10*T_adsorpt^2;
  c[11] = char_curve_i-1e-10*T_adsorpt^2;

  //
  // Partial derivative of coefficients of isotherm model w.r.t. temperature
  //
  dc_dT[1] = Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.dptofT(T=T_adsorpt);
  dc_dT[2] = Modelica.Media.Water.IF97_Utilities.BaseIF97.Regions.drhol_dp(p=c[1]) *
    dc_dT[1];
  dc_dT[3] = -2*1e-10*T_adsorpt;
  dc_dT[4] = -2*1e-10*T_adsorpt;
  dc_dT[5] = -2*1e-10*T_adsorpt;
  dc_dT[6] = -2*1e-10*T_adsorpt;
  dc_dT[7] = -2*1e-10*T_adsorpt;
  dc_dT[8] = -2*1e-10*T_adsorpt;
  dc_dT[9] = -2*1e-10*T_adsorpt;
  dc_dT[10] = -2*1e-10*T_adsorpt;
  dc_dT[11] = -2*1e-10*T_adsorpt;

  //
  // Second-oder partial derivative of coefficients of isotherm model w.r.t. temperature
  //
  ddc_dT_dT[1] = (Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.dptofT(T=T_adsorpt+dT)
    - Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.dptofT(T=T_adsorpt-dT)) /
    (2*dT);
  ddc_dT_dT[2] = (Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.dptofT(T=T_adsorpt+dT) *
    Modelica.Media.Water.IF97_Utilities.BaseIF97.Regions.drhol_dp(p=c_pdT[1]) -
    Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.dptofT(T=T_adsorpt-dT) *
    Modelica.Media.Water.IF97_Utilities.BaseIF97.Regions.drhol_dp(p=c_mdT[1])) / (2*dT);
  ddc_dT_dT[3] = -2*1e-10;
  ddc_dT_dT[4] = -2*1e-10;
  ddc_dT_dT[5] = -2*1e-10;
  ddc_dT_dT[6] = -2*1e-10;
  ddc_dT_dT[7] = -2*1e-10;
  ddc_dT_dT[8] = -2*1e-10;
  ddc_dT_dT[9] = -2*1e-10;
  ddc_dT_dT[10] = -2*1e-10;
  ddc_dT_dT[11] = -2*1e-10;

  //
  // Coefficients of isotherm model: T + dT K
  //
  c_pdT[1] = Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.psat(T=T_adsorpt+dT);
  c_pdT[2] = Modelica.Media.Water.IF97_Utilities.BaseIF97.Regions.rhol_T(T=T_adsorpt+dT);
  c_pdT[3] = char_curve_a-1e-10*(T_adsorpt+dT)^2;
  c_pdT[4] = char_curve_b-1e-10*(T_adsorpt+dT)^2;
  c_pdT[5] = char_curve_c-1e-10*(T_adsorpt+dT)^2;
  c_pdT[6] = char_curve_d-1e-10*(T_adsorpt+dT)^2;
  c_pdT[7] = char_curve_e-1e-10*(T_adsorpt+dT)^2;
  c_pdT[8] = char_curve_f-1e-10*(T_adsorpt+dT)^2;
  c_pdT[9] = char_curve_g-1e-10*(T_adsorpt+dT)^2;
  c_pdT[10] = char_curve_h-1e-10*(T_adsorpt+dT)^2;
  c_pdT[11] = char_curve_i-1e-10*(T_adsorpt+dT)^2;

  //
  // Coefficients of isotherm model: T - dT K
  //
  c_mdT[1] = Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.psat(T=T_adsorpt-dT);
  c_mdT[2] = Modelica.Media.Water.IF97_Utilities.BaseIF97.Regions.rhol_T(T=T_adsorpt-dT);
  c_mdT[3] = char_curve_a-1e-10*(T_adsorpt-dT)^2;
  c_mdT[4] = char_curve_b-1e-10*(T_adsorpt-dT)^2;
  c_mdT[5] = char_curve_c-1e-10*(T_adsorpt-dT)^2;
  c_mdT[6] = char_curve_d-1e-10*(T_adsorpt-dT)^2;
  c_mdT[7] = char_curve_e-1e-10*(T_adsorpt-dT)^2;
  c_mdT[8] = char_curve_f-1e-10*(T_adsorpt-dT)^2;
  c_mdT[9] = char_curve_g-1e-10*(T_adsorpt-dT)^2;
  c_mdT[10] = char_curve_h-1e-10*(T_adsorpt-dT)^2;
  c_mdT[11] = char_curve_i-1e-10*(T_adsorpt-dT)^2;

  //
  // Annotations
  //
  annotation (experiment(
      StopTime=100,
      Interval=0.01,
      Tolerance=1e-06),
Documentation(revisions="<html>
<ul>
  <li>
  November 2, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>", info="<html>
<p>
This tester shows the behavior of all functions of the Dubinin-Chebyshev-Series-3/3
isotherm models. 
<br/><br/>
As an example, this tester increases the temperature with time. To see the behavior of
all functions, plot the variables <i>x_adsorpt</i>, <i>dx_adsorpt_dp_adsorpt</i>, 
<i>dx_adsorpt_dT_adsorpt</i>, <i>dx_adsorpt_dp_adsorpt_dT_adsorpt</i>, 
<i>dx_adsorpt_dp_adsorpt_dp_adsorpt</i>, <i>dx_adsorpt_dT_adsorpt_dT_adsorpt</i>, 
and <i>red_spreading_pressure</i> over the variable <i>p_adsorpt</i>. The simulation 
time is correctly preset (Start: 0 s, Stop = 100 s). 
</p>
</html>"));
end Test_changing_temperature;
