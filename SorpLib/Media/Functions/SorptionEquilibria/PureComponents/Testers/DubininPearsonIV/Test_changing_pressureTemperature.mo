within SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Testers.DubininPearsonIV;
model Test_changing_pressureTemperature
  "Tester for all functions of the Dubinin-Pearson-IV isotherm model: Changing pressure and temperature"

  //
  // Definition of parameters
  //
  parameter SorpLib.Units.FilledPoreVolume char_curve_a(min=-1) = -7.689279e-1 / 1000
    "First fitting parameter"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real char_curve_b(unit="J/mol") = 1.176831 / 1000
    "Second fitting parameter"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real char_curve_c(unit="J/mol") = 1.485965e1 * 1000 * 0.018
    "Third fitting parameter"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter SorpLib.Units.FilledPoreVolume char_curve_d = 4.244922e1 * 1000 * 0.018
    "Fourth fitting parameter"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter SorpLib.Units.FilledPoreVolume char_curve_e = 2.207797e-2
    "Fivth fitting parameter"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter SorpLib.Units.FilledPoreVolume char_curve_f = 1.146067e-1
    "Sixth fitting parameter"
    annotation (Dialog(tab="General", group="Parameters"));

  extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialTestPureDubinin(
    final p_adsorpt_der = 300,
    final T_adsorpt_der = 80/100,
    final p_adsorpt_start = 1,
    final T_adsorpt_start = 298.15,
    final M_adsorptive = 0.018,
    final no_coefficients = 8,
    redeclare final package IsothermModel =
      SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininPearsonIV,
    final p_lb_pi=1e-2,
    final tolerance_p_pi=1e-6,
    final tolerance_pi=100*Modelica.Constants.eps,
    final dp=1e-3,
    final dT=1e-3,
    final dA=1e-3,
    check_func_p_piT=false);

equation
  //
  // Coefficients of the isotherm model
  //
  c[1] = Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.psat(T=T_adsorpt);
  c[2] = Modelica.Media.Water.IF97_Utilities.BaseIF97.Regions.rhol_T(T=T_adsorpt);
  c[3] = char_curve_a-1e-10*T_adsorpt^2;
  c[4] = char_curve_b-1e-10*T_adsorpt^2;
  c[5] = char_curve_c-1e-10*T_adsorpt^2;
  c[6] = char_curve_d-1e-10*T_adsorpt^2;
  c[7] = char_curve_e-1e-10*T_adsorpt^2;
  c[8] = char_curve_f-1e-10*T_adsorpt^2;

  //
  // Partial derivative of coefficients of isotherm model w.r.t. temperature
  //
  dc_dT[1] = Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.dptofT(T=T_adsorpt);
  dc_dT[2] = Modelica.Media.Water.IF97_Utilities.BaseIF97.Regions.drhol_dp(p=c[1]) *
    dc_dT[1];
  dc_dT[3] = -2*1e-10*T_adsorpt;
  dc_dT[4] = -2*1e-10*T_adsorpt;
  dc_dT[5] = -2*1e-10*T_adsorpt;
  dc_dT[6] = -2*1e-10*T_adsorpt;
  dc_dT[7] = -2*1e-10*T_adsorpt;
  dc_dT[8] = -2*1e-10*T_adsorpt;

  //
  // Second-oder partial derivative of coefficients of isotherm model w.r.t. temperature
  //
  ddc_dT_dT[1] = (Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.dptofT(T=T_adsorpt+dT)
    - Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.dptofT(T=T_adsorpt-dT)) /
    (2*dT);
  ddc_dT_dT[2] = (Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.dptofT(T=T_adsorpt+dT) *
    Modelica.Media.Water.IF97_Utilities.BaseIF97.Regions.drhol_dp(p=c_pdT[1]) -
    Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.dptofT(T=T_adsorpt-dT) *
    Modelica.Media.Water.IF97_Utilities.BaseIF97.Regions.drhol_dp(p=c_mdT[1])) / (2*dT);
  ddc_dT_dT[3] = -2*1e-10;
  ddc_dT_dT[4] = -2*1e-10;
  ddc_dT_dT[5] = -2*1e-10;
  ddc_dT_dT[6] = -2*1e-10;
  ddc_dT_dT[7] = -2*1e-10;
  ddc_dT_dT[8] = -2*1e-10;

  //
  // Coefficients of isotherm model: T + dT K
  //
  c_pdT[1] = Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.psat(T=T_adsorpt+dT);
  c_pdT[2] = Modelica.Media.Water.IF97_Utilities.BaseIF97.Regions.rhol_T(T=T_adsorpt+dT);
  c_pdT[3] = char_curve_a-1e-10*(T_adsorpt+dT)^2;
  c_pdT[4] = char_curve_b-1e-10*(T_adsorpt+dT)^2;
  c_pdT[5] = char_curve_c-1e-10*(T_adsorpt+dT)^2;
  c_pdT[6] = char_curve_d-1e-10*(T_adsorpt+dT)^2;
  c_pdT[7] = char_curve_e-1e-10*(T_adsorpt+dT)^2;
  c_pdT[8] = char_curve_f-1e-10*(T_adsorpt+dT)^2;

  //
  // Coefficients of isotherm model: T - dT K
  //
  c_mdT[1] = Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.psat(T=T_adsorpt-dT);
  c_mdT[2] = Modelica.Media.Water.IF97_Utilities.BaseIF97.Regions.rhol_T(T=T_adsorpt-dT);
  c_mdT[3] = char_curve_a-1e-10*(T_adsorpt-dT)^2;
  c_mdT[4] = char_curve_b-1e-10*(T_adsorpt-dT)^2;
  c_mdT[5] = char_curve_c-1e-10*(T_adsorpt-dT)^2;
  c_mdT[6] = char_curve_d-1e-10*(T_adsorpt-dT)^2;
  c_mdT[7] = char_curve_e-1e-10*(T_adsorpt-dT)^2;
  c_mdT[8] = char_curve_f-1e-10*(T_adsorpt-dT)^2;

  //
  // Annotations
  //
  annotation (experiment(StartTime=0, StopTime=100, Tolerance=1e-6),
Documentation(revisions="<html>
<ul>
  <li>
  November 2, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>", info="<html>
<p>
This tester shows the behavior of all functions of the Dubinin-Pearson-IV
isotherm models.
<br/><br/>
As an example, this tester increases the pressure and temperature with time. To 
see the behavior of all functions, plot the variables <i>x_adsorpt</i>, 
<i>dx_adsorpt_dp_adsorpt</i>, <i>dx_adsorpt_dT_adsorpt</i>, 
<i>dx_adsorpt_dp_adsorpt_dT_adsorpt</i>, <i>dx_adsorpt_dp_adsorpt_dp_adsorpt</i>, 
<i>dx_adsorpt_dT_adsorpt_dT_adsorpt</i>, and <i>red_spreading_pressure</i> over 
the variable <i>p_adsorpt</i>. The simulation time is correctly preset (Start: 0 s, 
Stop = 100 s).
</p>
</html>"));
end Test_changing_pressureTemperature;
