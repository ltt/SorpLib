within SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Testers.Henry;
model Test_changing_pressure
  "Tester for all functions of the Henry isotherm model: Changing pressure"

  //
  // Definition of parameters
  //
  parameter Real H_ref(unit="kg/(kg.Pa)") = 0.01
    "Henry constant at reference temperature"
    annotation (Dialog(tab="General", group="Parameters"));
  final parameter Real C_exp(unit="K") = 2400
    "Exponent describing change of Henry constant with temperature"
    annotation (Dialog(tab="General", group="Parameters"));
  final parameter Modelica.Units.SI.Temperature T_ref = 298.15
    "Reference temperature for Henry constant"
    annotation (Dialog(tab="General", group="Parameters"));

  extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialTestPure(
    final p_adsorpt_der = 1,
    final T_adsorpt_der = 0,
    final p_adsorpt_start = 0,
    final T_adsorpt_start = 298.15,
    final M_adsorptive = 0.018,
    final no_coefficients = 1,
    redeclare final package IsothermModel =
      SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Henry,
    final dp=1e-3,
    final dT=1e-3);

equation
  //
  // Coefficients of the isotherm model
  //
  c[1] = H_ref * exp(C_exp * (1/T_adsorpt - 1 / T_ref));

  //
  // Partial derivative of coefficients of isotherm model w.r.t. temperature
  //
  dc_dT[1] = -C_exp * c[1] / (T_adsorpt^2);

  //
  // Second-oder partial derivative of coefficients of isotherm model w.r.t. temperature
  //
  ddc_dT_dT[1] = -dc_dT[1] * (2 * T_adsorpt + C_exp) / (T_adsorpt^2);

  //
  // Coefficients of isotherm model: T + dT K
  //
  c_pdT[1] = H_ref * exp(C_exp * (1/(T_adsorpt + dT) - 1 / T_ref));

  //
  // Coefficients of isotherm model: T - dT K
  //
  c_mdT[1] = H_ref * exp(C_exp * (1/(T_adsorpt - dT) - 1 / T_ref));

  //
  // Annotations
  //
  annotation (experiment(StartTime=0, StopTime=100, Tolerance=1e-6),
Documentation(revisions="<html>
<ul>
  <li>
  November 1, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>", info="<html>
<p>
This tester shows the behavior of all functions of the Henry isotherm model.
<br/><br/>
As an example, this tester increases the pressure with time. To see the behavior of
all functions, plot the variables <i>x_adsorpt</i>, <i>dx_adsorpt_dp_adsorpt</i>, 
<i>dx_adsorpt_dT_adsorpt</i>, <i>dx_adsorpt_dp_adsorpt_dT_adsorpt</i>, 
<i>dx_adsorpt_dp_adsorpt_dp_adsorpt</i>, <i>dx_adsorpt_dT_adsorpt_dT_adsorpt</i>, 
and <i>red_spreading_pressure</i> over the variable <i>p_adsorpt</i>. The simulation 
time is correctly preset (Start: 0 s, Stop = 100 s).
</p>
</html>"));
end Test_changing_pressure;
