within SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Testers;
package Toth "Models to test and varify functions of the Toth isotherm"
  extends Modelica.Icons.ExamplesPackage;

  annotation (Documentation(info="<html>
<p>
This package contains executable test models for all functions of the Toth isotherm
model. The test models check the implementation of the functions and enable verification 
of the function behavior. Three test models are implemented, in which the pressure and 
temperature change over time. In addition, the test models demonstrate the functions' 
general applicability.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end Toth;
