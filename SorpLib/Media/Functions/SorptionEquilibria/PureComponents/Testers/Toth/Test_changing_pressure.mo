within SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Testers.Toth;
model Test_changing_pressure
  "Tester for all functions of the Toth isotherm model: Changing pressure"

  //
  // Definition of parameters
  //
  parameter SorpLib.Units.Uptake x_ref = 0.38
    "Saturation uptake at reference temperature"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real chi(unit="1") = 0.01
    "Parameter describing the change of the saturation uptake with temperature"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real b_ref(unit="1/Pa") = 10.54e-3
    "Toth coefficient at reference temperature"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real Q(unit="J/mol") = 46.093e3
    "Parameter describing the change of the Toth coefficient with temperature"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real t_ref(unit="1") = 0.2842
    "Toth exponent at reference temperature"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real alpha(unit="1") = 0.284
    "Parameter describing the change of the Toth exponent with temperature"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Modelica.Units.SI.Temperature T_ref = 283
    "Reference temperature"
    annotation (Dialog(tab="General", group="Parameters"));

  extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialTestPure(
    final p_adsorpt_der = 1400,
    final T_adsorpt_der = 0,
    final p_adsorpt_start = 1,
    final T_adsorpt_start = 298.15,
    final M_adsorptive = 0.044,
    final no_coefficients = 3,
    redeclare final package IsothermModel =
      SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Toth,
    final tolerance_p_pi=1e-6,
    final tolerance_pi=100*Modelica.Constants.eps,
    final dp=1e-3,
    final dT=1e-3);

equation
  //
  // Coefficients of the isotherm model
  //
  c[1] = x_ref * exp(chi * (1 - T_adsorpt/T_ref));
  c[2] = b_ref * exp(Q/Modelica.Constants.R/T_ref * (T_ref/T_adsorpt - 1));
  c[3] = t_ref + alpha * (1 - T_ref/T_adsorpt);

  //
  // Partial derivative of coefficients of isotherm model w.r.t. temperature
  //
  dc_dT[1] = -chi/T_ref * c[1];
  dc_dT[2] = -Q/Modelica.Constants.R/T_adsorpt^2 * c[2];
  dc_dT[3] = T_ref*alpha/T_adsorpt^2;

  //
  // Second-oder partial derivative of coefficients of isotherm model w.r.t. temperature
  //
  ddc_dT_dT[1] = -chi/T_ref * dc_dT[1];
  ddc_dT_dT[2] = -1/Modelica.Constants.R/T_adsorpt^2 *
    (2*Modelica.Constants.R*T_adsorpt + Q) * dc_dT[2];
  ddc_dT_dT[3] = -2/T_adsorpt * dc_dT[3];

  //
  // Coefficients of isotherm model: T + dT K
  //
  c_pdT[1] = x_ref * exp(chi * (1 - (T_adsorpt+dT)/T_ref));
  c_pdT[2] = b_ref * exp(Q/Modelica.Constants.R/T_ref * (T_ref/(T_adsorpt+dT) - 1));
  c_pdT[3] = t_ref + alpha * (1 - T_ref/(T_adsorpt+dT));

  //
  // Coefficients of isotherm model: T - dT K
  //
  c_mdT[1] = x_ref * exp(chi * (1 - (T_adsorpt-dT)/T_ref));
  c_mdT[2] = b_ref * exp(Q/Modelica.Constants.R/T_ref * (T_ref/(T_adsorpt-dT) - 1));
  c_mdT[3] = t_ref + alpha * (1 - T_ref/(T_adsorpt-dT));

  //
  // Annotations
  //
  annotation (experiment(StartTime=0, StopTime=100, Tolerance=1e-6),
Documentation(revisions="<html>
<ul>
  <li>
  November 1, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>", info="<html>
<p>
This tester shows the behavior of all functions of the Toth isotherm models.
<br/><br/>
As an example, this tester increases the pressure with time. To see the behavior of
all functions, plot the variables <i>x_adsorpt</i>, <i>dx_adsorpt_dp_adsorpt</i>, 
<i>dx_adsorpt_dT_adsorpt</i>, <i>dx_adsorpt_dp_adsorpt_dT_adsorpt</i>, 
<i>dx_adsorpt_dp_adsorpt_dp_adsorpt</i>, <i>dx_adsorpt_dT_adsorpt_dT_adsorpt</i>, 
and <i>red_spreading_pressure</i> over the variable <i>p_adsorpt</i>. The simulation 
time is correctly preset (Start: 0 s, Stop = 100 s). 
</p>
</html>"));
end Test_changing_pressure;
