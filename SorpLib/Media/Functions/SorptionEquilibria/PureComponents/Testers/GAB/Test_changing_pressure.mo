within SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Testers.GAB;
model Test_changing_pressure
  "Tester for all functions of the GAB isotherm model: Changing pressure"

  //
  // Definition of parameters
  //
  parameter SorpLib.Units.Uptake x_mon_ref = 0.06534
    "Monolayer uptake at reference temperature"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real chi(unit="1") = 0
    "Parameter describing the change of the monolayer uptake with temperature"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real C(unit="J/mol") = 47110
    "Adsorption enthalpy of first layer: First fitting parameter"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real D(unit="1/K") = 0.023744
    "Adsorption enthalpy of first layer: Second fitting parameter"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real F(unit="J/mol") = 57706
    "Adsorption enthalpy of layers 2-9: First fitting parameter"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real G(unit="J/(mol.K)") = -47.814
    "Adsorption enthalpy of layers 2-9: Second fitting parameter"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real H(unit="J/mol") = 57220
    "Adsorption enthalpy of layer 10 or higher layers: First fitting parameter"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real I(unit="J/(mol.K)") = -44.38
    "Adsorption enthalpy of layer 10 or higher layers: Second fitting parameter"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Modelica.Units.SI.Temperature T_ref = 298.15
    "Reference temperature"
    annotation (Dialog(tab="General", group="Parameters"));

  extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialTestPure(
    final p_adsorpt_der = 30,
    final T_adsorpt_der = 0,
    final p_adsorpt_start = 1,
    final T_adsorpt_start = 298.15,
    final M_adsorptive = 0.018,
    final no_coefficients = 4,
    redeclare final package IsothermModel =
      SorpLib.Media.Functions.SorptionEquilibria.PureComponents.GAB,
    final dp=1e-3,
    final dT=1e-3);

equation
  //
  // Coefficients of the isotherm model
  //
  c[1] = Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.psat(T=T_adsorpt);
  c[2] = x_mon_ref * exp(chi * (1 - T_adsorpt/T_ref));
  c[3] = exp(((C - exp(D*T_adsorpt)) - (H + I*T_adsorpt)) /
    (Modelica.Constants.R*T_adsorpt));
  c[4] = exp(((F + G*T_adsorpt) - (H + I*T_adsorpt)) /
    (Modelica.Constants.R*T_adsorpt));

  //
  // Partial derivative of coefficients of isotherm model w.r.t. temperature
  //
  dc_dT[1] = Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.dptofT(T=T_adsorpt);
  dc_dT[2] = -chi/T_ref * c[2];
  dc_dT[3] = -((D*T_adsorpt - 1) * exp(D*T_adsorpt) - H + C) /
    (Modelica.Constants.R*T_adsorpt^2) * c[3];
  dc_dT[4] = (H - F) / (Modelica.Constants.R*T_adsorpt^2) * c[4];

  //
  // Second-oder partial derivative of coefficients of isotherm model w.r.t. temperature
  //
  ddc_dT_dT[1] = (Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.dptofT(T=T_adsorpt+dT)
    - Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.dptofT(T=T_adsorpt-dT)) /
    (2*dT);
  ddc_dT_dT[2] = -chi/T_ref * dc_dT[2];
  ddc_dT_dT[3] = (((D^2 * T_adsorpt^2 - 2 * D * T_adsorpt + 1) *
    exp(2 * D * T_adsorpt) + (-D^2 * Modelica.Constants.R * T_adsorpt^3 +
    2 * D * Modelica.Constants.R * T_adsorpt^2 + (-2 * Modelica.Constants.R -
    2 * D * H + 2 * C * D) * T_adsorpt + 2 * H - 2 * C) * exp(D * T_adsorpt) +
    (2 * C - 2 * H) * Modelica.Constants.R * T_adsorpt + H^2 - 2 * C * H + C^2) *
    exp((-exp(D * T_adsorpt) - I * T_adsorpt - H + C) / (Modelica.Constants.R *
    T_adsorpt))) / (Modelica.Constants.R^2 * T_adsorpt^4);
  ddc_dT_dT[4] = -1 / (Modelica.Constants.R*T_adsorpt^2) *
    (2*Modelica.Constants.R*T_adsorpt - H + F) * dc_dT[2];

  //
  // Coefficients of isotherm model: T + dT K
  //
  c_pdT[1] = Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.psat(T=T_adsorpt+dT);
  c_pdT[2] = x_mon_ref * exp(chi * (1 - (T_adsorpt+dT)/T_ref));
  c_pdT[3] = exp(((C - exp(D*(T_adsorpt+dT))) - (H + I*(T_adsorpt+dT))) /
    (Modelica.Constants.R*(T_adsorpt+dT)));
  c_pdT[4] = exp(((F + G*(T_adsorpt+dT)) - (H + I*(T_adsorpt+dT))) /
    (Modelica.Constants.R*(T_adsorpt+dT)));

  //
  // Coefficients of isotherm model: T - dT K
  //
  c_mdT[1] = Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.psat(T=T_adsorpt-dT);
  c_mdT[2] = x_mon_ref * exp(chi * (1 - (T_adsorpt-dT)/T_ref));
  c_mdT[3] = exp(((C - exp(D*(T_adsorpt-dT))) - (H + I*(T_adsorpt-dT))) /
    (Modelica.Constants.R*(T_adsorpt-dT)));
  c_mdT[4] = exp(((F + G*(T_adsorpt-dT)) - (H + I*(T_adsorpt-dT))) /
    (Modelica.Constants.R*(T_adsorpt-dT)));

  //
  // Annotations
  //
  annotation (experiment(StartTime=0, StopTime=100, Tolerance=1e-6),
Documentation(revisions="<html>
<ul>
  <li>
  November 1, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>", info="<html>
<p>
This tester shows the behavior of all functions of the GAB isotherm models.
<br/><br/>
As an example, this tester increases the pressure with time. To see the behavior of
all functions, plot the variables <i>x_adsorpt</i>, <i>dx_adsorpt_dp_adsorpt</i>, 
<i>dx_adsorpt_dT_adsorpt</i>, <i>dx_adsorpt_dp_adsorpt_dT_adsorpt</i>, 
<i>dx_adsorpt_dp_adsorpt_dp_adsorpt</i>, <i>dx_adsorpt_dT_adsorpt_dT_adsorpt</i>, 
and <i>red_spreading_pressure</i> over the variable <i>p_adsorpt</i>. The simulation 
time is correctly preset (Start: 0 s, Stop = 100 s). 
</p>
</html>"));
end Test_changing_pressure;
