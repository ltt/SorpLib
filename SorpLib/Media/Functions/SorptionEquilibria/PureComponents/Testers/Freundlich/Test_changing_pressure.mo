within SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Testers.Freundlich;
model Test_changing_pressure
  "Tester for all functions of the Freundlich isotherm model: Changing pressure"

  //
  // Definition of parameters
  //
  parameter Real a0 = 0.01
    "First fitting parameter"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real a1(unit="1/K") = 1/273.15
    "Second fitting parameter"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real b0(unit="1") = 2
    "Third fitting parameter"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real b1(unit="K") = 50
    "Fourth fitting parameter"
    annotation (Dialog(tab="General", group="Parameters"));

  extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialTestPure(
    final p_adsorpt_der = 100,
    final T_adsorpt_der = 0,
    final p_adsorpt_start = 1,
    final T_adsorpt_start = 298.15,
    final M_adsorptive = 0.018,
    final no_coefficients = 2,
    redeclare final package IsothermModel =
      SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Freundlich,
    final dp=1e-3,
    final dT=1e-3);

equation
  //
  // Coefficients of the isotherm model
  //
  c[1] = a0*exp(-a1*T_adsorpt);
  c[2] = b0 + b1/T_adsorpt;

  //
  // Partial derivative of coefficients of isotherm model w.r.t. temperature
  //
  dc_dT[1] = -a1 * c[1];
  dc_dT[2] = -b1/T_adsorpt^2;

  //
  // Second-oder partial derivative of coefficients of isotherm model w.r.t. temperature
  //
  ddc_dT_dT[1] = -a1 * dc_dT[1];
  ddc_dT_dT[2] = -2 / T_adsorpt * dc_dT[2];

  //
  // Coefficients of isotherm model: T + dT K
  //
  c_pdT[1] = a0*exp(-a1*(T_adsorpt+dT));
  c_pdT[2] = b0 + b1/(T_adsorpt+dT);

  //
  // Coefficients of isotherm model: T - dT K
  //
  c_mdT[1] = a0*exp(-a1*(T_adsorpt-dT));
  c_mdT[2] = b0 + b1/(T_adsorpt-dT);

  //
  // Annotations
  //
  annotation (experiment(StartTime=0, StopTime=100, Tolerance=1e-6),
Documentation(revisions="<html>
<ul>
  <li>
  November 1, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>", info="<html>
<p>
This tester shows the behavior of all functions of the Freundlich isotherm models.
<br/><br/>
As an example, this tester increases the pressure with time. To see the behavior of
all functions, plot the variables <i>x_adsorpt</i>, <i>dx_adsorpt_dp_adsorpt</i>, 
<i>dx_adsorpt_dT_adsorpt</i>, <i>dx_adsorpt_dp_adsorpt_dT_adsorpt</i>, 
<i>dx_adsorpt_dp_adsorpt_dp_adsorpt</i>, <i>dx_adsorpt_dT_adsorpt_dT_adsorpt</i>, 
and <i>red_spreading_pressure</i> over the variable <i>p_adsorpt</i>. The simulation 
time is correctly preset (Start: 0 s, Stop = 100 s).
</p>
</html>"));
end Test_changing_pressure;
