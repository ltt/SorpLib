within SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Testers.BET;
model Test_changing_pressureTemperature
  "Tester for all functions of the BET isotherm model: Changing pressure and temperature"

  //
  // Definition of parameters
  //
  parameter SorpLib.Units.Uptake x_mon_ref = 0.35
    "Monolayer uptake at reference temperature"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real chi(unit="1") = 0.25
    "Parameter describing the change of the monolayer uptake with temperature"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real C_ref(unit="1/Pa") = 100
    "BET coefficient at reference temperature"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real Q(unit="J/mol") = 35e3
    "Parameter describing the change of the BET coefficient with temperature"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real Q_cap(unit="J/mol") = 12.420e3
    "Parameter describing extra enthalpy added due to capillary forces at high layers"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Integer n = 10
    "Number of layers that can be occupied"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Modelica.Units.SI.Temperature T_ref = 298.15
    "Reference temperature"
    annotation (Dialog(tab="General", group="Parameters"));

  extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialTestPure(
    final p_adsorpt_der = 300,
    final T_adsorpt_der = 80/100,
    final p_adsorpt_start = 1,
    final T_adsorpt_start = 298.15,
    final M_adsorptive = 0.018,
    final no_coefficients = 5,
    redeclare final package IsothermModel =
      SorpLib.Media.Functions.SorptionEquilibria.PureComponents.BET,
    final tolerance_p_inv = 1e-6,
    final tolerance_p_pi = 1e-6,
    final tolerance_pi = 1e-6,
    final dp=1e-3,
    final dT=1e-3);

equation
  //
  // Coefficients of the isotherm model
  //
  c[1] = Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.psat(T=T_adsorpt);
  c[2] = x_mon_ref * exp(chi * (1 - T_adsorpt/T_ref));
  c[3] = C_ref * exp(Q/Modelica.Constants.R/T_ref * (T_ref/T_adsorpt - 1));
  c[4] = n+0.1/273.15*T_adsorpt;
  c[5] = exp(Q_cap/(Modelica.Constants.R*T_adsorpt));

  //
  // Partial derivative of coefficients of isotherm model w.r.t. temperature
  //
  dc_dT[1] = Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.dptofT(T=T_adsorpt);
  dc_dT[2] = -chi/T_ref * c[2];
  dc_dT[3] = -Q/Modelica.Constants.R/T_adsorpt^2 * c[3];
  dc_dT[4] = 0.1/273.15;
  dc_dT[5] = -Q_cap/(Modelica.Constants.R*T_adsorpt^2)*c[5];

  //
  // Second-oder partial derivative of coefficients of isotherm model w.r.t. temperature
  //
  ddc_dT_dT[1] = (Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.dptofT(T=T_adsorpt+dT)
    - Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.dptofT(T=T_adsorpt-dT)) /
    (2*dT);
  ddc_dT_dT[2] = -chi/T_ref * dc_dT[2];
  ddc_dT_dT[3] = -1/Modelica.Constants.R/T_adsorpt^2 *
    (2*Modelica.Constants.R*T_adsorpt+Q) * dc_dT[3];
  ddc_dT_dT[4] = 0;
  ddc_dT_dT[5] = -1/(Modelica.Constants.R*T_adsorpt^2) *
    (2*Modelica.Constants.R*T_adsorpt+Q_cap) * dc_dT[5];

  //
  // Coefficients of isotherm model: T + dT K
  //
  c_pdT[1] = Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.psat(T=T_adsorpt+dT);
  c_pdT[2] = x_mon_ref * exp(chi * (1 - (T_adsorpt+dT)/T_ref));
  c_pdT[3] = C_ref * exp(Q/Modelica.Constants.R/T_ref * (T_ref/(T_adsorpt+dT) - 1));
  c_pdT[4] = n+0.1/273.15*(T_adsorpt+dT);
  c_pdT[5] = exp(Q_cap/(Modelica.Constants.R*(T_adsorpt+dT)));

  //
  // Coefficients of isotherm model: T - dT K
  //
  c_mdT[1] = Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.psat(T=T_adsorpt-dT);
  c_mdT[2] = x_mon_ref * exp(chi * (1 - (T_adsorpt-dT)/T_ref));
  c_mdT[3] = C_ref * exp(Q/Modelica.Constants.R/T_ref * (T_ref/(T_adsorpt-dT) - 1));
  c_mdT[4] = n+0.1/273.15*(T_adsorpt-dT);
  c_mdT[5] = exp(Q_cap/(Modelica.Constants.R*(T_adsorpt-dT)));

  //
  // Annotations
  //
  annotation (experiment(StartTime=0, StopTime=100, Tolerance=1e-6),
Documentation(revisions="<html>
<ul>
  <li>
  November 1, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>", info="<html>
<p>
This tester shows the behavior of all functions of the BET isotherm models.
<br/><br/>
As an example, this tester increases the pressure and temperature with time. To 
see the behavior of all functions, plot the variables <i>x_adsorpt</i>, 
<i>dx_adsorpt_dp_adsorpt</i>, <i>dx_adsorpt_dT_adsorpt</i>, 
<i>dx_adsorpt_dp_adsorpt_dT_adsorpt</i>, <i>dx_adsorpt_dp_adsorpt_dp_adsorpt</i>, 
<i>dx_adsorpt_dT_adsorpt_dT_adsorpt</i>, and <i>red_spreading_pressure</i> over 
the variable <i>p_adsorpt</i>. The simulation time is correctly preset (Start: 0 s, 
Stop = 100 s).
</p>
</html>"));
end Test_changing_pressureTemperature;
