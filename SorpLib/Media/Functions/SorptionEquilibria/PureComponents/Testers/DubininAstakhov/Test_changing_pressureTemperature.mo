within SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Testers.DubininAstakhov;
model Test_changing_pressureTemperature
  "Tester for all functions of the Dubinin-Astakhov isotherm model: Changing pressure and temperature"

  //
  // Definition of parameters
  //
  parameter SorpLib.Units.FilledPoreVolume W_0 = 4.32e-4
    "Maximum filled pore volume"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real E(unit="J/mol") = 2.364e3
    "Characteristic energy"
    annotation (Dialog(tab="General", group="Parameters"));
  parameter Real n(unit="1") = 2.283
    "Parameter describing the heterogenity of the system"
    annotation (Dialog(tab="General", group="Parameters"));

  extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialTestPureDubinin(
    final p_adsorpt_der = 300,
    final T_adsorpt_der = 80/100,
    final p_adsorpt_start = 400,
    final T_adsorpt_start = 298.15,
    final M_adsorptive = 0.018,
    final no_coefficients = 5,
    redeclare final package IsothermModel =
      SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininAstakhov,
    final p_lb_pi=Modelica.Constants.small,
    final tolerance_p_pi=1e-6,
    final tolerance_pi=100*Modelica.Constants.eps,
    final dp=1e-3,
    final dT=1e-3,
    final dA=1e-3);

equation
  //
  // Coefficients of the isotherm model
  //
  c[1] = Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.psat(T=T_adsorpt);
  c[2] = Modelica.Media.Water.IF97_Utilities.BaseIF97.Regions.rhol_T(T=T_adsorpt);
  c[3] = W_0+T_adsorpt/273.15*1e-4;
  c[4] = E+1*T_adsorpt;
  c[5] = n+T_adsorpt/273.15;

  //
  // Partial derivative of coefficients of isotherm model w.r.t. temperature
  //
  dc_dT[1] = Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.dptofT(T=T_adsorpt);
  dc_dT[2] = Modelica.Media.Water.IF97_Utilities.BaseIF97.Regions.drhol_dp(p=c[1]) *
    dc_dT[1];
  dc_dT[3] = 1e-4/273.15;
  dc_dT[4] = 1;
  dc_dT[5] = 1/273.15;

  //
  // Second-oder partial derivative of coefficients of isotherm model w.r.t. temperature
  //
  ddc_dT_dT[1] = (Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.dptofT(T=T_adsorpt+dT)
    - Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.dptofT(T=T_adsorpt-dT)) /
    (2*dT);
  ddc_dT_dT[2] = (Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.dptofT(T=T_adsorpt+dT) *
    Modelica.Media.Water.IF97_Utilities.BaseIF97.Regions.drhol_dp(p=c_pdT[1]) -
    Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.dptofT(T=T_adsorpt-dT) *
    Modelica.Media.Water.IF97_Utilities.BaseIF97.Regions.drhol_dp(p=c_mdT[1])) / (2*dT);
  ddc_dT_dT[3] = 0;
  ddc_dT_dT[4] = 0;
  ddc_dT_dT[5] = 0;

  //
  // Coefficients of isotherm model: T + dT K
  //
  c_pdT[1] = Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.psat(T=T_adsorpt+dT);
  c_pdT[2] = Modelica.Media.Water.IF97_Utilities.BaseIF97.Regions.rhol_T(T=T_adsorpt+dT);
  c_pdT[3] = W_0+(T_adsorpt+dT)/273.15*1e-4;
  c_pdT[4] = E+1*(T_adsorpt+dT);
  c_pdT[5] = n+(T_adsorpt+dT)/273.15;

  //
  // Coefficients of isotherm model: T - dT K
  //
  c_mdT[1] = Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.psat(T=T_adsorpt-dT);
  c_mdT[2] = Modelica.Media.Water.IF97_Utilities.BaseIF97.Regions.rhol_T(T=T_adsorpt-dT);
  c_mdT[3] = W_0+(T_adsorpt-dT)/273.15*1e-4;
  c_mdT[4] = E+1*(T_adsorpt-dT);
  c_mdT[5] = n+(T_adsorpt-dT)/273.15;

  //
  // Annotations
  //
  annotation (experiment(StartTime=0, StopTime=100, Tolerance=1e-6),
Documentation(revisions="<html>
<ul>
  <li>
  November 2, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>", info="<html>
<p>
This tester shows the behavior of all functions of the Dubinin-Astakhov isotherm models.
<br/><br/>
As an example, this tester increases the pressure and temperature with time. To 
see the behavior of all functions, plot the variables <i>x_adsorpt</i>, 
<i>dx_adsorpt_dp_adsorpt</i>, <i>dx_adsorpt_dT_adsorpt</i>, 
<i>dx_adsorpt_dp_adsorpt_dT_adsorpt</i>, <i>dx_adsorpt_dp_adsorpt_dp_adsorpt</i>, 
<i>dx_adsorpt_dT_adsorpt_dT_adsorpt</i>, and <i>red_spreading_pressure</i> over 
the variable <i>p_adsorpt</i>. The simulation time is correctly preset (Start: 0 s, 
Stop = 100 s).
</p>
</html>"));
end Test_changing_pressureTemperature;
