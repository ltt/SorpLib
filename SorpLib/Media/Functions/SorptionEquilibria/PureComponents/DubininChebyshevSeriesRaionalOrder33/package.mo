within SorpLib.Media.Functions.SorptionEquilibria.PureComponents;
package DubininChebyshevSeriesRaionalOrder33 "Package containing all functions regarding the Dubinin isotherm model using a 'Chebyshev series' equation type with rational order 3/3"
extends
  SorpLib.Media.Functions.SorptionEquilibria.Interfaces.PartialPureComponentsDubinin;

  redeclare final function extends x_pT
    "Dubinin-Chebyshev-Series-3/3 isotherm model: Uptake as function of pressure and temperature"
    //
    // Annotations
    //
    annotation (Inline=false,
  InlineAfterIndexReduction=false,
  LateInline=true,
  inverse(p_adsorpt=SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininChebyshevSeriesRaionalOrder33.p_xT(x_adsorpt, T_adsorpt, c, p_adsorpt_lb_start, p_adsorpt_ub_start, tolerance)));
  end x_pT;

  redeclare final function extends p_xT
    "Dubinin-Chebyshev-Series-3/3 isotherm model: Pressure as function of uptake and temperature"
    //
    // Annotations
    //
    annotation (Inline=false,
  InlineAfterIndexReduction=false,
  LateInline=true,
  inverse(x_adsorpt=SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininChebyshevSeriesRaionalOrder33.x_pT(p_adsorpt, T_adsorpt, c, p_adsorpt_lb_start, p_adsorpt_ub_start, tolerance)));
  end p_xT;

  redeclare final function extends dx_dT
    "Dubinin-Chebyshev-Series-3/3 isotherm model: Partial derivative of uptake w.r.t. temperature at constant pressure"

    //
    // Definition of variables
    //
protected
    SorpLib.Units.MolarAdsorptionPotential A=
      SorpLib.Media.Functions.SorptionEquilibria.Utilities.A_ppsT(
      p_adsorpt=p_adsorpt,
      p_sat=c[1],
      T_adsorpt=T_adsorpt)
      "Adsorption potential";
    SorpLib.Units.DerMolarAdsorptionPotentialByTemperature dA_dT_adsorpt=
      SorpLib.Media.Functions.SorptionEquilibria.Utilities.dA_dT(
      p_adsorpt=p_adsorpt,
      p_sat=c[1],
      T_adsorpt=T_adsorpt,
      dp_sat_dT_adsorpt=dc_dT_adsorpt[1])
      "Partial derivative of adsorption potential w.r.t. equilibrium temperature
      at constant pressure";

    Real n = (A - c[10]) / c[11]
      "Auxiliary variable";

    Real num = c[3] + c[5] * n + c[7] * (2 * n^2 - 1) + c[9] * (4 * n^3 - 3 * n)
      "Numerator of characteristic curve";
    Real den = 1 + c[4] * n + c[6] * (2 * n^2 - 1) + c[8] * (4 * n^3 - 3 * n)
      "Denominator of characteristic curve";

    Real dn_dT_adsorpt = (1 / c[11]) * dA_dT_adsorpt +
      (-1 / c[11]) * dc_dT_adsorpt[10] +
      ((c[10] - A) / c[11]^2) * dc_dT_adsorpt[11]
      "Partial derivative of auxiliary variable w.r.t. temprature";
    Real dnum_dT_adsorpt = (1) * dc_dT_adsorpt[3] +
      (n) * dc_dT_adsorpt[5] +
      (2 * n^2 - 1) * dc_dT_adsorpt[7] +
      (4 * n^3 - 3 * n) * dc_dT_adsorpt[9] +
      (4 * n * (3 * c[9] * n + c[7]) - 3 * c[9] + c[5]) * dn_dT_adsorpt
      "Partial derivative of numerator of characteristic curve w.r.t. temprature";
    Real dden_dT_adsorpt = (n) * dc_dT_adsorpt[4] +
      (2 * n^2 - 1) * dc_dT_adsorpt[6] +
      (4 * n^3 - 3 * n) * dc_dT_adsorpt[8] +
      (4 * n * (3 * c[8] * n + c[6]) - 3 * c[8] + c[4]) * dn_dT_adsorpt
      "Partial derivative of denominator of characteristic curve w.r.t. temprature";

  algorithm
    dx_adsorpt_dT_adsorpt :=
    (1 * num / den) * dc_dT_adsorpt[2] +
    (c[2] / den) * dnum_dT_adsorpt +
    (-c[2] * num / den^2) * dden_dT_adsorpt
      "Calculation of the partial derivative of the equilibrium uptake w.r.t. the
     equilibrium temperature at constant pressure";
  end dx_dT;

  redeclare final function extends ddx_dT_dT
    "Dubinin-Chebyshev-Series-3/3 isotherm model: Second-order partial derivative of uptake w.r.t. temperature at constant pressure"

    //
    // Definition of variables
    //
protected
    SorpLib.Units.MolarAdsorptionPotential A=
      SorpLib.Media.Functions.SorptionEquilibria.Utilities.A_ppsT(
      p_adsorpt=p_adsorpt,
      p_sat=c[1],
      T_adsorpt=T_adsorpt)
      "Adsorption potential";
    SorpLib.Units.DerMolarAdsorptionPotentialByTemperature dA_dT_adsorpt=
      SorpLib.Media.Functions.SorptionEquilibria.Utilities.dA_dT(
      p_adsorpt=p_adsorpt,
      p_sat=c[1],
      T_adsorpt=T_adsorpt,
      dp_sat_dT_adsorpt=dc_dT_adsorpt[1])
      "Partial derivative of adsorption potential w.r.t. equilibrium temperature
      at constant pressure";
    SorpLib.Units.DerMolarAdsorptionPotentialByTemperatureTemperature ddA_dT_adsorpt_dT_adsorpt=
      SorpLib.Media.Functions.SorptionEquilibria.Utilities.ddA_dT_dT(
      p_adsorpt=p_adsorpt,
      p_sat=c[1],
      T_adsorpt=T_adsorpt,
      dp_sat_dT_adsorpt=dc_dT_adsorpt[1],
      ddp_sat_dT_adsorpt_dT_adsorpt=ddc_dT_adsorpt_dT_adsorpt[1])
      "Second-order partial derivative of adsorption potential w.r.t. equilibrium 
      temperature at constant pressure";

    Real n = (A - c[10]) / c[11]
      "Auxiliary variable";
    Real num = c[3] + c[5] * n + c[7] * (2 * n^2 - 1) + c[9] * (4 * n^3 - 3 * n)
      "Numerator of characteristic curve";
    Real den = 1 + c[4] * n + c[6] * (2 * n^2 - 1) + c[8] * (4 * n^3 - 3 * n)
      "Denominator of characteristic curve";

    Real dn_dT_adsorpt = (1 / c[11]) * dA_dT_adsorpt +
      (-1 / c[11]) * dc_dT_adsorpt[10] +
      ((c[10] - A) / c[11]^2) * dc_dT_adsorpt[11]
      "Partial derivative of auxiliary variable w.r.t. temprature";
    Real dnum_dT_adsorpt = (1) * dc_dT_adsorpt[3] +
      (n) * dc_dT_adsorpt[5] +
      (2 * n^2 - 1) * dc_dT_adsorpt[7] +
      (4 * n^3 - 3 * n) * dc_dT_adsorpt[9] +
      (4 * n * (3 * c[9] * n + c[7]) - 3 * c[9] + c[5]) * dn_dT_adsorpt
      "Partial derivative of numerator of characteristic curve w.r.t. temprature";
    Real dden_dT_adsorpt = (n) * dc_dT_adsorpt[4] +
      (2 * n^2 - 1) * dc_dT_adsorpt[6] +
      (4 * n^3 - 3 * n) * dc_dT_adsorpt[8] +
      (4 * n * (3 * c[8] * n + c[6]) - 3 * c[8] + c[4]) * dn_dT_adsorpt
      "Partial derivative of denominator of characteristic curve w.r.t. temprature";

    Real ddn_dT_adsorpt_dT_adsorpt=
      ((-1 / c[11]^2 * dA_dT_adsorpt) * dc_dT_adsorpt[11] +
       (1 / c[11]) * ddA_dT_adsorpt_dT_adsorpt) +
      ((-1 / c[11]) * ddc_dT_adsorpt_dT_adsorpt[10] +
       (1 / c[11]^2 * dc_dT_adsorpt[10]) * dc_dT_adsorpt[11]) +
      ((1 / c[11]^2 * dc_dT_adsorpt[11]) * dc_dT_adsorpt[10] +
       (-2 * (c[10] - A) / c[11]^3 * dc_dT_adsorpt[11]) * dc_dT_adsorpt[11] +
       (-1 / c[11]^2 * dc_dT_adsorpt[11]) * dA_dT_adsorpt +
       ((c[10] - A) / c[11]^2) * ddc_dT_adsorpt_dT_adsorpt[11])
      "Second-order partial derivative of auxiliary variable w.r.t. temprature";
    Real ddnum_dT_adsorpt_dT_adsorpt=
      ((1) * ddc_dT_adsorpt_dT_adsorpt[3]) +
      ((dc_dT_adsorpt[5]) * dn_dT_adsorpt +
       (n) * ddc_dT_adsorpt_dT_adsorpt[5]) +
      ((4 * n * dc_dT_adsorpt[7]) * dn_dT_adsorpt +
       (2 * n^2 - 1) * ddc_dT_adsorpt_dT_adsorpt[7]) +
      (((12 * n^2 - 3) * dc_dT_adsorpt[9]) * dn_dT_adsorpt +
       (4 * n^3 - 3 * n) * ddc_dT_adsorpt_dT_adsorpt[9]) +
      ((4 * dn_dT_adsorpt * (6 * c[9] * n + c[7])) * dn_dT_adsorpt +
       (1 * dn_dT_adsorpt) * dc_dT_adsorpt[5] +
       (4 * n * dn_dT_adsorpt) * dc_dT_adsorpt[7] +
       ((12 * n^2 - 3) * dn_dT_adsorpt) * dc_dT_adsorpt[9] +
       (4 * n * (3 * c[9] * n + c[7]) - 3 * c[9] + c[5]) * ddn_dT_adsorpt_dT_adsorpt)
      "Second-order partial derivative of numerator of characteristic curve w.r.t. 
    temprature";
    Real ddden_dT_adsorpt_dT_adsorpt=
      ((dc_dT_adsorpt[4]) * dn_dT_adsorpt +
       (n) * ddc_dT_adsorpt_dT_adsorpt[4]) +
      ((4 * n * dc_dT_adsorpt[6]) * dn_dT_adsorpt +
       (2 * n^2 - 1) * ddc_dT_adsorpt_dT_adsorpt[6]) +
      (((12 * n^2 - 3) * dc_dT_adsorpt[8]) * dn_dT_adsorpt +
       (4 * n^3 - 3 * n) * ddc_dT_adsorpt_dT_adsorpt[8]) +
      (((4 * (6 * c[8] * n + c[6])) * dn_dT_adsorpt) * dn_dT_adsorpt +
       (1 * dn_dT_adsorpt) * dc_dT_adsorpt[4] +
       (4 * n * dn_dT_adsorpt) * dc_dT_adsorpt[6] +
       ((12 * n^2 - 3) * dn_dT_adsorpt) * dc_dT_adsorpt[8] +
       (4 * n * (3 * c[8] * n + c[6]) - 3 * c[8] + c[4]) * ddn_dT_adsorpt_dT_adsorpt)
      "Second-order partial derivative of denominator of characteristic curve w.r.t. 
    temprature";

  algorithm
    ddx_adsorpt_dT_adsorpt_dT_adsorpt :=
      ((1/den * dc_dT_adsorpt[2]) * dnum_dT_adsorpt +
       (-num / den^2 * dc_dT_adsorpt[2]) * dden_dT_adsorpt +
       (num / den) * ddc_dT_adsorpt_dT_adsorpt[2]) +
      ((1/den * dnum_dT_adsorpt) * dc_dT_adsorpt[2] +
       (-c[2] / den^2 * dnum_dT_adsorpt) * dden_dT_adsorpt +
       (c[2] / den) * ddnum_dT_adsorpt_dT_adsorpt) +
      ((-num / den^2 * dden_dT_adsorpt) * dc_dT_adsorpt[2] +
       (-c[2] / den^2 * dden_dT_adsorpt) * dnum_dT_adsorpt +
       (2 * c[2] * num / den^3 * dden_dT_adsorpt) * dden_dT_adsorpt +
       (-c[2] * num / den^2) * ddden_dT_adsorpt_dT_adsorpt)
      "Calculation of the second-order partial derivative of the equilibrium uptake 
    w.r.t. the equilibrium temperature at constant pressure";
  end ddx_dT_dT;

  redeclare final function extends ddx_dp_dT
    "Dubinin-Chebyshev-Series-3/3 isotherm model: Second-order partial derivative of uptake w.r.t. pressure and temperature"

    //
    // Definition of variables
    //
protected
    SorpLib.Units.MolarAdsorptionPotential A=
        SorpLib.Media.Functions.SorptionEquilibria.Utilities.A_ppsT(
        p_adsorpt=p_adsorpt,
        p_sat=c[1],
        T_adsorpt=T_adsorpt)
        "Adsorption potential";
    SorpLib.Units.DerMolarAdsorptionPotentialByPressure dA_dp_adsorpt=
        SorpLib.Media.Functions.SorptionEquilibria.Utilities.dA_dp(
        p_adsorpt=p_adsorpt,
        p_sat=c[1],
        T_adsorpt=T_adsorpt)
      "Partial derivative of adsorption potential w.r.t. to pressure at constant
      temperature";
    SorpLib.Units.DerMolarAdsorptionPotentialByPressureTemperature ddA_dp_adsorpt_dT_adsorpt=
        SorpLib.Media.Functions.SorptionEquilibria.Utilities.ddA_dp_dT(
        p_adsorpt=p_adsorpt,
        p_sat=c[1],
        T_adsorpt=T_adsorpt)
      "Second-order partial derivative of adsorption potential w.r.t. to pressure
    and temperature";

    SorpLib.Units.DerFilledPoreVolumeByAdsorptionPotential dW_dA=
        SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininChebyshevSeriesRaionalOrder33.dW_dA(
        A=A, c=c)
      "Partial derivative of filled pore volume w.r.t. adsorption potential at
      constant pressure and temperature";
    SorpLib.Units.DerFilledPoreVolumeByAdsorptionPotentialTemperature ddW_dA_dT_adsorpt=
        SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininChebyshevSeriesRaionalOrder33.ddW_dA_dT(
        p_adsorpt=p_adsorpt,
        T_adsorpt=T_adsorpt,
        A=A,
        c=c,
        dc_dT_adsorpt=dc_dT_adsorpt)
      "Second-order partial derivative of characteristic curve w.r.t. adsorption 
      potential and temperature at constant pressure";

  algorithm
    ddx_adsorpt_dp_adsorpt_dT_adsorpt :=
      dc_dT_adsorpt[2] * dW_dA * dA_dp_adsorpt +
      c[2] * ddW_dA_dT_adsorpt * dA_dp_adsorpt +
      c[2] * dW_dA * ddA_dp_adsorpt_dT_adsorpt
      "Calculation of the second-oder partial derivative of the equilibrium uptake 
     w.r.t. the equilibrium pressure and temperature";
  end ddx_dp_dT;

  redeclare function pi_pT
    "Dubinin-Chebyshev-Series-3/3 isotherm model: Reduced spreading pressure as function of pressure and temperature (numerical solution)"
    extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_pi_pT_num(
       integral_pi_lb = 1e-2,
       tolerance = 1e-6,
       redeclare final function func_x_pT =
       SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininChebyshevSeriesRaionalOrder33.x_pT);
  end pi_pT;

  redeclare function p_piT
    "Dubinin-Chebyshev-Series-3/3 isotherm model: Pressure as function of reduced spreading pressure and temperature (numerical solution)"
    extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_p_piT_num(
        p_adsorpt_lb_start = 1,
        integral_pi_lb = 1e-2,
        tolerance_p_adsorpt = 1e-6,
        tolerance_pi = 1e-6,
        redeclare final function func_pi_pT =
        SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininChebyshevSeriesRaionalOrder33.pi_pT);
  end p_piT;

  redeclare final function extends W_A
    "Dubinin-Chebyshev-Series-3/3 isotherm model: Filled pore volume as function of adsorption potential"

    //
    // Definition of variables
    //
protected
    Real n = (A - c[10]) / c[11]
      "Auxiliary variable";

    Real num = c[3] + c[5] * n + c[7] * (2 * n^2 - 1) + c[9] * (4 * n^3 - 3 * n)
      "Numerator of characteristic curve";
    Real den = 1 + c[4] * n + c[6] * (2 * n^2 - 1) + c[8] * (4 * n^3 - 3 * n)
      "Denominator of characteristic curve";

  algorithm
    W := num / den
      "Calculation of the equilibrium uptake of the adsorpt phase";

    //
    // Annotations
    //
    annotation (Inline=false,
  InlineAfterIndexReduction=false,
  LateInline=true,
  inverse(A=SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininChebyshevSeriesRaionalOrder33.A_W(W, c, A_lb_start, A_ub_start, tolerance)));
  end W_A;

  redeclare final function extends A_W
    "Dubinin-Chebyshev-Series-3/3 isotherm model: Adsorption potential as function of the filled pore volume (numerical solution)"
    extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_A_W_num(
        redeclare final function func_W_A =
        SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininChebyshevSeriesRaionalOrder33.W_A);

    //
    // Annotations
    //
    annotation (Inline=false,
  InlineAfterIndexReduction=false,
  LateInline=true,
  inverse(W=SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininChebyshevSeriesRaionalOrder33.W_A(A, c, A_lb_start, A_ub_start, tolerance)));
  end A_W;

  redeclare final function extends dW_dA
    "Dubinin-Chebyshev-Series-3/3 isotherm model: Partial derivative of filled pore volume w.r.t. adsorption potential at constant pressure and temperature"

    //
    // Definition of variables
    //
protected
    Real n = (A - c[10]) / c[11]
      "Auxiliary variable";

    Real num = c[3] + c[5] * n + c[7] * (2 * n^2 - 1) + c[9] * (4 * n^3 - 3 * n)
      "Numerator of characteristic curve";
    Real den = 1 + c[4] * n + c[6] * (2 * n^2 - 1) + c[8] * (4 * n^3 - 3 * n)
      "Denominator of characteristic curve";

    Real dn_dA = 1 / c[11]
      "Partial derivative of the auxiliary variable w.r.t. adsorption potential";

    Real dnum_dn = c[9] * (12 * n^2 - 3) + 4 * c[7] * n + c[5]
      "Partial derivative of the numerator w.r.t. auxiliary variable";
    Real dden_dn = c[8] * (12 * n^2 - 3) + 4 * c[6] * n + c[4]
      "Partial derivative of the numerator w.r.t. auxiliary variable";

    Real dW_dn = (dnum_dn * den - num * dden_dn) / den^2
      "Partial derivative of the equilibrium uptake w.r.t. auxiliary variable";

  algorithm
    dW_dA := dW_dn*dn_dA
      "Partial derivative of the filled pore volume w.r.t. adsorption potential at 
    constant pressure and temperature";
  end dW_dA;

  redeclare final function extends ddW_dA_dA
    "Dubinin-Chebyshev-Series-3/3: Second-order partial derivative of filled pore volume w.r.t. adsorption potential at constant pressure and temperature"

    //
    // Definition of variables
    //
protected
    Real n = (A - c[10]) / c[11]
      "Auxiliary variable";

    Real num = c[3] + c[5] * n + c[7] * (2 * n^2 - 1) + c[9] * (4 * n^3 - 3 * n)
      "Numerator of characteristic curve";
    Real den = 1 + c[4] * n + c[6] * (2 * n^2 - 1) + c[8] * (4 * n^3 - 3 * n)
      "Denominator of characteristic curve";

    Real dn_dA = 1 / c[11]
      "Partial derivative of the auxiliary variable w.r.t. adsorption potential";

    Real dnum_dn = c[9] * (12 * n^2 - 3) + 4 * c[7] * n + c[5]
      "Partial derivative of the numerator w.r.t. auxiliary variable";
    Real dden_dn = c[8] * (12 * n^2 - 3) + 4 * c[6] * n + c[4]
      "Partial derivative of the numerator w.r.t. auxiliary variable";

    Real dW_dn = (dnum_dn * den - num * dden_dn) / den^2
      "Partial derivative of the equilibrium uptake w.r.t. auxiliary variable";

    Real ddnum_dn_dn = 24 * c[9] * n + 4 * c[7]
      "Second-order partial derivative of the numerator w.r.t. auxiliary variable";
    Real ddden_dn_dn = 24 * c[8] * n + 4 * c[6]
      "Second-order partial derivative of the numerator w.r.t. auxiliary variable";

    Real ddW_dn_dn=
      (1 / den) * ddnum_dn_dn +
      (-dnum_dn / den^2) * dden_dn +
      (-dden_dn / den^2) * dnum_dn +
      (-num / den^2) * ddden_dn_dn +
      (2 * num * dden_dn / den^3) * dden_dn
      "Second-order partial derivative of the equilibrium uptake w.r.t. auxiliary 
    variable";

  algorithm
    ddW_dA_dA := (dn_dA) * (ddW_dn_dn*dn_dA)
      "Second-order partial derivative of filled pore volume w.r.t. adsorption 
    potential at constant pressure and temperature";
  end ddW_dA_dA;

  redeclare final function extends ddW_dA_dT
    "Dubinin-Chebyshev-Series-3/3: Second-order partial derivative of filled pore volume w.r.t. adsorption potential and temperature at constant pressure"

    //
    // Definition of variables
    //
protected
    SorpLib.Units.DerMolarAdsorptionPotentialByTemperature dA_dT_adsorpt=
      SorpLib.Media.Functions.SorptionEquilibria.Utilities.dA_dT(
      p_adsorpt=p_adsorpt,
      p_sat=c[1],
      T_adsorpt=T_adsorpt,
      dp_sat_dT_adsorpt=dc_dT_adsorpt[1])
      "Partial derivative of adsorption potential w.r.t. equilibrium temperature
      at constant pressure";

    Real n = (A - c[10]) / c[11]
      "Auxiliary variable";

    Real num = c[3] + c[5] * n + c[7] * (2 * n^2 - 1) + c[9] * (4 * n^3 - 3 * n)
      "Numerator of characteristic curve";
    Real den = 1 + c[4] * n + c[6] * (2 * n^2 - 1) + c[8] * (4 * n^3 - 3 * n)
      "Denominator of characteristic curve";

    Real dn_dA = 1 / c[11]
      "Partial derivative of the auxiliary variable w.r.t. adsorption potential";

    Real dnum_dn = c[9] * (12 * n^2 - 3) + 4 * c[7] * n + c[5]
      "Partial derivative of the numerator w.r.t. auxiliary variable";
    Real dden_dn = c[8] * (12 * n^2 - 3) + 4 * c[6] * n + c[4]
      "Partial derivative of the numerator w.r.t. auxiliary variable";

    Real dW_dn = (dnum_dn * den - num * dden_dn) / den^2
      "Partial derivative of the equilibrium uptake w.r.t. auxiliary variable";

    Real dn_dT_adsorpt = (1 / c[11]) * dA_dT_adsorpt +
      (-1 / c[11]) * dc_dT_adsorpt[10] +
      ((c[10] - A) / c[11]^2) * dc_dT_adsorpt[11]
      "Partial derivative of auxiliary variable w.r.t. temprature";
    Real dnum_dT_adsorpt = (1) * dc_dT_adsorpt[3] +
      (n) * dc_dT_adsorpt[5] +
      (2 * n^2 - 1) * dc_dT_adsorpt[7] +
      (4 * n^3 - 3 * n) * dc_dT_adsorpt[9] +
      (4 * n * (3 * c[9] * n + c[7]) - 3 * c[9] + c[5]) * dn_dT_adsorpt
      "Partial derivative of numerator of characteristic curve w.r.t. temprature";
    Real dden_dT_adsorpt = (n) * dc_dT_adsorpt[4] +
      (2 * n^2 - 1) * dc_dT_adsorpt[6] +
      (4 * n^3 - 3 * n) * dc_dT_adsorpt[8] +
      (4 * n * (3 * c[8] * n + c[6]) - 3 * c[8] + c[4]) * dn_dT_adsorpt
      "Partial derivative of denominator of characteristic curve w.r.t. temprature";

    Real ddn_dA_dT_adsorpt = (-1 / c[11]^2) * dc_dT_adsorpt[11]
      "Second-order partial derivative of the auxiliary variable w.r.t. adsorption
    potential and temperature at constant pressure";

    Real ddnum_dn_dT_adsorpt=
      (1) * dc_dT_adsorpt[5] +
      (4 * n) * dc_dT_adsorpt[7] +
      (12 * n^2 - 3) * dc_dT_adsorpt[9] +
      (24 * c[9] * n + 4 * c[7]) * dn_dT_adsorpt
      "Second-order partial derivative of numerator w.r.t. auxiliary variable and
    temperature";
    Real ddden_dn_dT_adsorpt=
      (1) * dc_dT_adsorpt[4] +
      (4 * n) * dc_dT_adsorpt[6] +
      (12 * n^2 - 3) * dc_dT_adsorpt[8] +
      (24 * c[8] * n + 4 * c[6]) * dn_dT_adsorpt
      "Second-order partial derivative of numerator w.r.t. auxiliary variable and
    temperature";

    Real ddW_dn_dT_adsorpt=
      ((1/den) * ddnum_dn_dT_adsorpt +
       (-1/den^2 * dnum_dn) * dden_dT_adsorpt) +
      ((-dden_dn/den^2) * dnum_dT_adsorpt +
       (-num/den^2) * ddden_dn_dT_adsorpt +
       (2 * num * dden_dn / den^3) * dden_dT_adsorpt)
      "Second-order partial derivative of the equilibrium uptake w.r.t. auxiliary 
    variable and temperature";

  algorithm
    ddW_dA_dT := (dn_dA) * ddW_dn_dT_adsorpt +
      (dW_dn) * ddn_dA_dT_adsorpt
      "Second-order partial derivative of filled pore volume w.r.t. adsorption 
    potential and temperature";
  end ddW_dA_dT;
  //
  // Annotation
  //
  annotation (Documentation(revisions="<html>
<ul>
  <li>
  November 2, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>", info="<html>
<p>
The Dubinin-Chebyshev-Series-3/3 isotherm model is a eleven-parameter model for calculating 
the equilibrium uptake <i>x_adsorpt</i> as a function of the equilibrium  pressure 
<i>p_adsorpt</i>.
</p>

<h4>Main equations</h4>
<p>
The Dubinin-Chebyshev-Series-3/3 isotherm model has the following form:
</p>
<pre>
    x<sub>adsorpt</sub> = &rho;<sub>adsorpt</sub>(T<sub>adsorpt</sub>) * W(A(T<sub>adsorpt</sub>));
</pre>
<p>
with
</p>
<pre>
    W(A(T<sub>adsorpt</sub>)) = (a + c * n + e * (2 * n<sup>2</sup> - 1) + g * (4 * n<sup>3</sup> - 3 * n)) / (1 + b * n + d * (2 * n<sup>2</sup> - 1) + f * (4 * n<sup>3</sup> - 3 * n));
</pre>
<pre>
    n(A(T<sub>adsorpt</sub>)) = (A - h) / i;
</pre>
<pre>
    A(T<sub>adsorpt</sub>) = R * T<sub>adsorpt</sub> * <strong>ln</strong>(p<sub>sat</sub>(T<sub>adsorpt</sub>) / p<sub>adsorpt</sub>);
</pre>
<p>
Herein, <i>W(A(T<sub>adsorpt</sub>))</i> is the so-called characteristic curve and
<i>A(T<sub>adsorpt</sub>)</i> is the adsorption potential. Within the characteristic 
curve, the parameters <i>a</i>, <i>b</i>, <i>c</i>, <i>d</i>, <i>e</i>, <i>f</i>,
<i>g</i>, <i>h</i>, and <i>i</i> are fitting parameters.
<br/><br/>
Note that the density of the adsorpt <i>&rho;<sub>adsorpt</sub>(T<sub>adsorpt</sub>)</i> 
is assumed to be the saturated liquid density &rho;<sub>sat,liq</sub>(T<sub>adsorpt</sub>) 
without any further information about the system under consideration. For super-critical
adsorptives (i.e., <i>T<sub>adsorpt</sub> &ge; T<sub>crit</sub></i>), the density of
the adsorpt <i>&rho;<sub>adsorpt</sub>(T<sub>adsorpt</sub>)</i> can be estimated by
</p>
<pre>
    &rho;<sub>adsorpt</sub>(T<sub>adsorpt</sub>) = &rho;<sub>sat,liq</sub>(T<sub>boiling,0</sub>) * <strong>exp</strong>(-0.0025 * (T<sub>adsorpt</sub> - T<sub>boiling,0</sub>));
</pre>
<p>
and a pseudo-vapour pressure <i>p<sub>sat</sub>(T<sub>adsorpt</sub>)</i> can be calculated by
</p>
<pre>
    p<sub>sat</sub>(T<sub>adsorpt</sub>) = p<sub>crit</sub>(T<sub>adsorpt</sub>) * (T<sub>adsorpt</sub> / T<sub>crit</sub>) ^ k;
</pre>
<p>
where <i>T<sub>boiling,0</sub></i> is the normal boiling point at 1 atm and <i>k</i> is
a fitting parameter specific to the system under consideration.
</p>

<h4>Required parameter order in function input c[:]:</h4>
<ul>
  <li>
  c[1] = p<sub>sat</sub>(T<sub>adsorpt</sub>) in Pa
  </li>
  <li>
  c[2] = &rho;<sub>adsorpt</sub>(T<sub>adsorpt</sub>) in kg/m<sup>3</sup>
  </li>
  <li>
  c[3] = a in m<sup>3</sup>/kg
  </li>
  <li>
  c[4] = b in -
  </li>
  <li>
  c[5] = c in m<sup>3</sup>/kg
  </li>
  <li>
  c[6] = d in -
  </li>
  <li>
  c[7] = e in m<sup>3</sup>/kg
  </li>
  <li>
  c[8] = f in -
  </li>
  <li>
  c[9] = g in m<sup>3</sup>/kg
  </li>
  <li>
  c[10] = h in J/mol
  </li>
  <li>
  c[11] = i in J/mol
  </li>
</ul>

<h4>Assumptions and limitations</h4>
<ul>
  <li>
  The characteristic curve <i>W(A)</i> must decrease strictly monotonically with 
  increasing adsorption potential <i>A</i>. Otherwise, the inverses <i>A(W)</i>
  and <i>p(x,T</i>) may not be solveable.
  </li>
  <li>
  The reduced spreading pressure <i>&pi;</i> may not be calculable. Accordingly, the 
  inverse <i>p(&pi;,T)</i> cannot be calculated either.
  </li>
</ul>

<h4>Example</h4>
<p>
The following figure shows the Dubinin-Chebyshev-Series-3/3 isotherm model for one 
parameter set. In the upper sub-figure, the equilibrium pressure changes with 
time, while the equilibrium temperature is constant. In the centered sub-figure, the 
equilibrium temperature changes with time, while the equilibrium pressure is constant. 
In the lower sub-figure, the characteristic curve is shown. 
<br/><br/>
</p>
<img src=\"Modelica://SorpLib/Resources/doc/media_functions_equilibria_pure_dubinin_chebyshev_series_3_3.png\" alt=\"media_functions_equilibria_pure_dubinin_chebyshev_series_3_3.png\">

<h4>References</h4>
<ul>
  <li>
  Do, D. D. (1998). Adsorption Analysis: Equilibria and Kinetics, 1st Edition, ISBN 978-1-86094-130-6, Imperial College Press.
  </li>
  <li>
  Schawe, D. (1999). Theoretical and Experimental Investigations of an Adsorption Heat Pump with Heat Transfer between two Adsorbers, PhD Thesis, Stuttgart.
  </li>
</ul>
</html>"));
end DubininChebyshevSeriesRaionalOrder33;
