within SorpLib.Media.Functions.SorptionEquilibria.PureComponents;
package Freundlich "Package containing all functions regarding the Freundlich isotherm"
extends
  SorpLib.Media.Functions.SorptionEquilibria.Interfaces.PartialPureComponents;

  redeclare final function extends x_pT
    "Freundlich isotherm model: Uptake as function of pressure and temperature"
  algorithm
    x_adsorpt := c[1] * p_adsorpt^(1/c[2])
      "Calculation of the equilibrium uptake of the adsorpt phase";

    //
    // Annotations
    //
    annotation (Inline=false,
  InlineAfterIndexReduction=false,
  LateInline=true,
  inverse(p_adsorpt=SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Freundlich.p_xT(x_adsorpt, T_adsorpt, c, p_adsorpt_lb_start, p_adsorpt_ub_start, tolerance)));
  end x_pT;

  redeclare final function extends p_xT
    "Freundlich isotherm model: Pressure as function of uptake and temperature"
  algorithm
    p_adsorpt := (x_adsorpt/c[1])^c[2]
      "Calculation of the equilibrium pressure of the adsorpt phase";

    //
    // Annotations
    //
    annotation (Inline=false,
  InlineAfterIndexReduction=false,
  LateInline=true,
  inverse(x_adsorpt=SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Freundlich.x_pT(p_adsorpt, T_adsorpt, c, p_adsorpt_lb_start, p_adsorpt_ub_start, tolerance)));
  end p_xT;

  redeclare final function extends dx_dp
    "Freundlich isotherm model: Partial derivative of uptake w.r.t. pressure at constant temperature"
  algorithm
    dx_adsorpt_dp_adsorpt := c[1]/c[2] * p_adsorpt^(1/c[2] - 1)
      "Calculation of the partial derivative of the equilibrium uptake w.r.t. the
     equilibrium pressure at constant temperature";
  end dx_dp;

  redeclare final function extends dx_dT
    "Freundlich isotherm model: Partial derivative of uptake w.r.t. temperature at constant pressure"

    //
    // Definition of variables
    //
protected
    Real dx_adsorpt_dc1 = p_adsorpt ^ (1/c[2])
      "Derivative of uptake w.r.t. to first coefficient of Freundlich isotherm";
    Real dx_adsorpt_dc2 = -c[1] / c[2]^2 * p_adsorpt ^ (1/c[2]) * log(p_adsorpt)
      "Derivative of uptake w.r.t. to second coefficient of Freundlich isotherm";

  algorithm
    dx_adsorpt_dT_adsorpt :=
      dx_adsorpt_dc1*dc_dT_adsorpt[1] +
      dx_adsorpt_dc2*dc_dT_adsorpt[2]
      "Calculation of the partial derivative of the equilibrium uptake w.r.t. the
     equilibrium temperature at constant pressure";
  end dx_dT;

  redeclare final function extends ddx_dp_dp
    "Freundlich isotherm model: Second-order partial derivative of uptake w.r.t. pressure at constant temperature"
  algorithm
    ddx_adsorpt_dp_adsorpt_dp_adsorpt := (c[1] * (1 / c[2] - 1) *
      p_adsorpt^(1 / c[2] - 2)) / c[2]
      "Calculation of the second-order partial derivative of the equilibrium uptake 
    w.r.t. the equilibrium pressure at constant temperature";
  end ddx_dp_dp;

  redeclare final function extends ddx_dT_dT
    "Freundlich isotherm model: Second-order partial derivative of uptake w.r.t. temperature at constant pressure"

    //
    // Definition of variables
    //
protected
    Real dx_adsorpt_dc1 = p_adsorpt ^ (1/c[2])
      "Derivative of uptake w.r.t. to first coefficient of Freundlich isotherm";
    Real dx_adsorpt_dc2 = -c[1] / c[2]^2 * p_adsorpt ^ (1/c[2]) * log(p_adsorpt)
      "Derivative of uptake w.r.t. to second coefficient of Freundlich isotherm";

    Real ddx_adsorpt_dc1_dc2 = -1 / c[2]^2 * p_adsorpt ^ (1/c[2]) * log(p_adsorpt)
      "Second-order partial derivative of uptake w.r.t. to first and second 
    coefficient of Freundlich isotherm";

    Real ddx_adsorpt_dc2_dc2 = c[1] / c[2]^4 * p_adsorpt ^ (1/c[2]) *
      log(p_adsorpt) * (2*c[2] + log(p_adsorpt))
      "Second-order partial derivative of uptake w.r.t. to second coefficient of 
    Freundlich isotherm";

    Real ddx_adsorpt_dc1_dT_adsorpt = ddx_adsorpt_dc1_dc2*dc_dT_adsorpt[2]
      "Second-order partial derivative of uptake w.r.t. to first coefficient of 
    Freundlich isotherm and temperature";
    Real ddx_adsorpt_dc2_dT_adsorpt = ddx_adsorpt_dc1_dc2*dc_dT_adsorpt[1] +
      ddx_adsorpt_dc2_dc2*dc_dT_adsorpt[2]
      "Second-order partial derivative of uptake w.r.t. to second coefficient of 
    Freundlich isotherm and temperature";

  algorithm
    ddx_adsorpt_dT_adsorpt_dT_adsorpt :=
      (ddx_adsorpt_dc1_dT_adsorpt*dc_dT_adsorpt[1] +
       dx_adsorpt_dc1*ddc_dT_adsorpt_dT_adsorpt[1]) +
      (ddx_adsorpt_dc2_dT_adsorpt*dc_dT_adsorpt[2] +
       dx_adsorpt_dc2*ddc_dT_adsorpt_dT_adsorpt[2])
      "Calculation of the second-order partial derivative of the equilibrium uptake 
    w.r.t. the equilibrium temperature at constant pressure";
  end ddx_dT_dT;

  redeclare final function extends ddx_dp_dT
    "Freundlich isotherm model: Second-order partial derivative of uptake w.r.t. pressure and temperature"

    //
    // Definition of variables
    //
protected
    Real ddx_adsorpt_dp_adsorpt_dc1 = 1 / c[2] * p_adsorpt ^ (1/c[2] - 1)
      "Second-order partial derivative of uptake w.r.t. to equilibrium pressure and
    first coefficient of Freundlich isotherm";
    Real ddx_adsorpt_dp_adsorpt_dc2 = -c[1] / c[2]^3 * p_adsorpt ^ (1/c[2] - 1) *
      (log(p_adsorpt) + c[2])
      "Second-order partial derivative of uptake w.r.t. to equilibrium pressure and
    second coefficient of Freundlich isotherm";

  algorithm
    ddx_adsorpt_dp_adsorpt_dT_adsorpt :=
      ddx_adsorpt_dp_adsorpt_dc1*dc_dT_adsorpt[1] +
      ddx_adsorpt_dp_adsorpt_dc2*dc_dT_adsorpt[2]
      "Calculation of the second-oder partial derivative of the equilibrium uptake 
     w.r.t. the equilibrium pressure and temperature";
  end ddx_dp_dT;

  redeclare final function extends pi_pT
    "Freundlich isotherm model: Reduced spreading pressure as function of pressure and temperature"
  algorithm
    pi := 1/M_adsorptive * (c[1]*c[2] * p_adsorpt^(1/c[2]))
      "Calculation of the reduced spreading pressure";
  end pi_pT;

  redeclare final function extends p_piT
    "Freundlich isotherm model: Pressure as function of reduced spreading pressure and temperature"
  algorithm
    p_adsorpt := (M_adsorptive * pi / (c[1]*c[2])) ^ c[2]
      "Calculation of the equilibrium pressure";
  end p_piT;
  //
  // Annotations
  //
annotation (Documentation(revisions="<html>
<ul>
  <li>
  November 1, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>", info="<html>
<p>
The Freundlich isotherm model is a two-parameter model for calculating the  equilibrium
uptake <i>x_adsorpt</i> as a function of the equilibrium pressure <i>p_adsorpt</i>.
The Freundlich isotherm model is suitable for type III isotherms according to the 
IUPAC definition.
</p>

<h4>Main equations</h4>
<p>
The Freundlich isotherm model has the following form:
</p>
<pre>
    x<sub>adsorpt</sub> = K(T<sub>adsorpt</sub>) * p<sub>adsorpt</sub> ^ (1/n(T<sub>adsorpt</sub>));
</pre>
<p>
where <i>K(T<sub>adsorpt</sub>)</i> is the Freundlich coefficient and 
<i>n(T<sub>adsorpt</sub>)</i> is the Freundlich exponent. Typical temperature
dependencies may have the following forms:
</p>
</p>
<pre>
    K(T<sub>adsorpt</sub>) = a<sub>0</sub> * <strong>exp</strong>(-a<sub>1</sub> * T<sub>adsorpt</sub>));
</pre>
<pre>
    n(T<sub>adsorpt</sub>) = b<sub>0</sub> + b<sub>1</sub>/T<sub>adsorpt</sub>;
</pre>
<p>
where <i>a<sub>0</sub></i>, <i>a<sub>1</sub></i>, <i>b<sub>0</sub></i>, and 
<i>b<sub>1</sub></i> are fiiting parameters.
<br/><br/>
Note that the Freundlich exponent <i>n(T<sub>adsorpt</sub>)</i> is typically 
greater than unity.
</p>

<h4>Required parameter order in function input c[:]:</h4>
<ul>
  <li>
  c[1] = K(T<sub>adsorpt</sub>) in 1/Pa^(1/n(T<sub>adsorpt</sub>))
  </li>
  <li>
  c[2] = n(T<sub>adsorpt</sub>) in -
  </li>
</ul>

<h4>Assumptions and limitations</h4>
<ul>
  <li>
  No proper Henry law behavior.
  </li>
  <li>
  No finite limit when pressure is sufficiently high.
  </li>
</ul>
<p>
Hence, the Freundlich isotherm is generally valid narrow the range of adsorption
data used for fitting.
</p>

<h4>Typical use</h4>
<p>
The isotherm model is used for type III isotherms according to the IUPAC definition.
</p>

<h4>Example</h4>
<p>
The following figure shows the Freundlich isotherm model for different parameter sets.
In the upper sub-figure, the equilibrium pressure changes with time, while the 
equilibrium temperature remains constant; in the lower sub-figure, the equilibrium 
temperature changes with time, while the equilibrium pressure remains constant. 
<br/><br/>
</p>
<img src=\"Modelica://SorpLib/Resources/doc/media_functions_equilibria_pure_freundlich.png\" alt=\"media_functions_equilibria_pure_freundlich.png\">

<h4>References</h4>
<ul>
  <li>
  Bathen, D. and Breitbach, M. (2001). Adsorptionstechnik (in German), 1st Edition, ISBN 3-540-41908-X, Springer-Verlag Berlin Heidelberg New York.
  </li>
  <li>
  Do, D. D. (1998). Adsorption Analysis: Equilibria and Kinetics, 1st Edition, ISBN 978-1-86094-130-6, Imperial College Press.
  </li>
</ul>
</html>"));
end Freundlich;
