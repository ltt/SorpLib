within SorpLib.Media.Functions.SorptionEquilibria;
package PureComponents "Functions required to calculate sorption equlibria for pure components"
extends Modelica.Icons.VariantsPackage;

annotation (Documentation(info="<html>
<p>
This package contains isotherm models for pure component adsorption. Each isotherm 
model is stored as a separate package. There are also test models for each isotherm 
model, which test all functions of the isotherm models regarding their implementation. 
The isotherm models already implemented can be found in the package content. 
</p>

<h4>Implemented functions for each isotherm model</h4>
<p>
The following functions are provided for each isotherm model:
</p>
<ul>
  <li>
  Equilibrium uptake as function of equilibrium pressure and eqiulibrium temperature.
  </li>
  <li>
  Equilibrium pressure as function of equilibrium uptake and eqiulibrium temperature.
  </li>
  <li>
  Partial derivative of equilibrium uptake w.r.t. equilibrium pressure at constant
  pressure.
  </li>
  <li>
  Partial derivative of equilibrium uptake w.r.t. equilibrium temperature at constant
  pressure.
  </li>
  <li>
  Second-order partial derivative of equilibrium uptake w.r.t. equilibrium pressure at
  constant temperature.
  </li>
  <li>
  Second-order partial derivative of equilibrium uptake w.r.t. equilibrium temperature
  at constant pressure.
  </li>
  <li>
  Second-order partial derivative of equilibrium uptake w.r.t. equilibrium pressure and
  temperature.
  </li>
  <li>
  Reduced spreading pressure as function of equilibrium pressure and eqiulibrium 
  temperature.
  </li>
  <li>
  Equilibrium pressure as function of reduced spreading pressure and eqiulibrium 
  temperature.
  </li>
</ul>
<p>
The following additional functions are provided for isotherm models based on the model
of Dubinin:
</p>
<ul>
  <li>
  Filled pore volume as function of adsorption potential.
  </li>
  <li>
  Adsorption potential as function of filled pore volume.
  </li>
  <li>
  Partial derivative of filled pore volume w.r.t. adsorption potential at constant pressure
  and temperature.
  </li>
  <li>
  Second-order partial derivative of filled pore volume w.r.t. adsorption potential at
  constant pressure and temperature.
  </li>
  <li>
  Second-order partial derivative of filled pore volume w.r.t. adsorption potential and
  temperature at constant pressure.
  </li>
</ul>

<h4>How to add new isotherm models</h4>
<p>
To add a new isotherm model, duplicate the package of a similar isotherm model that is 
already implemented. Then, customise all functions of the isotherm model. If new 
functions are implemented (e.g., equilibrium temperature as function of equilibrium 
pressure and uptake), add these for all existing isotherm models as well. Then, adapt 
the test models and check the functions of the new isotherm model regarding their 
implementation. Finally, write the documentation for each function, model, and package.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end PureComponents;
