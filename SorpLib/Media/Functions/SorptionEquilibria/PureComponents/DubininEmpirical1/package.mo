within SorpLib.Media.Functions.SorptionEquilibria.PureComponents;
package DubininEmpirical1 "Package containing all functions regarding the Dubinin-Empirical-1 isotherm model"
extends
  SorpLib.Media.Functions.SorptionEquilibria.Interfaces.PartialPureComponentsDubinin;

  redeclare final function extends x_pT
    "Dubinin-Empirical-1 isotherm model: Uptake as function of pressure and temperature"
    //
    // Annotations
    //
    annotation (Inline=false,
  InlineAfterIndexReduction=false,
  LateInline=true,
  inverse(p_adsorpt=SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininEmpirical1.p_xT(x_adsorpt, T_adsorpt, c, p_adsorpt_lb_start, p_adsorpt_ub_start, tolerance)));
  end x_pT;

  redeclare final function extends p_xT
    "Dubinin-Empirical-1 isotherm model: Pressure as function of uptake and temperature"
    //
    // Annotations
    //
    annotation (Inline=false,
  InlineAfterIndexReduction=false,
  LateInline=true,
  inverse(x_adsorpt=SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininEmpirical1.x_pT(p_adsorpt, T_adsorpt, c, p_adsorpt_lb_start, p_adsorpt_ub_start, tolerance)));
  end p_xT;

  redeclare final function extends dx_dT
    "Dubinin-Empirical-1 isotherm model: Partial derivative of uptake w.r.t. temperature at constant pressure"

    //
    // Definition of variables
    //
protected
    SorpLib.Units.MolarAdsorptionPotential A=
      SorpLib.Media.Functions.SorptionEquilibria.Utilities.A_ppsT(
      p_adsorpt=p_adsorpt,
      p_sat=c[1],
      T_adsorpt=T_adsorpt)
      "Adsorption potential";
    SorpLib.Units.DerMolarAdsorptionPotentialByTemperature dA_dT_adsorpt=
      SorpLib.Media.Functions.SorptionEquilibria.Utilities.dA_dT(
      p_adsorpt=p_adsorpt,
      p_sat=c[1],
      T_adsorpt=T_adsorpt,
      dp_sat_dT_adsorpt=dc_dT_adsorpt[1])
      "Partial derivative of adsorption potential w.r.t. equilibrium temperature
      at constant pressure";

    Real den = (c[3] + c[4] * A^2 * log(c[5] * A) + c[6] * A^3)
      "Denominator of characteristic curve";

    Real dden_dc3 = 1
      "Partial derivative of denominator of characteristic curve w.r.t. third
    coefficient of isotherm";
    Real dden_dc4 = A^2 * log(c[5] * A)
      "Partial derivative of denominator of characteristic curve w.r.t. fourth
    coefficient of isotherm";
    Real dden_dc5 = c[4] * A^2 / c[5]
      "Partial derivative of denominator of characteristic curve w.r.t. fivth
    coefficient of isotherm";
    Real dden_dc6 = A^3
      "Partial derivative of denominator of characteristic curve w.r.t. sixth
    coefficient of isotherm";
    Real dden_dA = A * (2 * c[4] * log(c[5] * A) + 3 * c[6] * A + c[4])
      "Partial derivative of denominator of characteristic curve w.r.t. adsorption
    potential";

    Real dden_dT_adsorpt = dden_dc3 * dc_dT_adsorpt[3] +
      dden_dc4 * dc_dT_adsorpt[4] +
      dden_dc5 * dc_dT_adsorpt[5] +
      dden_dc6 * dc_dT_adsorpt[6] +
      dden_dA * dA_dT_adsorpt
      "Partial derivative of denominator of characteristic curve w.r.t. temprature";

  algorithm
    dx_adsorpt_dT_adsorpt := (1 / den) * dc_dT_adsorpt[2] +
      (-c[2]/den^2) * dden_dT_adsorpt
      "Calculation of the partial derivative of the equilibrium uptake w.r.t. the
     equilibrium temperature at constant pressure";
  end dx_dT;

  redeclare final function extends ddx_dT_dT
    "Dubinin-Empirical-1 isotherm model: Second-order partial derivative of uptake w.r.t. temperature at constant pressure"

    //
    // Definition of variables
    //
protected
    SorpLib.Units.MolarAdsorptionPotential A=
      SorpLib.Media.Functions.SorptionEquilibria.Utilities.A_ppsT(
      p_adsorpt=p_adsorpt,
      p_sat=c[1],
      T_adsorpt=T_adsorpt)
      "Adsorption potential";
    SorpLib.Units.DerMolarAdsorptionPotentialByTemperature dA_dT_adsorpt=
      SorpLib.Media.Functions.SorptionEquilibria.Utilities.dA_dT(
      p_adsorpt=p_adsorpt,
      p_sat=c[1],
      T_adsorpt=T_adsorpt,
      dp_sat_dT_adsorpt=dc_dT_adsorpt[1])
      "Partial derivative of adsorption potential w.r.t. equilibrium temperature
      at constant pressure";
    SorpLib.Units.DerMolarAdsorptionPotentialByTemperatureTemperature ddA_dT_adsorpt_dT_adsorpt=
      SorpLib.Media.Functions.SorptionEquilibria.Utilities.ddA_dT_dT(
      p_adsorpt=p_adsorpt,
      p_sat=c[1],
      T_adsorpt=T_adsorpt,
      dp_sat_dT_adsorpt=dc_dT_adsorpt[1],
      ddp_sat_dT_adsorpt_dT_adsorpt=ddc_dT_adsorpt_dT_adsorpt[1])
      "Second-order partial derivative of adsorption potential w.r.t. equilibrium 
      temperature at constant pressure";

    Real den = (c[3] + c[4] * A^2 * log(c[5] * A) + c[6] * A^3)
      "Denominator of characteristic curve";

    Real dden_dc3 = 1
      "Partial derivative of denominator of characteristic curve w.r.t. third
    coefficient of isotherm";
    Real dden_dc4 = A^2 * log(c[5] * A)
      "Partial derivative of denominator of characteristic curve w.r.t. fourth
    coefficient of isotherm";
    Real dden_dc5 = c[4] * A^2 / c[5]
      "Partial derivative of denominator of characteristic curve w.r.t. fivth
    coefficient of isotherm";
    Real dden_dc6 = A^3
      "Partial derivative of denominator of characteristic curve w.r.t. sixth
    coefficient of isotherm";
    Real dden_dA = A * (2 * c[4] * log(c[5] * A) + 3 * c[6] * A + c[4])
      "Partial derivative of denominator of characteristic curve w.r.t. adsorption
    potential";

    Real dden_dT_adsorpt = dden_dc3 * dc_dT_adsorpt[3] +
      dden_dc4 * dc_dT_adsorpt[4] +
      dden_dc5 * dc_dT_adsorpt[5] +
      dden_dc6 * dc_dT_adsorpt[6] +
      dden_dA * dA_dT_adsorpt
      "Partial derivative of denominator of characteristic curve w.r.t. temprature";

    Real ddden_dc4_dT_adsorpt = (A^2 / c[5]) * dc_dT_adsorpt[5] +
      (2 * A * log(c[5] * A) + A) * dA_dT_adsorpt
      "Second-order partial darivative of denominator of characteristic curve w.r.t.
    fourth coefficient of isotherm and temperature";
    Real ddden_dc5_dT_adsorpt = (A^2 / c[5]) * dc_dT_adsorpt[4] +
      (-c[4] * A^2 / c[5]^2) * dc_dT_adsorpt[5] +
      (2 * c[4] * A / c[5]) * dA_dT_adsorpt
      "Second-order partial darivative of denominator of characteristic curve w.r.t.
    fivth coefficient of isotherm and temperature";
    Real ddden_dc6_dT_adsorpt = 3 * A^2 * dA_dT_adsorpt
      "Second-order partial darivative of denominator of characteristic curve w.r.t.
    sixth coefficient of isotherm and temperature";
    Real ddden_dA_dT_adsorpt = (A * (2 * log(A * c[5]) + 1)) * dc_dT_adsorpt[4] +
      ((2 * A * c[4]) / c[5]) * dc_dT_adsorpt[5] +
      (3 * A^2) * dc_dT_adsorpt[6] +
      (2 * c[4] * log(c[5] * A) + 6 * c[6] * A + 3 * c[4]) * dA_dT_adsorpt
      "Second-order partial darivative of denominator of characteristic curve w.r.t.
    adsorption potential and temperature";

    Real ddden_dT_adsorpt_dT_adsorpt=
      ((dden_dc3) * ddc_dT_adsorpt_dT_adsorpt[3]) +
      ((dc_dT_adsorpt[4]) * ddden_dc4_dT_adsorpt +
       (dden_dc4) * ddc_dT_adsorpt_dT_adsorpt[4]) +
      ((dc_dT_adsorpt[5]) * ddden_dc5_dT_adsorpt +
       (dden_dc5) * ddc_dT_adsorpt_dT_adsorpt[5]) +
      ((dc_dT_adsorpt[6]) * ddden_dc6_dT_adsorpt +
       (dden_dc6) * ddc_dT_adsorpt_dT_adsorpt[6]) +
      ((dA_dT_adsorpt) * ddden_dA_dT_adsorpt +
       (dden_dA) * ddA_dT_adsorpt_dT_adsorpt)
      "Second-order partial derivative of denominator of characteristic curve w.r.t. 
    temprature";

  algorithm
    ddx_adsorpt_dT_adsorpt_dT_adsorpt :=
      ((-1/den^2 * dc_dT_adsorpt[2]) * dden_dT_adsorpt +
       (1 / den) * ddc_dT_adsorpt_dT_adsorpt[2]) +
      ((-1/den^2 * dden_dT_adsorpt) * dc_dT_adsorpt[2] +
       (2 * c[2]/den^3 * dden_dT_adsorpt) * dden_dT_adsorpt +
       (-c[2]/den^2) * ddden_dT_adsorpt_dT_adsorpt)
      "Calculation of the second-order partial derivative of the equilibrium uptake 
    w.r.t. the equilibrium temperature at constant pressure";
  end ddx_dT_dT;

  redeclare final function extends ddx_dp_dT
    "Dubinin-Empirical-1 isotherm model: Second-order partial derivative of uptake w.r.t. pressure and temperature"

    //
    // Definition of variables
    //
protected
    SorpLib.Units.MolarAdsorptionPotential A=
        SorpLib.Media.Functions.SorptionEquilibria.Utilities.A_ppsT(
        p_adsorpt=p_adsorpt,
        p_sat=c[1],
        T_adsorpt=T_adsorpt)
        "Adsorption potential";
    SorpLib.Units.DerMolarAdsorptionPotentialByPressure dA_dp_adsorpt=
        SorpLib.Media.Functions.SorptionEquilibria.Utilities.dA_dp(
        p_adsorpt=p_adsorpt,
        p_sat=c[1],
        T_adsorpt=T_adsorpt)
      "Partial derivative of adsorption potential w.r.t. to pressure at constant
      temperature";
    SorpLib.Units.DerMolarAdsorptionPotentialByPressureTemperature ddA_dp_adsorpt_dT_adsorpt=
        SorpLib.Media.Functions.SorptionEquilibria.Utilities.ddA_dp_dT(
        p_adsorpt=p_adsorpt,
        p_sat=c[1],
        T_adsorpt=T_adsorpt)
      "Second-order partial derivative of adsorption potential w.r.t. to pressure
      and temperature";

    SorpLib.Units.DerFilledPoreVolumeByAdsorptionPotential dW_dA=
        SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininEmpirical1.dW_dA(
        A=A, c=c)
      "Partial derivative of filled pore volume w.r.t. adsorption potential at
      constant pressure and temperature";
    SorpLib.Units.DerFilledPoreVolumeByAdsorptionPotentialTemperature ddW_dA_dT_adsorpt=
        SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininEmpirical1.ddW_dA_dT(
        p_adsorpt=p_adsorpt,
        T_adsorpt=T_adsorpt,
        A=A,
        c=c,
        dc_dT_adsorpt=dc_dT_adsorpt)
      "Second-order partial derivative of characteristic curve w.r.t. adsorption 
      potential and temperature at constant pressure";

  algorithm
    ddx_adsorpt_dp_adsorpt_dT_adsorpt :=
      dc_dT_adsorpt[2] * dW_dA * dA_dp_adsorpt +
      c[2] * ddW_dA_dT_adsorpt * dA_dp_adsorpt +
      c[2] * dW_dA * ddA_dp_adsorpt_dT_adsorpt
      "Calculation of the second-oder partial derivative of the equilibrium uptake 
     w.r.t. the equilibrium pressure and temperature";
  end ddx_dp_dT;

  redeclare function pi_pT
    "Dubinin-Empirical-1 isotherm model: Reduced spreading pressure as function of pressure and temperature (numerical solution)"
    extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_pi_pT_num(
       integral_pi_lb = 1e-12,
       tolerance = 100*Modelica.Constants.eps,
       redeclare final function func_x_pT =
       SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininEmpirical1.x_pT);
  end pi_pT;

  redeclare function p_piT
    "Dubinin-Empirical-1 isotherm model: Pressure as function of reduced spreading pressure and temperature (numerical solution)"
    extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_p_piT_num(
       p_adsorpt_lb_start = 1,
       integral_pi_lb = 1e-12,
       tolerance_p_adsorpt = 1e-6,
       tolerance_pi = 100*Modelica.Constants.eps,
       redeclare final function func_pi_pT =
       SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininEmpirical1.pi_pT);
  end p_piT;

  redeclare final function extends W_A
    "Dubinin-Empirical-1 isotherm model: Filled pore volume as function of adsorption potential"

  algorithm
    W := 1 / max((c[3] + c[4] * A^2 * log(max(c[5] * A,
      Modelica.Constants.small)) + c[6] * A^3), Modelica.Constants.small)
      "Calculation of the equilibrium uptake of the adsorpt phase";

    //
    // Annotations
    //
    annotation (Inline=false,
  InlineAfterIndexReduction=false,
  LateInline=true,
  inverse(A=SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininEmpirical1.A_W(W, c, A_lb_start, A_ub_start, tolerance)));
  end W_A;

  redeclare function A_W
    "Dubinin-Empirical-1 isotherm model: Adsorption potential as function of the filled pore volume (numerical solution)"
    extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_A_W_num(
        A_lb_start = 1,
        redeclare final function func_W_A =
        SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininEmpirical1.W_A);

    //
    // Annotations
    //
    annotation (Inline=false,
  InlineAfterIndexReduction=false,
  LateInline=true,
  inverse(W=SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininEmpirical1.W_A(A, c, A_lb_start, A_ub_start, tolerance)));
  end A_W;

  redeclare final function extends dW_dA
    "Dubinin-Empirical-1 isotherm model: Partial derivative of filled pore volume w.r.t. adsorption potential at constant pressure and temperature"
  algorithm
    dW_dA := -A * (2 * c[4] * log(c[5] * A) + 3 * c[6] * A + c[4]) /
      (c[3] + c[4] * A^2 * log(c[5] * A) + c[6] * A^3)^2
      "Partial derivative of the filled pore volume w.r.t. adsorption potential at 
    constant pressure and temperature";
  end dW_dA;

  redeclare final function extends ddW_dA_dA
    "Dubinin-Empirical-1: Second-order partial derivative of filled pore volume w.r.t. adsorption potential at constant pressure and temperature"
  algorithm
    ddW_dA_dA := (6 * c[4]^2 * A^2 * log(c[5] * A)^2 + c[4] * (A^2 * (16 * c[6] *
      A + 5 * c[4]) - 2 * c[3]) * log(c[5] * A) + 12 * c[6]^2 * A^4 + 9 * c[4] *
      c[6] * A^3 + 2 * c[4]^2 * A^2 - 6 * c[3] * c[6] * A - 3 * c[3] * c[4]) /
      (A^2 * (c[4] * log(c[5] * A) + c[6] * A) + c[3])^3
      "Second-order partial derivative of filled pore volume w.r.t. adsorption 
    potential at constant pressure and temperature";
  end ddW_dA_dA;

  redeclare final function extends ddW_dA_dT
    "Dubinin-Empirical-1: Second-order partial derivative of filled pore volume w.r.t. adsorption potential and temperature at constant pressure"

    //
    // Definition of variables
    //
protected
    SorpLib.Units.DerMolarAdsorptionPotentialByTemperature dA_dT_adsorpt=
      SorpLib.Media.Functions.SorptionEquilibria.Utilities.dA_dT(
      p_adsorpt=p_adsorpt,
      p_sat=c[1],
      T_adsorpt=T_adsorpt,
      dp_sat_dT_adsorpt=dc_dT_adsorpt[1])
      "Partial derivative of adsorption potential w.r.t. equilibrium temperature
      at constant pressure";

    Real num = -A * (2 * c[4] * log(c[5] * A) + 3 * c[6] * A + c[4])
      "Numerator of characteristic curve";
    Real den = (c[3] + c[4] * A^2 * log(c[5] * A) + c[6] * A^3)^2
      "Denominator of characteristic curve";

    Real dnum_dc4 = -A * (2 * log(A * c[5]) + 1)
      "Partial derivative of numerator of characteristic curve w.r.t. fourth
    coefficient of isotherm";
    Real dnum_dc5 = -(2 * A * c[4]) / c[5]
      "Partial derivative of numerator of characteristic curve w.r.t. fivth
    coefficient of isotherm";
    Real dnum_dc6 = -3 * A^2
      "Partial derivative of numerator of characteristic curve w.r.t. sixth
    coefficient of isotherm";
    Real dnum_dA = -c[4] * (2 * log(c[5] * A) + 3) - 6 * c[6] * A
      "Partial derivative of numerator of characteristic curve w.r.t. adsorption
    potential";

    Real dden_dc3 = 2 * (c[3] + A^2 * (A * c[6] + c[4] * log(A * c[5])))
      "Partial derivative of denominator of characteristic curve w.r.t. third
    coefficient of isotherm";
    Real dden_dc4 = 2 * A^2 * log(A * c[5]) * (A^2 * (log(A * c[5]) * c[4] +
      A * c[6]) + c[3])
      "Partial derivative of denominator of characteristic curve w.r.t. fourth
    coefficient of isotherm";
    Real dden_dc5 = (2 * A^2 * c[4] * (A^2 * (c[4] * log(A * c[5]) + A * c[6]) +
      c[3])) / c[5]
      "Partial derivative of denominator of characteristic curve w.r.t. fivth
    coefficient of isotherm";
    Real dden_dc6 = 2 * A^3 * (A^2 * (A * c[6] + c[4] * log(A * c[5])) + c[3])
      "Partial derivative of denominator of characteristic curve w.r.t. sixth
    coefficient of isotherm";
    Real dden_dA = 2 * A * (2 * c[4] * log(c[5] * A) + 3 * c[6] * A + c[4]) *
      (A^2 * (c[4] * log(c[5] * A) + c[6] * A) + c[3])
      "Partial derivative of denominator of characteristic curve w.r.t. adsorption
    potential";

    Real dnum_dT_adsorpt = dnum_dc4 * dc_dT_adsorpt[4] +
      dnum_dc5 * dc_dT_adsorpt[5] +
      dnum_dc6 * dc_dT_adsorpt[6] +
      dnum_dA * dA_dT_adsorpt
      "Partial derivative of numerator of characteristic curve w.r.t. temprature";
    Real dden_dT_adsorpt = dden_dc3 * dc_dT_adsorpt[3] +
      dden_dc4 * dc_dT_adsorpt[4] +
      dden_dc5 * dc_dT_adsorpt[5] +
      dden_dc6 * dc_dT_adsorpt[6] +
      dden_dA * dA_dT_adsorpt
      "Partial derivative of denominator of characteristic curve w.r.t. temprature";

  algorithm
    ddW_dA_dT := (1/den) * dnum_dT_adsorpt +
      (-num/den^2) * dden_dT_adsorpt
      "Second-order partial derivative of filled pore volume w.r.t. adsorption 
    potential and temperature at constant pressure";
  end ddW_dA_dT;
  //
  // Annotations
  //
  annotation (Documentation(revisions="<html>
<ul>
  <li>
  November 3, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>", info="<html>
<p>
The Dubinin-Empirical-1 isotherm model is a four-parameter model for calculating 
the equilibrium uptake <i>x_adsorpt</i> as a function of the equilibrium  pressure 
<i>p_adsorpt</i>.
</p>

<h4>Main equations</h4>
<p>
The Dubinin-Empirical-1 isotherm model has the following form:
</p>
<pre>
    x<sub>adsorpt</sub> = &rho;<sub>adsorpt</sub>(T<sub>adsorpt</sub>) * W(A(T<sub>adsorpt</sub>));
</pre>
<p>
with
</p>
<pre>
    W(A(T<sub>adsorpt</sub>)) = 1 / (a + b * A<sup>2</sup> * <strong>ln</strong>(c * A) + d * A<sup>3</sup>);
</pre>
<pre>
    A(T<sub>adsorpt</sub>) = R * T<sub>adsorpt</sub> * <strong>ln</strong>(p<sub>sat</sub>(T<sub>adsorpt</sub>) / p<sub>adsorpt</sub>);
</pre>
<p>
Herein, <i>W(A(T<sub>adsorpt</sub>))</i> is the so-called characteristic curve and
<i>A(T<sub>adsorpt</sub>)</i> is the adsorption potential. Within the characteristic 
curve, the parameters <i>a</i>, <i>b</i>, and <i>c</i> are fitting parameters.
<br/><br/>
Note that the density of the adsorpt <i>&rho;<sub>adsorpt</sub>(T<sub>adsorpt</sub>)</i> 
is assumed to be the saturated liquid density &rho;<sub>sat,liq</sub>(T<sub>adsorpt</sub>) 
without any further information about the system under consideration. For super-critical
adsorptives (i.e., <i>T<sub>adsorpt</sub> &ge; T<sub>crit</sub></i>), the density of
the adsorpt <i>&rho;<sub>adsorpt</sub>(T<sub>adsorpt</sub>)</i> can be estimated by
</p>
<pre>
    &rho;<sub>adsorpt</sub>(T<sub>adsorpt</sub>) = &rho;<sub>sat,liq</sub>(T<sub>boiling,0</sub>) * <strong>exp</strong>(-0.0025 * (T<sub>adsorpt</sub> - T<sub>boiling,0</sub>));
</pre>
<p>
and a pseudo-vapour pressure <i>p<sub>sat</sub>(T<sub>adsorpt</sub>)</i> can be calculated by
</p>
<pre>
    p<sub>sat</sub>(T<sub>adsorpt</sub>) = p<sub>crit</sub>(T<sub>adsorpt</sub>) * (T<sub>adsorpt</sub> / T<sub>crit</sub>) ^ k;
</pre>
<p>
where <i>T<sub>boiling,0</sub></i> is the normal boiling point at 1 atm and <i>k</i> is
a fitting parameter specific to the system under consideration.
</p>

<h4>Required parameter order in function input c[:]:</h4>
<ul>
  <li>
  c[1] = p<sub>sat</sub>(T<sub>adsorpt</sub>) in Pa
  </li>
  <li>
  c[2] = &rho;<sub>adsorpt</sub>(T<sub>adsorpt</sub>) in kg/m<sup>3</sup>
  </li>
  <li>
  c[3] = a in kg/m<sup>3</sup>
  </li>
  <li>
  c[4] = b in kg.mol<sup>2</sup>/(m<sup>3</sup>.J<sup>2</sup>)
  </li>
  <li>
  c[5] = c in mol/J
  </li>
  <li>
  c[6] = d in kg.mol<sup>3</sup>/(m<sup>3</sup>.J<sup>3</sup>)
  </li>
</ul>

<h4>Assumptions and limitations</h4>
<ul>
  <li>
  The characteristic curve <i>W(A)</i> must decrease strictly monotonically with 
  increasing adsorption potential <i>A</i>. Otherwise, the inverses <i>A(W)</i>
  and <i>p(x,T</i>) may not be solveable.
  </li>
  <li>
  The reduced spreading pressure <i>&pi;</i> may not be calculable. Accordingly, the 
  inverse <i>p(&pi;,T)</i> cannot be calculated either.
  </li>
</ul>

<h4>Example</h4>
<p>
The following figure shows the Dubinin-Empirical-1 isotherm model for one 
parameter set. In the upper sub-figure, the equilibrium pressure changes with 
time, while the equilibrium temperature is constant. In the centered sub-figure, the 
equilibrium temperature changes with time, while the equilibrium pressure is constant. 
In the lower sub-figure, the characteristic curve is shown. 
<br/><br/>
</p>
<img src=\"Modelica://SorpLib/Resources/doc/media_functions_equilibria_pure_dubinin_empirical_1.png\" alt=\"media_functions_equilibria_pure_dubinin_empirical_1.png\">

<h4>References</h4>
<ul>
  <li>
  Do, D. D. (1998). Adsorption Analysis: Equilibria and Kinetics, 1st Edition, ISBN 978-1-86094-130-6, Imperial College Press.
  </li>
  <li>
  Schawe, D. (1999). Theoretical and Experimental Investigations of an Adsorption Heat Pump with Heat Transfer between two Adsorbers, PhD Thesis, Stuttgart.
  </li>
</ul>
</html>"));
end DubininEmpirical1;
