within SorpLib.Media.Functions.SorptionEquilibria.PureComponents;
package BET "Package containing all functions regarding the BET isotherm"
extends
  SorpLib.Media.Functions.SorptionEquilibria.Interfaces.PartialPureComponents;

  redeclare final function extends x_pT
    "BET isotherm model: Uptake as function of pressure and temperature"

    //
    // Definition of variables
    //
protected
    Real phi = p_adsorpt/c[1]
      "Relative pressure";

    Real F = c[2]*c[3] * phi / (1 - phi)
      "Pre-factor";
    Real A = 1 + (c[4]*c[5]/2 - c[4]) * phi ^ (c[4]-1) -
      (c[4]*c[5] - c[4] + 1) * phi^c[4] + (c[4]*c[5]/2) * phi ^ (c[4]+1)
      "Numerator of the fraction";
    Real B = 1 + (c[3] - 1) * phi + (c[3]*c[5]/2 - c[3]) * phi^c[4] -
      (c[3]*c[5]/2) * phi ^ (c[4]+1)
      "Denominator of the fraction";

  algorithm
    x_adsorpt := F * A/B
      "Calculation of the equilibrium uptake of the adsorpt phase";

    //
    // Annotations
    //
    annotation (Inline=false,
  InlineAfterIndexReduction=false,
  LateInline=true,
  inverse(p_adsorpt=SorpLib.Media.Functions.SorptionEquilibria.PureComponents.BET.p_xT(x_adsorpt, T_adsorpt, c, p_adsorpt_lb_start, p_adsorpt_ub_start, tolerance)));
  end x_pT;

  redeclare function p_xT
    "BET isotherm model: Pressure as function of uptake and temperature (numerical solution)"
    extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_p_xT_num(
        redeclare final function func_x_pT =
        SorpLib.Media.Functions.SorptionEquilibria.PureComponents.BET.x_pT);

    //
    // Annotations
    //
    annotation (Inline=false,
  InlineAfterIndexReduction=false,
  LateInline=true,
  inverse(x_adsorpt=SorpLib.Media.Functions.SorptionEquilibria.PureComponents.BET.x_pT(p_adsorpt, T_adsorpt, c, p_adsorpt_lb_start, p_adsorpt_ub_start, tolerance)));
  end p_xT;

  redeclare final function extends dx_dp
    "BET isotherm model: Partial derivative of uptake w.r.t. pressure at constant temperature"

    //
    // Definition of variables
    //
protected
    Real phi = p_adsorpt/c[1]
      "Relative pressure";

    Real F = c[2]*c[3] * phi / (1 - phi)
      "Pre-factor";
    Real A = 1 + (c[4]*c[5]/2 - c[4]) * phi ^ (c[4]-1) -
      (c[4]*c[5] - c[4] + 1) * phi^c[4] + (c[4]*c[5]/2) * phi ^ (c[4]+1)
      "Numerator of the fraction";
    Real B = 1 + (c[3] - 1) * phi + (c[3]*c[5]/2 - c[3]) * phi^c[4] -
      (c[3]*c[5]/2) * phi ^ (c[4]+1)
      "Denominator of the fraction";

    Real dphi_dp_adsorpt =  1/c[1]
      "Partial derivative of relative pressure w.r.t. to pressure";

    Real dF_dphi = (c[2] * c[3]) / (phi - 1)^2
      "Partial derivative of pre-factor w.r.t. to relative pressure";
    Real dA_dphi = (c[4] * phi ^ (c[4]-2) * ((c[4] + 1) * c[5] * phi^2 +
      (-2*c[4]*c[5] + 2*c[4] - 2) * phi + (c[4]-1) * c[5] - 2*c[4] + 2)) / 2
      "Partial derivative of numerator of the fraction w.r.t. to relative pressure";
    Real dB_dphi = -(c[3] * (c[4]+1) * c[5] * phi^c[4]) / 2 +
      c[4] * ((c[3]*c[5])/2 - c[3]) * phi ^ (c[4]-1) + c[3] - 1
      "Partial derivative of denominator of the fraction w.r.t. to relative pressure";

    Real dF_dp_adsorpt = dF_dphi * dphi_dp_adsorpt
      "Partial derivative of pre-factor w.r.t. pressure";
    Real dA_dp_adsorpt = dA_dphi * dphi_dp_adsorpt
      "Partial derivative of numerator w.r.t. pressure";
    Real dB_dp_adsorpt = dB_dphi * dphi_dp_adsorpt
      "Partial derivative of denominator w.r.t. pressure";

  algorithm
    dx_adsorpt_dp_adsorpt := (A/B) * dF_dp_adsorpt +
      (F/B) * dA_dp_adsorpt +
      (-F * A/B^2) * dB_dp_adsorpt
      "Calculation of the partial derivative of the equilibrium uptake w.r.t. the
     equilibrium pressure at constant temperature";
  end dx_dp;

  redeclare final function extends dx_dT
    "BET isotherm model: Partial derivative of uptake w.r.t. temperature at constant pressure"

    //
    // Definition of variables
    //
protected
    Real phi = p_adsorpt/c[1]
      "Relative pressure";

    Real F = c[2]*c[3] * phi / (1 - phi)
      "Pre-factor";
    Real A = 1 + (c[4]*c[5]/2 - c[4]) * phi ^ (c[4]-1) -
      (c[4]*c[5] - c[4] + 1) * phi^c[4] + (c[4]*c[5]/2) * phi ^ (c[4]+1)
      "Numerator of the fraction";
    Real B = 1 + (c[3] - 1) * phi + (c[3]*c[5]/2 - c[3]) * phi^c[4] -
      (c[3]*c[5]/2) * phi ^ (c[4]+1)
      "Denominator of the fraction";

    Real dphi_dT_adsorpt = -p_adsorpt/c[1]^2 * dc_dT_adsorpt[1]
      "Partial derivative of relative pressure w.r.t. to temperature";

    Real dF_dc2 = c[3] * phi / (1 - phi)
      "Partial derivative of pre-factor w.r.t. to second coefficient of BET isotherm";
    Real dF_dc3 = c[2] * phi / (1 - phi)
      "Partial derivative of pre-factor w.r.t. to third coefficient of BET isotherm";
    Real dF_dphi = (c[2] * c[3]) / (phi - 1)^2
      "Partial derivative of pre-factor w.r.t. to relative pressure";

    Real dA_dc4 = (phi ^ (c[4]-1) * ((c[5] * phi^2 + (2-2*c[5]) * phi + c[5] - 2) *
      log(phi) * c[4] - 2*phi * log(phi) + c[5] * phi^2 + (2-2*c[5]) * phi + c[5] -
      2)) / 2
      "Partial derivative of numerator of the fraction w.r.t. to fourth coefficient 
    of BET isotherm";
    Real dA_dc5 = (c[4] * phi ^ (c[4]-1) * (phi^2 - 2*phi + 1)) / 2
      "Partial derivative of numerator of the fraction w.r.t. to fivth coefficient 
    of BET isotherm";
    Real dA_dphi = (c[4] * phi ^ (c[4]-2) * ((c[4] + 1) * c[5] * phi^2 +
      (-2*c[4]*c[5] + 2*c[4] - 2) * phi + (c[4]-1) * c[5] - 2*c[4] + 2)) / 2
      "Partial derivative of numerator of the fraction w.r.t. to relative pressure";

    Real dB_dc3 = -(phi^c[4] * (c[5]*phi - c[5] + 2) - 2*phi) / 2
      "Partial derivative of denominator of the fraction w.r.t. to third coefficient 
    of BET isotherm";
    Real dB_dc4 = -(c[3] * phi^c[4] * (c[5]*phi - c[5] + 2) * log(phi)) / 2
      "Partial derivative of denominator of the fraction w.r.t. to fourth coefficient 
    of BET isotherm";
    Real dB_dc5 = -(c[3] * (phi-1) * phi^c[4]) / 2
      "Partial derivative of denominator of the fraction w.r.t. to fivth coefficient 
    of BET isotherm";
    Real dB_dphi = -(c[3] * (c[4]+1) * c[5] * phi^c[4]) / 2 +
      c[4] * ((c[3]*c[5])/2 - c[3]) * phi ^ (c[4]-1) + c[3] - 1
      "Partial derivative of denominator of the fraction w.r.t. to relative pressure";

    Real dF_dT_adsorpt = dF_dc2 * dc_dT_adsorpt[2] +
      dF_dc3 * dc_dT_adsorpt[3] +
      dF_dphi * dphi_dT_adsorpt
      "Partial derivative of pre-factor w.r.t. temperature";
    Real dA_dT_adsorpt = dA_dc4 * dc_dT_adsorpt[4] +
      dA_dc5 * dc_dT_adsorpt[5] +
      dA_dphi * dphi_dT_adsorpt
      "Partial derivative of numerator w.r.t. temperature";
    Real dB_dT_adsorpt = dB_dc3 * dc_dT_adsorpt[3] +
      dB_dc4 * dc_dT_adsorpt[4] +
      dB_dc5 * dc_dT_adsorpt[5] +
      dB_dphi * dphi_dT_adsorpt
      "Partial derivative of denominator w.r.t. temperature";

  algorithm
    dx_adsorpt_dT_adsorpt := (A/B) * dF_dT_adsorpt +
      (F/B) * dA_dT_adsorpt +
      (-F * A/B^2) * dB_dT_adsorpt
      "Calculation of the partial derivative of the equilibrium uptake w.r.t. the
     equilibrium temperature at constant pressure";
  end dx_dT;

  redeclare final function extends ddx_dp_dp
    "BET isotherm model: Second-order partial derivative of uptake w.r.t. pressure at constant temperature"

    //
    // Definition of variables
    //
protected
    Real phi = p_adsorpt/c[1]
      "Relative pressure";

    Real F = c[2]*c[3] * phi / (1 - phi)
      "Pre-factor";
    Real A = 1 + (c[4]*c[5]/2 - c[4]) * phi ^ (c[4]-1) -
      (c[4]*c[5] - c[4] + 1) * phi^c[4] + (c[4]*c[5]/2) * phi ^ (c[4]+1)
      "Numerator of the fraction";
    Real B = 1 + (c[3] - 1) * phi + (c[3]*c[5]/2 - c[3]) * phi^c[4] -
      (c[3]*c[5]/2) * phi ^ (c[4]+1)
      "Denominator of the fraction";

    Real dphi_dp_adsorpt =  1/c[1]
      "Partial derivative of relative pressure w.r.t. to pressure";

    Real dF_dphi = (c[2] * c[3]) / (phi - 1)^2
      "Partial derivative of pre-factor w.r.t. to relative pressure";
    Real dA_dphi = (c[4] * phi ^ (c[4]-2) * ((c[4] + 1) * c[5] * phi^2 +
      (-2*c[4]*c[5] + 2*c[4] - 2) * phi + (c[4]-1) * c[5] - 2*c[4] + 2)) / 2
      "Partial derivative of numerator of the fraction w.r.t. to relative pressure";
    Real dB_dphi = -(c[3] * (c[4]+1) * c[5] * phi^c[4]) / 2 +
      c[4] * ((c[3]*c[5])/2 - c[3]) * phi ^ (c[4]-1) + c[3] - 1
      "Partial derivative of denominator of the fraction w.r.t. to relative pressure";

    Real dF_dp_adsorpt = dF_dphi * dphi_dp_adsorpt
      "Partial derivative of pre-factor w.r.t. pressure";
    Real dA_dp_adsorpt = dA_dphi * dphi_dp_adsorpt
      "Partial derivative of numerator w.r.t. pressure";
    Real dB_dp_adsorpt = dB_dphi * dphi_dp_adsorpt
      "Partial derivative of denominator w.r.t. pressure";

    Real ddF_dphi_dp_adsorpt=
      (-(2 * c[2] * c[3]) / (phi - 1)^3) * dphi_dp_adsorpt
      "Second-order artial derivative of pre-factor w.r.t. to relative pressure 
    and pressure";
    Real ddA_dphi_dp_adsorpt=
      ((c[4] * phi^(c[4] - 3) * (phi * (c[4] * (c[5] * ((c[4] + 1) * phi -
      2 * c[4] + 2) + 2 * c[4] - 4) + 2) + (c[4] - 2) * (c[4] - 1) * c[5] -
      2 * (c[4] - 2) * (c[4] - 1))) / 2) * dphi_dp_adsorpt
      "Second-order artial derivative of numerator of the fraction w.r.t. to 
    relative pressure and pressure";
    Real ddB_dphi_dp_adsorpt=
      (-(c[3] * c[4] * phi^(c[4] - 2) * (c[5] * ((c[4] + 1) * phi -
      c[4] + 1) + 2 * c[4] - 2)) / 2) * dphi_dp_adsorpt
      "Second-order artial derivative of denominator of the fraction w.r.t. to 
    relative pressure and pressure";

    Real ddF_dp_adsorpt_dp_adsorpt= (dphi_dp_adsorpt) * ddF_dphi_dp_adsorpt
      "Second-order partial derivative of pre-factor w.r.t. pressure";
    Real ddA_dp_adsorpt_dp_adsorpt= (dphi_dp_adsorpt) * ddA_dphi_dp_adsorpt
      "Second-order partial derivative of numerator w.r.t. pressure";
    Real ddB_dp_adsorpt_dp_adsorpt= (dphi_dp_adsorpt) * ddB_dphi_dp_adsorpt
      "Second-order partial derivative of denominator w.r.t. pressure";

  algorithm
    ddx_adsorpt_dp_adsorpt_dp_adsorpt :=
      ((1/B * dF_dp_adsorpt) * dA_dp_adsorpt +
       (-A/B^2 * dF_dp_adsorpt) * dB_dp_adsorpt +
       (A/B) * ddF_dp_adsorpt_dp_adsorpt) +
      ((1/B * dA_dp_adsorpt) * dF_dp_adsorpt +
       (-F/B^2 * dA_dp_adsorpt) * dB_dp_adsorpt +
       (F/B) * ddA_dp_adsorpt_dp_adsorpt) +
       ((-A/B^2 * dB_dp_adsorpt) * dF_dp_adsorpt +
        (-F/B^2 * dB_dp_adsorpt) * dA_dp_adsorpt +
        (2 * F * A/B^3 * dB_dp_adsorpt) * dB_dp_adsorpt +
        (-F * A/B^2) * ddB_dp_adsorpt_dp_adsorpt)
      "Calculation of the second-order partial derivative of the equilibrium uptake 
    w.r.t. the equilibrium pressure at constant temperature";
  end ddx_dp_dp;

  redeclare final function extends ddx_dT_dT
    "BET isotherm model: Second-order partial derivative of uptake w.r.t. temperature at constant pressure"

    //
    // Definition of variables
    //
protected
    Real phi = p_adsorpt/c[1]
      "Relative pressure";

    Real F = c[2]*c[3] * phi / (1 - phi)
      "Pre-factor";
    Real A = 1 + (c[4]*c[5]/2 - c[4]) * phi ^ (c[4]-1) -
      (c[4]*c[5] - c[4] + 1) * phi^c[4] + (c[4]*c[5]/2) * phi ^ (c[4]+1)
      "Numerator of the fraction";
    Real B = 1 + (c[3] - 1) * phi + (c[3]*c[5]/2 - c[3]) * phi^c[4] -
      (c[3]*c[5]/2) * phi ^ (c[4]+1)
      "Denominator of the fraction";

    Real dphi_dT_adsorpt = -p_adsorpt/c[1]^2 * dc_dT_adsorpt[1]
      "Partial derivative of relative pressure w.r.t. to temperature";

    Real dF_dc2 = c[3] * phi / (1 - phi)
      "Partial derivative of pre-factor w.r.t. to second coefficient of BET isotherm";
    Real dF_dc3 = c[2] * phi / (1 - phi)
      "Partial derivative of pre-factor w.r.t. to third coefficient of BET isotherm";
    Real dF_dphi = (c[2] * c[3]) / (phi - 1)^2
      "Partial derivative of pre-factor w.r.t. to relative pressure";

    Real dA_dc4 = (phi ^ (c[4]-1) * ((c[5] * phi^2 + (2-2*c[5]) * phi + c[5] - 2) *
      log(phi) * c[4] - 2*phi * log(phi) + c[5] * phi^2 + (2-2*c[5]) * phi + c[5] -
      2)) / 2
      "Partial derivative of numerator of the fraction w.r.t. to fourth coefficient 
    of BET isotherm";
    Real dA_dc5 = (c[4] * phi ^ (c[4]-1) * (phi^2 - 2*phi + 1)) / 2
      "Partial derivative of numerator of the fraction w.r.t. to fivth coefficient 
    of BET isotherm";
    Real dA_dphi = (c[4] * phi ^ (c[4]-2) * ((c[4] + 1) * c[5] * phi^2 +
      (-2*c[4]*c[5] + 2*c[4] - 2) * phi + (c[4]-1) * c[5] - 2*c[4] + 2)) / 2
      "Partial derivative of numerator of the fraction w.r.t. to relative pressure";

    Real dB_dc3 = -(phi^c[4] * (c[5]*phi - c[5] + 2) - 2*phi) / 2
      "Partial derivative of denominator of the fraction w.r.t. to third coefficient 
    of BET isotherm";
    Real dB_dc4 = -(c[3] * phi^c[4] * (c[5]*phi - c[5] + 2) * log(phi)) / 2
      "Partial derivative of denominator of the fraction w.r.t. to fourth coefficient 
    of BET isotherm";
    Real dB_dc5 = -(c[3] * (phi-1) * phi^c[4]) / 2
      "Partial derivative of denominator of the fraction w.r.t. to fivth coefficient 
    of BET isotherm";
    Real dB_dphi = -(c[3] * (c[4]+1) * c[5] * phi^c[4]) / 2 +
      c[4] * ((c[3]*c[5])/2 - c[3]) * phi ^ (c[4]-1) + c[3] - 1
      "Partial derivative of denominator of the fraction w.r.t. to relative pressure";

    Real dF_dT_adsorpt = dF_dc2 * dc_dT_adsorpt[2] +
      dF_dc3 * dc_dT_adsorpt[3] +
      dF_dphi * dphi_dT_adsorpt
      "Partial derivative of pre-factor w.r.t. temperature";
    Real dA_dT_adsorpt = dA_dc4 * dc_dT_adsorpt[4] +
      dA_dc5 * dc_dT_adsorpt[5] +
      dA_dphi * dphi_dT_adsorpt
      "Partial derivative of numerator w.r.t. temperature";
    Real dB_dT_adsorpt = dB_dc3 * dc_dT_adsorpt[3] +
      dB_dc4 * dc_dT_adsorpt[4] +
      dB_dc5 * dc_dT_adsorpt[5] +
      dB_dphi * dphi_dT_adsorpt
      "Partial derivative of denominator w.r.t. temperature";

    Real ddphi_dT_adsorpt_dT_adsorpt = (2*p_adsorpt/c[1]^3 * dc_dT_adsorpt[1]^2) +
      (-p_adsorpt/c[1]^2) * ddc_dT_adsorpt_dT_adsorpt[1]
      "Second-order partial derivative of relative pressure w.r.t. to temperature";

    Real ddF_dc2_dc3 = phi / (1 - phi)
      "Second-order partial derivative of pre-factor w.r.t. to second and third 
     coefficient of BET isotherm";
    Real ddF_dc2_dphi = c[3] / (phi -1)^2
      "Second-order partial derivative of pre-factor w.r.t. to second coefficient 
    of BET isotherm and relative pressure";
    Real ddF_dc3_dphi = c[2] / (phi -1)^2
      "Second-order partial derivative of pre-factor w.r.t. to third coefficient 
    of BET isotherm and relative pressure";
    Real ddF_dphi_dphi = -2 * (c[2] * c[3]) / (phi - 1)^3
      "Second-order partial derivative of pre-factor w.r.t. to relative pressure";

    Real ddA_dc4_dc4 = (phi^(c[4] - 1) * log(phi) * ((c[5] * (phi - 1) + 2) *
      (phi - 1) * log(phi) * c[4] - 2 * phi * log(phi) + 2 * c[5] * phi^2 - 4 *
      (c[5] - 1) * phi + 2 * c[5] - 4)) / 2
      "Second-order partial derivative of numerator w.r.t. to fourth coefficient
    of BET isoterm";
    Real ddA_dc4_dc5 = ((phi - 1)^2 * phi^(c[4] - 1) * (c[4] * log(phi) + 1)) / 2
      "Second-order partial derivative of numerator w.r.t. to fourth and fivth 
    coefficient of BET isoterm";
    Real ddA_dc4_dphi = ((phi - 1) * phi^(c[4] - 2) * (c[4] * (c[5] * ((c[4] + 1) *
      phi - c[4] + 1) + 2 * c[4] - 2) * log(phi) + (2 * c[4] + 1) * c[5] * phi -
      (2 * c[4] - 1) * c[5] + 4 * c[4] - 2)) / 2
      "Second-order partial derivative of numerator w.r.t. to fourth coefficient
    of BET isoterm and relative pressure";
    Real ddA_dc5_dphi = (c[4] * (phi - 1) * phi^(c[4] - 2) * (c[4] * phi + phi -
      c[4] + 1)) / 2
      "Second-order partial derivative of numerator w.r.t. to fivth coefficient
    of BET isoterm and relative pressure";
    Real ddA_dphi_dphi = (c[4] * phi^(c[4] - 3) * (phi * (c[4] * (c[5] * ((c[4] +
      1) * phi - 2 * c[4] + 2) + 2 * c[4] - 4) + 2) + (c[4] - 2) * (c[4] - 1) *
      c[5] - 2 * (c[4] - 2) * (c[4] - 1))) / 2
      "Second-order partial derivative of numerator w.r.t. to relative pressure";

    Real ddB_dc3_dc4 = -((c[5] * (phi - 1) + 2) * phi^c[4] * log(phi)) / 2
      "Second-order partial derivative of denominator w.r.t. to third and fourht
    coefficient of BET isoterm";
    Real ddB_dc3_dc5 = -((phi - 1) * phi^c[4]) / 2
      "Second-order partial derivative of denominator w.r.t. to third and fivth 
    coefficient of BET isoterm";
    Real ddB_dc3_dphi = (-c[5] * phi^c[4] - c[4] * phi^(c[4] - 1) * (c[5] * phi -
      c[5] + 2)) / 2 + 1
      "Second-order partial derivative of denominator w.r.t. to third coefficient
    of BET isoterm and relative pressure";
    Real ddB_dc4_dc4 = -(c[3] * (c[5] * (phi - 1) + 2) * phi^c[4] * log(phi)^2) / 2
      "Second-order partial derivative of denominator w.r.t. to fourth coefficient
    of BET isoterm";
    Real ddB_dc4_dc5 = -(c[3] * (phi - 1) * phi^c[4] * log(phi)) / 2
      "Second-order partial derivative of denominator w.r.t. to fourth and fivth 
    coefficient of BET isoterm";
    Real ddB_dc4_dphi = -(c[3] * phi^(c[4] - 1) * (((c[4] + 1) * c[5] * phi -
      c[4] * c[5] + 2 * c[4]) * log(phi) + c[5] * phi - c[5] + 2)) / 2
      "Second-order partial derivative of denominator w.r.t. to fourth coefficient
    of BET isoterm and relative pressure";
    Real ddB_dc5_dphi = -(c[3] * phi^(c[4] - 1) * (c[4] * phi + phi - c[4])) / 2
      "Second-order partial derivative of denominator w.r.t. to fivth coefficient
    of BET isoterm and relative pressure";
    Real ddB_dphi_dphi = -(c[3] * c[4] * phi^(c[4] - 2) * (c[5] * ((c[4] + 1) *
      phi - c[4] + 1) + 2 * c[4] - 2)) / 2
      "Second-order partial derivative of denominator w.r.t. to relative pressure";

    Real ddF_dc2_dT_adsorpt = ddF_dc2_dc3 * dc_dT_adsorpt[3] +
      ddF_dc2_dphi * dphi_dT_adsorpt
      "Second-order partial derivative of pre-factor w.r.t. to second coefficient
    of BET isoterm and temperature";
    Real ddF_dc3_dT_adsorpt = ddF_dc2_dc3 * dc_dT_adsorpt[2] +
      ddF_dc3_dphi * dphi_dT_adsorpt
      "Second-order partial derivative of pre-factor w.r.t. to third coefficient
    of BET isoterm and temperature";
    Real ddF_dphi_dT_adsorpt = ddF_dc2_dphi * dc_dT_adsorpt[2] +
      ddF_dc3_dphi * dc_dT_adsorpt[3] +
      ddF_dphi_dphi * dphi_dT_adsorpt
      "Second-order partial derivative of pre-factor w.r.t. to relative pressure
    and temperature";

    Real ddA_dc4_dT_adsorpt = ddA_dc4_dc4 * dc_dT_adsorpt[4] +
      ddA_dc4_dc5 * dc_dT_adsorpt[5] +
      ddA_dc4_dphi * dphi_dT_adsorpt
      "Second-order partial derivative of numerator w.r.t. to fourth coefficient
    of BET isoterm and temperature";
    Real ddA_dc5_dT_adsorpt = ddA_dc4_dc5 * dc_dT_adsorpt[4] +
      ddA_dc5_dphi * dphi_dT_adsorpt
      "Second-order partial derivative of numerator w.r.t. to fivth coefficient
    of BET isoterm and temperature";
    Real ddA_dphi_dT_adsorpt = ddA_dc4_dphi * dc_dT_adsorpt[4] +
      ddA_dc5_dphi * dc_dT_adsorpt[5] +
      ddA_dphi_dphi * dphi_dT_adsorpt
      "Second-order partial derivative of numerator w.r.t. to relative pressure
    and temperature";

    Real ddB_dc3_dT_adsorpt = ddB_dc3_dc4 * dc_dT_adsorpt[4] +
      ddB_dc3_dc5 * dc_dT_adsorpt[5] +
      ddB_dc3_dphi * dphi_dT_adsorpt
      "Second-order partial derivative of denominator w.r.t. to fourth coefficient
    of BET isoterm and temperature";
    Real ddB_dc4_dT_adsorpt = ddB_dc3_dc4 * dc_dT_adsorpt[3] +
      ddB_dc4_dc4 * dc_dT_adsorpt[4] +
      ddB_dc4_dc5 * dc_dT_adsorpt[5] +
      ddB_dc4_dphi * dphi_dT_adsorpt
      "Second-order partial derivative of denominator w.r.t. to fourth coefficient
    of BET isoterm and temperature";
    Real ddB_dc5_dT_adsorpt = ddB_dc3_dc5 * dc_dT_adsorpt[3] +
      ddB_dc4_dc5 * dc_dT_adsorpt[4] +
      ddB_dc5_dphi * dphi_dT_adsorpt
      "Second-order partial derivative of denominator w.r.t. to fivth coefficient
    of BET isoterm and temperature";
    Real ddB_dphi_dT_adsorpt = ddB_dc3_dphi * dc_dT_adsorpt[3] +
      ddB_dc4_dphi * dc_dT_adsorpt[4] +
      ddB_dc5_dphi * dc_dT_adsorpt[5] +
      ddB_dphi_dphi * dphi_dT_adsorpt
      "Second-order partial derivative of denominator w.r.t. to relative pressure
    and temperature";

    Real ddF_dT_adsorpt_dT_adsorpt = ddF_dc2_dT_adsorpt * dc_dT_adsorpt[2] +
      dF_dc2 * ddc_dT_adsorpt_dT_adsorpt[2] +
      ddF_dc3_dT_adsorpt * dc_dT_adsorpt[3] +
      dF_dc3 * ddc_dT_adsorpt_dT_adsorpt[3] +
      ddF_dphi_dT_adsorpt * dphi_dT_adsorpt +
      dF_dphi * ddphi_dT_adsorpt_dT_adsorpt
      "Second-order partial derivative of pre-factor w.r.t. temperature";
    Real ddA_dT_adsorpt_dT_adsorpt = ddA_dc4_dT_adsorpt * dc_dT_adsorpt[4] +
      dA_dc4 * ddc_dT_adsorpt_dT_adsorpt[4] +
      ddA_dc5_dT_adsorpt * dc_dT_adsorpt[5] +
      dA_dc5 * ddc_dT_adsorpt_dT_adsorpt[5] +
      ddA_dphi_dT_adsorpt * dphi_dT_adsorpt +
      dA_dphi * ddphi_dT_adsorpt_dT_adsorpt
      "Second-order partial derivative of numerator w.r.t. temperature";
    Real ddB_dT_adsorpt_dT_adsorpt = ddB_dc3_dT_adsorpt * dc_dT_adsorpt[3] +
      dB_dc3 * ddc_dT_adsorpt_dT_adsorpt[3] +
      ddB_dc4_dT_adsorpt * dc_dT_adsorpt[4] +
      dB_dc4 * ddc_dT_adsorpt_dT_adsorpt[4] +
      ddB_dc5_dT_adsorpt * dc_dT_adsorpt[5] +
      dB_dc5 * ddc_dT_adsorpt_dT_adsorpt[5] +
      ddB_dphi_dT_adsorpt * dphi_dT_adsorpt +
      dB_dphi * ddphi_dT_adsorpt_dT_adsorpt
      "Second-order partial derivative of denominator w.r.t. temperature";

  algorithm
    ddx_adsorpt_dT_adsorpt_dT_adsorpt :=
      ((dF_dT_adsorpt/B) * dA_dT_adsorpt +
       (-A/B^2 * dF_dT_adsorpt) * dB_dT_adsorpt +
       (A/B) * ddF_dT_adsorpt_dT_adsorpt) +
      ((dA_dT_adsorpt/B) * dF_dT_adsorpt +
       (-F/B^2 * dA_dT_adsorpt) * dB_dT_adsorpt +
       (F/B) * ddA_dT_adsorpt_dT_adsorpt) +
      ((-A/B^2 * dB_dT_adsorpt) * dF_dT_adsorpt +
       (-F/B^2 * dB_dT_adsorpt) * dA_dT_adsorpt +
       (2*F * A/B^3 * dB_dT_adsorpt) * dB_dT_adsorpt +
       (-F * A/B^2) * ddB_dT_adsorpt_dT_adsorpt)
      "Calculation of the second-order partial derivative of the equilibrium uptake 
    w.r.t. the equilibrium temperature at constant pressure";
  end ddx_dT_dT;

  redeclare final function extends ddx_dp_dT
    "BET isotherm model: Second-order partial derivative of uptake w.r.t. pressure and temperature"

    //
    // Definition of variables
    //
protected
    Real phi = p_adsorpt/c[1]
      "Relative pressure";

    Real F = c[2]*c[3] * phi / (1 - phi)
      "Pre-factor";
    Real A = 1 + (c[4]*c[5]/2 - c[4]) * phi ^ (c[4]-1) -
      (c[4]*c[5] - c[4] + 1) * phi^c[4] + (c[4]*c[5]/2) * phi ^ (c[4]+1)
      "Numerator of the fraction";
    Real B = 1 + (c[3] - 1) * phi + (c[3]*c[5]/2 - c[3]) * phi^c[4] -
      (c[3]*c[5]/2) * phi ^ (c[4]+1)
      "Denominator of the fraction";

    Real dphi_dp_adsorpt =  1/c[1]
      "Partial derivative of relative pressure w.r.t. to pressure";
    Real dphi_dT_adsorpt = -p_adsorpt/c[1]^2 * dc_dT_adsorpt[1]
      "Partial derivative of relative pressure w.r.t. to temperature";

    Real dF_dc2 = c[3] * phi / (1 - phi)
      "Partial derivative of pre-factor w.r.t. to second coefficient of BET isotherm";
    Real dF_dc3 = c[2] * phi / (1 - phi)
      "Partial derivative of pre-factor w.r.t. to third coefficient of BET isotherm";
    Real dF_dphi = (c[2] * c[3]) / (phi - 1)^2
      "Partial derivative of pre-factor w.r.t. to relative pressure";

    Real dA_dc4 = (phi ^ (c[4]-1) * ((c[5] * phi^2 + (2-2*c[5]) * phi + c[5] - 2) *
      log(phi) * c[4] - 2*phi * log(phi) + c[5] * phi^2 + (2-2*c[5]) * phi + c[5] -
      2)) / 2
      "Partial derivative of numerator of the fraction w.r.t. to fourth coefficient 
    of BET isotherm";
    Real dA_dc5 = (c[4] * phi ^ (c[4]-1) * (phi^2 - 2*phi + 1)) / 2
      "Partial derivative of numerator of the fraction w.r.t. to fivth coefficient 
    of BET isotherm";
    Real dA_dphi = (c[4] * phi ^ (c[4]-2) * ((c[4] + 1) * c[5] * phi^2 +
      (-2*c[4]*c[5] + 2*c[4] - 2) * phi + (c[4]-1) * c[5] - 2*c[4] + 2)) / 2
      "Partial derivative of numerator of the fraction w.r.t. to relative pressure";

    Real dB_dc3 = -(phi^c[4] * (c[5]*phi - c[5] + 2) - 2*phi) / 2
      "Partial derivative of denominator of the fraction w.r.t. to third coefficient 
    of BET isotherm";
    Real dB_dc4 = -(c[3] * phi^c[4] * (c[5]*phi - c[5] + 2) * log(phi)) / 2
      "Partial derivative of denominator of the fraction w.r.t. to fourth coefficient 
    of BET isotherm";
    Real dB_dc5 = -(c[3] * (phi-1) * phi^c[4]) / 2
      "Partial derivative of denominator of the fraction w.r.t. to fivth coefficient 
    of BET isotherm";
    Real dB_dphi = -(c[3] * (c[4]+1) * c[5] * phi^c[4]) / 2 +
      c[4] * ((c[3]*c[5])/2 - c[3]) * phi ^ (c[4]-1) + c[3] - 1
      "Partial derivative of denominator of the fraction w.r.t. to relative pressure";

    Real ddphi_dp_adsorpt_dT_adsorpt = -1/c[1]^2 * dc_dT_adsorpt[1]
      "Second-order partial derivative of relative pressure w.r.t. pressure and 
    temperature";

    Real ddF_dphi_dc2 =  c[3] / (phi - 1)^2
      "Second-order partial derivative of pre-factor w.r.t. to relative pressure
    and second coefficient of BET isotherm";
    Real ddF_dphi_dc3 = c[2] / (phi - 1)^2
      "Second-order partial derivative of pre-factor w.r.t. to relative pressure
    and third coefficient of BET isotherm";
    Real ddF_dphi_dphi = -2 * (c[2] * c[3]) / (phi - 1)^3
      "Second-order partial derivative of pre-factor w.r.t. to relative pressure";

    Real ddA_dphi_dc4 = ((phi - 1) * phi^(c[4] - 2) * (c[4] * (log(phi) * ((c[5] *
      (phi - 1) + 2) * c[4] + c[5] * (phi + 1) - 2) + 2 * c[5] * (phi - 1) + 4) +
      c[5] * (phi + 1) - 2)) / 2
      "Second-order partial derivative of numerator w.r.t. to relative pressure
    and fourth coefficient of BET isotherm";
    Real ddA_dphi_dc5 = (c[4] * (phi - 1) * phi^(c[4] - 2) * ((c[4] + 1) * phi -
      c[4] + 1)) / 2
      "Second-order partial derivative of numerator w.r.t. to relative pressure
    and fivth coefficient of BET isotherm";
    Real ddA_dphi_dphi = (c[4] * phi^(c[4] - 3) * (phi * (c[4] * (c[5] * ((c[4] +
      1) * phi - 2 * c[4] + 2) + 2 * c[4] - 4) + 2) + (c[4] - 2) * (c[4] - 1) *
      c[5] - 2 * (c[4] - 2) * (c[4] - 1))) / 2
      "Second-order partial derivative of numerator w.r.t. to relative pressure";

    Real ddB_dphi_dc3 = (-(c[4] + 1) * c[5] * phi^c[4] + c[4] * (c[5] - 2) *
      phi^(c[4] - 1) + 2) / 2
      "Second-order partial derivative of denominator w.r.t. to relative pressure
    and fourth coefficient of BET isotherm";
    Real ddB_dphi_dc4 = -(c[3] * phi^(c[4] - 1) * ((c[5] * (phi - 1) + 2) *
      log(phi) * c[4] + c[5] * phi * log(phi) + c[5] * phi - c[5] + 2)) / 2
      "Second-order partial derivative of denominator w.r.t. to relative pressure
    and fourth coefficient of BET isotherm";
    Real ddB_dphi_dc5 = -(c[3] * phi^(c[4] - 1) * (c[4] * phi + phi - c[4])) / 2
      "Second-order partial derivative of denominator w.r.t. to relative pressure
    and fivth coefficient of BET isotherm";
    Real ddB_dphi_dphi = -(c[3] * c[4] * phi^(c[4] - 2) * (c[5] * ((c[4] + 1) *
      phi - c[4] + 1) + 2 * c[4] - 2)) / 2
      "Second-order partial derivative of denominator w.r.t. to relative pressure";

    Real dF_dp_adsorpt = dF_dphi * dphi_dp_adsorpt
      "Partial derivative of pre-factor w.r.t. pressure";
    Real dA_dp_adsorpt = dA_dphi * dphi_dp_adsorpt
      "Partial derivative of numerator w.r.t. pressure";
    Real dB_dp_adsorpt = dB_dphi * dphi_dp_adsorpt
      "Partial derivative of denominator w.r.t. pressure";

    Real dF_dT_adsorpt = dF_dc2 * dc_dT_adsorpt[2] +
      dF_dc3 * dc_dT_adsorpt[3] +
      dF_dphi * dphi_dT_adsorpt
      "Partial derivative of pre-factor w.r.t. temperature";
    Real dA_dT_adsorpt = dA_dc4 * dc_dT_adsorpt[4] +
      dA_dc5 * dc_dT_adsorpt[5] +
      dA_dphi * dphi_dT_adsorpt
      "Partial derivative of numerator w.r.t. temperature";
    Real dB_dT_adsorpt = dB_dc3 * dc_dT_adsorpt[3] +
      dB_dc4 * dc_dT_adsorpt[4] +
      dB_dc5 * dc_dT_adsorpt[5] +
      dB_dphi * dphi_dT_adsorpt
      "Partial derivative of denominator w.r.t. temperature";

    Real ddF_dphi_dT_adsorpt = ddF_dphi_dc2 * dc_dT_adsorpt[2] +
      ddF_dphi_dc3 * dc_dT_adsorpt[3] +
      ddF_dphi_dphi * dphi_dT_adsorpt
      "Second-order partial derivative of pre-factor w.r.t. relative pressure and 
    temperature";
    Real ddA_dphi_dT_adsorpt = ddA_dphi_dc4 * dc_dT_adsorpt[4] +
      ddA_dphi_dc5 * dc_dT_adsorpt[5] +
      ddA_dphi_dphi * dphi_dT_adsorpt
      "Second-order partial derivative of numerator w.r.t. relative pressure and 
    temperature";
    Real ddB_dphi_dT_adsorpt = ddB_dphi_dc3 * dc_dT_adsorpt[3] +
      ddB_dphi_dc4 * dc_dT_adsorpt[4] +
      ddB_dphi_dc5 * dc_dT_adsorpt[5] +
      ddB_dphi_dphi * dphi_dT_adsorpt
      "Second-order partial derivative of denominator w.r.t. relative pressure and 
    temperature";

    Real ddF_dp_adsorpt_dT_adsorpt = ddF_dphi_dT_adsorpt * dphi_dp_adsorpt +
      dF_dphi * ddphi_dp_adsorpt_dT_adsorpt
      "Second-order partial derivative of pre-factor w.r.t. pressure and temperature";
    Real ddA_dp_adsorpt_dT_adsorpt = ddA_dphi_dT_adsorpt * dphi_dp_adsorpt +
      dA_dphi * ddphi_dp_adsorpt_dT_adsorpt
      "Second-order partial derivative of numerator w.r.t. pressure and temperature";
    Real ddB_dp_adsorpt_dT_adsorpt = ddB_dphi_dT_adsorpt * dphi_dp_adsorpt +
      dB_dphi * ddphi_dp_adsorpt_dT_adsorpt
      "Second-order partial derivative of denominator w.r.t. pressure and temperature";

  algorithm
    ddx_adsorpt_dp_adsorpt_dT_adsorpt :=
      ((dF_dp_adsorpt/B) * dA_dT_adsorpt +
       (-A/B^2 * dF_dp_adsorpt) * dB_dT_adsorpt +
       (A/B) * ddF_dp_adsorpt_dT_adsorpt) +
      ((dA_dp_adsorpt/B) * dF_dT_adsorpt +
       (-F/B^2 * dA_dp_adsorpt) * dB_dT_adsorpt +
       (F/B) * ddA_dp_adsorpt_dT_adsorpt) +
      ((-A/B^2 * dB_dp_adsorpt) * dF_dT_adsorpt +
       (-F/B^2 * dB_dp_adsorpt) * dA_dT_adsorpt +
       (2*F * A/B^3 * dB_dp_adsorpt) * dB_dT_adsorpt +
       (-F * A/B^2) * ddB_dp_adsorpt_dT_adsorpt)
      "Calculation of the second-oder partial derivative of the equilibrium uptake 
     w.r.t. the equilibrium pressure and temperature";
  end ddx_dp_dT;

  redeclare function pi_pT
    "BET isotherm model: Reduced spreading pressure as function of pressure and temperature (numerical solution)"
    extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_pi_pT_num(
        redeclare final function func_x_pT =
        SorpLib.Media.Functions.SorptionEquilibria.PureComponents.BET.x_pT);
  end pi_pT;

  redeclare function p_piT
    "BET isotherm model: Pressure as function of reduced spreading pressure and temperature (numerical solution)"
    extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_p_piT_num(
        redeclare final function func_pi_pT =
        SorpLib.Media.Functions.SorptionEquilibria.PureComponents.BET.pi_pT);
  end p_piT;
  //
  // Annotations
  //
  annotation (Documentation(revisions="<html>
<ul>
  <li>
  November 1, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>", info="<html>
<p>
The BET isotherm model is a five-parameter model for calculating the  equilibrium
uptake <i>x_adsorpt</i> as a function of the equilibrium pressure <i>p_adsorpt</i>.
The BET isotherm model is suitable for type I-IV isotherms according to the IUPAC 
definition.
</p>

<h4>Main equations</h4>
<p>
The BET isotherm model has the following form:
</p>
<pre>
    x<sub>adsorpt</sub> = x<sub>mon</sub>(T<sub>adsorpt</sub>) * C(T<sub>adsorpt</sub>) * &phi;(T<sub>adsorpt</sub>) / (1 - &phi;(T<sub>adsorpt</sub>)) * A(T<sub>adsorpt</sub>) / B(T<sub>adsorpt</sub>);
</pre>
<p>
with
</p>
<pre>
    &phi;(T<sub>adsorpt</sub>) = p<sub>adsorpt</sub>/p<sub>sat</sub>(T<sub>adsorpt</sub>);
</pre>
<pre>
    A(T<sub>adsorpt</sub>) = 1 + (n * g(T<sub>adsorpt</sub>)/2 - n) * &phi;(T<sub>adsorpt</sub>) ^ (n-1) - (n * g(T<sub>adsorpt</sub>) - n + 1) * &phi;(T<sub>adsorpt</sub>) ^ n + (n *g(T<sub>adsorpt</sub>)/2) * &phi;(T<sub>adsorpt</sub>) ^ (n+1);
</pre>
<pre>
    B(T<sub>adsorpt</sub>) = 1 + (C(T<sub>adsorpt</sub>) - 1) * &phi;(T<sub>adsorpt</sub>) + (C(T<sub>adsorpt</sub>) * g(T<sub>adsorpt</sub>)/2 - C(T<sub>adsorpt</sub>)) * &phi;(T<sub>adsorpt</sub>) ^ n - (C(T<sub>adsorpt</sub>) * g(T<sub>adsorpt</sub>)/2) * &phi;(T<sub>adsorpt</sub>) ^ (n + 1);
</pre>
<p>
and
</p>
<pre>
    g(T<sub>adsorpt</sub>) = <strong>exp</strong>(Q<sub>cap</sub> / (R * T<sub>adsorpt</sub>));
</pre>
<p>
Herein, <i>x<sub>mon</sub>(T<sub>adsorpt</sub>)</i> is the monolayer uptake, 
<i>C(T<sub>adsorpt</sub>)</i> is the BET coefficient, <i>n</i> is the number of
layers that can be occupied, and <i>Q<sub>cap</sub> </i> is a measure for the 
extra enthalpy added to the enthalpy of vaporization at high layers due to capillary
forces. In the original BET equation, all parameters are independent of the temperature.
However, sometimes the two parameters <i>x<sub>mon</sub>(T<sub>adsorpt</sub>)</i>
and <i>C(T<sub>adsorpt</sub>)</i> are modeled as a function of the temperature,
with typical temperature dependencies may have the following forms:
</p>
<pre>
    x<sub>mon</sub>(T<sub>adsorpt</sub>) =  x<sub>mon,ref</sub> * <strong>exp</strong>(&Chi; * (1 - T<sub>adsorpt</sub>/T<sub>ref</sub>));
</pre>
<pre>
    C(T<sub>adsorpt</sub>) = C<sub>ref</sub> * <strong>exp</strong>(Q/(R * T<sub>ref</sub>) * (T<sub>ref</sub>/T<sub>adsorpt</sub> - 1));
</pre>
<p>
where <i>x<sub>mon,ref</sub></i> is the monolayer uptake at reference temperature 
<i>T<sub>ref</sub></i> and <i>C<sub>ref</sub></i> is the BET coefficient at reference 
temperature. The parameter <i>&Chi;</i> describes the change of the monolayer uptake 
with temperature and the parameter <i>Q</i> is a measure for some energy. These five 
parameters and the two parameters <i>n</i> and <i>Q<sub>cap</sub></i> can be used 
as fitting parameters.
</p>

<h4>Required parameter order in function input c[:]:</h4>
<ul>
  <li>
  c[1] = p<sub>sat</sub>(T<sub>adsorpt</sub>) in Pa
  </li>
  <li>
  c[2] = x<sub>mon</sub>(T<sub>adsorpt</sub>) in kg/kg
  </li>
  <li>
  c[3] = C(T<sub>adsorpt</sub>) in -
  </li>
  <li>
  c[4] = n in -
  </li>
  <li>
  c[5] = g(T<sub>adsorpt</sub>) in -
  </li>
</ul>

<h4>Assumptions and limitations</h4>
<ul>
  <li>
  All adsorption sites are energetically equivalent.
  </li>
  <li>
  All adsorption sites can be occupied.
  </li>
  <li>
  No interactions occur between the adsorbent molecules.
  </li>
  <li>
  No limit exists for the number of layers.
  </li>
  <li>
  Heat of adsorption of all layers except the first layer equals the heat of vaporization.
  </li>
</ul>

<h4>Typical use</h4>
<p>
The isotherm model is used for type I-V isotherms according to the IUPAC definition.
Besides, the BET isotherm can used to calculate the surface area of the adsorbent.
</p>

<h4>Example</h4>
<p>
The following figure shows the BET isotherm model for different parameter sets.
In the upper sub-figure, the equilibrium pressure changes with time, while the 
equilibrium temperature remains constant; in the lower sub-figure, the equilibrium 
temperature changes with time, while the equilibrium pressure remains constant. 
<br/><br/>
</p>
<img src=\"Modelica://SorpLib/Resources/doc/media_functions_equilibria_pure_bet.png\" alt=\"media_functions_equilibria_pure_bet.png\">

<h4>References</h4>
<ul>
  <li>
  Do, D. D. (1998). Adsorption Analysis: Equilibria and Kinetics, 1st Edition, ISBN 978-1-86094-130-6, Imperial College Press.
  </li>
</ul>
</html>"));
end BET;
