within SorpLib.Media.Functions.SorptionEquilibria.PureComponents;
package BiLangmuir "Package containing all functions regarding the Bi-Langmuir isotherm"
extends
  SorpLib.Media.Functions.SorptionEquilibria.Interfaces.PartialPureComponents;

  redeclare final function extends x_pT
    "Bi-Langmuir isotherm model: Uptake as function of pressure and temperature"
  algorithm
    x_adsorpt :=
      SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Langmuir.x_pT(
      p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt,
      c=c[1:2],
      p_adsorpt_lb_start=p_adsorpt_lb_start,
      p_adsorpt_ub_start=p_adsorpt_ub_start,
      tolerance=tolerance) +
      SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Langmuir.x_pT(
      p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt,
      c=c[3:4],
      p_adsorpt_lb_start=p_adsorpt_lb_start,
      p_adsorpt_ub_start=p_adsorpt_ub_start,
      tolerance=tolerance)
      "Calculation of the equilibrium uptake of the adsorpt phase";

    //
    // Annotations
    //
    annotation (Inline=false,
  InlineAfterIndexReduction=false,
  LateInline=true,
  inverse(p_adsorpt=SorpLib.Media.Functions.SorptionEquilibria.PureComponents.BiLangmuir.p_xT(x_adsorpt, T_adsorpt, c, p_adsorpt_lb_start, p_adsorpt_ub_start, tolerance)));
  end x_pT;

  redeclare function p_xT
    "Bi-Langmuir isotherm model: Pressure as function of uptake and temperature (numerical solution)"
    extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_p_xT_num(
        redeclare final function func_x_pT =
        SorpLib.Media.Functions.SorptionEquilibria.PureComponents.BiLangmuir.x_pT);

    //
    // Annotations
    //
    annotation (Inline=false,
  InlineAfterIndexReduction=false,
  LateInline=true,
  inverse(x_adsorpt=SorpLib.Media.Functions.SorptionEquilibria.PureComponents.BiLangmuir.x_pT(p_adsorpt, T_adsorpt, c, p_adsorpt_lb_start, p_adsorpt_ub_start, tolerance)));
  end p_xT;

  redeclare final function extends dx_dp
    "Bi-Langmuir isotherm model: Partial derivative of uptake w.r.t. pressure at constant temperature"
  algorithm
    dx_adsorpt_dp_adsorpt :=
      SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Langmuir.dx_dp(
      p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt,
      c=c[1:2]) +
      SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Langmuir.dx_dp(
      p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt,
      c=c[3:4])
      "Calculation of the partial derivative of the equilibrium uptake w.r.t. the
     equilibrium pressure at constant temperature";
  end dx_dp;

  redeclare final function extends dx_dT
    "Bi-Langmuir isotherm model: Partial derivative of uptake w.r.t. temperature at constant pressure"
  algorithm
    dx_adsorpt_dT_adsorpt :=
      SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Langmuir.dx_dT(
      p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt,
      c=c[1:2],
      dc_dT_adsorpt=dc_dT_adsorpt[1:2]) +
      SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Langmuir.dx_dT(
      p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt,
      c=c[3:4],
      dc_dT_adsorpt=dc_dT_adsorpt[3:4])
      "Calculation of the partial derivative of the equilibrium uptake w.r.t. the
     equilibrium temperature at constant pressure";
  end dx_dT;

  redeclare final function extends ddx_dp_dp
    "Bi-Langmuir isotherm model: Second-order partial derivative of uptake w.r.t. pressure at constant temperature"
  algorithm
    ddx_adsorpt_dp_adsorpt_dp_adsorpt :=
      SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Langmuir.ddx_dp_dp(
      p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt,
      c=c[1:2]) +
      SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Langmuir.ddx_dp_dp(
      p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt,
      c=c[3:4])
      "Calculation of the second-order partial derivative of the equilibrium uptake 
    w.r.t. the equilibrium pressure at constant temperature";
  end ddx_dp_dp;

  redeclare final function extends ddx_dT_dT
    "Bi-Langmuir isotherm model: Second-order partial derivative of uptake w.r.t. temperature at constant pressure"
  algorithm
    ddx_adsorpt_dT_adsorpt_dT_adsorpt :=
      SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Langmuir.ddx_dT_dT(
      p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt,
      c=c[1:2],
      dc_dT_adsorpt=dc_dT_adsorpt[1:2],
      ddc_dT_adsorpt_dT_adsorpt=ddc_dT_adsorpt_dT_adsorpt[1:2]) +
      SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Langmuir.ddx_dT_dT(
      p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt,
      c=c[3:4],
      dc_dT_adsorpt=dc_dT_adsorpt[3:4],
      ddc_dT_adsorpt_dT_adsorpt=ddc_dT_adsorpt_dT_adsorpt[3:4])
      "Calculation of the second-order partial derivative of the equilibrium uptake 
    w.r.t. the equilibrium temperature at constant pressure";
  end ddx_dT_dT;

  redeclare final function extends ddx_dp_dT
    "Bi-Langmuir isotherm model: Second-order partial derivative of uptake w.r.t. pressure and temperature"
  algorithm
    ddx_adsorpt_dp_adsorpt_dT_adsorpt :=
      SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Langmuir.ddx_dp_dT(
      p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt,
      c=c[1:2],
      dc_dT_adsorpt=dc_dT_adsorpt[1:2]) +
      SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Langmuir.ddx_dp_dT(
      p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt,
      c=c[3:4],
      dc_dT_adsorpt=dc_dT_adsorpt[3:4])
      "Calculation of the second-oder partial derivative of the equilibrium uptake 
     w.r.t. the equilibrium pressure and temperature";
  end ddx_dp_dT;

  redeclare final function extends pi_pT
    "Bi-Langmuir isotherm model: Pressure as function of reduced spreading pressure and temperature"
  algorithm
    pi :=
      SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Langmuir.pi_pT(
      M_adsorptive=M_adsorptive,
      p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt,
      c=c[1:2],
      integral_pi_lb=integral_pi_lb,
      tolerance=tolerance) +
      SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Langmuir.pi_pT(
      M_adsorptive=M_adsorptive,
      p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt,
      c=c[3:4],
      integral_pi_lb=integral_pi_lb,
      tolerance=tolerance)
      "Calculation of the reduced spreading pressure";
  end pi_pT;

  redeclare function p_piT
    "Bi-Langmuir isotherm model: Pressure as function of reduced spreading pressure and temperature (numerical solution)"
    extends
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_p_piT_num(
        redeclare final function func_pi_pT =
        SorpLib.Media.Functions.SorptionEquilibria.PureComponents.BiLangmuir.pi_pT);
  end p_piT;
  //
  // Annotations
  //
annotation (Documentation(revisions="<html>
<ul>
  <li>
  November 1, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>", info="<html>
<p>
The Bi-Langmuir isotherm model is a four-parameter model for calculating the equilibrium
uptake <i>x_adsorpt</i> as a function of the equilibrium pressure <i>p_adsorpt</i>.
</p>

<h4>Main equations</h4>
<p>
The Bi-Langmuir isotherm model has the following form:
</p>
<pre>
    x<sub>adsorpt</sub> = x<sub>sat,1</sub>(T<sub>adsorpt</sub>) * (b<sub>1</sub>(T<sub>adsorpt</sub>) * p<sub>adsorpt</sub>) / (1 + b<sub>1</sub>(T<sub>adsorpt</sub>) * p<sub>adsorpt</sub>) + x<sub>sat,2</sub>(T<sub>adsorpt</sub>) * (b<sub>2</sub>(T<sub>adsorpt</sub>) * p<sub>adsorpt</sub>) / (1 + b<sub>2</sub>(T<sub>adsorpt</sub>) * p<sub>adsorpt</sub>);
</pre>
<p>
where <i>x<sub>sat,1</sub>(T<sub>adsorpt</sub>)</i> is the saturation uptake of 
adsorption site 1, <i>x<sub>sat,2</sub>(T<sub>adsorpt</sub>)</i> is the saturation 
uptake of adsorption site 2, <i>b<sub>1</sub>(T<sub>adsorpt</sub>)</i> is the 
Langmuir coefficient of adsorption site 1, and <i>b<sub>2</sub>(T<sub>adsorpt</sub>)</i> 
is the Langmuir coefficient of adsorption site 2. Typical temperature dependencies 
may have the following forms:
</p>
<pre>
    x<sub>sat,<i>i</i></sub>(T<sub>adsorpt</sub>) = a<sub>0,<i>i</i></sub> + a<sub>1,<i>i</i></sub>/T<sub>adsorpt</sub>;
</pre>
<pre>
    b<sub><i>i</i></sub>(T<sub>adsorpt</sub>) = b<sub>0,<i>i</i></sub> * <strong>exp</strong>(-&Delta;H<sub>ads,<i>i</i></sub>/(R * T<sub>adsorpt</sub>));
</pre>
<p>
where <i>a<sub>0,<i>i</i></sub></i>, <i>a<sub>1,<i>i</i></sub></i>, <i>b<sub>0</sub></i>, 
and <i>&Delta;H<sub>ads,<i>i</i></sub></i> are fiting parameters. The parameter
<i>&Delta;H<sub>ads,<i>i</i></sub></i> is the isosteric adsorption heat, which is
invariant with the surface uptake.
</p>

<h4>Required parameter order in function input c[:]:</h4>
<ul>
  <li>
  c[1] = x<sub>sat,1</sub>(T<sub>adsorpt</sub>) in kg/kg
  </li>
  <li>
  c[2] = b<sub>1</sub>(T<sub>adsorpt</sub>) in 1/Pa
  </li>
  <li>
  c[3] = x<sub>sat,2</sub>(T<sub>adsorpt</sub>) in kg/kg
  </li>
  <li>
  c[4] = b<sub>2</sub>(T<sub>adsorpt</sub>) in 1/Pa
  </li>
</ul>

<h4>Assumptions and limitations</h4>
<ul>
  <li>
  All adsorption sites can be occupied.
  </li>
  <li>
  No interactions occur between the adsorbent molecules.
  </li>
  <li>
  The adsorbent surface is covered monomolecularly.
  </li>
</ul>

<h4>Example</h4>
<p>
The following figure shows the Bi-Langmuir isotherm model for different parameter sets.
In the upper sub-figure, the equilibrium pressure changes with time, while the 
equilibrium temperature remains constant; in the lower sub-figure, the equilibrium 
temperature changes with time, while the equilibrium pressure remains constant. 
<br/><br/>
</p>
<img src=\"Modelica://SorpLib/Resources/doc/media_functions_equilibria_pure_bi_langmuir.png\" alt=\"media_functions_equilibria_pure_bi_langmuir.png\">

<h4>References</h4>
<ul>
  <li>
  Do, D. D. (1998). Adsorption Analysis: Equilibria and Kinetics, 1st Edition, ISBN 978-1-86094-130-6, Imperial College Press.
  </li>
</ul>
</html>"));
end BiLangmuir;
