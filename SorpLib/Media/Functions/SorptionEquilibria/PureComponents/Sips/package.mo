within SorpLib.Media.Functions.SorptionEquilibria.PureComponents;
package Sips "Package containing all functions regarding the Sips isotherm"
extends
  SorpLib.Media.Functions.SorptionEquilibria.Interfaces.PartialPureComponents;

  redeclare final function extends x_pT
    "Sips isotherm model: Uptake as function of pressure and temperature"
  algorithm
    x_adsorpt := c[1] * (c[2]*p_adsorpt)^(1/c[3]) / (1 + (c[2]*p_adsorpt)^(1/c[3]))
      "Calculation of the equilibrium uptake of the adsorpt phase";

    //
    // Annotations
    //
    annotation (Inline=false,
  InlineAfterIndexReduction=false,
  LateInline=true,
  inverse(p_adsorpt=SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Sips.p_xT(x_adsorpt, T_adsorpt, c, p_adsorpt_lb_start, p_adsorpt_ub_start, tolerance)));
  end x_pT;

  redeclare final function extends p_xT
    "Sips isotherm model: Pressure as function of uptake and temperature"
  algorithm
    p_adsorpt := 1/c[2] * (x_adsorpt / (c[1] - x_adsorpt)) ^ c[3]
      "Calculation of the equilibrium pressure of the adsorpt phase";

    //
    // Annotations
    //
    annotation (Inline=false,
  InlineAfterIndexReduction=false,
  LateInline=true,
  inverse(x_adsorpt=SorpLib.Media.Functions.SorptionEquilibria.PureComponents.Sips.x_pT(p_adsorpt, T_adsorpt, c, p_adsorpt_lb_start, p_adsorpt_ub_start, tolerance)));
  end p_xT;

  redeclare final function extends dx_dp
    "Sips isotherm model: Partial derivative of uptake w.r.t. pressure at constant temperature"
  algorithm
    dx_adsorpt_dp_adsorpt := c[1] * (c[2]*p_adsorpt)^(1/c[3]) /
      (c[3] * p_adsorpt * (1 + (c[2]*p_adsorpt)^(1/c[3]))^2)
      "Calculation of the partial derivative of the equilibrium uptake w.r.t. the
     equilibrium pressure at constant temperature";
  end dx_dp;

  redeclare final function extends dx_dT
    "Sips isotherm model: Partial derivative of uptake w.r.t. temperature at constant pressure"

    //
    // Definition of variables
    //
protected
    Real dx_adsorpt_dc1 = (c[2]*p_adsorpt)^(1/c[3]) /
      (1 + (c[2]*p_adsorpt)^(1/c[3]))
      "Derivative of uptake w.r.t. to first coefficient of Sips isotherm";
    Real dx_adsorpt_dc2 = c[1] * (c[2]*p_adsorpt)^(1/c[3]) /
      (c[2] * c[3] * (1 + (c[2]*p_adsorpt)^(1/c[3]))^2)
      "Derivative of uptake w.r.t. to second coefficient of Sips isotherm";
    Real dx_adsorpt_dc3 = -c[1] * (c[2]*p_adsorpt)^(1/c[3]) * log(c[2]*p_adsorpt)/
      (c[3]^2 * (1 + (c[2]*p_adsorpt)^(1/c[3]))^2)
      "Derivative of uptake w.r.t. to third coefficient of Sips isotherm";

  algorithm
    dx_adsorpt_dT_adsorpt :=
      dx_adsorpt_dc1*dc_dT_adsorpt[1] +
      dx_adsorpt_dc2*dc_dT_adsorpt[2] +
      dx_adsorpt_dc3*dc_dT_adsorpt[3]
      "Calculation of the partial derivative of the equilibrium uptake w.r.t. the
     equilibrium temperature at constant pressure";
  end dx_dT;

  redeclare final function extends ddx_dp_dp
    "Sips isotherm model: Second-order partial derivative of uptake w.r.t. pressure at constant temperature"
  algorithm
    ddx_adsorpt_dp_adsorpt_dp_adsorpt := -(c[1] * (c[2] * p_adsorpt)^(1 / c[3]) *
      ((c[3] + 1) * (c[2] * p_adsorpt)^(1 / c[3]) + c[3] - 1)) /
      (c[3]^2 * p_adsorpt^2 * ((c[2] * p_adsorpt)^(1 / c[3]) + 1)^3)
      "Calculation of the second-order partial derivative of the equilibrium uptake 
    w.r.t. the equilibrium pressure at constant temperature";
  end ddx_dp_dp;

  redeclare final function extends ddx_dT_dT
    "Sips isotherm model: Second-order partial derivative of uptake w.r.t. temperature at constant pressure"

    //
    // Definition of variables
    //
protected
    Real dx_adsorpt_dc1 = (c[2]*p_adsorpt)^(1/c[3]) /
      (1 + (c[2]*p_adsorpt)^(1/c[3]))
      "Derivative of uptake w.r.t. to first coefficient of Sips isotherm";
    Real dx_adsorpt_dc2 = c[1] * (c[2]*p_adsorpt)^(1/c[3]) /
      (c[2] * c[3] * (1 + (c[2]*p_adsorpt)^(1/c[3]))^2)
      "Derivative of uptake w.r.t. to second coefficient of Sips isotherm";
    Real dx_adsorpt_dc3 = -c[1] * (c[2]*p_adsorpt)^(1/c[3]) * log(c[2]*p_adsorpt)/
      (c[3]^2 * (1 + (c[2]*p_adsorpt)^(1/c[3]))^2)
      "Derivative of uptake w.r.t. to third coefficient of Sips isotherm";

    Real ddx_adsorpt_dc1_dc2 = (c[2]*p_adsorpt)^(1/c[3]) /
      (c[3] * c[2] * (1 + (c[2]*p_adsorpt)^(1/c[3]))^2)
      "Second-order partial derivative of uptake w.r.t. to first and second 
    coefficient of Sips isotherm";
    Real ddx_adsorpt_dc1_dc3 = -(c[2]*p_adsorpt)^(1/c[3]) * log(c[2]*p_adsorpt) /
      ((1 + (c[2]*p_adsorpt)^(1/c[3]))^2 * c[3]^2)
      "Second-order partial derivative of uptake w.r.t. to first and third 
    coefficient of Sips isotherm";

    Real ddx_adsorpt_dc2_dc2 = -c[1] * (c[2]*p_adsorpt)^(1/c[3]) * ((c[3] + 1) *
      (c[2]*p_adsorpt)^(1/c[3]) + c[3] - 1) /
      (c[2]^2 * c[3]^2 * (1 + (c[2]*p_adsorpt)^(1/c[3]))^3)
      "Second-order partial derivative of uptake w.r.t. to second coefficient of 
    Sips isotherm";
    Real ddx_adsorpt_dc2_dc3 = -(c[1] * (c[2] * p_adsorpt)^(1 / c[3]) *
      (((c[2] * p_adsorpt)^(1 / c[3]) + 1) * c[3] - (c[2] * p_adsorpt)^(1 / c[3]) *
      log(c[2] * p_adsorpt) + log(c[2] * p_adsorpt))) /
      (c[2] * ((c[2] * p_adsorpt)^(1 / c[3]) + 1)^3 * c[3]^3)
      "Second-order partial derivative of uptake w.r.t. to second and third 
    coefficient of Sips isotherm";

    Real ddx_adsorpt_dc3_dc3 = (c[1] * (c[2] * p_adsorpt)^(1 / c[3]) * log(c[2] *
      p_adsorpt) * ((2 * (c[2] * p_adsorpt)^(1 / c[3]) + 2) *
      c[3] - (c[2] * p_adsorpt)^(1 / c[3]) * log(c[2] * p_adsorpt) +
      log(c[2] * p_adsorpt))) / (((c[2] * p_adsorpt)^(1 / c[3]) + 1)^3 * c[3]^4)
      "Second-order partial derivative of uptake w.r.t. to third coefficient of 
    Sips isotherm";

    Real ddx_adsorpt_dc1_dT_adsorpt = ddx_adsorpt_dc1_dc2*dc_dT_adsorpt[2] +
      ddx_adsorpt_dc1_dc3*dc_dT_adsorpt[3]
      "Second-order partial derivative of uptake w.r.t. to first coefficient of 
    Sips isotherm and temperature";
    Real ddx_adsorpt_dc2_dT_adsorpt = ddx_adsorpt_dc1_dc2*dc_dT_adsorpt[1] +
      ddx_adsorpt_dc2_dc2*dc_dT_adsorpt[2] +
      ddx_adsorpt_dc2_dc3*dc_dT_adsorpt[3]
      "Second-order partial derivative of uptake w.r.t. to second coefficient of 
    Sips isotherm and temperature";
    Real ddx_adsorpt_dc3_dT_adsorpt = ddx_adsorpt_dc1_dc3*dc_dT_adsorpt[1] +
      ddx_adsorpt_dc2_dc3*dc_dT_adsorpt[2] +
      ddx_adsorpt_dc3_dc3*dc_dT_adsorpt[3]
      "Second-order partial derivative of uptake w.r.t. to third coefficient of 
    Sips isotherm and temperature";

  algorithm
    ddx_adsorpt_dT_adsorpt_dT_adsorpt :=
      (ddx_adsorpt_dc1_dT_adsorpt*dc_dT_adsorpt[1] +
       dx_adsorpt_dc1*ddc_dT_adsorpt_dT_adsorpt[1]) +
      (ddx_adsorpt_dc2_dT_adsorpt*dc_dT_adsorpt[2] +
       dx_adsorpt_dc2*ddc_dT_adsorpt_dT_adsorpt[2]) +
      (ddx_adsorpt_dc3_dT_adsorpt*dc_dT_adsorpt[3] +
       dx_adsorpt_dc3*ddc_dT_adsorpt_dT_adsorpt[3])
      "Calculation of the second-order partial derivative of the equilibrium uptake 
    w.r.t. the equilibrium temperature at constant pressure";
  end ddx_dT_dT;

  redeclare final function extends ddx_dp_dT
    "Sips isotherm model: Second-order partial derivative of uptake w.r.t. pressure and temperature"

    //
    // Definition of variables
    //
protected
    Real ddx_adsorpt_dp_adsorpt_dc1 = (c[2]*p_adsorpt)^(1/c[3]) /
      ((1 + (c[2]*p_adsorpt)^(1/c[3]))^2 * c[3] * p_adsorpt)
      "Second-order partial derivative of uptake w.r.t. to equilibrium pressure and
    first coefficient of Sips isotherm";
    Real ddx_adsorpt_dp_adsorpt_dc2 = -c[1] * (c[2]*p_adsorpt)^(1/c[3]) *
      ((c[2]*p_adsorpt)^(1/c[3]) - 1) /
      (c[2] * c[3]^2 * p_adsorpt * (1 + (c[2]*p_adsorpt)^(1/c[3]))^3)
      "Second-order partial derivative of uptake w.r.t. to equilibrium pressure and
    second coefficient of Sips isotherm";
    Real ddx_adsorpt_dp_adsorpt_dc3 = c[1] * (c[2]*p_adsorpt)^(1/c[3]) *
      (((c[2]*p_adsorpt)^(1/c[3]) - 1) * log(c[2]*p_adsorpt) - c[3] *
      (c[2]*p_adsorpt)^(1/c[3]) - c[3]) /
      (c[3]^3 * p_adsorpt * (1 + (c[2]*p_adsorpt)^(1/c[3]))^3)
      "Second-order partial derivative of uptake w.r.t. to equilibrium pressure and
    third coefficient of Sips isotherm";

  algorithm
    ddx_adsorpt_dp_adsorpt_dT_adsorpt :=
      ddx_adsorpt_dp_adsorpt_dc1*dc_dT_adsorpt[1] +
      ddx_adsorpt_dp_adsorpt_dc2*dc_dT_adsorpt[2] +
      ddx_adsorpt_dp_adsorpt_dc3*dc_dT_adsorpt[3]
      "Calculation of the second-oder partial derivative of the equilibrium uptake 
     w.r.t. the equilibrium pressure and temperature";
  end ddx_dp_dT;

  redeclare final function extends pi_pT
    "Sips isotherm model: Reduced spreading pressure as function of pressure and temperature"
  algorithm
    pi := 1/M_adsorptive * (c[1]*c[3] * log(1 + (c[2]*p_adsorpt)^(1/c[3])))
      "Calculation of the reduced spreading pressure";
  end pi_pT;

  redeclare final function extends p_piT
    "Sips isotherm model: Pressure as function of reduced spreading pressure and temperature"
  algorithm
    p_adsorpt := (exp(M_adsorptive * pi / (c[1]*c[3])) - 1) ^ c[3] / c[2]
      "Calculation of the equilibrium pressure";
  end p_piT;
  //
  // Annotations
  //
annotation (Documentation(revisions="<html>
<ul>
  <li>
  November 1, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>", info="<html>
<p>
The Sips isotherm model is a three-parameter model for calculating the  equilibrium
uptake <i>x_adsorpt</i> as a function of the equilibrium pressure <i>p_adsorpt</i>.
The Sips isotherm model is suitable for type I and II isotherms according to the 
IUPAC definition.
</p>

<h4>Main equations</h4>
<p>
The Sips isotherm model has the following form:
</p>
<pre>
    x<sub>adsorpt</sub> = x<sub>sat</sub>(T<sub>adsorpt</sub>) * (b(T<sub>adsorpt</sub>) * p<sub>adsorpt</sub>) ^ (1/n(T<sub>adsorpt</sub>)) / (1 + (b(T<sub>adsorpt</sub>) * p<sub>adsorpt</sub>) ^ (1/n(T<sub>adsorpt</sub>)));
</pre>
<p>
where <i>x<sub>sat</sub>(T<sub>adsorpt</sub>)</i> is the saturation uptake, 
<i>b(T<sub>adsorpt</sub>)</i> is the Sips coefficient, and <i>n(T<sub>adsorpt</sub>)</i>
is the Sips exponent. Typical temperature dependencies may have the following forms:
</p>
</p>
<pre>
    x<sub>sat</sub>(T<sub>adsorpt</sub>) =  x<sub>ref</sub> * <strong>exp</strong>(&Chi; * (1 - T<sub>adsorpt</sub>/T<sub>ref</sub>));
</pre>
<pre>
    b(T<sub>adsorpt</sub>) = b<sub>ref</sub> * <strong>exp</strong>(Q/(R * T<sub>ref</sub>) * (T<sub>ref</sub>/T<sub>adsorpt</sub> - 1));
</pre>
<pre>
    n(T<sub>adsorpt</sub>) = (1/n<sub>ref</sub> + &alpha; * (1 - T<sub>ref</sub>/T<sub>adsorpt</sub>)) ^ (-1);
</pre>
<p>
where <i>x<sub>ref</sub></i> is the saturation uptake at reference temperature 
<i>T<sub>ref</sub></i>, <i>b<sub>ref</sub></i> is the Sips coefficient at reference 
temperature, and <i>n<sub>ref</sub></i> is the Sips exponent at reference temperature. 
The parameter <i>Q</i> is a measure for the isosteric adsorption enthalpy at a fractional
loading of <i>x<sub>adsorpt</sub>/x<sub>sat</sub>(T<sub>adsorpt</sub>) = 0.5</i>, the 
parameter <i>&Chi;</i> describes the change of the saturation uptake with temperature, 
and the parameter <i>&alpha;</i> describes the change of the Sips exponent with 
temperature. All seven parameters can be used as fitting parameters.
<br/><br/>
Note that the Sips exponent <i>n(T<sub>adsorpt</sub>)</i> is typically greater than
unity. For <i>n(T<sub>adsorpt</sub>) = 1</i>, the Sips isotherm becomes the
Langmuir isotherm. Hence, the Sips exponent <i>n(T<sub>adsorpt</sub>)</i> can be 
interpreted as a parameter describing the heterogeneity of the adsorption system. 
</p>

<h4>Required parameter order in function input c[:]:</h4>
<ul>
  <li>
  c[1] = x<sub>sat</sub>(T<sub>adsorpt</sub>) in kg/kg
  </li>
  <li>
  c[2] = b(T<sub>adsorpt</sub>) in 1/Pa
  </li>
  <li>
  c[3] = n(T<sub>adsorpt</sub>) in -
  </li>
</ul>

<h4>Assumptions and limitations</h4>
<ul>
  <li>
  No proper Henry law behavior.
  </li>
</ul>

<h4>Typical use</h4>
<p>
The isotherm model is used for type I an II isotherms according to the IUPAC definition.
</p>

<h4>Example</h4>
<p>
The following figure shows the Sips isotherm model for different parameter sets.
In the upper sub-figure, the equilibrium pressure changes with time, while the 
equilibrium temperature remains constant; in the lower sub-figure, the equilibrium 
temperature changes with time, while the equilibrium pressure remains constant. 
<br/><br/>
</p>
<img src=\"Modelica://SorpLib/Resources/doc/media_functions_equilibria_pure_sips.png\" alt=\"media_functions_equilibria_pure_sips.png\">

<h4>References</h4>
<ul>
  <li>
  Do, D. D. (1998). Adsorption Analysis: Equilibria and Kinetics, 1st Edition, ISBN 978-1-86094-130-6, Imperial College Press.
  </li>
</ul>
</html>"));
end Sips;
