within SorpLib.Media.Functions.SorptionEquilibria.Utilities;
function A_ppsT "Calculates the molar adsorption potential"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.Pressure p_adsorpt
    "Equilibrium pressure of the adsorpt phase"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.Pressure p_sat
    "Saturation pressure at the equilibrium temperature"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.Temperature T_adsorpt
    "Equilibrium temperature of the adsorpt phase"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of outputs
  //
  output SorpLib.Units.MolarAdsorptionPotential A "Molar adsorption potential"
    annotation (Dialog(
      tab="General",
      group="Outputs",
      enable=false));

algorithm
  A := Modelica.Constants.R * T_adsorpt * log(p_sat/p_adsorpt)
    "Molar adsorption potential";

  //
  // Annotations
  //
  annotation (Inline=false,
InlineAfterIndexReduction=false,
LateInline=true,
inverse(p_adsorpt=p_ApsT(A,p_sat,T_adsorpt)),
Documentation(info="<html>
<p>
This function calculates the molar adsorption potential <i>A</i>. The molar adsorption
potential <i>A</i> is an important quantity for isotherm models following the model
of Dubinin.
</p>

<h4>Main equations</h4>
<p>
The molar adsorption potential <i>A</i> is defined as follows:
</p>
<pre>
    A(T<sub>adsorpt</sub>) = R * T<sub>adsorpt</sub> * <strong>ln</strong>(p<sub>sat</sub>(T<sub>adsorpt</sub>) / p<sub>adsorpt</sub>);
</pre>

<h4>Typical use</h4>
<p>
The molar adsorption potential <i>A</i> is used to calculate the characteristic
curve of isotherm models following the model of Dubinin.
</p>

<h4>References</h4>
<ul>
  <li>
  Do, D. D. (1998). Adsorption Analysis: Equilibria and Kinetics, 1st Edition, ISBN 978-1-86094-130-6, Imperial College Press.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  November 2, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end A_ppsT;
