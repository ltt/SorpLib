within SorpLib.Media.Functions.SorptionEquilibria.Utilities;
function ddA_dp_dT
  "Calculates the second-order partial derivative of molar adsorption potential w.r.t. equilibrium pressure and temperature"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.Pressure p_adsorpt
    "Equilibrium pressure of the adsorpt phase"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.Pressure p_sat
    "Saturation pressure at the equilibrium temperature"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.Temperature T_adsorpt
    "Equilibrium temperature of the adsorpt phase"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of outputs
  //
  output SorpLib.Units.DerMolarAdsorptionPotentialByPressureTemperature ddA_dp_adsorpt_dT_adsorpt
    "Second-order partial derivative of the molar adsorption potential w.r.t. 
    equilibrium pressure and temperature"
    annotation (Dialog(
      tab="General",
      group="Outputs",
      enable=false));

algorithm
  ddA_dp_adsorpt_dT_adsorpt := -Modelica.Constants.R / p_adsorpt
    "Second-order partial derivative of the molar adsorption potential w.r.t. 
    equilibrium pressure and temperature";

  //
  // Annotations
  //
  annotation (Inline=true,
Documentation(info="<html>
<p>
This function calculates the second-prder partial derivative of the molar adsorption 
potential with respect to the equilibrium pressure and temperature 
<i>ddA_dp_adsorpt_dT_adsorpt</i>. For more information about the molar adsorption 
potential <i>A</i>, check the documentation of the function
<a href=\"Modelica://SorpLib.Media.Functions.SorptionEquilibria.Utilities.A_ppsT\">SorpLib.Media.Functions.SorptionEquilibria.Utilities.A_ppsT</a>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 2, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end ddA_dp_dT;
