within SorpLib.Media.Functions.SorptionEquilibria.Utilities;
function ddA_dT_dT
  "Calculates the second-order partial derivative of molar adsorption potential w.r.t. equilibrium temperature at constant pressure"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.Pressure p_adsorpt
    "Equilibrium pressure of the adsorpt phase"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.Pressure p_sat
    "Saturation pressure at the equilibrium temperature"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.Temperature T_adsorpt
    "Equilibrium temperature of the adsorpt phase"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real dp_sat_dT_adsorpt(unit="Pa/K")
    "Partial derivative of saturation pressure at the equilibrium temperature 
    w.r.t. temperature"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real ddp_sat_dT_adsorpt_dT_adsorpt(unit="Pa/K2")
    "Second-order partial derivative of saturation pressure at the equilibrium 
    temperature w.r.t. temperature"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of outputs
  //
  output SorpLib.Units.DerMolarAdsorptionPotentialByTemperatureTemperature ddA_dT_adsorpt_dT_adsorpt
    "Second-order partial derivative of the molar adsorption potential w.r.t. 
    equilibrium temperature at constant pressure"
    annotation (Dialog(
      tab="General",
      group="Outputs",
      enable=false));

algorithm
  ddA_dT_adsorpt_dT_adsorpt := Modelica.Constants.R * (
    1 / p_sat * dp_sat_dT_adsorpt +
    (1 / p_sat * dp_sat_dT_adsorpt -
    T_adsorpt / p_sat^2 * dp_sat_dT_adsorpt^2 +
    T_adsorpt / p_sat * ddp_sat_dT_adsorpt_dT_adsorpt))
    "Second-order partial derivative of the molar adsorption potential w.r.t. 
    equilibrium temperature at constant pressure";

  //
  // Annotations
  //
  annotation (Inline=true,
Documentation(info="<html>
<p>
This function calculates the second-order partial derivative of the molar adsorption 
potential with respect to the equilibrium temperature <i>ddA_dT_adsorpt_dT_adsorpt</i> 
at constant pressure. For more information about the molar adsorption potential 
<i>A</i>, check the documentation of the function
<a href=\"Modelica://SorpLib.Media.Functions.SorptionEquilibria.Utilities.A_ppsT\">SorpLib.Media.Functions.SorptionEquilibria.Utilities.A_ppsT</a>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 2, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end ddA_dT_dT;
