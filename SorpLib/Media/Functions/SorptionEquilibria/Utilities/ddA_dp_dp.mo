within SorpLib.Media.Functions.SorptionEquilibria.Utilities;
function ddA_dp_dp
  "Calculates the second-order partial derivative of molar adsorption potential w.r.t. equilibrium pressure at constant temperature"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.Pressure p_adsorpt
    "Equilibrium pressure of the adsorpt phase"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.Pressure p_sat
    "Saturation pressure at the equilibrium temperature"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.Temperature T_adsorpt
    "Equilibrium temperature of the adsorpt phase"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of outputs
  //
  output
    SorpLib.Units.DerMolarAdsorptionPotentialByPressurePressure dA_dp_adsorpt
    "Second-order partial derivative of the molar adsorption potential w.r.t. 
    equilibrium pressure at constant temperature"
    annotation (Dialog(
      tab="General",
      group="Outputs",
      enable=false));

algorithm
  dA_dp_adsorpt := Modelica.Constants.R * T_adsorpt / p_adsorpt^2
    "Second-order partial derivative of the molar adsorption potential w.r.t. 
    equilibrium pressure at constant temperature";

  //
  // Annotations
  //
  annotation (Inline=true,
Documentation(info="<html>
<p>
This function calculates the second-orderpartial derivative of the molar adsorption 
potential with respect to the equilibrium pressure <i>ddA_dp_adsorpt_dp_adsorpt</i> 
at constant temperature. For more information about the molar adsorption potential 
<i>A</i>, check the documentation of the function
<a href=\"Modelica://SorpLib.Media.Functions.SorptionEquilibria.Utilities.A_ppsT\">SorpLib.Media.Functions.SorptionEquilibria.Utilities.A_ppsT</a>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 2, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end ddA_dp_dp;
