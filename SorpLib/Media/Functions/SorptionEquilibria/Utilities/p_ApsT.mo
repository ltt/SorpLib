within SorpLib.Media.Functions.SorptionEquilibria.Utilities;
function p_ApsT
  "Calculates the inverse of the molar adsorption potential"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input SorpLib.Units.MolarAdsorptionPotential A "Molar adsorption potential"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.Pressure p_sat
    "Saturation pressure at the equilibrium temperature"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.Temperature T_adsorpt
    "Equilibrium temperature of the adsorpt phase"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of outputs
  //
  output Modelica.Units.SI.Pressure p_adsorpt
    "Equilibrium pressure of the adsorpt phase"
    annotation (Dialog(tab="General", group="Outputs",
                enable=false));

algorithm
  p_adsorpt := p_sat / exp(A / (Modelica.Constants.R * T_adsorpt))
    "Equilibrium pressure of the adsorpt phase";

  //
  // Annotations
  //
  annotation (Inline=false,
InlineAfterIndexReduction=false,
LateInline=true,
inverse(A=A_ppsT(p_adsorpt,p_sat,T_adsorpt)),
Documentation(info="<html>
<p>
This function calculates the equilibrium pressure <i>p<sub>adsorpt</sub></i> for
a given molar adsorption potential <i>A</i>.
</p>

<h4>Main equations</h4>
<p>
The equilibrium pressure <i>p<sub>adsorpt</sub></i> is defined as follows:
</p>
<pre>
    p<sub>adsorpt</sub> = p<sub>sat</sub> * <strong>exp</strong>(-A / (R * T<sub>adsorpt</sub>));
</pre>

<h4>References</h4>
<ul>
  <li>
  Do, D. D. (1998). Adsorption Analysis: Equilibria and Kinetics, 1st Edition, ISBN 978-1-86094-130-6, Imperial College Press.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  November 2, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end p_ApsT;
