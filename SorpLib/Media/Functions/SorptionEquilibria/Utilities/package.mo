within SorpLib.Media.Functions.SorptionEquilibria;
package Utilities "Package containing utility functions and models for calculating sorption equilibria"
extends Modelica.Icons.UtilitiesPackage;

annotation (Documentation(info="<html>
<p>
This package contains utility functions and models required to calculate sorption
equilibria of pure component or multicomponent adsorption.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end Utilities;
