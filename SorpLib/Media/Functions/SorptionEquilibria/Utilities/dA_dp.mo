within SorpLib.Media.Functions.SorptionEquilibria.Utilities;
function dA_dp
  "Calculates the partial derivative of molar adsorption potential w.r.t. equilibrium pressure at constant temperature"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.Pressure p_adsorpt
    "Equilibrium pressure of the adsorpt phase"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.Pressure p_sat
    "Saturation pressure at the equilibrium temperature"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.Temperature T_adsorpt
    "Equilibrium temperature of the adsorpt phase"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of outputs
  //
  output SorpLib.Units.DerMolarAdsorptionPotentialByPressure dA_dp_adsorpt
    "Partial derivative of the molar adsorption potential w.r.t. equilibrium 
    pressure at constant temperature"
    annotation (Dialog(
      tab="General",
      group="Outputs",
      enable=false));

algorithm
  dA_dp_adsorpt := -Modelica.Constants.R * T_adsorpt / p_adsorpt
    "Partial derivative of the molar adsorption potential w.r.t. equilibrium 
    pressure at constant temperature";

  //
  // Annotations
  //
  annotation (Inline=true,
Documentation(info="<html>
<p>
This function calculates the partial derivative of the molar adsorption potential 
with respect to the equilibrium pressure <i>dA_dp_adsorpt</i> at constant temperature.
For more information about the molar adsorption potential <i>A</i>, check the 
documentation of the function
<a href=\"Modelica://SorpLib.Media.Functions.SorptionEquilibria.Utilities.A_ppsT\">SorpLib.Media.Functions.SorptionEquilibria.Utilities.A_ppsT</a>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 2, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end dA_dp;
