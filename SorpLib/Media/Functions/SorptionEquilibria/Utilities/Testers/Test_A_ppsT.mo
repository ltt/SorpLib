within SorpLib.Media.Functions.SorptionEquilibria.Utilities.Testers;
model Test_A_ppsT
  "Tester for the function 'calc_A_ppsT' and all corresponding functions"
  extends Modelica.Icons.Example;

  //
  // Definition of parameters
  //
  parameter Modelica.Units.SI.PressureDifference dp = 1e-3
    "Pressure difference used to calculate partial derivatives numerically"
    annotation (Dialog(tab="General", group="Numerical parameters"),
                Evaluate=true,
                HideResult=true);
  parameter Modelica.Units.SI.TemperatureDifference dT = 1e-3
    "Temperature difference used to calculate partial derivatives numerically"
    annotation (Dialog(tab="General", group="Numerical parameters"),
                Evaluate=true,
                HideResult=true);

  //
  // Definition of variables
  //
  Modelica.Units.SI.Pressure p_adsorpt(start=100, fixed=true)
    "Equilibrium pressure";
  Modelica.Units.SI.Temperature T_adsorpt(start=278.15, fixed=true)
    "Equilibrium temperature";

  Modelica.Units.SI.Pressure p_sat
    "Saturation pressure at equilibrium temperature";
  Modelica.Units.SI.DerPressureByTemperature dp_sat_dT
    "Partial derivative of saturation pressure w.r.t. equilibrium temperature";
  Real ddp_sat_dT_dT(unit="Pa/K2")
    "Second-order partial derivative of saturation pressure w.r.t. equilibrium 
    temperature";

  SorpLib.Units.MolarAdsorptionPotential A
    "Molar adsorption potential";

  Modelica.Units.SI.Pressure p_adsorpt_inv
   "Equilibrium pressure calculated via inverse function of the molar adsorption 
    potential";

  SorpLib.Units.DerMolarAdsorptionPotentialByPressure dA_dp_adsorpt
    "Partial derivative of the molar adsorption potential w.r.t. pressure at 
    constant temperature";
  SorpLib.Units.DerMolarAdsorptionPotentialByPressure dA_dp_adsorpt_num
    "Partial derivative of the molar adsorption potential w.r.t. pressure at 
    constant temperature calculated numerically";

  SorpLib.Units.DerMolarAdsorptionPotentialByTemperature dA_dT_adsorpt
    "Partial derivative of the molar adsorption potential w.r.t. temperature at 
    constant pressure";
  SorpLib.Units.DerMolarAdsorptionPotentialByTemperature dA_dT_adsorpt_num
    "Partial derivative of the molar adsorption potential w.r.t. temperature at 
    constant pressure calculated numerically";

  SorpLib.Units.DerMolarAdsorptionPotentialByPressurePressure ddA_dp_adsorpt_dp_adsorpt
    "Second-order partial derivative of the molar adsorption potential w.r.t. 
    pressure at constant temperature";
  SorpLib.Units.DerMolarAdsorptionPotentialByPressurePressure ddA_dp_adsorpt_dp_adsorpt_num
    "Second-order partial derivative of the molar adsorption potential w.r.t. 
    pressure at constant temperature calculated numerically";

  SorpLib.Units.DerMolarAdsorptionPotentialByTemperatureTemperature ddA_dT_adsorpt_dT_adsorpt
    "Second-order partial derivative of the molar adsorption potential w.r.t. 
    temperature at constant pressure";
  SorpLib.Units.DerMolarAdsorptionPotentialByTemperatureTemperature ddA_dT_adsorpt_dT_adsorpt_num
    "Second-order partial derivative of the molar adsorption potential w.r.t. 
    temperature at constant pressure calculated numerically";

  SorpLib.Units.DerMolarAdsorptionPotentialByPressureTemperature ddA_dp_adsorpt_dT_adsorpt
    "Second-order partial derivative of the molar adsorption potential w.r.t. 
    pressure and temperature";
  SorpLib.Units.DerMolarAdsorptionPotentialByPressureTemperature ddA_dp_adsorpt_dT_adsorpt_num
    "Second-order partial derivative of the molar adsorption potential w.r.t. 
    pressure and temperature calculated numerically";

protected
  Modelica.Units.SI.Pressure p_sat_pdT
    "Saturation pressure at equilibrium temperature: T + 1e-6 K";
  Modelica.Units.SI.Pressure p_sat_mdT
    "Saturation pressure at equilibrium temperature: T - 1e-6 K";

  Modelica.Units.SI.DerPressureByTemperature dp_sat_dT_pdT
    "Partial derivative of saturation pressure w.r.t. equilibrium temperature: 
    T + 1e-6 K";
  Modelica.Units.SI.DerPressureByTemperature dp_sat_dT_mdT
    "Partial derivative of saturation pressure w.r.t. equilibrium temperature: 
    T - 1e-6 K";

equation
  //
  // Definition of derivatives
  //
  der(p_adsorpt) = 500
    "Predecsriped slope of p_adsorpt";
  der(T_adsorpt) = 95/20
    "Predecsriped slope of T_adsorpt";

  //
  // Calculation of properties
  //
  p_sat= Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.psat(T=T_adsorpt)
    "Saturation pressure at equilibrium temperature";
  dp_sat_dT= Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.dptofT(T=T_adsorpt)
    "Partial derivative of saturation pressure w.r.t. equilibrium temperature";
  ddp_sat_dT_dT=
    (Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.dptofT(T=T_adsorpt + dT) -
    Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.dptofT(T=T_adsorpt - dT)) /
    (2*dT)
    "Second-order partial derivative of saturation pressure w.r.t. equilibrium 
    temperature";

  p_adsorpt_inv= SorpLib.Media.Functions.SorptionEquilibria.Utilities.p_ApsT(
    A=A,
    p_sat=p_sat,
    T_adsorpt=T_adsorpt)
    "Equilibrium pressure calculated via inverse function of the molar adsorption 
    potential";

  A= SorpLib.Media.Functions.SorptionEquilibria.Utilities.A_ppsT(
    p_adsorpt=p_adsorpt,
    p_sat=p_sat,
    T_adsorpt=T_adsorpt)
    "Molar adsorption potential";

  //
  // Calculation of derivatives
  //
  dA_dp_adsorpt=
    SorpLib.Media.Functions.SorptionEquilibria.Utilities.dA_dp(
    p_adsorpt=p_adsorpt,
    p_sat=p_sat,
    T_adsorpt=T_adsorpt)
    "Partial derivative of the molar adsorption potential w.r.t. pressure at 
    constant temperature";

  dA_dT_adsorpt=
    SorpLib.Media.Functions.SorptionEquilibria.Utilities.dA_dT(
    p_adsorpt=p_adsorpt,
    p_sat=p_sat,
    T_adsorpt=T_adsorpt,
    dp_sat_dT_adsorpt=dp_sat_dT)
    "Partial derivative of the molar adsorption potential w.r.t. temperature at 
    constant pressure";

  ddA_dp_adsorpt_dp_adsorpt=
    SorpLib.Media.Functions.SorptionEquilibria.Utilities.ddA_dp_dp(
    p_adsorpt=p_adsorpt,
    p_sat=p_sat,
    T_adsorpt=T_adsorpt)
    "Second-order partial derivative of the molar adsorption potential w.r.t. 
    pressure at constant temperature";
  ddA_dT_adsorpt_dT_adsorpt=
    SorpLib.Media.Functions.SorptionEquilibria.Utilities.ddA_dT_dT(
    p_adsorpt=p_adsorpt,
    p_sat=p_sat,
    T_adsorpt=T_adsorpt,
    dp_sat_dT_adsorpt=dp_sat_dT,
    ddp_sat_dT_adsorpt_dT_adsorpt=ddp_sat_dT_dT)
    "Second-order partial derivative of the molar adsorption potential w.r.t. 
    temperature at constant pressure";
  ddA_dp_adsorpt_dT_adsorpt=
    SorpLib.Media.Functions.SorptionEquilibria.Utilities.ddA_dp_dT(
    p_adsorpt=p_adsorpt,
    p_sat=p_sat,
    T_adsorpt=T_adsorpt)
    "Second-order partial derivative of the molar adsorption potential w.r.t. 
    pressure and temperature";

  //
  // Calculation of numerical derivatives
  //
  p_sat_pdT= Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.psat(T=T_adsorpt+dT)
    "Saturation pressure at equilibrium temperature: T + 1e-6 K";
  p_sat_mdT= Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.psat(T=T_adsorpt-dT)
    "Saturation pressure at equilibrium temperature: T - 1e-6 K";

  dp_sat_dT_pdT= Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.dptofT(T=T_adsorpt+dT)
    "Partial derivative of saturation pressure w.r.t. equilibrium temperature: 
    T + 1e-6 K";
  dp_sat_dT_mdT= Modelica.Media.Water.IF97_Utilities.BaseIF97.Basic.dptofT(T=T_adsorpt-dT)
    "Partial derivative of saturation pressure w.r.t. equilibrium temperature: 
    T - 1e-6 K";

  dA_dp_adsorpt_num=(
    SorpLib.Media.Functions.SorptionEquilibria.Utilities.A_ppsT(
    p_adsorpt=p_adsorpt + dp,
    p_sat=p_sat,
    T_adsorpt=T_adsorpt) -
    SorpLib.Media.Functions.SorptionEquilibria.Utilities.A_ppsT(
    p_adsorpt=p_adsorpt - dp,
    p_sat=p_sat,
    T_adsorpt=T_adsorpt))/(2*dp)
    "Partial derivative of the molar adsorption potential w.r.t. pressure at 
    constant temperature calculated numerically";
  dA_dT_adsorpt_num=(
    SorpLib.Media.Functions.SorptionEquilibria.Utilities.A_ppsT(
    p_adsorpt=p_adsorpt,
    p_sat=p_sat_pdT,
    T_adsorpt=T_adsorpt + dT) -
    SorpLib.Media.Functions.SorptionEquilibria.Utilities.A_ppsT(
    p_adsorpt=p_adsorpt,
    p_sat=p_sat_mdT,
    T_adsorpt=T_adsorpt - dT))/(2*dT)
    "Partial derivative of the molar adsorption potential w.r.t. temperature at 
    constant pressure calculated numerically";

  ddA_dp_adsorpt_dp_adsorpt_num=(
    SorpLib.Media.Functions.SorptionEquilibria.Utilities.dA_dp(
    p_adsorpt=p_adsorpt + dp,
    p_sat=p_sat,
    T_adsorpt=T_adsorpt) -
    SorpLib.Media.Functions.SorptionEquilibria.Utilities.dA_dp(
    p_adsorpt=p_adsorpt - dp,
    p_sat=p_sat,
    T_adsorpt=T_adsorpt))/(2*dp)
    "Second-order partial derivative of the molar adsorption potential w.r.t. 
    pressure at constant temperature calculated numerically";
  ddA_dT_adsorpt_dT_adsorpt_num=(
    SorpLib.Media.Functions.SorptionEquilibria.Utilities.dA_dT(
    p_adsorpt=p_adsorpt,
    p_sat=p_sat_pdT,
    T_adsorpt=T_adsorpt + dT,
    dp_sat_dT_adsorpt=dp_sat_dT_pdT) -
    SorpLib.Media.Functions.SorptionEquilibria.Utilities.dA_dT(
    p_adsorpt=p_adsorpt,
    p_sat=p_sat_mdT,
    T_adsorpt=T_adsorpt - dT,
    dp_sat_dT_adsorpt=dp_sat_dT_mdT))/(2*dT)
    "Second-order partial derivative of the molar adsorption potential w.r.t. 
    temperature at constant pressure calculated numerically";
  ddA_dp_adsorpt_dT_adsorpt_num=(
    SorpLib.Media.Functions.SorptionEquilibria.Utilities.dA_dp(
    p_adsorpt=p_adsorpt,
    p_sat=p_sat_pdT,
    T_adsorpt=T_adsorpt + dT) -
    SorpLib.Media.Functions.SorptionEquilibria.Utilities.dA_dp(
    p_adsorpt=p_adsorpt,
    p_sat=p_sat_mdT,
    T_adsorpt=T_adsorpt - dT))/(2*dT)
    "Second-order partial derivative of the molar adsorption potential w.r.t. 
    pressure and temperature calculated numerically";

  //
  // Definition of assertions: Check numerical implementation of isotherm model
  //
  assert(abs(p_adsorpt-p_adsorpt_inv) < 1e-6,
    "Inverse function of molar adsorption potential is not valid: Deviation (|" +
    String(abs(p_adsorpt-p_adsorpt_inv)) +
    "|) is greater than 1e-6 Pa!",
    level = AssertionLevel.warning);
  assert(abs(dA_dp_adsorpt-dA_dp_adsorpt_num) < 1e-6,
    "Partial derivative of molar adsorption potential w.r.t. pressure at " +
    "constant temperature is not valid: Deviation (|" +
    String(abs(dA_dp_adsorpt-dA_dp_adsorpt_num)) +
    "|) is greater than 1e-6 J/mol/Pa!",
    level = AssertionLevel.warning);
  assert(abs(dA_dT_adsorpt-dA_dT_adsorpt_num) < 1e-6,
    "Partial derivative of molar adsorption potential w.r.t. temperature at " +
    "constant pressure is not valid: Deviation (|" +
    String(abs(dA_dT_adsorpt-dA_dT_adsorpt_num)) +
    "|) is greater than 1e-6 J/mol/K!",
    level = AssertionLevel.warning);
  assert(abs(ddA_dp_adsorpt_dp_adsorpt-ddA_dp_adsorpt_dp_adsorpt_num) < 1e-6,
    "Second-order partial derivative of molar adsorption potential w.r.t. " +
    "pressure at constant temperature is not valid: Deviation (|" +
    String(abs(ddA_dp_adsorpt_dp_adsorpt-ddA_dp_adsorpt_dp_adsorpt_num)) +
    "|) is greater than 1e-6 J/mol/Pa!",
    level = AssertionLevel.warning);
  assert(abs(ddA_dT_adsorpt_dT_adsorpt-ddA_dT_adsorpt_dT_adsorpt_num) < 1e-6,
    "Second-order partial derivative of molar adsorption potential w.r.t. " +
    "temperature at constant pressure is not valid: Deviation (|" +
    String(abs(ddA_dT_adsorpt_dT_adsorpt-ddA_dT_adsorpt_dT_adsorpt_num)) +
    "|) is greater than 1e-6 J/mol/K2!",
    level = AssertionLevel.warning);
  assert(abs(ddA_dp_adsorpt_dT_adsorpt-ddA_dp_adsorpt_dT_adsorpt_num) < 1e-6,
    "Second-order partial derivative of molar adsorption potential w.r.t. " +
    "pressure and temperature is not valid: Deviation (|" +
    String(abs(ddA_dp_adsorpt_dT_adsorpt-ddA_dp_adsorpt_dT_adsorpt_num)) +
    "|) is greater than 1e-6 J/mol/Pa/K!",
    level = AssertionLevel.warning);

  //
  // Annotations
  //
  annotation (experiment(
      StopTime=20,
      Tolerance=1e-08),
Documentation(revisions="<html>
<ul>
  <li>
  November 2, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>", info="<html>
<p>
This tester shows the behavior of the 'calc_A_ppsT' function and its corresponding
functions, such as inverse functions of partial derivatives.
<br/><br/>
To see the function behavior, plot the variable <i>A</i>, <i>dA_dp_adsorpt</i>, 
<i>dA_dT_adsorpt</i>, <i>ddA_dp_adsorpt_dp_adsorpt</i>, <i>ddA_dT_adsorpt_dT_adsorpt</i>, 
and <i>ddA_dp_adsorpt_dT_adsorpt</i> over the time. The simulation time is correctly 
preset (Start: 0 s, Stop = 20 s).  
</p>
</html>"));
end Test_A_ppsT;
