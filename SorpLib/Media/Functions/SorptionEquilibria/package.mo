within SorpLib.Media.Functions;
package SorptionEquilibria "Functions required to calculate sorption equilibria"
  extends Modelica.Icons.VariantsPackage;

  annotation (Documentation(info="<html>
<p>
This package contains isotherm models for pure component and multi-component 
adsorption. Please check the documentation of the individual packages for more 
details on the implemented isotherm models and functions provided for each
isotherm model.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end SorptionEquilibria;
