within SorpLib.Media.Functions;
package SorptionEnthalpies "Functions required to calculate sorption enthalpies"
  extends Modelica.Icons.VariantsPackage;

  annotation (Documentation(info="<html>
<p>
This package contains sorption enthalpy models for pure component and multi-component 
adsorption. Please check the documentation of the individual packages for more 
details on the implemented sorption enthalpy models .
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end SorptionEnthalpies;
