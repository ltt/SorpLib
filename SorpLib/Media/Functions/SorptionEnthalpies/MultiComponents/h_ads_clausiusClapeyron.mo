within SorpLib.Media.Functions.SorptionEnthalpies.MultiComponents;
function h_ads_clausiusClapeyron
  "Molar adsorption enthalpies according to Clausius Clapeyron"
  extends
    SorpLib.Media.Functions.SorptionEnthalpies.BasesClasses.PartialMulti_h_ads;

  //
  // Definition of variables
  //
protected
  Modelica.Units.SI.Pressure p_adsorpt_
    "Limited total pressure";
  Modelica.Units.SI.MoleFraction[size(M_i,1)-1] y_i_
    "Limited mole fractions of independent components in the vapor or gas phase";

  //
  // Dimension 1: Components
  // Dimension 2: p_adsorpt or T_adsorpt | x_1 | ... | x_size(M_i,1)
  // Dimension 3: p_adsorpt | T_adsorpt | y_1 | ... | y_size(M_i,1)-1
  //
  Real[size(M_i,1),size(M_i,1)+1,size(M_i,1)+1] numerator_i
    "Numerators of all components";
  Real[size(M_i,1),size(M_i,1)+1,size(M_i,1)+1] denominator_i
    "Denominators of all components";

algorithm
  //
  // Define initial values
  //
  p_adsorpt_ := max(p_adsorpt, p_threshold_min)
    "Limited total pressure";
  for ind in 1:size(M_i,1)-1 loop
    y_i_[ind] := max(y_i[ind], y_i_threshold_min)
      "Limited mole fractions of independent components in the vapor or gas phase";
  end for;

  numerator_i:= zeros(size(M_i,1),size(M_i,1)+1,size(M_i,1)+1)
    "Numerators of all components";
  denominator_i:= zeros(size(M_i,1),size(M_i,1)+1,size(M_i,1)+1)
    "Denominators of all components";

  //
  // Set up numerators and denominators
  //
  for ind_comp in 1:size(M_i,1) loop
    //
    // First row of numerators and denominators
    //
    numerator_i[ind_comp,1,1] :=1/p_adsorpt_
      "Partial derivatives of partial pressures w.r.t. pressure";
    numerator_i[ind_comp,1,2] :=0
      "Partial derivatives of partial pressures w.r.t. temperature";

    denominator_i[ind_comp,1,1] :=0
      "Partial derivatives of temperature w.r.t. pressure";
    denominator_i[ind_comp,1,2] :=1
      "Partial derivatives of temperature w.r.t. temperature";

    for ind_indep_comp in 1:size(M_i,1)-1 loop
      if ind_comp < size(M_i,1) then
        numerator_i[ind_comp,1,2+ind_indep_comp] :=if ind_comp ==
          ind_indep_comp then 1/y_i_[ind_indep_comp] else 0
          "Partial derivatives of partial pressures w.r.t. independent mole
          fractions of adsorptive phase";
      else
        numerator_i[ind_comp,1,2+ind_indep_comp] := -1 / (1 - sum(y_i_))
          "Partial derivatives of partial pressures w.r.t. independent mole
          fractions of adsorptive phase";
      end if;

      denominator_i[ind_comp,1,2+ind_indep_comp] :=0
        "Partial derivatives of partial pressures w.r.t. independent mole
        fractions of adsorptive phase";

    end for;

    //
    // Second to last row of numerators and denominators
    //
    for ind_comp_inner in 1:size(M_i,1) loop
      //
      // Partial derivatives w.r.t. pressure
      //
      numerator_i[ind_comp,1+ind_comp_inner,1] :=
        1 / M_i[ind_comp_inner] * dx_adsorpt_dp[ind_comp_inner]
        "Partial derivatives of uptake w.r.t. pressure";
      denominator_i[ind_comp,1+ind_comp_inner,1] :=
        1 / M_i[ind_comp_inner] * dx_adsorpt_dp[ind_comp_inner]
        "Partial derivatives of uptake w.r.t. pressure";

      //
      // Partial derivatives w.r.t. temperature
      //
      numerator_i[ind_comp,1+ind_comp_inner,2] :=
        1 / M_i[ind_comp_inner] * dx_adsorpt_dT[ind_comp_inner]
        "Partial derivatives of uptake w.r.t. pressure";
      denominator_i[ind_comp,1+ind_comp_inner,2] :=
        1 / M_i[ind_comp_inner] * dx_adsorpt_dT[ind_comp_inner]
        "Partial derivatives of uptake w.r.t. pressure";

      //
      // Partial derivatives w.r.t. independent mole fractions of adsorptive phase
      //
      for ind_indep_comp in 1:size(M_i,1)-1 loop
        numerator_i[ind_comp,1+ind_comp_inner,2+ind_indep_comp] :=
          1 / M_i[ind_comp_inner] * dx_adsorpt_dy_i[ind_comp_inner,ind_indep_comp]
          "Partial derivatives of uptake w.r.t. independent mole fractions";
        denominator_i[ind_comp,1+ind_comp_inner,2+ind_indep_comp] :=
          1 / M_i[ind_comp_inner] * dx_adsorpt_dy_i[ind_comp_inner,ind_indep_comp]
          "Partial derivatives of uptake w.r.t. independent mole fractions";

      end for;
    end for;
  end for;

  //
  // Calculate adsorption enthalpies
  //
  if p_adsorpt < p_threshold_min then
    //
    // Vacuum
    //
    h_ads := fill(100*Modelica.Constants.eps, size(M_i,1))
      "Molar adsorption enthalpy";

  else
    //
    // Independent components
    //
    for ind_comp in 1:size(M_i,1)-1 loop
      if y_i[ind_comp] > y_i_threshold_min then
        h_ads[ind_comp] := Modelica.Constants.R * T_adsorpt^2 *
          Modelica.Math.Matrices.det(numerator_i[ind_comp,:,:]) /
          Modelica.Math.Matrices.det(denominator_i[ind_comp,:,:])
          "Molar adsorption enthalpy";

      else
        h_ads[ind_comp] := -100*Modelica.Constants.eps
          "Molar adsorption enthalpy";

      end if;
    end for;

    //
    // Dependent components
    //
    if 1-sum(y_i) > y_i_threshold_min then
      h_ads[size(M_i,1)] := Modelica.Constants.R * T_adsorpt^2 *
        Modelica.Math.Matrices.det(numerator_i[size(M_i,1),:,:]) /
        Modelica.Math.Matrices.det(denominator_i[size(M_i,1),:,:])
        "Molar adsorption enthalpy";

    else
      h_ads[size(M_i,1)] := 100*Modelica.Constants.eps
        "Molar adsorption enthalpy";

    end if;
  end if;

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function calculates the molar adsorption enthalpies for mixtures according to 
Clausius Clapeyron assumptions.
</p>

<h4>Main equations</h4>
<p>
The molar adsorption enthalpy <i>&Delta;h<sub>ads,i</sub></i> of component <i>i</i>
is defined as follows:
</p>
<pre>
    &Delta;h<sub>ads,<i>i</i></sub> = R * T<sup>2</sup> * [&part;(<strong>ln</strong>(y<sub><i>i</i></sub> * p), x<sub>1</sub>, ..., x<sub>N</sub>) / &part;(p, T, y<sub>1</sub>, ..., y<sub>N-1</sub>)] / [&part;(T, x<sub>1</sub>, ..., x<sub>N</sub>) / &part;(p, T, y<sub>1</sub>, ..., y<sub>N-1</sub>)];
</pre>
<p>
Herein, <i>R</i> is the ideal gas constant, <i>p</i> is the pressure, <i>T</i> is
the temperature, <i>x<sub>i</sub></i> are the uptakes, and <i>y<sub>i</sub></i>
are the independent mole fractions in the adsorptive phase.
<br><br>
The determinants in the numerator and denominator are calculated using the Modelica
function
<a href=\"Modelica://Modelica.Math.Matrices.det\">Modelica.Math.Matrices.det</a>.
Hence, it is more efficient to use other functions that not rely on the Modelica
function but have the determinats already solved.
</p>

<h4>Assumptions and limitations</h4>
<ul>
  <li>
  The specific volume of the adsorpt phase can be neglected compared to the specific
  volume of the adsorptive phase.
  </li>
  <li>
  The adsorptive can be treated as an ideal gas mixture.
  </li>
  <li>
  The adsorpt can be treated as an ideal solution.
  </li>
  <li>
  Inert sorbent.
  </li>
</ul>

<h4>References</h4>
<ul>
  <li>
  Sircar, S. (1984). Excess Properties and Thermodynamics of Multicomponent Gas Adsorption. Journal of the Chemical Society, Faraday Transactions 1: Physical Chemistry in Condensed Phases, 81:1527-1540. DOI: https://doi.org/10.1039/F19858101527.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  November 17, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end h_ads_clausiusClapeyron;
