within SorpLib.Media.Functions.SorptionEnthalpies;
package MultiComponents "Functions required to calculate sorption enthalpies for multi components"
  extends Modelica.Icons.VariantsPackage;

  annotation (Documentation(info="<html>
<p>
This package contains models to calculate sorption enthalpies for pure component 
adsorption.
</p>

<h4>Implemented functions for each sorption enthalpy model</h4>
<p>
The following functions are provided for each sorption enthalpy model:
</p>
<ul>
  <li>
  Sorption enthalpy.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end MultiComponents;
