within SorpLib.Media.Functions.SorptionEnthalpies.MultiComponents;
function h_ads_ternary_clausiusClapeyron
  "Molar adsorption enthalpies for a ternary mixture according to Clausius Clapeyron"
  extends
    SorpLib.Media.Functions.SorptionEnthalpies.BasesClasses.PartialMulti_h_ads;

  //
  // Definition of variables
  //
protected
  Modelica.Units.SI.Pressure p_adsorpt_
    "Limited total pressure";
  Modelica.Units.SI.MoleFraction[size(M_i,1)-1] y_i_
    "Limited mole fractions of independent components in the vapor or gas phase";

  Real[size(M_i,1)] numerator_i
    "Numerators of all components";
  Real numerator_summand_1
    "Summand 1 of the numerator for each component";
  Real numerator_summand_2
    "Summand 2 of the numerator for each component";
  Real numerator_summand_3
    "Summand 3 of the numerator for each component";

  Real denominator
    "Denominator for each component";

algorithm
  //
  // Define initial values
  //
  p_adsorpt_ := max(p_adsorpt, p_threshold_min)
    "Limited total pressure";
  for ind in 1:size(M_i,1)-1 loop
    y_i_[ind] := max(y_i[ind], y_i_threshold_min)
      "Limited mole fractions of independent components in the vapor or gas phase";
  end for;

  //
  // Set up numerators
  //
  numerator_summand_1 := dx_adsorpt_dT[1] *
    (dx_adsorpt_dy_i[2,1] * dx_adsorpt_dy_i[3,2] -
     dx_adsorpt_dy_i[3,1] * dx_adsorpt_dy_i[2,2]) - dx_adsorpt_dy_i[1,1] *
    (dx_adsorpt_dT[2] * dx_adsorpt_dy_i[3,2] -
     dx_adsorpt_dT[3] * dx_adsorpt_dy_i[2,2]) + dx_adsorpt_dy_i[1,2] *
    (dx_adsorpt_dT[2] * dx_adsorpt_dy_i[3,1] -
     dx_adsorpt_dT[3] * dx_adsorpt_dy_i[2,1])
    "Summand 1 of the numerator for each component";
  numerator_summand_2 :=dx_adsorpt_dp[1] *
    (dx_adsorpt_dT[2] * dx_adsorpt_dy_i[3,2] -
     dx_adsorpt_dT[3] * dx_adsorpt_dy_i[2,2]) - dx_adsorpt_dT[1] *
    (dx_adsorpt_dp[2] * dx_adsorpt_dy_i[3,2] -
     dx_adsorpt_dp[3] * dx_adsorpt_dy_i[2,2]) + dx_adsorpt_dy_i[1,2] *
    (dx_adsorpt_dp[2] * dx_adsorpt_dT[3] -
     dx_adsorpt_dp[3] * dx_adsorpt_dT[2])
    "Summand 2 of the numerator for each component";
  numerator_summand_3 :=dx_adsorpt_dp[1] *
    (dx_adsorpt_dT[2] * dx_adsorpt_dy_i[3,1] -
     dx_adsorpt_dT[3] * dx_adsorpt_dy_i[2,1]) - dx_adsorpt_dT[1] *
    (dx_adsorpt_dp[2] * dx_adsorpt_dy_i[3,1] -
     dx_adsorpt_dp[3] * dx_adsorpt_dy_i[2,1]) + dx_adsorpt_dy_i[1,1] *
    (dx_adsorpt_dp[2] * dx_adsorpt_dT[3] -
     dx_adsorpt_dp[3] * dx_adsorpt_dT[2])
    "Summand 3 of the numerator for each component";

  numerator_i[1] :=
    1 / (p_adsorpt_ * M_i[1] *  M_i[2] *  M_i[3]) * numerator_summand_1 +
    1 / (y_i_[1] * M_i[1] *  M_i[2] *  M_i[3]) * numerator_summand_2
    "Numerator of first component";
  numerator_i[2] :=
    1 / (p_adsorpt_ * M_i[1] *  M_i[2] *  M_i[3]) * numerator_summand_1 -
    1 / (y_i_[2] * M_i[1] *  M_i[2] *  M_i[3]) * numerator_summand_3
    "Numerator of second component";
  numerator_i[3] :=
    1 / (p_adsorpt_ * M_i[1] *  M_i[2] *  M_i[3]) * numerator_summand_1 +
    1 / (-(1 - sum(y_i_)) * M_i[1] *  M_i[2] *  M_i[3]) * numerator_summand_2 -
    1 / (-(1 - sum(y_i_)) * M_i[1] *  M_i[2] *  M_i[3]) * numerator_summand_3
    "Numerator of third component";

  //
  // Set up denominator
  //
  denominator := -1 / (M_i[1] *  M_i[2] *  M_i[3]) * (dx_adsorpt_dp[1] *
     (dx_adsorpt_dy_i[2,1] * dx_adsorpt_dy_i[3,2] -
      dx_adsorpt_dy_i[3,1] * dx_adsorpt_dy_i[2,2]) - dx_adsorpt_dy_i[1,1] *
     (dx_adsorpt_dp[2] * dx_adsorpt_dy_i[3,2] -
      dx_adsorpt_dp[3] * dx_adsorpt_dy_i[2,2]) + dx_adsorpt_dy_i[1,2] *
     (dx_adsorpt_dp[2] * dx_adsorpt_dy_i[3,1] -
      dx_adsorpt_dp[3] * dx_adsorpt_dy_i[2,1]))
    "Denominator for each components";

  //
  // Calculate adsorption enthalpies
  //
  if p_adsorpt < p_threshold_min then
    //
    // Vacuum
    //
    h_ads := fill(100*Modelica.Constants.eps, size(M_i,1))
      "Molar adsorption enthalpy";

  else
    //
    // No vacuum
    //
    if y_i[1] > y_i_threshold_min then
      h_ads[1] := Modelica.Constants.R * T_adsorpt^2 *
        numerator_i[1] / denominator
        "Molar adsorption enthalpy of first component";
    else
      h_ads[1] := 100*Modelica.Constants.eps
        "Molar adsorption enthalpy of first component";
    end if;

    if y_i[2] > y_i_threshold_min then
      h_ads[2] := Modelica.Constants.R * T_adsorpt^2 *
        numerator_i[2] / denominator
        "Molar adsorption enthalpy of first component";
    else
      h_ads[2] := 100*Modelica.Constants.eps
        "Molar adsorption enthalpy of second component";
    end if;

    if 1-y_i[1]-y_i[2] > y_i_threshold_min then
      h_ads[3] := Modelica.Constants.R * T_adsorpt^2 *
        numerator_i[3] / denominator
        "Molar adsorption enthalpy of third component";
    else
      h_ads[3] := 100*Modelica.Constants.eps
        "Molar adsorption enthalpy of third component";
    end if;
  end if;

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function calculates the molar adsorption enthalpies for ternary mixtures according 
to Clausius Clapeyron assumptions.
</p>

<h4>Main equations</h4>
<p>
The molar adsorption enthalpy <i>&Delta;h<sub>ads,i</sub></i> of component <i>i</i>
is defined as follows:
</p>
<pre>
    &Delta;h<sub>ads,<i>i</i></sub> = R * T<sup>2</sup> * [&part;(<strong>ln</strong>(y<sub><i>i</i></sub> * p), x<sub>1</sub>, ..., x<sub>N</sub>) / &part;(p, T, y<sub>1</sub>, ..., y<sub>N-1</sub>)] / [&part;(T, x<sub>1</sub>, ..., x<sub>N</sub>) / &part;(p, T, y<sub>1</sub>, ..., y<sub>N-1</sub>)];
</pre>
<p>
Herein, <i>R</i> is the ideal gas constant, <i>p</i> is the pressure, <i>T</i> is
the temperature, <i>x<sub>i</sub></i> are the uptakes, and <i>y<sub>i</sub></i>
are the independent mole fractions in the adsorptive phase.
<br><br>
The determinants in the numerator and denominator have already been solved.
</p>

<h4>Assumptions and limitations</h4>
<ul>
  <li>
  The specific volume of the adsorpt phase can be neglected compared to the specific
  volume of the adsorptive phase.
  </li>
  <li>
  The adsorptive can be treated as an ideal gas mixture.
  </li>
  <li>
  The adsorpt can be treated as an ideal solution.
  </li>
  <li>
  Inert sorbent.
  </li>
</ul>

<h4>References</h4>
<ul>
  <li>
  Sircar, S. (1984). Excess Properties and Thermodynamics of Multicomponent Gas Adsorption. Journal of the Chemical Society, Faraday Transactions 1: Physical Chemistry in Condensed Phases, 81:1527-1540. DOI: https://doi.org/10.1039/F19858101527.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  November 17, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end h_ads_ternary_clausiusClapeyron;
