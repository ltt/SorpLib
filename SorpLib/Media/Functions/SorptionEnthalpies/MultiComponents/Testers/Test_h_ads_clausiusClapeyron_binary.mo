within SorpLib.Media.Functions.SorptionEnthalpies.MultiComponents.Testers;
model Test_h_ads_clausiusClapeyron_binary
  "Tester for the functions 'h_ads_clausiusClapeyron' and 'h_ads_binary_clausiusClapeyron'"
  extends
    SorpLib.Media.Functions.SorptionEnthalpies.BasesClasses.PartialTestMulti;

  //
  // Definition of parameters
  //
  parameter SorpLib.Units.Uptake[no_components] x_ref = {0.32, 0.22}
    "Saturation uptake at reference temperature"
    annotation (Dialog(tab="Isotherm model", group="Parameters"),
                HideResult=true);
  parameter Real[no_components] chi(each unit="1") = {0, 0.1}
    "Parameter describing the change of the saturation uptake with temperature"
    annotation (Dialog(tab="Isotherm model", group="Parameters"),
                HideResult=true);
  parameter Real[no_components] b_ref(each unit="1/Pa") = {1.075e-4, 1.075e-4}
    "Sips coefficient at reference temperature"
    annotation (Dialog(tab="Isotherm model", group="Parameters"),
                HideResult=true);
  parameter Real[no_components] Q(each unit="J/mol") = {28.752e3, 28.752e3}
    "Parameter describing the change of the Sips coefficient with temperature"
    annotation (Dialog(tab="Isotherm model", group="Parameters"),
                HideResult=true);
  parameter Real[no_components] n_ref(each unit="1") = {2.312, 2.812}
    "Sips exponent at reference temperature"
    annotation (Dialog(tab="Isotherm model", group="Parameters"),
                HideResult=true);
  parameter Real[no_components] alpha(each unit="1") = {0.5559, 0.5559}
    "Parameter describing the change of the Sips exponent with temperature"
    annotation (Dialog(tab="Isotherm model", group="Parameters"),
                HideResult=true);
  parameter Modelica.Units.SI.Temperature[no_components] T_ref = {283, 283}
    "Reference temperature"
    annotation (Dialog(tab="Isotherm model", group="Parameters"),
                HideResult=true);

  //
  // Definition of variables
  //
  Modelica.Units.SI.MolarEnthalpy[no_components] h_ads_binary(each displayUnit="kJ/mol")
    "Molar adsorption enthalpy calculated via binary approach";

equation
  //
  // Definition of derivatives
  //
  der(p_adsorpt) = 100000/20
    "Predecsriped slope of p_adsorpt to demonstrate the isotherm model";
  der(y_i) = {0.98/20}
    "Predecsriped slope of y_i to demonstrate the isotherm model";
  der(T_adsorpt) = 90/20
    "Predecsriped slope of T_adsorpt to demonstrate the isotherm model";

  //
  // Calculate coefficients of the isotherm model
  //
  for ind_comp in 1:no_components loop
    //
    // Coefficients of the isotherm model
    //
    c[1,ind_comp] = x_ref[ind_comp] * exp(chi[ind_comp] *
      (1 - T_adsorpt/T_ref[ind_comp]))
      "Coefficients of isotherm model";
    c[2,ind_comp] = b_ref[ind_comp] * exp(Q[ind_comp]/Modelica.Constants.R/T_ref[ind_comp] *
      (T_ref[ind_comp]/T_adsorpt - 1))
      "Coefficients of isotherm model";
    c[3,ind_comp] = 1 / (1/n_ref[ind_comp] + alpha[ind_comp] *
      (1 - T_ref[ind_comp]/T_adsorpt))
      "Coefficients of isotherm model";

    //
    // Partial derivative of coefficients of isotherm model w.r.t. temperature
    //
    dc_dT[1,ind_comp] = -chi[ind_comp]/T_ref[ind_comp] * c[1,ind_comp]
      "Partial derivative of coefficients of isotherm model w.r.t. temperature";
    dc_dT[2,ind_comp] = -Q[ind_comp]/Modelica.Constants.R/T_adsorpt^2 * c[2,ind_comp]
      "Partial derivative of coefficients of isotherm model w.r.t. temperature";
    dc_dT[3,ind_comp] = -T_ref[ind_comp]*alpha[ind_comp]/T_adsorpt^2 /
      (1/n_ref[ind_comp] + alpha[ind_comp] * (1 - T_ref[ind_comp]/T_adsorpt))^2
      "Partial derivative of coefficients of isotherm model w.r.t. temperature";

  end for;

  //
  // Calculate properties
  //
  x_adsorpt = SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.Sips.x_pyT(
    p_adsorpt=p_adsorpt,
    y_i=y_i,
    T_adsorpt=T_adsorpt,
    c=c,
    p_threshold_min=p_threshold_min)
    "Uptakes";

  dx_adsorpt_dp = SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.Sips.dx_dp(
    p_adsorpt=p_adsorpt,
    y_i=y_i,
    T_adsorpt=T_adsorpt,
    c=c,
    p_threshold_min=p_threshold_min)
    "Partial derivative of uptakes w.r.t. pressure at constant mole fractions and
    temperature";

  dx_adsorpt_dy_i = SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.Sips.dx_dy(
    p_adsorpt=p_adsorpt,
    y_i=y_i,
    T_adsorpt=T_adsorpt,
    c=c,
    p_threshold_min=p_threshold_min)
    "Partial derivatives of the uptakes w.r.t. the molar fractions of independent 
    components of adsorptive phase at constant pressure and temperature";

  dx_adsorpt_dT = SorpLib.Media.Functions.SorptionEquilibria.MultiComponents.Sips.dx_dT(
    p_adsorpt=p_adsorpt,
    y_i=y_i,
    T_adsorpt=T_adsorpt,
    c=c,
    dc_dT_adsorpt=dc_dT,
    p_threshold_min=p_threshold_min)
    "Partial derivative of uptakes w.r.t. temperature at constant pressure and
    mole fractions";

  h_ads_binary = SorpLib.Media.Functions.SorptionEnthalpies.MultiComponents.h_ads_binary_clausiusClapeyron(
    M_i=M_i,
    p_adsorpt=p_adsorpt,
    y_i=y_i,
    T_adsorpt=T_adsorpt,
    dx_adsorpt_dp=dx_adsorpt_dp,
    dx_adsorpt_dy_i=dx_adsorpt_dy_i,
    dx_adsorpt_dT=dx_adsorpt_dT)
    "Molar adsorption enthalpy calculated via binary approach";

  //
  // Annotations
  //
  annotation (experiment(
      StopTime=20,
      Tolerance=1e-06),
Documentation(revisions="<html>
<ul>
  <li>
  November 17, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>", info="<html>
<p>
This tester shows the behavior of the 'h_ads_ClausiusClapeyron_binary.'
<br/><br/>
To see the function behavior, plot the variables <i>h_ads_i</i> over the time. The 
simulation time is correctly preset (Start: 0 s, Stop = 20 s). 
</p>
</html>"));
end Test_h_ads_clausiusClapeyron_binary;
