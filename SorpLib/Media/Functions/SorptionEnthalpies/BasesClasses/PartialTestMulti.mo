within SorpLib.Media.Functions.SorptionEnthalpies.BasesClasses;
partial model PartialTestMulti
  "Base model for testers of sorption enthalpy models describing multi-component adsorption"
  extends Modelica.Icons.Example;

  //
  // Definition of parameters
  //
  parameter Integer no_components = 2
    "Number of components of the isotherm model"
    annotation (Dialog(tab="Isotherm model", group="General"),
                Evaluate=true,
                HideResult=true);
  parameter Integer no_coefficients = 3
    "Number of coefficients of the isotherm model (i.e., highest number among 
    different components)"
    annotation (Dialog(tab="Isotherm model", group="General"),
                Evaluate=true,
                HideResult=true);
  parameter Modelica.Units.SI.MolarMass[no_components] M_i = {0.018, 0.044}
    "Molar masses of the components"
    annotation (Dialog(tab="Isotherm model", group="General"),
                Evaluate=true,
                HideResult=true);

  parameter Modelica.Units.SI.Pressure p_adsorpt_start = 1
    "Start value of equilibrium pressure"
    annotation (Dialog(tab="General", group="Test setup"),
                Evaluate=false,
                HideResult=false);
  parameter Modelica.Units.SI.MoleFraction[no_components-1] y_i_start={1e-2}
    "Start values independent mole fractions"
    annotation (Dialog(tab="General", group="Test setup"),
                Evaluate=false,
                HideResult=false);
  parameter Modelica.Units.SI.Temperature T_adsorpt_start = 278.15
    "Start value of equilibrium temperature"
    annotation (Dialog(tab="General", group="Test setup"),
                Evaluate=false,
                HideResult=false);

  parameter Modelica.Units.SI.Pressure p_threshold_min = 1e-6
    "Threshold for partial pressures of all components: If a partial pressure is
    below the threshold, its value is set to the threshold"
    annotation (Dialog(tab="Numerics", group="Limiter"),
                Evaluate=true,
                HideResult=true);
  parameter Modelica.Units.SI.MoleFraction y_i_threshold_min = 1e-6
    "Threshold for independent mole fractions of the adsorptive phase: If a independent
    mole fraction is below the threshold, its value is set to the threshold"
    annotation (Dialog(tab="Numerics", group="Limiter"),
                Evaluate=true,
                HideResult=true);

  //
  // Definition of variables
  //
  Modelica.Units.SI.Pressure p_adsorpt(start=p_adsorpt_start, fixed=true)
    "Pressure";
  Modelica.Units.SI.MoleFraction[no_components-1] y_i(start=y_i_start, each fixed=true)
    "Independent mole fractions";
  Modelica.Units.SI.Temperature T_adsorpt(start=T_adsorpt_start, fixed=true)
    "Temperature";

  SorpLib.Units.Uptake[no_components] x_adsorpt
    "Uptake";
  SorpLib.Units.DerUptakeByPressure[no_components] dx_adsorpt_dp
    "Partial derivative of uptakes w.r.t. pressure at constant mole fractions and
    temperature";
  SorpLib.Units.DerUptakeByMolarFraction[no_components,no_components-1] dx_adsorpt_dy_i
    "Partial derivatives of the uptakes w.r.t. the molar fractions of independent 
    components of adsorptive phase at constant pressure and temperature";
  SorpLib.Units.DerUptakeByTemperature[no_components] dx_adsorpt_dT
    "Partial derivative of uptakes w.r.t. temperature at constant pressure and
    mole fractions";

  Modelica.Units.SI.MolarEnthalpy[no_components] h_ads(each displayUnit="kJ/mol")
    "Molar adsorption enthalpy";

protected
  Real[no_coefficients,no_components] c
    "Coefficients of isotherm model";
  Real[no_coefficients,no_components] dc_dT
    "Partial derivative of coefficients of isotherm model w.r.t. temperature";

equation
  //
  // Calculate sorptions enthalies
  //
  h_ads = SorpLib.Media.Functions.SorptionEnthalpies.MultiComponents.h_ads_clausiusClapeyron(
    M_i=M_i,
    p_adsorpt=p_adsorpt,
    y_i=y_i,
    T_adsorpt=T_adsorpt,
    dx_adsorpt_dp=dx_adsorpt_dp,
    dx_adsorpt_dy_i=dx_adsorpt_dy_i,
    dx_adsorpt_dT=dx_adsorpt_dT)
    "Molar adsorption enthalpy";

  //
  // Annotations
  //
  annotation (
    Icon(coordinateSystem(preserveAspectRatio=false)),
    Diagram(coordinateSystem(preserveAspectRatio=false)),
    Documentation(info="<html>
<p>
This partial model is the basic model for all testers of sorption enthalpies for
multi components. This partial model defines all some relevant parameters and 
variables that are required for most sorption enthalpy models.
<br/><br/>
Models that inherit properties from this partial model have to specify the 
coefficients of the isotherm model (i.e., <i>c</i>) and their partial derivatives 
with respect to temperature (i.e., <i>dc_dT</i>). Additionally, equations for the
following variables must be implemted: <i>p_adsorpt</i>, <i>y_i</i>, <i>T_adsorpt</i>, 
<i>x_adsorpt</i>, <i>dx_adsorpt_dp</i>, <i>dx_adsorpt_dy_i</i>, and <i>dx_adsorpt_dT</i>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 17, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end PartialTestMulti;
