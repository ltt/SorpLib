within SorpLib.Media.Functions.SorptionEnthalpies.BasesClasses;
partial function PartialMulti_h_ads
  "Base function for functions calculating the sorption enthalpy for multi components"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.MolarMass[:] M_i
    "Molar masses of components"
    annotation (Dialog(tab="General", group="Inputs"));

  input Modelica.Units.SI.Pressure p_adsorpt
    "Pressure"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.MoleFraction[size(M_i,1)-1] y_i
    "Mole fractions of independent components in the vapor or gas phase"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.Temperature T_adsorpt
    "Temperature"
    annotation (Dialog(tab="General", group="Inputs"));

  input SorpLib.Units.DerUptakeByPressure[size(M_i,1)] dx_adsorpt_dp
    "Partial derivatives of the uptakes w.r.t. pressure at constant molar fractions
    and temperature"
    annotation (Dialog(tab="General", group="Inputs"));
  input SorpLib.Units.DerUptakeByMolarFraction[size(M_i,1),size(M_i,1)-1] dx_adsorpt_dy_i
    "Partial derivatives of the uptakes w.r.t. the molar fractions of independent 
    gas phase components at constant pressure and temperature"
    annotation (Dialog(tab="General", group="Inputs"));
  input SorpLib.Units.DerUptakeByTemperature[size(M_i,1)] dx_adsorpt_dT
    "Partial derivatives of the uptakes w.r.t. temperature at contant pressure and
    molar fractions"
    annotation (Dialog(tab="General", group="Inputs"));

  input Modelica.Units.SI.Pressure p_threshold_min = 100*Modelica.Constants.eps
    "Threshold for partial pressures of all components: If a partial pressure is
    below the threshold, its value is set to the threshold"
    annotation (Dialog(tab="General", group="Numerical inputs"));
  input Modelica.Units.SI.MoleFraction y_i_threshold_min = 100*Modelica.Constants.eps
    "Threshold for independent mole fractions of the adsorptive phase: If a independent
    mole fraction is below the threshold, its value is set to the threshold"
    annotation (Dialog(tab="General", group="Numerical inputs"));

  //
  // Definition of outputs
  //
  output Modelica.Units.SI.MolarEnthalpy[size(M_i,1)] h_ads(each displayUnit="kJ/mol")
    "Molar adsorption enthalpies of components"
    annotation (Dialog(
      tab="General",
      group="Outputs",
      enable=false));

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial function is the basic function for functions that calculate the
sorption enthalpies for multi components.
<br/><br/>
This partial function defines the molar masses of the components <i>M_i</i>, the
pressure <i>p_adsorpt</i>, the independent mole fractions in the adsorptive phase
<i>y_i</i>, the temperature <i>T_adsorpt</i>, the partial derivatives of the
uptakes with respect to the pressure at constant molar fractions and temperature
<i>dx_adsorpt_dp</i>, the partial derivatives of the uptakes with respect to the 
independent mole fractions in the adsorptive phase at constant pressure and
temperature <i>dx_adsorpt_dy_i</i>, and the partial derivatives of the uptakes with
respect to the temperature at constant pressure and molar fractions <i>dx_adsorpt_dT</i> 
as inputs. Optional inputs regarding numerics are the thresholds <i>p_threshold_min</i> 
and <i>y_i_threshold_min</i>, describing the lower limits of the total pressure 
and independent molar fractions of the adsorptive phase. The sorption enthalpies 
<i>h_ads</i> are defined as outputs.
<br/><br/>
Functions that inherit properties from this partial function may have to add
furhter inputs, outputs, and the function algorithm.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 17, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end PartialMulti_h_ads;
