within SorpLib.Media.Functions.SorptionEnthalpies.BasesClasses;
partial model PartialTestPure
  "Base model for testers of sorption enthalpy models describing pure component adsorption"
  extends Modelica.Icons.Example;

  //
  // Definition of parameters
  //
  replaceable package Medium = Modelica.Media.Water.WaterIF97_ph
    constrainedby Modelica.Media.Interfaces.PartialTwoPhaseMedium
    "Medium (i.e., adsorptive)"
    annotation (Dialog(tab="General",group="Medium"),
                choicesAllMatching = true);

  parameter Integer no_coefficients = 6
    "Number of coefficients of selected isotherm model"
    annotation (Dialog(tab="General", group="Test setup"),
                Evaluate=true,
                HideResult=true);
  parameter Modelica.Units.SI.Pressure p_adsorpt_start = 700
    "Start value of equilibrium pressure"
    annotation (Dialog(tab="General", group="Test setup"),
                Evaluate=false,
                HideResult=false);
  parameter Modelica.Units.SI.Temperature T_adsorpt_start = 278.15
    "Start value of equilibrium temperature"
    annotation (Dialog(tab="General", group="Test setup"),
                Evaluate=false,
                HideResult=false);
  parameter Modelica.Units.SI.MolarMass M_adsorptive = 0.018
    "Molar mass of adsorptive"
    annotation (Dialog(tab="General", group="Test setup"),
                Evaluate=true,
                HideResult=true);

  parameter Modelica.Units.SI.PressureDifference dp = 1e-3
    "Pressure difference used to calculated partial derivatives numerically"
    annotation (Dialog(tab="General",group="Numerics"),
                Evaluate=true,
                HideResult=true);
  parameter Modelica.Units.SI.TemperatureDifference dT = 1e-3
    "Temperature difference used to calculated partial derivatives numerically"
    annotation (Dialog(tab="General",group="Numerics"),
                Evaluate=true,
                HideResult=true);
  parameter SorpLib.Units.Uptake dx = 1e-3
    "Uptake difference used to calculated partial derivatives numerically"
    annotation (Dialog(tab="General",group="Numerics"),
                Evaluate=true,
                HideResult=true);

  //
  // Definition of variables
  //
  Modelica.Units.SI.Pressure p_adsorpt(start=p_adsorpt_start, fixed=true)
    "Pressure";
  Modelica.Units.SI.Temperature T_adsorpt(start=T_adsorpt_start, fixed=true)
    "Temperature";
  SorpLib.Units.Uptake x_adsorpt
    "Uptake";

  Medium.ThermodynamicState state_adsorptive_pT
    "State properties of adsorptive at p_adsorpt and T_adsorpt";
  Medium.ThermodynamicState state_bubble_T
    "State properties of bubble point at T_adsorpt";

  Modelica.Units.SI.MolarEnthalpy h_ads(displayUnit="kJ/mol")
    "Molar adsorption enthalpy";

  SorpLib.Units.DerMolarEnthalpyByPressure dh_ads_dp_T
    "Partial derivative of molar adsorption enthalpy w.r.t. pressure at
    constant temperature";
  SorpLib.Units.DerMolarEnthalpyByPressure dh_ads_dp_T_num
    "Partial derivative of molar adsorption enthalpy w.r.t. pressure at
    constant temperature calculated numerically";

  Modelica.Units.SI.MolarHeatCapacity dh_ads_dT_p(displayUnit="kJ/(mol.K)")
    "Partial derivative of molar adsorption enthalpy w.r.t. temperature at
    constant pressure";
  Modelica.Units.SI.MolarHeatCapacity dh_ads_dT_p_num(displayUnit="kJ/(mol.K)")
    "Partial derivative of molar adsorption enthalpy w.r.t. temperature
    calculated numerically at constant pressure";

  Modelica.Units.SI.MolarHeatCapacity dh_ads_dT_x(displayUnit="kJ/(mol.K)")
    "Partial derivative of molar adsorption enthalpy w.r.t. temperature at
    constant uptake";
  Modelica.Units.SI.MolarHeatCapacity dh_ads_dT_x_num(displayUnit="kJ/(mol.K)")
    "Partial derivative of molar adsorption enthalpy w.r.t. temperature
    calculated numerically at constant uptake";

  SorpLib.Units.DerMolarEnthalpyByUptake dh_ads_dx_T
    "Partial derivative of molar adsorption enthalpy w.r.t. uptake at
    constant temperature";
  SorpLib.Units.DerMolarEnthalpyByUptake dh_ads_dx_T_num
    "Partial derivative of molar adsorption enthalpy w.r.t. uptake at
    constant temperature calculated numerically";

  SorpLib.Units.DerMolarEnthalpyByPressure dh_ads_dp_x
    "Partial derivative of molar adsorption enthalpy w.r.t. pressure at
    constant uptake";
  SorpLib.Units.DerMolarEnthalpyByUptake dh_ads_dx_p
    "Partial derivative of molar adsorption enthalpy w.r.t. uptake at
    constant pressure";

protected
  Real c[no_coefficients]
    "Coefficients of the isotherm model";
  Real c_pdT[no_coefficients]
    "Coefficients of the isotherm model: T + dT";
  Real c_mdT[no_coefficients]
    "Coefficients of the isotherm model: T - dT";

  Real dc_dT[no_coefficients]
    "Partial derivative of coefficients of the isotherm model w.r.t. temperature";
  Real dc_dT_pdT[no_coefficients]
    "Partial derivative of coefficients of the isotherm model w.r.t. temperature:
    T + dT";
  Real dc_dT_mdT[no_coefficients]
    "Partial derivative of coefficients of the isotherm model w.r.t. temperature:
    T - dT";

  Real ddc_dT_dT[no_coefficients]
    "Second-order partial derivative of coefficients of the isotherm model w.r.t. 
    temperature";

  Medium.SaturationProperties sat_T
    "Saturated state properties at T_adsorpt";
  Medium.SaturationProperties sat_T_pdT
    "Saturated state properties at T_adsorpt: T + dT";
  Medium.SaturationProperties sat_T_mdT
    "Saturated state properties at T_adsorpt: T - dT";

  Medium.ThermodynamicState state_adsorptive_pT_pdp
    "State properties of adsorptive at p_adsorpt and T_adsorpt: p + dp";
  Medium.ThermodynamicState state_adsorptive_pT_mdp
    "State properties of adsorptive at p_adsorpt and T_adsorpt: p - dp";
  Medium.ThermodynamicState state_adsorptive_pT_pdT
    "State properties of adsorptive at p_adsorpt and T_adsorpt: T + dT";
  Medium.ThermodynamicState state_adsorptive_pT_mdT
    "State properties of adsorptive at p_adsorpt and T_adsorpt: T - dT";

  SorpLib.Units.DerUptakeByPressure dx_adsorpt_dp
    "Partial derivative of uptake w.r.t. pressure at constant temperature";
  SorpLib.Units.DerUptakeByTemperature dx_adsorpt_dT
    "Partial derivative of uptake w.r.t. temperature at constant prssure";

  SorpLib.Units.DerUptakeByPressurePressure ddx_adsorpt_dp_dp
    "Second-order partial derivative of uptake w.r.t. pressure at contant 
    temperature";
  SorpLib.Units.DerUptakeByTemperatureTemperature ddx_adsorpt_dT_dT
    "Second-order partial derivative of uptake w.r.t. temperature at contant 
    pressure";
  SorpLib.Units.DerUptakeByPressureTemperature ddx_adsorpt_dp_dT
    "Second-order partial derivative of uptake w.r.t. pressure and temperature";

  Modelica.Units.SI.MolarEnthalpy h_ads_pdp_T(displayUnit="kJ/mol")
    "Molar adsorption enthalpy at p_adsorpt and T_adsorpt: p + dp";
  Modelica.Units.SI.MolarEnthalpy h_ads_mdp_T(displayUnit="kJ/mol")
    "Molar adsorption enthalpy at p_adsorpt and T_adsorpt: p - dp";

  Modelica.Units.SI.MolarEnthalpy h_ads_pdT_p(displayUnit="kJ/mol")
    "Molar adsorption enthalpy at p_adsorpt and T_adsorpt: T + dT";
  Modelica.Units.SI.MolarEnthalpy h_ads_mdT_p(displayUnit="kJ/mol")
    "Molar adsorption enthalpy at p_adsorpt and T_adsorpt: T - dT";

  Modelica.Units.SI.MolarEnthalpy h_ads_pdT_x(displayUnit="kJ/mol")
    "Molar adsorption enthalpy at p_adsorpt(x_adsorpt, T+dT) and T_adsorpt: T + dT";
  Modelica.Units.SI.MolarEnthalpy h_ads_mdT_x(displayUnit="kJ/mol")
    "Molar adsorption enthalpy at p_adsorpt(x_adsorpt, T-dT) and T_adsorpt: T - dT";

  Modelica.Units.SI.MolarEnthalpy h_ads_pdx_T(displayUnit="kJ/mol")
    "Molar adsorption enthalpy at p_adsorpt(x_adsorpt+dx) and T_adsorpt";
  Modelica.Units.SI.MolarEnthalpy h_ads_mdx_T(displayUnit="kJ/mol")
    "Molar adsorption enthalpy at p_adsorpt(x_adsorpt-dx) and T_adsorpt";

equation
  //
  // Calculation of state properties
  //
  sat_T = Medium.setSat_T(T=T_adsorpt)
    "Saturated state properties at T_adsorpt";
  sat_T_pdT = Medium.setSat_T(T=T_adsorpt+dT)
    "Saturated state properties at T_adsorpt: T + dT";
  sat_T_mdT = Medium.setSat_T(T=T_adsorpt-dT)
    "Saturated state properties at T_adsorpt: T - dT";

  state_adsorptive_pT = Medium.setState_pTX(
    p=p_adsorpt,
    T=T_adsorpt)
    "State properties of adsorptive at p_adsorpt and T_adsorpt";
  state_adsorptive_pT_pdp = Medium.setState_pTX(
    p=p_adsorpt+dp,
    T=T_adsorpt)
    "State properties of adsorptive at p_adsorpt and T_adsorpt: p + dp";
  state_adsorptive_pT_mdp = Medium.setState_pTX(
    p=p_adsorpt-dp,
    T=T_adsorpt)
    "State properties of adsorptive at p_adsorpt and T_adsorpt: p - dp";
  state_adsorptive_pT_pdT = Medium.setState_pTX(
    p=p_adsorpt,
    T=T_adsorpt+dT)
    "State properties of adsorptive at p_adsorpt and T_adsorpt: T + dT";
  state_adsorptive_pT_mdT = Medium.setState_pTX(
    p=p_adsorpt,
    T=T_adsorpt-dT)
    "State properties of adsorptive at p_adsorpt and T_adsorpt: T - dT";

  state_bubble_T = Medium.setBubbleState(sat=sat_T)
    "State properties of bubble point at T_adsorpt";

  //
  // Calculate properties
  //
  dh_ads_dT_x = dh_ads_dT_p - dh_ads_dp_T * dx_adsorpt_dT / dx_adsorpt_dp
    "Partial derivative of molar adsorption enthalpy w.r.t. temperature at
    constant uptake";
  dh_ads_dx_T = dh_ads_dp_T / dx_adsorpt_dp
    "Partial derivative of molar adsorption enthalpy w.r.t. uptake at
    constant temperature";

  dh_ads_dp_x = dh_ads_dp_T - dh_ads_dT_p * dx_adsorpt_dp / dx_adsorpt_dT
    "Partial derivative of molar adsorption enthalpy w.r.t. pressure at
    constant uptake";
  dh_ads_dx_p = dh_ads_dT_p / dx_adsorpt_dT
    "Partial derivative of molar adsorption enthalpy w.r.t. uptake at
    constant pressure";

  //
  // Calculate nuermical derivatives
  //
  dh_ads_dp_T_num = (h_ads_pdp_T - h_ads_mdp_T) / (2*dp)
    "Partial derivative of molar adsorption enthalpy w.r.t. pressure at constant
    temperature calculated numerically";
  dh_ads_dT_p_num = (h_ads_pdT_p - h_ads_mdT_p) / (2*dT)
    "Partial derivative of molar adsorption enthalpy w.r.t. temperature at constant
    pressure calculated numerically";
  dh_ads_dT_x_num = (h_ads_pdT_x - h_ads_mdT_x) / (2*dT)
    "Partial derivative of molar adsorption enthalpy w.r.t. temperature at constant
    uptake calculated numerically";
  dh_ads_dx_T_num = (h_ads_pdx_T - h_ads_mdx_T) / (2*dx)
    "Partial derivative of molar adsorption enthalpy w.r.t. uptake at constant
    temperature calculated numerically";

  //
  // Definition of assertions: Check numerical implementations
  //
  assert(abs(dh_ads_dp_T-dh_ads_dp_T_num) < 1e-6,
    "Partial derivative of h_ads w.r.t. pressure at constant temperature is not " +
    "valied: Deviation (|" +
    String(abs(dh_ads_dp_T-dh_ads_dp_T_num)) +
    "|) is greater than 1e-6 J/mol/Pa!",
    level = AssertionLevel.warning);
  assert(abs(dh_ads_dT_p-dh_ads_dT_p_num) < 1e-6,
    "Partial derivative of h_ads w.r.t. temperature at constant pressure is not " +
    "valied: Deviation (|" +
    String(abs(dh_ads_dT_p-dh_ads_dT_p_num)) +
    "|) is greater than 1e-6 J/mol/K!",
    level = AssertionLevel.warning);
  assert(abs(dh_ads_dT_x-dh_ads_dT_x_num) < 1e-6,
    "Partial derivative of h_ads w.r.t. temperature at constant uptake is not " +
    "valied: Deviation (|" +
    String(abs(dh_ads_dT_x-dh_ads_dT_x_num)) +
    "|) is greater than 1e-6 J/mol/K!",
    level = AssertionLevel.warning);
  assert(abs(dh_ads_dx_T-dh_ads_dx_T_num) < 1e-6,
    "Partial derivative of h_ads w.r.t. uptake at constant temperature is not " +
    "valied: Deviation (|" +
    String(abs(dh_ads_dx_T-dh_ads_dx_T_num)) +
    "|) is greater than 1e-6 J.kg/mol/kg!",
    level = AssertionLevel.warning);

  //
  // Annotations
  //
  annotation (
    Icon(coordinateSystem(preserveAspectRatio=false)),
    Diagram(coordinateSystem(preserveAspectRatio=false)),
    Documentation(info="<html>
<p>
This partial model is the basic model for all testers of sorption enthalpies for
pure components. This partial model defines all some relevant parameters and 
variables that are required for most sorption enthalpy models.
<br/><br/>
Models that inherit properties from this partial model have to specify the 
<i>medium</i> and test setup. Besides, the coefficients of the isotherm model 
(i.e., <i>c</i>, <i>c_pdT</i>, and <i>c_mdT</i>) and their partial derivatives 
with respect to temperature (i.e., <i>dc_dT</i>, <i>dc_dT_pdT</i>, <i>dc_dT_mdT</i>, 
and <i>ddc_dT_dT</i>) have to be implemented. Additionally, equations for the
following variables must be implemted: <i>p_adsorpt</i>, <i>T_adsorpt</i>, 
<i>x_adsorpt</i>, <i>dx_adsorpt_dp</i>, <i>dx_adsorpt_dT</i>, <i>ddx_adsorpt_dp_dp</i>,
<i>ddx_adsorpt_dT_dT</i>, <i>ddx_adsorpt_dp_dT</i>, <i>h_ads</i>, <i>dh_ads_dp_T</i>, 
<i>dh_ads_dT_p</i>, <i>h_ads_pdp_T</i>, <i>h_ads_mdp_T</i>, <i>h_ads_pdT_p</i>, 
<i>h_ads_mdT_p</i>, <i>h_ads_pdT_x</i>, <i>h_ads_mdT_x</i>, <i>h_ads_pdx_T</i>, 
and <i>h_ads_mdx_T</i>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 17, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end PartialTestPure;
