within SorpLib.Media.Functions.SorptionEnthalpies;
package BasesClasses "Base classes used to build new functions calculating sorption enthalpies"
  extends Modelica.Icons.BasesPackage;

annotation (Documentation(revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>", info="<html>
<p>
This package contains partial basic functions and models. These partial functions
and models contain fundamental definitions of functions calculating sorption enthalpies
for pure and multi-component adsorption. The content of this package is only of 
interest when adding new functions to the library.
</p>
</html>"));
end BasesClasses;
