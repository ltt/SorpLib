within SorpLib.Media.Functions.SorptionEnthalpies.BasesClasses;
partial function PartialPure_h_ads
  "Base function for functions calculating the sorption enthalpy for pure components"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.Temperature T_adsorpt
    "Temperature"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of outputs
  //
  output Modelica.Units.SI.MolarEnthalpy h_ads(displayUnit="kJ/mol")
    "Molar adsorption enthalpy"
    annotation (Dialog(
      tab="General",
      group="Outputs",
      enable=false));

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial function is the basic function for functions that calculate the
sorption enthalpy for pure components.
<br/><br/>
This partial function defines the temperature <i>T_adsorpt</i> as input and the 
sorption enthalpy <i>h_ads</i> as output.
<br/><br/>
Functions that inherit properties from this partial function may have to add
furhter inputs, outputs, and the function algorithm.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 17, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end PartialPure_h_ads;
