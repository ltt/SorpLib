within SorpLib.Media.Functions.SorptionEnthalpies.BasesClasses;
partial function PartialPure_dh_ads_dT
  "Base function for functions calculating the partial derivative of the sorption enthalpy w.r.t. temperature at constant pressure for pure components"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.Temperature T_adsorpt
    "Temperature"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of outputs
  //
  output Modelica.Units.SI.MolarHeatCapacity dh_ads_dT(displayUnit="kJ/(mol.K)")
    "Partial derivative of molar adsorption enthalpy w.r.t. temperature at constant
    pressure"
    annotation (Dialog(
      tab="General",
      group="Outputs",
      enable=false));

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial function is the basic function for functions that calculate the
partial derivative of the sorption enthalpy with respect to the temperature at
consstant pressure for pure components.
<br/><br/>
This partial function defines the temperature <i>T_adsorpt</i> as input and the
partial derivative of the sorption enthalpy fwith respect to the temperature at
constant pressure <i>dh_ads_dT</i> as output.
<br/><br/>
Functions that inherit properties from this partial function may have to add
furhter inputs, outputs, and the function algorithm.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 17, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end PartialPure_dh_ads_dT;
