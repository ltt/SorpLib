within SorpLib.Media.Functions.SorptionEnthalpies.PureComponents;
function h_ads_Dubinin
  "Molar adsorption enthalpy according to the model of Dubinin"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.MolarMass M_adsorptive
    "Molar mass of the adsorptive"
    annotation (Dialog(tab="General", group="Inputs"));
  extends
    SorpLib.Media.Functions.SorptionEnthalpies.BasesClasses.PartialPure_h_ads;
  input SorpLib.Units.Uptake x_adsorpt
    "Uptake"
    annotation (Dialog(tab="General", group="Inputs"));

  input Modelica.Units.SI.SpecificEnthalpy h_adsorptiveToLiquid
    "Specific enthalpy difference between adsorptive state and saturated liquid
    state (i.e., bubble point)"
    annotation (Dialog(tab="General", group="Inputs"));

  input Modelica.Units.SI.SpecificVolume v_adsorpt
    "Specific volume of the adsorpt"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.RelativePressureCoefficient beta_adsorpt
    "Isobaric expansion coefficient of the adsorpt phase"
    annotation (Dialog(tab="General", group="Inputs"));

  input SorpLib.Units.MolarAdsorptionPotential A
    "Molar adsorption potential"
    annotation (Dialog(tab="General", group="Inputs"));
  input SorpLib.Units.DerFilledPoreVolumeByAdsorptionPotential dW_dA
    "Partial derivative of filled pore volume w.r.t. molar adsorption potential
    at constant pressure and temperature"
    annotation (Dialog(tab="General", group="Inputs"));

algorithm
  h_ads := M_adsorptive * h_adsorptiveToLiquid + A -
    beta_adsorpt * T_adsorpt * v_adsorpt * x_adsorpt / dW_dA
    "Molar adsorption enthalpy";

  //
  // Annotations
  //
  annotation (Inline=true,
Documentation(info="<html>
<p>
This function calculates the molar adsorption enthalpy according to the model of
Dubinin.
</p>

<h4>Main equations</h4>
<p>
The molar adsorption enthalpy <i>&Delta;h<sub>ads</sub></i> is defined as difference
of the specific enthalpy of the gas/vapor phase <i>h<sub>adsorptive</sub></i> and the
adsorpt phase <i>h<sub>adsorpt</sub></i>:
</p>
<pre>
    &Delta;h<sub>ads</sub> = h<sub>adsorptive</sub> - h<sub>adsorpt</sub> = M<sub>adsorptive</sub> * &Delta;h<sub>adsorptiveToLiquid</sub> + A - &beta; * T * v<sub>adsorpt</sub> * x * 1 / (&part;W/&part;A)<sub>p,T</sub>;
</pre>
<p>
Herein, <i>M<sub>adsorptive</sub></i> is the molar mass of the adsorptive, <i>T</i> 
is the temperature, <i>x</i> is the uptake, <i>v<sub>adsorpt</sub></i> is the specific 
volume of the adsorpt, <i>&beta;</i> is the isobaric expansion coefficient of the 
adsorpt, <i>&Delta;h<sub>adsorptiveToLiquid</sub></i> is the specific enthalpy difference
between adsorptive phase and saturated liquid phase (i.e., bubble point), <i>A</i> is 
the molar adsorption potential, and <i>(&part;W/&part;A)<sub>p,T</sub></i> is the partial 
derivative of the filled pore volume with respect to the molar adsorption potential at 
constant pressure and temperature.
</p>

<h4>Assumptions and limitations</h4>
<ul>
  <li>
  Inert sorbent.
  </li>
</ul>

<h4>References</h4>
<ul>
  <li>
  Schwamberger, V. (2016). Thermodynamic and numerical investigation of a novel sorption cycle for application in adsorption heat pumps and chillers (in German), PhD thesis, Karlsruhe.
</ul>
</html>", revisions="<html>
<ul>
  <li>
  November 17, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end h_ads_Dubinin;
