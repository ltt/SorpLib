within SorpLib.Media.Functions.SorptionEnthalpies.PureComponents;
function h_ads_clausiusClapeyron
  "Molar adsorption enthalpy according to Clausius Clapeyron"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.Pressure p_adsorpt
    "Pressure"
    annotation (Dialog(tab="General", group="Inputs"));
  extends
    SorpLib.Media.Functions.SorptionEnthalpies.BasesClasses.PartialPure_h_ads;

  input SorpLib.Units.DerUptakeByPressure dx_adsorpt_dp
    "Partial derivative of uptake w.r.t. pressure at constant temperature"
    annotation (Dialog(tab="General", group="Inputs"));
  input SorpLib.Units.DerUptakeByTemperature dx_adsorpt_dT
    "Partial derivative of uptake w.r.t. temperature at constant pressure"
    annotation (Dialog(tab="General", group="Inputs"));

algorithm
  h_ads := Modelica.Constants.R * T_adsorpt^2 / p_adsorpt *
    (-dx_adsorpt_dT / dx_adsorpt_dp)
    "Molar adsorption enthalpy";

  //
  // Annotations
  //
  annotation (Inline=true,
Documentation(info="<html>
<p>
This function calculates the molar adsorption enthalpy according to Clausius
Clapeyron assumptions.
</p>

<h4>Main equations</h4>
<p>
The molar adsorption enthalpy <i>&Delta;h<sub>ads</sub></i> is defined as difference
of the specific enthalpy of the gas/vapor phase <i>h<sub>adsorptive</sub></i> and the
adsorpt phase <i>h<sub>adsorpt</sub></i>:
</p>
<pre>
    &Delta;h<sub>ads</sub> = h<sub>adsorptive</sub> - h<sub>adsorpt</sub> = R * T<sup>2</sup> / p * (&part;p/&part;T)<sub>x</sub> = R * T<sup>2</sup> / p * (-(&part;x/&part;T)<sub>p</sub> / (&part;x/&part;p)<sub>T</sub>);
</pre>
<p>
Herein, <i>R</i> is the ideal gas constant, <i>p</i> is the pressure, <i>T</i> is
the temperature, <i>(&part;x/&part;p)<sub>T</sub></i> is the partial derivative of 
the uptake with respect to the pressure at constant temperature, and 
<i>(&part;x/&part;T)<sub>p</sub></i> is the partial derivative  of the uptake with 
respect to the temperature at constant pressure.
</p>

<h4>Assumptions and limitations</h4>
<ul>
  <li>
  The specific volume of the adsorpt phase can be neglected compared to the specific
  volume of the adsorptive phase.
  </li>
  <li>
  The adsorptive can be treated as an ideal gas (i.e., p * V = n * R * T).
  </li>
  <li>
  Inert sorbent.
  </li>
</ul>

<h4>References</h4>
<ul>
  <li>
  Bathen, D. and Breitbach, M. (2001). Adsorptionstechnik (in German), 1st Edition, ISBN 3-540-41908-X, Springer-Verlag Berlin Heidelberg New York.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  November 17, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end h_ads_clausiusClapeyron;
