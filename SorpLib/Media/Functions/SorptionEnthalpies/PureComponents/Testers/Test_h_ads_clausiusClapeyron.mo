within SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.Testers;
model Test_h_ads_clausiusClapeyron
  "Tester for the function 'h_ads_clausiusClapeyron' and all corresponding functions"
  extends
    SorpLib.Media.Functions.SorptionEnthalpies.BasesClasses.PartialTestPure(
    final no_coefficients = 6);

  //
  // Definition of variables
  //
protected
  Modelica.Units.SI.Pressure p_adsorpt_pdT_x
    "Pressure at x_adsorpt and T_adsorpt + dT";
  Modelica.Units.SI.Pressure p_adsorpt_mdT_x
    "Pressure at x_adsorpt and T_adsorpt - dT";

  Modelica.Units.SI.Pressure p_adsorpt_pdx_T
    "Pressure at T_adsorpt and x_adsorpt + xT";
  Modelica.Units.SI.Pressure p_adsorpt_mdx_T
    "Pressure at T_adsorpt and x_adsorpt - xT";

  SorpLib.Units.DerUptakeByPressure dx_adsorpt_dp_pdT_p
    "Partial derivative of uptake w.r.t. pressure at constant temperature: T + dT";
  SorpLib.Units.DerUptakeByPressure dx_adsorpt_dp_mdT_p
    "Partial derivative of uptake w.r.t. pressure at constant temperature: T - dT";

  SorpLib.Units.DerUptakeByPressure dx_adsorpt_dp_pdp_T
    "Partial derivative of uptake w.r.t. pressure at constant temperature: p + dp";
  SorpLib.Units.DerUptakeByPressure dx_adsorpt_dp_mdp_T
    "Partial derivative of uptake w.r.t. pressure at constant temperature: p - dp";

  SorpLib.Units.DerUptakeByPressure dx_adsorpt_dp_pdT_x
    "Partial derivative of uptake w.r.t. pressure at constant temperature: x_adsorpt
    and T + dT";
  SorpLib.Units.DerUptakeByPressure dx_adsorpt_dp_mdT_x
    "Partial derivative of uptake w.r.t. pressure at constant temperature: x_adsorpt
    and T - dT";

  SorpLib.Units.DerUptakeByPressure dx_adsorpt_dp_pdx_T
    "Partial derivative of uptake w.r.t. pressure at constant temperature: x + dx";
  SorpLib.Units.DerUptakeByPressure dx_adsorpt_dp_mdx_T
    "Partial derivative of uptake w.r.t. pressure at constant temperature: x - dx";

  SorpLib.Units.DerUptakeByTemperature dx_adsorpt_dT_pdT_p
    "Partial derivative of uptake w.r.t. temperature at constant pressure: T + dT";
  SorpLib.Units.DerUptakeByTemperature dx_adsorpt_dT_mdT_p
    "Partial derivative of uptake w.r.t. temperature at constant pressure: T - dT";

  SorpLib.Units.DerUptakeByTemperature dx_adsorpt_dT_pdp_T
    "Partial derivative of uptake w.r.t. temperature at constant pressure: p + dp";
  SorpLib.Units.DerUptakeByTemperature dx_adsorpt_dT_mdp_T
    "Partial derivative of uptake w.r.t. temperature at constant pressure: p - dp";

  SorpLib.Units.DerUptakeByTemperature dx_adsorpt_dT_pdT_x
    "Partial derivative of uptake w.r.t. temperature at constant pressure: x_adsorpt
    and T + dT";
  SorpLib.Units.DerUptakeByTemperature dx_adsorpt_dT_mdT_x
    "Partial derivative of uptake w.r.t. temperature at constant pressure: x_adsorpt
    and T - dT";

  SorpLib.Units.DerUptakeByTemperature dx_adsorpt_dT_pdx_T
    "Partial derivative of uptake w.r.t. temperature at constant pressure: x + dx";
  SorpLib.Units.DerUptakeByTemperature dx_adsorpt_dT_mdx_T
    "Partial derivative of uptake w.r.t. temperature at constant pressure: x - dx";

equation
  //
  // Definition of derivatives
  //
  der(p_adsorpt) = 0.75 * dc_dT[1]
    "Predecsriped slope of p_adsorpt";
  der(T_adsorpt) = 90/20
    "Predecsriped slope of T_adsorpt";

  //
  // Calculate coefficients of the isotherm model
  //
  c[1] = state_bubble_T.p;
  c[2] = state_bubble_T.d;
  c[3] = 5.072313e-1 / 1000;
  c[4] = 1.305531e2 * 1000 * 0.018;
  c[5] = -8.492403e1 * 1000 * 0.018;
  c[6] = 4.128962e-3 / 1000;

  c_pdT[1] = sat_T_pdT.psat;
  c_pdT[2] = Medium.bubbleDensity(sat=sat_T_pdT);
  c_pdT[3] = 5.072313e-1 / 1000;
  c_pdT[4] = 1.305531e2 * 1000 * 0.018;
  c_pdT[5] = -8.492403e1 * 1000 * 0.018;
  c_pdT[6] = 4.128962e-3 / 1000;

  c_mdT[1] = sat_T_mdT.psat;
  c_mdT[2] = Medium.bubbleDensity(sat=sat_T_mdT);
  c_mdT[3] = 5.072313e-1 / 1000;
  c_mdT[4] = 1.305531e2 * 1000 * 0.018;
  c_mdT[5] = -8.492403e1 * 1000 * 0.018;
  c_mdT[6] = 4.128962e-3 / 1000;

  //
  // Calculate partial derivatives of coefficients of the isotherm model w.r.t.
  // temperature
  //
  dc_dT[1] = 1/Medium.saturationTemperature_derp_sat(sat=sat_T);
  dc_dT[2] = Medium.dBubbleDensity_dPressure(sat=sat_T) * dc_dT[1];
  dc_dT[3] = 0;
  dc_dT[4] = 0;
  dc_dT[5] = 0;
  dc_dT[6] = 0;

  dc_dT_pdT[1] = 1/Medium.saturationTemperature_derp_sat(sat=sat_T_pdT);
  dc_dT_pdT[2] = Medium.dBubbleDensity_dPressure(sat=sat_T_pdT) * dc_dT_pdT[1];
  dc_dT_pdT[3] = 0;
  dc_dT_pdT[4] = 0;
  dc_dT_pdT[5] = 0;
  dc_dT_pdT[6] = 0;

  dc_dT_mdT[1] = 1/Medium.saturationTemperature_derp_sat(sat=sat_T_mdT);
  dc_dT_mdT[2] = Medium.dBubbleDensity_dPressure(sat=sat_T_mdT) * dc_dT_mdT[1];
  dc_dT_mdT[3] = 0;
  dc_dT_mdT[4] = 0;
  dc_dT_mdT[5] = 0;
  dc_dT_mdT[6] = 0;

  //
  // Calculate second-order partial derivatives of coefficients of the isotherm
  // model w.r.t. temperature
  //
  ddc_dT_dT[1] = (dc_dT_pdT[1] - dc_dT_mdT[1]) / (2*dT);
  ddc_dT_dT[2] = (dc_dT_pdT[2] - dc_dT_mdT[2]) / (2*dT);
  ddc_dT_dT[3] = 0;
  ddc_dT_dT[4] = 0;
  ddc_dT_dT[5] = 0;
  ddc_dT_dT[6] = 0;

  //
  // Calculate properties
  //
  x_adsorpt = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.x_pT(
    p_adsorpt=p_adsorpt,
    T_adsorpt=T_adsorpt,
    c=c,
    p_adsorpt_lb_start=1,
    p_adsorpt_ub_start=10,
    tolerance=100*Modelica.Constants.eps)
    "Uptake";

  dx_adsorpt_dp = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.dx_dp(
    p_adsorpt=p_adsorpt,
    T_adsorpt=T_adsorpt,
    c=c)
    "Partial derivative of uptake w.r.t. pressure at constant temperature";
  dx_adsorpt_dT = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.dx_dT(
    p_adsorpt=p_adsorpt,
    T_adsorpt=T_adsorpt,
    c=c,
    dc_dT_adsorpt=dc_dT)
    "Partial derivative of uptake w.r.t. temperature at constant pressure";

  ddx_adsorpt_dp_dp = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.ddx_dp_dp(
    p_adsorpt=p_adsorpt,
    T_adsorpt=T_adsorpt,
    c=c)
    "Second-order partial derivative of uptake w.r.t. pressure at constant temperature";
  ddx_adsorpt_dT_dT = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.ddx_dT_dT(
    p_adsorpt=p_adsorpt,
    T_adsorpt=T_adsorpt,
    c=c,
    dc_dT_adsorpt=dc_dT,
    ddc_dT_adsorpt_dT_adsorpt=ddc_dT_dT)
    "Second-order partial derivative of uptake w.r.t. temperature at constant pressure";
  ddx_adsorpt_dp_dT = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.ddx_dp_dT(
    p_adsorpt=p_adsorpt,
    T_adsorpt=T_adsorpt,
    c=c,
    dc_dT_adsorpt=dc_dT)
    "Second-order partial derivative of uptake w.r.t. pressure and temperature";

  h_ads = SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.h_ads_clausiusClapeyron(
    p_adsorpt=p_adsorpt,
    T_adsorpt=T_adsorpt,
    dx_adsorpt_dp=dx_adsorpt_dp,
    dx_adsorpt_dT=dx_adsorpt_dT)
    "Molar adsorption enthalpy";

  dh_ads_dp_T = SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.dh_ads_dp_clausiusClapeyron(
    p_adsorpt=p_adsorpt,
    T_adsorpt=T_adsorpt,
    dx_adsorpt_dp=dx_adsorpt_dp,
    dx_adsorpt_dT=dx_adsorpt_dT,
    ddx_adsorpt_dp_dp=ddx_adsorpt_dp_dp,
    ddx_adsorpt_dp_dT=ddx_adsorpt_dp_dT)
    "Partial derivative of molar adsorption enthalpy w.r.t. pressure at
    constant temperature";
  dh_ads_dT_p = SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.dh_ads_dT_clausiusClapeyron(
    p_adsorpt=p_adsorpt,
    T_adsorpt=T_adsorpt,
    dx_adsorpt_dp=dx_adsorpt_dp,
    dx_adsorpt_dT=dx_adsorpt_dT,
    ddx_adsorpt_dp_dT=ddx_adsorpt_dp_dT,
    ddx_adsorpt_dT_dT=ddx_adsorpt_dT_dT)
    "Partial derivative of molar adsorption enthalpy w.r.t. temperature at
    constant pressure";

  //
  // Calculate pressures for numerical derivatives
  //
  p_adsorpt_pdT_x = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.p_xT(
    x_adsorpt=x_adsorpt,
    T_adsorpt=T_adsorpt+dT,
    c=c_pdT,
    p_adsorpt_lb_start=1,
    p_adsorpt_ub_start=10,
    tolerance=100*Modelica.Constants.eps)
    "Pressure at x_adsorpt and T_adsorpt + dT";
  p_adsorpt_mdT_x = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.p_xT(
    x_adsorpt=x_adsorpt,
    T_adsorpt=T_adsorpt-dT,
    c=c_mdT,
    p_adsorpt_lb_start=1,
    p_adsorpt_ub_start=10,
    tolerance=100*Modelica.Constants.eps)
    "Pressure at x_adsorpt and T_adsorpt - dT";

  p_adsorpt_pdx_T = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.p_xT(
    x_adsorpt=x_adsorpt+dx,
    T_adsorpt=T_adsorpt,
    c=c,
    p_adsorpt_lb_start=1,
    p_adsorpt_ub_start=10,
    tolerance=100*Modelica.Constants.eps)
    "Pressure at T_adsorpt and x_adsorpt + dx";
  p_adsorpt_mdx_T = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.p_xT(
    x_adsorpt=x_adsorpt-dx,
    T_adsorpt=T_adsorpt,
    c=c,
    p_adsorpt_lb_start=1,
    p_adsorpt_ub_start=10,
    tolerance=100*Modelica.Constants.eps)
    "Pressure at T_adsorpt and x_adsorpt - dx";

  //
  // Calculate partial derivatives of uptake w.r.t. pressure at constant temperature
  // for numerical derivatives
  //
  dx_adsorpt_dp_pdp_T = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.dx_dp(
    p_adsorpt=p_adsorpt+dp,
    T_adsorpt=T_adsorpt,
    c=c)
    "Partial derivative of uptake w.r.t. pressure at constant temperature: p + dp";
  dx_adsorpt_dp_mdp_T = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.dx_dp(
    p_adsorpt=p_adsorpt-dp,
    T_adsorpt=T_adsorpt,
    c=c)
    "Partial derivative of uptake w.r.t. pressure at constant temperature: p - dp";

  dx_adsorpt_dp_pdT_p = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.dx_dp(
    p_adsorpt=p_adsorpt,
    T_adsorpt=T_adsorpt+dT,
    c=c_pdT)
    "Partial derivative of uptake w.r.t. pressure at constant temperature: T + dT";
  dx_adsorpt_dp_mdT_p = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.dx_dp(
    p_adsorpt=p_adsorpt,
    T_adsorpt=T_adsorpt-dT,
    c=c_mdT)
    "Partial derivative of uptake w.r.t. pressure at constant temperature: T - dT";

  dx_adsorpt_dp_pdT_x = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.dx_dp(
    p_adsorpt=p_adsorpt_pdT_x,
    T_adsorpt=T_adsorpt+dT,
    c=c_pdT)
    "Partial derivative of uptake w.r.t. pressure at constant temperature: x_adsorpt 
    and T + dT";
  dx_adsorpt_dp_mdT_x = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.dx_dp(
    p_adsorpt=p_adsorpt_mdT_x,
    T_adsorpt=T_adsorpt-dT,
    c=c_mdT)
    "Partial derivative of uptake w.r.t. pressure at constant temperature: x_adsorpt 
    and T - dT";

  dx_adsorpt_dp_pdx_T = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.dx_dp(
    p_adsorpt=p_adsorpt_pdx_T,
    T_adsorpt=T_adsorpt,
    c=c)
    "Partial derivative of uptake w.r.t. pressure at constant temperature: T_adsorpt 
    and x + dx";
  dx_adsorpt_dp_mdx_T = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.dx_dp(
    p_adsorpt=p_adsorpt_mdx_T,
    T_adsorpt=T_adsorpt,
    c=c)
    "Partial derivative of uptake w.r.t. pressure at constant temperature: T_adsorpt 
    and x - dx";

  //
  // Calculate partial derivatives of uptake w.r.t. temperature at constant pressure
  // for numerical derivatives
  //
  dx_adsorpt_dT_pdp_T = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.dx_dT(
    p_adsorpt=p_adsorpt+dp,
    T_adsorpt=T_adsorpt,
    c=c,
    dc_dT_adsorpt=dc_dT)
    "Partial derivative of uptake w.r.t. temperature at constant pressure: p + dp";
  dx_adsorpt_dT_mdp_T = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.dx_dT(
    p_adsorpt=p_adsorpt-dp,
    T_adsorpt=T_adsorpt,
    c=c,
    dc_dT_adsorpt=dc_dT)
    "Partial derivative of uptake w.r.t. temperature at constant pressure: p - dp";

  dx_adsorpt_dT_pdT_p = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.dx_dT(
    p_adsorpt=p_adsorpt,
    T_adsorpt=T_adsorpt+dT,
    c=c_pdT,
    dc_dT_adsorpt=dc_dT_pdT)
    "Partial derivative of uptake w.r.t. temperature at constant pressure: T + dT";
  dx_adsorpt_dT_mdT_p = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.dx_dT(
    p_adsorpt=p_adsorpt,
    T_adsorpt=T_adsorpt-dT,
    c=c_mdT,
    dc_dT_adsorpt=dc_dT_mdT)
    "Partial derivative of uptake w.r.t. temperature at constant pressure: T - dT";

  dx_adsorpt_dT_pdT_x = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.dx_dT(
    p_adsorpt=p_adsorpt_pdT_x,
    T_adsorpt=T_adsorpt+dT,
    c=c_pdT,
    dc_dT_adsorpt=dc_dT_pdT)
    "Partial derivative of uptake w.r.t. temperature at constant pressure: x_adsorpt
    and T + dT";
  dx_adsorpt_dT_mdT_x = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.dx_dT(
    p_adsorpt=p_adsorpt_mdT_x,
    T_adsorpt=T_adsorpt-dT,
    c=c_mdT,
    dc_dT_adsorpt=dc_dT_mdT)
    "Partial derivative of uptake w.r.t. temperature at constant pressure: x_adsorpt
    and T - dT";

  dx_adsorpt_dT_pdx_T = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.dx_dT(
    p_adsorpt=p_adsorpt_pdx_T,
    T_adsorpt=T_adsorpt,
    c=c,
    dc_dT_adsorpt=dc_dT)
    "Partial derivative of uptake w.r.t. temperature at constant pressure: T_adsorpt
    and x + dx";
  dx_adsorpt_dT_mdx_T = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.dx_dT(
    p_adsorpt=p_adsorpt_mdx_T,
    T_adsorpt=T_adsorpt,
    c=c,
    dc_dT_adsorpt=dc_dT)
    "Partial derivative of uptake w.r.t. temperature at constant pressure: T_adsorpt
    and x - dx";

  //
  // Calculate molar adsorption potentials for numerical derivatives
  //
  h_ads_pdp_T = SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.h_ads_clausiusClapeyron(
    p_adsorpt=p_adsorpt+dp,
    T_adsorpt=T_adsorpt,
    dx_adsorpt_dp=dx_adsorpt_dp_pdp_T,
    dx_adsorpt_dT=dx_adsorpt_dT_pdp_T)
    "Molar adsorption enthalpy at p_adsorpt and T_adsorpt: p + dp";
  h_ads_mdp_T = SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.h_ads_clausiusClapeyron(
    p_adsorpt=p_adsorpt-dp,
    T_adsorpt=T_adsorpt,
    dx_adsorpt_dp=dx_adsorpt_dp_mdp_T,
    dx_adsorpt_dT=dx_adsorpt_dT_mdp_T)
    "Molar adsorption enthalpy at p_adsorpt and T_adsorpt: p - dp";

  h_ads_pdT_p = SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.h_ads_clausiusClapeyron(
    p_adsorpt=p_adsorpt,
    T_adsorpt=T_adsorpt+dT,
    dx_adsorpt_dp=dx_adsorpt_dp_pdT_p,
    dx_adsorpt_dT=dx_adsorpt_dT_pdT_p)
    "Molar adsorption enthalpy at p_adsorpt and T_adsorpt: T + dT";
  h_ads_mdT_p = SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.h_ads_clausiusClapeyron(
    p_adsorpt=p_adsorpt,
    T_adsorpt=T_adsorpt-dT,
    dx_adsorpt_dp=dx_adsorpt_dp_mdT_p,
    dx_adsorpt_dT=dx_adsorpt_dT_mdT_p)
    "Molar adsorption enthalpy at p_adsorpt and T_adsorpt: T - dT";

  h_ads_pdT_x = SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.h_ads_clausiusClapeyron(
    p_adsorpt=p_adsorpt_pdT_x,
    T_adsorpt=T_adsorpt+dT,
    dx_adsorpt_dp=dx_adsorpt_dp_pdT_x,
    dx_adsorpt_dT=dx_adsorpt_dT_pdT_x)
    "Molar adsorption enthalpy at p_adsorpt(x_adsorpt, T+dT) and T_adsorpt: T + dT";
  h_ads_mdT_x = SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.h_ads_clausiusClapeyron(
    p_adsorpt=p_adsorpt_mdT_x,
    T_adsorpt=T_adsorpt-dT,
    dx_adsorpt_dp=dx_adsorpt_dp_mdT_x,
    dx_adsorpt_dT=dx_adsorpt_dT_mdT_x)
    "Molar adsorption enthalpy at p_adsorpt(x_adsorpt, T-dT) and T_adsorpt: T - dT";

  h_ads_pdx_T = SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.h_ads_clausiusClapeyron(
    p_adsorpt=p_adsorpt_pdx_T,
    T_adsorpt=T_adsorpt,
    dx_adsorpt_dp=dx_adsorpt_dp_pdx_T,
    dx_adsorpt_dT=dx_adsorpt_dT_pdx_T)
    "Molar adsorption enthalpy at p_adsorpt(x_adsorpt+dx) and T_adsorpt";
  h_ads_mdx_T = SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.h_ads_clausiusClapeyron(
    p_adsorpt=p_adsorpt_mdx_T,
    T_adsorpt=T_adsorpt,
    dx_adsorpt_dp=dx_adsorpt_dp_mdx_T,
    dx_adsorpt_dT=dx_adsorpt_dT_mdx_T)
    "Molar adsorption enthalpy at p_adsorpt(x_adsorpt-dx) and T_adsorpt";

  //
  // Annotations
  //
  annotation (experiment(
      StopTime=20,
      Tolerance=1e-06),
Documentation(revisions="<html>
<ul>
  <li>
  November 17, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>", info="<html>
<p>
This tester shows the behavior of the 'h_ads_ClausiusClaperon' function and its 
partial derivative with resprect to temperature.
<br/><br/>
To see the function behavior, plot the variables <i>h_ads_i</i> over the time. The 
simulation time is correctly preset (Start: 0 s, Stop = 20 s). 
</p>
</html>"));
end Test_h_ads_clausiusClapeyron;
