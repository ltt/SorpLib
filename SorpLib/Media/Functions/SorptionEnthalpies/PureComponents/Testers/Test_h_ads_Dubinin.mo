within SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.Testers;
model Test_h_ads_Dubinin
  "Tester for the function 'h_ads_Dubinin' and all corresponding functions"
  extends
    SorpLib.Media.Functions.SorptionEnthalpies.BasesClasses.PartialTestPure(
    final no_coefficients = 6);

  //
  // Definition of variables
  //
  Modelica.Units.SI.SpecificEnthalpy h_adsorptiveToLiquid
    "Specific enthalpy difference between adsorptive state and saturated liquid
    state (i.e., bubble point)";

  Modelica.Units.SI.RelativePressureCoefficient beta_adsorpt
    "Isobaric expansion coefficient of the adsorpt phase";

  SorpLib.Units.MolarAdsorptionPotential A
    "Molar adsorption potential";

protected
  Medium.ThermodynamicState state_bubble_T_pdT
    "State properties of bubble point at T_adsorpt: T + dT";
  Medium.ThermodynamicState state_bubble_T_mdT
    "State properties of bubble point at T_adsorpt: T - dT";

  SorpLib.Units.Uptake x_adsorpt_pdp_T
    "Uptake: T_adsorpt and p + dp";
  SorpLib.Units.Uptake x_adsorpt_mdp_T
    "Uptake: T_adsorpt and p - dp";
  SorpLib.Units.Uptake x_adsorpt_pdT_p
    "Uptake: p_adsorpt and T + dT";
  SorpLib.Units.Uptake x_adsorpt_mdT_p
    "Uptake: p_adsorpt and T - dT";

  Modelica.Units.SI.Pressure p_adsorpt_pdT_x
    "Pressure at x_adsorpt and T_adsorpt + dT";
  Modelica.Units.SI.Pressure p_adsorpt_mdT_x
    "Pressure at x_adsorpt and T_adsorpt - dT";
  Modelica.Units.SI.Pressure p_adsorpt_pdx_T
    "Pressure at T_adsorpt and x_adsorpt + xT";
  Modelica.Units.SI.Pressure p_adsorpt_mdx_T
    "Pressure at T_adsorpt and x_adsorpt - xT";

  Modelica.Units.SI.SpecificEnthalpy h_adsorptiveToLiquid_pdp
    "Specific enthalpy difference between adsorptive state and saturated liquid
    state (i.e., bubble point): p + dp";
  Modelica.Units.SI.SpecificEnthalpy h_adsorptiveToLiquid_mdp
    "Specific enthalpy difference between adsorptive state and saturated liquid
    state (i.e., bubble point): p - dp";
  Modelica.Units.SI.SpecificEnthalpy h_adsorptiveToLiquid_pdT
    "Specific enthalpy difference between adsorptive state and saturated liquid
    state (i.e., bubble point): T + dT";
  Modelica.Units.SI.SpecificEnthalpy h_adsorptiveToLiquid_mdT
    "Specific enthalpy difference between adsorptive state and saturated liquid
    state (i.e., bubble point): T - dT";
  Modelica.Units.SI.SpecificEnthalpy h_adsorptiveToLiquid_pdT_x
    "Specific enthalpy difference between adsorptive state and saturated liquid
    state (i.e., bubble point): x_adsorpt and T + dT";
  Modelica.Units.SI.SpecificEnthalpy h_adsorptiveToLiquid_mdT_x
    "Specific enthalpy difference between adsorptive state and saturated liquid
    state (i.e., bubble point): x_adsorpt and T - dT";
  Modelica.Units.SI.SpecificEnthalpy h_adsorptiveToLiquid_pdx_T
    "Specific enthalpy difference between adsorptive state and saturated liquid
    state (i.e., bubble point): x_adsorpt + dx and T";
  Modelica.Units.SI.SpecificEnthalpy h_adsorptiveToLiquid_mdx_T
    "Specific enthalpy difference between adsorptive state and saturated liquid
    state (i.e., bubble point): x_adsorpt - dx and T";

  SorpLib.Units.DerSpecificEnthalpyByPressure dh_adsorptiveToLiquid_dp
    "Partial derivative of specific enthalpy difference between adsorptive state 
    and saturated liquid state (i.e., bubble point) w.r.t. pressure at constant 
    temperature";
  Modelica.Units.SI.SpecificHeatCapacity dh_adsorptiveToLiquid_dT
    "Partial derivative of specific enthalpy difference between adsorptive state 
    and saturated liquid state (i.e., bubble point) w.r.t. temperature at constant
    pressure";

  SorpLib.Units.DerIsobaricExpansionCoefficientByTemperature dbeta_adsorpt_dT
    "Partial derivative of isobaric expansion coefficient of the adsorpt phase
    w.r.t. temperature at constant pressure";

  SorpLib.Units.MolarAdsorptionPotential A_pdp_T
    "Molar adsorption potential: T_adsorpt p + dp";
  SorpLib.Units.MolarAdsorptionPotential A_mdp_T
    "Molar adsorption potential: T_adsorpt and p - dp";
  SorpLib.Units.MolarAdsorptionPotential A_pdT_p
    "Molar adsorption potential: p_adsorpt and T + dT";
  SorpLib.Units.MolarAdsorptionPotential A_mdT_p
    "Molar adsorption potential: p_adsorpt and T - dT";
  SorpLib.Units.MolarAdsorptionPotential A_pdT_x
    "Molar adsorption potential: x_adsorpt and T + dT";
  SorpLib.Units.MolarAdsorptionPotential A_mdT_x
    "Molar adsorption potential: x_adsorpt and T - dT";
  SorpLib.Units.MolarAdsorptionPotential A_pdx_T
    "Molar adsorption potential: T_adsorpt and x + dx";
  SorpLib.Units.MolarAdsorptionPotential A_mdx_T
    "Molar adsorption potential: T_adsorpt and x - dx";

  SorpLib.Units.DerMolarAdsorptionPotentialByPressure dA_dp
    "Partial derivative of molar adsorption potential w.r.t. pressure at
    constant temperature";
  SorpLib.Units.DerMolarAdsorptionPotentialByTemperature dA_dT
    "Partial derivative of molar adsorption potential w.r.t. temperature at
    constant pressure";

  SorpLib.Units.DerFilledPoreVolumeByAdsorptionPotential dW_dA
    "Partial derivative of filled pore volume w.r.t. molar adsorption potential
    at constant pressure and temperature";
  SorpLib.Units.DerFilledPoreVolumeByAdsorptionPotentialAdsorptionPotential ddW_dA_dA
    "Partial derivative of filled pore volume w.r.t. molar adsorption potential
    at constant pressure and temperature";
  SorpLib.Units.DerFilledPoreVolumeByAdsorptionPotentialTemperature ddW_dA_dT
    "Second-order partial derivative of filled pore volume w.r.t. molar adsorption 
    potential and temperature at constant pressure";

  SorpLib.Units.DerFilledPoreVolumeByAdsorptionPotential dW_dA_pdp_T
    "Partial derivative of filled pore volume w.r.t. molar adsorption potential: 
    T_adsorpt and p + dp";
  SorpLib.Units.DerFilledPoreVolumeByAdsorptionPotential dW_dA_mdp_T
    "Partial derivative of filled pore volume w.r.t. molar adsorption potential: 
    T_adsorpt and p + dp";
  SorpLib.Units.DerFilledPoreVolumeByAdsorptionPotential dW_dA_pdT_p
    "Partial derivative of filled pore volume w.r.t. molar adsorption potential: 
    p_adsorpt and T + dT";
  SorpLib.Units.DerFilledPoreVolumeByAdsorptionPotential dW_dA_mdT_p
    "Partial derivative of filled pore volume w.r.t. molar adsorption potential: 
    p_adsorpt and T - dT";
  SorpLib.Units.DerFilledPoreVolumeByAdsorptionPotential dW_dA_pdT_x
    "Partial derivative of filled pore volume w.r.t. molar adsorption potential: 
    x_adsorpt and T + dT";
  SorpLib.Units.DerFilledPoreVolumeByAdsorptionPotential dW_dA_mdT_x
    "Partial derivative of filled pore volume w.r.t. molar adsorption potential: 
    x_adsorpt and T - dT";
  SorpLib.Units.DerFilledPoreVolumeByAdsorptionPotential dW_dA_pdx_T
    "Partial derivative of filled pore volume w.r.t. molar adsorption potential: 
    T_adsorpt and x + dx";
  SorpLib.Units.DerFilledPoreVolumeByAdsorptionPotential dW_dA_mdx_T
    "Partial derivative of filled pore volume w.r.t. molar adsorption potential: 
    T_adsorpt and x + dx";

equation
  //
  // Definition of derivatives
  //
  der(p_adsorpt) = 0.75 * dc_dT[1]
    "Predecsriped slope of p_adsorpt";
  der(T_adsorpt) = 90/20
    "Predecsriped slope of T_adsorpt";

  //
  // Calculate coefficients of the isotherm model
  //
  c[1] = state_bubble_T.p;
  c[2] = state_bubble_T.d;
  c[3] = 5.072313e-1 / 1000;
  c[4] = 1.305531e2 * 1000 * 0.018;
  c[5] = -8.492403e1 * 1000 * 0.018;
  c[6] = 4.128962e-3 / 1000;

  c_pdT[1] = sat_T_pdT.psat;
  c_pdT[2] = Medium.bubbleDensity(sat=sat_T_pdT);
  c_pdT[3] = 5.072313e-1 / 1000;
  c_pdT[4] = 1.305531e2 * 1000 * 0.018;
  c_pdT[5] = -8.492403e1 * 1000 * 0.018;
  c_pdT[6] = 4.128962e-3 / 1000;

  c_mdT[1] = sat_T_mdT.psat;
  c_mdT[2] = Medium.bubbleDensity(sat=sat_T_mdT);
  c_mdT[3] = 5.072313e-1 / 1000;
  c_mdT[4] = 1.305531e2 * 1000 * 0.018;
  c_mdT[5] = -8.492403e1 * 1000 * 0.018;
  c_mdT[6] = 4.128962e-3 / 1000;

  //
  // Calculate partial derivatives of coefficients of the isotherm model w.r.t.
  // temperature
  //
  dc_dT[1] = 1/Medium.saturationTemperature_derp_sat(sat=sat_T);
  dc_dT[2] = Medium.dBubbleDensity_dPressure(sat=sat_T) * dc_dT[1];
  dc_dT[3] = 0;
  dc_dT[4] = 0;
  dc_dT[5] = 0;
  dc_dT[6] = 0;

  dc_dT_pdT[1] = 1/Medium.saturationTemperature_derp_sat(sat=sat_T_pdT);
  dc_dT_pdT[2] = Medium.dBubbleDensity_dPressure(sat=sat_T_pdT) * dc_dT_pdT[1];
  dc_dT_pdT[3] = 0;
  dc_dT_pdT[4] = 0;
  dc_dT_pdT[5] = 0;
  dc_dT_pdT[6] = 0;

  dc_dT_mdT[1] = 1/Medium.saturationTemperature_derp_sat(sat=sat_T_mdT);
  dc_dT_mdT[2] = Medium.dBubbleDensity_dPressure(sat=sat_T_mdT) * dc_dT_mdT[1];
  dc_dT_mdT[3] = 0;
  dc_dT_mdT[4] = 0;
  dc_dT_mdT[5] = 0;
  dc_dT_mdT[6] = 0;

  //
  // Calculate second-order partial derivatives of coefficients of the isotherm
  // model w.r.t. temperature
  //
  ddc_dT_dT[1] = (dc_dT_pdT[1] - dc_dT_mdT[1]) / (2*dT);
  ddc_dT_dT[2] = (dc_dT_pdT[2] - dc_dT_mdT[2]) / (2*dT);
  ddc_dT_dT[3] = 0;
  ddc_dT_dT[4] = 0;
  ddc_dT_dT[5] = 0;
  ddc_dT_dT[6] = 0;

  //
  // Calculation of state properties
  //
  state_bubble_T_pdT = Medium.setBubbleState(sat=sat_T_pdT)
    "State properties of bubble point at T_adsorpt: T + dT";
  state_bubble_T_mdT = Medium.setBubbleState(sat=sat_T_mdT)
    "State properties of bubble point at T_adsorpt: T - dT";

  //
  // Calculate properties
  //
  x_adsorpt = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.x_pT(
    p_adsorpt=p_adsorpt,
    T_adsorpt=T_adsorpt,
    c=c,
    p_adsorpt_lb_start=1,
    p_adsorpt_ub_start=10,
    tolerance=100*Modelica.Constants.eps)
    "Uptake";

  dx_adsorpt_dp = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.dx_dp(
    p_adsorpt=p_adsorpt,
    T_adsorpt=T_adsorpt,
    c=c)
    "Partial derivative of uptake w.r.t. pressure at constant temperature";
  dx_adsorpt_dT = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.dx_dT(
    p_adsorpt=p_adsorpt,
    T_adsorpt=T_adsorpt,
    c=c,
    dc_dT_adsorpt=dc_dT)
    "Partial derivative of uptake w.r.t. temperature at constant pressure";

  ddx_adsorpt_dp_dp = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.ddx_dp_dp(
    p_adsorpt=p_adsorpt,
    T_adsorpt=T_adsorpt,
    c=c)
    "Second-order partial derivative of uptake w.r.t. pressure at constant temperature";
  ddx_adsorpt_dT_dT = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.ddx_dT_dT(
    p_adsorpt=p_adsorpt,
    T_adsorpt=T_adsorpt,
    c=c,
    dc_dT_adsorpt=dc_dT,
    ddc_dT_adsorpt_dT_adsorpt=ddc_dT_dT)
    "Second-order partial derivative of uptake w.r.t. temperature at constant pressure";
  ddx_adsorpt_dp_dT = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.ddx_dp_dT(
    p_adsorpt=p_adsorpt,
    T_adsorpt=T_adsorpt,
    c=c,
    dc_dT_adsorpt=dc_dT)
    "Second-order partial derivative of uptake w.r.t. pressure and temperature";

  h_adsorptiveToLiquid = state_adsorptive_pT.h - state_bubble_T.h
    "Specific enthalpy difference between adsorptive state and saturated liquid
    state (i.e., bubble point)";
  dh_adsorptiveToLiquid_dp = 1/state_adsorptive_pT.d * (1 - T_adsorpt *
    Medium.isobaricExpansionCoefficient(state=state_adsorptive_pT)) - 0
    "Partial derivative of specific enthalpy difference between adsorptive state 
    and saturated liquid state (i.e., bubble point) w.r.t. pressure at constant 
    temperature";
  dh_adsorptiveToLiquid_dT =
    Medium.specificHeatCapacityCp(state=state_adsorptive_pT) -
    Medium.specificHeatCapacityCp(state=state_bubble_T)
    "Partial derivative of specific enthalpy difference between adsorptive state 
    and saturated liquid state (i.e., bubble point) w.r.t. temperature at constant
    pressure";

  beta_adsorpt = Medium.isobaricExpansionCoefficient(state=state_bubble_T)
    "Isobaric expansion coefficient of the adsorpt phase";
  dbeta_adsorpt_dT =
    (Medium.isobaricExpansionCoefficient(state=state_bubble_T_pdT) -
     Medium.isobaricExpansionCoefficient(state=state_bubble_T_mdT)) /(2*dT)
    "Partial derivative of the ssobaric expansion coefficient of the adsorpt phase
    w.r.t. temperature at constant pressure";

  A =SorpLib.Media.Functions.SorptionEquilibria.Utilities.A_ppsT(
    p_adsorpt=p_adsorpt,
    p_sat=c[1],
    T_adsorpt=T_adsorpt)
    "Molar adsorption potential";
  dA_dp =SorpLib.Media.Functions.SorptionEquilibria.Utilities.dA_dp(
    p_adsorpt=p_adsorpt,
    p_sat=c[1],
    T_adsorpt=T_adsorpt)
    "Partial derivative of molar adsorption potential w.r.t. pressure at
    constant temperature";
  dA_dT =SorpLib.Media.Functions.SorptionEquilibria.Utilities.dA_dT(
    p_adsorpt=p_adsorpt,
    p_sat=c[1],
    T_adsorpt=T_adsorpt,
    dp_sat_dT_adsorpt=dc_dT[1])
    "Partial derivative of molar adsorption potential w.r.t. temperature at
    constant pressure";

  dW_dA = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.dW_dA(
    A=A,
    c=c)
    "Partial derivative of filled pore volume w.r.t. molar adsorption potential
    at constant pressure and temperature";
  ddW_dA_dA = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.ddW_dA_dA(
    A=A,
    c=c)
    "Second-order prtial derivative of filled pore volume w.r.t. molar adsorption potential
    at constant pressure and temperature";
  ddW_dA_dT = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.ddW_dA_dT(
    p_adsorpt=p_adsorpt,
    T_adsorpt=T_adsorpt,
    A=A,
    c=c,
    dc_dT_adsorpt=dc_dT)
    "Partial derivative of filled pore volume w.r.t. molar adsorption potential
    and temperature at constant pressure";

  h_ads = SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.h_ads_Dubinin(
    M_adsorptive=M_adsorptive,
    T_adsorpt=T_adsorpt,
    x_adsorpt=x_adsorpt,
    h_adsorptiveToLiquid=h_adsorptiveToLiquid,
    v_adsorpt=1/state_bubble_T.d,
    beta_adsorpt=beta_adsorpt,
    A=A,
    dW_dA=dW_dA)
    "Molar adsorption enthalpy";

  dh_ads_dp_T = SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.dh_ads_dp_Dubinin(
    M_adsorptive=M_adsorptive,
    T_adsorpt=T_adsorpt,
    x_adsorpt=x_adsorpt,
    dx_adsorpt_dp=dx_adsorpt_dp,
    h_adsorptiveToLiquid=h_adsorptiveToLiquid,
    dh_adsorptiveToLiquid_dp=dh_adsorptiveToLiquid_dp,
    v_adsorpt=1/state_bubble_T.d,
    dv_adsorpt_dp=0,
    beta_adsorpt=beta_adsorpt,
    dbeta_adsorpt_dp=0,
    dA_dp=dA_dp,
    dW_dA=dW_dA,
    ddW_dA_dA=ddW_dA_dA)
    "Partial derivative of molar adsorption enthalpy w.r.t. pressure at
    constant temperature";
  dh_ads_dT_p = SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.dh_ads_dT_Dubinin(
    M_adsorptive=M_adsorptive,
    T_adsorpt=T_adsorpt,
    x_adsorpt=x_adsorpt,
    dx_adsorpt_dT=dx_adsorpt_dT,
    h_adsorptiveToLiquid=h_adsorptiveToLiquid,
    dh_adsorptiveToLiquid_dT=dh_adsorptiveToLiquid_dT,
    v_adsorpt=1/state_bubble_T.d,
    dv_adsorpt_dT=-1/state_bubble_T.d^2 * dc_dT[2],
    beta_adsorpt=beta_adsorpt,
    dbeta_adsorpt_dT=dbeta_adsorpt_dT,
    dA_dT=dA_dT,
    dW_dA=dW_dA,
    ddW_dA_dT=ddW_dA_dT)
    "Partial derivative of molar adsorption enthalpy w.r.t. temperature at
    constant pressure";

  //
  // Calculate loadings for numerical derivatives
  //
  x_adsorpt_pdp_T = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.x_pT(
    p_adsorpt=p_adsorpt+dp,
    T_adsorpt=T_adsorpt,
    c=c,
    p_adsorpt_lb_start=1,
    p_adsorpt_ub_start=10,
    tolerance=100*Modelica.Constants.eps)
    "Uptake: T_adsorpt and p + dp";
  x_adsorpt_mdp_T = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.x_pT(
    p_adsorpt=p_adsorpt-dp,
    T_adsorpt=T_adsorpt,
    c=c,
    p_adsorpt_lb_start=1,
    p_adsorpt_ub_start=10,
    tolerance=100*Modelica.Constants.eps)
    "Uptake: p_adsorpt and p - dp";

  x_adsorpt_pdT_p = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.x_pT(
    p_adsorpt=p_adsorpt,
    T_adsorpt=T_adsorpt+dT,
    c=c_pdT,
    p_adsorpt_lb_start=1,
    p_adsorpt_ub_start=10,
    tolerance=100*Modelica.Constants.eps)
    "Uptake: p_adsorpt and T + dT";
  x_adsorpt_mdT_p = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.x_pT(
    p_adsorpt=p_adsorpt,
    T_adsorpt=T_adsorpt-dT,
    c=c_mdT,
    p_adsorpt_lb_start=1,
    p_adsorpt_ub_start=10,
    tolerance=100*Modelica.Constants.eps)
    "Uptake: p_adsorpt and T - dT";

  //
  // Calculate pressures for numerical derivatives
  //
  p_adsorpt_pdT_x = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.p_xT(
    x_adsorpt=x_adsorpt,
    T_adsorpt=T_adsorpt+dT,
    c=c_pdT,
    p_adsorpt_lb_start=1,
    p_adsorpt_ub_start=10,
    tolerance=100*Modelica.Constants.eps)
    "Pressure at x_adsorpt and T_adsorpt + dT";
  p_adsorpt_mdT_x = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.p_xT(
    x_adsorpt=x_adsorpt,
    T_adsorpt=T_adsorpt-dT,
    c=c_mdT,
    p_adsorpt_lb_start=1,
    p_adsorpt_ub_start=10,
    tolerance=100*Modelica.Constants.eps)
    "Pressure at x_adsorpt and T_adsorpt - dT";

  p_adsorpt_pdx_T = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.p_xT(
    x_adsorpt=x_adsorpt+dx,
    T_adsorpt=T_adsorpt,
    c=c,
    p_adsorpt_lb_start=1,
    p_adsorpt_ub_start=10,
    tolerance=100*Modelica.Constants.eps)
    "Pressure at T_adsorpt and x_adsorpt + dx";
  p_adsorpt_mdx_T = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.p_xT(
    x_adsorpt=x_adsorpt-dx,
    T_adsorpt=T_adsorpt,
    c=c,
    p_adsorpt_lb_start=1,
    p_adsorpt_ub_start=10,
    tolerance=100*Modelica.Constants.eps)
    "Pressure at T_adsorpt and x_adsorpt - dx";

  //
  // Calculate further properties for numerical derivatives
  //
  h_adsorptiveToLiquid_pdp = state_adsorptive_pT_pdp.h - state_bubble_T.h
    "Specific enthalpy difference between adsorptive state and saturated liquid
    state (i.e., bubble point): p + dp";
  h_adsorptiveToLiquid_mdp = state_adsorptive_pT_mdp.h - state_bubble_T.h
    "Specific enthalpy difference between adsorptive state and saturated liquid
    state (i.e., bubble point): p - dp";

  h_adsorptiveToLiquid_pdT = state_adsorptive_pT_pdT.h - state_bubble_T_pdT.h
    "Specific enthalpy difference between adsorptive state and saturated liquid
    state (i.e., bubble point): T + dT";
  h_adsorptiveToLiquid_mdT = state_adsorptive_pT_mdT.h - state_bubble_T_mdT.h
    "Specific enthalpy difference between adsorptive state and saturated liquid
    state (i.e., bubble point): T - dT";

  h_adsorptiveToLiquid_pdT_x = Medium.specificEnthalpy_pT(
     p=p_adsorpt_pdT_x, T=T_adsorpt+dT) - state_bubble_T_pdT.h
    "Specific enthalpy difference between adsorptive state and saturated liquid
    state (i.e., bubble point): x_adsorpt and T + dT";
  h_adsorptiveToLiquid_mdT_x = Medium.specificEnthalpy_pT(
     p=p_adsorpt_mdT_x, T=T_adsorpt-dT) - state_bubble_T_mdT.h
    "Specific enthalpy difference between adsorptive state and saturated liquid
    state (i.e., bubble point): x_adsorpt and T - dT";

  h_adsorptiveToLiquid_pdx_T = Medium.specificEnthalpy_pT(
     p=p_adsorpt_pdx_T, T=T_adsorpt) - state_bubble_T.h
    "Specific enthalpy difference between adsorptive state and saturated liquid
    state (i.e., bubble point): x_adsorpt + dx and T";
  h_adsorptiveToLiquid_mdx_T = Medium.specificEnthalpy_pT(
     p=p_adsorpt_mdx_T, T=T_adsorpt) - state_bubble_T.h
    "Specific enthalpy difference between adsorptive state and saturated liquid
    state (i.e., bubble point): x_adsorpt - dx and T";


  A_pdp_T =SorpLib.Media.Functions.SorptionEquilibria.Utilities.A_ppsT(
    p_adsorpt=p_adsorpt+dp,
    p_sat=c[1],
    T_adsorpt=T_adsorpt)
    "Molar adsorption potential: T_adsorpt p + dp";
  A_mdp_T =SorpLib.Media.Functions.SorptionEquilibria.Utilities.A_ppsT(
    p_adsorpt=p_adsorpt-dp,
    p_sat=c[1],
    T_adsorpt=T_adsorpt)
    "Molar adsorption potential: T_adsorpt and p - dp";

  A_pdT_p =SorpLib.Media.Functions.SorptionEquilibria.Utilities.A_ppsT(
    p_adsorpt=p_adsorpt,
    p_sat=c_pdT[1],
    T_adsorpt=T_adsorpt+dT)
    "Molar adsorption potential: p_adsorpt and T + dT";
  A_mdT_p =SorpLib.Media.Functions.SorptionEquilibria.Utilities.A_ppsT(
    p_adsorpt=p_adsorpt,
    p_sat=c_mdT[1],
    T_adsorpt=T_adsorpt-dT)
    "Molar adsorption potential: p_adsorpt and T - dT";

  A_pdT_x =SorpLib.Media.Functions.SorptionEquilibria.Utilities.A_ppsT(
    p_adsorpt=p_adsorpt_pdT_x,
    p_sat=c_pdT[1],
    T_adsorpt=T_adsorpt+dT)
    "Molar adsorption potential: x_adsorpt and T + dT";
  A_mdT_x =SorpLib.Media.Functions.SorptionEquilibria.Utilities.A_ppsT(
    p_adsorpt=p_adsorpt_mdT_x,
    p_sat=c_mdT[1],
    T_adsorpt=T_adsorpt-dT)
    "Molar adsorption potential: x_adsorpt and T - dT";

  A_pdx_T =SorpLib.Media.Functions.SorptionEquilibria.Utilities.A_ppsT(
    p_adsorpt=p_adsorpt_pdx_T,
    p_sat=c[1],
    T_adsorpt=T_adsorpt)
    "Molar adsorption potential: T_adsorpt and x + dx";
  A_mdx_T =SorpLib.Media.Functions.SorptionEquilibria.Utilities.A_ppsT(
    p_adsorpt=p_adsorpt_mdx_T,
    p_sat=c[1],
    T_adsorpt=T_adsorpt)
    "Molar adsorption potential: T_adsorpt and x - dx";

  dW_dA_pdp_T = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.dW_dA(
    A=A_pdp_T,
    c=c)
    "Partial derivative of filled pore volume w.r.t. molar adsorption potential: 
    T_adsorpt and p + dp";
  dW_dA_mdp_T = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.dW_dA(
    A=A_mdp_T,
    c=c)
    "Partial derivative of filled pore volume w.r.t. molar adsorption potential: 
    T_adsorpt and p + dp";

  dW_dA_pdT_p = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.dW_dA(
    A=A_pdT_p,
    c=c_pdT)
    "Partial derivative of filled pore volume w.r.t. molar adsorption potential: 
    p_adsorpt and T + dT";
  dW_dA_mdT_p = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.dW_dA(
    A=A_mdT_p,
    c=c_mdT)
    "Partial derivative of filled pore volume w.r.t. molar adsorption potential: 
    p_adsorpt and T + dT";

  dW_dA_pdT_x = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.dW_dA(
    A=A_pdT_x,
    c=c_pdT)
    "Partial derivative of filled pore volume w.r.t. molar adsorption potential: 
    x_adsorpt and T + dT";
  dW_dA_mdT_x = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.dW_dA(
    A=A_mdT_x,
    c=c_mdT)
    "Partial derivative of filled pore volume w.r.t. molar adsorption potential: 
    x_adsorpt and T + dT";

  dW_dA_pdx_T = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.dW_dA(
    A=A_pdx_T,
    c=c)
    "Partial derivative of filled pore volume w.r.t. molar adsorption potential: 
    T_adsorpt and x + dx";
  dW_dA_mdx_T = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.dW_dA(
    A=A_mdx_T,
    c=c)
    "Partial derivative of filled pore volume w.r.t. molar adsorption potential: 
    T_adsorpt and x + dx";

  //
  // Calculate molar adsorption potentials for numerical derivatives
  //
  h_ads_pdp_T = SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.h_ads_Dubinin(
    M_adsorptive=M_adsorptive,
    T_adsorpt=T_adsorpt,
    x_adsorpt=x_adsorpt_pdp_T,
    h_adsorptiveToLiquid=h_adsorptiveToLiquid_pdp,
    v_adsorpt=1/state_bubble_T.d,
    beta_adsorpt=Medium.isobaricExpansionCoefficient(state=state_bubble_T),
    A=A_pdp_T,
    dW_dA=dW_dA_pdp_T)
    "Molar adsorption enthalpy at p_adsorpt and T_adsorpt: p + dp";
  h_ads_mdp_T = SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.h_ads_Dubinin(
    M_adsorptive=M_adsorptive,
    T_adsorpt=T_adsorpt,
    x_adsorpt=x_adsorpt_mdp_T,
    h_adsorptiveToLiquid=h_adsorptiveToLiquid_mdp,
    v_adsorpt=1/state_bubble_T.d,
    beta_adsorpt=Medium.isobaricExpansionCoefficient(state=state_bubble_T),
    A=A_mdp_T,
    dW_dA=dW_dA_mdp_T)
    "Molar adsorption enthalpy at p_adsorpt and T_adsorpt: p - dp";

  h_ads_pdT_p = SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.h_ads_Dubinin(
    M_adsorptive=M_adsorptive,
    T_adsorpt=T_adsorpt+dT,
    x_adsorpt=x_adsorpt_pdT_p,
    h_adsorptiveToLiquid=h_adsorptiveToLiquid_pdT,
    v_adsorpt=1/state_bubble_T_pdT.d,
    beta_adsorpt=Medium.isobaricExpansionCoefficient(state=state_bubble_T_pdT),
    A=A_pdT_p,
    dW_dA=dW_dA_pdT_p)
    "Molar adsorption enthalpy at p_adsorpt and T_adsorpt: T + dT";
  h_ads_mdT_p = SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.h_ads_Dubinin(
    M_adsorptive=M_adsorptive,
    T_adsorpt=T_adsorpt-dT,
    x_adsorpt=x_adsorpt_mdT_p,
    h_adsorptiveToLiquid=h_adsorptiveToLiquid_mdT,
    v_adsorpt=1/state_bubble_T_mdT.d,
    beta_adsorpt=Medium.isobaricExpansionCoefficient(state=state_bubble_T_mdT),
    A=A_mdT_p,
    dW_dA=dW_dA_mdT_p)
    "Molar adsorption enthalpy at p_adsorpt and T_adsorpt: T - dT";

  h_ads_pdT_x = SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.h_ads_Dubinin(
    M_adsorptive=M_adsorptive,
    T_adsorpt=T_adsorpt+dT,
    x_adsorpt=x_adsorpt,
    h_adsorptiveToLiquid=h_adsorptiveToLiquid_pdT_x,
    v_adsorpt=1/state_bubble_T_pdT.d,
    beta_adsorpt=Medium.isobaricExpansionCoefficient(state=state_bubble_T_pdT),
    A=A_pdT_x,
    dW_dA=dW_dA_pdT_x)
    "Molar adsorption enthalpy at p_adsorpt(x_adsorpt, T+dT) and T_adsorpt: T + dT";
  h_ads_mdT_x = SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.h_ads_Dubinin(
    M_adsorptive=M_adsorptive,
    T_adsorpt=T_adsorpt-dT,
    x_adsorpt=x_adsorpt,
    h_adsorptiveToLiquid=h_adsorptiveToLiquid_mdT_x,
    v_adsorpt=1/state_bubble_T_mdT.d,
    beta_adsorpt=Medium.isobaricExpansionCoefficient(state=state_bubble_T_mdT),
    A=A_mdT_x,
    dW_dA=dW_dA_mdT_x)
    "Molar adsorption enthalpy at p_adsorpt(x_adsorpt, T-dT) and T_adsorpt: T - dT";

  h_ads_pdx_T = SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.h_ads_Dubinin(
    M_adsorptive=M_adsorptive,
    T_adsorpt=T_adsorpt,
    x_adsorpt=x_adsorpt+dx,
    h_adsorptiveToLiquid=h_adsorptiveToLiquid_pdx_T,
    v_adsorpt=1/state_bubble_T.d,
    beta_adsorpt=Medium.isobaricExpansionCoefficient(state=state_bubble_T),
    A=A_pdx_T,
    dW_dA=dW_dA_pdx_T)
    "Molar adsorption enthalpy at p_adsorpt(x_adsorpt+dx) and T_adsorpt";
  h_ads_mdx_T = SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.h_ads_Dubinin(
    M_adsorptive=M_adsorptive,
    T_adsorpt=T_adsorpt,
    x_adsorpt=x_adsorpt-dx,
    h_adsorptiveToLiquid=h_adsorptiveToLiquid_mdx_T,
    v_adsorpt=1/state_bubble_T.d,
    beta_adsorpt=Medium.isobaricExpansionCoefficient(state=state_bubble_T),
    A=A_mdx_T,
    dW_dA=dW_dA_mdx_T)
    "Molar adsorption enthalpy at p_adsorpt(x_adsorpt-dx) and T_adsorpt";

  //
  // Annotations
  //
  annotation (experiment(
      StopTime=20,
      Tolerance=1e-06),
Documentation(revisions="<html>
<ul>
  <li>
  November 17, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>", info="<html>
<p>
This tester shows the behavior of the 'h_ads_Dubinin' function and its 
partial derivative with resprect to temperature.
<br/><br/>
To see the function behavior, plot the variables <i>h_ads_i</i> over the time. The 
simulation time is correctly preset (Start: 0 s, Stop = 20 s). 
</p>
</html>"));
end Test_h_ads_Dubinin;
