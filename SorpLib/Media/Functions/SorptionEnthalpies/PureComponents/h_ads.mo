within SorpLib.Media.Functions.SorptionEnthalpies.PureComponents;
function h_ads
  "Molar adsorption enthalpy"

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.MolarMass M_adsorptive
    "Molar mass of the adsorptive"
    annotation (Dialog(tab="General", group="Inputs"));
  extends
    SorpLib.Media.Functions.SorptionEnthalpies.BasesClasses.PartialPure_h_ads;

  input Modelica.Units.SI.SpecificVolume v_adsorptive
    "Specific volume of the adsorptive"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.SpecificVolume v_adsorpt
    "Specific volume of the adsorpt"
    annotation (Dialog(tab="General", group="Inputs"));

  input SorpLib.Units.DerUptakeByPressure dx_adsorpt_dp
    "Partial derivative of uptake w.r.t. pressure"
    annotation (Dialog(tab="General", group="Inputs"));
  input SorpLib.Units.DerUptakeByTemperature dx_adsorpt_dT
    "Partial derivative of uptake w.r.t. temperature"
    annotation (Dialog(tab="General", group="Inputs"));

algorithm
  h_ads := T_adsorpt * M_adsorptive * (v_adsorptive - v_adsorpt) *
    (-dx_adsorpt_dT / dx_adsorpt_dp)
    "Molar adsorption enthalpy";

  //
  // Annotations
  //
  annotation (Inline=true,
Documentation(info="<html>
<p>
This function calculates the molar adsorption enthalpy.
</p>

<h4>Main equations</h4>
<p>
The molar adsorption enthalpy <i>&Delta;h<sub>ads</sub></i> is defined as difference
of the specific enthalpy of the gas/vapor phase <i>h<sub>adsorptive</sub></i> and the
adsorpt phase <i>h<sub>adsorpt</sub></i>:
</p>
<pre>
    &Delta;h<sub>ads</sub> = h<sub>adsorptive</sub> - h<sub>adsorpt</sub> = T * M<sub>adsorptive</sub> * (v<sub>adsorptive</sub> - v<sub>adsorpt</sub>) (dp/dT) &asymp; T * M<sub>adsorptive</sub> * (v<sub>adsorptive</sub> - v<sub>adsorpt</sub>) * (-(&part;x/&part;T)<sub>p</sub> / (&part;x/&part;p)<sub>T</sub>);
</pre>
<p>
Herein, <i>M<sub>adsorptive</sub></i> is the molar mass of the adsorptive, <i>T</i> 
is the temperature, <i>v<sub>adsorptive</sub></i> ist the specific volume of the 
adsorptive, <i>v<sub>adsorpt</sub></i> ist the specific volume of the adsorpt, 
<i>dx/dp</i> is the partial derivative of the uptake with respect to the pressure
at constant temperature, and <i>dx/dT</i> is the partial derivative of the uptake 
with respect to the temperature at constant pressure.
</p>

<h4>Assumptions and limitations</h4>
<ul>
  <li>
  Inert sorbent.
  </li>
  <li>
  Neglecting the term <i>(&part;p/&part;x)<sub>T</sub> * (dx/dp)</i> of term 
  <i>dp/dT = -(&part;x/&part;T)<sub>p</sub> / (&part;x/&part;p)<sub>T</sub> +
  (&part;p/&part;x)<sub>T</sub> * 
  (dx/dp)</i>.
  </li>
</ul>

<h4>References</h4>
<ul>
  <li>
  Bathen, D. and Breitbach, M. (2001). Adsorptionstechnik (in German), 1st Edition, ISBN 3-540-41908-X, Springer-Verlag Berlin Heidelberg New York.
  </li>
  <li>
  Chakraborty, A. and Saha, B.B. and Ng, K.C. and Koyama (2006). On the thermodynamic modeling of the isosteric heat of adsorption and comparison with experiments, Applied Physics Letters, 89:171901. DOI: http://doi.org/10.1063/1.2360925.
  </li>
  <li>
  Chakraborty, A. and Saha, B.B. and Ng, K.C. and Koyama, S. and Srinivasan, K. (2009). Theoretical Insight of Physical Adsorption for a Single-Component Adsorbent + Adsorbate System: I. Thermodynamic Property Surfaces, Langmuir, 25:2204-221. DOI: http://doi.org/10.1021/la803289p.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  November 17, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end h_ads;
