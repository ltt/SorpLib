within SorpLib.Media.Functions.SorptionEnthalpies.PureComponents;
function dh_ads_dT_Dubinin
  "Partial derivative of molar adsorption enthalpy according to the model of Dubinin w.r.t. temperature at constant pressure"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.MolarMass M_adsorptive
    "Molar mass of the adsorptive"
    annotation (Dialog(tab="General", group="Inputs"));
  extends
    SorpLib.Media.Functions.SorptionEnthalpies.BasesClasses.PartialPure_dh_ads_dT;

  input SorpLib.Units.Uptake x_adsorpt
    "Uptake"
    annotation (Dialog(tab="General", group="Inputs"));
  input SorpLib.Units.DerUptakeByTemperature dx_adsorpt_dT
    "Partial derivative of uptake w.r.t. temperature at constant pressure"
    annotation (Dialog(tab="General", group="Inputs"));

  input Modelica.Units.SI.SpecificEnthalpy h_adsorptiveToLiquid
    "Specific enthalpy difference between adsorptive state and saturated liquid
    state (i.e., bubble point)"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.SpecificHeatCapacity dh_adsorptiveToLiquid_dT
    "Partial derivative of specific enthalpy difference between adsorptive state 
    and saturated liquid state (i.e., bubble point) w.r.t. temperature at constant
    pressure"
    annotation (Dialog(tab="General", group="Inputs"));

  input Modelica.Units.SI.SpecificVolume v_adsorpt
    "Specific volume of the adsorpt"
    annotation (Dialog(tab="General", group="Inputs"));
  input SorpLib.Units.DerSpecificVolumeByTemperature dv_adsorpt_dT
    "Partial derivative of specific volume of the adsorpt w.r.t. temperature at
    constant pressure"
    annotation (Dialog(tab="General", group="Inputs"));

  input Modelica.Units.SI.RelativePressureCoefficient beta_adsorpt
    "Isobaric expansion coefficient of the adsorpt phase"
    annotation (Dialog(tab="General", group="Inputs"));
  input SorpLib.Units.DerIsobaricExpansionCoefficientByTemperature dbeta_adsorpt_dT
    "Partial derivative of isobaric expansion coefficient of the adsorpt phase
    w.r.t. temperature at constant pressure"
    annotation (Dialog(tab="General", group="Inputs"));

  input SorpLib.Units.DerMolarAdsorptionPotentialByTemperature dA_dT
    "Partial derivative of molar adsorption potential w.r.t. temperature at
    constant pressure"
    annotation (Dialog(tab="General", group="Inputs"));
  input SorpLib.Units.DerFilledPoreVolumeByAdsorptionPotential dW_dA
    "Partial derivative of filled pore volume w.r.t. molar adsorption potential
    at constant pressure and temperature"
    annotation (Dialog(tab="General", group="Inputs"));
  input SorpLib.Units.DerFilledPoreVolumeByAdsorptionPotentialTemperature ddW_dA_dT
    "Second-order partial derivative of filled pore volume w.r.t. molar 
    adsorption potential and temperature at constant pressure"
    annotation (Dialog(tab="General", group="Inputs"));

algorithm
  dh_ads_dT :=
    (M_adsorptive) * dh_adsorptiveToLiquid_dT +
    (1) * dA_dT +
    (-beta_adsorpt * v_adsorpt * x_adsorpt / dW_dA) * 1 +
    (-T_adsorpt * v_adsorpt * x_adsorpt / dW_dA) * dbeta_adsorpt_dT +
    (-beta_adsorpt * T_adsorpt * x_adsorpt / dW_dA) * dv_adsorpt_dT +
    (-beta_adsorpt * T_adsorpt * v_adsorpt / dW_dA) * dx_adsorpt_dT +
    (beta_adsorpt * T_adsorpt * v_adsorpt * x_adsorpt / dW_dA^2) * ddW_dA_dT
    "Partial derivative of molar adsorption enthalpy w.r.t. temperature at
    constant pressure";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function is the partial derivative of the function 'h_ads_Dubinin' 
with respect to the temperature at constant pressure. For full details 
of the original function 'h_ads_Dubinin,' check the documentation of the 
function 
<a href=\"Modelica://SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.h_ads_Dubinin\">SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.h_ads_Dubinin</a>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 17, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end dh_ads_dT_Dubinin;
