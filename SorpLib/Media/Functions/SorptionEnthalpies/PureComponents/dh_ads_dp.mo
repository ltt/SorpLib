within SorpLib.Media.Functions.SorptionEnthalpies.PureComponents;
function dh_ads_dp
  "Partial derivative of molar adsorption w.r.t. pressure at constant temperature"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.MolarMass M_adsorptive
    "Molar mass of the adsorptive"
    annotation (Dialog(tab="General", group="Inputs"));
  extends
    SorpLib.Media.Functions.SorptionEnthalpies.BasesClasses.PartialPure_dh_ads_dp;

  input Modelica.Units.SI.SpecificVolume v_adsorptive
    "Specific volume of the adsorptive"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.SpecificVolume v_adsorpt
    "Specific volume of the adsorpt"
    annotation (Dialog(tab="General", group="Inputs"));

  input SorpLib.Units.DerSpecificVolumeByPressure dv_adsorptive_dp
    "Partial derivative of specific volume of the adsorptive w.r.t. pressure
    at constant temperature"
    annotation (Dialog(tab="General", group="Inputs"));
  input SorpLib.Units.DerSpecificVolumeByPressure dv_adsorpt_dp
    "Partial derivative of specific volume of the adsorpt w.r.t. pressure at
    constant temperature"
    annotation (Dialog(tab="General", group="Inputs"));

  input SorpLib.Units.DerUptakeByPressure dx_adsorpt_dp
    "Partial derivative of uptake w.r.t. pressure at constant temperature"
    annotation (Dialog(tab="General", group="Inputs"));
  input SorpLib.Units.DerUptakeByTemperature dx_adsorpt_dT
    "Partial derivative of uptake w.r.t. temperature at constant pressure"
    annotation (Dialog(tab="General", group="Inputs"));

  input SorpLib.Units.DerUptakeByPressurePressure ddx_adsorpt_dp_dp
    "Second-order partial derivative of uptake w.r.t. pressure at constant 
    temperature"
    annotation (Dialog(tab="General", group="Inputs"));
  input SorpLib.Units.DerUptakeByPressureTemperature ddx_adsorpt_dp_dT
    "Second-order partial derivative of uptake w.r.t. pressure and temperature"
    annotation (Dialog(tab="General", group="Inputs"));

algorithm
  dh_ads_dp :=
    (T_adsorpt * M_adsorptive *
      (-dx_adsorpt_dT / dx_adsorpt_dp)) * dv_adsorptive_dp +
    (-T_adsorpt * M_adsorptive *
      (-dx_adsorpt_dT / dx_adsorpt_dp)) * dv_adsorpt_dp +
    (T_adsorpt * M_adsorptive * (v_adsorptive - v_adsorpt) *
      (-1 / dx_adsorpt_dp)) * ddx_adsorpt_dp_dT +
    (T_adsorpt * M_adsorptive * (v_adsorptive - v_adsorpt) *
      (dx_adsorpt_dT / dx_adsorpt_dp^2)) * ddx_adsorpt_dp_dp
    "Partial derivative of molar adsorption enthalpy w.r.t. pressure at constant
    temperature";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function is the partial derivative of the function 'h_ads' with respect to 
the pressure at constant temperature. For full details of the original function 
'h_ads,' check the documentation of the function 
<a href=\"Modelica://SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.h_ads\">SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.h_ads</a>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 17, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end dh_ads_dp;
