within SorpLib.Media.Functions.SorptionEnthalpies.PureComponents;
function dh_ads_dT_clausiusClapeyron
  "Partial derivative of molar adsorption enthalpy according to Clausius Clapeyron w.r.t. temperature at constant pressure"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.Pressure p_adsorpt
    "Pressure"
    annotation (Dialog(tab="General", group="Inputs"));
  extends
    SorpLib.Media.Functions.SorptionEnthalpies.BasesClasses.PartialPure_dh_ads_dT;

  input SorpLib.Units.DerUptakeByPressure dx_adsorpt_dp
    "Partial derivative of uptake w.r.t. pressure at constant temperature"
    annotation (Dialog(tab="General", group="Inputs"));
  input SorpLib.Units.DerUptakeByTemperature dx_adsorpt_dT
    "Partial derivative of uptake w.r.t. temperature at constant pressure"
    annotation (Dialog(tab="General", group="Inputs"));

  input SorpLib.Units.DerUptakeByTemperatureTemperature ddx_adsorpt_dT_dT
    "Second-order partial derivative of uptake w.r.t. temperature at constant
    pressure"
    annotation (Dialog(tab="General", group="Inputs"));
  input SorpLib.Units.DerUptakeByPressureTemperature ddx_adsorpt_dp_dT
    "Second-order partial derivative of uptake w.r.t. pressure and temperature"
    annotation (Dialog(tab="General", group="Inputs"));

algorithm
  dh_ads_dT :=
    (2 * Modelica.Constants.R * T_adsorpt / p_adsorpt *
      (-dx_adsorpt_dT / dx_adsorpt_dp)) +
    (Modelica.Constants.R * T_adsorpt^2 / p_adsorpt * (-1 / dx_adsorpt_dp)) *
      ddx_adsorpt_dT_dT +
    (Modelica.Constants.R * T_adsorpt^2 / p_adsorpt *
      (dx_adsorpt_dT / dx_adsorpt_dp^2)) * ddx_adsorpt_dp_dT
    "Partial derivative of molar adsorption enthalpy w.r.t. temperature at
    constant pressure";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function is the partial derivative of the function 'h_ads_clausiusClapeyron' 
with respect to the temperature at constant pressure. For full details of the 
original function 'h_ads_clausiusClapeyron,' check the documentation of the function 
<a href=\"Modelica://SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.h_ads_clausiusClapeyron\">SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.h_ads_clausiusClapeyron</a>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 17, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end dh_ads_dT_clausiusClapeyron;
