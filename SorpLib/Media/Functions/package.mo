within SorpLib.Media;
package Functions "Functions required to calculate fluid property data"
  extends Modelica.Icons.FunctionsPackage;

  annotation (Documentation(info="<html>
<p>
This package contains functions used to calculate fluid property data. In particular,
these functions are required for calculating data for pure and multi-component
adsorption. Please check the documentation of the individual packages for more 
details on the implemented functions.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end Functions;
