within SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.BasesClasses;
partial function PartialPure_cp
  "Base function for functions calculating the specific heat capacities of adsorpt phases for pure components"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.Temperature T_adsorpt
    "Temperature"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of outputs
  //
  output Modelica.Units.SI.SpecificHeatCapacity cp_adsorpt
    "Specific heat capacitiy of adsorpt"
  annotation (Dialog(tab="General", group="Outputs", enable=false));

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial function is the basic function for functions that calculate the
specific heat capacities of the adsorpt phase for pure components.
<br/><br/>
This partial function defines the temperature <i>T_adsorpt</i> as input and the
specific heat capacity <i>cp_adsorpt</i> as output.
<br/><br/>
Functions that inherit properties from this partial function may have to add
furhter inputs, outputs, and the function algorithm.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 20, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end PartialPure_cp;
