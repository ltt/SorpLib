within SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.BasesClasses;
partial function PartialPureIntegrand
  "Base function for integrand functions for pure components"
  extends Modelica.Math.Nonlinear.Interfaces.partialScalarFunction;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.Temperature T_adsorpt
    "Temperature"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.MolarMass M_adsorptive
    "Molar mass of the adsorptive"
    annotation (Dialog(tab="General", group="Inputs"));

  input Real c[:]
    "Coefficients of the isotherm model"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real dc_dT[:]
    "Partial derivative of coefficients of the isotherm model w.r.t. 
    temperature"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real ddc_dT_dT[:]
    "Second-order partial derivative of coefficients of the isotherm model w.r.t. 
    temperature"
    annotation (Dialog(tab="General", group="Inputs"));

  input SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_p_xT func_p_xT
    "Pressure as function of uptake and temperature"
    annotation (Dialog(tab="General", group="Inputs"));
  input SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_dx_dp func_dx_dp
    "Partial derivative of uptake w.r.t. pressure at constant temperature"
    annotation (Dialog(tab="General", group="Inputs"));
  input SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_dx_dT func_dx_dT
    "Partial derivative of uptake w.r.t. temperature at constant pressure"
    annotation (Dialog(tab="General", group="Inputs"));
  input SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_ddx_dp_dp func_ddx_dp_dp
    "Second-order partial derivative of uptake w.r.t. pressure at constant 
    temperature"
    annotation (Dialog(tab="General", group="Inputs"));
  input SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_ddx_dT_dT func_ddx_dT_dT
    "Second-order partial derivative of uptake w.r.t. temperature at constant 
    pressure"
    annotation (Dialog(tab="General", group="Inputs"));
  input SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_ddx_dp_dT func_ddx_dp_dT
    "Second-order partial derivative of uptake w.r.t. pressure and temperature"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of variables
protected
  Modelica.Units.SI.Pressure p_adsorpt
    "Pressure at x_adsorpt = u and T_adsorpt";

  SorpLib.Units.DerUptakeByPressure dx_adsorpt_dp
    "Partial derivative of uptake w.r.t. pressure at constant temperature";
  SorpLib.Units.DerUptakeByTemperature dx_adsorpt_dT
    "Partial derivative of uptake w.r.t. temperature at constant pressure";

  SorpLib.Units.DerUptakeByPressurePressure ddx_adsorpt_dp_dp
    "Second-order partial derivative of uptake w.r.t. pressure at contant 
    temperature";
  SorpLib.Units.DerUptakeByTemperatureTemperature ddx_adsorpt_dT_dT
    "Second-order partial derivative of uptake w.r.t. temperature at contant 
    pressure";
  SorpLib.Units.DerUptakeByPressureTemperature ddx_adsorpt_dp_dT
    "Second-order partial derivative of uptake w.r.t. pressure and temperature";

  SorpLib.Units.DerMolarEnthalpyByPressure dh_ads_dp_T
    "Partial derivative of molar adsorption enthalpy w.r.t. pressure at constant
    temperature";
  Modelica.Units.SI.MolarHeatCapacity dh_ads_dT_p
    "Partial derivative of molar adsorption enthalpy w.r.t. temperature at constant
    pressure";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial function is the basic function for integrand functions that may be
required to calculate specific heat capacities of the adsorpt phase for pure 
components.
<br/><br/>
Functions that inherit properties from this partial function may have to add
furhter inputs and the function algorithm.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 20, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end PartialPureIntegrand;
