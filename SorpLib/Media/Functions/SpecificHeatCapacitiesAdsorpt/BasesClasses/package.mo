within SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt;
package BasesClasses "Base classes used to build new functions calculating specific heat capacities"
  extends Modelica.Icons.BasesPackage;

  annotation (Documentation(revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>", info="<html>
<p>
This package contains partial basic functions and models. These partial functions
and models contain fundamental definitions of functions calculating specific heat
capacities of adsorpt phases for pure and multi-component adsorption. The content 
of this package is only of interest when adding new functions to the library.
</p>
</html>"));
end BasesClasses;
