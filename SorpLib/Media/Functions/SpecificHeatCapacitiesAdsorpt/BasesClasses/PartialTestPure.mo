within SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.BasesClasses;
partial model PartialTestPure
  "Base model for testers of specific heat capacity models of the adsorpt phase describing pure component adsorption"
  extends Modelica.Icons.Example;

  //
  // Definition of parameters
  //
  replaceable package Medium = Modelica.Media.Water.WaterIF97_ph
    constrainedby Modelica.Media.Interfaces.PartialTwoPhaseMedium
    "Medium (i.e., adsorptive)"
    annotation (Dialog(tab="General",group="Medium"),
                choicesAllMatching = true);

  parameter Integer no_coefficients = 6
    "Number of coefficients of selected isotherm model"
    annotation (Dialog(tab="General", group="Test setup"),
                Evaluate=true,
                HideResult=true);
  parameter Modelica.Units.SI.Pressure p_adsorpt_start = 700
    "Start value of equilibrium pressure"
    annotation (Dialog(tab="General", group="Test setup"),
                Evaluate=false,
                HideResult=false);
  parameter Modelica.Units.SI.Temperature T_adsorpt_start = 278.15
    "Start value of equilibrium temperature"
    annotation (Dialog(tab="General", group="Test setup"),
                Evaluate=false,
                HideResult=false);
  parameter Modelica.Units.SI.MolarMass M_adsorptive = 0.018
    "Molar mass of adsorptive"
    annotation (Dialog(tab="General", group="Test setup"),
                Evaluate=true,
                HideResult=true);

  parameter Modelica.Units.SI.PressureDifference dp = 1e-3
    "Pressure difference used to calculated partial derivatives numerically"
    annotation (Dialog(tab="General",group="Numerics"),
                Evaluate=true,
                HideResult=true);
  parameter Modelica.Units.SI.TemperatureDifference dT = 1e-3
    "Temperature difference used to calculated partial derivatives numerically"
    annotation (Dialog(tab="General",group="Numerics"),
                Evaluate=true,
                HideResult=true);
  parameter SorpLib.Units.Uptake dx = 1e-3
    "Uptake difference used to calculated partial derivatives numerically"
    annotation (Dialog(tab="General",group="Numerics"),
                Evaluate=true,
                HideResult=true);

  //
  // Definition of variables
  //
  Modelica.Units.SI.Pressure p_adsorpt(start=p_adsorpt_start, fixed=true)
    "Pressure";
  Modelica.Units.SI.Temperature T_adsorpt(start=T_adsorpt_start, fixed=true)
    "Temperature";
  SorpLib.Units.Uptake x_adsorpt
    "Uptake";

  Medium.ThermodynamicState state_adsorptive_pT
    "State properties of adsorptive at p_adsorpt and T_adsorpt";
  Medium.ThermodynamicState state_bubble_T
    "State properties of bubble point at T_adsorpt";
  Medium.ThermodynamicState state_dew_T
    "State properties of dew point at T_adsorpt";

  Modelica.Units.SI.SpecificHeatCapacity cp_adsorptive
    "Specific heat capacitiy of adsorptive";
  Modelica.Units.SI.SpecificHeatCapacity cp_sat_bubble
    "Specific heat capacitiy at saturated bubble line at T_adsorpt";
  Modelica.Units.SI.SpecificHeatCapacity cp_sat_dew
    "Specific heat capacitiy at saturated dew line at T_adsorpt";

  Modelica.Units.SI.SpecificHeatCapacity cp_adsorpt
    "Specific heat capacitiy of adsorpt";
  Modelica.Units.SI.SpecificHeatCapacity cp_adsorpt_CC
    "Specific heat capacitiy of adsorpt calculated using molar adsorption enthalpy
    according to Clausius Clapeyron";
  Modelica.Units.SI.SpecificHeatCapacity cp_adsorpt_Dubinin
    "Specific heat capacitiy of adsorptive using molar adsorption enthalpy
    according to Dubinin model";

protected
  Real c[no_coefficients]
    "Coefficients of the isotherm model";
  Real c_pdT[no_coefficients]
    "Coefficients of the isotherm model: T + dT";
  Real c_mdT[no_coefficients]
    "Coefficients of the isotherm model: T - dT";

  Real dc_dT[no_coefficients]
    "Partial derivative of coefficients of the isotherm model w.r.t. temperature";
  Real dc_dT_pdT[no_coefficients]
    "Partial derivative of coefficients of the isotherm model w.r.t. temperature:
    T + dT";
  Real dc_dT_mdT[no_coefficients]
    "Partial derivative of coefficients of the isotherm model w.r.t. temperature:
    T - dT";

  Real ddc_dT_dT[no_coefficients]
    "Second-order partial derivative of coefficients of the isotherm model w.r.t. 
    temperature";

  Medium.SaturationProperties sat_T
    "Saturated state properties at T_adsorpt";
  Medium.SaturationProperties sat_T_pdT
    "Saturated state properties at T_adsorpt: T + dT";
  Medium.SaturationProperties sat_T_mdT
    "Saturated state properties at T_adsorpt: T - dT";

equation
  //
  // Calculation of state properties
  //
  sat_T = Medium.setSat_T(T=T_adsorpt)
    "Saturated state properties at T_adsorpt";
  sat_T_pdT = Medium.setSat_T(T=T_adsorpt+dT)
    "Saturated state properties at T_adsorpt: T + dT";
  sat_T_mdT = Medium.setSat_T(T=T_adsorpt-dT)
    "Saturated state properties at T_adsorpt: T - dT";

  state_adsorptive_pT = Medium.setState_pTX(
    p=p_adsorpt,
    T=T_adsorpt)
    "State properties of adsorptive at p_adsorpt and T_adsorpt";

  state_bubble_T = Medium.setBubbleState(sat=sat_T)
    "State properties of bubble point at T_adsorpt";
  state_dew_T = Medium.setDewState(sat=sat_T)
    "State properties of dew point at T_adsorpt";

  //
  // Calculate properties
  //
  cp_adsorptive = Medium.specificHeatCapacityCp(state=state_adsorptive_pT)
    "Specific heat capacitiy of adsorptive";
  cp_sat_bubble = Medium.specificHeatCapacityCp(state=state_bubble_T)
    "Specific heat capacitiy at saturated bubble line at T_adsorpt";
  cp_sat_dew = Medium.specificHeatCapacityCp(state=state_dew_T)
    "Specific heat capacitiy at saturated dew line at T_adsorpt";

  //
  // Annotations
  //
  annotation (
    Icon(coordinateSystem(preserveAspectRatio=false)),
    Diagram(coordinateSystem(preserveAspectRatio=false)),
    Documentation(info="<html>
<p>
This partial model is the basic model for all testers of specific heat capacities
of the adsorpt phase for pure components. This partial model defines all some 
relevant parameters and variables that are required for most specific heat capacity
models.
<br/><br/>
Models that inherit properties from this partial model have to specify the 
<i>medium</i> and test setup. Besides, the coefficients of the isotherm model 
(i.e., <i>c</i>, <i>c_pdT</i>, and <i>c_mdT</i>) and their partial derivatives 
with respect to temperature (i.e., <i>dc_dT</i>, <i>dc_dT_pdT</i>, <i>dc_dT_mdT</i>, 
and <i>ddc_dT_dT</i>) have to be implemented. Additionally, equations for the
following variables must be implemted: <i>p_adsorpt</i>, <i>T_adsorpt</i>, 
<i>x_adsorpt</i>, <i>cp_adsorpt</i>, <i>cp_adsorpt_CC</i>, and <i>cp_adsorpt_Dubinin</i>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 20, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end PartialTestPure;
