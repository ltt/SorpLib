within SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.BasesClasses;
partial function PartialPureIntegrad_cp
  "Base function for integrand-based functions calculating the specific heat capacities of adsorpt phases for pure components"

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.MolarMass M_adsorptive
    "Molar mass of the adsorptive"
    annotation (Dialog(tab="General", group="Inputs"));
  extends
    SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.BasesClasses.PartialPure_cp;
  input SorpLib.Units.Uptake x_adsorpt
    "Uptake"
    annotation (Dialog(tab="General", group="Inputs"));

  input Real c[:]
    "Coefficients of the isotherm model"
    annotation (Dialog(tab="General", group="Inputs - Functions"));
  input Real dc_dT[:]
    "Partial derivative of coefficients of the isotherm model w.r.t. 
    temperature"
    annotation (Dialog(tab="General", group="Inputs - Functions"));
  input Real ddc_dT_dT[:]
    "Second-order partial derivative of coefficients of the isotherm model w.r.t. 
    temperature"
    annotation (Dialog(tab="General", group="Inputs - Functions"));

  input SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_p_xT func_p_xT
    "Pressure as function of uptake and temperature"
    annotation (Dialog(tab="General", group="Inputs - Functions"),
                choicesAllMatching=true);
  input SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_dx_dp func_dx_dp
    "Partial derivative of uptake w.r.t. pressure at constant temperature"
    annotation (Dialog(tab="General", group="Inputs - Functions"),
                choicesAllMatching=true);
  input SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_dx_dT func_dx_dT
    "Partial derivative of uptake w.r.t. temperature at constant pressure"
    annotation (Dialog(tab="General", group="Inputs - Functions"),
                choicesAllMatching=true);
  input SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_ddx_dp_dp func_ddx_dp_dp
    "Second-order partial derivative of uptake w.r.t. pressure at constant 
    temperature"
    annotation (Dialog(tab="General", group="Inputs - Functions"),
                choicesAllMatching=true);
  input SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_ddx_dT_dT func_ddx_dT_dT
    "Second-order partial derivative of uptake w.r.t. temperature at constant 
    pressure"
    annotation (Dialog(tab="General", group="Inputs - Functions"),
                choicesAllMatching=true);
  input SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_ddx_dp_dT func_ddx_dp_dT
    "Second-order partial derivative of uptake w.r.t. pressure and temperature"
    annotation (Dialog(tab="General", group="Inputs - Functions"),
                choicesAllMatching=true);

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial function is the basic function for functions that calculate the
specific heat capacities of the adsorpt phase for pure components. These function
use integrals and, thus, integrad functions to calculate the specific heat 
capacity.
<br/><br/>
Functions that inherit properties from this partial function may have to add
furhter inputs, outputs, and the function algorithm.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 20, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end PartialPureIntegrad_cp;
