within SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.Integrands;
function dh_ads_dT_x_CC
  "Partial derivative of the molar adsorption enthalpy w.r.t. temperature at constant uptake according to Clausius Clapeyron"
  extends
    SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.BasesClasses.PartialPureIntegrand;

algorithm
  //
  // Calculation of sorption equilibrium
  //
  p_adsorpt := func_p_xT(
      x_adsorpt=u,
      T_adsorpt=T_adsorpt,
      c=c,
      p_adsorpt_lb_start=1,
      p_adsorpt_ub_start=10,
      tolerance=100*Modelica.Constants.eps)
    "Pressure at x_adsorpt = u and T_adsorpt";

  dx_adsorpt_dp := func_dx_dp(
      p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt,
      c=c)
    "Partial derivative of uptake w.r.t. pressure at constant temperature";
  dx_adsorpt_dT := func_dx_dT(
      p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt,
      c=c,
      dc_dT_adsorpt=dc_dT)
    "Partial derivative of uptake w.r.t. temperature at constant pressure";

  ddx_adsorpt_dp_dp := func_ddx_dp_dp(
      p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt,
      c=c)
    "Second-order partial derivative of uptake w.r.t. pressure at constant temperature";
  ddx_adsorpt_dT_dT := func_ddx_dT_dT(
      p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt,
      c=c,
      dc_dT_adsorpt=dc_dT,
      ddc_dT_adsorpt_dT_adsorpt=ddc_dT_dT)
    "Second-order partial derivative of uptake w.r.t. temperature at constant pressure";
  ddx_adsorpt_dp_dT := func_ddx_dp_dT(
      p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt,
      c=c,
      dc_dT_adsorpt=dc_dT)
    "Second-order partial derivative of uptake w.r.t. pressure and temperature";

  //
  // Calculation of partial derivatives of molar sorption enthalpy
  //
  dh_ads_dp_T :=
    SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.dh_ads_dp_clausiusClapeyron(
      p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt,
      dx_adsorpt_dp=dx_adsorpt_dp,
      dx_adsorpt_dT=dx_adsorpt_dT,
      ddx_adsorpt_dp_dp=ddx_adsorpt_dp_dp,
      ddx_adsorpt_dp_dT=ddx_adsorpt_dp_dT)
  "Partial derivative of molar adsorption enthalpy w.r.t. pressure at constant
  temperature";
  dh_ads_dT_p :=
    SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.dh_ads_dT_clausiusClapeyron(
      p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt,
      dx_adsorpt_dp=dx_adsorpt_dp,
      dx_adsorpt_dT=dx_adsorpt_dT,
      ddx_adsorpt_dp_dT=ddx_adsorpt_dp_dT,
      ddx_adsorpt_dT_dT=ddx_adsorpt_dT_dT)
    "Partial derivative of molar adsorption enthalpy w.r.t. temperature at constant
    pressure";

  //
  // Calculation of the integrand
  //
  y := dh_ads_dT_p - dh_ads_dp_T * dx_adsorpt_dT / dx_adsorpt_dp
    "Integrand: Partial derivative of molar sorption enthalpy w.r.t. temperature
    at constant uptake";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function calculates the partial derivative of the molar adsorption ethalpy
with respect to temperature at constant uptake. The molar adsorption enthalpy is
calculated using Clausius Clapeyron assumptions:
</p>
<pre>
    (&part;&Delta;h<sub>ads</sub>/&part;T)<sub>x</sub> = (&part;&Delta;h<sub>ads</sub>/&part;T)<sub>p</sub> - (&part;&Delta;h<sub>ads</sub>/&part;p)<sub>T</sub> * (&part;x/&part;T)<sub>p</sub> / (&part;x/&part;p)<sub>T</sub>;
</pre>

<h4>Main equations</h4>
<p>
The molar adsorption enthalpy <i>&Delta;h<sub>ads</sub></i> is defined as difference
of the specific enthalpy of the gas/vapor phase <i>h<sub>adsorptive</sub></i> and the
adsorpt phase <i>h<sub>adsorpt</sub></i>:
</p>
<pre>
    &Delta;h<sub>ads</sub> = h<sub>adsorptive</sub> - h<sub>adsorpt</sub> = R * T<sup>2</sup> / p * (&part;p/&part;T)<sub>x</sub> = R * T<sup>2</sup> / p * (-(&part;x/&part;T)<sub>p</sub> / (&part;x/&part;p)<sub>T</sub>);
</pre>
<p>
Herein, <i>R</i> is the ideal gas constant, <i>p</i> is the pressure, <i>T</i> is
the temperature, <i>(&part;x/&part;p)<sub>T</sub></i> is the partial derivative of 
the uptake with respect to the pressure at constant temperature, and 
<i>(&part;x/&part;T)<sub>p</sub></i> is the partial derivative  of the uptake with 
respect to the temperature at constant pressure.
</p>

<h4>Assumptions and limitations</h4>
<ul>
  <li>
  The specific volume of the adsorpt phase can be neglected compared to the specific
  volume of the adsorptive phase.
  </li>
  <li>
  The adsorptive can be treated as an ideal gas (i.e., p * V = n * R * T).
  </li>
  <li>
  Inert sorbent.
  </li>
</ul>

<h4>References</h4>
<ul>
  <li>
  Bathen, D. and Breitbach, M. (2001). Adsorptionstechnik (in German), 1st Edition, ISBN 3-540-41908-X, Springer-Verlag Berlin Heidelberg New York.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  November 20, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end dh_ads_dT_x_CC;
