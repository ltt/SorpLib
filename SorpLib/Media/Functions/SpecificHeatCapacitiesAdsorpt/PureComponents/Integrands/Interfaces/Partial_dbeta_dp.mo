within SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.Integrands.Interfaces;
partial function Partial_dbeta_dp
  "Base function for functions calculating the partial derivative of isobaric expansion coefficients w.r.t. pressure at constant temperature"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.Pressure p
    "Pressure"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.Temperature T
    "Temperature"
    annotation (Dialog(tab="General", group="Inputs"));

  input Modelica.Units.SI.PressureDifference dp
    "Pressure difference used to calculate partial derivatives w.r.t. pressure
    numerially (just used if analytical solution does not exist)"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of outputs
  //
  output SorpLib.Units.DerIsobaricExpansionCoefficientByPressure dbeta_dp
    "Partial derivative of isobaric expansion coefficients w.r.t. pressure at 
    constant temperature"
    annotation (Dialog(tab="General", group="Outputs", enable=false));

  //
  // Annotation
  //
  annotation (Documentation(info="<html>
<p>
This partial function is the basic function for functions that calculate partial
derivatives of isobaric expansion coefficients with respect to pressure at constant 
temperature <i>dbeta_dp</i> as function of pressure <i>p</i> and temperature <i>T</i>.
<br/><br/>
Specific functions derived from this partial function can be used as functional
arguments in the integrad functions.
<br/><br/>
Functions that inherit properties from this partial function may have to add
furhter inputs, outputs, and the function algorithm.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 20, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Partial_dbeta_dp;
