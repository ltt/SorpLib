within SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.Integrands;
package Interfaces "Package containing interfaces used as function inputs"
extends Modelica.Icons.InterfacesPackage;

annotation (Documentation(revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>", info="<html>
<p>
This package contains partial functions used as functional arguments for integrand
functions.
</p>
</html>"));
end Interfaces;
