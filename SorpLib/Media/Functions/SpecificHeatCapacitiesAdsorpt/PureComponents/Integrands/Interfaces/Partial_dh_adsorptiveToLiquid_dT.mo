within SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.Integrands.Interfaces;
partial function Partial_dh_adsorptiveToLiquid_dT
  "Base function for functions calculating the partial derivative of specific enthalpy differences between adsorptive state and saturated liquid state (i.e., bubble point) w.r.t. temperature at constant pressure"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.Pressure p
    "Pressure"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.Temperature T
    "Temperature"
    annotation (Dialog(tab="General", group="Inputs"));

  input Modelica.Units.SI.TemperatureDifference dT
    "Temperature difference used to calculate partial derivatives w.r.t. temperature
    numerially (just used if analytical solution does not exist)"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of outputs
  //
  output Modelica.Units.SI.SpecificHeatCapacity dh_adsorptiveToLiquid_dT
    "Partial derivative of specific enthalpy difference between adsorptive state 
    and saturated liquid state (i.e., bubble point) w.r.t. temperature at constant
    pressure"
    annotation (Dialog(tab="General", group="Outputs", enable=false));

  //
  // Annotation
  //
  annotation (Documentation(info="<html>
<p>
This partial function is the basic function for functions that calculate the partial
derivative of specific enthalpies of vaprozation with respect to temperature at constant
pressure <i>dh_vap_dp</i> as function of temperature <i>T</i>.
<br/><br/>
Specific functions derived from this partial function can be used as functional
arguments in the integrad functions.
<br/><br/>
Functions that inherit properties from this partial function may have to add
furhter inputs, outputs, and the function algorithm.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 20, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Partial_dh_adsorptiveToLiquid_dT;
