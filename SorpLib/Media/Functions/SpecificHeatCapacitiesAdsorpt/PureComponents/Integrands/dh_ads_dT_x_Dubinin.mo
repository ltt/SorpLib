within SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.Integrands;
function dh_ads_dT_x_Dubinin
  "Partial derivative of the molar adsorption enthalpy w.r.t. temperature at constant uptake according to the model of Dubinin"
  extends
    SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.BasesClasses.PartialPureIntegrand;

  //
  // Definition of inputs
  //
  input
    SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.Integrands.Interfaces.Partial_h_adsorptiveToLiquid_pT
    func_h_adsorptiveToLiquid_pT "Specific enthalpy difference between adsorptive state and saturated liquid
    state (i.e., bubble point)"
    annotation (Dialog(tab="General", group="Inputs"));
  input
    SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.Integrands.Interfaces.Partial_dh_adsorptiveToLiquid_dp
    func_dh_adsorptiveToLiquid_dp
    "Partial derivative of specific enthalpy difference between adsorptive state 
    and saturated liquid state (i.e., bubble point) w.r.t. pressure at constant 
    temperature"
    annotation (Dialog(tab="General", group="Inputs"));
  input
    SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.Integrands.Interfaces.Partial_dh_adsorptiveToLiquid_dT
    func_dh_adsorptiveToLiquid_dT
    "Partial derivative of specific enthalpy difference between adsorptive state 
    and saturated liquid state (i.e., bubble point) w.r.t. temperature at constant
    pressure"
    annotation (Dialog(tab="General", group="Inputs"));

  input
    SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.Integrands.Interfaces.Partial_v_pT
    func_v_adsorpt_pT "Specific volume of the adsorpt"
    annotation (Dialog(tab="General", group="Inputs"));
  input
    SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.Integrands.Interfaces.Partial_dv_dp
    func_dv_adsorpt_dp "Partial derivative of the specific volume of the adsorpt 
    w.r.t. pressure
    at constant temperature" annotation (Dialog(tab="General", group="Inputs"));
  input
    SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.Integrands.Interfaces.Partial_dv_dT
    func_dv_adsorpt_dT
    "Partial derivative of the specific volume of the adsorpt w.r.t. temperature
    at constant pressure" annotation (Dialog(tab="General", group="Inputs"));

  input
    SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.Integrands.Interfaces.Partial_beta_pT
    func_beta_adsorpt_pT
    "Isobaric expansion coefficient of the adsorpt"
    annotation (Dialog(tab="General", group="Inputs"));
  input
    SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.Integrands.Interfaces.Partial_dbeta_dp
    func_dbeta_adsorpt_dp
    "Partial derivative of the isobaric expansion coefficient of the adsorpt w.r.t. 
    pressure at constant temperature"
                                     annotation (Dialog(tab="General", group="Inputs"));
  input
    SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.Integrands.Interfaces.Partial_dbeta_dT
    func_dbeta_adsorpt_dT
    "Partial derivative of the isobaric expansion coefficient of the adsorpt w.r.t. 
    temperature at constant pressure"
     annotation (Dialog(tab="General", group="Inputs"));

  input SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_dW_dA func_dW_dA
    "Partial derivative of the characteristic curve w.r.t. molar adsorption potential
    at constant pressure and temperature"
    annotation (Dialog(tab="General", group="Inputs"));
  input SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_ddW_dA_dA func_ddW_dA_dA
    "Second-order partial derivative of the characteristic curve w.r.t. molar 
    adsorption potential at constant pressure and temperature"
    annotation (Dialog(tab="General", group="Inputs"));
  input SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_ddW_dA_dT func_ddW_dA_dT
    "Second-order partial derivative of the characteristic curve w.r.t. molar 
    adsorption potential and temperature at constant pressure"
    annotation (Dialog(tab="General", group="Inputs"));

  input Modelica.Units.SI.Pressure p_clausiusClyperon
    "Maximum pressure up to which the molar adsorption enthalpy is calculated
    according to the Clausius Clyperon assumptions"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.PressureDifference dp
    "Pressure difference used to calculate partial derivatives w.r.t. pressure
    numerially (just used if analytical solution does not exist)"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.TemperatureDifference dT
    "Temperature difference used to calculate partial derivatives w.r.t. pressure
    numerially (just used if analytical solution does not exist)"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of variables
protected
  Modelica.Units.SI.SpecificEnthalpy h_adsorptiveToLiquid
    "Specific enthalpy difference between adsorptive state and saturated liquid
    state (i.e., bubble point)";
  SorpLib.Units.DerSpecificEnthalpyByPressure dh_adsorptiveToLiquid_dp
    "Partial derivative of specific enthalpy difference between adsorptive state 
    and saturated liquid state (i.e., bubble point) w.r.t. pressure at constant 
    temperature";
  Modelica.Units.SI.SpecificHeatCapacity dh_adsorptiveToLiquid_dT
    "Partial derivative of specific enthalpy difference between adsorptive state 
    and saturated liquid state (i.e., bubble point) w.r.t. temperature at constant
    pressure";

  Modelica.Units.SI.SpecificVolume v_adsorpt
    "Specific volume of the adsorpt";
  SorpLib.Units.DerSpecificVolumeByPressure dv_adsorpt_dp
    "Partial derivative of specific volume of the adsorpt w.r.t. pressure at
    constant temperature";
  SorpLib.Units.DerSpecificVolumeByTemperature dv_adsorpt_dT
    "Partial derivative of specific volume of the adsorpt w.r.t. temperature at
    constant pressure";

  Modelica.Units.SI.RelativePressureCoefficient beta_adsorpt
    "Isobaric expansion coefficient of the adsorpt";
  SorpLib.Units.DerIsobaricExpansionCoefficientByPressure dbeta_adsorpt_dp
    "Partial derivative of isobaric expansion coefficient of the adsorpt w.r.t. 
    pressure at constant temperature";
  SorpLib.Units.DerIsobaricExpansionCoefficientByTemperature dbeta_adsorpt_dT
    "Partial derivative of isobaric expansion coefficient of the adsorpt w.r.t. 
    temperature at constant pressure";

  SorpLib.Units.MolarAdsorptionPotential A
    "Molar adsorption potential";
  SorpLib.Units.DerMolarAdsorptionPotentialByPressure dA_dp
    "Partial derivative of molar adsorption potential w.r.t. pressure at
    constant temperature";
  SorpLib.Units.DerMolarAdsorptionPotentialByTemperature dA_dT
    "Partial derivative of molar adsorption potential w.r.t. temperature at
    constant pressure";

  SorpLib.Units.DerFilledPoreVolumeByAdsorptionPotential dW_dA
    "Partial derivative of filled pore volume w.r.t. molar adsorption potential
    at constant pressure and temperature";
  SorpLib.Units.DerFilledPoreVolumeByAdsorptionPotentialAdsorptionPotential ddW_dA_dA
    "Partial derivative of filled pore volume w.r.t. molar adsorption potential
    at constant pressure and temperature";
  SorpLib.Units.DerFilledPoreVolumeByAdsorptionPotentialTemperature ddW_dA_dT
    "Second-order partial derivative of filled pore volume w.r.t. molar adsorption 
    potential and temperature at constant pressure";

algorithm
  //
  // Calculation of sorption equilibrium
  //
  p_adsorpt := func_p_xT(
      x_adsorpt=u,
      T_adsorpt=T_adsorpt,
      c=c,
      p_adsorpt_lb_start=1,
      p_adsorpt_ub_start=10,
      tolerance=100*Modelica.Constants.eps)
    "Pressure at x_adsorpt = u and T_adsorpt";

  dx_adsorpt_dp := func_dx_dp(
      p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt,
      c=c)
    "Partial derivative of uptake w.r.t. pressure at constant temperature";
  dx_adsorpt_dT := func_dx_dT(
      p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt,
      c=c,
      dc_dT_adsorpt=dc_dT)
    "Partial derivative of uptake w.r.t. temperature at constant pressure";

  ddx_adsorpt_dp_dp := func_ddx_dp_dp(
      p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt,
      c=c)
    "Second-order partial derivative of uptake w.r.t. pressure at constant temperature";
  ddx_adsorpt_dT_dT := func_ddx_dT_dT(
      p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt,
      c=c,
      dc_dT_adsorpt=dc_dT,
      ddc_dT_adsorpt_dT_adsorpt=ddc_dT_dT)
    "Second-order partial derivative of uptake w.r.t. temperature at constant pressure";
  ddx_adsorpt_dp_dT := func_ddx_dp_dT(
      p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt,
      c=c,
      dc_dT_adsorpt=dc_dT)
    "Second-order partial derivative of uptake w.r.t. pressure and temperature";

  //
  // Calculation of further properties
  //
  if p_adsorpt > p_clausiusClyperon then
    //
    // Do not use Clausius Clyperon assumptions
    //
    h_adsorptiveToLiquid := func_h_adsorptiveToLiquid_pT(
      p=p_adsorpt,
      T=T_adsorpt)
      "Specific enthalpy difference between adsorptive state and saturated liquid
      state (i.e., bubble point)";
    dh_adsorptiveToLiquid_dp:=func_dh_adsorptiveToLiquid_dp(
      p=p_adsorpt,
      T=T_adsorpt,
      dp=dp)
      "Partial derivative of specific enthalpy difference between adsorptive state 
      and saturated liquid state (i.e., bubble point) w.r.t. pressure at constant 
      temperature";
    dh_adsorptiveToLiquid_dT:=func_dh_adsorptiveToLiquid_dT(
      p=p_adsorpt,
      T=T_adsorpt,
      dT=dT)
      "Partial derivative of specific enthalpy difference between adsorptive state 
      and saturated liquid state (i.e., bubble point) w.r.t. temperature at constant
      pressure";

    v_adsorpt :=func_v_adsorpt_pT(
      p=p_adsorpt,
      T=T_adsorpt)
      "Specific volume of the adsorpt";
    dv_adsorpt_dp :=func_dv_adsorpt_dp(
      p=p_adsorpt,
      T=T_adsorpt,
      dp=dp)
      "Partial derivative of specific volume of the adsorpt w.r.t. pressure at
      constant temperature";
    dv_adsorpt_dT :=func_dv_adsorpt_dT(
      p=p_adsorpt,
      T=T_adsorpt,
      dT=dT)
      "Partial derivative of specific volume of the adsorpt w.r.t. temperature at
      constant pressure";

    beta_adsorpt:=func_beta_adsorpt_pT(
      p=p_adsorpt,
      T=T_adsorpt)
      "Isobaric expansion coefficient of the adsorpt";
    dbeta_adsorpt_dp:=func_dbeta_adsorpt_dp(
      p=p_adsorpt,
      T=T_adsorpt,
      dp=dp)
      "Partial derivative of isobaric expansion coefficient of the adsorpt w.r.t. 
      pressure at constant temperature";
    dbeta_adsorpt_dT:=func_dbeta_adsorpt_dT(
      p=p_adsorpt,
      T=T_adsorpt,
      dT=dT)
      "Partial derivative of isobaric expansion coefficient of the adsorpt w.r.t. 
      temperature at constant pressure";

    A := SorpLib.Media.Functions.SorptionEquilibria.Utilities.A_ppsT(
      p_adsorpt=p_adsorpt,
      p_sat=c[1],
      T_adsorpt=T_adsorpt)
      "Molar adsorption potential";
    dA_dp:=SorpLib.Media.Functions.SorptionEquilibria.Utilities.dA_dp(
      p_adsorpt=p_adsorpt,
      p_sat=c[1],
      T_adsorpt=T_adsorpt)
      "Partial derivative of molar adsorption potential w.r.t. pressure at
      constant temperature";
    dA_dT:=SorpLib.Media.Functions.SorptionEquilibria.Utilities.dA_dT(
      p_adsorpt=p_adsorpt,
      p_sat=c[1],
      T_adsorpt=T_adsorpt,
      dp_sat_dT_adsorpt=dc_dT[1])
      "Partial derivative of molar adsorption potential w.r.t. temperature at
      constant pressure";

    dW_dA:=func_dW_dA(
      A=A,
      c=c)
      "Partial derivative of filled pore volume w.r.t. molar adsorption potential
      at constant pressure and temperature";
    ddW_dA_dA:=func_ddW_dA_dA(
      A=A,
      c=c)
      "Partial derivative of filled pore volume w.r.t. molar adsorption potential
      at constant pressure and temperature";
    ddW_dA_dT:=func_ddW_dA_dT(
      p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt,
      A=A,
      c=c,
      dc_dT_adsorpt=dc_dT)
      "Second-order partial derivative of filled pore volume w.r.t. molar adsorption 
      potential and temperature at constant pressure";

    //
    // Calculation of partial derivatives of molar sorption enthalpy
    //
    dh_ads_dp_T :=
      SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.dh_ads_dp_Dubinin(
        M_adsorptive=M_adsorptive,
        T_adsorpt=T_adsorpt,
        x_adsorpt=u,
        dx_adsorpt_dp=dx_adsorpt_dp,
        h_adsorptiveToLiquid=h_adsorptiveToLiquid,
        dh_adsorptiveToLiquid_dp=dh_adsorptiveToLiquid_dp,
        v_adsorpt=v_adsorpt,
        dv_adsorpt_dp=dv_adsorpt_dp,
        beta_adsorpt=beta_adsorpt,
        dbeta_adsorpt_dp=dbeta_adsorpt_dp,
        dA_dp=dA_dp,
        dW_dA=dW_dA,
        ddW_dA_dA=ddW_dA_dA)
    "Partial derivative of molar adsorption enthalpy w.r.t. pressure at constant
    temperature";
    dh_ads_dT_p :=
      SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.dh_ads_dT_Dubinin(
        M_adsorptive=M_adsorptive,
        T_adsorpt=T_adsorpt,
        x_adsorpt=u,
        dx_adsorpt_dT=dx_adsorpt_dT,
        h_adsorptiveToLiquid=h_adsorptiveToLiquid,
        dh_adsorptiveToLiquid_dT=dh_adsorptiveToLiquid_dT,
        v_adsorpt=v_adsorpt,
        dv_adsorpt_dT=dv_adsorpt_dT,
        beta_adsorpt=beta_adsorpt,
        dbeta_adsorpt_dT=dbeta_adsorpt_dT,
        dA_dT=dA_dT,
        dW_dA=dW_dA,
        ddW_dA_dT=ddW_dA_dT)
      "Partial derivative of molar adsorption enthalpy w.r.t. temperature at constant
      pressure";

  else
    //
    // Use Clausius Clyperon assumptions: No further propiertes are required
    //
    dh_ads_dp_T :=
      SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.dh_ads_dp_clausiusClapeyron(
        p_adsorpt=p_adsorpt,
        T_adsorpt=T_adsorpt,
        dx_adsorpt_dp=dx_adsorpt_dp,
        dx_adsorpt_dT=dx_adsorpt_dT,
        ddx_adsorpt_dp_dp=ddx_adsorpt_dp_dp,
        ddx_adsorpt_dp_dT=ddx_adsorpt_dp_dT)
    "Partial derivative of molar adsorption enthalpy w.r.t. pressure at constant
    temperature";
    dh_ads_dT_p :=
      SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.dh_ads_dT_clausiusClapeyron(
        p_adsorpt=p_adsorpt,
        T_adsorpt=T_adsorpt,
        dx_adsorpt_dp=dx_adsorpt_dp,
        dx_adsorpt_dT=dx_adsorpt_dT,
        ddx_adsorpt_dp_dT=ddx_adsorpt_dp_dT,
        ddx_adsorpt_dT_dT=ddx_adsorpt_dT_dT)
      "Partial derivative of molar adsorption enthalpy w.r.t. temperature at constant
      pressure";

  end if;

    //
    // Calculation of the integrand
    //
    y := dh_ads_dT_p - dh_ads_dp_T * dx_adsorpt_dT / dx_adsorpt_dp
      "Integrand: Partial derivative of molar sorption enthalpy w.r.t. temperature
      at constant uptake";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function calculates the partial derivative of the molar adsorption ethalpy
with respect to temperature at constant uptake. The molar adsorption enthalpy is
calculated according to the model of Dubinin:
</p>
<pre>
    (&part;&Delta;h<sub>ads</sub>/&part;T)<sub>x</sub> = (&part;&Delta;h<sub>ads</sub>/&part;T)<sub>p</sub> - (&part;&Delta;h<sub>ads</sub>/&part;p)<sub>T</sub> * (&part;x/&part;T)<sub>p</sub> / (&part;x/&part;p)<sub>T</sub>;
</pre>
</p>

<h4>Main equations</h4>
<p>
The molar adsorption enthalpy <i>&Delta;h<sub>ads</sub></i> is defined as difference
of the specific enthalpy of the gas/vapor phase <i>h<sub>adsorptive</sub></i> and the
adsorpt phase <i>h<sub>adsorpt</sub></i>:
</p>
<pre>
    &Delta;h<sub>ads</sub> = h<sub>adsorptive</sub> - h<sub>adsorpt</sub> = M<sub>adsorptive</sub> * &Delta;h<sub>adsorptiveToLiquid</sub> + A - &beta; * T * v<sub>adsorpt</sub> * x * 1 / (&part;W/&part;A)<sub>p,T</sub>;
</pre>
<p>
Herein, <i>M<sub>adsorptive</sub></i> is the molar mass of the adsorptive, <i>T</i> 
is the temperature, <i>x</i> is the uptake, <i>v<sub>adsorpt</sub></i> is the specific 
volume of the adsorpt, <i>&beta;</i> is the isobaric expansion coefficient of the 
adsorpt, <i>&Delta;h<sub>adsorptiveToLiquid</sub></i> is the specific enthalpy difference
between adsorptive phase and saturated liquid phase (i.e., bubble point), <i>A</i> is 
the molar adsorption potential, and <i>(&part;W/&part;A)<sub>p,T</sub></i> is the partial 
derivative of the filled pore volume with respect to the molar adsorption potential at 
constant pressure and temperature.
</p>

<h4>Assumptions and limitations</h4>
<ul>
  <li>
  Inert sorbent.
  </li>
</ul>

<h4>References</h4>
<ul>
  <li>
  Schwamberger, V. (2016). Thermodynamic and numerical investigation of a novel sorption cycle for application in adsorption heat pumps and chillers (in German), PhD thesis, Karlsruhe.
</ul>
</html>", revisions="<html>
<ul>
  <li>
  November 20, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end dh_ads_dT_x_Dubinin;
