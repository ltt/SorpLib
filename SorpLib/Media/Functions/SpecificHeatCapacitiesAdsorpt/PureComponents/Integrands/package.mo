within SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents;
package Integrands "Package containing functions defining integrands for numerical integration"
extends Modelica.Icons.InternalPackage;

annotation (Documentation(info="<html>
<p>
This package contains functions defining integrands. These integrands are used
while numerical integration, which is sometimes requied depending on the calculation
approach of the specific heat capacity of the adsorpt phase. Numerical integration is 
performed using the very efficient function
<a href=\"Modelica://Modelica.Math.Nonlinear.quadratureLobatto\">Modelica.Math.Nonlinear.quadratureLobatto</a>.
<br><br>
The content of this package is only of interest when adding new functions to the 
library or when adding new approach for calculating the molar adsorption enthalpy.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Integrands;
