within SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.Integrands;
function dh_adsorpt_dT_x
  "Partial derivative of the adsorpt w.r.t. temperature at constant uptake"
  extends
    SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.BasesClasses.PartialPureIntegrand;

  //
  // Definition of inputs
  //
  input
    SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.Integrands.Interfaces.Partial_v_pT
    func_v_adsorptive_pT "Specific volume of the adsorptive"
    annotation (Dialog(tab="General", group="Inputs"));
  input
    SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.Integrands.Interfaces.Partial_dv_dp
    func_dv_adsorptive_dp "Partial derivative of the specific volume of the adsorptive w.r.t. pressure
    at constant temperature" annotation (Dialog(tab="General", group="Inputs"));
  input
    SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.Integrands.Interfaces.Partial_dv_dT
    func_dv_adsorptive_dT "Partial derivative of the specific volume of the adsorptive w.r.t. temperature
    at constant pressure" annotation (Dialog(tab="General", group="Inputs"));

  input
    SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.Integrands.Interfaces.Partial_dh_dT
    func_dh_adsorptive_dT
    "Partial deriivative of the specific enthalpy of the adsorptive w.r.t. temperature
    at constant pressure"
    annotation (Dialog(tab="General", group="Inputs - Functions"),
                choicesAllMatching=true);
  input
    SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.Integrands.Interfaces.Partial_dh_dT
    func_dh_adsorptive_ig_dT
    "Partial deriivative of the specific enthalpy of the adsorptive (ideal gas) 
    w.r.t. temperature at constant pressure"
    annotation (Dialog(tab="General", group="Inputs - Functions"),
                choicesAllMatching=true);

  input
    SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.Integrands.Interfaces.Partial_v_pT
    func_v_adsorpt_pT "Specific volume of the adsorpt"
    annotation (Dialog(tab="General", group="Inputs"));
  input
    SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.Integrands.Interfaces.Partial_dv_dp
    func_dv_adsorpt_dp "Partial derivative of the specific volume of the adsorpt w.r.t. pressure
    at constant temperature" annotation (Dialog(tab="General", group="Inputs"));
  input
    SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.Integrands.Interfaces.Partial_dv_dT
    func_dv_adsorpt_dT "Partial derivative of the specific volume of the adsorpt w.r.t. temperature
    at constant pressure" annotation (Dialog(tab="General", group="Inputs"));

  input Modelica.Units.SI.Pressure p_clausiusClyperon
    "Maximum pressure up to which the molar adsorption enthalpy is calculated
    according to the Clausius Clyperon assumptions"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.PressureDifference dp
    "Pressure difference used to calculate partial derivatives w.r.t. pressure
    numerially (just used if analytical solution does not exist)"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.TemperatureDifference dT
    "Temperature difference used to calculate partial derivatives w.r.t. pressure
    numerially (just used if analytical solution does not exist)"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of variables
protected
  Modelica.Units.SI.SpecificVolume v_adsorptive
    "Specific volume of the adsorptive";
  SorpLib.Units.DerSpecificVolumeByPressure dv_adsorptive_dp
    "Partial derivative of specific volume of the adsorptive w.r.t. pressure at
    constant temperature";
  SorpLib.Units.DerSpecificVolumeByTemperature dv_adsorptive_dT
    "Partial derivative of specific volume of the adsorptive w.r.t. temperature at
    constant pressure";

  Modelica.Units.SI.SpecificHeatCapacity dh_adsorptive_dT
    "Partial derivative of the specific enthalpy of the adsorptive w.r.t. temperature
    at constant pressure";

  Modelica.Units.SI.SpecificVolume v_adsorpt
    "Specific volume of the adsorpt";
  SorpLib.Units.DerSpecificVolumeByPressure dv_adsorpt_dp
    "Partial derivative of specific volume of the adsorpt w.r.t. pressure at
    constant temperature";
  SorpLib.Units.DerSpecificVolumeByTemperature dv_adsorpt_dT
    "Partial derivative of specific volume of the adsorpt w.r.t. temperature at
    constant pressure";

algorithm
  //
  // Calculation of sorption equilibrium
  //
  p_adsorpt := func_p_xT(
      x_adsorpt=u,
      T_adsorpt=T_adsorpt,
      c=c,
      p_adsorpt_lb_start=1,
      p_adsorpt_ub_start=10,
      tolerance=100*Modelica.Constants.eps)
    "Pressure at x_adsorpt = u and T_adsorpt";

  dx_adsorpt_dp := func_dx_dp(
      p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt,
      c=c)
    "Partial derivative of uptake w.r.t. pressure at constant temperature";
  dx_adsorpt_dT := func_dx_dT(
      p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt,
      c=c,
      dc_dT_adsorpt=dc_dT)
    "Partial derivative of uptake w.r.t. temperature at constant pressure";

  ddx_adsorpt_dp_dp := func_ddx_dp_dp(
      p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt,
      c=c)
    "Second-order partial derivative of uptake w.r.t. pressure at constant temperature";
  ddx_adsorpt_dT_dT := func_ddx_dT_dT(
      p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt,
      c=c,
      dc_dT_adsorpt=dc_dT,
      ddc_dT_adsorpt_dT_adsorpt=ddc_dT_dT)
    "Second-order partial derivative of uptake w.r.t. temperature at constant pressure";
  ddx_adsorpt_dp_dT := func_ddx_dp_dT(
      p_adsorpt=p_adsorpt,
      T_adsorpt=T_adsorpt,
      c=c,
      dc_dT_adsorpt=dc_dT)
    "Second-order partial derivative of uptake w.r.t. pressure and temperature";

  //
  // Calculation of further properties
  //
  if p_adsorpt > p_clausiusClyperon then
    //
    // Do not use Clausius Clyperon assumptions
    //
    v_adsorptive := func_v_adsorptive_pT(
      p=p_adsorpt,
      T=T_adsorpt)
      "Specific volume of the adsorptive";
    dv_adsorptive_dp :=func_dv_adsorptive_dp(
      p=p_adsorpt,
      T=T_adsorpt,
      dp=dp)
      "Partial derivative of specific volume of the adsorptive w.r.t. pressure at
      constant temperature";
    dv_adsorptive_dT :=func_dv_adsorptive_dT(
      p=p_adsorpt,
      T=T_adsorpt,
      dT=dT)
      "Partial derivative of specific volume of the adsorptive w.r.t. temperature at
      constant pressure";

    dh_adsorptive_dT := func_dh_adsorptive_dT(
      p=p_adsorpt,
      T=T_adsorpt,
      dT=dT)
      "Partial derivative of the specific enthalpy of the adsorptive w.r.t. temperature
      at constant pressure";

    v_adsorpt :=func_v_adsorpt_pT(
      p=p_adsorpt,
      T=T_adsorpt)
      "Specific volume of the adsorpt";
    dv_adsorpt_dp :=func_dv_adsorpt_dp(
      p=p_adsorpt,
      T=T_adsorpt,
      dp=dp)
      "Partial derivative of specific volume of the adsorpt w.r.t. pressure at
      constant temperature";
    dv_adsorpt_dT :=func_dv_adsorpt_dT(
      p=p_adsorpt,
      T=T_adsorpt,
      dT=dT)
      "Partial derivative of specific volume of the adsorpt w.r.t. temperature at
      constant pressure";

    //
    // Calculation of partial derivatives of molar sorption enthalpy
    //
    dh_ads_dp_T :=
      SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.dh_ads_dp(
        M_adsorptive=M_adsorptive,
        T_adsorpt=T_adsorpt,
        v_adsorptive=v_adsorptive,
        v_adsorpt=v_adsorpt,
        dv_adsorptive_dp=dv_adsorptive_dp,
        dv_adsorpt_dp=dv_adsorpt_dp,
        dx_adsorpt_dp=dx_adsorpt_dp,
        dx_adsorpt_dT=dx_adsorpt_dT,
        ddx_adsorpt_dp_dp=ddx_adsorpt_dp_dp,
        ddx_adsorpt_dp_dT=ddx_adsorpt_dp_dT)
    "Partial derivative of molar adsorption enthalpy w.r.t. pressure at constant
    temperature";
    dh_ads_dT_p :=
      SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.dh_ads_dT(
        M_adsorptive=M_adsorptive,
        T_adsorpt=T_adsorpt,
        v_adsorptive=v_adsorptive,
        v_adsorpt=v_adsorpt,
        dv_adsorptive_dT=dv_adsorptive_dT,
        dv_adsorpt_dT=dv_adsorpt_dT,
        dx_adsorpt_dp=dx_adsorpt_dp,
        dx_adsorpt_dT=dx_adsorpt_dT,
        ddx_adsorpt_dT_dT=ddx_adsorpt_dT_dT,
        ddx_adsorpt_dp_dT=ddx_adsorpt_dp_dT)
      "Partial derivative of molar adsorption enthalpy w.r.t. temperature at constant
      pressure";

  else
    //
    // Use Clausius Clyperon assumptions
    //
    dh_adsorptive_dT := func_dh_adsorptive_ig_dT(
      p=p_adsorpt,
      T=T_adsorpt,
      dT=dT)
      "Partial derivative of the specific enthalpy of the adsorptive w.r.t. temperature
      at constant pressure";

    dh_ads_dp_T :=
      SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.dh_ads_dp_clausiusClapeyron(
        p_adsorpt=p_adsorpt,
        T_adsorpt=T_adsorpt,
        dx_adsorpt_dp=dx_adsorpt_dp,
        dx_adsorpt_dT=dx_adsorpt_dT,
        ddx_adsorpt_dp_dp=ddx_adsorpt_dp_dp,
        ddx_adsorpt_dp_dT=ddx_adsorpt_dp_dT)
    "Partial derivative of molar adsorption enthalpy w.r.t. pressure at constant
    temperature";
    dh_ads_dT_p :=
      SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.dh_ads_dT_clausiusClapeyron(
        p_adsorpt=p_adsorpt,
        T_adsorpt=T_adsorpt,
        dx_adsorpt_dp=dx_adsorpt_dp,
        dx_adsorpt_dT=dx_adsorpt_dT,
        ddx_adsorpt_dp_dT=ddx_adsorpt_dp_dT,
        ddx_adsorpt_dT_dT=ddx_adsorpt_dT_dT)
      "Partial derivative of molar adsorption enthalpy w.r.t. temperature at constant
      pressure";

  end if;

  //
  // Calculation of the integrand
  //
  y := dh_adsorptive_dT -
    (dh_ads_dT_p - dh_ads_dp_T * dx_adsorpt_dT / dx_adsorpt_dp) / M_adsorptive
    "Integrand: Partial derivative of the specific enthalpy of the adsorpt w.r.t. 
    temperature at constant uptake";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function calculates the partial derivative of the specific enthalpy of the 
adsorpt with respect to temperature at constant uptake:
</p>
<pre>
    (&part;h<sub>adsorpt</sub>/&part;T)<sub>x</sub> = (&part;h<sub>adsorptive</sub>/&part;T)<sub>x</sub> - (&part;&Delta;h<sub>ads</sub>/&part;T)<sub>x</sub>;
</pre>

<h4>Main equations</h4>
<p>
The molar adsorption enthalpy <i>&Delta;h<sub>ads</sub></i> is defined as difference
of the specific enthalpy of the gas/vapor phase <i>h<sub>adsorptive</sub></i> and the
adsorpt phase <i>h<sub>adsorpt</sub></i>:
</p>
<pre>
    &Delta;h<sub>ads</sub> = h<sub>adsorptive</sub> - h<sub>adsorpt</sub> =T * M<sub>adsorptive</sub> * (v<sub>adsorptive</sub> - v<sub>adsorpt</sub>) (dp/dT) &asymp; T * M<sub>adsorptive</sub> * (v<sub>adsorptive</sub> - v<sub>adsorpt</sub>) * (-(&part;x/&part;T)<sub>p</sub> / (&part;x/&part;p)<sub>T</sub>);
</pre>
<p>
Herein, <i>M<sub>adsorptive</sub></i> is the molar mass of the adsorptive, <i>T</i> 
is the temperature, <i>v<sub>adsorptive</sub></i> ist the specific volume of the 
adsorptive, <i>v<sub>adsorpt</sub></i> ist the specific volume of the adsorpt, 
<i>dx/dp</i> is the partial derivative of the uptake with respect to the pressure
at constant temperature, and <i>dx/dT</i> is the partial derivative of the uptake 
with respect to the temperature at constant pressure.
</p>

<h4>Assumptions and limitations</h4>
<ul>
  <li>
  Inert sorbent.
  </li>
  <li>
  Neglecting the termn <i>(&part;p/&part;x)<sub>T</sub> * (dx/dp)</i> of term 
  <i>dp/dT = -(&part;x/&part;T)<sub>p</sub> / (&part;x/&part;p)<sub>T</sub> +
  (&part;p/&part;x)<sub>T</sub> * 
  (dx/dp)</i>.
  </li>
</ul>

<h4>References</h4>
<ul>
  <li>
  Bathen, D. and Breitbach, M. (2001). Adsorptionstechnik (in German), 1st Edition, ISBN 3-540-41908-X, Springer-Verlag Berlin Heidelberg New York.
  </li>
  <li>
  Chakraborty, A. and Saha, B.B. and Ng, K.C. and Koyama (2006). On the thermodynamic modeling of the isosteric heat of adsorption and comparison with experiments, Applied Physics Letters, 89:171901. DOI: http://doi.org/10.1063/1.2360925.
  </li>
  <li>
  Chakraborty, A. and Saha, B.B. and Ng, K.C. and Koyama, S. and Srinivasan, K. (2009). Theoretical Insight of Physical Adsorption for a Single-Component Adsorbent + Adsorbate System: I. Thermodynamic Property Surfaces, Langmuir, 25:2204-221. DOI: http://doi.org/10.1021/la803289p.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  November 20, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end dh_adsorpt_dT_x;
