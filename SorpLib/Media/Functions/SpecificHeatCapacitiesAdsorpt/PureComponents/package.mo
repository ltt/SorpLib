within SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt;
package PureComponents "Functions required to calculate specific heat capacities of adsorpt phases for pure components"
  extends Modelica.Icons.VariantsPackage;

  annotation (Documentation(info="<html>
<p>
This package contains models to calculate specific heat capacities of adsorpt 
phases for pure component adsorption.
</p>

<h4>Implemented functions for each specific heat capacity model</h4>
<p>
The following functions are provided for each specific heat capacity model:
</p>
<ul>
  <li>
  Specific heat capacity.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end PureComponents;
