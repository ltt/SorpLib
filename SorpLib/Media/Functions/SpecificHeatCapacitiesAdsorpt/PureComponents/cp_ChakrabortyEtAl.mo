﻿within SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents;
function cp_ChakrabortyEtAl
  "Specific heat capacity of adsorpt phase according to Chakraborty et al. (2007)"

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.MolarMass M_adsorptive
    "Molar mass of the adsorptive"
    annotation (Dialog(tab="General", group="Inputs"));
  extends
    SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.BasesClasses.PartialPure_cp;

  input Modelica.Units.SI.SpecificHeatCapacity cp_adsorptive
    "Specific heat capacity of adsorptive"
    annotation (Dialog(tab="General", group="Inputs"));

  input Modelica.Units.SI.SpecificVolume v_adsorptive
    "Specific volume of the adsorptive"
    annotation (Dialog(tab="General", group="Inputs"));
  input SorpLib.Units.DerSpecificVolumeByTemperature dv_adsorptive_dT
    "Partial derivative of specific volume of the adsorptive w.r.t. temperature
    at constant pressure"
    annotation (Dialog(tab="General", group="Inputs"));

  input Modelica.Units.SI.SpecificVolume v_adsorpt
    "Specific volume of the v_adsorpt"
    annotation (Dialog(tab="General", group="Inputs"));

  input Modelica.Units.SI.MolarEnthalpy h_ads
    "Molar adsorption enthalpy"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.MolarHeatCapacity dh_ads_dT_x
    "Partial derivative of molar adsorption enthalpy w.r.t. temperature at constant
    uptake"
    annotation (Dialog(tab="General", group="Inputs"));

algorithm
  cp_adsorpt := cp_adsorptive + (v_adsorptive - T_adsorpt * dv_adsorptive_dT) *
    h_ads / (M_adsorptive * T_adsorpt * (v_adsorptive - v_adsorpt)) -
    dh_ads_dT_x / M_adsorptive
    "Specific heat capacitiy of adsorpt";

  //
  // Annotations
  //
  annotation (Inline=true,
Documentation(info="<html>
<p>
This function calculates the specific heat capacity of the adsorpt phase for a
pure component according to Chakraborty et al. (2009).
</p>

<h4>Main equations</h4>
<p>
The specific heat capacity is defined as follows:
</p>
<pre>
    c<sub>x,adsorpt</sub> = (&part;h<sub>adsorpt</sub>/&part;T<sub>adsorpt</sub>)<sub>p</sub> + (&part;h<sub>adsorpt</sub>/&part;p<sub>adsorpt</sub>)<sub>T</sub> * (&part;p<sub>adsorpt</sub>/&part;T<sub>adsorpt</sub>)<sub>x</sub>;
</pre>
<p>
Herein, <i>(&part;h<sub>adsorpt</sub>/&part;T<sub>adsorpt</sub>)<sub>p</sub></i>
is the partial derivative of the specific enthalpy of the adsorpt w.r.t. temperature
at constant pressure
<i>(&part;h<sub>adsorpt</sub>/&part;p<sub>adsorpt</sub>)<sub>T</sub></i>
is the partial derivative of the specific enthalpy of the adsorpt w.r.t. pressure
at constant temperature, and 
<i>(&part;p<sub>adsorpt</sub>/&part;T<sub>adsorpt</sub>)<sub>x</sub></i>
is the partial derivative of the pressure w.r.t. temperature at constant uptake. Now,
Chakraborty et al. assume that the adsorpt properties can be substituted by adsorptive
properties (i.e., gas/vapor phase) and they substitute the Clapeyron equation. Thus, the
specific heat capacity can be expressed as:
</p>
<pre>
    c<sub>x,adsorpt</sub> = c<sub>p,adsorptive</sub> + (v<sub>adsorptive</sub> - T<sub>adsorpt</sub> * (&part;v<sub>adsorptive</sub>/&part;T)<sub>p</sub>) * h<sub>ads</sub> / (M<sub>adsorptive</sub> * T<sub>adsoropt</sub> * (v<sub>adsorptive</sub> - v<sub>adsorpt</sub>)) - (&part;h<sub>ads</sub>/&part;T)<sub>x</sub>
</pre>
<p>
Herein, <i>T<sub>adsorpt</sub></i> is the equilibrium temperature, <i>c<sub>p,adsorptive</sub></i> 
is the specific heat capacity of the adsorptive, <i>v<sub>adsorptive</sub></i> is 
the specific volume of the adsorptive,<i><i>v<sub>adsorpt</sub></i></i> is the
specific volume of the adsorpt, <i>(&part;v<sub>adsorptive</sub>/&part;T)<sub>p</sub></i> 
is the partial derivative of the specic volume of the adsorptive w.r.t. temperature
at constant pressure, <i>h<sub>ads</sub></i> is the molar adsorption enthalpy, 
<i>(&part;h<sub>ads</sub>/&part;T)<sub>x</sub></i> is the partial derivative of the
molar enthalpy w.r.t. temperature at constant uptake, and <i>M<sub>adsorptive</sub></i>
is the molar mass of the adsorptive.
</p>

<h4>References</h4>
<ul>
  <li>
  Chakraborty, A. and Saha, B.B. and Ng, K.C. and Koyama, S. and Srinivasan, K. (2009). Theoretical Insight of Physical Adsorption for a Single-Component Adsorbent + Adsorbate System: I. Thermodynamic Property Surfaces, Langmuir, 25:2204-221. DOI: http://doi.org/10.1021/la803289p.
  </li>
  <li>
  Schwamberger, V. and Schmidt, F.P. (2013). Estimating the Heat Capacity of the Adsorbate−Adsorbent System from Adsorption Equilibria Regarding Thermodynamic Consistency, Industrial & Engineering Chemostry Research, 52:16958−16965. DOI: https://doi.org/10.1021/ie4011832.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  November 20, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end cp_ChakrabortyEtAl;
