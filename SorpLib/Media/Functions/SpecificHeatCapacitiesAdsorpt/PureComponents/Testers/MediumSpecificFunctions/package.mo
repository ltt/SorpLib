within SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.Testers;
package MediumSpecificFunctions "Package containing medium specific functions that are used as functional arguments in integrand functions"
  extends Modelica.Icons.InternalPackage;

  //
  // Definition of replaceable medium package: This is required to have access to
  // functions that change with the selected medium. These functions are used as
  // functional input arguments.
  //
  replaceable package Medium = Modelica.Media.Water.WaterIF97_ph
    constrainedby Modelica.Media.Interfaces.PartialTwoPhaseMedium
    "Medium (i.e., adsorptive)"
    annotation (Dialog(tab="General",group="Medium"),
                choicesAllMatching = true);
  //
  // Redeclaration of all replacable partial functions regarding the adsorptive
  //
  replaceable function v_adsorptive_pT
  "Calculates specific volume of adsorptive as function pressure and temperature"
    extends
    SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.Integrands.Interfaces.Partial_v_pT;

  algorithm
    v :=1/Medium.density_pT(p=p, T=T)
      "Specific volume of the adsorptive";
  end v_adsorptive_pT;

  replaceable function dv_adsorptive_dp
    "Calculates the partial derivative of the specific volume of the adsotpive w.r.t. pressure at constant temperature"
    extends
    SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.Integrands.Interfaces.Partial_dv_dp;

  algorithm
    dv_dp :=
      (1/Medium.density_pT(p=p+dp, T=T) - 1/Medium.density_pT(p=p-dp, T=T)) /
      (2*dp)
      "Partial derivative of the specific volume of the adsorptive w.r.t.
      pressure at constant temperature";
  end dv_adsorptive_dp;

  replaceable function dv_adsorptive_dT
    "Calculates the partial derivative of the specific volume of the adsotpive w.r.t. temperature at constant pressure"
    extends
    SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.Integrands.Interfaces.Partial_dv_dT;

  algorithm
    dv_dT :=
      (1/Medium.density_pT(p=p, T=T+dT) - 1/Medium.density_pT(p=p, T=T-dT)) /
      (2*dT)
      "Partial derivative of the specific volume of the adsorptive w.r.t.
      temperature at constant pressure";
  end dv_adsorptive_dT;

  replaceable function dh_adsorptive_dT
  "Calculates specific enthalpy of adsorptive as function pressure and temperature"
    extends
    SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.Integrands.Interfaces.Partial_dh_dT;

  algorithm
    dh_dT := (Medium.specificEnthalpy_pT(p=p, T=T+dT) -
      Medium.specificEnthalpy_pT(p=p, T=T-dT)) / (2 * dT)
      "Partial derivative of the specific enthalpy w.r.t. temperature at constant
    pressure";
  end dh_adsorptive_dT;

  replaceable function dh_adsorptive_ig_dT
    "Calculates specific volume of the adsorptive (ideal gas) as function temperature"
    extends
    SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.Integrands.Interfaces.Partial_dh_dT;

  algorithm
    dh_dT := 1860
      "Partial derivative of the specific enthalpy w.r.t. temperature at constant
    pressure";
  end dh_adsorptive_ig_dT;

  replaceable function h_adsorptiveToLiquid_pT
    "Base function for functions calculating specific enthalpy differences between adsorptive state and saturated liquid state (i.e., bubble point)"
    extends
      SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.Integrands.Interfaces.Partial_h_adsorptiveToLiquid_pT;

    //
    // Definition of variables
    //
protected
    Medium.ThermodynamicState state_pT
      "State properties at p and T";
    Medium.SaturationProperties sat_T
      "Saturated state properties at T";

  algorithm
    state_pT := Medium.setState_pT(p=p, T=T)
      "State properties at p and T";
    sat_T :=Medium.setSat_T(T=T)
      "Saturated state properties at T";

    h_adsorptiveToLiquid :=Medium.specificEnthalpy(state=state_pT) -
      Medium.bubbleEnthalpy(sat=sat_T)
      "Specific enthalpy difference between adsorptive state and saturated liquid
    state (i.e., bubble point)";
  end h_adsorptiveToLiquid_pT;

  replaceable function dh_adsorptiveToLiquid_dp
    "Base function for functions calculating the partial derivative of specific enthalpy differences between adsorptive state and saturated liquid state (i.e., bubble point) w.r.t. pressure at constant temperature"
    extends
    SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.Integrands.Interfaces.Partial_dh_adsorptiveToLiquid_dp;

    //
    // Definition of variables
    //
protected
    Medium.ThermodynamicState state_pT
      "State properties at p and T";

  algorithm
    //
    // The second summand of the partial derivative is zero because the specific
    // enthalpy of vaporization does only depend on the temperature, which determines
    // the saturation state but is constant
    //
    state_pT := Medium.setState_pT(p=p, T=T)
      "State properties at p and T";

    dh_adsorptiveToLiquid_dp := 1/Medium.density(state=state_pT) * (1 - T *
      Medium.isobaricExpansionCoefficient(state=state_pT)) - 0
      "Partial derivative of specific enthalpy difference between adsorptive state 
    and saturated liquid state (i.e., bubble point) w.r.t. pressure at constant 
    temperature";
  end dh_adsorptiveToLiquid_dp;

  replaceable function dh_adsorptiveToLiquid_dT
    "Base function for functions calculating the partial derivative of specific enthalpy differences between adsorptive state and saturated liquid state (i.e., bubble point) w.r.t. temperature at constant pressure"
    extends
    SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.Integrands.Interfaces.Partial_dh_adsorptiveToLiquid_dT;

    //
    // Definition of variables
    //
protected
    Medium.SaturationProperties sat_T
      "Saturated state properties at T";
    Medium.ThermodynamicState state_pT
      "State properties at p and T";
    Medium.ThermodynamicState state_bubble_T
      "Bubble state properties at p and T";

  algorithm
    sat_T :=Medium.setSat_T(T=T)
      "Saturated state properties at T";
    state_pT := Medium.setState_pT(p=p, T=T)
      "State properties at p and T";
    state_bubble_T := Medium.setBubbleState(sat=sat_T)
      "Bubble state properties at T";

    dh_adsorptiveToLiquid_dT :=
      Medium.specificHeatCapacityCp(state=state_pT) -
      Medium.specificHeatCapacityCp(state=state_bubble_T)
      "Partial derivative of specific enthalpy difference between adsorptive state 
    and saturated liquid state (i.e., bubble point) w.r.t. temperature at constant
    pressure";
  end dh_adsorptiveToLiquid_dT;
  //
  // Redeclaration of all replacable partial functions regarding the adsorpt
  //
  replaceable function v_adsorpt_pT
    "Calculates specific volume of the adsorpt as function pressure and temperature"
    extends
    SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.Integrands.Interfaces.Partial_v_pT;

  algorithm
    v := 1 / Medium.bubbleDensity(sat=Medium.setSat_T(T=T))
      "Specific volume of the adsorptive";
  end v_adsorpt_pT;

  replaceable function dv_adsorpt_dp
    "Calculates the partial derivative of specific volume of the adsorpt w.r.t. pressure at constant temperature"
    extends
    SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.Integrands.Interfaces.Partial_dv_dp;

    //
    // Definition of variables
    //
protected
    Medium.SaturationProperties sat_T
      "Saturated state properties at T";

  algorithm
    //
    // The partial derivative is zero because the specific volume of the adsorpt
    // does only depend on the temperature, which determines the saturation state
    // but is constant
    //
    sat_T :=Medium.setSat_T(T=T)
      "Saturated state properties at T";
    dv_dp := dv_adsorpt_dT(p=p, T=T, dT=dp*1)
      * Medium.saturationTemperature_derp_sat(sat=sat_T) * 0
      "Partial derivative of the specific volume of the adsorptive w.r.t.
      pressure at constant temperature";
  end dv_adsorpt_dp;

  replaceable function dv_adsorpt_dT
    "Calculates the partial derivative of specific volume of the adsorpt w.r.t. temperature at constant pressure"
    extends
    SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.Integrands.Interfaces.Partial_dv_dT;

    //
    // Definition of variables
    //
protected
    Medium.SaturationProperties sat_T
      "Saturated state properties at T";

  algorithm
    sat_T :=Medium.setSat_T(T=T)
      "Saturated state properties at T";
    dv_dT := -(1 / Medium.bubbleDensity(sat=sat_T)) ^ 2 *
      Medium.dBubbleDensity_dPressure(sat=sat_T) /
      Medium.saturationTemperature_derp_sat(sat=sat_T)
      "Partial derivative of the specific volume of the adsorptive w.r.t.
      temperature at constant pressure";
  end dv_adsorpt_dT;

  replaceable function beta_adsorpt_pT
    "Calculates isobaric expansion coefficient of the adsorpt as function pressure and temperature"
    extends
    SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.Integrands.Interfaces.Partial_beta_pT;

    //
    // Definition of variables
    //
protected
    Medium.SaturationProperties sat_T
      "Saturated state properties at T";
    Medium.ThermodynamicState state_bubble_T
      "State properties of ubble point at T";

  algorithm
    sat_T :=Medium.setSat_T(T=T)
      "Saturated state properties at T";
    state_bubble_T :=Medium.setBubbleState(sat=sat_T)
      "State properties of ubble point at T";

    beta :=Medium.isobaricExpansionCoefficient(state=state_bubble_T)
      "Isobaric expansion coefficient";
  end beta_adsorpt_pT;

  replaceable function dbeta_adsorpt_dp
    "Calculates the partial derivative of the isobaric expansion coefficient of the adsorpt w.r.t. pressure at constant temperature"
    extends
    SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.Integrands.Interfaces.Partial_dbeta_dp;

    //
    // Definition of variables
    //
protected
    Medium.SaturationProperties sat_T
      "Saturated state properties at T";

  algorithm
    //
    // The partial derivative is zero because the isobaric expansion coefficient of
    // the adsorpt does only depend on the temperature, which determines the saturation
    // state but is constant
    //
    sat_T :=Medium.setSat_T(T=T)
      "Saturated state properties at T";
    dbeta_dp := dbeta_adsorpt_dT(p=p, T=T, dT=dp*1)
      * Medium.saturationTemperature_derp_sat(sat=sat_T) * 0
      "Partial derivative of isobaric expansion coefficients w.r.t. pressure at 
     constant temperature";
  end dbeta_adsorpt_dp;

  replaceable function dbeta_adsorpt_dT
    "Calculates the partial derivative of the isobaric expansion coefficient of the adsorpt w.r.t. temperature at constant pressure"
    extends
    SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.Integrands.Interfaces.Partial_dbeta_dT;

    //
    // Definition of variables
    //
protected
    Medium.SaturationProperties sat_T_pdT
      "Saturated state properties at T + dT";
    Medium.SaturationProperties sat_T_mdT
      "Saturated state properties at T - dT";

    Medium.ThermodynamicState state_bubble_T_pdT
      "State properties of ubble point at T + dT";
    Medium.ThermodynamicState state_bubble_T_mdT
      "State properties of ubble point at T - dT";

  algorithm
    sat_T_pdT :=Medium.setSat_T(T=T+dT)
      "Saturated state properties at T + dT";
    sat_T_mdT :=Medium.setSat_T(T=T-dT)
      "Saturated state properties at T - dT";

    state_bubble_T_pdT :=Medium.setBubbleState(sat=sat_T_pdT)
      "State properties of ubble point at T + dT";
    state_bubble_T_mdT :=Medium.setBubbleState(sat=sat_T_mdT)
      "State properties of ubble point at T - dT";

    dbeta_dT :=
      (Medium.isobaricExpansionCoefficient(state=state_bubble_T_pdT) -
      Medium.isobaricExpansionCoefficient(state=state_bubble_T_mdT)) / (2 * dT)
      "Partial derivative of isobaric expansion coefficients w.r.t. temperature at 
      constant pressure";
  end dbeta_adsorpt_dT;
  //
  // Annoations
  //
  annotation (Documentation(revisions="<html>
<ul>
  <li>
  November 20, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>", info="<html>
<p>
This package defines functions that are used as functional arguments in integrante 
functions and use media-specific functions. More details on the integand functions 
can be found here:
<a href=\"Modelica://SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.Integrands\">SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.Integrands</a>.
<br><br>
Within this package, the media-specific functions are provided via the media library 
of the Modelica standard Library. This package demonstrates one possibility to build
genral models that can use different Modelica libraries for media property calculation
by just exchaning this package. This package is only used for the testers and it should
not be used within other simulation models.
</p>
</html>"));
end MediumSpecificFunctions;
