within SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.Testers;
model Test_cp_WaltonLeVan
  "Tester for the function 'cp_WaltonLeVan' and all corresponding functions"
  extends
    SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.BasesClasses.PartialTestPure;

  //
  // Load functions package and select correct medium
  //
  package MediumSpecificFunctionsForIntegrands =
    SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.Testers.MediumSpecificFunctions
      (redeclare final package Medium = Medium)
    "Library containing medium-specific functions that are used as functional
    arguments"
    annotation (Dialog(tab="General", group="Functions"),
                choicesAllMatching=true);

  //
  // Definition of parameters
  //
  replaceable function func_x_pT =
    SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.x_pT
    constrainedby
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_x_pT
    "Classical form of isotherm model: Calculates uptake as function of pressure and temperature"
    annotation (Dialog(tab="General", group="Functions"),
                choicesAllMatching=true);
  replaceable function func_p_xT =
    SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.p_xT
    constrainedby
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_p_xT
    "Inverse form of General: Calculates pressure as function of uptake 
    and temperature"
    annotation (Dialog(tab="General", group="Functions"),
                choicesAllMatching=true);
  replaceable function func_dx_dp =
    SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.dx_dp
    constrainedby
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_dx_dp
    "Partial derivative of uptake w.r.t. pressure at constant temperature as 
    function of pressure and temperature"
    annotation (Dialog(tab="General", group="Functions"),
                choicesAllMatching=true);
  replaceable function func_dx_dT =
    SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.dx_dT
    constrainedby
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_dx_dT
    "Partial derivative of uptake w.r.t. temperature at constant pressure as 
    function of pressure and temperature"
    annotation (Dialog(tab="General", group="Functions"),
                choicesAllMatching=true);
  replaceable function func_ddx_dp_dp =
    SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.ddx_dp_dp
    constrainedby
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_ddx_dp_dp
    "Second-order partial derivative of uptake w.r.t. pressure at constant temperature 
    as function of pressure and temperature"
    annotation (Dialog(tab="General", group="Functions"),
                choicesAllMatching=true);
  replaceable function func_ddx_dT_dT =
    SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.ddx_dT_dT
    constrainedby
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_ddx_dT_dT
    "Second-order partial derivative of uptake w.r.t. temperature at constant pressure
    as function of pressure and temperature"
    annotation (Dialog(tab="General", group="Functions"),
                choicesAllMatching=true);
  replaceable function func_ddx_dp_dT =
    SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.ddx_dp_dT
    constrainedby
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_ddx_dp_dT
    "Second-order partial derivative of uptake w.r.t. pressure and temperature
    as function of pressure and temperature"
    annotation (Dialog(tab="General", group="Functions"),
                choicesAllMatching=true);
  replaceable function func_dW_dA =
    SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.dW_dA
    constrainedby
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_dW_dA
    "Partial derivative of filled pore volume w.r.t. molar adsorption potential
    at constant pressure and temperature"
    annotation (Dialog(tab="General", group="Functions"),
                choicesAllMatching=true);
  replaceable function func_ddW_dA_dA =
    SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.ddW_dA_dA
    constrainedby
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_ddW_dA_dA
    "Second-order prtial derivative of filled pore volume w.r.t. molar adsorption potential
    at constant pressure and temperature"
    annotation (Dialog(tab="General", group="Functions"),
                choicesAllMatching=true);
  replaceable function func_ddW_dA_dT =
    SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.ddW_dA_dT
    constrainedby
    SorpLib.Media.Functions.SorptionEquilibria.BaseClasses.PartialPure_ddW_dA_dT
    "Partial derivative of filled pore volume w.r.t. molar adsorption potential
    and temperature at constant pressure"
    annotation (Dialog(tab="General", group="Functions"),
                choicesAllMatching=true);

equation
  //
  // Definition of derivatives
  //
  der(p_adsorpt) = 0.75 * dc_dT[1]
    "Predecsriped slope of p_adsorpt";
  der(T_adsorpt) = 90/20
    "Predecsriped slope of T_adsorpt";

  //
  // Calculate coefficients of the isotherm model
  //
  c[1] = state_bubble_T.p;
  c[2] = state_bubble_T.d;
  c[3] = 5.072313e-1 / 1000;
  c[4] = 1.305531e2 * 1000 * 0.018;
  c[5] = -8.492403e1 * 1000 * 0.018;
  c[6] = 4.128962e-3 / 1000;

  c_pdT[1] = sat_T_pdT.psat;
  c_pdT[2] = Medium.bubbleDensity(sat=sat_T_pdT);
  c_pdT[3] = 5.072313e-1 / 1000;
  c_pdT[4] = 1.305531e2 * 1000 * 0.018;
  c_pdT[5] = -8.492403e1 * 1000 * 0.018;
  c_pdT[6] = 4.128962e-3 / 1000;

  c_mdT[1] = sat_T_mdT.psat;
  c_mdT[2] = Medium.bubbleDensity(sat=sat_T_mdT);
  c_mdT[3] = 5.072313e-1 / 1000;
  c_mdT[4] = 1.305531e2 * 1000 * 0.018;
  c_mdT[5] = -8.492403e1 * 1000 * 0.018;
  c_mdT[6] = 4.128962e-3 / 1000;

  //
  // Calculate partial derivatives of coefficients of the isotherm model w.r.t.
  // temperature
  //
  dc_dT[1] = 1/Medium.saturationTemperature_derp_sat(sat=sat_T);
  dc_dT[2] = Medium.dBubbleDensity_dPressure(sat=sat_T) * dc_dT[1];
  dc_dT[3] = 0;
  dc_dT[4] = 0;
  dc_dT[5] = 0;
  dc_dT[6] = 0;

  dc_dT_pdT[1] = 1/Medium.saturationTemperature_derp_sat(sat=sat_T_pdT);
  dc_dT_pdT[2] = Medium.dBubbleDensity_dPressure(sat=sat_T_pdT) * dc_dT_pdT[1];
  dc_dT_pdT[3] = 0;
  dc_dT_pdT[4] = 0;
  dc_dT_pdT[5] = 0;
  dc_dT_pdT[6] = 0;

  dc_dT_mdT[1] = 1/Medium.saturationTemperature_derp_sat(sat=sat_T_mdT);
  dc_dT_mdT[2] = Medium.dBubbleDensity_dPressure(sat=sat_T_mdT) * dc_dT_mdT[1];
  dc_dT_mdT[3] = 0;
  dc_dT_mdT[4] = 0;
  dc_dT_mdT[5] = 0;
  dc_dT_mdT[6] = 0;

  //
  // Calculate second-order partial derivatives of coefficients of the isotherm
  // model w.r.t. temperature
  //
  ddc_dT_dT[1] = (dc_dT_pdT[1] - dc_dT_mdT[1]) / (2*dT);
  ddc_dT_dT[2] = (dc_dT_pdT[2] - dc_dT_mdT[2]) / (2*dT);
  ddc_dT_dT[3] = 0;
  ddc_dT_dT[4] = 0;
  ddc_dT_dT[5] = 0;
  ddc_dT_dT[6] = 0;

  //
  // Calculate properties
  //
  x_adsorpt = func_x_pT(
    p_adsorpt=p_adsorpt,
    T_adsorpt=T_adsorpt,
    c=c,
    p_adsorpt_lb_start=1,
    p_adsorpt_ub_start=10,
    tolerance=100*Modelica.Constants.eps)
    "Uptake";

  //
  // Calculate specific heat capacities of adsorpt phase
  //
  cp_adsorpt = SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.cp_WaltonLeVan(
    cp_adsorptive=cp_adsorptive,
    M_adsorptive=M_adsorptive,
    T_adsorpt=T_adsorpt,
    x_adsorpt=x_adsorpt,
    c=c,
    dc_dT=dc_dT,
    ddc_dT_dT=ddc_dT_dT,
    func_p_xT=function func_p_xT(),
    func_dx_dp=function func_dx_dp(),
    func_dx_dT=function func_dx_dT(),
    func_ddx_dp_dp=function func_ddx_dp_dp(),
    func_ddx_dT_dT=function func_ddx_dT_dT(),
    func_ddx_dp_dT=function func_ddx_dp_dT(),
    func_v_adsorptive_pT=function MediumSpecificFunctionsForIntegrands.v_adsorptive_pT(),
    func_dv_adsorptive_dp=function MediumSpecificFunctionsForIntegrands.dv_adsorptive_dp(),
    func_dv_adsorptive_dT=function MediumSpecificFunctionsForIntegrands.dv_adsorptive_dT(),
    func_v_adsorpt_pT=function MediumSpecificFunctionsForIntegrands.v_adsorpt_pT(),
    func_dv_adsorpt_dp=function MediumSpecificFunctionsForIntegrands.dv_adsorpt_dp(),
    func_dv_adsorpt_dT=function MediumSpecificFunctionsForIntegrands.dv_adsorpt_dT(),
    p_clausiusClyperon=615,
    x_adsorpt_lb=0.006,
    tolerance=1e-2,
    dp=dp,
    dT=dT)
    "Specific heat capacitiy of adsorpt";

  cp_adsorpt_CC = SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.cp_WaltonLeVan_clausiusClapeyron(
    cp_adsorptive=cp_adsorptive,
    M_adsorptive=M_adsorptive,
    T_adsorpt=T_adsorpt,
    x_adsorpt=x_adsorpt,
    c=c,
    dc_dT=dc_dT,
    ddc_dT_dT=ddc_dT_dT,
    func_p_xT=function func_p_xT(),
    func_dx_dp=function func_dx_dp(),
    func_dx_dT=function func_dx_dT(),
    func_ddx_dp_dp=function func_ddx_dp_dp(),
    func_ddx_dT_dT=function func_ddx_dT_dT(),
    func_ddx_dp_dT=function func_ddx_dp_dT(),
    x_adsorpt_lb=0.006,
    tolerance=1e-2)
    "Specific heat capacitiy of adsorpt calculated using molar adsorption enthalpy
    according to Clausius Clapeyron";

  cp_adsorpt_Dubinin =
    SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.cp_WaltonLeVan_Dubinin(
    cp_adsorptive=cp_adsorptive,
    M_adsorptive=M_adsorptive,
    T_adsorpt=T_adsorpt,
    x_adsorpt=x_adsorpt,
    c=c,
    dc_dT=dc_dT,
    ddc_dT_dT=ddc_dT_dT,
    func_p_xT=function func_p_xT(),
    func_dx_dp=function func_dx_dp(),
    func_dx_dT=function func_dx_dT(),
    func_ddx_dp_dp=function func_ddx_dp_dp(),
    func_ddx_dT_dT=function func_ddx_dT_dT(),
    func_ddx_dp_dT=function func_ddx_dp_dT(),
    func_h_adsorptiveToLiquid_pT=function MediumSpecificFunctions.h_adsorptiveToLiquid_pT(),
    func_dh_adsorptiveToLiquid_dp=function MediumSpecificFunctionsForIntegrands.dh_adsorptiveToLiquid_dp(),
    func_dh_adsorptiveToLiquid_dT=function MediumSpecificFunctionsForIntegrands.dh_adsorptiveToLiquid_dT(),
    func_v_adsorpt_pT=function MediumSpecificFunctionsForIntegrands.v_adsorpt_pT(),
    func_dv_adsorpt_dp=function MediumSpecificFunctionsForIntegrands.dv_adsorpt_dp(),
    func_dv_adsorpt_dT=function MediumSpecificFunctionsForIntegrands.dv_adsorpt_dT(),
    func_beta_adsorpt_pT=function MediumSpecificFunctionsForIntegrands.beta_adsorpt_pT(),
    func_dbeta_adsorpt_dp=function MediumSpecificFunctionsForIntegrands.dbeta_adsorpt_dp(),
    func_dbeta_adsorpt_dT=function MediumSpecificFunctionsForIntegrands.dbeta_adsorpt_dT(),
    func_dW_dA=function func_dW_dA(),
    func_ddW_dA_dA=function func_ddW_dA_dA(),
    func_ddW_dA_dT=function func_ddW_dA_dT(),
    p_clausiusClyperon=615,
    x_adsorpt_lb=0.006,
    tolerance=1e-2,
    dp=dp,
    dT=dT)
    "Specific heat capacitiy of adsorptive using molar adsorption enthalpy
    according to Dubinin model";

  //
  // Annotations
  //
  annotation (experiment(
      StopTime=20,
      Tolerance=1e-06),
Documentation(revisions="<html>
<ul>
  <li>
  November 20, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>", info="<html>
<p>
This tester shows the behavior of the 'cp_WaltonLeVan' function.
<br/><br/>
To see the function behavior, plot the variables <i>cp_i</i> over the time. The 
simulation time is correctly preset (Start: 0 s, Stop = 20 s). 
</p>
</html>"));
end Test_cp_WaltonLeVan;
