within SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.Testers;
model Test_cp_ChakrabortyEtAl
  "Tester for the function 'cp_ChakrabortyEtAl' and all corresponding functions"
  extends
    SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.BasesClasses.PartialTestPure;

  //
  // Definition of variables
  //
  Modelica.Units.SI.MolarEnthalpy h_ads
    "Molar adsorption enthalpy";
  Modelica.Units.SI.MolarEnthalpy h_ads_CC
    "Molar adsorption enthalpy according to Clausius Clapeyron";
  Modelica.Units.SI.MolarEnthalpy h_ads_Dubinin
    "Molar adsorption enthalpy according to Dubinin model";

  SorpLib.Units.DerMolarEnthalpyByPressure dh_ads_dp_T
    "Partial derivative of molar adsorption enthalpy w.r.t. pressure at constant
    temperature";
  SorpLib.Units.DerMolarEnthalpyByPressure dh_ads_dp_T_CC
    "Partial derivative of molar adsorption enthalpy w.r.t. pressure at constant
    temperature according to Clausius Clapeyron";
  SorpLib.Units.DerMolarEnthalpyByPressure dh_ads_dp_T_Dubinin
    "Partial derivative of molar adsorption enthalpy w.r.t. pressure at constant
    temperature according to Dubinin model";

  Modelica.Units.SI.MolarHeatCapacity dh_ads_dT_p
    "Partial derivative of molar adsorption enthalpy w.r.t. temperature at constant
    pressure";
  Modelica.Units.SI.MolarHeatCapacity dh_ads_dT_p_CC
    "Partial derivative of molar adsorption enthalpy w.r.t. temperature at constant
    pressure according to Clausius Clapeyron";
  Modelica.Units.SI.MolarHeatCapacity dh_ads_dT_p_Dubinin
    "Partial derivative of molar adsorption enthalpy w.r.t. temperature at constant
    pressure according to Dubinin model";

  Modelica.Units.SI.MolarHeatCapacity dh_ads_dT_x
    "Partial derivative of molar adsorption enthalpy w.r.t. temperature at constant
    uptake";
  Modelica.Units.SI.MolarHeatCapacity dh_ads_dT_x_CC
    "Partial derivative of molar adsorption enthalpy w.r.t. temperature at constant
    uptake according to Clausius Clapeyron";
  Modelica.Units.SI.MolarHeatCapacity dh_ads_dT_x_Dubinin
    "Partial derivative of molar adsorption enthalpy w.r.t. temperature at constant
    uptake according to Dubinin model";

protected
  Medium.ThermodynamicState state_adsorptive_pT_pdp
    "State properties of adsorptive at p_adsorpt and T_adsorpt: p + dp";
  Medium.ThermodynamicState state_adsorptive_pT_mdp
    "State properties of adsorptive at p_adsorpt and T_adsorpt: p - dp";
  Medium.ThermodynamicState state_adsorptive_pT_pdT
    "State properties of adsorptive at p_adsorpt and T_adsorpt: T + dT";
  Medium.ThermodynamicState state_adsorptive_pT_mdT
    "State properties of adsorptive at p_adsorpt and T_adsorpt: T - dT";

  Medium.ThermodynamicState state_bubble_T_pdT
    "State properties of bubble point at T_adsorpt: T + dT";
  Medium.ThermodynamicState state_bubble_T_mdT
    "State properties of bubble point at T_adsorpt: T - dT";

  SorpLib.Units.DerUptakeByPressure dx_adsorpt_dp
    "Partial derivative of uptake w.r.t. pressure at constant temperature";
  SorpLib.Units.DerUptakeByTemperature dx_adsorpt_dT
    "Partial derivative of uptake w.r.t. temperature at constant prssure";

  SorpLib.Units.DerUptakeByPressurePressure ddx_adsorpt_dp_dp
    "Second-order partial derivative of uptake w.r.t. pressure at contant 
    temperature";
  SorpLib.Units.DerUptakeByTemperatureTemperature ddx_adsorpt_dT_dT
    "Second-order partial derivative of uptake w.r.t. temperature at contant 
    pressure";
  SorpLib.Units.DerUptakeByPressureTemperature ddx_adsorpt_dp_dT
    "Second-order partial derivative of uptake w.r.t. pressure and temperature";

  Modelica.Units.SI.SpecificEnthalpy h_adsorptiveToLiquid
    "Specific enthalpy difference between adsorptive state and saturated liquid
    state (i.e., bubble point)";
  SorpLib.Units.DerSpecificEnthalpyByPressure dh_adsorptiveToLiquid_dp
    "Partial derivative of specific enthalpy difference between adsorptive state 
    and saturated liquid state (i.e., bubble point) w.r.t. pressure at constant 
    temperature";
  Modelica.Units.SI.SpecificHeatCapacity dh_adsorptiveToLiquid_dT
    "Partial derivative of specific enthalpy difference between adsorptive state 
    and saturated liquid state (i.e., bubble point) w.r.t. temperature at constant
    pressure";

  Modelica.Units.SI.RelativePressureCoefficient beta_adsorpt
    "Isobaric expansion coefficient of the adsorpt phase";
  SorpLib.Units.DerIsobaricExpansionCoefficientByTemperature dbeta_adsorpt_dT
    "Partial derivative of isobaric expansion coefficient of the adsorpt phase
    w.r.t. temperature at constant pressure";

  SorpLib.Units.MolarAdsorptionPotential A
    "Molar adsorption potential";
  SorpLib.Units.DerMolarAdsorptionPotentialByPressure dA_dp
    "Partial derivative of molar adsorption potential w.r.t. pressure at
    constant temperature";
  SorpLib.Units.DerMolarAdsorptionPotentialByTemperature dA_dT
    "Partial derivative of molar adsorption potential w.r.t. temperature at
    constant pressure";

  SorpLib.Units.DerFilledPoreVolumeByAdsorptionPotential dW_dA
    "Partial derivative of filled pore volume w.r.t. molar adsorption potential
    at constant pressure and temperature";
  SorpLib.Units.DerFilledPoreVolumeByAdsorptionPotentialAdsorptionPotential ddW_dA_dA
    "Partial derivative of filled pore volume w.r.t. molar adsorption potential
    at constant pressure and temperature";
  SorpLib.Units.DerFilledPoreVolumeByAdsorptionPotentialTemperature ddW_dA_dT
    "Second-order partial derivative of filled pore volume w.r.t. molar adsorption 
    potential and temperature at constant pressure";

equation
  //
  // Definition of derivatives
  //
  der(p_adsorpt) = 0.75 * dc_dT[1]
    "Predecsriped slope of p_adsorpt";
  der(T_adsorpt) = 90/20
    "Predecsriped slope of T_adsorpt";

  //
  // Calculate coefficients of the isotherm model
  //
  c[1] = state_bubble_T.p;
  c[2] = state_bubble_T.d;
  c[3] = 5.072313e-1 / 1000;
  c[4] = 1.305531e2 * 1000 * 0.018;
  c[5] = -8.492403e1 * 1000 * 0.018;
  c[6] = 4.128962e-3 / 1000;

  c_pdT[1] = sat_T_pdT.psat;
  c_pdT[2] = Medium.bubbleDensity(sat=sat_T_pdT);
  c_pdT[3] = 5.072313e-1 / 1000;
  c_pdT[4] = 1.305531e2 * 1000 * 0.018;
  c_pdT[5] = -8.492403e1 * 1000 * 0.018;
  c_pdT[6] = 4.128962e-3 / 1000;

  c_mdT[1] = sat_T_mdT.psat;
  c_mdT[2] = Medium.bubbleDensity(sat=sat_T_mdT);
  c_mdT[3] = 5.072313e-1 / 1000;
  c_mdT[4] = 1.305531e2 * 1000 * 0.018;
  c_mdT[5] = -8.492403e1 * 1000 * 0.018;
  c_mdT[6] = 4.128962e-3 / 1000;

  //
  // Calculate partial derivatives of coefficients of the isotherm model w.r.t.
  // temperature
  //
  dc_dT[1] = 1/Medium.saturationTemperature_derp_sat(sat=sat_T);
  dc_dT[2] = Medium.dBubbleDensity_dPressure(sat=sat_T) * dc_dT[1];
  dc_dT[3] = 0;
  dc_dT[4] = 0;
  dc_dT[5] = 0;
  dc_dT[6] = 0;

  dc_dT_pdT[1] = 1/Medium.saturationTemperature_derp_sat(sat=sat_T_pdT);
  dc_dT_pdT[2] = Medium.dBubbleDensity_dPressure(sat=sat_T_pdT) * dc_dT_pdT[1];
  dc_dT_pdT[3] = 0;
  dc_dT_pdT[4] = 0;
  dc_dT_pdT[5] = 0;
  dc_dT_pdT[6] = 0;

  dc_dT_mdT[1] = 1/Medium.saturationTemperature_derp_sat(sat=sat_T_mdT);
  dc_dT_mdT[2] = Medium.dBubbleDensity_dPressure(sat=sat_T_mdT) * dc_dT_mdT[1];
  dc_dT_mdT[3] = 0;
  dc_dT_mdT[4] = 0;
  dc_dT_mdT[5] = 0;
  dc_dT_mdT[6] = 0;

  //
  // Calculate second-order partial derivatives of coefficients of the isotherm
  // model w.r.t. temperature
  //
  ddc_dT_dT[1] = (dc_dT_pdT[1] - dc_dT_mdT[1]) / (2*dT);
  ddc_dT_dT[2] = (dc_dT_pdT[2] - dc_dT_mdT[2]) / (2*dT);
  ddc_dT_dT[3] = 0;
  ddc_dT_dT[4] = 0;
  ddc_dT_dT[5] = 0;
  ddc_dT_dT[6] = 0;

  //
  // Calculation of state properties
  //
  state_adsorptive_pT_pdp = Medium.setState_pTX(
    p=p_adsorpt+dp,
    T=T_adsorpt)
    "State properties of adsorptive at p_adsorpt and T_adsorpt: p + dp";
  state_adsorptive_pT_mdp = Medium.setState_pTX(
    p=p_adsorpt-dp,
    T=T_adsorpt)
    "State properties of adsorptive at p_adsorpt and T_adsorpt: p - dp";

  state_adsorptive_pT_pdT = Medium.setState_pTX(
    p=p_adsorpt,
    T=T_adsorpt+dT)
    "State properties of adsorptive at p_adsorpt and T_adsorpt: T + dT";
  state_adsorptive_pT_mdT = Medium.setState_pTX(
    p=p_adsorpt,
    T=T_adsorpt-dT)
    "State properties of adsorptive at p_adsorpt and T_adsorpt: T - dT";

  state_bubble_T_pdT = Medium.setBubbleState(sat=sat_T_pdT)
    "State properties of bubble point at T_adsorpt: T + dT";
  state_bubble_T_mdT = Medium.setBubbleState(sat=sat_T_mdT)
    "State properties of bubble point at T_adsorpt: T - dT";

  //
  // Calculate properties
  //
  x_adsorpt = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.x_pT(
    p_adsorpt=p_adsorpt,
    T_adsorpt=T_adsorpt,
    c=c,
    p_adsorpt_lb_start=1,
    p_adsorpt_ub_start=10,
    tolerance=100*Modelica.Constants.eps)
    "Uptake";

  dx_adsorpt_dp = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.dx_dp(
    p_adsorpt=p_adsorpt,
    T_adsorpt=T_adsorpt,
    c=c)
    "Partial derivative of uptake w.r.t. pressure at constant temperature";
  dx_adsorpt_dT = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.dx_dT(
    p_adsorpt=p_adsorpt,
    T_adsorpt=T_adsorpt,
    c=c,
    dc_dT_adsorpt=dc_dT)
    "Partial derivative of uptake w.r.t. temperature at constant pressure";

  ddx_adsorpt_dp_dp = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.ddx_dp_dp(
    p_adsorpt=p_adsorpt,
    T_adsorpt=T_adsorpt,
    c=c)
    "Second-order partial derivative of uptake w.r.t. pressure at constant temperature";
  ddx_adsorpt_dT_dT = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.ddx_dT_dT(
    p_adsorpt=p_adsorpt,
    T_adsorpt=T_adsorpt,
    c=c,
    dc_dT_adsorpt=dc_dT,
    ddc_dT_adsorpt_dT_adsorpt=ddc_dT_dT)
    "Second-order partial derivative of uptake w.r.t. temperature at constant pressure";
  ddx_adsorpt_dp_dT = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.ddx_dp_dT(
    p_adsorpt=p_adsorpt,
    T_adsorpt=T_adsorpt,
    c=c,
    dc_dT_adsorpt=dc_dT)
    "Second-order partial derivative of uptake w.r.t. pressure and temperature";

  h_adsorptiveToLiquid = state_adsorptive_pT.h - state_bubble_T.h
    "Specific enthalpy difference between adsorptive state and saturated liquid
    state (i.e., bubble point)";
  dh_adsorptiveToLiquid_dp = 1/state_adsorptive_pT.d * (1 - T_adsorpt *
    Medium.isobaricExpansionCoefficient(state=state_adsorptive_pT)) - 0
    "Partial derivative of specific enthalpy difference between adsorptive state 
    and saturated liquid state (i.e., bubble point) w.r.t. pressure at constant 
    temperature";
  dh_adsorptiveToLiquid_dT =
    Medium.specificHeatCapacityCp(state=state_adsorptive_pT) -
    Medium.specificHeatCapacityCp(state=state_bubble_T)
    "Partial derivative of specific enthalpy difference between adsorptive state 
    and saturated liquid state (i.e., bubble point) w.r.t. temperature at constant
    pressure";

  beta_adsorpt = Medium.isobaricExpansionCoefficient(state=state_bubble_T)
    "Isobaric expansion coefficient of the adsorpt phase";
  dbeta_adsorpt_dT =
    (Medium.isobaricExpansionCoefficient(state=state_bubble_T_pdT) -
     Medium.isobaricExpansionCoefficient(state=state_bubble_T_mdT)) /(2*dT)
    "Partial derivative of the ssobaric expansion coefficient of the adsorpt phase
    w.r.t. temperature at constant pressure";

  A =SorpLib.Media.Functions.SorptionEquilibria.Utilities.A_ppsT(
    p_adsorpt=p_adsorpt,
    p_sat=c[1],
    T_adsorpt=T_adsorpt)
    "Molar adsorption potential";
  dA_dp =SorpLib.Media.Functions.SorptionEquilibria.Utilities.dA_dp(
    p_adsorpt=p_adsorpt,
    p_sat=c[1],
    T_adsorpt=T_adsorpt)
    "Partial derivative of molar adsorption potential w.r.t. pressure at
    constant temperature";
  dA_dT =SorpLib.Media.Functions.SorptionEquilibria.Utilities.dA_dT(
    p_adsorpt=p_adsorpt,
    p_sat=c[1],
    T_adsorpt=T_adsorpt,
    dp_sat_dT_adsorpt=dc_dT[1])
    "Partial derivative of molar adsorption potential w.r.t. temperature at
    constant pressure";

  dW_dA = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.dW_dA(
    A=A,
    c=c)
    "Partial derivative of filled pore volume w.r.t. molar adsorption potential
    at constant pressure and temperature";
  ddW_dA_dA = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.ddW_dA_dA(
    A=A,
    c=c)
    "Second-order prtial derivative of filled pore volume w.r.t. molar adsorption potential
    at constant pressure and temperature";
  ddW_dA_dT = SorpLib.Media.Functions.SorptionEquilibria.PureComponents.DubininLorentzianCumulative.ddW_dA_dT(
    p_adsorpt=p_adsorpt,
    T_adsorpt=T_adsorpt,
    A=A,
    c=c,
    dc_dT_adsorpt=dc_dT)
    "Partial derivative of filled pore volume w.r.t. molar adsorption potential
    and temperature at constant pressure";

  //
  // Calculate molar adsorption enthalpies and their derivatives
  //
  h_ads = SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.h_ads(
    M_adsorptive=M_adsorptive,
    T_adsorpt=T_adsorpt,
    v_adsorptive=1/state_adsorptive_pT.d,
    v_adsorpt=1/state_bubble_T.d,
    dx_adsorpt_dp=dx_adsorpt_dp,
    dx_adsorpt_dT=dx_adsorpt_dT)
    "Molar adsorption enthalpy";
  h_ads_CC = SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.h_ads_clausiusClapeyron(
    p_adsorpt=p_adsorpt,
    T_adsorpt=T_adsorpt,
    dx_adsorpt_dp=dx_adsorpt_dp,
    dx_adsorpt_dT=dx_adsorpt_dT)
    "Molar adsorption enthalpy according to Clausius Clapeyron";
  h_ads_Dubinin = SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.h_ads_Dubinin(
    M_adsorptive=M_adsorptive,
    T_adsorpt=T_adsorpt,
    x_adsorpt=x_adsorpt,
    h_adsorptiveToLiquid=h_adsorptiveToLiquid,
    v_adsorpt=1/state_bubble_T.d,
    beta_adsorpt=beta_adsorpt,
    A=A,
    dW_dA=dW_dA)
    "Molar adsorption enthalpy according to Dubinin model";

  dh_ads_dp_T = SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.dh_ads_dp(
    M_adsorptive=M_adsorptive,
    T_adsorpt=T_adsorpt,
    v_adsorptive=1/state_adsorptive_pT.d,
    v_adsorpt=1/state_bubble_T.d,
    dv_adsorptive_dp=(1/state_adsorptive_pT_pdp.d - 1/state_adsorptive_pT_mdp.d) /
      (2*dp),
    dv_adsorpt_dp=0,
    dx_adsorpt_dp=dx_adsorpt_dp,
    dx_adsorpt_dT=dx_adsorpt_dT,
    ddx_adsorpt_dp_dp=ddx_adsorpt_dp_dp,
    ddx_adsorpt_dp_dT=ddx_adsorpt_dp_dT)
    "Partial derivative of molar adsorption enthalpy w.r.t. pressure at constant
    temperature";
  dh_ads_dp_T_CC = SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.dh_ads_dp_clausiusClapeyron(
    p_adsorpt=p_adsorpt,
    T_adsorpt=T_adsorpt,
    dx_adsorpt_dp=dx_adsorpt_dp,
    dx_adsorpt_dT=dx_adsorpt_dT,
    ddx_adsorpt_dp_dp=ddx_adsorpt_dp_dp,
    ddx_adsorpt_dp_dT=ddx_adsorpt_dp_dT)
    "Partial derivative of molar adsorption enthalpy w.r.t. pressure at constant
    temperature according to Clausius Clapeyron";
  dh_ads_dp_T_Dubinin = SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.dh_ads_dp_Dubinin(
    M_adsorptive=M_adsorptive,
    T_adsorpt=T_adsorpt,
    x_adsorpt=x_adsorpt,
    dx_adsorpt_dp=dx_adsorpt_dp,
    h_adsorptiveToLiquid=h_adsorptiveToLiquid,
    dh_adsorptiveToLiquid_dp=dh_adsorptiveToLiquid_dp,
    v_adsorpt=1/state_bubble_T.d,
    dv_adsorpt_dp=0,
    beta_adsorpt=beta_adsorpt,
    dbeta_adsorpt_dp=0,
    dA_dp=dA_dp,
    dW_dA=dW_dA,
    ddW_dA_dA=ddW_dA_dA)
    "Partial derivative of molar adsorption enthalpy w.r.t. pressure at constant
    temperature according to Dubinin model";

  dh_ads_dT_p =  SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.dh_ads_dT(
    M_adsorptive=M_adsorptive,
    T_adsorpt=T_adsorpt,
    v_adsorptive=1/state_adsorptive_pT.d,
    v_adsorpt=1/state_bubble_T.d,
    dv_adsorptive_dT=(1/state_adsorptive_pT_pdT.d - 1/state_adsorptive_pT_mdT.d) /
      (2*dT),
    dv_adsorpt_dT=-1/state_bubble_T.d^2 * dc_dT[2],
    dx_adsorpt_dp=dx_adsorpt_dp,
    dx_adsorpt_dT=dx_adsorpt_dT,
    ddx_adsorpt_dT_dT=ddx_adsorpt_dT_dT,
    ddx_adsorpt_dp_dT=ddx_adsorpt_dp_dT)
    "Partial derivative of molar adsorption enthalpy w.r.t. temperature at constant
    pressure";
  dh_ads_dT_p_CC = SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.dh_ads_dT_clausiusClapeyron(
    p_adsorpt=p_adsorpt,
    T_adsorpt=T_adsorpt,
    dx_adsorpt_dp=dx_adsorpt_dp,
    dx_adsorpt_dT=dx_adsorpt_dT,
    ddx_adsorpt_dp_dT=ddx_adsorpt_dp_dT,
    ddx_adsorpt_dT_dT=ddx_adsorpt_dT_dT)
    "Partial derivative of molar adsorption enthalpy w.r.t. temperature at constant
    pressure according to Clausius Clapeyron";
  dh_ads_dT_p_Dubinin = SorpLib.Media.Functions.SorptionEnthalpies.PureComponents.dh_ads_dT_Dubinin(
    M_adsorptive=M_adsorptive,
    T_adsorpt=T_adsorpt,
    x_adsorpt=x_adsorpt,
    dx_adsorpt_dT=dx_adsorpt_dT,
    h_adsorptiveToLiquid=h_adsorptiveToLiquid,
    dh_adsorptiveToLiquid_dT=dh_adsorptiveToLiquid_dT,
    v_adsorpt=1/state_bubble_T.d,
    dv_adsorpt_dT=-1/state_bubble_T.d^2 * dc_dT[2],
    beta_adsorpt=beta_adsorpt,
    dbeta_adsorpt_dT=dbeta_adsorpt_dT,
    dA_dT=dA_dT,
    dW_dA=dW_dA,
    ddW_dA_dT=ddW_dA_dT)
    "Partial derivative of molar adsorption enthalpy w.r.t. temperature at constant
    pressure according to Dubinin model";

  dh_ads_dT_x = dh_ads_dT_p - dh_ads_dp_T *
    dx_adsorpt_dT / dx_adsorpt_dp
    "Partial derivative of molar adsorption enthalpy w.r.t. temperature at constant
    uptake";
  dh_ads_dT_x_CC = dh_ads_dT_p_CC - dh_ads_dp_T_CC *
    dx_adsorpt_dT / dx_adsorpt_dp
    "Partial derivative of molar adsorption enthalpy w.r.t. temperature at constant
    uptake according to Clausius Clapeyron";
  dh_ads_dT_x_Dubinin = dh_ads_dT_p_Dubinin - dh_ads_dp_T_Dubinin *
    dx_adsorpt_dT / dx_adsorpt_dp
    "Partial derivative of molar adsorption enthalpy w.r.t. temperature at constant
    uptake according to Dubinin model";

  //
  // Calculate specific heat capacities of adsorpt phase
  //
  cp_adsorpt = SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.cp_ChakrabortyEtAl(
    M_adsorptive=M_adsorptive,
    T_adsorpt=T_adsorpt,
    cp_adsorptive=cp_adsorptive,
    v_adsorptive=1/state_adsorptive_pT.d,
    dv_adsorptive_dT=(1/state_adsorptive_pT_pdT.d - 1/state_adsorptive_pT_mdT.d) /
      (2*dT),
    v_adsorpt=1/state_bubble_T.d,
    h_ads=h_ads,
    dh_ads_dT_x=dh_ads_dT_x)
    "Specific heat capacitiy of adsorpt";
  cp_adsorpt_CC = SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.cp_ChakrabortyEtAl(
    M_adsorptive=M_adsorptive,
    T_adsorpt=T_adsorpt,
    cp_adsorptive=cp_adsorptive,
    v_adsorptive=1/state_adsorptive_pT.d,
    dv_adsorptive_dT=(1/state_adsorptive_pT_pdT.d - 1/state_adsorptive_pT_mdT.d) /
      (2*dT),
    v_adsorpt=1/state_bubble_T.d,
    h_ads=h_ads_CC,
    dh_ads_dT_x=dh_ads_dT_x_CC)
    "Specific heat capacitiy of adsorpt calculated using molar adsorption enthalpy
    according to Clausius Clapeyron";
  cp_adsorpt_Dubinin = SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.cp_ChakrabortyEtAl(
    M_adsorptive=M_adsorptive,
    T_adsorpt=T_adsorpt,
    cp_adsorptive=cp_adsorptive,
    v_adsorptive=1/state_adsorptive_pT.d,
    dv_adsorptive_dT=(1/state_adsorptive_pT_pdT.d - 1/state_adsorptive_pT_mdT.d) /
      (2*dT),
    v_adsorpt=1/state_bubble_T.d,
    h_ads=h_ads_Dubinin,
    dh_ads_dT_x=dh_ads_dT_x_Dubinin)
    "Specific heat capacitiy of adsorptive using molar adsorption enthalpy
    according to Dubinin model";

  //
  // Annotations
  //
  annotation (experiment(
      StopTime=20,
      Tolerance=1e-06),
Documentation(revisions="<html>
<ul>
  <li>
  November 20, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>", info="<html>
<p>
This tester shows the behavior of the 'cp_ChakrabortyEtAl' function.
<br/><br/>
To see the function behavior, plot the variables <i>cp_i</i> over the time. The 
simulation time is correctly preset (Start: 0 s, Stop = 20 s). 
</p>
</html>"));
end Test_cp_ChakrabortyEtAl;
