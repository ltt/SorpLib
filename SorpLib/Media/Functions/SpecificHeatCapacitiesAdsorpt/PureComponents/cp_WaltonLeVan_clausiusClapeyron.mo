﻿within SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents;
function cp_WaltonLeVan_clausiusClapeyron
  "Specific heat capacity of adsorpt phase according to Walton and LeVan (2005) using the molar adsorption enthalpy accourding to Clausius Clapeyon"

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.SpecificHeatCapacity cp_adsorptive
    "Specific heat capacity of adsorptive"
    annotation (Dialog(tab="General", group="Inputs"));
  extends
    SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.BasesClasses.PartialPureIntegrad_cp;

  input SorpLib.Units.Uptake x_adsorpt_lb = 0
    "Lower limit for integral (i.e., should be zero)"
    annotation (Dialog(tab="General", group="Inputs - Numerics"));
  input Real tolerance = 100*Modelica.Constants.eps
    "Tolerance for solution of integral calculated numerically"
    annotation (Dialog(tab="General", group="Inputs - Numerics"));

  //
  // Definition of variables
  //
protected
  SorpLib.Units.IntegralMolarHeatCapacityByUptake int_dh_ads_dT_x
    "Partial derivative of molar adsorption enthalpy w.r.t. temperature at constant
    uptake integrated from x_adsorpt_lb to x_adsorpt";

algorithm
  //
  // Calculate integral
  //
  int_dh_ads_dT_x :=Modelica.Math.Nonlinear.quadratureLobatto(
    f=function
      SorpLib.Media.Functions.SpecificHeatCapacitiesAdsorpt.PureComponents.Integrands.dh_ads_dT_x_CC(
      T_adsorpt=T_adsorpt,
      M_adsorptive=M_adsorptive,
      c=c,
      dc_dT=dc_dT,
      ddc_dT_dT=ddc_dT_dT,
      func_p_xT=function func_p_xT(),
      func_dx_dp=function func_dx_dp(),
      func_dx_dT=function func_dx_dT(),
      func_ddx_dp_dp=function func_ddx_dp_dp(),
      func_ddx_dT_dT=function func_ddx_dT_dT(),
      func_ddx_dp_dT=function func_ddx_dp_dT()),
    a=x_adsorpt_lb,
    b=x_adsorpt,
    tolerance=tolerance)
  "Partial derivative of molar adsorption enthalpy w.r.t. temperature at constant
  uptake integrated from x_adsorpt_lb to x_adsorpt";

  //
  // Calculate final result
  //
  cp_adsorpt := cp_adsorptive - 1 / (x_adsorpt-x_adsorpt_lb) * int_dh_ads_dT_x /
    M_adsorptive
    "Specific heat capacitiy of adsorpt";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function calculates the specific heat capacity of the adsorpt phase for a
pure component according to Walton and LeVan (2005). The molar sorption enthalpy
is calculated according to the Clausius Clapeyron assumptions.
</p>

<h4>Main equations</h4>
<p>
The specific heat capacity is defined as follows:
</p>
<pre>
    c<sub>x,adsorpt</sub> = c<sub>p,adsorptive</sub> - 1 / (M<sub>adsorptive</sub> * (x<sub>adsorpt</sub> - x<sub>adsorpt,lb</sub>)) * &int; [(&part;h<sub>ads</sub>/&part;T)<sub>x</sub>] * dx
</pre>
<p>
Herein, <i>x<sub>adsorpt</sub></i> is the equilibrium uptake, <i>x<sub>adsorpt,lb</sub></i> 
is the lower bound of the equilibrium uptake, <i>(&part;h<sub>ads</sub>/&part;T)<sub>x</sub></i> 
is the partial derivative of the molar enthalpy w.r.t. temperature at constant uptake, and 
<i>M<sub>adsorptive</sub></i> is the molar mass of the adsorptive.
</p>

<h4>References</h4>
<ul>
  <li>
  Walton, K.S. and LeVan, M.D. (2005). Adsorbed-Phase Heat Capacities: Thermodynamically Consistent Values Determined from Temperature-Dependent Equilibrium Models, Industrial & Engineering Chemistry Researc, 44(1):178-182. DOI: https://doi.org/10.1021/ie049394j.
  </li>
  <li>
  Schwamberger, V. and Schmidt, F.P. (2013). Estimating the Heat Capacity of the Adsorbate−Adsorbent System from Adsorption Equilibria Regarding Thermodynamic Consistency, Industrial & Engineering Chemistry Research, 52:16958−16965. DOI: https://doi.org/10.1021/ie4011832.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  November 20, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end cp_WaltonLeVan_clausiusClapeyron;
