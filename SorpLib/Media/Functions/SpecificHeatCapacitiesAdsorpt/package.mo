within SorpLib.Media.Functions;
package SpecificHeatCapacitiesAdsorpt "Functions required to calculate specific heat capacities of adsorpt phases"
  extends Modelica.Icons.VariantsPackage;

  annotation (Documentation(info="<html>
<p>
This package contains models to calculate specific heat capacities of adsorpt
phases for pure component and multi-component adsorption. Please check the 
documentation of the individual packages for more details on the implemented 
specific heat capacity models.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end SpecificHeatCapacitiesAdsorpt;
