within SorpLib.Media.Functions.Utilities;
function intGeneralizedFunction_dT
  "Calculates indefinite integral of an arbitrary media property z via a generalized function w.r.t. temperature"
  extends SorpLib.Media.Functions.Utilities.BasesClasses.Partial_int_z_dT;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.Temperature T_ref
    "Reference temperature"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real z_ref
    "Reference fluid property data"
    annotation (Dialog(tab="General", group="Inputs"));

  input Real coefficients[:]
    "Coefficients of generalized function"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real exponents[size(coefficients,1)]
    "Exponents of generalized function"
    annotation (Dialog(tab="General", group="Inputs"));

  input Real tolerance = 100*Modelica.Constants.eps
    "Tolerance if numerical integration is required"
    annotation (Dialog(tab="General", group="Inputs"));

  input SorpLib.Choices.GeneralizedFunctionApproach approach=SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionTemperature
    "Function approach" annotation (Dialog(tab="General", group="Inputs"));

algorithm
  //
  // Check approach
  //
  int_z_dT := 0
    "Indefinite integral of fluid property data w.r.t. temperature without integration
    constant";

  if approach == SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionTemperature then
    //
    // Analytical solution exists
    //
    for ind in 1:size(coefficients,1) loop
      if not Modelica.Math.isEqual(s1=exponents[ind], s2=-1, eps=100*Modelica.Constants.eps) then
        int_z_dT := int_z_dT + coefficients[ind] * T ^ (exponents[ind] + 1) /
          (exponents[ind] + 1)
          "Indefinite integral of fluid property data w.r.t. temperature without integration
          constant";
      else
        int_z_dT := int_z_dT + coefficients[ind] * log(abs(T))
          "Indefinite integral of fluid property data w.r.t. temperature without integration
          constant";
      end if;
    end for;

    int_z_dT := z_ref * int_z_dT
      "Indefinite integral of fluid property data w.r.t. temperature without integration
      constant";

  elseif approach == SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionReducedTemperature then
    //
    // Analytical solution exists
    //
    for ind in 1:size(coefficients,1) loop
      if not Modelica.Math.isEqual(s1=exponents[ind], s2=-1, eps=100*Modelica.Constants.eps) then
        int_z_dT := int_z_dT - coefficients[ind] * T_ref * (1 - T / T_ref) ^
          (exponents[ind] + 1) / (exponents[ind] + 1)
          "Indefinite integral of fluid property data w.r.t. temperature without integration
          constant";
      else
        int_z_dT := int_z_dT - coefficients[ind] * T_ref * log(abs(1 - T / T_ref))
          "Indefinite integral of fluid property data w.r.t. temperature without integration
          constant";
      end if;
    end for;

    int_z_dT := z_ref * int_z_dT
      "Indefinite integral of fluid property data w.r.t. temperature without integration
      constant";

  else
    //
    // Analytical solution does not exist for 'ExponentialFunction_Temperature'
    // and 'ExponentialFunction_ReducedTemperature'
    //
    assert(false,
      "An analytical integral does not exist for the approach: " + String(approach) + ". Only a specific integral can be determined numerically.",
      level = AssertionLevel.warning);

  end if;

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function is the indefinite integral of the function 'generalizedFunction_T' 
with respect to the temperature. The integral can only be solved analytically for
the approaches 'PolynomialFunction_Temperature' and 'PolynomialFunction_ReducedTemperature.' 
Note that the integration constant is neglected. For full details of the original 
function 'generalizedFunction_T,' check the documentation of the function 
<a href=\"Modelica://SorpLib.Media.Functions.Utilities.generalizedFunction_T\">SorpLib.Media.Functions.Utilities.generalizedFunction_T</a>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 15, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end intGeneralizedFunction_dT;
