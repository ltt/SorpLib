within SorpLib.Media.Functions.Utilities;
function dgeneralizedFunction_dT
  "Calculates partial derivative of an arbitrary media property z via a generalized function w.r.t. temperature"
  extends SorpLib.Media.Functions.Utilities.BasesClasses.Partial_dz_dT;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.Temperature T_ref
    "Reference temperature"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real z_ref
    "Reference fluid property data"
    annotation (Dialog(tab="General", group="Inputs"));

  input Real coefficients[:]
    "Coefficients of generalized function"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real exponents[size(coefficients,1)]
    "Exponents of generalized function"
    annotation (Dialog(tab="General", group="Inputs"));

  input SorpLib.Choices.GeneralizedFunctionApproach approach=SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionTemperature
    "Function approach" annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of variables
  //
protected
  Real z
    "Fluid property data";

algorithm
  //
  // Check approach
  //
  z := 0
    "Fluid property data";
  dz_dT :=0
    "Partial derivative of fluid property data w.r.t. temperature";

  if approach == SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionTemperature then
    for ind in 1:size(coefficients,1) loop
      if not Modelica.Math.isEqual(s1=exponents[ind], s2=0, eps=100*Modelica.Constants.eps) then
        dz_dT := dz_dT + (exponents[ind] * coefficients[ind] *
          T ^ (exponents[ind] - 1))
          "Partial derivative of fluid property data w.r.t. temperature";
      end if;
    end for;

    dz_dT := z_ref * dz_dT
      "Partial derivative of fluid property data w.r.t. temperature";

  elseif approach == SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionReducedTemperature then
    for ind in 1:size(coefficients,1) loop
      if not Modelica.Math.isEqual(s1=exponents[ind], s2=0, eps=100*Modelica.Constants.eps) then
        dz_dT := dz_dT + (exponents[ind] * coefficients[ind] * (1 - T / T_ref) ^
          (exponents[ind] - 1))
          "Partial derivative of fluid property data w.r.t. temperature";
      end if;
    end for;

    dz_dT := z_ref * (-1 / T_ref) * dz_dT
      "Partial derivative of fluid property data w.r.t. temperature";

  elseif approach == SorpLib.Choices.GeneralizedFunctionApproach.ExponentialFunctionTemperature then
    for ind in 1:size(coefficients,1) loop
      z := z + coefficients[ind] * T ^ exponents[ind]
        "Fluid property data";

      if not Modelica.Math.isEqual(s1=exponents[ind], s2=0, eps=100*Modelica.Constants.eps) then
        dz_dT := dz_dT + (exponents[ind] * coefficients[ind] *
          T ^ (exponents[ind] - 1))
          "Partial derivative of fluid property data w.r.t. temperature";
      end if;
    end for;

    dz_dT := z_ref * exp(z) * dz_dT
      "Partial derivative of fluid property data w.r.t. temperature";

  else
    for ind in 1:size(coefficients,1) loop
      z := z + coefficients[ind] * (1 - T/T_ref) ^ exponents[ind]
        "Fluid property data";

      if not Modelica.Math.isEqual(s1=exponents[ind], s2=0, eps=100*Modelica.Constants.eps) then
        dz_dT := dz_dT + (exponents[ind] * coefficients[ind] * (1 - T / T_ref) ^
          (exponents[ind] - 1))
          "Partial derivative of fluid property data w.r.t. temperature";
      end if;
    end for;

    dz_dT := z_ref * exp(z) * (-1 / T_ref) * dz_dT
      "Partial derivative of fluid property data w.r.t. temperature";

  end if;

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function is the partial derivative of the function 'generalizedFunction_T' 
with respect to the temperature. For full details of the original function
'generalizedFunction_T,' check the documentation of the function 
<a href=\"Modelica://SorpLib.Media.Functions.Utilities.generalizedFunction_T\">SorpLib.Media.Functions.Utilities.generalizedFunction_T</a>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 15, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end dgeneralizedFunction_dT;
