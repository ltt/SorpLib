within SorpLib.Media.Functions.Utilities;
function linearInterpolation_T
  "Interpolates an arbitrary fluid property z linearly"
  extends SorpLib.Media.Functions.Utilities.BasesClasses.Partial_z_T;

  //
  // Definition of inputs
  //
  input Real abscissa[:]
    "Known abscissa values"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real ordinate[size(abscissa,1)]
    "Known ordinate values"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of variables
  //
protected
  Integer ind_gv
    "Index of abscissa that is greater than actual temperature";

algorithm
  if T <= abscissa[1] then
    z := ordinate[1]
      "Fluid property data";

  elseif T >= abscissa[end] then
    z := ordinate[end]
      "Fluid property data";

  else
    //
    // Finde abscissa that is greater than actual temperature
    //
    ind_gv :=1
      "Index of abscissa that is greater than actual temperature";

    while T > abscissa[ind_gv] loop
      ind_gv :=ind_gv + 1
        "Index of abscissa that is greater than actual temperature";
    end while;

    z := ordinate[ind_gv-1] +
      (ordinate[ind_gv] - ordinate[ind_gv-1]) /
      (abscissa[ind_gv] - abscissa[ind_gv-1]) *
      (T - abscissa[ind_gv-1])
      "Fluid property data";

  end if;

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function calculates an arbitrary fluid property <i>z</i> via piecewise linear 
interpolation as a function of temperature as well as known abscissa and ordinate 
values. Examples for the arbitrary fluid property <i>z</i> are the specific heat 
capacity <i>c</i> or the specific volume <i>v</i>.
</p>

<h4>Main equations</h4>
<p>
For a given temperature <i>T</i>, the arbitrary fluid property <i>z</i> is calculated
using abscissa <i>z<sub>known</sub></i> and its corresponding ordinate <i>T<sub>known</sub></i>, 
values that are closest to the given temperature <i>T</i>:
</p>
<pre>
    z = z<sub>known,smaller</sub> + (z<sub>known,greater</sub> - z<sub>known,smaller</sub>) / (T<sub>known,greater</sub> - T<sub>known,smaller</sub>) * (T - T<sub>known,smaller</sub>);
</pre>

<h4>Assumptions and limitations</h4>
<ul>
  <li>
  For temperatures outside the abscissa values, the ordinate values of the smallest or 
  largest abscissa value are returned.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  November 15, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end linearInterpolation_T;
