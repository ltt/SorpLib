within SorpLib.Media.Functions.Utilities;
function generalizedFunction_T
  "Calculates an arbitrary fluid property z via a generalized function"
  extends SorpLib.Media.Functions.Utilities.BasesClasses.Partial_z_T;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.Temperature T_ref
    "Reference temperature"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real z_ref
    "Reference fluid property data"
    annotation (Dialog(tab="General", group="Inputs"));

  input Real coefficients[:]
    "Coefficients of generalized function"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real exponents[size(coefficients,1)]
    "Exponents of generalized function"
    annotation (Dialog(tab="General", group="Inputs"));

  input SorpLib.Choices.GeneralizedFunctionApproach approach=SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionTemperature
    "Function approach" annotation (Dialog(tab="General", group="Inputs"));

algorithm
  //
  // Check approach
  //
  z := 0
    "Fluid property data";

  if approach == SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionTemperature then
    if not Modelica.Math.isEqual(s1=T, s2=0, eps=100*Modelica.Constants.eps) then
      for ind in 1:size(coefficients,1) loop
        z := z + coefficients[ind] * T ^ exponents[ind]
          "Fluid property data";
      end for;
    end if;

    z := z_ref * z
      "Fluid property data";

  elseif approach == SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionReducedTemperature then
    if not Modelica.Math.isEqual(s1=T, s2=T_ref, eps=100*Modelica.Constants.eps) then
      for ind in 1:size(coefficients,1) loop
        z := z + coefficients[ind] * (1 - T/T_ref) ^ exponents[ind]
          "Fluid property data";
      end for;
    end if;

    z := z_ref * z
      "Fluid property data";


  elseif approach == SorpLib.Choices.GeneralizedFunctionApproach.ExponentialFunctionTemperature then
    if not Modelica.Math.isEqual(s1=T, s2=0, eps=100*Modelica.Constants.eps) then
      for ind in 1:size(coefficients,1) loop
        z := z + coefficients[ind] * T ^ exponents[ind]
          "Fluid property data";
      end for;
    end if;

    z := z_ref * exp(z)
      "Fluid property data";

  else
    if not Modelica.Math.isEqual(s1=T, s2=T_ref, eps=100*Modelica.Constants.eps) then
      for ind in 1:size(coefficients,1) loop
        z := z + coefficients[ind] * (1 - T/T_ref) ^ exponents[ind]
          "Fluid property data";
      end for;
    end if;

    z := z_ref * exp(z)
      "Fluid property data";

  end if;

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function calculates an arbitrary fluid property <i>z</i> via a generalized function 
as a function of temperature. Examples of the arbitrary fluid property <i>z</i> are
the specific heat capacity <i>c</i> or the specific volume <i>v</i>.
</p>

<h4>Main equations</h4>
<p>
The generalized function comprises four function types often used for fluid
property data calculation. The function type depends on the input <i>approach</i>:
</p>
<ol>
  <li>
  For <i>approach = PolynomialFunction_Temperature</i>:
  <br>
  <pre>z = z<sub>ref</sub> * &sum;<sub>i</sub> a<sub>i</sub> * T ^ (b<sub>i</sub>);</pre>
  <br>
  </li>
  <li>
  For <i>approach = PolynomialFunction_ReducedTemperature</i>:
  <br>
  <pre>z = z<sub>ref</sub> * &sum;<sub>i</sub> a<sub>i</sub> * (1 - T / T<sub>ref</sub>) ^ (b<sub>i</sub>);</pre>
  <br>
  </li>
  <li>
  For <i>approach = ExponentialFunction_Temperature</i>:
  <br>
  <pre>z = z<sub>ref</sub> * <strong>exp</strong>(&sum;<sub>i</sub> a<sub>i</sub> * T ^ (b<sub>i</sub>));</pre>
  <br>
  </li>
  <li>
  For <i>approach = ExponentialFunction_ReducedTemperature</i>:
  <br>
  <pre>z = z<sub>ref</sub> * <strong>exp</strong>(&sum;<sub>i</sub> a<sub>i</sub> * (1 - T / T<sub>ref</sub>) ^ (b<sub>i</sub>));</pre>
  <br>
  </li>
</ol>
<p>
Herein, <i>z<sub>ref</sub></i> is either a pre-factor for approaches that use the 
temperature or the fluid property at reference temperature <i>T<sub>ref</sub></i> 
for approaches that use the reduced temperature. The vectors <i>a</i> and <i>b</i>
contain the coefficients and exponents for each summand of the sum.
</p>

<h4>References</h4>
<ul>
  <li>
  Wagner, W. and Pru&szlig;, A (2002). The IAPWS Formulation 1995 for the Thermodynamic Properties of Ordinary Water Substance for General and Scientific Use, Journal of Physical and Chemical Reference Data, 31:387. DOI: https://doi.org/10.1063/1.1461829.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  November 15, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end generalizedFunction_T;
