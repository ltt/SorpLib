within SorpLib.Media.Functions.Utilities;
function cubicSplineInterpolation_T
  "Interpolates an arbitrary fluid property z via cubic splines"
  extends SorpLib.Media.Functions.Utilities.BasesClasses.Partial_z_T;

  //
  // Definition of inputs
  //
  input Real abscissa[:]
    "Known abscissa values"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real ordinate[size(abscissa,1)]
    "Known ordinate values"
    annotation (Dialog(tab="General", group="Inputs"));

  input Real coefficients[size(abscissa,1),4]
    "Coefficient a to d for cubic polynomials"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of variables
  //
protected
  Integer ind_gv
    "Index of abscissa that is greater than actual temperature";

algorithm
  if T <= abscissa[1] then
    z := ordinate[1]
      "Fluid property data";

  elseif T >= abscissa[end] then
    z := ordinate[end]
      "Fluid property data";

  else
    ind_gv :=1
      "Index of abscissa that is greater than actual temperature";

    while T > abscissa[ind_gv+1] loop
      ind_gv :=ind_gv + 1
        "Index of abscissa that is greater than actual temperature";
    end while;

    z := coefficients[ind_gv,1] +
      coefficients[ind_gv,2] * (T - abscissa[ind_gv]) +
      coefficients[ind_gv,3] * (T - abscissa[ind_gv])^2 +
      coefficients[ind_gv,4] * (T - abscissa[ind_gv])^3
      "Fluid property data";
  end if;

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function calculates an arbitrary fluid property z via piecewise cubic spline 
interpolation as a function of temperature as well as known abscissa and ordinate 
value. Examples of the arbitrary fluid property <i>z</i> are the specific
heat capacity <i>c</i> or the specific volume <i>v</i>.
<br><br>
The coefficients of the cubic splines must be calculated via the function
<a href=\"Modelica://SorpLib.Media.Functions.Utilities.calcCubicSplineCoefficients\">SorpLib.Media.Functions.Utilities.calcCubicSplineCoefficients</a>.
</p>

<h4>Main equations</h4>
<p>
Cubic spline interpolation involves fitting a third-degree polynomial for each 
interval [<i>T<sub>i</sub></i>;<i>T<sub>i+1</sub></i>] between two neighboring  
abscissa values:
</p>
<pre>
    z<sub>i</sub>(T) = a<sub>i</sub></i> + b<sub>i</sub> * (T - T<sub>i</sub>) + c<sub>i</sub> * (T - T<sub>i</sub>)<sup>2</sup> + d<sub>i</sub> * (T - T<sub>i</sub>)<sup>3</sup>;
</pre>
<p>
Herein, a, b, c , and d describe the coefficients of the polynomial. A system of 
equations is created to determine the coefficients. For this purpose, the function 
values and the first and second derivatives of the polynomials of two neighboring  
intervals are equated at the common abscissa values. Natural cubic splines are 
assumed as a further boundary condition, i.e., the second derivatives are zero at 
the boundary points of the abscissa values. The resulting system of equations can 
be solved efficiently using the tridiagonal matrix algorithm.
</p>

<h4>References</h4>
<ul>
  <li>
  Wikipedia - Spline Interpolation: https://en.wikipedia.org/wiki/Spline_interpolation.
  </li>
  <li>
  Wikipedia - Tridiagonal Matrix Algorithm: https://en.wikipedia.org/wiki/Tridiagonal_matrix_algorithm.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  November 15, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end cubicSplineInterpolation_T;
