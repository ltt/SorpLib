within SorpLib.Media.Functions.Utilities.BasesClasses;
partial function Partial_dz_dT
  "Base function for utility functions calculating the partial derivative of properties w.r.t. temperature as a function of temperature"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.Temperature T
    "Temperature"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of outputs
  //
  output Real dz_dT
    "Partial derivative of fluid property data w.r.t. temperature"
    annotation (Dialog(tab="General", group="Outputs", enable=false));

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial function is the basic function for utility functions that calculate
the partial derivative of an arbitrary property with respect to temperature 
as a function of temperature.
<br/><br/>
This partial function defines the temperature <i>T</i> as input and the partial
derivative of the arbitrary property with respect to temperature <i>dz_dT</i> as 
output.
<br/><br/>
Functions that inherit properties from this partial function may have to add
further inputs, outputs, and the function algorithm.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 15, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Partial_dz_dT;
