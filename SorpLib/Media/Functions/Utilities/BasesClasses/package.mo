within SorpLib.Media.Functions.Utilities;
package BasesClasses "Base classes used to build new utility functions"
  extends Modelica.Icons.BasesPackage;

annotation (Documentation(revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>", info="<html>
<p>
This package contains partial basic functions and models. These partial functions
and models contain fundamental definitions of utility functions. The content of this 
package is only of interest when adding new utility functions to the library.
</p>
</html>"));
end BasesClasses;
