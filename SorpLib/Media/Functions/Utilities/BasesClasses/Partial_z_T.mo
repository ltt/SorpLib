within SorpLib.Media.Functions.Utilities.BasesClasses;
partial function Partial_z_T
  "Base function for utility functions calculating properties as a function of temperature"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.Temperature T
    "Temperature"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of outputs
  //
  output Real z
    "Fluid property data"
    annotation (Dialog(
      tab="General",
      group="Outputs",
      enable=false));

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This partial function is the basic function for utility functions that calculate
an arbitrary property as a function of temperature.
<br/><br/>
This partial function defines the temperature <i>T</i> as input and the arbitrary 
property <i>z</i> as output.
<br/><br/>
Functions that inherit properties from this partial function may have to add
further inputs, outputs, and the function algorithm.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 15, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Partial_z_T;
