within SorpLib.Media.Functions.Utilities;
function ddlinearInterpolation_dT_dT
  "Second-order partial derivative of linearly interpolated arbitrary fluid property z w.r.t. temperature"
  extends SorpLib.Media.Functions.Utilities.BasesClasses.Partial_ddz_dT_dT;

  //
  // Definition of inputs
  //
  input Real abscissa[:]
    "Known abscissa values"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real ordinate[size(abscissa,1)]
    "Known ordinate values"
    annotation (Dialog(tab="General", group="Inputs"));

algorithm
  ddz_dT_dT :=0
    "Second-order partial derivative of fluid property data w.r.t. temperature";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function is the second-order partial derivative of the function 
'linearInterpolation_T' with respect to the temperature. For full details of the 
original function 'linearInterpolation_T,' check the documentation of the function 
<a href=\"Modelica://SorpLib.Media.Functions.Utilities.linearInterpolation_T\">SorpLib.Media.Functions.Utilities.linearInterpolation_T</a>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 15, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end ddlinearInterpolation_dT_dT;
