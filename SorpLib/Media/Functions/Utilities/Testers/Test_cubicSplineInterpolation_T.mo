within SorpLib.Media.Functions.Utilities.Testers;
model Test_cubicSplineInterpolation_T
  "Tester for the function 'cubicSplineInterpolation_T' and all corresponding functions"
  extends Modelica.Icons.Example;

  //
  // Definition of parameters
  //
  parameter Real[21] abscissa = {
    0.01, 5, 10, 15, 20, 25, 30, 35, 40, 45,
    50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100} .+ 273.15
    "Abscissa values";
  parameter Real[21] ordinate = {
    206.14, 147.12, 106.38, 77.93, 57.79, 43.36, 32.89, 25.22, 19.52, 15.26,
    12.03, 9.586, 7.671, 6.197, 5.042, 4.131, 3.407, 2.828, 2.361, 1.982, 1.6729}
    "Abscissa values";

  final parameter Real[21,4] coefficients=
    SorpLib.Media.Functions.Utilities.calcCubicSplineCoefficients(
    abscissa=abscissa,
    ordinate=ordinate)
    "Coefficient a to d for cubic polynomials";

  parameter Modelica.Units.SI.TemperatureDifference dT = 1e-3
    "Temperature difference used to calculated partial derivatives numerically";

  //
  // Definition of variables
  //
  Modelica.Units.SI.Temperature T(start=263.15, fixed=true)
    "Temperature";

  Real z
    "Cubic spline interpolation";
  Real z_num
    "Linear interpolation calculated numerically";

  Real dz_dT
    "Partial derivative of cubic spline interpolation w.r.t. temperature";
  Real dz_dT_num
    "Partial derivative of cubic spline interpolation w.r.t. temperature calculated 
    numerically";

  Real ddz_dT_dT
    "Second-order partial derivative of linear interpolation w.r.t. temperature";
  Real ddz_dT_dT_num
    "Second-order partial derivative of linear interpolation w.r.t. temperature
    calculated numerically";

  Real int_z_dT
    "Indefinite integral of linear interpolation w.r.t. temperature";

equation
  //
  // Definition of derivatives
  //
  der(T) = 120/20
    "Predecsriped slope of T";

  //
  // Calculate properties
  //
  z=SorpLib.Media.Functions.Utilities.cubicSplineInterpolation_T(
    T=T,
    abscissa=abscissa,
    ordinate=ordinate,
    coefficients=coefficients)
    "Cubic spline interpolation";

  dz_dT=
    SorpLib.Media.Functions.Utilities.dcubicSplineInterpolation_dT(
    T=T,
    abscissa=abscissa,
    ordinate=ordinate,
    coefficients=coefficients)
    "Partial derivative of cubic spline interpolation w.r.t. temperature";
  dz_dT_num=
    (SorpLib.Media.Functions.Utilities.cubicSplineInterpolation_T(
    T=T+dT,
    abscissa=abscissa,
    ordinate=ordinate,
    coefficients=coefficients) -
    SorpLib.Media.Functions.Utilities.cubicSplineInterpolation_T(
    T=T-dT,
    abscissa=abscissa,
    ordinate=ordinate,
    coefficients=coefficients)) /
    (2*dT)
    "Partial derivative of cubic spline interpolation w.r.t. temperature calculated 
    numerically";

  ddz_dT_dT=
    SorpLib.Media.Functions.Utilities.ddcubicSplineInterpolation_dT_dT(
    T=T,
    abscissa=abscissa,
    ordinate=ordinate,
    coefficients=coefficients)
    "Second-order partial derivative of linear interpolation w.r.t. temperature";
  ddz_dT_dT_num=
    (SorpLib.Media.Functions.Utilities.dcubicSplineInterpolation_dT(
    T=T+dT,
    abscissa=abscissa,
    ordinate=ordinate,
    coefficients=coefficients) -
    SorpLib.Media.Functions.Utilities.dcubicSplineInterpolation_dT(
    T=T-dT,
    abscissa=abscissa,
    ordinate=ordinate,
    coefficients=coefficients)) /
    (2*dT)
    "Second-order partial derivative of linear interpolation w.r.t. temperature
    calculated numerically";

  int_z_dT = SorpLib.Media.Functions.Utilities.intCubicSplineInterpolation_dT(
    T=T,
    abscissa=abscissa,
    ordinate=ordinate,
    coefficients=coefficients)
    "Indefinite integral of linear interpolation w.r.t. temperature";
  z_num = (
    SorpLib.Media.Functions.Utilities.intCubicSplineInterpolation_dT(
    T=T+dT,
    abscissa=abscissa,
    ordinate=ordinate,
    coefficients=coefficients) -
    SorpLib.Media.Functions.Utilities.intCubicSplineInterpolation_dT(
    T=T-dT,
    abscissa=abscissa,
    ordinate=ordinate,
    coefficients=coefficients)) / (2 * dT)
    "Linear interpolation calculated numerically";

  //
  // Definition of assertions: Check numerical implementations
  //
  assert(abs(dz_dT-dz_dT_num) < 1e-6,
    "Partial derivative of z is not valied: Deviation (|" +
    String(abs(dz_dT-dz_dT_num)) +
    "|) is greater than 1e-6!",
    level = AssertionLevel.warning);

  assert(abs(ddz_dT_dT-ddz_dT_dT_num) < 1e-6,
    "Second-order partial derivative of z is not valied: Deviation (|" +
    String(abs(ddz_dT_dT-ddz_dT_dT_num)) +
    "|) is greater than 1e-6!",
    level = AssertionLevel.warning);

  assert(abs(z-z_num) < 1e-6,
    "Indefinite integral of of z is not valied: Deviation (|" +
    String(abs(z-z_num)) +
    "|) is greater than 1e-6!",
    level = AssertionLevel.warning);

  //
  // Annotations
  //
  annotation (experiment(
      StopTime=20,
      Tolerance=1e-06),
Documentation(revisions="<html>
<ul>
  <li>
  November 15, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>", info="<html>
<p>
This tester shows the behavior of the 'cubicSplineInterpolation_T' function, its 
partial derivative with respect to temperature, and its indefinite integral with 
respect to temperature.
<br/><br/>
To see the function behavior, plot the variables <i>z_i</i> over the time. The 
simulation time is correctly preset (Start: 0 s, Stop = 20 s). 
</p>
</html>"));
end Test_cubicSplineInterpolation_T;
