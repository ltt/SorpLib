within SorpLib.Media.Functions.Utilities;
package Testers "Models to test and/or varify utility functions or models"
  extends Modelica.Icons.ExamplesPackage;

  annotation (Documentation(info="<html>
<p>
This package contains executable test models for all functions and models of the
'Utilities' package. The test models check the implementation of the functions and
models and enable verification of their behavior. In addition, the test models 
demonstrate the functions' and models' general applicability.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Testers;
