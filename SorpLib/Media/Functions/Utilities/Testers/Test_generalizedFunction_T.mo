within SorpLib.Media.Functions.Utilities.Testers;
model Test_generalizedFunction_T
  "Tester for the function 'generalizedFunction_T' and all corresponding functions"
  extends Modelica.Icons.Example;

  //
  // Definition of parameters
  //
  parameter Modelica.Units.SI.TemperatureDifference dT = 1e-3
    "Temperature difference used to calculated partial derivatives numerically";

  //
  // Definition of variables
  //
  Modelica.Units.SI.Temperature T(start=273.15, fixed=true)
    "Temperature";

  //
  // Generalized function
  //
  Real z_polynomialTemperature
    "Polynomial function using temperature for calculation";
  Real z_polynomialReducedTemperature
    "Polynomial function using reduced temperature for calculation";
  Real z_exponentialTemperature
    "Exponential function using temperature for calculation";
  Real z_exponentialReducedTemperature
    "Exponential function using reduced temperature for calculation";

  Real z_polynomialTemperatureNum
    "Polynomial function using temperature for calculation calculated numerically
    from integral";
  Real z_polynomialReducedTemperatureNum
    "Polynomial function using reduced temperature for calculation calculated 
    numerically from integral";
  Real z_exponentialTemperatureNum
    "Exponential function using temperature for calculation calculated numerically
    from integral";
  Real z_exponentialReducedTemperatureNum
    "Exponential function using reduced temperature for calculation calculated
    numerically from integral";

  //
  // Partial derivative of generalized function w.r.t. temperature
  //
  Real dz_dT_polynomialTemperature
    "Partial derivative of polynomial function using temperature for calculation
    w.r.t. temperature";
  Real dz_dT_polynomialReducedTemperature
    "Partial derivative of polynomial function using reduced temperature for 
    calculation w.r.t. temperature";
  Real dz_dT_exponentialTemperature
    "Partial derivative of exponential function using temperature for calculation
    w.r.t. temperature";
  Real dz_dT_exponentialReducedTemperature
    "Partial derivative of exponential function using reduced temperature for 
    calculation w.r.t. temperature";

  Real dz_dT_polynomialTemperatureNum
    "Partial derivative of polynomial function using temperature for calculation
    w.r.t. temperature calculated numerically";
  Real dz_dT_polynomialReducedTemperatureNum
    "Partial derivative of polynomial function using reduced temperature for 
    calculation w.r.t. temperature calculated numerically";
  Real dz_dT_exponentialTemperatureNum
    "Partial derivative of exponential function using temperature for calculation
    w.r.t. temperature calculated numerically";
  Real dz_dT_exponentialReducedTemperatureNum
    "Partial derivative of exponential function using reduced temperature for 
    calculation w.r.t. temperature calculated numerically";

  //
  // Second-order partial derivative of generalized function w.r.t. temperature
  //
  Real ddz_dT_dT_polynomialTemperature
    "Second-order partial derivative of polynomial function using temperature for 
    calculation w.r.t. temperature";
  Real ddz_dT_dT_polynomialReducedTemperature
    "Second-order partial derivative of polynomial function using reduced 
    temperature for calculation w.r.t. temperature";
  Real ddz_dT_dT_exponentialTemperature
    "Second-order partial derivative of exponential function using temperature 
    for calculation  w.r.t. temperature";
  Real ddz_dT_dT_exponentialReducedTemperature
    "Second-order partial derivative of exponential function using reduced 
    temperature for  calculation w.r.t. temperature";

  Real ddz_dT_dT_polynomialTemperatureNum
    "Second-order partial derivative of polynomial function using temperature for 
    calculation w.r.t. temperature calculated numerically";
  Real ddz_dT_dT_polynomialReducedTemperatureNum
    "Second-order partial derivative of polynomial function using reduced 
    temperature for calculation w.r.t. temperature calculated numerically";
  Real ddz_dT_dT_exponentialTemperatureNum
    "Second-order partial derivative of exponential function using temperature 
    for calculation  w.r.t. temperature calculated numerically";
  Real ddz_dT_dT_exponentialReducedTemperatureNum
    "Second-order partial derivative of exponential function using reduced 
    temperature for  calculation w.r.t. temperature calculated numerically";

  //
  // Idefinite integral of generalized function w.r.t. temperature
  //
  Real int_z_dT_polynomialTemperature
    "Indefinite integral of polynomial function using temperature for calculation
    w.r.t. temperature";
  Real int_z_dT_polynomialReducedTemperature
    "Indefinite integral of polynomial function using reduced temperature for 
    calculation w.r.t. temperature";
  Real int_z_dT_exponentialTemperature
    "Indefinite integral of exponential function using temperature for calculation
    w.r.t. temperature";
  Real int_z_dT_exponentialReducedTemperature
    "Indefinite integral of exponential function using reduced temperature for 
    calculation w.r.t. temperature";

equation
  //
  // Definition of derivatives
  //
  der(T) = 100/20
    "Predecsriped slope of T";

  //
  // Generalized function
  //
  z_polynomialTemperature=
    SorpLib.Media.Functions.Utilities.generalizedFunction_T(
    T=T,
    T_ref=647.096,
    z_ref=5,
    coefficients={0,1,1,3},
    exponents={0,0.1,0.2,0.3},
    approach=SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionTemperature)
    "Polynomial function using temperature for calculation";
  z_polynomialReducedTemperature=
    SorpLib.Media.Functions.Utilities.generalizedFunction_T(
    T=T,
    T_ref=647.096,
    z_ref=322,
    coefficients={1,1.99274064,1.09965342,-0.510839303,-1.75493479,-45.5170352,
      -6.74694450e5},
    exponents={0,1/3,2/3,5/3,16/3,43/3,110/3},
    approach=SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionReducedTemperature)
    "Polynomial function using reduced temperature for calculation";
  z_exponentialTemperature=
    SorpLib.Media.Functions.Utilities.generalizedFunction_T(
    T=T,
    T_ref=647.096,
    z_ref=5,
    coefficients={0,1,1,3},
    exponents={0,0.01,0.02,0.03},
    approach=SorpLib.Choices.GeneralizedFunctionApproach.ExponentialFunctionTemperature)
    "Exponential function using temperature for calculation";
  z_exponentialReducedTemperature=
    SorpLib.Media.Functions.Utilities.generalizedFunction_T(
    T=T,
    T_ref=647.096,
    z_ref=322,
    coefficients={-2.03150240,-2.68302940,-5.38626492,-17.2991605,-44.7586581,-63.9201063},
    exponents={2/6,4/6,8/6,18/6,37/6,71/6},
    approach=SorpLib.Choices.GeneralizedFunctionApproach.ExponentialFunctionReducedTemperature)
    "Exponential function using reduced temperature for calculation";

  //
  // Partial derivative of generalized function w.r.t. temperature
  //
  dz_dT_polynomialTemperature=
    SorpLib.Media.Functions.Utilities.dgeneralizedFunction_dT(
    T=T,
    T_ref=647.096,
    z_ref=5,
    coefficients={0,1,1,3},
    exponents={0,0.1,0.2,0.3},
    approach=SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionTemperature)
    "Partial derivative of polynomial function using temperature for calculation
    w.r.t. temperature";
  dz_dT_polynomialReducedTemperature=
    SorpLib.Media.Functions.Utilities.dgeneralizedFunction_dT(
    T=T,
    T_ref=647.096,
    z_ref=322,
    coefficients={1,1.99274064,1.09965342,-0.510839303,-1.75493479,-45.5170352,
      -6.74694450e5},
    exponents={0,1/3,2/3,5/3,16/3,43/3,110/3},
    approach=SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionReducedTemperature)
    "Partial derivative of polynomial function using reduced temperature for 
    calculation w.r.t. temperature";
  dz_dT_exponentialTemperature=
    SorpLib.Media.Functions.Utilities.dgeneralizedFunction_dT(
    T=T,
    T_ref=647.096,
    z_ref=5,
    coefficients={0,1,1,3},
    exponents={0,0.01,0.02,0.03},
    approach=SorpLib.Choices.GeneralizedFunctionApproach.ExponentialFunctionTemperature)
    "Partial derivative of exponential function using temperature for calculation
    w.r.t. temperature";
  dz_dT_exponentialReducedTemperature=
    SorpLib.Media.Functions.Utilities.dgeneralizedFunction_dT(
    T=T,
    T_ref=647.096,
    z_ref=322,
    coefficients={-2.03150240,-2.68302940,-5.38626492,-17.2991605,-44.7586581,-63.9201063},
    exponents={2/6,4/6,8/6,18/6,37/6,71/6},
    approach=SorpLib.Choices.GeneralizedFunctionApproach.ExponentialFunctionReducedTemperature)
    "Partial derivative of exponential function using reduced temperature for 
    calculation w.r.t. temperature";

  dz_dT_polynomialTemperatureNum=(
    SorpLib.Media.Functions.Utilities.generalizedFunction_T(
    T=T + dT,
    T_ref=647.096,
    z_ref=5,
    coefficients={0,1,1,3},
    exponents={0,0.1,0.2,0.3},
    approach=SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionTemperature)
     - SorpLib.Media.Functions.Utilities.generalizedFunction_T(
    T=T - dT,
    T_ref=647.096,
    z_ref=5,
    coefficients={0,1,1,3},
    exponents={0,0.1,0.2,0.3},
    approach=SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionTemperature))
    /(2*dT)
    "Partial derivative of polynomial function using temperature for calculation
    w.r.t. temperature calculated numerically";
  dz_dT_polynomialReducedTemperatureNum=(
    SorpLib.Media.Functions.Utilities.generalizedFunction_T(
    T=T + dT,
    T_ref=647.096,
    z_ref=322,
    coefficients={1,1.99274064,1.09965342,-0.510839303,-1.75493479,-45.5170352,
      -6.74694450e5},
    exponents={0,1/3,2/3,5/3,16/3,43/3,110/3},
    approach=SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionReducedTemperature)
     - SorpLib.Media.Functions.Utilities.generalizedFunction_T(
    T=T - dT,
    T_ref=647.096,
    z_ref=322,
    coefficients={1,1.99274064,1.09965342,-0.510839303,-1.75493479,-45.5170352,
      -6.74694450e5},
    exponents={0,1/3,2/3,5/3,16/3,43/3,110/3},
    approach=SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionReducedTemperature))
    /(2*dT)
    "Partial derivative of polynomial function using reduced temperature for 
    calculation w.r.t. temperature calculated numerically";
  dz_dT_exponentialTemperatureNum=(
    SorpLib.Media.Functions.Utilities.generalizedFunction_T(
    T=T + dT,
    T_ref=647.096,
    z_ref=5,
    coefficients={0,1,1,3},
    exponents={0,0.01,0.02,0.03},
    approach=SorpLib.Choices.GeneralizedFunctionApproach.ExponentialFunctionTemperature)
     - SorpLib.Media.Functions.Utilities.generalizedFunction_T(
    T=T - dT,
    T_ref=647.096,
    z_ref=5,
    coefficients={0,1,1,3},
    exponents={0,0.01,0.02,0.03},
    approach=SorpLib.Choices.GeneralizedFunctionApproach.ExponentialFunctionTemperature))
    /(2*dT)
    "Partial derivative of exponential function using temperature for calculation
    w.r.t. temperature calculated numerically";
  dz_dT_exponentialReducedTemperatureNum=(
    SorpLib.Media.Functions.Utilities.generalizedFunction_T(
    T=T + dT,
    T_ref=647.096,
    z_ref=322,
    coefficients={-2.03150240,-2.68302940,-5.38626492,-17.2991605,-44.7586581,-63.9201063},
    exponents={2/6,4/6,8/6,18/6,37/6,71/6},
    approach=SorpLib.Choices.GeneralizedFunctionApproach.ExponentialFunctionReducedTemperature)
     - SorpLib.Media.Functions.Utilities.generalizedFunction_T(
    T=T - dT,
    T_ref=647.096,
    z_ref=322,
    coefficients={-2.03150240,-2.68302940,-5.38626492,-17.2991605,-44.7586581,-63.9201063},
    exponents={2/6,4/6,8/6,18/6,37/6,71/6},
    approach=SorpLib.Choices.GeneralizedFunctionApproach.ExponentialFunctionReducedTemperature))
    /(2*dT)
    "Partial derivative of exponential function using reduced temperature for 
    calculation w.r.t. temperature calculated numerically";

  //
  // Second-order partial derivative of generalized function w.r.t. temperature
  //
  ddz_dT_dT_polynomialTemperature=
    SorpLib.Media.Functions.Utilities.ddgeneralizedFunction_dT_dT(
    T=T,
    T_ref=647.096,
    z_ref=5,
    coefficients={0,1,1,3},
    exponents={0,0.1,0.2,0.3},
    approach=SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionTemperature)
    "Second-order partial derivative of polynomial function using temperature for 
    calculation w.r.t. temperature";
  ddz_dT_dT_polynomialReducedTemperature=
    SorpLib.Media.Functions.Utilities.ddgeneralizedFunction_dT_dT(
    T=T,
    T_ref=647.096,
    z_ref=322,
    coefficients={1,1.99274064,1.09965342,-0.510839303,-1.75493479,-45.5170352,
      -6.74694450e5},
    exponents={0,1/3,2/3,5/3,16/3,43/3,110/3},
    approach=SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionReducedTemperature)
    "Second-order partial derivative of polynomial function using reduced 
    temperature for calculation w.r.t. temperature";
  ddz_dT_dT_exponentialTemperature=
    SorpLib.Media.Functions.Utilities.ddgeneralizedFunction_dT_dT(
    T=T,
    T_ref=647.096,
    z_ref=5,
    coefficients={0,1,1,3},
    exponents={0,0.01,0.02,0.03},
    approach=SorpLib.Choices.GeneralizedFunctionApproach.ExponentialFunctionTemperature)
    "Second-order partial derivative of exponential function using temperature 
    for calculation  w.r.t. temperature";
  ddz_dT_dT_exponentialReducedTemperature=
    SorpLib.Media.Functions.Utilities.ddgeneralizedFunction_dT_dT(
    T=T,
    T_ref=647.096,
    z_ref=322,
    coefficients={-2.03150240,-2.68302940,-5.38626492,-17.2991605,-44.7586581,-63.9201063},
    exponents={2/6,4/6,8/6,18/6,37/6,71/6},
    approach=SorpLib.Choices.GeneralizedFunctionApproach.ExponentialFunctionReducedTemperature)
    "Second-order partial derivative of exponential function using reduced 
    temperature for  calculation w.r.t. temperature";

  ddz_dT_dT_polynomialTemperatureNum=(
    SorpLib.Media.Functions.Utilities.dgeneralizedFunction_dT(
    T=T + dT,
    T_ref=647.096,
    z_ref=5,
    coefficients={0,1,1,3},
    exponents={0,0.1,0.2,0.3},
    approach=SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionTemperature)
     - SorpLib.Media.Functions.Utilities.dgeneralizedFunction_dT(
    T=T - dT,
    T_ref=647.096,
    z_ref=5,
    coefficients={0,1,1,3},
    exponents={0,0.1,0.2,0.3},
    approach=SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionTemperature))
    /(2*dT)
    "Second-order partial derivative of polynomial function using temperature for 
    calculation w.r.t. temperature calculated numerically";
  ddz_dT_dT_polynomialReducedTemperatureNum=(
    SorpLib.Media.Functions.Utilities.dgeneralizedFunction_dT(
    T=T + dT,
    T_ref=647.096,
    z_ref=322,
    coefficients={1,1.99274064,1.09965342,-0.510839303,-1.75493479,-45.5170352,
      -6.74694450e5},
    exponents={0,1/3,2/3,5/3,16/3,43/3,110/3},
    approach=SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionReducedTemperature)
     - SorpLib.Media.Functions.Utilities.dgeneralizedFunction_dT(
    T=T - dT,
    T_ref=647.096,
    z_ref=322,
    coefficients={1,1.99274064,1.09965342,-0.510839303,-1.75493479,-45.5170352,
      -6.74694450e5},
    exponents={0,1/3,2/3,5/3,16/3,43/3,110/3},
    approach=SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionReducedTemperature))
    /(2*dT)
    "Second-order partial derivative of polynomial function using reduced 
    temperature for calculation w.r.t. temperature calculated numerically";
  ddz_dT_dT_exponentialTemperatureNum=(
    SorpLib.Media.Functions.Utilities.dgeneralizedFunction_dT(
    T=T + dT,
    T_ref=647.096,
    z_ref=5,
    coefficients={0,1,1,3},
    exponents={0,0.01,0.02,0.03},
    approach=SorpLib.Choices.GeneralizedFunctionApproach.ExponentialFunctionTemperature)
     - SorpLib.Media.Functions.Utilities.dgeneralizedFunction_dT(
    T=T - dT,
    T_ref=647.096,
    z_ref=5,
    coefficients={0,1,1,3},
    exponents={0,0.01,0.02,0.03},
    approach=SorpLib.Choices.GeneralizedFunctionApproach.ExponentialFunctionTemperature))
    /(2*dT)
    "Second-order partial derivative of exponential function using temperature 
    for calculation  w.r.t. temperature calculated numerically";
  ddz_dT_dT_exponentialReducedTemperatureNum=(
    SorpLib.Media.Functions.Utilities.dgeneralizedFunction_dT(
    T=T + dT,
    T_ref=647.096,
    z_ref=322,
    coefficients={-2.03150240,-2.68302940,-5.38626492,-17.2991605,-44.7586581,-63.9201063},
    exponents={2/6,4/6,8/6,18/6,37/6,71/6},
    approach=SorpLib.Choices.GeneralizedFunctionApproach.ExponentialFunctionReducedTemperature)
     - SorpLib.Media.Functions.Utilities.dgeneralizedFunction_dT(
    T=T - dT,
    T_ref=647.096,
    z_ref=322,
    coefficients={-2.03150240,-2.68302940,-5.38626492,-17.2991605,-44.7586581,-63.9201063},
    exponents={2/6,4/6,8/6,18/6,37/6,71/6},
    approach=SorpLib.Choices.GeneralizedFunctionApproach.ExponentialFunctionReducedTemperature))
    /(2*dT)
    "Second-order partial derivative of exponential function using reduced 
    temperature for  calculation w.r.t. temperature calculated numerically";

  //
  // Idefinite integral of generalized function w.r.t. temperature
  //
  int_z_dT_polynomialTemperature =
    SorpLib.Media.Functions.Utilities.intGeneralizedFunction_dT(
    T=T,
    T_ref=647.096,
    z_ref=5,
    coefficients={0,1,1,3},
    exponents={0,0.1,0.2,0.3},
    approach=SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionTemperature)
    "Indefinite integral of polynomial function using temperature for calculation
    w.r.t. temperature";
  int_z_dT_polynomialReducedTemperature =
    SorpLib.Media.Functions.Utilities.intGeneralizedFunction_dT(
    T=T,
    T_ref=647.096,
    z_ref=322,
    coefficients={1,1.99274064,1.09965342,-0.510839303,-1.75493479,-45.5170352,
      -6.74694450e5},
    exponents={0,1/3,2/3,5/3,16/3,43/3,110/3},
    approach=SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionReducedTemperature)
    "Indefinite integral of polynomial function using reduced temperature for 
    calculation w.r.t. temperature";
  int_z_dT_exponentialTemperature =
    SorpLib.Media.Functions.Utilities.intGeneralizedFunction_dT(
    T=T,
    T_ref=647.096,
    z_ref=5,
    coefficients={0,1,1,3},
    exponents={0,0.01,0.02,0.03},
    approach=SorpLib.Choices.GeneralizedFunctionApproach.ExponentialFunctionTemperature)
    "Indefinite integral of exponential function using temperature for calculation
    w.r.t. temperature";
  int_z_dT_exponentialReducedTemperature =
    SorpLib.Media.Functions.Utilities.intGeneralizedFunction_dT(
    T=T,
    T_ref=647.096,
    z_ref=322,
    coefficients={-2.03150240,-2.68302940,-5.38626492,-17.2991605,-44.7586581,-63.9201063},
    exponents={2/6,4/6,8/6,18/6,37/6,71/6},
    approach=SorpLib.Choices.GeneralizedFunctionApproach.ExponentialFunctionReducedTemperature)
    "Indefinite integral of exponential function using reduced temperature for 
    calculation w.r.t. temperature";

  z_polynomialTemperatureNum =(
    SorpLib.Media.Functions.Utilities.intGeneralizedFunction_dT(
    T=T + dT,
    T_ref=647.096,
    z_ref=5,
    coefficients={0,1,1,3},
    exponents={0,0.1,0.2,0.3},
    approach=SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionTemperature)
     - SorpLib.Media.Functions.Utilities.intGeneralizedFunction_dT(
    T=T - dT,
    T_ref=647.096,
    z_ref=5,
    coefficients={0,1,1,3},
    exponents={0,0.1,0.2,0.3},
    approach=SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionTemperature))
    /(2*dT)
    "Polynomial function using temperature for calculation calculated numerically
    from integral";
  z_polynomialReducedTemperatureNum =(
    SorpLib.Media.Functions.Utilities.intGeneralizedFunction_dT(
    T=T + dT,
    T_ref=647.096,
    z_ref=322,
    coefficients={1,1.99274064,1.09965342,-0.510839303,-1.75493479,-45.5170352,
      -6.74694450e5},
    exponents={0,1/3,2/3,5/3,16/3,43/3,110/3},
    approach=SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionReducedTemperature)
     - SorpLib.Media.Functions.Utilities.intGeneralizedFunction_dT(
    T=T - dT,
    T_ref=647.096,
    z_ref=322,
    coefficients={1,1.99274064,1.09965342,-0.510839303,-1.75493479,-45.5170352,
      -6.74694450e5},
    exponents={0,1/3,2/3,5/3,16/3,43/3,110/3},
    approach=SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionReducedTemperature))
    /(2*dT)
    "Polynomial function using reduced temperature for calculation calculated 
    numerically from integral";
  z_exponentialTemperatureNum =(
    SorpLib.Media.Functions.Utilities.intGeneralizedFunction_dT(
    T=T + dT,
    T_ref=647.096,
    z_ref=5,
    coefficients={0,1,1,3},
    exponents={0,0.01,0.02,0.03},
    approach=SorpLib.Choices.GeneralizedFunctionApproach.ExponentialFunctionTemperature)
     - SorpLib.Media.Functions.Utilities.intGeneralizedFunction_dT(
    T=T - dT,
    T_ref=647.096,
    z_ref=5,
    coefficients={0,1,1,3},
    exponents={0,0.01,0.02,0.03},
    approach=SorpLib.Choices.GeneralizedFunctionApproach.ExponentialFunctionTemperature))
    /(2*dT)
    "Exponential function using temperature for calculation calculated numerically
    from integral";
  z_exponentialReducedTemperatureNum =(
    SorpLib.Media.Functions.Utilities.intGeneralizedFunction_dT(
    T=T + dT,
    T_ref=647.096,
    z_ref=322,
    coefficients={-2.03150240,-2.68302940,-5.38626492,-17.2991605,-44.7586581,-63.9201063},
    exponents={2/6,4/6,8/6,18/6,37/6,71/6},
    approach=SorpLib.Choices.GeneralizedFunctionApproach.ExponentialFunctionReducedTemperature)
     - SorpLib.Media.Functions.Utilities.intGeneralizedFunction_dT(
    T=T - dT,
    T_ref=647.096,
    z_ref=322,
    coefficients={-2.03150240,-2.68302940,-5.38626492,-17.2991605,-44.7586581,-63.9201063},
    exponents={2/6,4/6,8/6,18/6,37/6,71/6},
    approach=SorpLib.Choices.GeneralizedFunctionApproach.ExponentialFunctionReducedTemperature))
    /(2*dT)
    "Exponential function using reduced temperature for calculation calculated
    numerically from integral";

  //
  // Definition of assertions: Check numerical implementations
  //
  assert(abs(dz_dT_polynomialTemperature-dz_dT_polynomialTemperatureNum) < 1e-6,
    "Partial derivative of dz_dT_polynomialTemperature is not valied: Deviation (|" +
    String(abs(dz_dT_polynomialTemperature-dz_dT_polynomialTemperatureNum)) +
    "|) is greater than 1e-6!",
    level = AssertionLevel.warning);
  assert(abs(dz_dT_polynomialReducedTemperature-dz_dT_polynomialReducedTemperatureNum) < 1e-6,
    "Partial derivative of dz_dT_polynomialTemperature is not valied: Deviation (|" +
    String(abs(dz_dT_polynomialReducedTemperature-dz_dT_polynomialReducedTemperatureNum)) +
    "|) is greater than 1e-6!",
    level = AssertionLevel.warning);
  assert(abs(dz_dT_exponentialTemperature-dz_dT_exponentialTemperatureNum) < 1e-6,
    "Partial derivative of dz_dT_polynomialTemperature is not valied: Deviation (|" +
    String(abs(dz_dT_exponentialTemperature-dz_dT_exponentialTemperatureNum)) +
    "|) is greater than 1e-6!",
    level = AssertionLevel.warning);
  assert(abs(dz_dT_exponentialReducedTemperature-dz_dT_exponentialReducedTemperatureNum) < 1e-6,
    "Partial derivative of dz_dT_polynomialTemperature is not valied: Deviation (|" +
    String(abs(dz_dT_exponentialReducedTemperature-dz_dT_exponentialReducedTemperatureNum)) +
    "|) is greater than 1e-6!",
    level = AssertionLevel.warning);

  assert(abs(ddz_dT_dT_polynomialTemperature-ddz_dT_dT_polynomialTemperatureNum) < 1e-6,
    "Second-order partial derivative of ddz_dT_dT_polynomialTemperature is not " +
    "valied: Deviation (|" +
    String(abs(ddz_dT_dT_polynomialTemperature-ddz_dT_dT_polynomialTemperatureNum)) +
    "|) is greater than 1e-6!",
    level = AssertionLevel.warning);
  assert(abs(ddz_dT_dT_polynomialReducedTemperature-ddz_dT_dT_polynomialReducedTemperatureNum) < 1e-6,
    "Second-order partial derivative of ddz_dT_dT_polynomialTemperature is not " +
    "valied: Deviation (|" +
    String(abs(ddz_dT_dT_polynomialReducedTemperature-ddz_dT_dT_polynomialReducedTemperatureNum)) +
    "|) is greater than 1e-6!",
    level = AssertionLevel.warning);
  assert(abs(ddz_dT_dT_exponentialTemperature-ddz_dT_dT_exponentialTemperatureNum) < 1e-6,
    "Second-order partial derivative of ddz_dT_dT_polynomialTemperature is not " +
    "valied: Deviation (|" +
    String(abs(ddz_dT_dT_exponentialTemperature-ddz_dT_dT_exponentialTemperatureNum)) +
    "|) is greater than 1e-6!",
    level = AssertionLevel.warning);
  assert(abs(ddz_dT_dT_exponentialReducedTemperature-ddz_dT_dT_exponentialReducedTemperatureNum) < 1e-6,
    "Second-order partial derivative of ddz_dT_dT_polynomialTemperature is not " +
    "valied: Deviation (|" +
    String(abs(ddz_dT_dT_exponentialReducedTemperature-ddz_dT_dT_exponentialReducedTemperatureNum)) +
    "|) is greater than 1e-6!",
    level = AssertionLevel.warning);

  assert(abs(z_polynomialTemperature-z_polynomialTemperatureNum) < 1e-6,
    "Indefinite integral of z_polynomialTemperature is not valied: Deviation (|" +
    String(abs(z_polynomialTemperature-z_polynomialTemperatureNum)) +
    "|) is greater than 1e-6!",
    level = AssertionLevel.warning);
  assert(abs(z_polynomialReducedTemperature-z_polynomialReducedTemperatureNum) < 1e-6,
    "Indefinite integral of z_polynomialTemperature is not valied: Deviation (|" +
    String(abs(z_polynomialReducedTemperature-z_polynomialReducedTemperatureNum)) +
    "|) is greater than 1e-6!",
    level = AssertionLevel.warning);
  assert(abs(z_exponentialTemperature-z_exponentialTemperatureNum) < 1e-6,
    "Indefinite integral of z_polynomialTemperature is not valied: Deviation (|" +
    String(abs(z_exponentialTemperature-z_exponentialTemperatureNum)) +
    "|) is greater than 1e-6!",
    level = AssertionLevel.warning);
  assert(abs(z_exponentialReducedTemperature-z_exponentialReducedTemperatureNum) < 1e-6,
    "Indefinite integral of z_polynomialTemperature is not valied: Deviation (|" +
    String(abs(z_exponentialReducedTemperature-z_exponentialReducedTemperatureNum)) +
    "|) is greater than 1e-6!",
    level = AssertionLevel.warning);
  //
  // Annotations
  //
  annotation (experiment(
      StopTime=20,
      Tolerance=1e-06),
Documentation(revisions="<html>
<ul>
  <li>
  November 15, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>", info="<html>
<p>
This tester shows the behavior of the 'generalizedFunction_T' function, its partial 
derivative with respect to temperature, and its indefinite integral with respect to 
temperature.
<br/><br/>
To see the function behavior, plot the variables <i>z_polynomialTemperatureI</i>
over the time. The simulation time is correctly preset (Start: 0 s, Stop = 20 s).  
</p>
</html>"));
end Test_generalizedFunction_T;
