within SorpLib.Media.Functions.Utilities;
function calcCubicSplineCoefficients
  "Calculates coefficients a, b, c, and d for cubic spline interpolation as a function of abscissa and ordinate values"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Real abscissa[:]
    "Known abscissa values"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real ordinate[size(abscissa,1)]
    "Known ordinate values"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of outputs
  //
  output Real coefficients[size(abscissa,1), 4]
    "Coefficient a to d for cubic polynomials"
    annotation (Dialog(tab="General", group="Outputs", enable=false));

  //
  // Definition of variables
  //
protected
  Real h[size(abscissa,1)-1]
    "Interval lengths";

  Real alpha[size(abscissa,1)-1]
    "Intermediate values for solving system";
  Real l[size(abscissa,1)]
    "Lower diagonal of tridiagonal matrix";
  Real mu[size(abscissa,1)-1]
    "Upper diagonal of tridiagonal matrix";
  Real zeta[size(abscissa,1)]
    "Solution vector for second derivatives";

  Real a[size(abscissa,1)]
    "Coefficient a for cubic polynomials";
  Real b[size(abscissa,1)]
    "Coefficient b for cubic polynomials";
  Real c[size(abscissa,1)]
    "Coefficient c for cubic polynomials (i.e., second derivatives at data 
    points)";
  Real d[size(abscissa,1)]
    "Coefficient d for cubic polynomials";

algorithm
  //
  // Initialize values
  //
  for ind in 1:size(abscissa,1) - 1 loop
    h[ind]:= abscissa[ind + 1] - abscissa[ind]
      "Interval lengths";
  end for;

  alpha := zeros(size(abscissa,1)-1)
    "Intermediate values for solving system";
  l := ones(size(abscissa,1))
    "Lower diagonal of tridiagonal matrix";
  mu := zeros(size(abscissa,1)-1)
    "Upper diagonal of tridiagonal matrix";
  zeta := zeros(size(abscissa,1))
    "Solution vector for second derivatives";

  a := ordinate
    "Coefficient a for cubic polynomials";
  b := zeros(size(abscissa,1))
    "Coefficient b for cubic polynomials";
  c := zeros(size(abscissa,1))
    "Coefficient c for cubic polynomials (i.e., second derivatives at data 
    points)";
  d := zeros(size(abscissa,1))
    "Coefficient d for cubic polynomials";

  //
  // System of equations required to solve for the second derivatives
  //
  for ind in 2:size(abscissa,1)-1 loop
    alpha[ind] := (3/h[ind]) * (ordinate[ind+1] - ordinate[ind]) -
      (3/h[ind-1]) * (ordinate[ind] - ordinate[ind-1])
      "Intermediate values for solving system";
  end for;

  //
  // Solve the tridiagonal system using the Thomas algorithm
  //
  for ind in 2:size(abscissa,1)-1 loop
    l[ind] := 2 * (abscissa[ind+1] - abscissa[ind-1]) - h[ind-1] * mu[ind-1]
      "Lower diagonal of tridiagonal matrix";
    mu[ind] := h[ind] / l[ind]
      "Upper diagonal of tridiagonal matrix";
    zeta[ind] := (alpha[ind] - h[ind-1] * zeta[ind-1]) / l[ind]
      "Solution vector for second derivatives";
  end for;

  //
  // Back substitution to find the c-values
  //
  for ind in 1:size(abscissa,1)-1 loop
    c[size(abscissa,1)-ind] := zeta[size(abscissa,1)-ind] -
      mu[size(abscissa,1)-ind] * c[size(abscissa,1)-ind+1]
      "Coefficient c for cubic pilynomials (i.e., second derivatives at data 
      points)";
  end for;

  //
  // Calculate the b- and d-values
  //
  for ind in 1:size(abscissa,1)-1 loop
    b[ind] := (ordinate[ind+1] - ordinate[ind]) / h[ind] -
      h[ind] * (c[ind+1] + 2 * c[ind]) / 3
      "Coefficient b for cubic polynomials";
    d[ind] := (c[ind+1] - c[ind]) / (3 * h[ind])
      "Coefficient d for cubic polynomials";
  end for;

  //
  // Return coefficients
  //
  coefficients[:, 1] :=a
    "Coefficient a for cubic polynomials";
  coefficients[:, 2] :=b
    "Coefficient b for cubic polynomials";
  coefficients[:, 3] :=c
    "Coefficient c for cubic polynomials";
  coefficients[:, 4] :=d
    "Coefficient d for cubic polynomials";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function calculates piecewise cubic spline interpolation coefficients as 
function abscissa and ordinate values. 
</p>

<h4>Main equations</h4>
<p>
Cubic spline interpolation involves fitting a third-degree polynomial for each 
interval [<i>T<sub>i</sub></i>;<i>T<sub>i+1</sub></i>] between two neighboring  
abscissa values:
</p>
<pre>
    z<sub>i</sub>(T) = a<sub>i</sub></i> + b<sub>i</sub> * (T - T<sub>i</sub>) + c<sub>i</sub> * (T - T<sub>i</sub>)<sup>2</sup> + d<sub>i</sub> * (T - T<sub>i</sub>)<sup>3</sup>;
</pre>
<p>
Herein, a, b, c , and d describe the coefficients of the polynomial. A system of 
equations is created to determine the coefficients. For this purpose, the function 
values and the first and second derivatives of the polynomials of two neighboring  
intervals are equated at the common abscissa values. Natural cubic splines are 
assumed as a further boundary condition, i.e., the second derivatives are zero at 
the boundary points of the abscissa values. The resulting system of equations can 
be solved efficiently using the tridiagonal matrix algorithm.
</p>

<h4>References</h4>
<ul>
  <li>
  Wikipedia - Spline Interpolation: https://en.wikipedia.org/wiki/Spline_interpolation.
  </li>
  <li>
  Wikipedia - Tridiagonal Matrix Algorithm: https://en.wikipedia.org/wiki/Tridiagonal_matrix_algorithm.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  November 15, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end calcCubicSplineCoefficients;
