within SorpLib.Media.Functions.Utilities;
function dlinearInterpolation_dT
  "Partial derivative of linearly interpolated arbitrary fluid property z w.r.t. temperature"
  extends SorpLib.Media.Functions.Utilities.BasesClasses.Partial_dz_dT;

  //
  // Definition of inputs
  //
  input Real abscissa[:]
    "Known abscissa values"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real ordinate[size(abscissa,1)]
    "Known ordinate values"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of variables
  //
protected
  Integer ind_gv
    "Index of abscissa that is greater than actual temperature";

algorithm
  if T <= abscissa[1] then
    dz_dT := 0
    "Partial derivative of fluid property data w.r.t. temperature";

  elseif T >= abscissa[end] then
    dz_dT := 0
    "Partial derivative of fluid property data w.r.t. temperature";

  else
    //
    // Finde abscissa that is greater than actual temperature
    //
    ind_gv :=1
      "Index of abscissa that is greater than actual temperature";

      while T > abscissa[ind_gv] loop
      ind_gv :=ind_gv + 1
        "Index of abscissa that is greater than actual temperature";
    end while;

    dz_dT := (ordinate[ind_gv] - ordinate[ind_gv-1]) /
      (abscissa[ind_gv] - abscissa[ind_gv-1])
    "Partial derivative of fluid property data w.r.t. temperature";

  end if;

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function is the partial derivative of the function 'linearInterpolation_T' 
with respect to the temperature. For full details of the original function
'linearInterpolation_T,' check the documentation of the function 
<a href=\"Modelica://SorpLib.Media.Functions.Utilities.linearInterpolation_T\">SorpLib.Media.Functions.Utilities.linearInterpolation_T</a>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 15, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end dlinearInterpolation_dT;
