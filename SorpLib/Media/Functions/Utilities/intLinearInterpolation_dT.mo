within SorpLib.Media.Functions.Utilities;
function intLinearInterpolation_dT
  "Indefinite integral of linearly interpolated arbitrary fluid property z w.r.t. temperature"
  extends SorpLib.Media.Functions.Utilities.BasesClasses.Partial_int_z_dT;

  //
  // Definition of inputs
  //
  input Real abscissa[:]
    "Known abscissa values"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real ordinate[size(abscissa,1)]
    "Known ordinate values"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of variables
  //
protected
  Integer ind_gv
    "Index of abscissa that is greater than actual temperature";

algorithm
  if T <= abscissa[1] then
    int_z_dT := ordinate[1] * T
      "Indefinite integral of fluid property data w.r.t. temperature without 
      integration constant";

  elseif T >= abscissa[end] then
    int_z_dT := ordinate[end] * T
      "Indefinite integral of fluid property data w.r.t. temperature without 
      integration constant";

  else
    //
    // Finde abscissa that is greater than actual temperature
    //
    ind_gv :=1
      "Index of abscissa that is greater than actual temperature";

      while T > abscissa[ind_gv] loop
      ind_gv :=ind_gv + 1
        "Index of abscissa that is greater than actual temperature";
    end while;

    int_z_dT := ordinate[ind_gv-1] * T +
      (ordinate[ind_gv] - ordinate[ind_gv-1]) /
      (abscissa[ind_gv] - abscissa[ind_gv-1]) *
      (T^2 / 2 - abscissa[ind_gv-1] * T)
      "Indefinite integral of fluid property data w.r.t. temperature without 
      integration constant";

  end if;

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function is the indefinite integral of the function 'linearInterpolation_T' 
with respect to the temperature. Note that the integration constant is neglected.
For full details of the original function 'linearInterpolation_T,' check the 
documentation of the function 
<a href=\"Modelica://SorpLib.Media.Functions.Utilities.linearInterpolation_T\">SorpLib.Media.Functions.Utilities.linearInterpolation_T</a>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 15, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end intLinearInterpolation_dT;
