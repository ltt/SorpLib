within SorpLib.Media.Functions.Utilities;
function ddcubicSplineInterpolation_dT_dT
  "Second-order partial derivative of arbitrary fluid property z interpolated via cubic splines w.r.t. temperature"
  extends SorpLib.Media.Functions.Utilities.BasesClasses.Partial_ddz_dT_dT;

  //
  // Definition of inputs
  //
  input Real abscissa[:]
    "Known abscissa values"
    annotation (Dialog(tab="General", group="Inputs"));
  input Real ordinate[size(abscissa,1)]
    "Known ordinate values"
    annotation (Dialog(tab="General", group="Inputs"));

  input Real coefficients[size(abscissa,1),4]
    "Coefficient a to d for cubic polynomials"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of variables
  //
protected
  Integer ind_gv
    "Index of abscissa that is greater than actual temperature";

algorithm
  if T <= abscissa[1] then
    ddz_dT_dT := 0
      "Second-order partial derivative of fluid property data w.r.t. temperature";

  elseif T >= abscissa[end] then
    ddz_dT_dT := 0
      "Second-order partial derivative of fluid property data w.r.t. temperature";

  else
    ind_gv :=1
      "Index of abscissa that is greater than actual temperature";

    while T > abscissa[ind_gv+1] loop
      ind_gv :=ind_gv + 1
        "Index of abscissa that is greater than actual temperature";
    end while;

    ddz_dT_dT := 2 * coefficients[ind_gv,3] +
      6 * coefficients[ind_gv,4] * (T - abscissa[ind_gv])
      "Second-order partial derivative of fluid property data w.r.t. temperature";
  end if;

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function is the second-order partial derivative of the function 
'cubicSplineInterpolation_T' with respect to the temperature. For full details of 
the original function 'cubicSplineInterpolation_T,' check the documentation of the 
function 
<a href=\"Modelica://SorpLib.Media.Functions.Utilities.cubicSplineInterpolation_T\">SorpLib.Media.Functions.Utilities.cubicSplineInterpolation_T</a>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 15, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end ddcubicSplineInterpolation_dT_dT;
