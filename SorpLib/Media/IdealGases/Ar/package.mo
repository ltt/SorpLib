within SorpLib.Media.IdealGases;
package Ar "SorpLib: Ideal gas Ar from NASA Glenn coefficients"
  extends Modelica.Media.IdealGases.SingleGases.Ar(
    final reference_T(min=0) = 0,
    final reference_p = 1e5,
    final reference_X=fill(1/nX, nX));

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This medium model calculates fluid property data of Ar. For details,
check the package
<a href=\"Modelica://Modelica.Media.IdealGases.SingleGases.Ar\">Modelica.Media.IdealGases.SingleGases.Ar</a>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Ar;
