within SorpLib.Media.IdealGases;
package H2O "SorpLib: Ideal gas H2O from NASA Glenn coefficients"
  extends Modelica.Media.IdealGases.SingleGases.H2O(
    final reference_T(min=0) = 0,
    final reference_p = 1e5,
    final reference_X=fill(1/nX, nX));

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This medium model calculates fluid property data of H<sub>2</sub>O. For details,
check the package
<a href=\"Modelica://Modelica.Media.IdealGases.SingleGases.H2O\">Modelica.Media.IdealGases.SingleGases.H2O</a>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end H2O;
