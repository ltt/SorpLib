within SorpLib.Media.IdealGases;
package CH4 "SorpLib: Ideal gas CH4 from NASA Glenn coefficients"
  extends Modelica.Media.IdealGases.SingleGases.CH4(
    final reference_T(min=0) = 0,
    final reference_p = 1e5,
    final reference_X=fill(1/nX, nX));

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This medium model calculates fluid property data of CH<sub>4</sub>. For details,
check the package
<a href=\"Modelica://Modelica.Media.IdealGases.SingleGases.CH4\">Modelica.Media.IdealGases.SingleGases.CH4</a>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end CH4;
