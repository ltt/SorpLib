within SorpLib.Media.IdealGases.Tester;
model Test_N2 "Tester for ideal gas N2"
  extends SorpLib.Media.IdealGases.Tester.Test_Ar(
     redeclare package Medium = SorpLib.Media.IdealGases.N2);

  //
  // Annotations
  //
  annotation (experiment(StopTime=20, Tolerance=1e-06),
Documentation(info="<html>
<p>
This model checks the fluid property calculation of the ideal
gas N<sub>2</sub>.
<br/><br/>
To see the model behavior, plot the variables of the models over the 
time. The simulation time is correctly preset (Start: 0 s, Stop = 20 s). 
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Test_N2;
