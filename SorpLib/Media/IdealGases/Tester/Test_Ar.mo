within SorpLib.Media.IdealGases.Tester;
model Test_Ar "Tester for ideal gas Ar"
  extends Modelica.Icons.Example;

  //
  // Definition of paramters
  //
  replaceable package Medium = SorpLib.Media.IdealGases.Ar
    constrainedby Modelica.Media.IdealGases.Common.SingleGasNasa
    "Medium"
    annotation (Dialog(tab="General",group="Models and Media"),
                choicesAllMatching = true);

  //
  // Definition of variables
  //
  Modelica.Units.SI.Pressure p(start=100, fixed=true)
    "Pressure";
  Modelica.Units.SI.Temperature T(start=253.15, fixed=true)
    "Temperature";
  Modelica.Units.SI.SpecificVolume v
    "Specific volume";
  Modelica.Units.SI.SpecificEntropy s
    "Specific entropy";

  Medium.BaseProperties medium_pT
    "Base properties calculated with pressure and temperature";


  Modelica.Units.SI.SpecificHeatCapacity cp
    "Specific heat capacity";
  Modelica.Units.SI.RelativePressureCoefficient beta
    "Isobaric expansion coefficient";
  Modelica.Units.SI.IsothermalCompressibility kappa
    "Isothermal compressibility";


equation
  //
  // Change independent state variables
  //
  der(p) = (20e5 - 100) / 20
    "Predescribed slope of pressure";
  der(T) = (573.15 - 253.15) / 20
    "Predescribed slope of temperature";

  //
  // Calculate state variables
  //
  medium_pT.p = p
    "Base properties calculated with pressure and temperature";
  medium_pT.T = T
    "Base properties calculated with pressure and temperature";

  //
  // Calculate further variables
  //
  v = 1/medium_pT.d
    "Specific volume";
  s = Medium.specificEntropy(state=medium_pT.state)
    "Specific entropy";

  cp = Medium.specificHeatCapacityCp(state=medium_pT.state)
    "Specific heat capacity";
  beta = Medium.beta(state=medium_pT.state)
    "Isobaric expansion coefficient";
  kappa = Medium.kappa(state=medium_pT.state)
    "Isothermal compressibility";

  //
  // Annotations
  //
  annotation (experiment(StopTime=20, Tolerance=1e-06),
Documentation(info="<html>
<p>
This model checks the fluid property calculation of the ideal
gas Ar.
<br/><br/>
To see the model behavior, plot the variables of the models over the 
time. The simulation time is correctly preset (Start: 0 s, Stop = 20 s). 
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Test_Ar;
