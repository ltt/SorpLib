within SorpLib.Media.IdealGases.Tester;
model Test_O2 "Tester for ideal gas O2"
  extends SorpLib.Media.IdealGases.Tester.Test_Ar(
     redeclare package Medium = SorpLib.Media.IdealGases.O2);

  //
  // Annotations
  //
  annotation (experiment(StopTime=20, Tolerance=1e-06),
Documentation(info="<html>
<p>
This model checks the fluid property calculation of the ideal
gas O<sub>2</sub>.
<br/><br/>
To see the model behavior, plot the variables of the models over the 
time. The simulation time is correctly preset (Start: 0 s, Stop = 20 s). 
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Test_O2;
