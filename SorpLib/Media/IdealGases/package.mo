within SorpLib.Media;
package IdealGases "Package containing medium models of ideal gases"
  extends Modelica.Icons.VariantsPackage;

  annotation (Documentation(info="<html>
<p>
This package contains models of ideal gases. These models are based on the 
Modelica Standard Library. Note that the reference temperature must be 0 K
and the reference enthalpy and entropy must be 0 J/kg and 0 J/(Kg.K). Otherwise,
absolute values caloric and entropic properties may not be correctly calculated.
Hence, existing models of ideal gases are implemented in this package again, but
their reference points are correctly set. For details of the ideal gas models, 
check the documentation of the package 
<a href=\"Modelica://Modelica.Media.IdealGases.Common.SingleGasNasa\">Modelica.Media.IdealGases.Common.SingleGasNasa</a>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end IdealGases;
