within SorpLib.Media.IdealGases;
package Ne "SorpLib: Ideal gas Ne from NASA Glenn coefficients"
  extends Modelica.Media.IdealGases.SingleGases.Ne(
    final reference_T(min=0) = 0,
    final reference_p = 1e5,
    final reference_X=fill(1/nX, nX));

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This medium model calculates fluid property data of Ne. For details,
check the package
<a href=\"Modelica://Modelica.Media.IdealGases.SingleGases.Ne\">Modelica.Media.IdealGases.SingleGases.Ne</a>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 22, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Ne;
