within SorpLib.Media.IdealGasVaporMixtures;
package Tester "Package containing testers to test and varify models calculating fluid property data of ideal gas mixtures"
  extends Modelica.Icons.ExamplesPackage;

annotation (Documentation(info="<html>
<p>
This package contains executable test models for all implemented fluid
property models of ideal gas-vapor mixtures. 
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Tester;
