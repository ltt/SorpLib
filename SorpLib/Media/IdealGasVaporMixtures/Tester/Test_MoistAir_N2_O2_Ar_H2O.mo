within SorpLib.Media.IdealGasVaporMixtures.Tester;
model Test_MoistAir_N2_O2_Ar_H2O
  "Tester for moist air consisting of N2, O2, Ar, and H2O"
  extends Modelica.Icons.Example;

  //
  // Definition of paramters
  //
  replaceable package Medium =
    SorpLib.Media.IdealGasVaporMixtures.MoistAir_N2_O2_Ar_H2O(s_dryAir_off=-6625)
    constrainedby
    SorpLib.Media.IdealGasVaporMixtures.Interfaces.PartialIdealGasWaterVaporMixture
    "Medium"
    annotation (Dialog(tab="General",group="Models and Media"),
                choicesAllMatching = true);
  package MediumMoistAir =
    Modelica.Media.Air.MoistAir(reference_X={1e-5,1-1e-5})
    "Ideal moist air"
    annotation (Dialog(tab="General",group="Models and Media"));
  package MediumReferenceMoistAir =
    Modelica.Media.Air.ReferenceMoistAir(reference_X={1e-5,1-1e-5})
    "Reference moist air"
    annotation (Dialog(tab="General",group="Models and Media"));

  parameter Modelica.Units.SI.PressureDifference dp = 1e-3
    "Pressure difference used to calculate partial derivatives numerically"
    annotation (Dialog(tab="Numerics", group="Derivatives"),
                Evaluate=true,
                HideResult=true);
  parameter Modelica.Units.SI.TemperatureDifference dT = 1e-3
    "Temperature difference used to calculate partial derivatives numerically"
    annotation (Dialog(tab="Numerics", group="Derivatives"),
                Evaluate=true,
                HideResult=true);
  parameter Real dX = 1e-6
    "Mass fraction difference used to calculate partial derivatives numerically"
    annotation (Dialog(tab="Numerics", group="Derivatives"),
                Evaluate=true,
                HideResult=true);

  //
  // Definition of variables regarding state properties
  //
  Modelica.Units.SI.Pressure p(start=150, fixed=true)
    "Pressure";
  Modelica.Units.SI.Pressure p_solid(start=5, fixed=true)
    "Pressure used to check solid properties";
  Modelica.Units.SI.Pressure p_liquid(start=612, fixed=true)
    "Pressure used to check liquid properties";
  Modelica.Units.SI.Pressure p_vapor(start=150, fixed=true)
    "Pressure used to check vaporous properties";

  Modelica.Units.SI.Temperature T(start=223.15, fixed=true)
    "Temperature";
  Modelica.Units.SI.Temperature T_solid(start=50, fixed=true)
    "Temperature used to check solid properties";
  Modelica.Units.SI.Temperature T_liquid(start=273.16, fixed=true)
    "Temperature used to check liquid properties";
  Modelica.Units.SI.Temperature T_vapor(start=200, fixed=true)
    "Temperature used to check vapor properties";

  Modelica.Units.SI.MassFraction[Medium.nX] X
    "Mass fractions";
  Modelica.Units.SI.MassFraction[MediumMoistAir.nX] X_MoistAir
    "Mass fractions";
  Modelica.Units.SI.MassFraction[MediumReferenceMoistAir.nX] X_ReferenceMoistAir
    "Mass fractions";

  Modelica.Units.SI.MassFraction[Medium.nX] x
    "Mass fractions per dry air mass";
  Modelica.Units.SI.MassFraction[Medium.nX] X_from_x
    "Mass fractions per moist air mass calculated from x";
  Modelica.Units.SI.MassFraction[Medium.nX] X_from_xDryAirPhi
    "Mass fractions per moist air mass calculated from x of dry air components 
    and phi";

  Medium.ThermodynamicState state_pTX
    "State properties calculated with pressure, temperature, and composition";
  Medium.ThermodynamicState state_phX
    "State properties calculated with pressure, specific enthalpy, and composition";
  Medium.ThermodynamicState state_psX
    "State properties calculated with pressure, specific entropy, and composition";
  Medium.ThermodynamicState state_dTX
    "State properties calculated with density, temperature, and composition";

  Medium.BaseProperties medium_pTX
    "State properties";
  MediumMoistAir.BaseProperties mediumMoistAir_pTX
    "Base properties calculated with pressure, temperature, and mass fractions";
  MediumReferenceMoistAir.BaseProperties mediumReferenceMoistAir_pTX
    "Base properties calculated with pressure, temperature, and mass fractions";

  Modelica.Units.SI.MassFraction x_sat
    "Saturation uptake per unit of dry air";
  Modelica.Units.SI.MassFraction x_sat_MoistAir
    "Saturation uptake per unit of dry air";
  Modelica.Units.SI.MassFraction x_sat_ReferenceMoistAir
    "Saturation uptake per unit of dry air";

  Modelica.Units.SI.MassFraction X_sat
    "Saturation uptake per unit of moist air";
  Modelica.Units.SI.MassFraction X_sat_MoistAir
    "Saturation uptake per unit of moist air";
  Modelica.Units.SI.MassFraction X_sat_ReferenceMoistAir
    "Saturation uptake per unit of moist air";

  Modelica.Units.SI.SpecificEnthalpy h_1
    "Specific enthalpy of component 1";
  Modelica.Units.SI.SpecificEnthalpy h_last
    "Specific enthalpy of last component";

  Modelica.Units.SI.SpecificEntropy s
    "Specific entropy";
  Modelica.Units.SI.SpecificEntropy s_1
    "Specific entropy of component 1";
  Modelica.Units.SI.SpecificEntropy s_last
    "Specific entropy of last component";
  Modelica.Units.SI.SpecificEntropy s_MoistAir
    "Specific entropy";
  Modelica.Units.SI.SpecificEntropy s_ReferenceMoistAir
    "Specific entropy";

  //
  // Definition of variables regarding additional properties
  //
  Modelica.Units.SI.Pressure[Medium.nX] p_i
    "Partial pressures";
  Modelica.Units.SI.Pressure p_water_ReferenceMoistAir
    "Partial pressure of water";

  Modelica.Media.Interfaces.Types.DynamicViscosity eta
    "Dynamic viscosity";
  Modelica.Media.Interfaces.Types.DynamicViscosity eta_MoistAir
    "Dynamic viscosity";
  Modelica.Media.Interfaces.Types.DynamicViscosity eta_ReferenceMoistAir
    "Dynamic viscosity";

  Modelica.Media.Interfaces.Types.ThermalConductivity lambda
    "Thermal conductivity";
  Modelica.Media.Interfaces.Types.ThermalConductivity lambda_MoistAir
    "Thermal conductivity";
  Modelica.Media.Interfaces.Types.ThermalConductivity lambda_ReferenceMoistAir
    "Thermal conductivity";

  Modelica.Units.SI.SpecificHeatCapacity cp
    "Specific heat capacity";
  Modelica.Units.SI.SpecificHeatCapacity cp_MoistAir
    "Specific heat capacity";
  Modelica.Units.SI.SpecificHeatCapacity cp_ReferenceMoistAir
    "Specific heat capacity";

  Modelica.Units.SI.SpecificHeatCapacity cv
    "Specific heat capacity";
  Modelica.Units.SI.SpecificHeatCapacity cv_MoistAir
    "Specific heat capacity";
  Modelica.Units.SI.SpecificHeatCapacity cv_ReferenceMoistAir
    "Specific heat capacity";

  Modelica.Media.Interfaces.Types.IsentropicExponent gamma
    "Isentropic coefficient";
  Modelica.Media.Interfaces.Types.IsentropicExponent gamma_MoistAir
    "Isentropic coefficient";
  Modelica.Media.Interfaces.Types.IsentropicExponent gamma_ReferenceMoistAir
    "Isentropic coefficient";

  Modelica.Units.SI.SpecificEnthalpy h_is
    "Isentropic specific enthalpy";
  Modelica.Units.SI.SpecificEnthalpy h_is_MoistAir
    "Isentropic specific enthalpy";
  Modelica.Units.SI.SpecificEnthalpy h_is_ReferenceMoistAir
    "Isentropic specific enthalpy";

  Modelica.Media.Interfaces.Types.VelocityOfSound w_is
    "Velocity of sound";
  Modelica.Media.Interfaces.Types.VelocityOfSound w_is_MoistAir
    "Velocity of sound";
  Modelica.Media.Interfaces.Types.VelocityOfSound w_is_ReferenceMoistAir
    "Velocity of sound";

  Modelica.Units.SI.RelativePressureCoefficient beta
    "Isobaric expansion coefficient";
  Modelica.Units.SI.RelativePressureCoefficient beta_MoistAir
    "Isobaric expansion coefficient";

  Modelica.Units.SI.IsothermalCompressibility kappa
    "Isothermal compressibility";
  Modelica.Units.SI.IsothermalCompressibility kappa_MoistAir
    "Isothermal compressibility";

  Modelica.Units.SI.SpecificEnthalpy h_dryAir
    "Specific enthalpy of non-condensing components calculated for T_vapor";
  Modelica.Units.SI.SpecificEnthalpy h_dryAir_MoistAir
    "Specific enthalpy of liquid water calculated for T_vapor";
  Modelica.Units.SI.SpecificEnthalpy h_dryAir_ReferenceMoistAir
    "Specific enthalpy of non-condensing components calculated for T_vapor and 
    p_vapor";

  Modelica.Units.SI.SpecificEnthalpy h_dryAirVapor
    "Specific enthalpy of non-condensing components and vapor";
  Modelica.Units.SI.SpecificEnthalpy h_dryAirVapor_MoistAir
    "Specific enthalpy of non-condensing components and vapor";
  Modelica.Units.SI.SpecificEnthalpy h_dryAirVapor_ReferenceMoistAir
    "Specific enthalpy of non-condensing components and vapor";

  //
  // Definition of variables regarding partial derivatives
  //
  SorpLib.Units.DerSpecificVolumeByPressure dv_dp_TX
    "Partial derivative of specific volume w.r.t. pressure at constant
    temperature and mass fractions";
  SorpLib.Units.DerSpecificVolumeByPressure dv_dp_TX_num
    "Partial derivative of specific volume w.r.t. pressure at constant
    temperature and mass fractions calculated numerically";

  SorpLib.Units.DerSpecificVolumeByTemperature dv_dT_pX
    "Partial derivative of specific volume w.r.t. temperature  at constant
    pressure and mass fractions";
  SorpLib.Units.DerSpecificVolumeByTemperature dv_dT_pX_num
    "Partial derivative of specific volume w.r.t. temperature  at constant
    pressure and mass fractions calculated numerically";

  Real[Medium.nX] dv_dX_pT(each unit="m3/kg")
    "Partial derivatives of specific volume w.r.t. mass fractions  at constant
    pressure and temperature";
  Real[Medium.nX] dv_dX_pT_num(each unit="m3/kg")
    "Partial derivatives of specific volume w.r.t. mass fractions  at constant
    pressure and temperature calculated numerically";

  SorpLib.Units.DerSpecificEnthalpyByPressure dh_dp_TX
    "Partial derivative of specific enthalpy w.r.t. pressure at constant
    temperature and mass fractions";
  SorpLib.Units.DerSpecificEnthalpyByPressure dh_dp_TX_num
    "Partial derivative of specific enthalpy w.r.t. pressure at constant
    temperature and mass fractions calculated numerically";

  Modelica.Units.SI.SpecificHeatCapacity dh_dT_pX
    "Partial derivative of specific enthalpy w.r.t. temperature at constant
    pressure and mass fractions";
  Modelica.Units.SI.SpecificHeatCapacity dh_dT_pX_num
    "Partial derivative of specific enthalpy w.r.t. temperature at constant
    pressure and mass fractions calculated numerically";

  Real[Medium.nX] dh_dX_pT(each unit="J/kg")
    "Partial derivative of specific enthalpy w.r.t. mass fractions at constant
    pressure and temperature";
  Real[Medium.nX] dh_dX_pT_num(each unit="J/kg")
    "Partial derivative of specific enthalpy w.r.t. mass fractions at constant
    pressure and temperature calculated numerically";

  Modelica.Media.Interfaces.Types.DerDensityByPressure drho_dp_hX
    "Partial derivative of density w.r.t. pressure at constant specific enthalpy
    and mass fractions";
  Modelica.Media.Interfaces.Types.DerDensityByPressure drho_dp_hX_MoistAir
    "Partial derivative of density w.r.t. pressure at constant specific enthalpy
    and mass fractions";

  Modelica.Media.Interfaces.Types.DerDensityByEnthalpy drho_dh_pX
    "Partial derivative of density w.r.t. specific enthalpy at constant pressure
    and mass fractions";
  Modelica.Media.Interfaces.Types.DerDensityByEnthalpy drho_dh_pX_MoistAir
    "Partial derivative of density w.r.t. specific enthalpy at constant pressure
    and mass fractions";

  Real[Medium.nX] drho_dX_ph(each unit="kg.kg/(m3.kg)")
    "Partial derivative of density w.r.t. mass fractions at constant pressure
    and specific enthalpy";
  Real[Medium.nX] drho_dX_ph_num(each unit="kg.kg/(m3.kg)")
    "Partial derivative of density w.r.t. mass fractions at constant pressure
    and specific enthalpy calculated numerically";

  Modelica.Media.Interfaces.Types.DerDensityByPressure drho_dp_TX
    "Partial derivative of density w.r.t. pressure at constant temperature and
    mass fractions";
  Modelica.Media.Interfaces.Types.DerDensityByPressure drho_dp_TX_MoistAir
    "Partial derivative of density w.r.t. pressure at constant temperature and
    mass fractions";

  Modelica.Media.Interfaces.Types.DerDensityByTemperature drho_dT_pX
    "Partial derivative of density w.r.t. pressure at constant temperature and
    mass fractions";
  Modelica.Media.Interfaces.Types.DerDensityByTemperature drho_dT_pX_MoistAir
    "Partial derivative of density w.r.t. pressure at constant temperature and
    mass fractions";

  Real[Medium.nX] drho_dX_pT(each unit="kg.kg/(m3.kg)")
    "Partial derivative of density w.r.t. mass fractions at constant pressure
    and temperature";
  Real[Medium.nX] drho_dX_pT_num(each unit="kg.kg/(m3.kg)")
    "Partial derivative of density w.r.t. mass fractions at constant pressure
    and temperature calculated numerically";

  //
  // Definition of variables regarding condensing component
  //
  Modelica.Units.SI.Pressure p_sat
    "Saturation pressure of water";
  Modelica.Units.SI.Pressure p_sat_MoistAir
    "Saturation pressure of water";
  Modelica.Units.SI.Pressure p_sat_ReferenceMoistAir
    "Saturation pressure of water";

  Modelica.Units.SI.Temperature T_sat_p_sat
    "Saturation temperature of water at saturation pressure of water";

  Modelica.Units.SI.SpecificHeatCapacity c_solid
    "Specific heat capacity of ice calculated fot T_solid";
  Modelica.Units.SI.SpecificHeatCapacity c_liquid
    "Specific heat capacity of ice calculated fot T_liquid";
  Modelica.Units.SI.SpecificHeatCapacity cp_vapor
    "Specific heat capacity of vapor calculated for T_vapor";

  Modelica.Units.SI.SpecificEnthalpy dh_vap
    "Specific enthalpy of vaporization calculated for T_liquid";
  Modelica.Units.SI.SpecificEnthalpy dh_vap_MoistAir
    "Specific enthalpy of vaporization calculated for T_liquid";
  Modelica.Units.SI.SpecificEnthalpy dh_vap_ReferenceMoistAir
    "Specific enthalpy of vaporization calculated for T_liquid and p_liquid";

  Modelica.Units.SI.SpecificEnthalpy h_solid
    "Specific enthalpy of solid water calculated for T_solid and p_solid";
  Modelica.Units.SI.SpecificEnthalpy h_solid_MoistAir
    "Specific enthalpy of solid water calculated for T_solid";
  Modelica.Units.SI.SpecificEnthalpy h_solid_ReferenceMoistAir
    "Specific enthalpy of solid water calculated for T_solid and p_solid";

  Modelica.Units.SI.SpecificEnthalpy h_liquid
    "Specific enthalpy of liquid water calculated for T_liquid and p_liquid";
  Modelica.Units.SI.SpecificEnthalpy h_liquid_MoistAir
    "Specific enthalpy of liquid water calculated for T_liquid";
  Modelica.Units.SI.SpecificEnthalpy h_liquid_ReferenceMoistAir
    "Specific enthalpy of liquid water calculated for T_liquid and p_liquid";

  Modelica.Units.SI.SpecificEnthalpy h_vapor
    "Specific enthalpy of vaporous water calculated for T_vapor";
  Modelica.Units.SI.SpecificEnthalpy h_vapor_MoistAir
    "Specific enthalpy of vaporous water calculated for T_vapor";
  Modelica.Units.SI.SpecificEnthalpy h_vapor_ReferenceMoistAir
    "Specific enthalpy of vaporous water calculated for T_vapor and p_vapor";

  Modelica.Units.SI.SpecificEnthalpy h_water
    "Specific enthalpy of whole water";
  Modelica.Units.SI.SpecificEnthalpy h_solidLiquid_water
    "Specific enthalpy of solid or liquid water";
  Modelica.Units.SI.SpecificEnthalpy h_vapor_water
    "Specific enthalpy of vaporous water";

  //
  // Definition of protected variables
  //
protected
  Modelica.Units.SI.MassFraction X_water
    "Mass fraction of water";
  Modelica.Units.SI.MassFraction X_dryAir
    "Mass fraction of dry air (i.e., without water)";

  Medium.ThermodynamicState state_pdp
    "Thermodynamic state: p + dp";
  Medium.ThermodynamicState state_mdp
    "Thermodynamic state: p - dp";

  Medium.ThermodynamicState state_pdT
    "Thermodynamic state: T + dT";
  Medium.ThermodynamicState state_mdT
    "Thermodynamic state: T - dT";

  //
  // Definitio of parameters correcting reference states
  //
  parameter Modelica.Units.SI.SpecificEnthalpy dh_MoistAir=
    Medium.specificEnthalpy(
      state=Medium.ThermodynamicState(
        p=Medium.p_water_ref,
        T=Medium.T_water_ref,
        X=Medium.reference_X,
        Y=Medium.massToMoleFractions(X=Medium.reference_X, MMX=Medium.MMX))) -
    MediumMoistAir.specificEnthalpy(
      state=MediumMoistAir.ThermodynamicState(
        p=Medium.p_water_ref,
        T=Medium.T_water_ref,
        X={Medium.reference_X[Medium.nX],1-Medium.reference_X[Medium.nX]}))
    "Difference of reference points: Ideal gas-vapor mixture";
  parameter Modelica.Units.SI.SpecificEnthalpy dh_ReferenceMoistAir=
    Medium.specificEnthalpy(
      state=Medium.ThermodynamicState(
        p=Medium.p_water_ref,
        T=Medium.T_water_ref,
        X=Medium.reference_X,
        Y=Medium.massToMoleFractions(X=Medium.reference_X, MMX=Medium.MMX))) -
    MediumReferenceMoistAir.specificEnthalpy(
      state=MediumReferenceMoistAir.ThermodynamicState(
        p=Medium.p_water_ref,
        T=Medium.T_water_ref,
        X={Medium.reference_X[Medium.nX],1-Medium.reference_X[Medium.nX]}))
    "Difference of reference points: Ideal gas-vapor mixture";

  parameter Modelica.Units.SI.SpecificEntropy ds_MoistAir=
    Medium.specificEntropy(
      state=Medium.ThermodynamicState(
        p=Medium.p_water_ref,
        T=Medium.T_water_ref,
        X=Medium.reference_X,
        Y=Medium.massToMoleFractions(X=Medium.reference_X, MMX=Medium.MMX))) -
    MediumMoistAir.specificEntropy(
      state=MediumMoistAir.ThermodynamicState(
        p=Medium.p_water_ref,
        T=Medium.T_water_ref,
        X={Medium.reference_X[Medium.nX],1-Medium.reference_X[Medium.nX]}))
    "Difference of reference points: Ideal gas-vapor mixture";
  parameter Modelica.Units.SI.SpecificEntropy ds_ReferenceMoistAir=
    Medium.specificEntropy(
      state=Medium.ThermodynamicState(
        p=Medium.p_water_ref,
        T=Medium.T_water_ref,
        X=Medium.reference_X,
        Y=Medium.massToMoleFractions(X=Medium.reference_X, MMX=Medium.MMX))) -
    MediumReferenceMoistAir.specificEntropy(
      state=MediumReferenceMoistAir.ThermodynamicState(
        p=Medium.p_water_ref,
        T=Medium.T_water_ref,
        X={Medium.reference_X[Medium.nX],1-Medium.reference_X[Medium.nX]}))
    "Difference of reference points: Ideal gas-vapor mixture";

  parameter Modelica.Units.SI.SpecificEnthalpy dh_dryAir_MoistAir=
    Medium.enthalpyOfNonCondensingGas(
      T=Medium.T_water_ref,
      X=Medium.reference_X) -
    MediumMoistAir.enthalpyOfNonCondensingGas(
      T=Medium.T_water_ref)
    "Difference of reference points: Ideal gas mixture of dry gas";
  parameter Modelica.Units.SI.SpecificEnthalpy dh_dryAir_ReferenceMoistAir=
    Medium.enthalpyOfNonCondensingGas(
      T=Medium.T_water_ref,
      X=Medium.reference_X) -
    MediumReferenceMoistAir.enthalpyOfNonCondensingGas(
      state=MediumReferenceMoistAir.ThermodynamicState(
        p=Medium.p_water_ref,
        T=Medium.T_water_ref,
        X={Medium.reference_X[Medium.nX],1-Medium.reference_X[Medium.nX]}))
    "Difference of reference points: Ideal gas mixture of dry gas";

equation
  //
  // Change independent state variables
  //
  der(p) = (90e5 - 150) / 20
    "Predescribed slope of pressure";
  der(p_solid) = (90e5 - 5) / 20
    "Predescribed slope of pressure used to check solid properties";
  der(p_liquid) = (90e5 - 612) / 20
    "Predescribed slope of pressure used to check liquid properties";
  der(p_vapor) = (90e5 - 150) / 20
    "Predescribed slope of pressure used to check vaporous properties";

  der(T) = (563.15 - 223.15) / 20
    "Predescribed slope of temperature";
  der(T_solid) = (273.16 - 50) / 20
    "Predescribed slope of temperature used to check solid properties";
  der(T_liquid) = (573.15 - 273.16) / 20
    "Predescribed slope of temperature used to check liquid properties";
  der(T_vapor) = (1000 - 200) / 20
    "Predescribed slope of temperature used to check vapor properties";

  X_water = 0.5 + 0.5 * Modelica.Math.sin(2*Modelica.Constants.pi * 1/5 * time)
    "Mass fraction of water";
  X_dryAir = 1 - X_water
    "Mass fraction of dry air (i.e., without water)";

  X = cat(
    1,
    Medium.reference_X[1:end-1] ./ sum(Medium.reference_X[1:end-1]) * X_dryAir,
    {X_water})
    "Mass fractions";
  X_MoistAir =  {X_water, X_dryAir}
    "Mass fractions";
  X_ReferenceMoistAir = {X_water, X_dryAir}
    "Mass fractions";

  x = Medium.moistAirToDryAirMassFractions(X=X)
    "Mass fractions per dry air mass";
  X_from_x = Medium.dryAirToMoistAirMassFractions(x=x)
    "Mass fractions per moist air mass calculated from x";
  X_from_xDryAirPhi =
    Medium.massFractions_pTxDryPhi(p=p, T=T, x=x[1:end-1], phi=medium_pTX.phi)
    "Mass fractions per moist air mass calculated from x of dry air components 
    and phi";

  //
  // Calculate state variables
  //
  state_pdp = Medium.setState_pTX(p=p+dp, T=T, X=X)
    "Thermodynamic state: p + dp";
  state_mdp = Medium.setState_pTX(p=p-dp, T=T, X=X)
    "Thermodynamic state: p - dp";

  state_pdT = Medium.setState_pTX(p=p, T=T+dT, X=X)
    "Thermodynamic state: T + dT";
  state_mdT = Medium.setState_pTX(p=p, T=T-dT, X=X)
    "Thermodynamic state: T - dT";

  state_pTX = Medium.setState_pTX(p=p, T=T, X=X)
    "State properties calculated with pressure, temperature, and composition";
  state_phX = Medium.setState_phX(p=p, h=medium_pTX.h, X=X)
    "State properties calculated with pressure, specific enthalpy, and composition";
  state_psX = Medium.setState_psX(p=p, s=s, X=X)
    "State properties calculated with pressure, specific entropy, and composition";
  state_dTX = Medium.setState_dTX(d=medium_pTX.d, T=T, X=X)
    "State properties calculated with density, temperature, and composition";

  medium_pTX.p = p
    "Base properties calculated with pressure and temperature";
  medium_pTX.T = T
    "Base properties calculated with pressure and temperature";
  medium_pTX.X[1:end-1] = X[1:end-1]
    "Base properties calculated with pressure and temperature";

  mediumMoistAir_pTX.p = p
    "Base properties calculated with pressure and temperature";
  mediumMoistAir_pTX.T = T
    "Base properties calculated with pressure and temperature";
  mediumMoistAir_pTX.X[1] = X_MoistAir[1]
    "Base properties calculated with pressure and temperature";

  mediumReferenceMoistAir_pTX.p = p
    "Base properties calculated with pressure and temperature";
  mediumReferenceMoistAir_pTX.T = T
    "Base properties calculated with pressure and temperature";
  mediumReferenceMoistAir_pTX.X[1] = X_ReferenceMoistAir[1]
    "Base properties calculated with pressure and temperature";

  x_sat =
    Medium.dryMassFractionSaturation(state=medium_pTX.state)
    "Saturation uptake per unit of dry air";
  x_sat_MoistAir =
    MediumMoistAir.xsaturation(state=mediumMoistAir_pTX.state)
    "Saturation uptake per unit of dry air";
  x_sat_ReferenceMoistAir=
    MediumReferenceMoistAir.xsaturation(
      state=mediumReferenceMoistAir_pTX.state)
    "Saturation uptake per unit of dry air";

  X_sat =
    Medium.massFractionSaturation(state=medium_pTX.state)
    "Saturation uptake per unit of moist air";
  X_sat_MoistAir =
    MediumMoistAir.Xsaturation(state=mediumMoistAir_pTX.state)
    "Saturation uptake per unit of moist air";
  X_sat_ReferenceMoistAir=
    MediumReferenceMoistAir.Xsaturation(
      state=mediumReferenceMoistAir_pTX.state)
    "Saturation uptake per unit of moist air";

  h_1 =
    Medium.specificEnthalpy_i(ind_component=1, state=medium_pTX.state)
    "Specific enthalpy of component 1";
  h_last =
    Medium.specificEnthalpy_i(ind_component=Medium.nX, state=medium_pTX.state)
    "Specific enthalpy of last component";

  s =
    Medium.specificEntropy(state=medium_pTX.state)
    "Specific entropy";
  s_1 =
    Medium.specificEntropy_i(ind_component=1, state=medium_pTX.state)
    "Specific entropy of component 1";
  s_last =
    Medium.specificEntropy_i(ind_component=Medium.nX, state=medium_pTX.state)
    "Specific entropy of last component";
  s_MoistAir =
    MediumMoistAir.specificEntropy(state=mediumMoistAir_pTX.state) +
    ds_MoistAir
    "Specific entropy";
  s_ReferenceMoistAir =
    MediumReferenceMoistAir.specificEntropy(
      state=mediumReferenceMoistAir_pTX.state) +
    ds_ReferenceMoistAir
    "Specific entropy";

  //
  // Calculate further properties
  //
  p_i = Medium.partialPressures(state=medium_pTX.state)
    "Partial pressures";
  p_water_ReferenceMoistAir = MediumReferenceMoistAir.Utilities.pd_pTX(
    p=mediumReferenceMoistAir_pTX.state.p,
    T=mediumReferenceMoistAir_pTX.state.T,
    X=mediumReferenceMoistAir_pTX.state.X)
    "Partial pressure of water";

  eta =
    Medium.dynamicViscosity(state=medium_pTX.state)
    "Dynamic viscosity";
  eta_MoistAir =
    MediumMoistAir.dynamicViscosity(state=mediumMoistAir_pTX.state)
    "Dynamic viscosity";
  eta_ReferenceMoistAir =
    MediumReferenceMoistAir.dynamicViscosity(
      state=mediumReferenceMoistAir_pTX.state)
    "Dynamic viscosity";

  lambda =
    Medium.thermalConductivity(state=medium_pTX.state)
    "Thermal conductivity";
  lambda_MoistAir =
    MediumMoistAir.thermalConductivity(state=mediumMoistAir_pTX.state)
    "Thermal conductivity";
  lambda_ReferenceMoistAir =
    MediumReferenceMoistAir.thermalConductivity(
      state=mediumReferenceMoistAir_pTX.state)
    "Thermal conductivity";

  cp =
    Medium.specificHeatCapacityCp(state=medium_pTX.state)
    "Specific heat capacity";
  cp_MoistAir =
    MediumMoistAir.specificHeatCapacityCp(state=mediumMoistAir_pTX.state)
    "Specific heat capacity";
  cp_ReferenceMoistAir =
    MediumReferenceMoistAir.specificHeatCapacityCp(
      state=mediumReferenceMoistAir_pTX.state)
    "Specific heat capacity";

  cv =
    Medium.specificHeatCapacityCv(state=medium_pTX.state)
    "Specific heat capacity";
  cv_MoistAir =
    MediumMoistAir.specificHeatCapacityCv(state=mediumMoistAir_pTX.state)
    "Specific heat capacity";
  cv_ReferenceMoistAir =
    MediumReferenceMoistAir.specificHeatCapacityCv(
      state=mediumReferenceMoistAir_pTX.state)
    "Specific heat capacity";

  gamma =
    Medium.isentropicExponent(state=medium_pTX.state)
    "Isentropic coefficient";
  gamma_MoistAir=
    MediumMoistAir.isentropicExponent(state=mediumMoistAir_pTX.state)
    "Isentropic coefficient";
  gamma_ReferenceMoistAir=
    MediumReferenceMoistAir.isentropicExponent(
      state=mediumReferenceMoistAir_pTX.state)
    "Isentropic coefficient";

  h_is =
    Medium.isentropicEnthalpy(
      p_downstream=p, refState=medium_pTX.state)
    "Isentropic specific enthalpy";
  h_is_MoistAir =
    MediumMoistAir.isentropicEnthalpy(
      p_downstream=p, refState=mediumMoistAir_pTX.state) +
    dh_MoistAir
    "Isentropic specific enthalpy";
  h_is_ReferenceMoistAir =
    MediumReferenceMoistAir.isentropicEnthalpy(
      p_downstream=p, refState=mediumReferenceMoistAir_pTX.state)+
    dh_ReferenceMoistAir
    "Isentropic specific enthalpy";

  w_is =
    Medium.velocityOfSound(
      state=medium_pTX.state)
    "Velocity of sound";
  w_is_MoistAir =
    MediumMoistAir.velocityOfSound(
      state=mediumMoistAir_pTX.state)
    "Velocity of sound";
  w_is_ReferenceMoistAir =
    MediumReferenceMoistAir.velocityOfSound(
      state=mediumReferenceMoistAir_pTX.state)
    "Velocity of sound";

  beta =
    Medium.beta(state=medium_pTX.state)
    "Isobaric expansion coefficient";
  beta_MoistAir =
    MediumMoistAir.beta(state=mediumMoistAir_pTX.state)
    "Isobaric expansion coefficient";

  kappa =
    Medium.kappa(state=medium_pTX.state)
    "Isothermal compressibility";
  kappa_MoistAir =
    MediumMoistAir.kappa(state=mediumMoistAir_pTX.state)
    "Isothermal compressibility";

  h_dryAir =
    Medium.enthalpyOfNonCondensingGas(
      T=T_vapor,
      X=X)
    "Specific enthalpy of non-condensing components calculated for T_vapor";
  h_dryAir_MoistAir =
    MediumMoistAir.enthalpyOfNonCondensingGas(T=T_vapor) +
    dh_dryAir_MoistAir
    "Specific enthalpy of liquid water calculated for T_vapor";
  h_dryAir_ReferenceMoistAir =
    MediumReferenceMoistAir.enthalpyOfNonCondensingGas(
      state=MediumReferenceMoistAir.ThermodynamicState(
        p=p_vapor,
        T=T_vapor,
        X=X_ReferenceMoistAir)) +
    dh_dryAir_ReferenceMoistAir
    "Specific enthalpy of non-condensing components calculated for T_vapor and 
    p_vapor";

  h_dryAirVapor =
    Medium.enthalpyOfGas(state=medium_pTX.state)
    "Specific enthalpy of non-condensing components and vapor";
  h_dryAirVapor_MoistAir =
    MediumMoistAir.enthalpyOfGas(
      T=T,
      X=X_MoistAir) +
    dh_MoistAir
    "Specific enthalpy of non-condensing components and vapor";
  h_dryAirVapor_ReferenceMoistAir =
    MediumReferenceMoistAir.enthalpyOfGas(
      state=mediumReferenceMoistAir_pTX.state) /
    (1 + X_ReferenceMoistAir[1] / (1 - X_ReferenceMoistAir[1])) +
    dh_ReferenceMoistAir
    "Specific enthalpy of non-condensing components and vapor";

  //
  // Calculate partial derivatives
  //
  dv_dp_TX = Medium.dv_dp_TX(state=medium_pTX.state)
    "Partial derivative of specific volume w.r.t. pressure at constant
    temperature and mass fractions";
  dv_dp_TX_num = -1/medium_pTX.d^2 *
    ((Medium.density(state=state_pdp) -
    Medium.density(state=state_mdp)) / (2*dp))
    "Partial derivative of specific volume w.r.t. pressure at constant
    temperature and mass fractions calculated numerically";

  dv_dT_pX = Medium.dv_dT_pX(state=medium_pTX.state)
    "Partial derivative of specific volume w.r.t. temperature  at constant
    pressure and mass fractions";
  dv_dT_pX_num = -1/medium_pTX.d^2 *
    ((Medium.density(state=state_pdT) -
    Medium.density(state=state_mdT)) / (2*dT))
    "Partial derivative of specific volume w.r.t. temperature  at constant
    pressure and mass fractions calculated numerically";

  dv_dX_pT = Medium.dv_dX_pT(state=medium_pTX.state)
    "Partial derivatives of specific volume w.r.t. mass fractions at constant
    pressure and temperature";
  for ind in 1:Medium.nX loop
    dv_dX_pT_num[ind] = -1/medium_pTX.d^2 *
      ((Medium.density(
        state=Medium.setState_pTX(
          p=p,
          T=T,
          X=cat(1,
            X[1:ind-1],
            {X[ind] + dX},
            X[ind+1:Medium.nX]))) -
      Medium.density(
        state=Medium.setState_pTX(
          p=p,
          T=T,
          X=cat(1,
            X[1:ind-1],
            {X[ind] - dX},
            X[ind+1:Medium.nX])))) / (2*dX))
    "Partial derivatives of specific volume w.r.t. mass fractions  at constant
    pressure and temperature calculated numerically";
  end for;

  dh_dp_TX = Medium.dh_dp_TX(state=medium_pTX.state)
    "Partial derivative of specific enthalpy w.r.t. pressure at constant
    temperature and mass fractions";
  dh_dp_TX_num = (Medium.specificEnthalpy(state=state_pdp) -
    Medium.specificEnthalpy(state=state_mdp)) / (2*dp)
    "Partial derivative of specific enthalpy w.r.t. pressure at constant
    temperature and mass fractions calculated numerically";

  dh_dT_pX = Medium.dh_dT_pX(state=medium_pTX.state)
    "Partial derivative of specific enthalpy w.r.t. temperature at constant
    pressure and mass fractions";
  dh_dT_pX_num = (Medium.specificEnthalpy(state=state_pdT) -
    Medium.specificEnthalpy(state=state_mdT)) / (2*dT)
    "Partial derivative of specific enthalpy w.r.t. temperature at constant
    pressure and mass fractions calculated numerically";

  dh_dX_pT = Medium.dh_dX_pT(state=medium_pTX.state)
  "Partial derivative of specific enthalpy w.r.t. mass fractions at constant
  pressure and temperature";
  for ind in 1:Medium.nX loop
    dh_dX_pT_num[ind] =
      (Medium.specificEnthalpy(
        state=Medium.setState_pTX(
          p=p,
          T=T,
          X=cat(1,
            X[1:ind-1],
            {X[ind] + dX},
            X[ind+1:Medium.nX]))) -
      Medium.specificEnthalpy(
        state=Medium.setState_pTX(
          p=p,
          T=T,
          X=cat(1,
            X[1:ind-1],
            {X[ind] - dX},
            X[ind+1:Medium.nX]))))/ (2*dX)
    "Partial derivative of specific enthalpy w.r.t. mass fractions at constant
    pressure and temperature calculated numerically";
  end for;

  drho_dp_hX =
    Medium.density_derp_h(state=medium_pTX.state)
    "Partial derivative of density w.r.t. pressure at constant specific enthalpy
    and mass fractions";
  drho_dp_hX_MoistAir =
    MediumMoistAir.density_derp_h(state=mediumMoistAir_pTX.state)
    "Partial derivative of density w.r.t. pressure at constant specific enthalpy
    and mass fractions";

  drho_dh_pX =
    Medium.density_derh_p(state=medium_pTX.state)
    "Partial derivative of density w.r.t. specific enthalpy at constant pressure
    and mass fractions";
  drho_dh_pX_MoistAir =
    MediumMoistAir.density_derh_p(state=mediumMoistAir_pTX.state)
    "Partial derivative of density w.r.t. specific enthalpy at constant pressure
    and mass fractions";

  drho_dX_ph = Medium.drho_dX_ph(state=medium_pTX.state)
    "Partial derivative of density w.r.t. mass fractions at constant pressure
    and specific enthalpy";
  for ind in 1:Medium.nX loop
    drho_dX_ph_num[ind] =
      ((Medium.density(
        state=Medium.setState_phX(
          p=p,
          h=medium_pTX.h,
          X=cat(1,
            X[1:ind-1],
            {X[ind] + dX},
            X[ind+1:Medium.nX]))) -
      Medium.density(
        state=Medium.setState_phX(
          p=p,
          h=medium_pTX.h,
          X=cat(1,
            X[1:ind-1],
            {X[ind] - dX},
            X[ind+1:Medium.nX])))) / (2*dX))
      "Partial derivative of density w.r.t. mass fractions at constant pressure
      and specific enthalpy calculated numerically";
  end for;

  drho_dp_TX =
    Medium.density_derp_T(state=medium_pTX.state)
    "Partial derivative of density w.r.t. pressure at constant temperature and
    mass fractions";
  drho_dp_TX_MoistAir =
    MediumMoistAir.density_derp_T(state=mediumMoistAir_pTX.state)
    "Partial derivative of density w.r.t. pressure at constant temperature and
    mass fractions";

  drho_dT_pX =
    Medium.density_derT_p(state=medium_pTX.state)
    "Partial derivative of density w.r.t. pressure at constant temperature and
    mass fractions";
  drho_dT_pX_MoistAir=
    MediumMoistAir.density_derT_p(state=mediumMoistAir_pTX.state)
    "Partial derivative of density w.r.t. pressure at constant temperature and
    mass fractions";

  drho_dX_pT =
    Medium.density_derX(state=medium_pTX.state)
    "Partial derivative of density w.r.t. mass fractions at constant pressure
    and temperature";
  for ind in 1:Medium.nX loop
    drho_dX_pT_num[ind] =
      ((Medium.density(
        state=Medium.setState_pTX(
          p=p,
          T=T,
          X=cat(1,
            X[1:ind-1],
            {X[ind] + dX},
            X[ind+1:Medium.nX]))) -
      Medium.density(
        state=Medium.setState_pTX(
          p=p,
          T=T,
          X=cat(1,
            X[1:ind-1],
            {X[ind] - dX},
            X[ind+1:Medium.nX])))) / (2*dX))
    "Partial derivatives of density w.r.t. mass fractions at constant
    pressure and temperature calculated numerically";
  end for;

  //
  // Calculate properties regarding condensing component
  //
  p_sat =
    Medium.saturationPressure(Tsat=T)
    "Saturation pressure of water";
  p_sat_MoistAir =
    MediumMoistAir.saturationPressure(Tsat=T)
    "Saturation pressure of water";
  p_sat_ReferenceMoistAir=
    MediumReferenceMoistAir.saturationPressure(
      state=mediumReferenceMoistAir_pTX.state)
    "Saturation pressure of water";

  T_sat_p_sat =
    Medium.saturationTemperature(p_sat=p_sat)
    "Saturation temperature of water at saturation pressure of water";

  c_solid =
    Medium.specificHeatCapacitySolid(state=Medium.ThermodynamicState(
      p=p_solid,
      T=T_solid,
      X=medium_pTX.state.X,
      Y=medium_pTX.state.Y))
    "Specific heat capacity of solid calculated for T_solid and p_solid";
  c_liquid =
    Medium.specificHeatCapacityLiquid(state=Medium.ThermodynamicState(
      p=p_liquid,
      T=T_liquid,
      X=medium_pTX.state.X,
      Y=medium_pTX.state.Y))
    "Specific heat capacity of liquid calculated for T_liquid and p_liquid";
  cp_vapor =
    Medium.specificHeatCapacityVapor(state=Medium.ThermodynamicState(
      p=p_vapor,
      T=T_vapor,
      X=medium_pTX.state.X,
      Y=medium_pTX.state.Y))
    "Specific heat capacity of vapor calculated for T_vapor and p_vapor";

  dh_vap =
    Medium.enthalpyOfVaporization(T=T_liquid)
    "Specific enthalpy of vaporization calculated for T_liquid";
  dh_vap_MoistAir =
    MediumMoistAir.enthalpyOfVaporization(T=T_liquid)
    "Specific enthalpy of vaporization calculated for T_liquid";
  dh_vap_ReferenceMoistAir=
    MediumReferenceMoistAir.enthalpyOfVaporization(
      state=MediumReferenceMoistAir.ThermodynamicState(
        p=p_liquid,
        T=T_liquid,
        X=X_ReferenceMoistAir))
    "Specific enthalpy of vaporization calculated for T_liquid and p_liquid";

  h_solid =
    Medium.enthalpyOfSolid(
      p=p_solid,
      T=T_solid)
    "Specific enthalpy of solid water calculated for T_solid and p_solid";
  h_solid_MoistAir =
    MediumMoistAir.enthalpyOfWater(T=T_solid)
    "Specific enthalpy of solid water calculated for T_solid";
  h_solid_ReferenceMoistAir =
    MediumReferenceMoistAir.Utilities.Ice09_Utilities.h_pT(
      p=p_solid,
      T=T_solid)
    "Specific enthalpy of solid water calculated for T_solid and p_solid";

  h_liquid =
    Medium.enthalpyOfLiquid(
      p=p_liquid,
      T=T_liquid)
    "Specific enthalpy of liquid water calculated for T_liquid and p_liquid";
  h_liquid_MoistAir =
    MediumMoistAir.enthalpyOfWater(T=T_liquid)
    "Specific enthalpy of liquid water calculated for T_liquid";
  h_liquid_ReferenceMoistAir=
    Modelica.Media.Water.IF97_Utilities.h_pT(
      p=p_liquid,
      T=T_liquid,
      region=1)
    "Specific enthalpy of liquid water calculated for T_liquid and p_liquid";

  h_vapor =
    Medium.enthalpyOfCondensingGas(T=T_vapor)
    "Specific enthalpy of vaporous water calculated for T_vapor";
  h_vapor_MoistAir =
    MediumMoistAir.enthalpyOfCondensingGas(T=T_vapor)
    "Specific enthalpy of vaporous water calculated for T_vapor";
  h_vapor_ReferenceMoistAir=
    MediumReferenceMoistAir.enthalpyOfCondensingGas(
      state=MediumReferenceMoistAir.ThermodynamicState(
        p=p_vapor,
        T=T_vapor,
        X=X_ReferenceMoistAir)) /
    (X_ReferenceMoistAir[1] / (1 - X_ReferenceMoistAir[1]))
    "Specific enthalpy of vaporous water calculated for T_vapor and p_vapor";

  h_water =
    Medium.enthalpyOfCondensingComponent(state=medium_pTX.state)
    "Specific enthalpy of whole water";
  h_solidLiquid_water =
    Medium.enthalpyOfLiquidIce(state=medium_pTX.state)
    "Specific enthalpy of solid or liquid water";
  h_vapor_water =
    Medium.enthalpyOfVapor(state=medium_pTX.state)
    "Specific enthalpy of vaporous water";

  //
  // Annotations
  //
  annotation (experiment(StopTime=20, Tolerance=1e-06),
Documentation(info="<html>
<p>
This model checks the fluid property calculation of the ideal gas-vapor mixture
of N<sub>2</sub>, O<sub>2</sub>, Ar, and H<sub>2</sub>O.
<br/><br/>
To see the model behavior, plot the variables of the models over the 
time. The simulation time is correctly preset (Start: 0 s, Stop = 20 s). 
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 28, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Test_MoistAir_N2_O2_Ar_H2O;
