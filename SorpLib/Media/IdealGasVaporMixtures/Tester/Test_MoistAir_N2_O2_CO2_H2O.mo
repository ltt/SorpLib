within SorpLib.Media.IdealGasVaporMixtures.Tester;
model Test_MoistAir_N2_O2_CO2_H2O
  "Tester for moist air consisting of N2, O2, CO2, and H2O"
  extends SorpLib.Media.IdealGasVaporMixtures.Tester.Test_MoistAir_N2_O2_Ar_H2O(
    redeclare package Medium =
      SorpLib.Media.IdealGasVaporMixtures.MoistAir_N2_O2_CO2_H2O(s_dryAir_off=-6625));

  //
  // Annotations
  //
  annotation (experiment(StopTime=20, Tolerance=1e-06),
Documentation(info="<html>
<p>
This model checks the fluid property calculation of the ideal gas-vapor mixture
of N<sub>2</sub>, O<sub>2</sub>, CO<sub>2</sub>, and H<sub>2</sub>O.
<br/><br/>
To see the model behavior, plot the variables of the models over the 
time. The simulation time is correctly preset (Start: 0 s, Stop = 20 s). 
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 28, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Test_MoistAir_N2_O2_CO2_H2O;
