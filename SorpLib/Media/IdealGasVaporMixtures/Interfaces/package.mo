within SorpLib.Media.IdealGasVaporMixtures;
package Interfaces "Interfaces for models of ideal gas-vapor mixtures"
extends Modelica.Icons.InterfacesPackage;

annotation (Documentation(info="<html>
<p>
This package provides definitions of basic interfaces for models
of ideal gas-vapor mixtures.
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end Interfaces;
