within SorpLib.Media.IdealGasVaporMixtures.Interfaces.PartialIdealGasWaterVaporMixture;
function specificEntropy_pTXY
  "Returns specific entropy of the ideal gas-vapor mixture"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.Pressure p
    "Pressure"
    annotation (Dialog(tab="General",group="Inputs"));
  input Modelica.Units.SI.Temperature T
    "Pressure"
    annotation (Dialog(tab="General",group="Inputs"));
  input Modelica.Units.SI.MassFraction[nX] X
    "Mass fractions"
    annotation (Dialog(tab="General",group="Inputs"));
  input Modelica.Units.SI.MoleFraction[nX] Y
    "Mole fractions"
    annotation (Dialog(tab="General",group="Inputs"));

  //
  // Definition of outputs
  //
  output Modelica.Units.SI.SpecificEntropy s
    "Specific entropy"
    annotation (Dialog(tab="General",group="Outputs", enable=false));

  //
  // Definition of variables
  //
protected
  Modelica.Units.SI.MassFraction[nX] x=
    moistAirToDryAirMassFractions(X=X)
    "Mass fractions per dry air";
  Modelica.Units.SI.MassFraction x_sat=
    dryMassFractionSaturation(state=ThermodynamicState(p=p, T=T, X=X, Y=Y))
    "Saturation mass fraction of condensing component per dry air";

  Modelica.Units.SI.Pressure[nX] p_i=
    partialPressures(state=ThermodynamicState(p=p, T=T, X=X, Y=Y))
    "Partial pressures";

algorithm
  //
  // Calculate specific enthalpy per dry air mass
  //
  if Modelica.Math.isEqual(s1=x[nX], s2=0, eps=100*Modelica.Constants.eps) then
    s := entropyOfNonCondensingGas(T=T, X=x, p_i=p_i)
      "Specific entropy equals specific entropy of unsaturated dry air without 
      water: Calculated applying the law of ideal gas mixtures.";

  elseif x[nX] <= x_sat then
    s := entropyOfNonCondensingGas(T=T, X=x, p_i=p_i) +
      x[nX] * entropyOfCondensingGas(T=T, p=p_i[nX])
      "Specific entropy equals specific entropy of unsaturated dry air and water 
      vapor: Calculated applying the law of ideal gas mixtures.";

  elseif T > T_water_trp then
    s := entropyOfNonCondensingGas(T=T, X=x, p_i=p_i) +
      x_sat * entropyOfCondensingGas(T=T, p=p_i[nX]) +
      (x[nX]-x_sat) * entropyOfLiquid(T=T)
      "Specific entropy equals specific entropy of saturated dry air, water
      vapor, liquid water: Calculated applying the law of ideal gas mixtures.";

  else
    s := entropyOfNonCondensingGas(T=T, X=x, p_i=p_i) +
      x_sat * entropyOfCondensingGas(T=T, p=p_i[nX]) +
      (x[nX]-x_sat) * entropyOfSolid(T=T)
      "Specific entropy equals specific entropy of saturated dry air, water
      vapor, solid water: Calculated applying the law of ideal gas mixtures.";

  end if;

  //
  // Convert specific enthalpy per dry air mass
  //
  s := s / (1 + x[nX])
    "Specific entropy per moist air mass";

  //
  // Annotations
  //
  annotation (Inline=false, InlineAfterIndexReduction=false, LateInline=true,
  inverse(T=temperature_psXY(p=p, s=s, X=X, Y=Y)), Documentation(info="<html>
<p>
This function calculates the specific entropy of the ideal gas-vapor mixture as 
function of pressure, temperature, mass fractions, and mole fractions.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 27, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end specificEntropy_pTXY;
