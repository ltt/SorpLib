﻿within SorpLib.Media.IdealGasVaporMixtures.Interfaces;
partial package PartialIdealGasWaterVaporMixture "Medium model of an ideal gas-vapor mixture with water as condensing component based on NASA source "
  extends Modelica.Media.Interfaces.PartialCondensingGases(
    redeclare final record FluidConstants =
      Modelica.Media.Interfaces.Types.IdealGas.FluidConstants,
    final ThermoStates=Modelica.Media.Interfaces.Choices.IndependentVariables.pTX,
    final singleState=false,
    final reducedX=true,
    final fixedX=false,
    final reference_p=1e5,
    final reference_T(min=0)=0);

  //
  // Definition of constants
  //
  constant Modelica.Media.IdealGases.Common.DataRecord[:] data
    "Data records of ideal gas-vapor components. Note that water is the vapor
    substance and must be the last data record.";
  constant Modelica.Units.SI.MolarMass[nX] MMX=data[:].MM
    "Molar masses of components";

  constant Modelica.Units.SI.Pressure p_water_trp = 611.657
   "Triple point pressure of water";
  constant Modelica.Units.SI.Pressure p_water_crit = 22.064e6
   "Critical pressure of water";

  constant Modelica.Units.SI.Temperature T_water_trp = 273.16
   "Triple point temperature of water";
  constant Modelica.Units.SI.Temperature T_water_crit = 647.096
   "Critical temperature of water";

  constant Modelica.Units.SI.Density d_water_crit = 322
    "Critical density of water";

  constant Modelica.Units.SI.SpecificVolume v_water_liq = 1/1000
    "Specific volume of condensing component (i.e., water) at liquid phase
    assuming an ideal liquid";
  constant Modelica.Units.SI.SpecificVolume v_water_solid = 1/918
    "Specific volume of condensing component (i.e., water) at solid phase
    assuming an ideal solid";

  constant Modelica.Units.SI.Pressure p_water_ref = p_water_trp
   "Reference pressure of water";
  constant Modelica.Units.SI.Temperature T_water_ref = T_water_trp
   "Reference temperature of water";
  constant Modelica.Units.SI.SpecificEnthalpy h_water_ref = 0
    "Reference specific enthalpy of water (at liquid state)";
  constant Modelica.Units.SI.SpecificEnthalpy dh_vap_water_ref = 2500910
    "Reference specific enthalpy of vaporization of water";
  constant Modelica.Units.SI.SpecificEnthalpy dh_fus_water_ref = 333550
    "Reference specific enthalpy of fusion of water";
  constant Modelica.Units.SI.SpecificEntropy s_water_ref = 0
    "Reference specific entropy of water (at liquid state)";
  constant Modelica.Units.SI.SpecificEntropy ds_vap_water_ref=
    dh_vap_water_ref / T_water_ref
    "Reference specific entropy of vaporization of water";
  constant Modelica.Units.SI.SpecificEntropy ds_fus_water_ref=
    dh_fus_water_ref / T_water_ref
    "Reference specific entropy of fusion of water";

  constant Modelica.Units.SI.SpecificEnthalpy h_dryAir_off = 25097.2
    "Specific enthalpy offset of ideal gas mixture of dry air components to
    ensure that specific enthalpy of ideal dry air gas mixture is zero at
    reference state of water and reference mass fractions";
  constant Modelica.Units.SI.SpecificEntropy s_dryAir_off = -8239.74
    "Specific enthalpy offset of ideal gas mixture of dry air components to
    ensure that specific enthalpy of ideal dry air gas mixture is zero at
    reference state of water and reference mass fractions";

  constant Boolean print_warnings = false
    "= true, if warnings indicating bounds of validity shall be printed";

  //
  // Redeclare functions regarding states
  //
  redeclare final record extends ThermodynamicState
    "Thermodynamic state variables"
    Modelica.Units.SI.MoleFraction[nX] Y
      "Mole fractions (i.e., (component amount of substances)/(total amount of
       substances) n_i/n)";
  end ThermodynamicState;

  redeclare replaceable model extends BaseProperties(
    T(stateSelect=if preferredMediumStates then StateSelect.prefer else
      StateSelect.default),
    p(stateSelect=if preferredMediumStates then StateSelect.prefer else
      StateSelect.default),
    Xi(each stateSelect=if preferredMediumStates then StateSelect.prefer else
      StateSelect.default),
    final standardOrderComponents=true)
    "Calculats base properties of the ideal gas-vapor mixture"

    //
    // Definition of variables
    //
    Modelica.Units.SI.MoleFraction[nX] Y
      "Mole fractions of ideal gas vapor mixture";

    Modelica.Units.SI.MassFraction[nX] x
      "Mass fractions given per dry air mass";

    Modelica.Units.SI.MassFraction X_sat
      "Saturation mass fraction given per saturated moist air mass";
    Modelica.Units.SI.MassFraction X_vapor
      "Water vapor fraction given per moist air mass";
    Modelica.Units.SI.MassFraction X_liquidIce
      "Liquid or solid water fraction given per moist air mass";

    Modelica.Units.SI.MassFraction x_sat
      "Saturation mass fraction given per dry air mass";
    Modelica.Units.SI.MassFraction x_vapor
      "Water vapor fraction given per dry air mass";
    Modelica.Units.SI.MassFraction x_liquidIce
      "Liquid or solid water fraction given per dry air mass";

    Real phi(min=0, max=1)
      "Relative humidity";

  equation
    //
    // Calculate mass fractions, mole fractions, and relative humidity
    //
    Y = massToMoleFractions(X=X, MMX=MMX)
      "Mole fractions of ideal gas-vapor mixture";

    x = moistAirToDryAirMassFractions(X=X)
      "Mass fractions given per dry air mass";

    X_sat = massFractionSaturation(state=state)
      "Saturation mass fraction given per saturated moist air mass";
    X_vapor = X[nX] - X_liquidIce
      "Water vapor fraction given per moist air mass";
    X_liquidIce = max(0.0, X[nX] - X_sat)
      "Liquid or solid water fraction given per moist air mass";

    x_sat = dryMassFractionSaturation(state=state)
      "Saturation mass fraction given per dry air mass";
    x_vapor = x[nX] - x_liquidIce
      "Water vapor fraction given per dry air mass";
    x_liquidIce = max(0.0, x[nX] - x_sat)
      "Liquid or solid water fraction given per dry air mass";

    phi = relativeHumidity(state=state)
      "Relative humidity";

    //
    // Calculate state properties
    //
    d = rho_pTXY(p=p, T=T, X=X, Y=Y)
      "Density";
    h =specificEnthalpy_pTXY(
        p=p,
        T=T,
        X=X,
        Y=Y)
      "Specific enthalpy per moist air mass";
    u = h - p / d
      "Specific internal energy per moist air mass";

    state.p = p
      "State record: Pressure";
    state.T = T
      "State record: Temperature";
    state.X = X
      "State record: Mass fractions";
    state.Y = Y
      "State record: Mole fractions";

    //
    // Calculate additional properties
    //
    MM = molarMass(state=state)
      "Molar mass of the ideal gas-vapor mixture";
    R_s = gasConstant(state=state)
      "Specific ideal gas constant of the ideal gas-vapor mixture";

      //
      // Annotations
      //
      annotation (Documentation(info="<html>
<p>
This model calculates the base properties of an ideal gas-vapor mixture, including
pressure p, temperature T, density d, specific enthalpy h, specific internal
energy u, the specific ideal gas constant R_s, and the molar mass MM. Additionally,
a state record is provided. Furthermore, this model calculates the relative humidity
phi as well as the saturation mass fraction X/x_sat, water vapor mass fraction
X/x_vapor, and water liquid/ice mass fraction X/x_liquidIce per moist air mass (X)
and dry air mass (x). Note that preffered states are pressure, temperature, and
mass fractions of dry air to avoid non-linear equations.
</p>

</html>",     revisions="<html>
<ul>
  <li>
  November 27, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
  end BaseProperties;

  redeclare function extends setState_pTX
    "Return thermodynamic state as function of pressure p, temperature T, and composition X"
  algorithm
    state := if size(X, 1) == nX then
      ThermodynamicState(
        p=p,
        T=T,
        X=X,
        Y=massToMoleFractions(X=X, MMX=MMX)) else
      ThermodynamicState(
        p=p,
        T=T,
        X=cat(1, X, {1 - sum(X)}),
        Y=massToMoleFractions(X=cat(1, X, {1 - sum(X)}), MMX=MMX))
      "Thermodynamic state record";
  end setState_pTX;

  redeclare function extends setState_phX
    "Return thermodynamic state as function of pressure p, specific enthalpy h, and composition X"
  algorithm
    state :=if size(X, 1) == nX then ThermodynamicState(
        p=p,
        T=temperature_phXY(
          p=p,
          h=h,
          X=X,
          Y=massToMoleFractions(X=X, MMX=MMX)),
        X=X,
        Y=massToMoleFractions(X=X, MMX=MMX)) else ThermodynamicState(
        p=p,
        T=temperature_phXY(
          p=p,
          h=h,
          X=cat(
            1,
            X,
            {1 - sum(X)}),
          Y=massToMoleFractions(X=cat(
            1,
            X,
            {1 - sum(X)}), MMX=MMX)),
        X=cat(
          1,
          X,
          {1 - sum(X)}),
        Y=massToMoleFractions(X=cat(
          1,
          X,
          {1 - sum(X)}), MMX=MMX))
      "Thermodynamic state record";
  end setState_phX;

  redeclare function extends setState_psX
    "Return thermodynamic state as function of pressure p, specific enthalpy h, and composition X"
  algorithm
    state :=if size(X, 1) == nX then ThermodynamicState(
        p=p,
        T=temperature_psXY(
          p=p,
          s=s,
          X=X,
          Y=massToMoleFractions(X=X, MMX=MMX)),
        X=X,
        Y=massToMoleFractions(X=X, MMX=MMX)) else ThermodynamicState(
        p=p,
        T=temperature_psXY(
          p=p,
          s=s,
          X=cat(
            1,
            X,
            {1 - sum(X)}),
          Y=massToMoleFractions(X=cat(
            1,
            X,
            {1 - sum(X)}), MMX=MMX)),
        X=cat(
          1,
          X,
          {1 - sum(X)}),
        Y=massToMoleFractions(X=cat(
          1,
          X,
          {1 - sum(X)}), MMX=MMX))
      "Thermodynamic state record";
  end setState_psX;

  redeclare function extends setState_dTX
    "Return thermodynamic state as function of density d, temperature T, and composition X"
  algorithm
    state :=if size(X, 1) == nX then ThermodynamicState(
        p=pressure_dTXY(
          d=d,
          T=T,
          X=X,
          Y=massToMoleFractions(X=X, MMX=MMX)),
        T=T,
        X=X,
        Y=massToMoleFractions(X=X, MMX=MMX)) else ThermodynamicState(
        p=pressure_dTXY(
          d=d,
          T=T,
          X=cat(
            1,
            X,
            {1 - sum(X)}),
          Y=massToMoleFractions(X=cat(
            1,
            X,
            {1 - sum(X)}), MMX=MMX)),
        T=T,
        X=cat(
          1,
          X,
          {1 - sum(X)}),
        Y=massToMoleFractions(X=cat(
          1,
          X,
          {1 - sum(X)}), MMX=MMX))
      "Thermodynamic state record";
  end setState_dTX;

  redeclare function extends setSmoothState
    "Return thermodynamic state so that it smoothly approximates: If x > 0, then state_a else state_b"
  algorithm
    state := ThermodynamicState(
      p=Modelica.Media.Common.smoothStep(
        x,
        state_a.p,
        state_b.p,
        x_small),
      T=Modelica.Media.Common.smoothStep(
        x,
        state_a.T,
        state_b.T,
        x_small),
      X=Modelica.Media.Common.smoothStep(
        x,
        state_a.X,
        state_b.X,
        x_small),
      Y=Modelica.Media.Common.smoothStep(
        x,
        state_a.Y,
        state_b.Y,
        x_small));
  end setSmoothState;
  //
  // Redeclare functions regarding state properties
  //
  redeclare function extends pressure
    "Returns pressure of the ideal gas-vapor mixture"
  algorithm
    p := state.p
      "Pressure of ideal gas-vapor mixture";
  end pressure;

  redeclare function extends temperature
    "Returns temperature of the ideal gas-vapor mixture"
  algorithm
    T := state.T
      "Tempeerature of ideal gas-vapor mixture";
  end temperature;

  redeclare function extends density
    "Returns density of the ideal gas-vapor mixture"
  algorithm
    d := rho_pTXY(p=state.p, T=state.T, X=state.X, Y=state.Y)
      "Density of ideal gas-vapor mixture";
  end density;

  redeclare function extends specificEnthalpy
    "Returns specific enthalpy of the ideal gas-vapor mixture"
  algorithm
    h :=specificEnthalpy_pTXY(
        p=state.p,
        T=state.T,
        X=state.X,
        Y=state.Y)
      "Specific enthalpy of ideal gas-vapor mixture";
  end specificEnthalpy;

  redeclare function extends specificInternalEnergy
    "Returns specific internal energy of the ideal gas-vapor mixture"
  algorithm
    u :=specificEnthalpy_pTXY(
        p=state.p,
        T=state.T,
        X=state.X,
        Y=state.Y) - state.p/rho_pTXY(
        p=state.p,
        T=state.T,
        X=state.X,
        Y=state.Y)
      "Specific internal energy of ideal gas-vapor mixture";
  end specificInternalEnergy;

  redeclare function extends specificEntropy
    "Returns specific entropy of the ideal gas-vapor mixture"
  algorithm
    s :=specificEntropy_pTXY(
        p=state.p,
        T=state.T,
        X=state.X,
        Y=state.Y)
      "Specific entropy of ideal gas-vapor mixture";
  end specificEntropy;

  redeclare function extends specificGibbsEnergy
    "Returns specific Gibbs energy of the ideal gas-vapor mixture"
  algorithm
    g :=specificEnthalpy_pTXY(
        p=state.p,
        T=state.T,
        X=state.X,
        Y=state.Y) - state.T*specificEntropy_pTXY(
        p=state.p,
        T=state.T,
        X=state.X,
        Y=state.Y)
      "Specific Gibbs energy of ideal gas-vapor mixture";
  end specificGibbsEnergy;

  redeclare function extends specificHelmholtzEnergy
    "Returns specific Helmholtz energy of the ideal gas-vapor mixture"
  algorithm
    f :=specificEnthalpy_pTXY(
        p=state.p,
        T=state.T,
        X=state.X,
        Y=state.Y) - state.p/rho_pTXY(
        p=state.p,
        T=state.T,
        X=state.X,
        Y=state.Y) - state.T*specificEntropy_pTXY(
        p=state.p,
        T=state.T,
        X=state.X,
        Y=state.Y)
      "Specific Helmholtz energy of ideal gas-vapor mixture";
  end specificHelmholtzEnergy;
  //
  // Redeclare functions regarding additional properties
  //
  redeclare function extends dynamicViscosity
    "Returns dynamic viscosity"

    //
    // Definition of constants
    //
protected
    constant Modelica.Units.SI.Temperature A_liq = 0.45047
      "First constant of liquid water";
    constant Modelica.Units.SI.Temperature B_liq = 1.39753
      "Second constant of liquid water";
    constant Modelica.Units.SI.Temperature C_liq = 613.181
      "Third constant of liquid water";
    constant Modelica.Units.SI.Temperature D_liq = 63.697
      "fourth constant of liquid water";
    constant Modelica.Media.Interfaces.Types.DynamicViscosity E_liq = 0.00006896
      "Fivth constant of liquid water";

    //
    // Definition of variables
    //
    Modelica.Units.SI.MassFraction[nX] x=
      moistAirToDryAirMassFractions(X=state.X)
      "Mass fractions per dry air";
    Modelica.Units.SI.MassFraction x_sat=
      dryMassFractionSaturation(state=state)
      "Saturation mass fraction of condensing component per dry air";

    Modelica.Units.SI.Pressure[nX] p_i=
      partialPressures(state=state)
      "Partial pressures";

    Modelica.Units.SI.SpecificVolume v_dryAirVapor
      "Specific volume of dry air and water vapor per dry air mass";
    Modelica.Units.SI.SpecificVolume v_liquidSolid
      "Specific volume of liquid or solid water per dry air mass";

    Modelica.Media.Interfaces.Types.DynamicViscosity[nX] eta_gasVapor
      "Dynamic viscosities of components at gas or vapor state";
    Modelica.Media.Interfaces.Types.DynamicViscosity eta_dryAirVapor
      "Dynamic viscosity of dry air and water vapor";
    Modelica.Media.Interfaces.Types.DynamicViscosity eta_liquidSolid
      "Dynamic viscosity of water's liquid or solid phase";

  algorithm
    //
    // Calculate dynamic viscosity of ideal gas-vapor mixture at gas state
    //
    for ind in 1:nX loop
      eta_gasVapor[ind] :=
        Modelica.Media.IdealGases.Common.Functions.dynamicViscosityLowPressure(
          T=state.T,
          Tc=fluidConstants[ind].criticalTemperature,
          M=fluidConstants[ind].molarMass,
          Vc=fluidConstants[ind].criticalMolarVolume,
          w=fluidConstants[ind].acentricFactor,
          mu=fluidConstants[ind].dipoleMoment)
        "Dynamic viscosities of components at gas or vapor state";
    end for;

    eta_dryAirVapor := gasMixtureViscosity(
        yi=p_i./state.p,
        M=MMX,
        eta=eta_gasVapor)
      "Dynamic viscosity of dry air and water vapor";

    //
    // Calculate densities and dynamic viscosity of water at liquid or solid state
    //
    if x[nX] <= x_sat then
      v_dryAirVapor :=
        (sum(x ./ MMX .* Modelica.Constants.R .* state.T ./ state.p))
        "Specific volume of dry air and water vapor per dry air mass";
      v_liquidSolid :=0
        "Specific volume of liquid or solid water per dry air mass";
      eta_liquidSolid :=0
        "Dynamic viscosity of water's liquid or solid phase";

    elseif state.T > T_water_trp then
      v_dryAirVapor :=
        (sum(x[1:nX-1] .* Modelica.Constants.R ./ MMX[1:nX-1] .* state.T ./
        state.p) +
        x_sat * Modelica.Constants.R / MMX[nX] * state.T / state.p)
        "Specific volume of dry air and water vapor per dry air mass";
      v_liquidSolid :=
        ((x[nX] - x_sat) * v_water_liq)
        "Specific volume of liquid or solid water per dry air mass";
      eta_liquidSolid :=
        E_liq * Modelica.Math.exp(A_liq *
        ((C_liq - state.T)/(state.T - D_liq)) ^ (1/3) + B_liq *
        ((C_liq - state.T)/(state.T - D_liq)) ^ (4/3))
        "Dynamic viscosity of water's liquid or solid phase";

    else
      /*
    Note that the dynamic viscosity of solid water is neglected
    */
      v_dryAirVapor :=
        (sum(x[1:nX-1] .* Modelica.Constants.R ./ MMX[1:nX-1] .* state.T ./
        state.p) +
        x_sat * Modelica.Constants.R / MMX[nX] * state.T / state.p)
        "Specific volume of dry air and water vapor per dry air mass";
      v_liquidSolid :=
        ((x[nX] - x_sat) * v_water_solid) * 0
        "Specific volume of liquid or solid water per dry air mass";
      eta_liquidSolid := 1e14
        "Dynamic viscosity of water's liquid or solid phase";

    end if;

    //
    // Calculate density averaged dynamic viscosity
    //
    eta := v_dryAirVapor / (v_dryAirVapor + v_liquidSolid) * eta_dryAirVapor +
      v_liquidSolid / (v_dryAirVapor + v_liquidSolid) * eta_liquidSolid
      "Dynamic viscosity";

    //
    // Annotations
    //
    annotation (Documentation(info="<html>
<p>
This function calculates the dynamic viscosity of the ideal gas-vapor mixture as
function of the state record. For the case of saturated dry air with liquid or
solid water, the dynamic viscosity is calculated as average dynamic viscosity
using the specific volumes of the different phases for averaging. Note that the
dynamic viscosity of solid water is neglected. For the ideal gas-vapor mixture,
this function applies a simplification of the kinetic theory (Chapman and Enskog 
theory) neglecting the second-order effects. Details can be found in the orginal
implementations:
<a href=\"Modelica://Modelica.Media.IdealGases.Common.MixtureGasNasa.dynamicViscosity\">Modelica.Media.IdealGases.Common.MixtureGasNasa.dynamicViscosity</a>
and
<a href=\"Modelica://Modelica.Media.IdealGases.Common.MixtureGasNasa.gasMixtureViscosity\">Modelica.Media.IdealGases.Common.MixtureGasNasa.gasMixtureViscosity</a>. 
For liquid water, a generalized function is applied according to the VDI Heat
Atlas.
</p>

<h4>References</h4>
<ul>
  <li>
  Kleiber, M. and Joh, R. and Span, R. (2002). D3 Properties of Pure Fluid Substances, In: VDI Heat Atlas, 2nd Edition. DOI: https://doi.org/10.1007/978-3-540-77877-6.
  </li>
</ul>

</html>",   revisions="<html>
<ul>
  <li>
  November 27, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
  end dynamicViscosity;

  redeclare function extends thermalConductivity
    "Returns thermal conductivity"
    //
    // Definition of constants
    //
protected
    constant Modelica.Units.SI.ThermalConductivity A_liq = -2.4149
      "First constant of liquid water";
    constant Real B_liq(unit="W/(m.K2)") = 2.45165e-2
      "Second constant of liquid water";
    constant Real C_liq(unit="W/(m.K3)") = -0.73121e-4
      "Third constant of liquid water";
    constant Real D_liq(unit="W/(m.K4)") = 0.99492e-7
      "fourth constant of liquid water";
    constant Real E_liq(unit="W/(m.K5)") = -0.53730e-10
      "Fivth constant of liquid water";

    constant Modelica.Units.SI.ThermalConductivity A_solid = 2.60970865
      "First constant of solid water";
    constant Real B_solid(unit="W/(m.K2)") = 3.72962320e-2
      "Second constant of solid water";
    constant Real C_solid(unit="W/(m.K3)") = -2.63535950e-4
      "Third constant of solid water";
    constant Real D_solid(unit="W/(m.K4)") = 4.45674690e-7
      "fourth constant of solid water";

    //
    // Definition of variables
    //
    Modelica.Units.SI.MassFraction[nX] x=
      moistAirToDryAirMassFractions(X=state.X)
      "Mass fractions per dry air";
    Modelica.Units.SI.MassFraction x_sat=
      dryMassFractionSaturation(state=state)
      "Saturation mass fraction of condensing component per dry air";

    Modelica.Units.SI.Pressure[nX] p_i=
      partialPressures(state=state)
      "Partial pressures";

    Modelica.Units.SI.SpecificVolume v_dryAirVapor
      "Specific volume of dry air and water vapor per dry air mass";
    Modelica.Units.SI.SpecificVolume v_liquidSolid
      "Specific volume of liquid or solid water per dry air mass";

    Modelica.Units.SI.SpecificHeatCapacity[nX] cp_gasVapor
      "Specific heat capacities of components at gas or vapor state";
    Modelica.Units.SI.DynamicViscosity[nX] eta_gasVapor
      "Dynamic viscosities of components at gas or vapor state";

    Modelica.Units.SI.ThermalConductivity[nX] lambda_gasVapor
      "Thermal conductivities of components at gas or vapor state";
    Modelica.Units.SI.ThermalConductivity lambda_dryAirVapor
      "Thermal conductivity of dry air and water vapor";
    Modelica.Units.SI.ThermalConductivity lambda_liquidSolid
      "Thermal conductivity of water's liquid or solid phase";

  algorithm
    //
    // Calculate dynamic viscosity of ideal gas-vapor mixture at gas state
    //
    for ind in 1:nX loop
      cp_gasVapor[ind] :=
        Modelica.Media.IdealGases.Common.Functions.cp_T(
          data=data[ind],
          T=state.T)
        "Specific heat capacities of components at gas or vapor state";

      eta_gasVapor[ind] :=
        Modelica.Media.IdealGases.Common.Functions.dynamicViscosityLowPressure(
          T=state.T,
          Tc=fluidConstants[ind].criticalTemperature,
          M=fluidConstants[ind].molarMass,
          Vc=fluidConstants[ind].criticalMolarVolume,
          w=fluidConstants[ind].acentricFactor,
          mu=fluidConstants[ind].dipoleMoment)
        "Dynamic viscosities of components at gas or vapor state";

      lambda_gasVapor[ind] :=
       Modelica.Media.IdealGases.Common.Functions.thermalConductivityEstimate(
          Cp=cp_gasVapor[ind],
          eta=eta_gasVapor[ind],
          method=1,
          data=data[ind])
        "Thermal conductivities of components at gas or vapor state";
    end for;

    lambda_dryAirVapor := lowPressureThermalConductivity(
        y=p_i./state.p,
        T=state.T,
        Tc=fluidConstants[:].criticalTemperature,
        Pc=fluidConstants[:].criticalPressure,
        M=MMX,
        lambda=lambda_gasVapor)
      "Thermal conductivity of dry air and water vapor";

    //
    // Calculate densities and dynamic viscosity of water at liquid or solid state
    //
    if x[nX] <= x_sat then
      v_dryAirVapor :=
        (sum(x ./ MMX .* Modelica.Constants.R .* state.T ./ state.p))
        "Specific volume of dry air and water vapor per dry air mass";
      v_liquidSolid :=0
        "Specific volume of liquid or solid water per dry air mass";
      lambda_liquidSolid :=0
        "Thermal conductivity of water's liquid or solid phase";

    elseif state.T > T_water_trp then
      v_dryAirVapor :=
        (sum(x[1:nX-1] .* Modelica.Constants.R ./ MMX[1:nX-1] .* state.T ./
        state.p) +
        x_sat * Modelica.Constants.R / MMX[nX] * state.T / state.p)
        "Specific volume of dry air and water vapor per dry air mass";
      v_liquidSolid :=
        ((x[nX] - x_sat) * v_water_liq)
        "Specific volume of liquid or solid water per dry air mass";
      lambda_liquidSolid :=
        A_liq + B_liq * state.T + C_liq * state.T^2 +
        D_liq * state.T^3 + E_liq * state.T^4
        "Thermal conductivity of water's liquid or solid phase";

    else
      v_dryAirVapor :=
        (sum(x[1:nX-1] .* Modelica.Constants.R ./ MMX[1:nX-1] .* state.T ./
        state.p) +
        x_sat * Modelica.Constants.R / MMX[nX] * state.T / state.p)
        "Specific volume of dry air and water vapor per dry air mass";
      v_liquidSolid :=
        ((x[nX] - x_sat) * v_water_solid)
        "Specific volume of liquid or solid water per dry air mass";
      lambda_liquidSolid :=
        A_solid + B_solid * state.T + C_solid * state.T^2 + D_solid * state.T^3
        "Thermal conductivity of water's liquid or solid phase";

    end if;

    //
    // Calculate density averaged dynamic viscosity
    //
    lambda := v_dryAirVapor / (v_dryAirVapor + v_liquidSolid) * lambda_dryAirVapor +
      v_liquidSolid / (v_dryAirVapor + v_liquidSolid) * lambda_liquidSolid
      "Thermal conductivity";

    //
    // Annotations
    //
    annotation (Documentation(info="<html>
<p>
This function calculates the thermal conductivity of the ideal gas-vapor mixture
as function of the state record. For the case of saturated dry air with liquid or
solid water, the thermal conductivity is calculated as average dynamic viscosity
using the specific volumes of the different phases for averaging. For the ideal 
gas-vapor mixture, this function applies the Masson and Saxena modification of the
Wassiljewa Equation for the thermal conductivity for gas mixtures of n elements 
at low pressure. Details can be found in the orginal implementations:
<a href=\"Modelica://Modelica.Media.IdealGases.Common.MixtureGasNasa.thermalConductivity\">Modelica.Media.IdealGases.Common.MixtureGasNasa.thermalConductivity</a>
and
<a href=\"Modelica://Modelica.Media.IdealGases.Common.MixtureGasNasa.lowPressureThermalConductivity\">Modelica.Media.IdealGases.Common.MixtureGasNasa.lowPressureThermalConductivity</a>. 
For liquid water, a generalized function is applied according to the VDI Heat
Atlas. For solid water, a polynomial fit is applied using data from the
Engineering Toolbox.
</p>

<h4>References</h4>
<ul>
  <li>
  The Engineering ToolBox (2004). Ice - Thermal Properties. URL: https://www.engineeringtoolbox.com/ice-thermal-properties-d_576.html [Accessed November 27, 2023].
  </li>
  <li>
  Kleiber, M. and Joh, R. and Span, R. (2002). D3 Properties of Pure Fluid Substances, In: VDI Heat Atlas, 2nd Edition. DOI: https://doi.org/10.1007/978-3-540-77877-6.
  </li>
</ul>

</html>",   revisions="<html>
<ul>
  <li>
  November 27, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
  end thermalConductivity;

  redeclare function extends specificHeatCapacityCp
    "Returns specific heat capacity at constant pressure"
  algorithm
    cp := dh_dT_pX(state=state)
      "Specific heat capacity at constant pressure";
  end specificHeatCapacityCp;

  redeclare function extends specificHeatCapacityCv
    "Returns specific heat capacity at constant volume"

    //
    // Definition of variables
    //
protected
    SorpLib.Units.DerSpecificVolumeByPressure dv_dp_TX_=
      dv_dp_TX(state=state)
      "Partial derivative of specific volume w.r.t. pressure at constant 
      temperature and mass fractions";
    SorpLib.Units.DerSpecificVolumeByTemperature dv_dT_pX_=
      dv_dT_pX(state=state)
    "Partial derivative of specific volume w.r.t. temperature at constant pressure
    and mass fractions";

  algorithm
    cv := dh_dT_pX(state=state) - state.p * dv_dT_pX_ -
      dh_dp_TX(state=state) * dv_dT_pX_ / dv_dp_TX_ +
      1 / density(state=state) * dv_dT_pX_ / dv_dp_TX_ +
      state.p * dv_dT_pX_
      "Specific heat capacity at constant volume";
  end specificHeatCapacityCv;

  redeclare function extends isentropicExponent
    "Returns isentropic exponent that is only sound for unsaturated air"
  algorithm
    gamma := specificHeatCapacityCp(state=state) /
      specificHeatCapacityCv(state=state)
      "Isentropic exponent";
  end isentropicExponent;

  redeclare function extends isentropicEnthalpy
    "Returns isentropic enthalpy"
  algorithm
    h_is := specificEnthalpy_pTXY(
      p=p_downstream,
      T=temperature_psXY(
        p=p_downstream,
        s=specificEntropy(state=refState),
        X=refState.Y,
        Y=refState.Y),
      X=refState.X,
      Y=refState.Y)
      "Isentropic enthalpy";
  end isentropicEnthalpy;

  redeclare function extends velocityOfSound
    "Return veolicity of sound"
  algorithm
    a := sqrt(1 / (-density(state=state)^2 * (dv_dp_TX(state=state) +
      dv_dT_pX(state=state)^2 * state.T  / specificHeatCapacityCp(state=state))))
      "Velocity of sound";
  end velocityOfSound;

  redeclare function extends isobaricExpansionCoefficient
    "Returns isobaric expansion coefficient"
  algorithm
    beta := density(state=state) * dv_dT_pX(state=state)
      "Isobaric expansion coefficient";
  end isobaricExpansionCoefficient;

  redeclare function extends isothermalCompressibility
    "Returns isothermal compressibility"
  algorithm
    kappa := -density(state=state) * dv_dp_TX(state=state)
      "Isothermal compressibility";
  end isothermalCompressibility;

  redeclare function extends density_derp_h
    "Returns partial derivative of density w.r.t. pressure at constant specific enthalpy and mass fractions"
  algorithm
    ddph := density_derp_T(state=state) - density_derT_p(state=state) *
      dh_dp_TX(state=state) / dh_dT_pX(state=state)
      "Partial derivative of density w.r.t. pressure at constant specific  
      enthalpy and mass fractions";
  end density_derp_h;

  redeclare function extends density_derh_p
    "Returns partial derivative of density w.r.t. specific enthalpy at constant pressure and mass fractions"
  algorithm
    ddhp := density_derT_p(state=state) / dh_dT_pX(state=state)
      "Partial derivative of density w.r.t. specific enthalpy at constant  
      pressure and mass fractions";
  end density_derh_p;

  redeclare function extends density_derp_T
    "Returns partial derivative of density w.r.t. pressure at constant temperature and mass fractions"
  algorithm
    ddpT := -rho_pTXY(p=state.p, T=state.T, X=state.X, Y=state.Y)^2 *
      dv_dp_TX(state=state)
      "Partial derivative of density w.r.t. pressure at constant temperature 
      and mass fractions";
  end density_derp_T;

  redeclare function extends density_derT_p
    "Returns partial derivative of density w.r.t. temperature at constant pressure and mass fractions"
  algorithm
    ddTp := -rho_pTXY(p=state.p, T=state.T, X=state.X, Y=state.Y)^2 *
      dv_dT_pX(state=state)
      "Partial derivative of density w.r.t. temperature at constant pressure 
      and mass fractions";
  end density_derT_p;

  redeclare function extends density_derX
    "Returns partial derivatives of density w.r.t. mass fractions at constant pressure and temperature"
  algorithm
    dddX := -rho_pTXY(p=state.p, T=state.T, X=state.X, Y=state.Y)^2 .*
      dv_dX_pT(state=state)
      "Partial derivative of density w.r.t. mass fractions at constant pressure 
    and temperature";
  end density_derX;

  redeclare function extends molarMass
    "Returns molar mass of the ideal gas-vapor mixture"
  algorithm
    MM := state.Y * MMX
      "Molar mass of the ideal gas-vapor mixture";
  end molarMass;

  redeclare function extends gasConstant
    "Returns specific ideal gas constant of the ideal gas-vapor mixture"
  algorithm
    if (state.X[nX] / (1 - state.X[nX])) >
      dryMassFractionSaturation(state=state) then
      R_s := Modelica.Constants.R /
        sum(partialPressures(state=state) ./ state.p .* MMX)
        "Specific ideal gas constant for saturated gas-vapor mixture ";

    else
      R_s := Modelica.Constants.R * sum(state.X ./ MMX)
        "Specific ideal gas constant for unsaturated gas-vapor mixture ";

    end if;
  end gasConstant;

  redeclare function extends enthalpyOfNonCondensingGas
    "Returns specific enthalpy of non-condensing components (i.e., dry air) (per dry air mass)"

    //
    // Definition of inputs
    //
    input Modelica.Units.SI.MassFraction[:] X
      "Vector of mass fractions"
      annotation (Dialog(tab="General",group="Inputs"));

  algorithm
    h := X[1:nX-1] / sum(X[1:nX-1]) *
      {Modelica.Media.IdealGases.Common.Functions.h_T(
        data=data[i],
        T=T,
        exclEnthForm=true,
        refChoice=Modelica.Media.Interfaces.Choices.ReferenceEnthalpy.UserDefined,
        h_off=h_dryAir_off) for i in 1:nX-1}
      "Specific enthalpy of dry air";

    //
    // Annotations
    //
    annotation (Documentation(info="<html>
<p>
This function calculates the specific enthalpy of the non-condensing components
(i.e., dry air) per dry air mass according to the model of an ideal gas mixture 
as function of temperature and mass fractions. The specific enthalpy is calculated 
using NASA Glenn Coefficients. For more details, check the function
<a href=\"Modelica://Modelica.Media.IdealGases.Common.MixtureGasNasa.h_TX\">Modelica.Media.IdealGases.Common.MixtureGasNasa.h_TX</a>.
Note that this function does assume that no water is present. To calculate the 
specific enthalpy of the non-condensing components that are actually present, 
use the function
<a href=\"Modelica://SorpLib.Media.IdealGasVaporMixtures.Interfaces.PartialIdealGasWaterVaporMixture.enthalpyOfNonCondensingComponents\">SorpLib.Media.IdealGasVaporMixtures.Interfaces.PartialIdealGasWaterVaporMixture.enthalpyOfNonCondensingComponents</a>.
</p>

<h4>References</h4>
<ul>
  <li>
  McBride, B.J. and Zehe, M.J. and Gordon, S. (2002). NASA Glenn Coefficients for Calculating Thermodynamic Properties of Individual Species, Technical report, NASA/TP—2002-211556. URL: https://ntrs.nasa.gov/citations/20020085330.
  </li>
</ul>

</html>",   revisions="<html>
<ul>
  <li>
  November 27, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
  end enthalpyOfNonCondensingGas;

  redeclare function extends enthalpyOfGas(
    T=state.T,
    X=state.X)
    "Returns specific enthalpy of non-condensing components (i.e., dry air) and vaporous water (per moist air mass)"

    //
    // Definition of inputs
    //
    input ThermodynamicState state
      "Thermodynamic state record"
      annotation (Dialog(tab="General",group="Inputs"));

    //
    // Definition of variables
    //
protected
    Modelica.Units.SI.MassFraction[nX] x=
      moistAirToDryAirMassFractions(X=state.X)
      "Mass fractions per dry air";
    Modelica.Units.SI.MassFraction x_sat=
      dryMassFractionSaturation(state=state)
      "Saturation mass fraction of condensing component per dry air";

  algorithm
    //
    // Calculate specific enthalpy per dry air mass
    //
    if Modelica.Math.isEqual(s1=x[nX], s2=0, eps=100*Modelica.Constants.eps) then
      h := enthalpyOfNonCondensingGas(T=state.T, X=x)
        "Specific enthalpy equals specific enthalpy of unsaturated dry air without 
        water: Calculated applying the law of ideal gas mixtures.";

    elseif x[nX] <= x_sat then
      h := enthalpyOfNonCondensingGas(T=state.T, X=x) +
        x[nX] * enthalpyOfCondensingGas(T=state.T)
        "Specific enthalpy equals specific enthalpy of unsaturated dry air and water 
        vapor: Calculated applying the law of ideal gas mixtures.";

    else
      h := enthalpyOfNonCondensingGas(T=state.T, X=x) +
        x_sat * enthalpyOfCondensingGas(T=state.T)
        "Specific enthalpy equals specific enthalpy of saturated dry air and water
        vapor: Calculated applying the law of ideal gas mixtures.";

    end if;

    //
    // Convert specific enthalpy per dry air mass
    //
    h := h / (1 + x[nX])
      "Specific enthalpy per moist air mass";

    //
    // Annotations
    //
    annotation (Documentation(info="<html>
<p>
This function calculates the specific enthalpy of the non-condensing components
(i.e., dry air) and vaporous water (i.e., vapor) per moist air mass according to 
the model of an ideal gas-vapor mixture as function of temperature and mass 
fractions.
</p>

</html>",   revisions="<html>
<ul>
  <li>
  November 27, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
  end enthalpyOfGas;
  //
  // Redeclare functions regarding condensing component
  //
  redeclare function extends saturationPressure
    "Returns saturation pressure of condensing fluid (i.e., water)"
  algorithm
    psat := saturationPressureH2O_T(Tsat)
      "Saturation pressure";

    //
    // Annotations
    //
    annotation (Inline=false,
      InlineAfterIndexReduction=false,
      LateInline=true,
      inverse(Tsat=saturationTemperature(p_sat=psat)));
  end saturationPressure;

  redeclare function extends enthalpyOfVaporization
    "Returns specific enthalpy of vaporization of water (per water mass)"

    //
    // Definition of constants
    //
protected
    constant Real[7] coefficients_liq=
      {1, 1.99274064, 1.09965342, -0.510839303,
      -1.75493479, -45.5170352, -6.74694450e5}
      "Coefficients of saturated liquid density";
    constant Real[6] coefficients_vap=
      {-2.03150240, -2.68302940, -5.38626492, -17.2991605,
      -44.7586581, -63.9201063}
      "Coefficients of saturated vapor density";

    constant Real[7] exponents_liq=
      {0, 1/3, 2/3, 5/3, 16/3, 43/3, 110/3}
      "Exponents of saturated liquid density";
    constant Real[6] exponents_vap=
      {2/6, 4/6, 8/6, 18/6, 37/6, 71/6}
      "Exponents of saturated vapor density";

    //
    // Definition of variables
    //
    Modelica.Media.Common.DerPressureByTemperature dp_sat_dT=
      dsaturationPressureH2O_dT(T_sat=T)
      "First-order partial derivative of saturation pressure w.r.t. temperature";

    Modelica.Units.SI.Density rho_sat_liq=
      SorpLib.Media.Functions.Utilities.generalizedFunction_T(
        T=T,
        T_ref=T_water_crit,
        z_ref=d_water_crit,
        coefficients=coefficients_liq,
        exponents=exponents_liq,
        approach=SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionReducedTemperature)
      "Density of saturated liquid";
    Modelica.Units.SI.Density rho_sat_vap=
      SorpLib.Media.Functions.Utilities.generalizedFunction_T(
        T=T,
        T_ref=T_water_crit,
        z_ref=d_water_crit,
        coefficients=coefficients_vap,
        exponents=exponents_vap,
        approach=SorpLib.Choices.GeneralizedFunctionApproach.ExponentialFunctionReducedTemperature)
      "Density of saturated vapor";

  algorithm
    r0 := (T/rho_sat_vap - T/rho_sat_liq) * dp_sat_dT
      "Specific enthalpy of vaporization of water";

    //
    // Assertations
    //
    if print_warnings then
      assert(T_water_trp <= T and T <= T_water_crit,
        "Temperature (" + String(T) + " K) is not between 273.16 and 647.096 K. " +
        "Calculation of specific enthalpy of vaporization is not possible!",
        level = AssertionLevel.warning);
    end if;

    //
    // Annotations
    //
    annotation (Documentation(info="<html>
<p>
This function calculates the specific enthalpy of vaporization of the condensing 
component (i.e., water) per water mass as function of the temperature.
</p>

<h4>References</h4>
<ul>
  <li>
  Wagner, W. and Pru&szlig;, A (2002). The IAPWS Formulation 1995 for the Thermodynamic Properties of Ordinary Water Substance for General and Scientific Use, Journal of Physical and Chemical Reference Data, 31:387. DOI: https://doi.org/10.1063/1.1461829.
  </li>
</ul>
</html>",   revisions="<html>
<ul>
  <li>
  November 27, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
  end enthalpyOfVaporization;

  redeclare function extends enthalpyOfLiquid
    "Returns enthalpy of liquid water (per water mass)"

    //
    // Definition of inputs
    //
    input Modelica.Units.SI.Pressure p
      "Pressure"
      annotation (Dialog(tab="General",group="Inputs"));

    //
    // Definition of constants
    //
protected
    constant Real[10] coefficients=
      {4.66348457415926e-19, -1.16579701193758e-15, 9.49925152355854e-13,
      5.81553479401465e-11, -6.55387915784314e-7, 5.30868446475702e-4,
      -2.18234813641286e-1, 5.13043718148301e1, -6.58121774333134e3,
      3.63215816205503e5}
      "Coefficients of polynomical function";
    constant Real[10] exponents=
      {9, 8, 7, 6, 5, 4, 3, 2, 1, 0}
      "Exponents of polynomical function";

  algorithm
    h := h_water_ref + v_water_liq * (p - p_water_ref) +
      SorpLib.Media.Functions.Utilities.intGeneralizedFunction_dT(
        T=T,
        T_ref=573.15,
        z_ref=1,
        coefficients=coefficients,
        exponents=exponents,
        approach=SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionTemperature) -
      SorpLib.Media.Functions.Utilities.intGeneralizedFunction_dT(
        T=T_water_ref,
        T_ref=573.15,
        z_ref=1,
        coefficients=coefficients,
        exponents=exponents,
        approach=SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionTemperature)
      "Specific enthalpy of liquid water";

    //
    // Assertations
    //
    if print_warnings then
      assert(T_water_trp-1 <= T and T <= 573.15,
        "Temperature (" + String(T) + " K) is not between 272.16 and 573.15 K. " +
        "Calculation of specific enthalpy of liquid may not be valid!",
        level = AssertionLevel.warning);
    end if;

    //
    // Annotations
    //
    annotation (Documentation(info="<html>
<p>
This function calculates the specific enthalpy of the condensing component's
(i.e., water) liquid phase per water mass according to the model of an ideal 
liquid as function of pressure and temperature. The specific heat capacity is 
calculated as a polynomial fit to specific heat capacity data calculated at 
saturated liquid state with the reference equation of state for water. The data 
was calculated for temperature varying from 273.16 K to 573.15 K. Note that this 
function does not consider the actual water mass fraction. To calculate the 
specific enthalpy of the liquid water that is actually present, use the function
<a href=\"Modelica://SorpLib.Media.IdealGasVaporMixtures.Interfaces.PartialIdealGasWaterVaporMixture.enthalpyOfLiquidIce\">SorpLib.Media.IdealGasVaporMixtures.Interfaces.PartialIdealGasWaterVaporMixture.enthalpyOfLiquidIce</a>.
</p>

<h4>References</h4>
<ul>
  <li>
  McBride, B.J. and Zehe, M.J. and Gordon, S. (2002). NASA Glenn Coefficients for Calculating Thermodynamic Properties of Individual Species, Technical report, NASA/TP—2002-211556. URL: https://ntrs.nasa.gov/citations/20020085330.
  </li>
</ul>

</html>",   revisions="<html>
<ul>
  <li>
  November 27, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
  end enthalpyOfLiquid;

  redeclare function extends enthalpyOfCondensingGas
    "Returns enthalpy of vaporous water (per water mass)"

    //
    // Definition of constants
    //
protected
    constant Real[10] coefficients=
      {7.03241022996521e-23, -4.12454440465976e-19, 1.05640534898709e-15,
      -1.54797437245421e-12, 1.42577474111776e-9, -8.51385633913246e-7,
      3.26064390458737e-4, -7.52464997116974e-2, 9.42339113729594,
      1.36103845934352e+3}
      "Coefficients of polynomical function";
    constant Real[10] exponents=
      {9, 8, 7, 6, 5, 4, 3, 2, 1, 0}
      "Exponents of polynomical function";

  algorithm
    h := h_water_ref + dh_vap_water_ref +
      SorpLib.Media.Functions.Utilities.intGeneralizedFunction_dT(
        T=T,
        T_ref=T_water_crit,
        z_ref=1,
        coefficients=coefficients,
        exponents=exponents,
        approach=SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionTemperature) -
      SorpLib.Media.Functions.Utilities.intGeneralizedFunction_dT(
        T=T_water_ref,
        T_ref=T_water_crit,
        z_ref=1,
        coefficients=coefficients,
        exponents=exponents,
        approach=SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionTemperature)
      "Specific enthalpy of vaporous water";

    //
    // Assertations
    //
    if print_warnings then
      assert(200 <= T and T <= 1000,
        "Temperature (" + String(T) + " K) is not between 200 and 1000 K. " +
        "Calculation of specific enthalpy of vapor may not be valid!",
        level = AssertionLevel.warning);
    end if;

    //
    // Annotations
    //
    annotation (Documentation(info="<html>
<p>
This function calculates the specific enthalpy of the condensing component's
(i.e., water) vapor phase per mass of water according to the model of an ideal 
gas as function of temperature. The specific heat capacity is calculated as a 
polynomial fit to specific heat capacity data calculated according to the NASA 
Glenn Coefficients. The data was calculated for a temperature varying from 200 K 
to 1000 K. Note that this function does not consider the actual water mass fraction. 
To calculate the specific enthalpy of the vaporous water that is actually present, 
use the function
<a href=\"Modelica://SorpLib.Media.IdealGasVaporMixtures.Interfaces.PartialIdealGasWaterVaporMixture.enthalpyOfVapor\">SorpLib.Media.IdealGasVaporMixtures.Interfaces.PartialIdealGasWaterVaporMixture.enthalpyOfVapor</a>.
</p>

<h4>References</h4>
<ul>
  <li>
  McBride, B.J. and Zehe, M.J. and Gordon, S. (2002). NASA Glenn Coefficients for Calculating Thermodynamic Properties of Individual Species, Technical report, NASA/TP—2002-211556. URL: https://ntrs.nasa.gov/citations/20020085330.
  </li>
</ul>

</html>",   revisions="<html>
<ul>
  <li>
  November 27, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
  end enthalpyOfCondensingGas;
  //
  // Add new functions
  //  
  function saturationTemperature
    "Returns saturation temperature of condensing fluid (i.e., water)"
    extends Modelica.Icons.Function;

    //
    // Definition of inputs
    //
    input Modelica.Units.SI.Pressure p_sat
      "Saturation pressure"
      annotation (Dialog(tab="General",group="Inputs"));

    //
    // Definition of outputs
    //
    output Modelica.Units.SI.Temperature T_sat
      "Saturation temperature"
      annotation (Dialog(tab="General",group="Outputs", enable=false));

    //
    // Definition of protected funtions
    //
protected
    function p_sat_T_sat
      "Function used to solve non-linear equation 0 = p_sat(T_sat) - p_sat"
      extends Modelica.Math.Nonlinear.Interfaces.partialScalarFunction;

      //
      // Definition of inputs
      //
      input Modelica.Units.SI.Pressure p_sat
        "Saturation pressure";

    algorithm
      y :=saturationPressureH2O_T(T_sat=u) - p_sat
        "Zero point problem";
    end p_sat_T_sat;

  algorithm
    T_sat := Modelica.Math.Nonlinear.solveOneNonlinearEquation(
      f = function p_sat_T_sat(p_sat=p_sat),
      u_min = 50,
      u_max = T_water_crit-1,
      tolerance = 100*Modelica.Constants.eps)
      "Saturation temperature";

    //
    // Annotations
    //
    annotation (Inline=false,
      InlineAfterIndexReduction=false,
      LateInline=true,
      inverse(p_sat=saturationPressure(Tsat=T_sat)),
      Documentation(info="<html>
<p>
This function calculates the vapor temperature as function of the vapor pesssure
by solving a non-linear equation via the very efficient function
<a href=\"Modelica://Modelica.Math.Nonlinear.solveOneNonlinearEquation\">Modelica.Math.Nonlinear.solveOneNonlinearEquation</a>.
</p>
</html>",   revisions="<html>
<ul>
  <li>
  November 27, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
  end saturationTemperature;

protected 
  function gasMixtureViscosity
    "Returns viscosities of gas mixtures (at low pressures according to the Wilke method)"
    extends Modelica.Icons.Function;

    //
    // Definition of inputs
    //
    input Modelica.Units.SI.MoleFraction[nX] yi
      "Mole fractions"
    annotation (Dialog(tab="General",group="Inputs"));
    input Modelica.Units.SI.MolarMass[nX] M
      "Mole masses"
    annotation (Dialog(tab="General",group="Inputs"));
    input Modelica.Units.SI.DynamicViscosity[nX] eta
      "Pure component viscosities"
    annotation (Dialog(tab="General",group="Inputs"));

    //
    // Definition of outputs
    //
    output Modelica.Units.SI.DynamicViscosity etam
      "Viscosity of the mixture"
    annotation (Dialog(tab="General",group="Outputs", enable=false));

    //
    // Definition of variables
    //
protected
    Real fi[nX,nX];

  algorithm
    for i in 1:nX loop
      //
      // Check if calculation is possible
      //
      assert(fluidConstants[i].hasDipoleMoment,"Dipole moment for " +
        fluidConstants[i].chemicalFormula +
        " not known. Can not compute viscosity.");
      assert(fluidConstants[i].hasCriticalData, "Critical data for " +
        fluidConstants[i].chemicalFormula +
        " not known. Can not compute viscosity.");

      //
      // Calculate viscosity
      //
      for j in 1:nX loop
        if i==1 then
          fi[i,j] := (1 + (eta[i]/eta[j])^(1/2)*(M[j]/M[i])^(1/4))^2/
            (8*(1 + M[i]/M[j]))^(1/2);
        elseif j<i then
            fi[i,j] := eta[i]/eta[j]*M[j]/M[i]*fi[j,i];
          else
            fi[i,j] := (1 + (eta[i]/eta[j])^(1/2)*(M[j]/M[i])^(1/4))^2/
            (8*(1 + M[i]/M[j]))^(1/2);
        end if;
      end for;
    end for;

    etam := sum(yi[i]*eta[i]/sum(yi[j]*fi[i,j] for j in 1:nX) for i in 1:nX);

    //
    // Annotations
    //
    annotation (smoothOrder=2, Documentation(info="<html>  
<p>
Simplification of the kinetic theory (Chapman and Enskog theory) approach 
neglecting the second-order effects. This function is a copy of function 
<a href=\"Modelica://Modelica.Media.IdealGases.Common.MixtureGasNasa.gasMixtureViscosity\">Modelica.Media.IdealGases.Common.MixtureGasNasa.gasMixtureViscosity</a>, 
to be able to use this function within the curent media model.
</p>  
</html>", revisions="<html>
<ul>
  <li>
  November 27, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
  end gasMixtureViscosity;

  function lowPressureThermalConductivity
    "Returns thermal conductivity (at low pressure according to the Mason and Saxena modification)"
    extends Modelica.Icons.Function;

    //
    // Definition of inputs
    //
    input Modelica.Units.SI.MoleFraction[nX] y
      "Mole fraction of the components in the gas mixture"
    annotation (Dialog(tab="General",group="Inputs"));
    input Modelica.Units.SI.Temperature T
      "Temperature"
    annotation (Dialog(tab="General",group="Inputs"));
    input Modelica.Units.SI.Temperature[nX] Tc
      "Critical temperatures"
    annotation (Dialog(tab="General",group="Inputs"));
    input Modelica.Units.SI.AbsolutePressure[nX] Pc
      "Critical pressures"
    annotation (Dialog(tab="General",group="Inputs"));
    input Modelica.Units.SI.MolarMass[nX] M
      "Molecular weights"
    annotation (Dialog(tab="General",group="Inputs"));
    input Modelica.Units.SI.ThermalConductivity[nX] lambda
      "Thermal conductivities of the pure gases"
    annotation (Dialog(tab="General",group="Inputs"));

    //
    // Definition of outputs
    //
    output Modelica.Units.SI.ThermalConductivity lambdam
      "Thermal conductivity of the gas mixture"
    annotation (Dialog(tab="General",group="Outputs", enable=false));

    //
    // Definition of constants
    //
protected
    constant Real epsilon = 1.0
      "Numerical constant near unity";

    //
    // Definition of variables
    //
    Modelica.Units.SI.MolarMass[nX] gamma;
    Real[nX] Tr
      "Reduced temperature";
    Real[nX,nX] A
      "Mason and Saxena Modification";

  algorithm
    for i in 1:nX loop
      gamma[i] := 210*(Tc[i]*M[i]^3/Pc[i]^4)^(1/6);
      Tr[i] := T/Tc[i];

    end for;

    for i in 1:nX loop
      for j in 1:nX loop
        A[i,j] := epsilon*(1 + (gamma[j]*(Modelica.Math.exp(0.0464*Tr[i]) -
          Modelica.Math.exp(-0.2412*Tr[i])) /
          (gamma[i]*(Modelica.Math.exp(0.0464*Tr[j]) -
          Modelica.Math.exp(-0.2412*Tr[j])))) ^
          (1/2)*(M[i]/M[j])^(1/4))^2/(8*(1 + M[i]/M[j]))^(1/2);
      end for;
    end for;

    lambdam :=
      sum(y[i]*lambda[i]/(sum(y[j]*A[i,j] for j in 1:nX)) for i in 1:nX);

    //
    // Annotations
    //
    annotation (smoothOrder=2, Documentation(info="<html>  
<p>
This function applies the Masson and Saxena modification of the Wassiljewa 
Equation for the thermal conductivity for gas mixtures of n elements at low 
pressure. This function is a copy of function 
<a href=\"Modelica://Modelica.Media.IdealGases.Common.MixtureGasNasa.lowPressureThermalConductivity\">Modelica.Media.IdealGases.Common.MixtureGasNasa.lowPressureThermalConductivity</a>, 
to be able to use this function within the curent media model.
</p>  
</html>", revisions="<html>
<ul>
  <li>
  November 27, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
  end lowPressureThermalConductivity;
  //
  // Annotations
  //    
  annotation (Documentation(info="<html>
<p>
This model extends the medium model of an ideal gas-vapor mixture provided
in the Modelica Standard Library by additional functions needed in SorpLib.
Furthermore, the media model is modified so that it can describe an ideal gas 
mixture with any number of non-condensing components and one ideal condensing 
component. The condensing component is water and must always be the last component
when defining a medium model. The properties of the ideal gas mixture of the 
non-condensing components are calculated according to the media model
<a href=\"Modelica://Modelica.Media.IdealGases.Common.MixtureGasNasa\">Modelica.Media.IdealGases.Common.MixtureGasNasa</a>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 27, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end PartialIdealGasWaterVaporMixture;
