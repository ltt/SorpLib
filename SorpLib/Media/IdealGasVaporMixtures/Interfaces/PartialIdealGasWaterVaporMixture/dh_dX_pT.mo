within SorpLib.Media.IdealGasVaporMixtures.Interfaces.PartialIdealGasWaterVaporMixture;
function dh_dX_pT
  "Returns partial derivative of specific enthalpy w.r.t. mass fractions at constant pressure and temperature"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input ThermodynamicState state
    "Thermodynamic state record"
    annotation (Dialog(tab="General",group="Inputs"));

  //
  // Definition of outputs
  //
  output Real[nX] dh_dX_pT(each unit="J.kg/(kg2)")
    "Partial derivative of specific enthalpy w.r.t. mass fractions at constant 
    pressure and temperature"
    annotation (Dialog(tab="General",group="Outputs", enable=false));

  //
  // Definition of variables
  //
protected
  Modelica.Units.SI.MassFraction[nX] x=
    moistAirToDryAirMassFractions(X=state.X)
    "Mass fractions per dry air";
  Modelica.Units.SI.MassFraction x_sat=
    dryMassFractionSaturation(state=state)
    "Saturation mass fraction of condensing component per dry air";

  Real[nX, nX] dx_dX(each unit="kg.kg/(kg.kg)")=
    ddryAirMassFractions_dX(X=state.X)
    "Partial derivatives of mass fractions given per dry air mass w.r.t. mass
    fractions given per moist air mass: Rows are dry air mass and columns are
    moist air mass";
  Real[nX] dx_sat_dX_pT(each unit="kg.kg/(kg.kg)")=
    ddryMassFractionSaturation_dX_pT(state=state)
    "Partial derivatives of saturated wasser mass fraction per dry air mass 
    w.r.t. mass fractions given per moist air mass at constant pressure and 
    temperature";
  Real[nX-1,nX-1] dx_dry_dx(each unit="kg.kg/(kg.kg)")
    "Partial derivatives of mass fractions of dry air per dry air w.r.t. mass
    fractions of dry air per dry air";

  Modelica.Units.SI.SpecificEnthalpy[nX-1] h_dryAir
    "Specific enthalpies of dry air components";
  Modelica.Units.SI.SpecificEnthalpy h
    "Specific enthalpy per dry air mass";

algorithm
  //
  // Calculate specific enthalpy and partial derivatives per dry air mass
  //
  for ind_x in 1:nX-1 loop
    for ind_der in 1:nX-1 loop
      if ind_x == ind_der then
        dx_dry_dx[ind_x,ind_der] := 1/sum(x[1:nX-1]) - x[ind_x]/sum(x[1:nX-1])^2
          "Partial derivatives of mass fractions of dry air per dry air w.r.t. 
          mass fractions of dry air per dry air: Diagonal.";

      else
        dx_dry_dx[ind_x,ind_der] :=-x[ind_x]/sum(x[1:nX-1])^2
          "Partial derivatives of mass fractions of dry air per dry air w.r.t. 
          mass fractions of dry air per dry air: Other elements than diagonal.";

      end if;
    end for;
  end for;

  h_dryAir :={Modelica.Media.IdealGases.Common.Functions.h_T(
    data=data[i],
    T=state.T,
    exclEnthForm=true,
    refChoice=Modelica.Media.Interfaces.Choices.ReferenceEnthalpy.UserDefined,
    h_off=h_dryAir_off) for i in 1:nX-1}
    "Specific enthalpies of dry air components";

  if Modelica.Math.isEqual(s1=x[nX], s2=0, eps=100*Modelica.Constants.eps) then
    h := enthalpyOfNonCondensingGas(T=state.T, X=x)
      "Specific enthalpy equals specific enthalpy of unsaturated dry air without 
        water: Calculated applying the law of ideal gas mixtures.";
    dh_dX_pT :=
      {sum({sum(dx_dry_dx[1:nX-1,ind_x] .* h_dryAir) for ind_x in 1:nX-1} .*
      dx_dX[1:nX-1,ind_X]) for ind_X in 1:nX}
      "Partial derivative equals partial derivative of unsaturated dry air without 
      water: Calculated applying the law of ideal gas mixtures.";

  elseif x[nX] <= x_sat then
    h := enthalpyOfNonCondensingGas(T=state.T, X=x) +
      x[nX] * enthalpyOfCondensingGas(T=state.T)
      "Specific enthalpy equals specific enthalpy of unsaturated dry air and water 
        vapor: Calculated applying the law of ideal gas mixtures.";
    dh_dX_pT :=
      {sum({sum(dx_dry_dx[1:nX-1,ind_x] .* h_dryAir) for ind_x in 1:nX-1} .*
      dx_dX[1:nX-1,ind_X]) for ind_X in 1:nX} .+
      1 .* enthalpyOfCondensingGas(T=state.T) .* dx_dX[nX,1:nX]
      "Partial derivative equals partial derivative of unsaturated dry air and water 
      vapor: Calculated applying the law of ideal gas mixtures.";

  elseif state.T > T_water_trp then
    h := enthalpyOfNonCondensingGas(T=state.T, X=x) +
      x_sat * enthalpyOfCondensingGas(T=state.T) +
      (x[nX]-x_sat) * enthalpyOfLiquid(p=state.p, T=state.T)
      "Specific enthalpy equals specific enthalpy of saturated dry air, water
        vapor, liquid water: Calculated applying the law of ideal gas mixtures.";
    dh_dX_pT :=
      {sum({sum(dx_dry_dx[1:nX-1,ind_x] .* h_dryAir) for ind_x in 1:nX-1} .*
      dx_dX[1:nX-1,ind_X]) for ind_X in 1:nX} .+
      1 .* enthalpyOfCondensingGas(T=state.T) .* dx_sat_dX_pT .+
      (dx_dX[nX,1:nX] .- dx_sat_dX_pT) .* enthalpyOfLiquid(p=state.p, T=state.T)
      "Partial derivative equals partial derivative of saturated dry air, water
      vapor, liquid water: Calculated applying the law of ideal gas mixtures.";

  else
    h := enthalpyOfNonCondensingGas(T=state.T, X=x) +
      x_sat * enthalpyOfCondensingGas(T=state.T) +
      (x[nX]-x_sat) * enthalpyOfSolid(p=state.p, T=state.T)
      "Specific enthalpy equals specific enthalpy of saturated dry air, water
        vapor, solid water: Calculated applying the law of ideal gas mixtures.";
    dh_dX_pT :=
      {sum({sum(dx_dry_dx[1:nX-1,ind_x] .* h_dryAir) for ind_x in 1:nX-1} .*
      dx_dX[1:nX-1,ind_X]) for ind_X in 1:nX} .+
      1 .* enthalpyOfCondensingGas(T=state.T) .* dx_sat_dX_pT .+
      (dx_dX[nX,1:nX] .- dx_sat_dX_pT) .* enthalpyOfSolid(p=state.p, T=state.T)
      "Partial derivative equals partial derivative of saturated dry air, water
      vapor, solid water: Calculated applying the law of ideal gas mixtures.";

  end if;

  //
  // Convert result
  //
  dh_dX_pT := dh_dX_pT ./ (1 + x[nX]) .- h ./ (x[nX] + 1) ^2 .* dx_dX[nX,1:nX]
    "Partial derivatives per moist air mass";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function calculates the partial derivative of the specific enthalpy with
respect to mass fractions at constant pressure and temperature as function of
the state record.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 27, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end dh_dX_pT;
