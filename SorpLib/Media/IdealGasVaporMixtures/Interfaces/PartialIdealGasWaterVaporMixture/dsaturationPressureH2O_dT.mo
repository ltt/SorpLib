within SorpLib.Media.IdealGasVaporMixtures.Interfaces.PartialIdealGasWaterVaporMixture;
function dsaturationPressureH2O_dT
  "Returns partial derivative of saturatation pressure of water w.r.t. temperature"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.Temperature T_sat
    "Saturation temperature"
    annotation (Dialog(tab="General",group="Inputs"));

  //
  // Definition of outputs
  //
  output Modelica.Media.Common.DerPressureByTemperature dp_sat_dT
    "First-order partial derivative of saturation pressure w.r.t. temperature"
    annotation (Dialog(tab="General",group="Outputs", enable=false));

  //
  // Definition of constants
  //
protected
  constant Real[2] coeff_s = {-13.9281690, 34.7078238}
    "Coefficients of the gas-solid boundary";
  constant Real[6] coeff_l = {-7.85951783, 1.84408259, -11.7866497,
    22.6807411, -15.9618719, 1.80122502}
    "Coefficients of the gas-liquid boundary";

  constant Real[2] exp_s = {-1.5, -1.25}
    "Exponents of the gas-solid boundary";
  constant Real[6] exp_l = {1, 1.5, 3,
    3.5, 4, 7.5}
    "Exponents of the gas-liquid boundary";

  //
  // Definition of variables
  //
  Modelica.Units.SI.Pressure p_sat_s
    "Saturation pressure of gas-solid boundary";
  Modelica.Media.Common.DerPressureByTemperature dp_sat_s_dT
    "First-order partial derivative of saturation pressure of gas-solid boundary
    w.r.t. temperature";

  Modelica.Units.SI.Pressure p_sat_l
    "Saturation pressure of gas-liquid boundary";
  Modelica.Media.Common.DerPressureByTemperature dp_sat_l_dT
    "First-order partial derivative of saturation pressure of gas-liquid boundary
    w.r.t. temperature";

  Real T_red_s = T_sat/T_water_trp
    "Reduced temperature required for gas-solid boundary";
  Real T_red_fl = 1 - T_sat/T_water_crit
    "Reduced temperature required for gas-liquid boundary";

  Real lambda(unit="1") = SorpLib.Numerics.smoothTransition(
    x=T_sat, transitionPoint=T_water_trp, transitionLength=1, noDiff=3)
    "Transiation factor";
  Real dlambda_dT(unit="1/K") = SorpLib.Numerics.smoothTransition_der(
    x=T_sat, transitionPoint=T_water_trp, transitionLength=1, noDiff=3,
    x_der=1)
    "First-order derivative of transiation factor w.r.t. temperature";

algorithm
  //
  // Calculate pressures
  //
  p_sat_s := p_water_trp * exp(coeff_s[1] - coeff_s[1] * T_red_s ^ exp_s[1] +
    coeff_s[2] - coeff_s[2] * T_red_s ^ exp_s[2])
    "Saturation pressure of gas-solid boundary";
  p_sat_l := p_water_crit * exp(T_water_crit/T_sat *
    (coeff_l[1] * T_red_fl ^ exp_l[1] +
    coeff_l[2] * T_red_fl ^ exp_l[2] +
    coeff_l[3] * T_red_fl ^ exp_l[3] +
    coeff_l[4] * T_red_fl ^ exp_l[4] +
    coeff_l[5] * T_red_fl ^ exp_l[5] +
    coeff_l[6] * T_red_fl ^ exp_l[6]))
    "Saturation pressure of gas-liquid boundary";

  //
  // Calculate first-order partial derivatives of pressures w.r.t. temperature
  //
  dp_sat_s_dT := -p_sat_s/T_sat *
    (coeff_s[1] * exp_s[1] * T_red_s ^ exp_s[1] +
    coeff_s[2] * exp_s[2] * T_red_s ^ exp_s[2])
    "First-order partial derivative of saturation pressure of gas-solid boundary
    w.r.t. temperature";
  dp_sat_l_dT := -p_sat_l/T_sat * (
    log(p_sat_l/p_water_crit) +
    coeff_l[1] +
    coeff_l[2] * exp_l[2] * T_red_fl^(exp_l[2]-1) +
    coeff_l[3] * exp_l[3] * T_red_fl^(exp_l[3]-1) +
    coeff_l[4] * exp_l[4] * T_red_fl^(exp_l[4]-1) +
    coeff_l[5] * exp_l[5] * T_red_fl^(exp_l[5]-1) +
    coeff_l[6] * exp_l[6] * T_red_fl^(exp_l[6]-1))
    "First-order partial derivative of saturation pressure of gas-liquid boundary
    w.r.t. temperature";

  //
  // Check for boundary
  //
  dp_sat_dT :=dlambda_dT*p_sat_s +
    lambda*dp_sat_s_dT +
    (-dlambda_dT)*p_sat_l +
    (1-lambda)*dp_sat_l_dT
    "First-order partial derivative of saturation pressure w.r.t. temperature";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function calculates the first-order partial saturation pressure of water 
with respect to temperature depending on the temperature. This functions covers 
both, the solid-gas and the gas-liquid boundary. For details, please check the
function
<a href=\"Modelica://SorpLib.Media.IdealGasMixtures.Interfaces.PartialIdealGasMixture.saturationPressureH2O_T\">SorpLib.Media.IdealGasMixtures.Interfaces.PartialIdealGasMixture.saturationPressureH2O_T</a>.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 27, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end dsaturationPressureH2O_dT;
