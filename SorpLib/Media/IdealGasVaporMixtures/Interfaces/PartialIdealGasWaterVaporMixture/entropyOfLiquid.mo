within SorpLib.Media.IdealGasVaporMixtures.Interfaces.PartialIdealGasWaterVaporMixture;
function entropyOfLiquid
  "Returns specific entropy of liquid water (per water mass)"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.Temperature T
    "Temperature"
    annotation (Dialog(tab="General",group="Inputs"));

  //
  // Definition of outputs
  //
  output Modelica.Units.SI.SpecificEntropy s
    "Specific entropy of liquid water"
    annotation (Dialog(tab="General",group="Outputs", enable=false));

  //
  // Definition of constants
  //
protected
  constant Real[10] coefficients=
    {4.66348457415926e-19, -1.16579701193758e-15, 9.49925152355854e-13,
    5.81553479401465e-11, -6.55387915784314e-7, 5.30868446475702e-4,
    -2.18234813641286e-1, 5.13043718148301e1, -6.58121774333134e3,
    3.63215816205503e5}
    "Coefficients of polynomical function";
  constant Real[10] exponents=
    {9, 8, 7, 6, 5, 4, 3, 2, 1, 0} .- 1
    "Exponents of polynomical function";

algorithm
  s := s_water_ref +
    SorpLib.Media.Functions.Utilities.intGeneralizedFunction_dT(
      T=T,
      T_ref=T_water_crit,
      z_ref=1,
      coefficients=coefficients,
      exponents=exponents,
      approach=SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionTemperature) -
    SorpLib.Media.Functions.Utilities.intGeneralizedFunction_dT(
      T=T_water_ref,
      T_ref=T_water_crit,
      z_ref=1,
      coefficients=coefficients,
      exponents=exponents,
      approach=SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionTemperature)
    "Specific entropy of liquid water";

  //
  // Assertations
  //
  if print_warnings then
    assert(T_water_trp-1 <= T and T <= 573.15,
      "Temperature (" + String(T) + " K) is not between 272.16 and 573.15 K. " +
      "Calculation of specific entropy of liquid may not be valid!",
      level = AssertionLevel.warning);
  end if;

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function calculates the specific entropy of the condensing component's
(i.e., water) liquid phase per water mass according to the model of an ideal 
liquid as function of pressure and temperature. The specific heat capacity is 
calculated as a polynomial fit to specific heat capacity data calculated at 
saturated liquid state with the reference equation of state for water. The data 
was calculated for temperature varying from 273.16 K to 573.15 K. Note that this 
function does not consider the actual water mass fraction.
</p>

<h4>References</h4>
<ul>
  <li>
  The International Association for the Properties of Water and Steam (2009). Revised Release on the Equation of State 2006 for H2O Ice Ih. URL: https://iapws.org/relguide/Ice-2009.html.
  </li>
</ul>

</html>", revisions="<html>
<ul>
  <li>
  November 27, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end entropyOfLiquid;
