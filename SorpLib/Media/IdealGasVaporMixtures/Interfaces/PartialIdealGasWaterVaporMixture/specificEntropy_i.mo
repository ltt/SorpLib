within SorpLib.Media.IdealGasVaporMixtures.Interfaces.PartialIdealGasWaterVaporMixture;
function specificEntropy_i
  "Returns specific entropy of component i per componentmass"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Integer ind_component
    "Component index whose specific entropy is to be calculated"
    annotation (Dialog(tab="General", group="Inputs"));
  input ThermodynamicState state
    "Thermodynamic state record"
    annotation (Dialog(tab="General",group="Inputs"));

  //
  // Definition of outputs
  //
  output Modelica.Units.SI.SpecificEntropy s
    "Specific entropy of component ind_component"
    annotation (Dialog(tab="General", group="Outputs", enable=false));

  //
  // Definition of variables
  //
protected
  Modelica.Units.SI.MassFraction[nX] x=
    moistAirToDryAirMassFractions(X=state.X)
    "Mass fractions per dry air";
  Modelica.Units.SI.MassFraction x_sat=
    dryMassFractionSaturation(state=state)
    "Saturation mass fraction of condensing component per dry air";

  Modelica.Units.SI.Pressure[nX] p_i=
    partialPressures(state=state)
    "Partial pressures";

algorithm
  if ind_component <> nX then
    s := s_dryAir_off + Modelica.Media.IdealGases.Common.Functions.s0_T(
      data=data[ind_component], T=state.T) - Modelica.Constants.R /
      MMX[ind_component] * Modelica.Math.log(max(p_i[ind_component],
      Modelica.Constants.eps) / reference_p)
      "Specific entropy equals specific entropy of dry air component";

  elseif Modelica.Math.isEqual(s1=x[nX], s2=0, eps=100*Modelica.Constants.eps) then
    s := 0
      "Specific entropy equals specific entropy of water but water is not
      within the ideal gas-vapor mixture";

  elseif x[nX] <= x_sat then
    s := entropyOfCondensingGas(T=state.T, p=p_i[nX])
      "Specific entropy equals specific entropy of water: Unsaturated dry
      air";

  elseif state.T > T_water_trp then
    s := x_sat/x[nX] * entropyOfCondensingGas(T=state.T, p=p_i[nX]) +
      (x[nX]-x_sat)/x[nX] * entropyOfLiquid(T=state.T)
      "Specific entropy equals specific entropy of water: Saturated dry
      air with liquid water";

  else
    s := x_sat/x[nX] * entropyOfCondensingGas(T=state.T, p=p_i[nX]) +
      (x[nX]-x_sat)/x[nX] * entropyOfSolid(T=state.T)
      "Specific entropy equals specific entropy of water: Saturated dry
      air with solid water";

  end if;

  //
  // Annotations
  //
  annotation(Documentation(info="<html>
<p>
This function calculates the specific entropy of component i per component mass.
</p>
</html>",
        revisions="<html>
<ul>
  <li>
  November 27, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end specificEntropy_i;
