﻿within SorpLib.Media.IdealGasVaporMixtures.Interfaces.PartialIdealGasWaterVaporMixture;
function entropyOfNonCondensingGas
  "Returns specific entropy of non-condensing components (i.e., dry air) (per dry air mass)"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.Temperature T
    "Temperature"
    annotation (Dialog(tab="General",group="Inputs"));
  input Modelica.Units.SI.MassFraction[nX] X
    "Mass fractions"
    annotation (Dialog(tab="General",group="Inputs"));
  input Modelica.Units.SI.Pressure[nX] p_i
    "Partial pressures"
    annotation (Dialog(tab="General",group="Inputs"));

  //
  // Definition of outputs
  //
  output Modelica.Units.SI.SpecificEntropy s
    "Specific entropy of non-condensing components (i.e., dry air)"
    annotation (Dialog(tab="General",group="Outputs", enable=false));

algorithm
  s := X[1:nX-1] / sum(X[1:nX-1]) * {s_dryAir_off +
    Modelica.Media.IdealGases.Common.Functions.s0_T(data=data[i], T=T) -
    Modelica.Constants.R / MMX[i] * Modelica.Math.log(max(p_i[i],
    Modelica.Constants.eps) / reference_p) for i in 1:nX-1}
    "Specific entropy of non-condensing components (i.e., dry air)";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function calculates the specific entropy of the non-condensing components
(i.e., dry air) per moist air mass according to the model of an ideal gas mixture 
as function of partial pressures, temperature, and mass fractions. The specific 
entropy is calculated using NASA Glenn Coefficients. For more details, check the 
functions
<a href=\"Modelica://Modelica.Media.IdealGases.Common.MixtureGasNasa.specificEntropy\">Modelica.Media.IdealGases.Common.MixtureGasNasa.specificEntropy</a>,
<a href=\"Modelica://Modelica.Media.IdealGases.Common.MixtureGasNasa.specificEntropyOfpTX\">Modelica.Media.IdealGases.Common.MixtureGasNasa.specificEntropyOfpTX</a>,
and
<a href=\"Modelica://Modelica.Media.IdealGases.Common.MixtureGasNasa.s_TX\">Modelica.Media.IdealGases.Common.MixtureGasNasa.s_TX</a>.
</p>

<h4>References</h4>
<ul>
  <li>
  McBride, B.J. and Zehe, M.J. and Gordon, S. (2002). NASA Glenn Coefficients for Calculating Thermodynamic Properties of Individual Species, Technical report, NASA/TP—2002-211556. URL: https://ntrs.nasa.gov/citations/20020085330.
  </li>
</ul>

</html>", revisions="<html>
<ul>
  <li>
  November 27, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end entropyOfNonCondensingGas;
