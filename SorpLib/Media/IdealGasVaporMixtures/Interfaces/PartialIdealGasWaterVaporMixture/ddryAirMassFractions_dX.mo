within SorpLib.Media.IdealGasVaporMixtures.Interfaces.PartialIdealGasWaterVaporMixture;
function ddryAirMassFractions_dX
  "Partial derivatives of 'dry air'-based mass fractions w.r.t. 'moist air'-based mass fractions"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.MassFraction[nX] X
    "Mass fractions based on moist air mass"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of outputs
  //
  output Real[nX, nX] dx_dX(each unit="kg.kg/(kg.kg)")
    "Partial derivatives of mass fractions given per dry air mass w.r.t. mass
    fractions given per moist air mass: Rows are dry air mass and columns are
    moist air mass"
    annotation (Dialog(tab="General", group="Outputs", enable=false));

algorithm
  dx_dX :=zeros(nX, nX)
    "Partial derivatives of mass fractions given per dry air mass w.r.t. mass
    fractions given per moist air mass: Most elements are zero.";

  dx_dX[nX, nX] := 1 / (X[nX] - 1)^2
    "Partial derivatives of mass fractions given per dry air mass w.r.t. mass
    fractions given per moist air mass: Water w.r.t. water.";

  for ind in 1:nX-1 loop
    dx_dX[ind, ind] := (1 + X[nX] / (1 - X[nX]))
      "Partial derivatives of mass fractions given per dry air mass w.r.t. mass
      fractions given per moist air mass: Diagonal.";
    dx_dX[ind, nX] := X[ind]  / (X[nX] - 1)^2
      "Partial derivatives of mass fractions given per dry air mass w.r.t. mass
      fractions given per moist air mass: W.r.t. water.";

  end for;

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function calculates the partial derivatives of mass fractions given per dry
air mass with respect to mass fractions given per moist air mass.
</p>

</html>", revisions="<html>
<ul>
  <li>
  November 27, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end ddryAirMassFractions_dX;
