within SorpLib.Media.IdealGasVaporMixtures.Interfaces.PartialIdealGasWaterVaporMixture;
function ddryMassFractionSaturation_dp_TX
  "Returns partial derivative of saturated wasser mass fraction per dry air mass w.r.t. pressure at constant temperature and mass fractions"
  extends Modelica.Icons.Function;

  //
  // Deifnition of inputs
  //
  input ThermodynamicState state
    "Thermodynamic state record"
    annotation (Dialog(tab="General",group="Inputs"));

  //
  // Deifnition of outputs
  //
  output Real dx_sat_dp_TX(unit="kg/(kg.Pa)")
    "Partial derivative of saturated wasser mass fraction per dry air mass 
    w.r.t. pressure at constant temperature and mass fractions"
    annotation (Dialog(tab="General",group="Outputs", enable=false));

  //
  // Definition of variables
  //
protected
  Modelica.Units.SI.Pressure p_sat=
    saturationPressureH2O_T(T_sat = state.T)
    "Saturation pressure";

algorithm
  dx_sat_dp_TX := -MMX[nX] / (state.Y[1:nX-1]./sum(state.Y[1:nX-1]) * MMX[1:nX-1]) *
    p_sat / max((state.p - p_sat)^2, Modelica.Constants.small)
    "Partial derivative of saturated wasser mass fraction per dry air mass 
    w.r.t. pressure at constant temperature and mass fractions";

  //
  // Assertations
  //
  if print_warnings then
    assert(p_sat <= state.p,
      "Saturation pressure (" + String(p_sat) + " Pa) is greater than total " +
      "pressure (" + String(state.p) + " Pa). Thus, the calculation of the " +
      "saturation mass fraction is not sound!",
      level = AssertionLevel.warning);
  end if;

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function calculates the partial derivative of the saturation mass fraction 
of the condensing component (i.e., water) per dry air with respect to pressure
at constant temperature and mass fractions as function of the state record. Note 
that the saturation mass fraction can only be calculated if the partial pressure 
of the condensing component is less than the pressure of the ideal gas-vapor 
mixture.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 27, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end ddryMassFractionSaturation_dp_TX;
