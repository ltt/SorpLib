within SorpLib.Media.IdealGasVaporMixtures.Interfaces.PartialIdealGasWaterVaporMixture;
function dmassToMoleFractions_dX
  "Returns partial derivatives of mole fractions w.r.t mass fractions"
  extends Modelica.Icons.Function;

  //
  // Definition  of inputs
  //
  input Modelica.Units.SI.MassFraction[nX] X
    "Mass fractions"
    annotation (Dialog(tab="General",group="Inputs"));

  //
  // Definition  of outputs
  //
  output Real[nX, nX] dY_dX(each unit="mol.kg/(mol.kg)")
    "Partial derivatives of mole fractions w.r.t mass fractions: Rows are mole
    fractions and colums are mass fractions"
    annotation (Dialog(tab="General",group="Outputs", enable=false));

  //
  // Definition  of variables
  //
protected
  Modelica.Units.SI.MolarMass MM_mix = 1 / sum(X ./ MMX)
    "Molar mass of mixture";

algorithm
  for ind_row in 1:nX loop
    for ind_col in 1:nX loop
      if ind_row == ind_col then
        dY_dX[ind_row, ind_col] :=
          MM_mix / MMX[ind_row] - X[ind_row] * MM_mix^2 / MMX[ind_row]^2
          "Partial derivatives of mole fraction i w.r.t mass fraction i";

      else
        dY_dX[ind_row, ind_col] :=
          -X[ind_row] * MM_mix^2 / (MMX[ind_row] * MMX[ind_col])
          "Partial derivatives of mole fraction i w.r.t mass fraction j";

      end if;
    end for;
  end for;

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function calculates the partial derivatives of mole fractions with respect
to mass fractions (i.e., rows = mole fractions; colums = mass fractions) as 
function of mass fractions.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 27, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end dmassToMoleFractions_dX;
