within SorpLib.Media.IdealGasVaporMixtures.Interfaces.PartialIdealGasWaterVaporMixture;
function partialPressures
  "Returns partial pressures"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input ThermodynamicState state
    "Thermodynamic state record"
    annotation (Dialog(tab="General",group="Inputs"));

  //
  // Definition of outputs
  //
  output Modelica.Units.SI.Pressure[nX] p_i
    "Partial pressures"
    annotation (Dialog(tab="General",group="Outputs", enable=false));

  //
  // Definition of variables
  //
protected
  Modelica.Units.SI.Pressure p_sat=
    saturationPressure(Tsat=state.T)
    "Saturation pressure of condensing fluid (i.e., water)";
  Modelica.Units.SI.Pressure p_dryAir
    "Partial pressure of dry air (i.e., without condensing component)";

  Modelica.Units.SI.SpecificVolume v_dryAir
    "Specific volume of dry air (i.e., without condensing component) related
      to mass of moist air";

algorithm
  if (state.X[nX] / (1 - state.X[nX])) >
    dryMassFractionSaturation(state=state) then
    p_dryAir :=state.p - p_sat
      "Partial pressure of dry air (i.e., without partial pressure of water)";
    v_dryAir := sum(state.X[1:nX-1] ./ MMX[1:nX-1]) .* Modelica.Constants.R .*
      state.T ./ p_dryAir
      "Specific volume of dry air (i.e., without condensing component) related
        to mass of moist air";

    p_i[1:nX-1] := state.X[1:nX-1] ./ MMX[1:nX-1] .* Modelica.Constants.R .*
      state.T ./ v_dryAir
      "Partial pressures of non-condensing components";
    p_i[nX] := p_sat
      "Partial pressures of condensing component";

  else
    p_i := state.Y .* state.p
      "Partial pressures";

  end if;

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function calculates the partial pressures as function of the state record.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 27, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end partialPressures;
