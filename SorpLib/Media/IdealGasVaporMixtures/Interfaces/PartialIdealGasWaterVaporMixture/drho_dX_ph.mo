within SorpLib.Media.IdealGasVaporMixtures.Interfaces.PartialIdealGasWaterVaporMixture;
function drho_dX_ph
  "Returns partial derivative of density w.r.t. mass fractions at constant pressure and specific enthalpy"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input ThermodynamicState state
    "Thermodynamic state record"
    annotation (Dialog(tab="General",group="Inputs"));

  //
  // Definition of outputs
  //
  output Real[nX] drho_dX_ph(each unit="kg.kg/(m3.kg)")
    "Partial derivative of specific enthalpy w.r.t. mass fractions at constant 
      pressure and temperature"
    annotation (Dialog(tab="General",group="Outputs", enable=false));

algorithm
  drho_dX_ph := density_derX(state=state) .- density_derT_p(state=state) .*
    dh_dX_pT(state=state) ./ dh_dT_pX(state=state)
    "Partial derivative of specific enthalpy w.r.t. mass fractions at constant 
      pressure and temperature";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function calculates the partial derivative of the density with resepct to
mass fractions at constant pressure and specific enthalpy as function of the state 
record.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 27, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end drho_dX_ph;
