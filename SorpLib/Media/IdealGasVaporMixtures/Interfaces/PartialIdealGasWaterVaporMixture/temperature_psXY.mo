within SorpLib.Media.IdealGasVaporMixtures.Interfaces.PartialIdealGasWaterVaporMixture;
function temperature_psXY
  "Returns temperature as inverse of function s_pTXY"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.Pressure p
    "Pressure"
    annotation (Dialog(tab="General",group="Inputs"));
  input Modelica.Units.SI.SpecificEntropy s
    "Specific entropy"
    annotation (Dialog(tab="General",group="Inputs"));
  input Modelica.Units.SI.MassFraction[nX] X
    "Mass fractions"
    annotation (Dialog(tab="General",group="Inputs"));
  input Modelica.Units.SI.MoleFraction[nX] Y
    "Mole fractions"
    annotation (Dialog(tab="General",group="Inputs"));

  //
  // Definition of outputs
  //
  output Modelica.Units.SI.Temperature T
    "Temperature"
    annotation (Dialog(tab="General",group="Outputs", enable=false));

  //
  // Definition of protected funtions
  //
protected
  function s_pTXY_inverse
    "Function used to solve non-linear equation 0 = s(p,T,X,Y) - s"
    extends Modelica.Math.Nonlinear.Interfaces.partialScalarFunction;

    //
    // Definition of inputs
    //
    input Modelica.Units.SI.Pressure p
      "Pressure";
    input Modelica.Units.SI.SpecificEntropy s
    "Specific entropy";
    input Modelica.Units.SI.MassFraction[nX] X
      "Mass fractions";
    input Modelica.Units.SI.MoleFraction[nX] Y
      "Mole fractions";

  algorithm
    y :=specificEntropy_pTXY(
        p=p,
        T=u,
        X=X,
        Y=Y) - s
      "Zero point problem";
  end s_pTXY_inverse;

algorithm
  T := Modelica.Math.Nonlinear.solveOneNonlinearEquation(
    f = function s_pTXY_inverse(p=p, s=s, X=X, Y=Y),
    u_min = 200,
    u_max = 573.15,
    tolerance = 1e-6)
    "Temperature";

  //
  // Annotations
  //
  annotation (Inline=false, InlineAfterIndexReduction=false, LateInline=true,
  inverse(s=specificEntropy_pTXY(p=p, T=T, X=X, Y=Y)), Documentation(info="<html>
<p>
This function calculates the temperature of the ideal gas-vapor mixture as function
of pressure, specific entropy, mass fractions, and mole fractions.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 27, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end temperature_psXY;
