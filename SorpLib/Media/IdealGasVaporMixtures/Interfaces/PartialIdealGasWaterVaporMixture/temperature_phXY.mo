within SorpLib.Media.IdealGasVaporMixtures.Interfaces.PartialIdealGasWaterVaporMixture;
function temperature_phXY
  "Returns temperature as inverse of function h_pTXY"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.Pressure p
    "Pressure"
    annotation (Dialog(tab="General",group="Inputs"));
  input Modelica.Units.SI.SpecificEnthalpy h
    "Specific enthalpy"
    annotation (Dialog(tab="General",group="Inputs"));
  input Modelica.Units.SI.MassFraction[nX] X
    "Mass fractions"
    annotation (Dialog(tab="General",group="Inputs"));
  input Modelica.Units.SI.MoleFraction[nX] Y
    "Mole fractions"
    annotation (Dialog(tab="General",group="Inputs"));

  //
  // Definition of outputs
  //
  output Modelica.Units.SI.Temperature T
    "Temperature"
    annotation (Dialog(tab="General",group="Outputs", enable=false));

  //
  // Definition of protected funtions
  //
protected
  function h_pTXY_inverse
    "Function used to solve non-linear equation 0 = h(p,T,X,Y) - h"
    extends Modelica.Math.Nonlinear.Interfaces.partialScalarFunction;

    //
    // Definition of inputs
    //
    input Modelica.Units.SI.Pressure p
      "Pressure";
    input Modelica.Units.SI.SpecificEnthalpy h
    "Specific enthalpy";
    input Modelica.Units.SI.MassFraction[nX] X
      "Mass fractions";
    input Modelica.Units.SI.MoleFraction[nX] Y
      "Mole fractions";

  algorithm
    y :=specificEnthalpy_pTXY(
        p=p,
        T=u,
        X=X,
        Y=Y) - h
      "Zero point problem";
  end h_pTXY_inverse;

algorithm
  T := Modelica.Math.Nonlinear.solveOneNonlinearEquation(
    f = function h_pTXY_inverse(p=p, h=h, X=X, Y=Y),
    u_min = 200,
    u_max = 573.15,
    tolerance = 1e-6)
    "Temperature";

  //
  // Annotations
  //
  annotation (Inline=false, InlineAfterIndexReduction=false, LateInline=true,
  inverse(h=specificEnthalpy_pTXY(p=p, T=T, X=X, Y=Y)), Documentation(info="<html>
<p>
This function calculates the temperature of the ideal gas-vapor mixture as function
of pressure, specific enthalpy, mass fractions, and mole fractions.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 27, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end temperature_phXY;
