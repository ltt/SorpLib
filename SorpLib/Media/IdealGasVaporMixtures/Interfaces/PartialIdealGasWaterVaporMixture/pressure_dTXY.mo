within SorpLib.Media.IdealGasVaporMixtures.Interfaces.PartialIdealGasWaterVaporMixture;
function pressure_dTXY
  "Returns pressure as inverse of function rho_pTXY"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.Density d
    "Density"
    annotation (Dialog(tab="General",group="Inputs"));
  input Modelica.Units.SI.Temperature T
    "Pressure"
    annotation (Dialog(tab="General",group="Inputs"));
  input Modelica.Units.SI.MassFraction[nX] X
    "Mass fractions"
    annotation (Dialog(tab="General",group="Inputs"));
  input Modelica.Units.SI.MoleFraction[nX] Y
    "Mole fractions"
    annotation (Dialog(tab="General",group="Inputs"));

  //
  // Definition of outputs
  //
  output Modelica.Units.SI.Pressure p
    "Pressure"
    annotation (Dialog(tab="General",group="Outputs", enable=false));

  //
  // Definition of protected funtions
  //
protected
  function rho_pTXY_inverse
    "Function used to solve non-linear equation 0 = d(p,T,X,Y) - d"
    extends Modelica.Math.Nonlinear.Interfaces.partialScalarFunction;

    //
    // Definition of inputs
    //
    input Modelica.Units.SI.Density d
      "Density";
    input Modelica.Units.SI.Temperature T
      "Pressure";
    input Modelica.Units.SI.MassFraction[nX] X
      "Mass fractions";
    input Modelica.Units.SI.MoleFraction[nX] Y
      "Mole fractions";

  algorithm
    y :=rho_pTXY(p=u, T=T, X=X, Y=Y) - d
      "Zero point problem";
  end rho_pTXY_inverse;

algorithm
  p := Modelica.Math.Nonlinear.solveOneNonlinearEquation(
    f = function rho_pTXY_inverse(d=d, T=T, X=X, Y=Y),
    u_min = 1e-6,
    u_max = 100*10e5,
    tolerance = 100*Modelica.Constants.eps)
    "Pressure";

  //
  // Annotations
  //
  annotation (Inline=false, InlineAfterIndexReduction=false, LateInline=true,
inverse(d=rho_pTXY(p=p, T=T, X=X, Y=Y)),Documentation(info="<html>
<p>
This function calculates the pressure of the ideal gas-vapor mixture as function
of density, temperature, mass fractions, and mole fractions.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 27, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end pressure_dTXY;
