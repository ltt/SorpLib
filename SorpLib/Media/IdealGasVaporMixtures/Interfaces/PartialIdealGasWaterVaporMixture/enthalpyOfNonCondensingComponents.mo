﻿within SorpLib.Media.IdealGasVaporMixtures.Interfaces.PartialIdealGasWaterVaporMixture;
function enthalpyOfNonCondensingComponents
  "Returns specific enthalpy of non-condensing components (i.e., dry air) (per moist air mass)"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input ThermodynamicState state
    "Thermodynamic state record"
    annotation (Dialog(tab="General",group="Inputs"));

  //
  // Definition of outputs
  //
  output Modelica.Units.SI.SpecificEnthalpy h
    "Specific enthalpy of non-condensing components (i.e., dry air)"
    annotation (Dialog(tab="General",group="Outputs", enable=false));

algorithm
  h := state.X[1:nX-1] * {Modelica.Media.IdealGases.Common.Functions.h_T(
    data=data[i],
    T=state.T,
    exclEnthForm=true,
    refChoice=Modelica.Media.Interfaces.Choices.ReferenceEnthalpy.UserDefined,
    h_off=h_dryAir_off) for i in 1:nX-1}
    "Specific enthalpy of dry air";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function calculates the specific enthalpy of the non-condensing components
(i.e., dry air) per moist air mass according to the model of an ideal gas mixture 
as function of temperature and mass fractions. The specific enthalpy is calculated 
using NASA Glenn Coefficients. For more details, check the function
<a href=\"Modelica://Modelica.Media.IdealGases.Common.MixtureGasNasa.h_TX\">Modelica.Media.IdealGases.Common.MixtureGasNasa.h_TX</a>.</p>

<h4>References</h4>
<ul>
  <li>
  McBride, B.J. and Zehe, M.J. and Gordon, S. (2002). NASA Glenn Coefficients for Calculating Thermodynamic Properties of Individual Species, Technical report, NASA/TP—2002-211556. URL: https://ntrs.nasa.gov/citations/20020085330.
  </li>
</ul>

</html>", revisions="<html>
<ul>
  <li>
  November 27, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end enthalpyOfNonCondensingComponents;
