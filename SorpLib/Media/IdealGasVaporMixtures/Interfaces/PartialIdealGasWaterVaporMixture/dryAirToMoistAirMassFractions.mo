within SorpLib.Media.IdealGasVaporMixtures.Interfaces.PartialIdealGasWaterVaporMixture;
function dryAirToMoistAirMassFractions
  "Convert mass fractions from 'dry air'-based to 'moist air'-based"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.MassFraction[nX] x
    "Mass fractions based on dry air mass"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of outputs
  //
  output Modelica.Units.SI.MassFraction[nX] X
    "Mass fractions based on moist air mass"
    annotation (Dialog(tab="General", group="Outputs", enable=false));

algorithm
  X :=cat(
      1,
      x[1:nX-1] ./ (1 + x[nX]),
      {x[nX] / (1 + x[nX])})
    "Mass fractions based on moist air mass";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function converts mass fractions given per dry air mass to mass fractions
given per moist air mass.
</p>

</html>", revisions="<html>
<ul>
  <li>
  November 27, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end dryAirToMoistAirMassFractions;
