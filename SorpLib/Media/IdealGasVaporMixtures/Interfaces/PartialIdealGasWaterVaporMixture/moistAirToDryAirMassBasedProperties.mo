within SorpLib.Media.IdealGasVaporMixtures.Interfaces.PartialIdealGasWaterVaporMixture;
function moistAirToDryAirMassBasedProperties
  "Converts 'moist air'-specific property to 'dry air'-specific property"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Real z_moistAir
    "Abitrary property given per moist air mass"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.MassFraction X_water
    "Mass fraction of water based on moist air mass"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of outputs
  //
  output Real z_dryAir
    "Abitrary property given per dry air mass"
    annotation (Dialog(tab="General", group="Outputs", enable=false));

algorithm
  z_dryAir := z_moistAir * (1 + X_water / (1 - X_water))
    "Abitrary property given per dry air mass";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function converts an abitrary property given per moist air mass so that it is
given per dry air mass.
</p>

</html>", revisions="<html>
<ul>
  <li>
  November 27, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end moistAirToDryAirMassBasedProperties;
