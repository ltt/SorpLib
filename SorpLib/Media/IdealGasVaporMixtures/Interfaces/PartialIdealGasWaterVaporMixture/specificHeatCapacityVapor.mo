﻿within SorpLib.Media.IdealGasVaporMixtures.Interfaces.PartialIdealGasWaterVaporMixture;
function specificHeatCapacityVapor
  "Returns specific heat capacity cp of condensing component's ideal gas phase (i.e., vapor) (per water mass)"
  extends Modelica.Icons.Function;

  //
  // Deifnition of inputs
  //
  input ThermodynamicState state
    "Thermodynamic state record"
    annotation (Dialog(tab="General",group="Inputs"));

  //
  // Deifnition of outputs
  //
  output Modelica.Units.SI.SpecificHeatCapacity cp
    "Specific heat capacity of vapor"
    annotation (Dialog(tab="General",group="Outputs", enable=false));

  //
  // Definition of constants
  //
protected
  constant Real[10] coefficients=
    {7.03241022996521e-23, -4.12454440465976e-19, 1.05640534898709e-15,
    -1.54797437245421e-12, 1.42577474111776e-9, -8.51385633913246e-7,
    3.26064390458737e-4, -7.52464997116974e-2, 9.42339113729594,
    1.36103845934352e+3}
    "Coefficients of polynomical function";
  constant Real[10] exponents=
    {9, 8, 7, 6, 5, 4, 3, 2, 1, 0}
    "Exponents of polynomical function";

algorithm
  cp := SorpLib.Media.Functions.Utilities.generalizedFunction_T(
    T=state.T,
    T_ref=T_water_crit,
    z_ref=1,
    coefficients=coefficients,
    exponents=exponents,
    approach=SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionTemperature)
    "Specific heat capacity of vapor";

  //
  // Assertations
  //
  if print_warnings then
    assert(200 <= state.T and state.T <= 1000,
      "Temperature (" + String(state.T) + " K) is not between 200 and 1000 K. " +
      "Calculation of specific heat capacity of vapor may not be valid!",
      level = AssertionLevel.warning);
  end if;

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function calculates the specific heat capacity of the condensing component
(i.e., water) at its ideal gas phase (i.e., vapor) as function of the state record.
The specific heat capacity is calculated as a polynomial fit to specific heat capacity
data calculated according to the NASA Glenn Coefficients. The data was calculated
for a temperature varying from 200 K to 1000 K.
</p>

<h4>References</h4>
<ul>
  <li>
  McBride, B.J. and Zehe, M.J. and Gordon, S. (2002). NASA Glenn Coefficients for Calculating Thermodynamic Properties of Individual Species, Technical report, NASA/TP—2002-211556. URL: https://ntrs.nasa.gov/citations/20020085330.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  November 27, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end specificHeatCapacityVapor;
