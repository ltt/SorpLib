within SorpLib.Media.IdealGasVaporMixtures.Interfaces.PartialIdealGasWaterVaporMixture;
function specificHeatCapacitySolid
  "Returns specific heat capacity c of condensing component's solid phase (i.e., ice) (per water mass)"
  extends Modelica.Icons.Function;

  //
  // Deifnition of inputs
  //
  input ThermodynamicState state
    "Thermodynamic state record"
    annotation (Dialog(tab="General",group="Inputs"));

  //
  // Deifnition of outputs
  //
  output Modelica.Units.SI.SpecificHeatCapacity c
    "Specific heat capacity of ice"
    annotation (Dialog(tab="General",group="Outputs", enable=false));

  //
  // Definition of constants
  //
protected
  constant Real[7] coefficients=
    {2.19406894387930e-11, -2.21186473219202e-8, 8.60754642146296e-6,
    -1.57223901063024e-3, 1.24518779128263e-1, 5.27605328970081,
    1.18325880591863e+1}
    "Coefficients of polynomical function";
  constant Real[7] exponents=
    {6, 5, 4, 3, 2, 1, 0}
    "Exponents of polynomical function";

algorithm
  c := SorpLib.Media.Functions.Utilities.generalizedFunction_T(
    T=state.T,
    T_ref=T_water_trp,
    z_ref=1,
    coefficients=coefficients,
    exponents=exponents,
    approach=SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionTemperature)
    "Specific heat capacity of ice";

  //
  // Assertations
  //
  if print_warnings then
    assert(50 <= state.T and state.T <= T_water_trp+1,
      "Temperature (" + String( state.T) + " K) is not between 50 and 274.16 K. " +
      "Calculation of specific heat capacity of ice may not be valid!",
      level = AssertionLevel.warning);
  end if;

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function calculates the specific heat capacity of the condensing component
(i.e., water) at its solid phase (i.e., ice) as function of the state record. The
specific heat capacity is calculated as a polynomial fit to specific heat capacity
data calculated with the reference equation of state for ice. The data was calculated
for fixed pressure (p_water_trp = 611.657 Pa) and temperature varying from 50 K
to 274.16 K.
</p>

<h4>References</h4>
<ul>
  <li>
  The International Association for the Properties of Water and Steam (2009). Revised Release on the Equation of State 2006 for H2O Ice Ih. URL: https://iapws.org/relguide/Ice-2009.html.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  November 27, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end specificHeatCapacitySolid;
