within SorpLib.Media.IdealGasVaporMixtures.Interfaces.PartialIdealGasWaterVaporMixture;
function rho_pTXY
  "Returns density of the ideal gas-vapor mixture"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.Pressure p
    "Pressure"
    annotation (Dialog(tab="General",group="Inputs"));
  input Modelica.Units.SI.Temperature T
    "Pressure"
    annotation (Dialog(tab="General",group="Inputs"));
  input Modelica.Units.SI.MassFraction[nX] X
    "Mass fractions"
    annotation (Dialog(tab="General",group="Inputs"));
  input Modelica.Units.SI.MoleFraction[nX] Y
    "Mole fractions"
    annotation (Dialog(tab="General",group="Inputs"));

  //
  // Definition of outputs
  //
  output Modelica.Units.SI.Density rho
    "Density"
    annotation (Dialog(tab="General",group="Outputs", enable=false));

  //
  // Definition of variables
  //
protected
  Modelica.Units.SI.MassFraction[nX] x=
    moistAirToDryAirMassFractions(X=X)
    "Mass fractions per dry air";
  Modelica.Units.SI.MassFraction x_sat=
    dryMassFractionSaturation(state=ThermodynamicState(p=p, T=T, X=X, Y=Y))
    "Saturation mass fraction of condensing component per dry air";

  Modelica.Units.SI.SpecificVolume v
    "Specific volume per dry air mass";

algorithm
  //
  // Calculate specific volume per dry air mass
  //
  if Modelica.Math.isEqual(s1=x[nX], s2=0, eps=100*Modelica.Constants.eps) then
    v := sum(x[1:nX-1] ./ MMX[1:nX-1] .* Modelica.Constants.R .* T ./ p)
      "Specific volume equals specific volume of unsaturated dry air without 
        water: Calculated applying the law of ideal gas mixtures.";

  elseif x[nX] <= x_sat then
    v := sum(x ./ MMX .* Modelica.Constants.R  .* T ./ p)
      "Specific volume equals specific volume of unsaturated dry air and water 
        vapor: Calculated applying the law of ideal gas mixtures.";

  elseif T > T_water_trp then
    v := sum(x[1:nX-1] .* Modelica.Constants.R ./ MMX[1:nX-1] .* T ./ p) +
      x_sat * Modelica.Constants.R / MMX[nX] * T / p +
      (x[nX] - x_sat) * v_water_liq
      "Specific volume equals specific volume of saturated dry air, water vapor,
        and liquid water: Calculated applying the law of ideal gas  mixtures.";

  else
    v := sum(x[1:nX-1] .* Modelica.Constants.R ./ MMX[1:nX-1] .* T ./ p) +
      x_sat * Modelica.Constants.R / MMX[nX] * T / p +
      (x[nX] - x_sat) * v_water_solid
      "Specific volume equals specific volume of saturated dry air, water vapor,
        and solid water: Calculated applying the law of ideal gas  mixtures.";

  end if;

  //
  // Convert specific volume to density
  //
  rho := 1 / (v / (1 + x[nX]))
    "Density of ideal gas-vapor mixture";

  //
  // Annotations
  //
  annotation (Inline=false, InlineAfterIndexReduction=false, LateInline=true,
inverse(p=pressure_dTXY(d=rho, T=T, X=X, Y=Y)), Documentation(info="<html>
<p>
This function calculates the density of the ideal gas-vapor mixture as function
of pressure, temperature, mass fractions, and mole fractions.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 27, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end rho_pTXY;
