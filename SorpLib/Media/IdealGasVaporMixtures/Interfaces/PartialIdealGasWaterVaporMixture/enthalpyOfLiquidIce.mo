within SorpLib.Media.IdealGasVaporMixtures.Interfaces.PartialIdealGasWaterVaporMixture;
function enthalpyOfLiquidIce
  "Returns specific enthalpy of condensing component at solid or liquid state (i.e., ice or liquid water) (per moist air mass)"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input ThermodynamicState state
    "Thermodynamic state record"
    annotation (Dialog(tab="General",group="Inputs"));

  //
  // Definition of outputs
  //
  output Modelica.Units.SI.SpecificEnthalpy h
    "Specific enthalpy of condensing component at solid or liquid state (i.e., 
    ice or liquid water) per moist air mass"
    annotation (Dialog(tab="General",group="Outputs", enable=false));

  //
  // Definition of variables
  //
protected
  Modelica.Units.SI.MassFraction[nX] x=
    moistAirToDryAirMassFractions(X=state.X)
    "Mass fractions per dry air";
  Modelica.Units.SI.MassFraction x_sat=
    dryMassFractionSaturation(state=state)
    "Saturation mass fraction of condensing component per dry air";

algorithm
  //
  // Calculate specific enthalpy per dry air mass
  //
  if Modelica.Math.isEqual(s1=x[nX], s2=0, eps=100*Modelica.Constants.eps) then
    h := 0
      "Specific enthalpy equals zero because water is not within the mixture";

  elseif x[nX] <= x_sat then
    h := 0
      "Specific enthalpy equals zero because air is not saturated";

  elseif state.T > T_water_trp then
    h := (x[nX]-x_sat) * enthalpyOfLiquid(p=state.p, T=state.T)
      "Specific enthalpy equals specific enthalpy of liquid water";

  else
    h := (x[nX]-x_sat) * enthalpyOfSolid(p=state.p, T=state.T)
      "Specific enthalpy equals specific enthalpy of solid water";

  end if;

  //
  // Convert specific enthalpy per dry air mass
  //
  h := h / (1 + x[nX])
    "Specific enthalpy of condensing component at solid or liquid state per moist 
    air mass";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function calculates the specific enthalpy of the condensing component at the
solid or liquid state (i.e., ice or liquid water) per moist air mass according to 
the models of an ideal solid or liquid.
</p>

</html>", revisions="<html>
<ul>
  <li>
  November 27, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end enthalpyOfLiquidIce;
