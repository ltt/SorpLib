within SorpLib.Media.IdealGasVaporMixtures.Interfaces.PartialIdealGasWaterVaporMixture;
function specificEnthalpy_i
  "Returns specific enthalpy of component i per component mass"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Integer ind_component
    "Component index whose specific enthalpy is to be calculated"
    annotation (Dialog(tab="General", group="Inputs"));
  input ThermodynamicState state
    "Thermodynamic state record"
    annotation (Dialog(tab="General",group="Inputs"));

  //
  // Definition of outputs
  //
  output Modelica.Units.SI.SpecificEnthalpy h
    "Specific enthalpy of component ind_component"
    annotation (Dialog(tab="General", group="Outputs", enable=false));

  //
  // Definition of variables
  //
protected
  Modelica.Units.SI.MassFraction[nX] x=
    moistAirToDryAirMassFractions(X=state.X)
    "Mass fractions per dry air";
  Modelica.Units.SI.MassFraction x_sat=
    dryMassFractionSaturation(state=state)
    "Saturation mass fraction of condensing component per dry air";

algorithm
  if ind_component <> nX then
    h := Modelica.Media.IdealGases.Common.Functions.h_T(
      data=data[ind_component],
      T=state.T,
      exclEnthForm=true,
      refChoice=Modelica.Media.Interfaces.Choices.ReferenceEnthalpy.UserDefined,
      h_off=h_dryAir_off)
      "Specific enthalpy equals specific enthalpy of dry air component";

  elseif Modelica.Math.isEqual(s1=x[nX], s2=0, eps=100*Modelica.Constants.eps) then
    h := 0
      "Specific enthalpy equals specific enthalpy of water but water is not
      within the ideal gas-vapor mixture";

  elseif x[nX] <= x_sat then
    h := enthalpyOfCondensingGas(T=state.T)
      "Specific enthalpy equals specific enthalpy of water: Unsaturated dry
      air";

  elseif state.T > T_water_trp then
    h := x_sat/x[nX] * enthalpyOfCondensingGas(T=state.T) +
      (x[nX]-x_sat)/x[nX] * enthalpyOfLiquid(p=state.p, T=state.T)
      "Specific enthalpy equals specific enthalpy of water: Saturated dry
      air with liquid water";

  else
    h := x_sat/x[nX] * enthalpyOfCondensingGas(T=state.T) +
      (x[nX]-x_sat)/x[nX] * enthalpyOfSolid(p=state.p, T=state.T)
      "Specific enthalpy equals specific enthalpy of water: Saturated dry
      air with solid water";

  end if;

  //
  // Annotations
  //
  annotation(Documentation(info="<html>
<p>
This function calculates the specific enthalpy of component i per component mass.
</p>
</html>",
        revisions="<html>
<ul>
  <li>
  November 27, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end specificEnthalpy_i;
