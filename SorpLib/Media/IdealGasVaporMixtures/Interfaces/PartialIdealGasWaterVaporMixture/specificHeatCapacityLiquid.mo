within SorpLib.Media.IdealGasVaporMixtures.Interfaces.PartialIdealGasWaterVaporMixture;
function specificHeatCapacityLiquid
  "Returns specific heat capacity c of condensing component's liquid phase (per water mass)"
  extends Modelica.Icons.Function;

  //
  // Deifnition of inputs
  //
  input ThermodynamicState state
    "Thermodynamic state record"
    annotation (Dialog(tab="General",group="Inputs"));

  //
  // Deifnition of outputs
  //
  output Modelica.Units.SI.SpecificHeatCapacity c
    "Specific heat capacity of liquid"
    annotation (Dialog(tab="General",group="Outputs", enable=false));

  //
  // Definition of constants
  //
protected
  constant Real[10] coefficients=
    {4.66348457415926e-19, -1.16579701193758e-15, 9.49925152355854e-13,
    5.81553479401465e-11, -6.55387915784314e-7, 5.30868446475702e-4,
    -2.18234813641286e-1, 5.13043718148301e1, -6.58121774333134e3,
    3.63215816205503e5}
    "Coefficients of polynomical function";
  constant Real[10] exponents=
    {9, 8, 7, 6, 5, 4, 3, 2, 1, 0}
    "Exponents of polynomical function";

algorithm
  c := SorpLib.Media.Functions.Utilities.generalizedFunction_T(
    T=state.T,
    T_ref=573.15,
    z_ref=1,
    coefficients=coefficients,
    exponents=exponents,
    approach=SorpLib.Choices.GeneralizedFunctionApproach.PolynomialFunctionTemperature)
    "Specific heat capacity of liquid";

  //
  // Assertations
  //
  if print_warnings then
    assert(T_water_trp-1 <= state.T and state.T <= 573.15,
      "Temperature (" + String(state.T) + " K) is not between 272.16 and 573.15 K. " +
      "Calculation of specific heat capacity of liquid may not be valid!",
      level = AssertionLevel.warning);
  end if;

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function calculates the specific heat capacity of the condensing component
(i.e., water) at its liquid phase as function of the state record. The specific 
heat capacity is calculated as a polynomial fit to specific heat capacity data 
calculated at saturated liquid state with the reference equation of state for 
water. The data was calculated for temperature varying from 273.16 K to 573.15 K.
</p>

<h4>References</h4>
<ul>
  <li>
  Wagner, W. and Pru&szlig;, A (2002). The IAPWS Formulation 1995 for the Thermodynamic Properties of Ordinary Water Substance for General and Scientific Use, Journal of Physical and Chemical Reference Data, 31:387. DOI: https://doi.org/10.1063/1.1461829.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  November 27, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end specificHeatCapacityLiquid;
