within SorpLib.Media.IdealGasVaporMixtures.Interfaces.PartialIdealGasWaterVaporMixture;
function dh_dT_pX
  "Returns partial derivative of specific enthalpy w.r.t. temperature at constant pressure and mass fractions"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input ThermodynamicState state
    "Thermodynamic state record"
    annotation (Dialog(tab="General",group="Inputs"));

  //
  // Definition of outputs
  //
  output Modelica.Units.SI.SpecificHeatCapacity dh_dT_pX
    "Partial derivative of specific enthalpy w.r.t. temperature at constant 
    pressure and mass fractions"
    annotation (Dialog(tab="General",group="Outputs", enable=false));

  //
  // Definition of variables
  //
protected
  Modelica.Units.SI.MassFraction[nX] x=
    moistAirToDryAirMassFractions(X=state.X)
    "Mass fractions per dry air";
  Modelica.Units.SI.MassFraction x_sat=
    dryMassFractionSaturation(state=state)
    "Saturation mass fraction of condensing component per dry air";

  Real dx_sat_dT_pX(unit="kg/(kg.K)")=
    ddryMassFractionSaturation_dT_pX(state=state)
    "Partial derivative of saturated wasser mass fraction per dry air mass 
    w.r.t. temperature at constant pressure and mass fractions";

algorithm
  //
  // Calculate partial derivative per dry air mass
  //
  if Modelica.Math.isEqual(s1=x[nX], s2=0, eps=100*Modelica.Constants.eps) then
    dh_dT_pX := x[1:nX-1] / sum(x[1:nX-1]) *
      {Modelica.Media.IdealGases.Common.Functions.cp_T(
        data=data[i],
        T=state.T) for i in 1:nX-1}
      "Partial derivative equals partial derivative of unsaturated dry air without 
      water: Calculated applying the law of ideal gas mixtures.";

  elseif x[nX] <= x_sat then
    dh_dT_pX := x[1:nX-1] / sum(x[1:nX-1]) *
      {Modelica.Media.IdealGases.Common.Functions.cp_T(
        data=data[i],
        T=state.T) for i in 1:nX-1} +
      x[nX] * specificHeatCapacityVapor(state=state)
      "Partial derivative equals partial derivative of unsaturated dry air and water 
      vapor: Calculated applying the law of ideal gas mixtures.";

  elseif state.T > T_water_trp then
    dh_dT_pX := x[1:nX-1] / sum(x[1:nX-1]) *
      {Modelica.Media.IdealGases.Common.Functions.cp_T(
        data=data[i],
        T=state.T) for i in 1:nX-1} +
      enthalpyOfCondensingGas(T=state.T) * dx_sat_dT_pX +
      x_sat * specificHeatCapacityVapor(state=state) +
      (-1) * enthalpyOfLiquid(p=state.p, T=state.T) * dx_sat_dT_pX +
      (x[nX]-x_sat) * specificHeatCapacityLiquid(state=state)
      "Partial derivative equals partial derivative of saturated dry air, water
      vapor, liquid water: Calculated applying the law of ideal gas mixtures.";

  else
    dh_dT_pX := x[1:nX-1] / sum(x[1:nX-1]) *
      {Modelica.Media.IdealGases.Common.Functions.cp_T(
        data=data[i],
        T=state.T) for i in 1:nX-1} +
      enthalpyOfCondensingGas(T=state.T) * dx_sat_dT_pX +
      x_sat * specificHeatCapacityVapor(state=state) +
      (-1) * enthalpyOfSolid(p=state.p, T=state.T) * dx_sat_dT_pX +
      (x[nX]-x_sat) * specificHeatCapacitySolid(state=state)
      "Partial derivative equals partial derivative of saturated dry air, water
      vapor, solid water: Calculated applying the law of ideal gas mixtures.";

  end if;

  //
  // Convert result
  //
  dh_dT_pX := dh_dT_pX / (1 + x[nX])
    "Partial derivative per moist air mass";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function calculates the partial derivative of the specific enthalpy with
respect to temperature at constant pressure and mass fractions as function of
the state record.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 27, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end dh_dT_pX;
