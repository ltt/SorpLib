within SorpLib.Media.IdealGasVaporMixtures.Interfaces.PartialIdealGasWaterVaporMixture;
function enthalpyOfVapor
  "Returns specific enthalpy of condensing component at gas state (i.e., water vapor) (per moist air mass)"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input ThermodynamicState state
    "Thermodynamic state record"
    annotation (Dialog(tab="General",group="Inputs"));

  //
  // Definition of outputs
  //
  output Modelica.Units.SI.SpecificEnthalpy h
    "Specific enthalpy of condensing component at gas state (i.e., water vapor)
    per moist air mass"
    annotation (Dialog(tab="General",group="Outputs", enable=false));

  //
  // Definition of variables
  //
protected
  Modelica.Units.SI.MassFraction[nX] x=
    moistAirToDryAirMassFractions(X=state.X)
    "Mass fractions per dry air";
  Modelica.Units.SI.MassFraction x_sat=
    dryMassFractionSaturation(state=state)
    "Saturation mass fraction of condensing component per dry air";

algorithm
  //
  // Calculate specific enthalpy per dry air mass
  //
  if Modelica.Math.isEqual(s1=x[nX], s2=0, eps=100*Modelica.Constants.eps) then
    h := 0
      "Specific enthalpy equals zero because water is not within the mixture";

  elseif x[nX] <= x_sat then
    h := x[nX] * enthalpyOfCondensingGas(T=state.T)
      "Specific enthalpy equals specific enthalpy of water vapor (unsaturated)";

  else
    h := x_sat * enthalpyOfCondensingGas(T=state.T)
      "Specific enthalpy equals specific enthalpy of water vapor (saturated)";

  end if;

  //
  // Convert specific enthalpy per dry air mass
  //
  h := h / (1 + x[nX])
    "Specific enthalpy of condensing component at gas state (i.e., water vapor) 
    per moist air mass";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function calculates the specific enthalpy of the condensing component at the
gas state (i.e., vaporous water) per moist air mass according to the model of an 
ideal gas.
</p>

</html>", revisions="<html>
<ul>
  <li>
  November 27, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end enthalpyOfVapor;
