within SorpLib.Media.IdealGasVaporMixtures.Interfaces.PartialIdealGasWaterVaporMixture;
function dryAirToMoistAirMassBasedProperties
  "Converts 'dry air'-specific property to 'moist air'-specific property"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Real z_dryAir
    "Abitrary property given per dry air mass"
    annotation (Dialog(tab="General", group="Inputs"));
  input Modelica.Units.SI.MassFraction x_water
    "Mass fraction of water based on dry air mass"
    annotation (Dialog(tab="General", group="Inputs"));

  //
  // Definition of outputs
  //
  output Real z_moistAir
    "Abitrary property given per moist air mass"
    annotation (Dialog(tab="General", group="Outputs", enable=false));

algorithm
  z_moistAir := z_dryAir / (1 + x_water)
    "Abitrary property given per moist air mass";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function converts an abitrary property given per dry air mass so that it is
given per moist air mass.
</p>

</html>", revisions="<html>
<ul>
  <li>
  November 27, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end dryAirToMoistAirMassBasedProperties;
