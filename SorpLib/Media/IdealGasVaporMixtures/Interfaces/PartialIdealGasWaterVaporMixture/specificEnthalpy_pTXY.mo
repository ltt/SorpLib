within SorpLib.Media.IdealGasVaporMixtures.Interfaces.PartialIdealGasWaterVaporMixture;
function specificEnthalpy_pTXY
  "Returns specific enthalpy of the ideal gas-vapor mixture"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.Pressure p
    "Pressure"
    annotation (Dialog(tab="General",group="Inputs"));
  input Modelica.Units.SI.Temperature T
    "Pressure"
    annotation (Dialog(tab="General",group="Inputs"));
  input Modelica.Units.SI.MassFraction[nX] X
    "Mass fractions"
    annotation (Dialog(tab="General",group="Inputs"));
  input Modelica.Units.SI.MoleFraction[nX] Y
    "Mole fractions"
    annotation (Dialog(tab="General",group="Inputs"));

  //
  // Definition of outputs
  //
  output Modelica.Units.SI.SpecificEnthalpy h
    "Specific enthalpy"
    annotation (Dialog(tab="General",group="Outputs", enable=false));

  //
  // Definition of variables
  //
protected
  Modelica.Units.SI.MassFraction[nX] x=
    moistAirToDryAirMassFractions(X=X)
    "Mass fractions per dry air";
  Modelica.Units.SI.MassFraction x_sat=
    dryMassFractionSaturation(state=ThermodynamicState(p=p, T=T, X=X, Y=Y))
    "Saturation mass fraction of condensing component per dry air";

algorithm
  //
  // Calculate specific enthalpy per dry air mass
  //
  if Modelica.Math.isEqual(s1=x[nX], s2=0, eps=100*Modelica.Constants.eps) then
    h := enthalpyOfNonCondensingGas(T=T, X=x)
      "Specific enthalpy equals specific enthalpy of unsaturated dry air without 
        water: Calculated applying the law of ideal gas mixtures.";

  elseif x[nX] <= x_sat then
    h := enthalpyOfNonCondensingGas(T=T, X=x) +
      x[nX] * enthalpyOfCondensingGas(T=T)
      "Specific enthalpy equals specific enthalpy of unsaturated dry air and water 
        vapor: Calculated applying the law of ideal gas mixtures.";

  elseif T > T_water_trp then
    h := enthalpyOfNonCondensingGas(T=T, X=x) +
      x_sat * enthalpyOfCondensingGas(T=T) +
      (x[nX]-x_sat) * enthalpyOfLiquid(p=p, T=T)
      "Specific enthalpy equals specific enthalpy of saturated dry air, water
        vapor, liquid water: Calculated applying the law of ideal gas mixtures.";

  else
    h := enthalpyOfNonCondensingGas(T=T, X=x) +
      x_sat * enthalpyOfCondensingGas(T=T) +
      (x[nX]-x_sat) * enthalpyOfSolid(p=p, T=T)
      "Specific enthalpy equals specific enthalpy of saturated dry air, water
        vapor, solid water: Calculated applying the law of ideal gas mixtures.";

  end if;

  //
  // Convert specific enthalpy per dry air mass
  //
  h := h / (1 + x[nX])
    "Specific enthalpy per moist air mass";

  //
  // Annotations
  //
  annotation (Inline=false, InlineAfterIndexReduction=false, LateInline=true,
inverse(T=temperature_phXY(p=p, h=h, X=X, Y=Y)),Documentation(info="<html>
<p>
This function calculates the specific enthalpy of the ideal gas-vapor mixture as 
function of pressure, temperature, mass fractions, and mole fractions.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 27, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end specificEnthalpy_pTXY;
