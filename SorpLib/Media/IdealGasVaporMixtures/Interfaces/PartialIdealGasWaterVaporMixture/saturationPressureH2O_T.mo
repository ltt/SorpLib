within SorpLib.Media.IdealGasVaporMixtures.Interfaces.PartialIdealGasWaterVaporMixture;
function saturationPressureH2O_T
  "Returns saturatation pressure of water"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input Modelica.Units.SI.Temperature T_sat
    "Saturation temperature"
    annotation (Dialog(tab="General",group="Inputs"));

  //
  // Definition of outputs
  //
  output Modelica.Units.SI.Pressure p_sat
    "Saturation pressure"
    annotation (Dialog(tab="General",group="Outputs", enable=false));

  //
  // Definition of constants
  //
protected
  constant Real[2] coeff_s = {-13.9281690, 34.7078238}
    "Coefficients of the gas-solid boundary";
  constant Real[6] coeff_l = {-7.85951783, 1.84408259, -11.7866497,
    22.6807411, -15.9618719, 1.80122502}
    "Coefficients of the gas-liquid boundary";

  constant Real[2] exp_s = {-1.5, -1.25}
    "Exponents of the gas-solid boundary";
  constant Real[6] exp_l = {1, 1.5, 3,
    3.5, 4, 7.5}
    "Exponents of the gas-liquid boundary";

  //
  // Definition of variables
  //
  Modelica.Units.SI.Pressure p_sat_s
    "Saturation pressure of gas-solid boundary";
  Modelica.Units.SI.Pressure p_sat_l
    "Saturation pressure of gas-liquid boundary";

  Real T_red_s = T_sat/T_water_trp
    "Reduced temperature required for gas-solid boundary";
  Real T_red_fl = 1 - T_sat/T_water_crit
    "Reduced temperature required for gas-liquid boundary";

  Real lambda(unit="1") = SorpLib.Numerics.smoothTransition(
    x=T_sat, transitionPoint=T_water_trp, transitionLength=1, noDiff=3)
    "Transition factor";

algorithm
  //
  // Calculate pressures
  //
  p_sat_s := p_water_trp * exp(coeff_s[1] - coeff_s[1] * T_red_s ^ exp_s[1] +
    coeff_s[2] - coeff_s[2] * T_red_s ^ exp_s[2])
    "Saturation pressure of gas-solid boundary";
  p_sat_l := p_water_crit * exp(T_water_crit/T_sat *
    (coeff_l[1] * T_red_fl ^ exp_l[1] +
    coeff_l[2] * T_red_fl ^ exp_l[2] +
    coeff_l[3] * T_red_fl ^ exp_l[3] +
    coeff_l[4] * T_red_fl ^ exp_l[4] +
    coeff_l[5] * T_red_fl ^ exp_l[5] +
    coeff_l[6] * T_red_fl ^ exp_l[6]))
    "Saturation pressure of gas-liquid boundary";

  //
  // Check for boundary
  //
  p_sat :=lambda*p_sat_s + (1-lambda)*p_sat_l
    "Saturation pressure";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function calculates the saturation pressure of water depending on
the temperature. This functions covers both, the solid-gas and the gas-
liquid boundary.
</p>

<h4>References</h4>
<ul>
  <li>
  Wagner, W. and Pru&szlig;, A (2002). The IAPWS Formulation 1995 for the Thermodynamic Properties of Ordinary Water Substance for General and Scientific Use, Journal of Physical and Chemical Reference Data, 31:387. DOI: https://doi.org/10.1063/1.1461829.
  </li>
</ul>
</html>", revisions="<html>
<ul>
  <li>
  November 27, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end saturationPressureH2O_T;
