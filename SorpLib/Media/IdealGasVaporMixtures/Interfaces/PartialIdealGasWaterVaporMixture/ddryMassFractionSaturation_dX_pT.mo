within SorpLib.Media.IdealGasVaporMixtures.Interfaces.PartialIdealGasWaterVaporMixture;
function ddryMassFractionSaturation_dX_pT
  "Returns partial derivatives of saturated wasser mass fraction per dry air mass w.r.t. mass fractions at constant pressure and temperature"
  extends Modelica.Icons.Function;

  //
  // Deifnition of inputs
  //
  input ThermodynamicState state
    "Thermodynamic state record"
    annotation (Dialog(tab="General",group="Inputs"));

  //
  // Deifnition of outputs
  //
  output Real[nX] dx_sat_dX_pT(each unit="kg.kg/(kg.kg)")
    "Partial derivatives of saturated wasser mass fraction per dry air mass 
    w.r.t. mass fractions given per moist air mass at constant pressure and 
    temperature"
    annotation (Dialog(tab="General",group="Outputs", enable=false));

  //
  // Definition of variables
  //
protected
  Modelica.Units.SI.MolarMass Y_dryAir=
    sum(state.Y[1:nX-1])
    "Sum of molar fractions of dry air components";
  Modelica.Units.SI.MolarMass MM_dryAir=
    state.Y[1:nX-1] ./ Y_dryAir * MMX[1:nX-1]
    "Molar mass of dry air components";
  Modelica.Units.SI.Pressure p_sat=
    saturationPressureH2O_T(T_sat = state.T)
    "Saturation pressure";

  Real[nX-1] dMM_dryAir_dY=
    MMX[1:nX-1] ./ Y_dryAir .- sum(MMX[1:nX-1] .* state.Y[1:nX-1] ./ Y_dryAir^2)
    "Partial derivatives of molar mass of dry air components w.r.t. molar
    fractions of dry air components";
  Real[nX, nX] dY_dX(each unit="mol.kg/(mol.kg)")=
    dmassToMoleFractions_dX(X=state.X)
    "Partial derivatives of mole fractions given per moist air w.r.t mass 
    fractions given per moist air mass: Rows are mole fractions and colums 
    are mass fractions";
  Real[nX] dMM_dryAir_dX=
    {sum(dMM_dryAir_dY .* dY_dX[1:nX-1, ind]) for ind in 1:nX}
    "Partial derivatives of molar mass of dry air components w.r.t. mass
    fractions given per moist air mass";

algorithm
  dx_sat_dX_pT := (-MMX[nX] / MM_dryAir^2 *
    p_sat / max((state.p - p_sat), Modelica.Constants.small)) .*
    dMM_dryAir_dX
    "Partial derivatives of saturated wasser mass fraction per dry air mass 
    w.r.t. mass fractions given per moist air mass at constant pressure and 
    temperature";

  //
  // Assertations
  //
  if print_warnings then
    assert(p_sat <= state.p,
      "Saturation pressure (" + String(p_sat) + " Pa) is greater than total " +
      "pressure (" + String(state.p) + " Pa). Thus, the calculation of the " +
      "saturation mass fraction is not sound!",
      level = AssertionLevel.warning);
  end if;

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function calculates the partial derivatives of the saturation mass fraction 
of the condensing component (i.e., water) per dry air with respect to mass fractions
given per moist air mass at constant pressure and temperature as function of the 
state record. Note that the saturation mass fraction can only be calculated if 
the partial pressure of the condensing component is less than the pressure of the 
ideal gas-vapor mixture.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 27, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end ddryMassFractionSaturation_dX_pT;
