within SorpLib.Media.IdealGasVaporMixtures.Interfaces.PartialIdealGasWaterVaporMixture;
function dh_dp_TX
  "Returns partial derivative of specific enthalpy w.r.t. pressure at constant temperature and mass fractions"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input ThermodynamicState state
    "Thermodynamic state record"
    annotation (Dialog(tab="General",group="Inputs"));

  //
  // Definition of outputs
  //
  output SorpLib.Units.DerSpecificEnthalpyByPressure dh_dp_TX
    "Partial derivative of specific enthalpy w.r.t. pressure at constant 
    temperature and mass fractions"
    annotation (Dialog(tab="General",group="Outputs", enable=false));

  //
  // Definition of variables
  //
protected
  Modelica.Units.SI.MassFraction[nX] x=
    moistAirToDryAirMassFractions(X=state.X)
    "Mass fractions per dry air";
  Modelica.Units.SI.MassFraction x_sat=
    dryMassFractionSaturation(state=state)
    "Saturation mass fraction of condensing component per dry air";

  Real dx_sat_dp_TX(unit="kg/(kg.Pa)")=
    ddryMassFractionSaturation_dp_TX(state=state)
    "Partial derivative of saturated wasser mass fraction per dry air mass 
    w.r.t. pressure at constant temperature and mass fractions";

algorithm
  //
  // Calculate partial derivative per dry air mass
  //
  if Modelica.Math.isEqual(s1=x[nX], s2=0, eps=100*Modelica.Constants.eps) then
    dh_dp_TX := 0
      "Partial derivative equals partial derivative of unsaturated dry air without 
      water: Calculated applying the law of ideal gas mixtures.";

  elseif x[nX] <= x_sat then
    dh_dp_TX := 0
      "Partial derivative equals partial derivative of unsaturated dry air and water 
      vapor: Calculated applying the law of ideal gas mixtures.";

  elseif state.T > T_water_trp then
    dh_dp_TX := enthalpyOfCondensingGas(T=state.T) * dx_sat_dp_TX +
      (-1) * enthalpyOfLiquid(p=state.p, T=state.T) * dx_sat_dp_TX +
      (x[nX]-x_sat) * v_water_liq
      "Partial derivative equals partial derivative of saturated dry air, water
      vapor, liquid water: Calculated applying the law of ideal gas mixtures.";

  else
    dh_dp_TX := enthalpyOfCondensingGas(T=state.T) * dx_sat_dp_TX +
      (-1) * enthalpyOfSolid(p=state.p, T=state.T) * dx_sat_dp_TX +
      (x[nX]-x_sat) * v_water_solid
      "Partial derivative equals partial derivative of saturated dry air, water
      vapor, solid water: Calculated applying the law of ideal gas mixtures.";

  end if;

  //
  // Convert result
  //
  dh_dp_TX := dh_dp_TX / (1 + x[nX])
    "Partial derivative per moist air mass";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function calculates the partial derivative of the specific enthalpy with
respect to pressure at constant temperature and mass fractions as function of
the state record.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 27, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end dh_dp_TX;
