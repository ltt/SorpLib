within SorpLib.Media.IdealGasVaporMixtures.Interfaces.PartialIdealGasWaterVaporMixture;
function relativeHumidity
  "Returns relative humidity"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input ThermodynamicState state
    "Thermodynamic state record"
    annotation (Dialog(tab="General",group="Inputs"));

  //
  // Definition of outputs
  //
  output Real phi(min=0, max=1)
     "Relative humidity"
    annotation (Dialog(tab="General",group="Outputs", enable=false));

algorithm
  phi := if (state.X[nX] / (1 - state.X[nX])) >
    dryMassFractionSaturation(state=state) then 1 else
    state.Y[nX] * state.p / saturationPressure(Tsat=state.T)
    "Relative humidity";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function calculates the relative humidity as function of the state record.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 27, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end relativeHumidity;
