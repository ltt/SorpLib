within SorpLib.Media.IdealGasVaporMixtures.Interfaces.PartialIdealGasWaterVaporMixture;
function massFractionSaturation
  "Return saturation mass fraction of condensing component (i.e., water) (per saturated moist air mass)"
  extends Modelica.Icons.Function;

  //
  // Deifnition of inputs
  //
  input ThermodynamicState state
    "Thermodynamic state record"
    annotation (Dialog(tab="General",group="Inputs"));

  //
  // Deifnition of outputs
  //
  output Modelica.Units.SI.MassFraction X_sat
    "Saturation mass fraction per saturated moist air"
    annotation (Dialog(tab="General",group="Outputs", enable=false));

  //
  // Definition of variables
  //
protected
  MassFraction x_sat = dryMassFractionSaturation(state=state)
    "Saturation mass fraction per dry air";

algorithm
   X_sat :=x_sat / (1 + x_sat)
    "Saturation mass fraction per saturated moist air";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function calculates the saturation mass fraction of the condensing component
(i.e., water) per saturated moist air as function of the state record. Note that 
the saturation mass fraction can only be calculated if the partial pressure of the 
condensing component is less than the pressure of the ideal gas-vapor mixture.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 27, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end massFractionSaturation;
