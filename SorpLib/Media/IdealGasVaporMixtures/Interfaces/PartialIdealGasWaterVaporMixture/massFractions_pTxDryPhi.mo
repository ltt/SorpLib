within SorpLib.Media.IdealGasVaporMixtures.Interfaces.PartialIdealGasWaterVaporMixture;
function massFractions_pTxDryPhi
  "Returns mass fractions per moist air mass based on pressure, temperature, compoition of dry air, and relative humidity"
  extends Modelica.Icons.Function;

  //
  // Deifnition of inputs
  //
  input Modelica.Units.SI.Pressure p
    "Pressure"
    annotation (Dialog(tab="General",group="Inputs"));
  input Modelica.Units.SI.Temperature T
    "Temperature"
    annotation (Dialog(tab="General",group="Inputs"));
  input Modelica.Units.SI.MassFraction[nX-1] x
    "Mass fractions of dry air components per dry air mass"
    annotation (Dialog(tab="General",group="Inputs"));
  input Real phi(min=0, max=1)
    "Relative humidity"
    annotation (Dialog(tab="General",group="Inputs"));

  //
  // Deifnition of outputs
  //
  output Modelica.Units.SI.MassFraction[nX] X
    "Mass fractions per moist air mass"
    annotation (Dialog(tab="General",group="Outputs", enable=false));

  //
  // Definition of variables
  //
protected
  Modelica.Units.SI.MoleFraction[nX-1] y=
    massToMoleFractions(X=x, MMX=MMX)
    "Mole fractions of dry air components per dry air amount of substances";
  Modelica.Units.SI.Pressure p_sat=
    saturationPressureH2O_T(T_sat = T)
    "Saturation pressure";

  Modelica.Units.SI.MassFraction x_water
    "Water mass fraction per dry air mass";

algorithm
  x_water := MMX[nX] / (y * MMX[1:nX-1]) *
    phi*p_sat / max((p - phi*p_sat), Modelica.Constants.small)
    "Water mass fraction per dry air mass";

  X :=cat(
    1,
    x ./ (1 + x_water),
    {x_water} ./ (1 + x_water))
    "Mass fractions per moist air mass";

  //
  // Assertations
  //
  if print_warnings then
    assert(0 <= phi and phi <= 1,
      "Relative humidity (" + String(phi) + " ) is not between 0 and 1. Thus, the" +
      "calculation of the water mass fraction is not sound!",
      level = AssertionLevel.warning);

    assert(phi*p_sat <= p,
      "Partial pressure of water (" + String(phi*p_sat) + " Pa ) is greater " +
      "than the total pressure (" + String(p) + " Pa ) . Thus, the calculation " +
      " of the water mass fraction is not sound!",
      level = AssertionLevel.warning);
  end if;

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function calculates the mass fractions per moist air mass based on the pressure,
temperature, mass fractions of the dry air components per dry air mass, and relative 
humidity. Note that this function only returns reasonable results for unsaturated
air.
</p>
</html>",
        revisions="<html>
<ul>
  <li>
  November 27, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end massFractions_pTxDryPhi;
