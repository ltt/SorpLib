within SorpLib.Media.IdealGasVaporMixtures.Interfaces.PartialIdealGasWaterVaporMixture;
function dv_dp_TX
  "Returns partial derivative of specific volume w.r.t. pressure at constant temperature and mass fractions"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input ThermodynamicState state
    "Thermodynamic state record"
    annotation (Dialog(tab="General",group="Inputs"));

  //
  // Definition of outputs
  //
  output SorpLib.Units.DerSpecificVolumeByPressure dv_dp_TX
    "Partial derivative of specific volume w.r.t. pressure at constant temperature
    and mass fractions"
    annotation (Dialog(tab="General",group="Outputs", enable=false));

  //
  // Definition of variables
  //
protected
  Modelica.Units.SI.MassFraction[nX] x=
    moistAirToDryAirMassFractions(X=state.X)
    "Mass fractions per dry air";
  Modelica.Units.SI.MassFraction x_sat=
    dryMassFractionSaturation(state=state)
    "Saturation mass fraction of condensing component per dry air";

  Real dx_sat_dp_TX(unit="kg/(kg.Pa)")=
    ddryMassFractionSaturation_dp_TX(state=state)
    "Partial derivative of saturated wasser mass fraction per dry air mass 
    w.r.t. pressure at constant temperature and mass fractions";

algorithm
  //
  // Calculate partial derivative per dry air mass
  //
  if Modelica.Math.isEqual(s1=x[nX], s2=0, eps=100*Modelica.Constants.eps) then
    dv_dp_TX := sum(-x[1:nX-1] ./ MMX[1:nX-1] .*
      Modelica.Constants.R .* state.T ./ state.p^2)
      "Partial derivative equals partial derivative of unsaturated dry air without 
      water: Calculated applying the law of ideal gas mixtures.";

  elseif x[nX] <= x_sat then
    dv_dp_TX := sum(-x ./ MMX .* Modelica.Constants.R  .* state.T ./ state.p^2)
      "Partial derivative equals partial derivative of unsaturated dry air and water 
      vapor: Calculated applying the law of ideal gas mixtures.";

  elseif state.T > T_water_trp then
    dv_dp_TX := sum(-x[1:nX-1] .* Modelica.Constants.R ./ MMX[1:nX-1] .*
      state.T ./ state.p^2) +
      Modelica.Constants.R / MMX[nX] * state.T / state.p * dx_sat_dp_TX +
      (-1) * x_sat * Modelica.Constants.R / MMX[nX] * state.T / state.p^2 +
      (-1) * v_water_liq * dx_sat_dp_TX
      "Partial derivative equals partial derivative of saturated dry air, water vapor,
      and liquid water: Calculated applying the law of ideal gas  mixtures.";

  else
    dv_dp_TX := sum(-x[1:nX-1] .* Modelica.Constants.R ./ MMX[1:nX-1] .*
      state.T ./ state.p^2) +
      Modelica.Constants.R / MMX[nX] * state.T / state.p * dx_sat_dp_TX +
      (-1) * x_sat * Modelica.Constants.R / MMX[nX] * state.T / state.p^2 +
      (-1) * v_water_solid * dx_sat_dp_TX
      "Partial derivative equals partial derivative of saturated dry air, water vapor,
      and solid water: Calculated applying the law of ideal gas  mixtures.";

  end if;

  //
  // Convert result
  //
  dv_dp_TX := dv_dp_TX / (1 + x[nX])
    "Partial derivative per moist air mass";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function calculates the partial derivative of the specific volume with
respect to pressure at constant temperature and mass fractions as function of
the state record.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 27, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end dv_dp_TX;
