within SorpLib.Media.IdealGasVaporMixtures.Interfaces.PartialIdealGasWaterVaporMixture;
function dv_dX_pT
  "Returns partial derivatives of specific volume w.r.t. mass fractions at constant pressure and temperature"
  extends Modelica.Icons.Function;

  //
  // Definition of inputs
  //
  input ThermodynamicState state
    "Thermodynamic state record"
    annotation (Dialog(tab="General",group="Inputs"));

  //
  // Definition of outputs
  //
  output Real[nX] dv_dX_pT(each unit="m3/kg")
    "Partial derivatives of specific volume w.r.t. mass fractions at constant 
    pressure and temperature"
    annotation (Dialog(tab="General",group="Outputs", enable=false));

  //
  // Definition of variables
  //
protected
  Modelica.Units.SI.MassFraction[nX] x=
    moistAirToDryAirMassFractions(X=state.X)
    "Mass fractions per dry air";
  Modelica.Units.SI.MassFraction x_sat=
    dryMassFractionSaturation(state=state)
    "Saturation mass fraction of condensing component per dry air";

  Real[nX, nX] dx_dX(each unit="kg.kg/(kg.kg)")=
    ddryAirMassFractions_dX(X=state.X)
    "Partial derivatives of mass fractions given per dry air mass w.r.t. mass
    fractions given per moist air mass: Rows are dry air mass and columns are
    moist air mass";
  Real[nX] dx_sat_dX_pT(each unit="kg.kg/(kg.kg)")=
    ddryMassFractionSaturation_dX_pT(state=state)
    "Partial derivatives of saturated wasser mass fraction per dry air mass 
    w.r.t. mass fractions given per moist air mass at constant pressure and 
    temperature";

  Modelica.Units.SI.SpecificVolume v
    "Specific volume per dry air mass";

algorithm
  //
  // Calculate specific volume and partial derivatives per dry air mass
  //
  if Modelica.Math.isEqual(s1=x[nX], s2=0, eps=100*Modelica.Constants.eps) then
    v := sum(x[1:nX-1] ./ MMX[1:nX-1] .* Modelica.Constants.R .*
      state.T ./ state.p)
      "Specific volume equals specific volume of unsaturated dry air without 
      water: Calculated applying the law of ideal gas mixtures.";
    dv_dX_pT := {sum(1 ./ MMX[1:nX-1] .* Modelica.Constants.R .*
      state.T ./ state.p .* dx_dX[1:nX-1,ind]) for ind in 1:nX}
      "Partial derivatives equal partial derivatives of unsaturated dry air 
      without water: Calculated applying the law of ideal gas mixtures.";

  elseif x[nX] <= x_sat then
    v := sum(x ./ MMX .* Modelica.Constants.R  .*
      state.T ./ state.p)
      "Specific volume equals specific volume of unsaturated dry air and water 
      vapor: Calculated applying the law of ideal gas mixtures.";
    dv_dX_pT := {sum(1 ./ MMX .* Modelica.Constants.R  .*
      state.T ./ state.p .* dx_dX[1:nX,ind]) for ind in 1:nX}
      "Partial derivatives equal partial derivatives of unsaturated dry air and 
      water vapor: Calculated applying the law of ideal gas mixtures.";

  elseif state.T > T_water_trp then
    v := sum(x[1:nX-1] ./ MMX[1:nX-1] .* Modelica.Constants.R .*
      state.T ./ state.p) +
      x_sat * Modelica.Constants.R / MMX[nX] * state.T / state.p +
      (x[nX] - x_sat) * v_water_liq
      "Specific volume equals specific volume of saturated dry air, water vapor,
      and liquid water: Calculated applying the law of ideal gas  mixtures.";
    dv_dX_pT := {sum(1 ./ MMX[1:nX-1] .* Modelica.Constants.R .*
      state.T ./ state.p .* dx_dX[1:nX-1,ind]) for ind in 1:nX} .+
      1 .* Modelica.Constants.R ./ MMX[nX] .* state.T ./ state.p .*
      dx_sat_dX_pT .+
      (dx_dX[nX, 1:nX] - dx_sat_dX_pT) * v_water_liq
      "Partial derivatives equal partial derivatives of saturated dry air, water 
      vapor, and liquid water: Calculated applying the law of ideal gas mixtures.";

  else
    v := sum(x[1:nX-1] ./ MMX[1:nX-1] .* Modelica.Constants.R .*
      state.T ./ state.p) +
      x_sat * Modelica.Constants.R / MMX[nX] * state.T / state.p +
      (x[nX] - x_sat) * v_water_solid
      "Specific volume equals specific volume of saturated dry air, water vapor,
      and solid water: Calculated applying the law of ideal gas  mixtures.";
    dv_dX_pT := {sum(1 ./ MMX[1:nX-1] .* Modelica.Constants.R .*
      state.T ./ state.p .* dx_dX[1:nX-1,ind]) for ind in 1:nX} .+
      1 .* Modelica.Constants.R ./ MMX[nX] .* state.T ./ state.p .*
      dx_sat_dX_pT .+
      (dx_dX[nX, 1:nX] - dx_sat_dX_pT) * v_water_solid
      "Partial derivatives equal partial derivatives of saturated dry air, water 
      vapor, and solid water: Calculated applying the law of ideal gas  mixtures.";

  end if;

  //
  // Convert result
  //
  dv_dX_pT := dv_dX_pT ./ (1 + x[nX]) .- v ./ (x[nX] + 1) ^2 .* dx_dX[nX,1:nX]
    "Partial derivatives per moist air mass";

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function calculates the partial derivatives of the specific volume with
respect to mass fractions given per moist air mass at constant pressure and 
temperature as function of the state record.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 27, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end dv_dX_pT;
