within SorpLib.Media.IdealGasVaporMixtures.Interfaces.PartialIdealGasWaterVaporMixture;
function dryMassFractionSaturation
  "Returns saturation mass fraction of condensing component (i.e., water) (per dry air mass)"
  extends Modelica.Icons.Function;

  //
  // Deifnition of inputs
  //
  input ThermodynamicState state
    "Thermodynamic state record"
    annotation (Dialog(tab="General",group="Inputs"));

  //
  // Deifnition of outputs
  //
  output Modelica.Units.SI.MassFraction x_sat
    "Saturation mass fraction per dry air"
    annotation (Dialog(tab="General",group="Outputs", enable=false));

  //
  // Definition of variables
  //
protected
  Modelica.Units.SI.Pressure p_sat=
    saturationPressureH2O_T(T_sat = state.T)
    "Saturation pressure";

algorithm
  x_sat := MMX[nX] / (state.Y[1:nX-1]./sum(state.Y[1:nX-1]) * MMX[1:nX-1]) *
    p_sat / max((state.p - p_sat), Modelica.Constants.small)
    "Saturation mass fraction per dry air";

  //
  // Assertations
  //
  if print_warnings then
    assert(p_sat <= state.p,
      "Saturation pressure (" + String(p_sat) + " Pa) is greater than total " +
      "pressure (" + String(state.p) + " Pa). Thus, the calculation of the " +
      "saturation mass fraction is not sound!",
      level = AssertionLevel.warning);
  end if;

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This function calculates the saturation mass fraction of the condensing component
(i.e., water) per dry air as function of the state record. Note that the saturation
mass fraction can only be calculated if the partial pressure of the condensing
component is less than the pressure of the ideal gas-vapor mixture.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 27, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end dryMassFractionSaturation;
