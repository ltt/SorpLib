within SorpLib.Media.IdealGasVaporMixtures;
package MoistAir_N2_O2_Ar_H2O "SorpLib: Simple moist air consisting of N2, O2, Ar, and H2O"
  extends
    SorpLib.Media.IdealGasVaporMixtures.Interfaces.PartialIdealGasWaterVaporMixture(
    mediumName="MoistAir_N2_O2_Ar_H2O",
    data={Modelica.Media.IdealGases.Common.SingleGasesData.N2,
          Modelica.Media.IdealGases.Common.SingleGasesData.O2,
          Modelica.Media.IdealGases.Common.SingleGasesData.Ar,
          Modelica.Media.IdealGases.Common.SingleGasesData.H2O},
    fluidConstants={Modelica.Media.IdealGases.Common.FluidData.N2,
                    Modelica.Media.IdealGases.Common.FluidData.O2,
                    Modelica.Media.IdealGases.Common.FluidData.Ar,
                    Modelica.Media.IdealGases.Common.FluidData.H2O},
    substanceNames={"Nitrogen",
                    "Oxygen",
                    "Argon",
                    "Water"},
    final reference_X={0.7552/0.9994,0.2313/0.9994,0.0129/0.9994,1e-5},
    h_dryAir_off = 25097.2,
    s_dryAir_off = -8239.74);

  //
  // Annotations
  //
  annotation (Documentation(info="<html>
<p>
This model calculates the fluid property of moist air modeled as ideal gas-vapor
mixture of N<sub>2</sub>, O<sub>2</sub>, Ar, and H<sub>2</sub>0.
</p>
</html>", revisions="<html>
<ul>
  <li>
  November 28, 2023, by Mirko Engelpracht:<br/>
  First implementation.
  </li>
</ul>
</html>"));
end MoistAir_N2_O2_Ar_H2O;
