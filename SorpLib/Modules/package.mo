within SorpLib;
package Modules "Library containing sorption modules"
  extends Modelica.Icons.VariantsPackage;

  annotation (Documentation(info="<html>
<p>
TO BE ADDED!
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end Modules;
