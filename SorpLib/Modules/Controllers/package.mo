within SorpLib.Modules;
package Controllers "Controller to regulate soprtions modules or sorption systems"
  extends SorpLib.Icons.ControllerPackage;

  annotation (Documentation(info="<html>
<p>
TO BE ADDED!
</p>
</html>", revisions="<html>
<ul>
  <li>
  October 26, 2023, by Mirko Engelpracht:<br/>
  First implementation after restructuring the library.
  </li>
</ul>
</html>"));
end Controllers;
